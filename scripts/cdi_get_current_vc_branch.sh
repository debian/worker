#! /bin/bash

if [ "$#" -ge "1" ]; then
    cd "$1"
    if [ $? -ne "0" ]; then
        echo "(inaccessible)"
        exit
    fi
fi

git_output=$(git --no-optional-locks status -b --porcelain=v2 -uno 2>/dev/null)

if [ "$?" -ne 0 ]; then
    echo "directory"
else
    branch=$(echo "$git_output" | grep branch.head | cut -c15-)
    modified=""
    if [ "$(echo "$git_output" | grep -v '^#' | cut -c4 | grep M | wc -l)" -ge "1" ]; then
        modified="*"
    fi
    echo "directory (git $modified$branch)"
fi
