#! /bin/sh

if [ "$#" -lt "3" ]; then
    exit 1
fi

f1="$1"
f2="$2"
shift
shift

/bin/sh "$@"
res=$?

rm -- "$f1"
rm -- "$f2"

exit $res
