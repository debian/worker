#! /bin/bash

if [ "$#" -lt 1 ]; then
    commitcount=1
else
    commitcount=$1
fi

basedir=$(git rev-parse --show-toplevel 2>/dev/null)
if [ $? -ne 0 ]; then
    exit
fi

git log -$commitcount --name-only --pretty=tformat: -- . |\
    sort -u |
    while read f; do
        fullname="$basedir/$f"
        echo "$fullname"
    done
