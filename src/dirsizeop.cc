/* dirsizeop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dirsizeop.h"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "worker_locale.h"
#include "dirsizeorder.hh"
#include "virtualdirmode.hh"

const char *DirSizeOp::name="DirSizeOp";

DirSizeOp::DirSizeOp() : FunctionProto()
{
}

DirSizeOp::~DirSizeOp()
{
}

DirSizeOp*
DirSizeOp::duplicate() const
{
  DirSizeOp *ta=new DirSizeOp();
  return ta;
}

bool
DirSizeOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
DirSizeOp::getName()
{
  return name;
}

int
DirSizeOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if(dynamic_cast< VirtualDirMode * > ( lm1 ) ) {
              normalmodedirsize( msg );
          } else {
            lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

const char *
DirSizeOp::getDescription()
{
  return catalog.getLocale(1271);
}

int
DirSizeOp::normalmodedirsize( ActionMessage *am )
{
  ListerMode *lm1=NULL;
  struct NM_dirsizeorder dsorder;
  NM_specialsourceExt *specialsource=NULL;
  
  if(startlister==NULL) return 1;
  lm1=startlister->getActiveMode();
  if(lm1==NULL) return 1;
  
  memset( &dsorder, 0, sizeof( dsorder ) );
  if(am->mode==am->AM_MODE_ONLYACTIVE)
    dsorder.source=dsorder.NM_ONLYACTIVE;
  else if(am->mode==am->AM_MODE_DNDACTION) {
    // insert DND-element into list
    dsorder.source=dsorder.NM_SPECIAL;
    dsorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( NULL );
    //TODO: specialsource nach am besetzen (je nachdem wir ich das realisiere)
    dsorder.sources->push_back(specialsource);
  } else if(am->mode==am->AM_MODE_SPECIAL) {
    dsorder.source=dsorder.NM_SPECIAL;
    dsorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( am->getFE() );
    dsorder.sources->push_back(specialsource);
  } else
    dsorder.source=dsorder.NM_ALLENTRIES;

  if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
      vdm->dirsize( &dsorder );
  }

  if(dsorder.source==dsorder.NM_SPECIAL) {
    if ( specialsource != NULL ) delete specialsource;
    delete dsorder.sources;
  }

  return 0;
}

