/* volumemanagerop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "volumemanagerop.hh"
#include "listermode.h"
#include "volumemanagerui.hh"
#include "nwc_fsentry.hh"
#include "nmspecialsourceext.hh"
#include "worker_locale.h"
#include "worker.h"
#include "fileentry.hh"
#include "argclass.hh"
#include "nwc_path.hh"

const char *VolumeManagerOp::name = "VolumeManagerOp";

VolumeManagerOp::VolumeManagerOp() : FunctionProto()
{
}

VolumeManagerOp::~VolumeManagerOp()
{
}

VolumeManagerOp *VolumeManagerOp::duplicate() const
{
    VolumeManagerOp *ta = new VolumeManagerOp();
    return ta;
}

bool VolumeManagerOp::isName(const char *str)
{
    if ( strcmp( str, name ) == 0 )
        return true;
    else
        return false;
}

const char *VolumeManagerOp::getName()
{
    return name;
}

int VolumeManagerOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    Lister *l1;
    ListerMode *lm1;
    
    l1 = msg->getWorker()->getActiveLister();
    if ( l1 == NULL )
        return 1;

    msg->getWorker()->setWaitCursor();

    VolumeManagerUI ui( *Worker::getAGUIX(), msg->getWorker()->getVolumeManager() );

    lm1 = l1->getActiveMode();
    if ( lm1 ) {
        std::list< NM_specialsourceExt > sellist;
        lm1->getSelFiles( sellist, ListerMode::LM_GETFILES_ONLYACTIVE );
            
        std::string dirname, basename;

        dirname = lm1->getCurrentDirectory();

        if ( sellist.empty() == false ) {
            if ( sellist.begin()->entry() != NULL ) {
                basename = sellist.begin()->entry()->name;
                dirname = NWC::Path::dirname( sellist.begin()->entry()->fullname );
            }
        }
            
        ui.setCurrentDirname( dirname );
        ui.setCurrentBasename( basename );
    }
        
   if ( ui.mainLoop() == 1 ) {
        std::string dir = ui.getDirectoryToEnter();
        if ( ! dir.empty() ) {
            NWC::FSEntry fse( dir );
            if ( fse.isDir( true ) && lm1 ) {

                std::list< RefCount< ArgClass > > args;

                args.push_back( RefCount< ArgClass >( new StringArg( dir ) ) );
                lm1->runCommand( "enter_dir", args );
            }
        }
        //TODO maybe issue reload if we did not called enterPath?
    }
    msg->getWorker()->clearNewVolumesInfo();

    msg->getWorker()->unsetWaitCursor();
    return 0;
}

const char *VolumeManagerOp::getDescription()
{
    return catalog.getLocale( 1299 );
}
