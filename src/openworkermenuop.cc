/* openworkermenuop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "openworkermenuop.hh"
#include "listermode.h"
#include "worker_locale.h"
#include "worker.h"

const char *OpenWorkerMenuOp::name = "OpenWorkerMenuOp";

OpenWorkerMenuOp::OpenWorkerMenuOp() : FunctionProto()
{
}

OpenWorkerMenuOp::~OpenWorkerMenuOp()
{
}

OpenWorkerMenuOp *OpenWorkerMenuOp::duplicate() const
{
    OpenWorkerMenuOp *ta = new OpenWorkerMenuOp();
    return ta;
}

bool OpenWorkerMenuOp::isName(const char *str)
{
    if ( strcmp( str, name ) == 0 )
        return true;
    else
        return false;
}

const char *OpenWorkerMenuOp::getName()
{
    return name;
}

int OpenWorkerMenuOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    msg->getWorker()->openWorkerMenu();
    return 0;
}

const char *OpenWorkerMenuOp::getDescription()
{
    return catalog.getLocale( 1295 );
}
