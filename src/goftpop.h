/* goftpop.h
 * This file belongs to Worker, a file manager for UN&X/X11.
 * Copyright (C) 2005-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GOFTPOP_H
#define GOFTPOP_H

#include "wdefines.h"
#include "functionproto.h"
#include <string>
#include <vector>
#include <map>

class GoFTPOp:public FunctionProto
{
public:
  GoFTPOp();
  ~GoFTPOp();
  GoFTPOp( const GoFTPOp &other );
  GoFTPOp &operator=( const GoFTPOp &other );

  GoFTPOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;
  
  int doconfigure(int mode);
  
  void setDontEnterFTP( bool );
  void setHost( std::string );
  void setUser( std::string );
  void setPass( std::string );
  void setAlwaysStorePW( bool );
  void setAVFSModule( const std::string & );
  bool isInteractiveRun() const;
  void setInteractiveRun();

  static const char *name;
protected:
  // Infos to save

  bool dontenterftp, always_store_pw;
  std::string host, user, pass;
  std::string avfs_module;
  
  // temp variables
  //  Lister *startlister,*endlister;
  
  bool tdontenterftp, talways_store_pw;
  std::string thost, tuser, tpass;
  std::string tavfs_module;

  std::map<std::string, bool> pw_stored;

  std::vector<std::string> getAvailModules();
};

#endif
