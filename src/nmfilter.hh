/* nmfilter.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NMFILTER_HH
#define NMFILTER_HH

#include "wdefines.h"
#include <string>

class Datei;

class NM_Filter {
public:
    NM_Filter() {}
    NM_Filter( const NM_Filter &other );
    NM_Filter &operator=( const NM_Filter &other );
    bool operator==( const NM_Filter &other ) const;
    bool operator!=( const NM_Filter &other ) const;
    bool operator<( const NM_Filter &other ) const;
    
    void setPattern( const char* );
    const char *getPattern() const;
    
    typedef enum {
        INACTIVE,
        INCLUDE,
        EXCLUDE
    } check_t;
    
    void setCheck( check_t mode);
    check_t getCheck() const;
    int load();
    int save(Datei*);
    NM_Filter *duplicate();
private:
    std::string pattern;
    check_t _check = INACTIVE;
};

#endif
