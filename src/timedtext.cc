/* timedtex.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "timedtext.hh"

TimedText::TimedText() : m_text( "" ),
                         m_valid_at( 0 ),
                         m_expire_at( 0 )
{
}

TimedText::TimedText( const std::string &text,
                      time_t valid_at,
                      time_t expire_at ) : m_text( text ),
                                           m_valid_at( valid_at ),
                                           m_expire_at( expire_at )
{
}

std::string TimedText::getText() const
{
    return m_text;
}

bool TimedText::isValid( time_t t ) const
{
    if ( t < m_valid_at ) return false;
    if ( m_expire_at > 0 && t >= m_expire_at ) return false;
    return true;
}

bool TimedText::isExpired( time_t t ) const
{
    if ( m_expire_at > 0 && t >= m_expire_at ) return true;
    return false;
}

bool TimedText::isOlder( const TimedText &other ) const
{
    return m_valid_at < other.m_valid_at;
}

time_t TimedText::getValidAt() const
{
    return m_valid_at;
}

time_t TimedText::getExpireAt() const
{
    return m_expire_at;
}
