/* wchotkey.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCHOTKEY_HH
#define WCHOTKEY_HH

#include "wdefines.h"
#include "wckey.hh"
#include "functionproto.h"

class WCDoubleShortkey;
class List;
class Datei;

class WCHotkey:public WCKey
{
public:
  WCHotkey();
  ~WCHotkey();
  WCHotkey( const WCHotkey &other );
  WCHotkey &operator=( const WCHotkey &other );

  WCHotkey *duplicate() const;
  void setName(const char*);
  char *getName();
  command_list_t getComs();
  void setComs( const command_list_t & );
  bool save(Datei*) const;
protected:
  char *name;
  command_list_t com;
};

#endif
