/* timedtext.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TIMEDTEXT_HH
#define TIMEDTEXT_HH

#include "wdefines.h"
#include <string>

class TimedText {
public:
    TimedText();
    TimedText( const std::string &text,
               time_t valid_at = 0,
               time_t expire_at = 0 );

    std::string getText() const;
    time_t getValidAt() const;
    time_t getExpireAt() const;

    bool isValid( time_t t ) const;
    bool isExpired( time_t t ) const;
    bool isOlder( const TimedText &other ) const;
private:
    std::string m_text;
    time_t m_valid_at;
    time_t m_expire_at;
};

#endif
