#include <check.h>
#include <stdlib.h>
#include "aguix/lowlevelfunc.h"
#include "aguix/awidth.h"
#include "aguix/utf8.hh"
#include "filenameshrinker.hh"

START_TEST(shrink_filename_test1)
{
    ACharWidth l;
    FileNameShrinker fns;

    auto res = fns.shrink( "/fkdsjflkjdslfkjds/abcdefdlfdsflhdsjfhdskjfhksdjhfkjdsh", 50, l );
    ck_assert( res == "/fkdsjflkj.../abcdefdlfdsflhdsjfhdskjfhksdjhfkjdsh" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 35, l );
    ck_assert( res == "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 34, l );
    ck_assert( res == "/abc/def/ghi/jkl/.../stu/vwx/yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 30, l );
    ck_assert( res == "/abc/def/ghi/.../stu/vwx/yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 26, l );
    ck_assert( res == "/abc/def/ghi/.../vwx/yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 22, l );
    ck_assert( res == "/abc/def/.../vwx/yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 18, l );
    ck_assert( res == "/abc/def/.../yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 14, l );
    ck_assert( res == "/abc/.../yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 10, l );
    ck_assert( res == "/abc.../yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 6, l );
    ck_assert( res == "/...yz" );

    res = fns.shrink( "/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz", 2, l );
    ck_assert( res == "/a" );
}
END_TEST

Suite * prefix_suite( void )
{
    Suite *s;
    TCase *tc_shrink1;

    s = suite_create("FilenameShrinker");

    /* Core test case */
    tc_shrink1 = tcase_create( "shrink1" );

    tcase_add_test( tc_shrink1, shrink_filename_test1 );
    suite_add_tcase( s, tc_shrink1 );

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = prefix_suite();
    sr = srunner_create( s );

    srunner_set_fork_status( sr, CK_NOFORK );
    srunner_run_all( sr, CK_NORMAL );
    number_failed = srunner_ntests_failed( sr );
    srunner_free( sr );
    return ( number_failed == 0 ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
