/* wconfig_mouse.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2025 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_mouse.hh"
#include "wconfig.h"
#include "worker.h"
#include "aguix/acontainerbb.h"
#include "aguix/cyclebutton.h"
#include "aguix/choosebutton.h"
#include "worker_locale.h"

MousePanel::MousePanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

MousePanel::~MousePanel()
{
}

int MousePanel::create()
{
  Panel::create();

  AContainer *ac1 = setContainer( new AContainer( this, 1, 4 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  addMultiLineText( catalog.getLocale( 674 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  ac1_1->setBorderWidth( 0 );

  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 651 ) ), 0, 0, AContainer::CO_FIX );

  cycb1 = (CycleButton*)ac1_1->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 1, 0, AContainer::CO_INCW );
  cycb1->connect( this );

  cycb1->addOption( catalog.getLocale( 652 ) );
  cycb1->addOption( catalog.getLocale( 653 ) );
  cycb1->addOption( catalog.getLocale( 654 ) );
  cycb1->addOption( catalog.getLocale( 655 ) );
  cycb1->resize( cycb1->getMaxSize(), cycb1->getHeight() );
  ac1_1->readLimits();

  if ( _baseconfig.getMouseSelectButton() == 1 &&
       _baseconfig.getMouseActivateButton() == 2 &&
       _baseconfig.getMouseScrollButton() == 3 &&
       _baseconfig.getMouseSelectMethod() == WConfig::MOUSECONF_NORMAL_MODE ) {
    cycb1->setOption( 0 );
  } else if ( _baseconfig.getMouseSelectButton() == 2 &&
       _baseconfig.getMouseActivateButton() == 1 &&
       _baseconfig.getMouseScrollButton() == 3 &&
       _baseconfig.getMouseSelectMethod() == WConfig::MOUSECONF_NORMAL_MODE ) {
    cycb1->setOption( 1 );
  } else if ( _baseconfig.getMouseSelectButton() == 1 &&
       _baseconfig.getMouseActivateButton() == 2 &&
       _baseconfig.getMouseScrollButton() == 3 &&
       _baseconfig.getMouseSelectMethod() == WConfig::MOUSECONF_ALT_MODE ) {
    cycb1->setOption( 2 );
  } else {
    cycb1->setOption( 3 );
  }

  AContainerBB *ac1_2 = static_cast<AContainerBB*>( ac1->add( new AContainerBB( this, 2, 5 ), 0, 2 ) );
  ac1_2->setMinSpace( 2 );
  ac1_2->setMaxSpace( 2 );
  ac1_2->setBorderWidth( 5 );

  ac1_2->add( new Text( _aguix, 0, 0, catalog.getLocale( 656 ) ), 0, 0, AContainer::CO_FIX );

  AContainer *ac1_2_1 = ac1_2->add( new AContainer( this, 1, 1 ), 1, 0 );
  ac1_2_1->setMinSpace( 5 );
  ac1_2_1->setMaxSpace( 5 );
  ac1_2_1->setBorderWidth( 0 );

  cycbsb = (CycleButton*)ac1_2_1->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 0, 0, AContainer::CO_INCW );
  cycbsb->connect( this );
 
  cycbsb->addOption( catalog.getLocale( 657 ) );
  cycbsb->addOption( catalog.getLocale( 658 ) );
  cycbsb->addOption( catalog.getLocale( 659 ) );
  cycbsb->resize( cycbsb->getMaxSize(), cycbsb->getHeight() );
  ac1_2_1->readLimits();

  switch ( _baseconfig.getMouseSelectButton() ) {
    case 2:
    case 3:
      cycbsb->setOption( _baseconfig.getMouseSelectButton() - 1 );
      break;
    default:
      cycbsb->setOption( 0 );
      break;
  }

  ac1_2->add( new Text( _aguix, 0, 0, catalog.getLocale( 660 ) ), 0, 1, AContainer::CO_FIX );

  AContainer *ac1_2_2 = ac1_2->add( new AContainer( this, 2, 1 ), 1, 1 );
  ac1_2_2->setMinSpace( 5 );
  ac1_2_2->setMaxSpace( 5 );
  ac1_2_2->setBorderWidth( 0 );

  cycbab = (CycleButton*)ac1_2_2->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 0, 0, AContainer::CO_INCW );
  cycbab->connect( this );
  
  cycbab->addOption( catalog.getLocale( 657 ) );
  cycbab->addOption( catalog.getLocale( 658 ) );
  cycbab->addOption( catalog.getLocale( 659 ) );
  cycbab->resize( cycbab->getMaxSize(), cycbab->getHeight() );

  cycbamk = (CycleButton*)ac1_2_2->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 1, 0, AContainer::CO_INCW );
  cycbamk->connect( this );
 
  cycbamk->addOption( catalog.getLocale( 769 ) );
  cycbamk->addOption( catalog.getLocale( 770 ) );
  cycbamk->addOption( catalog.getLocale( 771 ) );
  cycbamk->addOption( catalog.getLocale( 772 ) );
  cycbamk->resize( cycbamk->getMaxSize(), cycbamk->getHeight() );

  ac1_2_2->readLimits();

  switch ( _baseconfig.getMouseActivateButton() ) {
    case 2:
    case 3:
      cycbab->setOption( _baseconfig.getMouseActivateButton() - 1 );
      break;
    default:
      cycbab->setOption( 0 );
      break;
  }

  switch ( _baseconfig.getMouseActivateMod() ) {
    case ShiftMask:
      cycbamk->setOption( 1 );
      break;
    case ControlMask:
      cycbamk->setOption( 2 );
      break;
    case Mod1Mask:
      cycbamk->setOption( 3 );
      break;
    default:
      cycbamk->setOption( 0 );
      break;
  }

  ac1_2->add( new Text( _aguix, 0, 0, catalog.getLocale( 661 ) ), 0, 2, AContainer::CO_FIX );

  AContainer *ac1_2_3 = ac1_2->add( new AContainer( this, 2, 1 ), 1, 2 );
  ac1_2_3->setMinSpace( 5 );
  ac1_2_3->setMaxSpace( 5 );
  ac1_2_3->setBorderWidth( 0 );

  cycbscb = (CycleButton*)ac1_2_3->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 0, 0, AContainer::CO_INCW );
  cycbscb->connect( this );
  
  cycbscb->addOption( catalog.getLocale( 657 ) );
  cycbscb->addOption( catalog.getLocale( 658 ) );
  cycbscb->addOption( catalog.getLocale( 659 ) );
  cycbscb->resize( cycbscb->getMaxSize(), cycbscb->getHeight() );

  cycbscmk = (CycleButton*)ac1_2_3->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 1, 0, AContainer::CO_INCW );
  cycbscmk->connect( this );
 
  cycbscmk->addOption( catalog.getLocale( 769 ) );
  cycbscmk->addOption( catalog.getLocale( 770 ) );
  cycbscmk->addOption( catalog.getLocale( 771 ) );
  cycbscmk->addOption( catalog.getLocale( 772 ) );
  cycbscmk->resize( cycbscmk->getMaxSize(), cycbscmk->getHeight() );

  ac1_2_3->readLimits();

  switch ( _baseconfig.getMouseScrollButton() ) {
    case 2:
    case 3:
      cycbscb->setOption( _baseconfig.getMouseScrollButton() - 1 );
      break;
    default:
      cycbscb->setOption( 0 );
      break;
  }

  switch ( _baseconfig.getMouseScrollMod() ) {
    case ShiftMask:
      cycbscmk->setOption( 1 );
      break;
    case ControlMask:
      cycbscmk->setOption( 2 );
      break;
    case Mod1Mask:
      cycbscmk->setOption( 3 );
      break;
    default:
      cycbscmk->setOption( 0 );
      break;
  }

  ac1_2->add( new Text( _aguix, 0, 0, catalog.getLocale( 773 ) ), 0, 3, AContainer::CO_FIX );

  AContainer *ac1_2_5 = ac1_2->add( new AContainer( this, 2, 1 ), 1, 3 );
  ac1_2_5->setMinSpace( 5 );
  ac1_2_5->setMaxSpace( 5 );
  ac1_2_5->setBorderWidth( 0 );

  cycb_contextb = (CycleButton*)ac1_2_5->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 0, 0, AContainer::CO_INCW );
  cycb_contextb->connect( this );
  
  cycb_contextb->addOption( catalog.getLocale( 657 ) );
  cycb_contextb->addOption( catalog.getLocale( 658 ) );
  cycb_contextb->addOption( catalog.getLocale( 659 ) );
  cycb_contextb->resize( cycb_contextb->getMaxSize(), cycb_contextb->getHeight() );

  cycb_contextm = (CycleButton*)ac1_2_5->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 1, 0, AContainer::CO_INCW );
  cycb_contextm->connect( this );
 
  cycb_contextm->addOption( catalog.getLocale( 769 ) );
  cycb_contextm->addOption( catalog.getLocale( 770 ) );
  cycb_contextm->addOption( catalog.getLocale( 771 ) );
  cycb_contextm->addOption( catalog.getLocale( 772 ) );
  cycb_contextm->resize( cycb_contextm->getMaxSize(), cycb_contextm->getHeight() );

  ac1_2_5->readLimits();

  switch ( _baseconfig.getMouseContextButton() ) {
    case 2:
    case 3:
      cycb_contextb->setOption( _baseconfig.getMouseContextButton() - 1 );
      break;
    default:
      cycb_contextb->setOption( 0 );
      break;
  }

  switch ( _baseconfig.getMouseContextMod() ) {
    case ShiftMask:
      cycb_contextm->setOption( 1 );
      break;
    case ControlMask:
      cycb_contextm->setOption( 2 );
      break;
    case Mod1Mask:
      cycb_contextm->setOption( 3 );
      break;
    default:
      cycb_contextm->setOption( 0 );
      break;
  }

  ac1_2->add( new Text( _aguix, 0, 0, catalog.getLocale( 662 ) ), 0, 4, AContainer::CO_FIX );

  AContainer *ac1_2_4 = ac1_2->add( new AContainer( this, 1, 1 ), 1, 4 );
  ac1_2_4->setMinSpace( 5 );
  ac1_2_4->setMaxSpace( 5 );
  ac1_2_4->setBorderWidth( 0 );

  cycbsmb = (CycleButton*)ac1_2_4->add( new CycleButton( _aguix, 0, 0, 10, 0 ), 0, 0, AContainer::CO_INCW );
  cycbsmb->connect( this );
  cycbsmb->addOption( catalog.getLocale( 663 ) );
  cycbsmb->addOption( catalog.getLocale( 664 ) );
  cycbsmb->resize( cycbsmb->getMaxSize(), cycbsmb->getHeight() );
  ac1_2_4->readLimits();

  if ( _baseconfig.getMouseSelectMethod() == WConfig::MOUSECONF_ALT_MODE ) {
    cycbsmb->setOption( 1 );
  } else {
    cycbsmb->setOption( 0 );
  }

  m_delayed_content_menu_cb = ac1->addWidget( new ChooseButton( _aguix,
                                                                0, 0,
                                                                _baseconfig.getMouseContextMenuWithDelayedActivate(),
                                                                catalog.getLocale( 1572 ),
                                                                LABEL_LEFT, 0 ),
                                              0, 3, AContainer::CO_FIXNR );

  contMaximize( true );
  return 0;
}

int MousePanel::saveValues()
{
  _baseconfig.setMouseSelectButton( cycbsb->getSelectedOption() + 1 );
  _baseconfig.setMouseActivateButton( cycbab->getSelectedOption() + 1 );
  _baseconfig.setMouseScrollButton( cycbscb->getSelectedOption() + 1 );
  _baseconfig.setMouseContextButton( cycb_contextb->getSelectedOption() + 1 );
  switch ( cycbsmb->getSelectedOption() ) {
    case 1:
      _baseconfig.setMouseSelectMethod( WConfig::MOUSECONF_ALT_MODE );
      break;
    default:
      _baseconfig.setMouseSelectMethod( WConfig::MOUSECONF_NORMAL_MODE );
      break;
  }
  switch ( cycbamk->getSelectedOption() ) {
    case 1:
      _baseconfig.setMouseActivateMod( ShiftMask );
      break;
    case 2:
      _baseconfig.setMouseActivateMod( ControlMask );
      break;
    case 3:
      _baseconfig.setMouseActivateMod( Mod1Mask );
      break;
    default:
      _baseconfig.setMouseActivateMod( 0 );
      break;
  }
  switch ( cycbscmk->getSelectedOption() ) {
    case 1:
      _baseconfig.setMouseScrollMod( ShiftMask );
      break;
    case 2:
      _baseconfig.setMouseScrollMod( ControlMask );
      break;
    case 3:
      _baseconfig.setMouseScrollMod( Mod1Mask );
      break;
    default:
      _baseconfig.setMouseScrollMod( 0 );
      break;
  }
  switch ( cycb_contextm->getSelectedOption() ) {
    case 1:
      _baseconfig.setMouseContextMod( ShiftMask );
      break;
    case 2:
      _baseconfig.setMouseContextMod( ControlMask );
      break;
    case 3:
      _baseconfig.setMouseContextMod( Mod1Mask );
      break;
    default:
      _baseconfig.setMouseContextMod( 0 );
      break;
  }

  _baseconfig.setMouseContextMenuWithDelayedActivate( m_delayed_content_menu_cb->getState() );

  return 0;
}

void MousePanel::run( Widget *elem, const AGMessage &msg )
{
  if ( msg.type == AG_CYCLEBUTTONCLICKED ) {
    if ( msg.cyclebutton.cyclebutton == cycb1 ) {
      switch ( msg.cyclebutton.option ) {
        case 1:
          cycbsb->setOption( 1 );
          cycbab->setOption( 0 );
          cycbscb->setOption( 2 );
          cycbsmb->setOption( 0 );
          cycb_contextb->setOption( 2 );
          cycbamk->setOption( 0 );
          cycbscmk->setOption( 1 );
          cycb_contextm->setOption( 0 );
          break;
        case 2:
          cycbsb->setOption( 0 );
          cycbab->setOption( 1 );
          cycbscb->setOption( 2 );
          cycbsmb->setOption( 1 );
          cycb_contextb->setOption( 2 );
          cycbamk->setOption( 0 );
          cycbscmk->setOption( 1 );
          cycb_contextm->setOption( 0 );
          break;
        case 3:
          break;
        default:
          cycbsb->setOption( 0 );
          cycbab->setOption( 1 );
          cycbscb->setOption( 2 );
          cycbsmb->setOption( 0 );
          cycb_contextb->setOption( 2 );
          cycbamk->setOption( 0 );
          cycbscmk->setOption( 1 );
          cycb_contextm->setOption( 0 );
          break;
      }
    } else if ( msg.cyclebutton.cyclebutton == cycbsb ||
                msg.cyclebutton.cyclebutton == cycbab ||
                msg.cyclebutton.cyclebutton == cycbscb ||
                msg.cyclebutton.cyclebutton == cycbsmb ||
                msg.cyclebutton.cyclebutton == cycb_contextb ||
                msg.cyclebutton.cyclebutton == cycbamk ||
                msg.cyclebutton.cyclebutton == cycbscmk ||
                msg.cyclebutton.cyclebutton == cycb_contextm ) {
      cycb1->setOption( 3 );
    }
  }
}
