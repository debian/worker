/* copy_parameters.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COPY_PARAMETERS_H
#define COPY_PARAMETERS_H

#include "wdefines.h"

namespace CopyParams {

    typedef enum { LEAVE_PERMISSIONS_UNMODIFIED,
                   ENSURE_USER_RW_PERMISSION,
                   ENSURE_USER_RW_GROUP_R_PERMISSION,
                   ENSURE_USER_RW_ALL_R_PERMISSION
    } ensure_mode_t;

    class EnsureFilePermissions {
    public:

        EnsureFilePermissions() : m_val( LEAVE_PERMISSIONS_UNMODIFIED ) {}
        EnsureFilePermissions( const ensure_mode_t &val ) : m_val( val ) {}

        const ensure_mode_t &operator()() const
        {
            return m_val;
        }

        void operator()( const ensure_mode_t &val )
        {
            m_val = val;
        }
    private:
        ensure_mode_t m_val;
    };

};

#endif /* COPY_PARAMETERS_H */
