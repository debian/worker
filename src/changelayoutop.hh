/* changelayoutop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHANGELAYOUTOP_H
#define CHANGELAYOUTOP_H

#include "wdefines.h"
#include "functionproto.h"
#include "layoutsettings.hh"
#include <map>
#include <utility>

class Worker;
class ChooseButton;
class AContainer;
class SolidButton;
class FieldListView;
class Button;
class AWindow;
class Slider;

class ChangeLayoutOp : public FunctionProto
{
public:
    ChangeLayoutOp();
    ~ChangeLayoutOp();
    ChangeLayoutOp( const ChangeLayoutOp &other );
    ChangeLayoutOp &operator=( const ChangeLayoutOp &other );
    
    ChangeLayoutOp *duplicate() const override;
    bool isName( const char * ) override;
    const char *getName() override;

    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;

    bool save( Datei* ) override;
    const char *getDescription() override;
    int configure() override;

    void setLayout( const LayoutSettings &l );

    static const char *name;
private:
    // Infos to save
    LayoutSettings m_layout;

    AGUIX *m_aguix;
    AWindow *m_win;
    bool m_bv, m_lvv;
    ChooseButton *m_cb1, *m_cb2;
    AContainer *m_ac1, *m_ac2;
    AContainer *m_button_ac;
    AContainer *m_lv_ac;
    AContainer *m_lv2_ac;
    AContainer *m_available_elements_ac;
    AContainer *m_used_elements_ac;

    std::map< std::pair< bool, bool >, LayoutSettings::available_elements_t > m_available_elements;
    AContainer *m_button_example_ac;
    AContainer *m_listview_example_ac;
    std::list<SolidButton*> m_example_buttons;
    SolidButton *m_listview_example_b1, *m_listview_example_b2;

    FieldListView *m_available_elements_lv;
    FieldListView *m_used_elements_lv;
    Button *m_add_element_b;
    Button *m_remove_element_b;
    AContainer *m_elements_example_ac1;
    AContainer *m_elements_example_ac2;
    SolidButton *m_example_statebar_b;
    SolidButton *m_example_clockbar_b;
    std::list<SolidButton*> m_example_buttons2;
    SolidButton *m_listview_example2_b1, *m_listview_example2_b2;
    std::list< LayoutSettings::layoutID_t > m_used_elements;
    Slider *m_weight_sl;
    SolidButton *m_left_weight_b, *m_right_weight_b;
    ChooseButton *m_weight_active_cb;

    void setupExampleContainers();
    void setupElementsExample();
    void setupElementsLV();
    void updateWeightSlider();
};

#endif
