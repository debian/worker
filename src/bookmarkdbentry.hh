/* bookmarkdbentry.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2007-2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BOOKMARKDBENTRY_HH
#define BOOKMARKDBENTRY_HH

#include "wdefines.h"
#include <string>

class BookmarkDBEntry
{
public:
    BookmarkDBEntry( const std::string &category,
                     const std::string &name,
                     const std::string &alias = "",
                     bool use_parent = false,
                     int last_access = 0,
                     bool sticky = false );

    bool equals( const BookmarkDBEntry &e, bool ignore_nr = false ) const;

    void setCategory( const std::string &cat );
    void setName( const std::string &name );
    void setAlias( const std::string &name );
    void setUseParent( bool use_parent );
    void setLastAccess( int last_access );
    void setSticky( bool sticky );

    std::string getCategory() const;
    std::string getName() const;
    std::string getAlias() const;
    bool getUseParent() const;
    int getLastAccess() const;
    bool getSticky() const;

    inline bool operator==( const BookmarkDBEntry &rhs ) const
    {
        return equals( rhs );
    }
private:
    std::string _category;
    std::string _name;
    std::string _alias;
    bool _use_parent;
    int _last_access;
    bool _sticky;

    int _nr;
};

#endif
