/* pers_string_list.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009,2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "pers_string_list.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include "nwc_fsentry.hh"
#include <algorithm>

PersistentStringList::PersistentStringList( const std::string &filename ) : m_filename( filename ),
                                                                            m_lastmod( 0 ),
                                                                            m_lastsize( 0 )
{
}

std::list< std::string > PersistentStringList::getList()
{
    read();
    return m_list;
}

void PersistentStringList::pushEntry( const std::string &v )
{
    read();
    m_list.push_back( v );
    write();
}

void PersistentStringList::removeEntry( const std::string &v )
{
    read();
    m_list.remove( v );
    write();
}

void PersistentStringList::read()
{
    NWC::FSEntry fe( m_filename );

    if ( ! fe.entryExists() ) return;

    if ( fe.isLink() ) {
        if ( fe.stat_dest_lastmod() == m_lastmod &&
             fe.stat_dest_size() == m_lastsize ) return;
    } else {
        if ( fe.stat_lastmod() == m_lastmod &&
             fe.stat_size() == m_lastsize ) return;
    }

    std::ifstream ifile( m_filename.c_str() );
    std::string line;

    m_list.clear();
    
    if ( fe.isLink() ) {
        m_lastmod = fe.stat_dest_lastmod();
        m_lastsize = fe.stat_dest_size();
    } else {
        m_lastmod = fe.stat_lastmod();
        m_lastsize = fe.stat_size();
    }

    if ( ifile.is_open() ) {
        while ( std::getline( ifile, line ) ) {
            m_list.push_back( line );
        }
    }
}

void PersistentStringList::write()
{
    std::string temp_target_file = m_filename;
    temp_target_file += ".new";

    bool fail = false;

    {
        std::ofstream ofile( temp_target_file.c_str() );

        for ( std::list< std::string >::const_iterator it1 = m_list.begin();
              it1 != m_list.end();
              it1++ ) {
            ofile << *it1 << std::endl;
        }

        ofile.flush();

        if ( ofile.fail() ) {
            fail = true;
        }
    }

    if ( ! fail ) {
        worker_rename( temp_target_file.c_str(), m_filename.c_str() );
    } else {
        worker_unlink( temp_target_file.c_str() );
    }
    //TODO set lastsize and lastmod to avoid next read?
}

bool PersistentStringList::contains( const std::string &v )
{
    read();
    if ( std::find( m_list.begin(),
                    m_list.end(), v ) != m_list.end() ) return true;
    return false;
}

int PersistentStringList::findPosition( const std::string &v )
{
    read();

    int pos = 0;

    for ( auto &it1 : m_list ) {
        if ( it1 == v ) {
            return pos;
        }

        pos++;
    }

    return -1;
}

std::list< std::string >::size_type PersistentStringList::size()
{
    read();
    return m_list.size();
}

void PersistentStringList::pushFrontEntry( const std::string &v )
{
    read();
    m_list.push_front( v );
    write();
}

void PersistentStringList::removeFirst()
{
    read();
    m_list.pop_front();
    write();
}

void PersistentStringList::removeLast()
{
    read();
    m_list.pop_back();
    write();
}
