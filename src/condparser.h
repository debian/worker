/* condparser.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2004-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CONDPARSER_H
#define CONDPARSER_H

#include "wdefines.h"
#include "aguix/compregex.hh"

class FileEntry;

class CondParser
{
public:
  CondParser();
  ~CondParser();

  int parse( const char *str, FileEntry *tfe );
  int getLastError() const;
  void resetLastError();
  static const char *requestFlag();
  char *getOutput( const char *str, FileEntry *tfe );
  void setIgnoreContent( bool nv );
  bool getIgnoreContent() const;
private:
  enum {
    TOK_NONE,
    TOK_CHAR,
    TOK_NUM,
    TOK_DONE,
    TOK_LASTERROR,
    TOK_STRING,
    TOK_TRUE,
    TOK_FALSE,
    TOK_P_AND,
    TOK_P_OR,
    TOK_E,
    TOK_NE,
    TOK_LT,
    TOK_LE,
    TOK_GT,
    TOK_GE,
    TOK_TONUM,
    TOK_TOSTR,
    TOK_REGEQ,
    TOK_ISREG,
    TOK_ISSOCK,
    TOK_ISFIFO,
    TOK_ISSYMLINK,
    TOK_ISBROKENSYMLINK,
    TOK_SIZE,
    TOK_PERM,
    TOK_TOLOWER,
    TOK_TOUPPER,
    TOK_CONTENTSTR,
    TOK_CONTENTNUM,
    TOK_NAME,
    TOK_FULLNAME,
    TOK_ISLOCAL
  };
  static struct deftokens {
    const char *str;
    int token;
  } keywords[];
  enum { BUFSIZE = 1024 };
  char lexbuf[BUFSIZE];
  int lookahead, tokenval;

  static struct flaghelp {
    const char *flag;
    int catID;
    int catflagID;
  } flags[];

  struct erg_t {
    int numval;
    char *strval;
    int wasstring;
  };

  erg_t *expr();
  erg_t *factor();
  int lookup( const char *s );
  int insert( const char *s, int tok );
  int lex();
  int match( int t );
  char *readBrace();
  int do_parse();

  const char *scanbuf;
  int scanbuflen;
  int curpos;
  int ignore;
  int lasterror;

  char *replaceFlags( const char *sstr, bool quote = true );
  FileEntry *fe;
  
  CompRegEx re;
  bool ignoreContent;
};

#endif

