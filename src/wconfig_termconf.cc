/* wconfig_termconf.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_termconf.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include "aguix/choosebutton.h"

TermConfPanel::TermConfPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

TermConfPanel::~TermConfPanel()
{
}

int TermConfPanel::create()
{
  Panel::create();

  AContainer *ac1 = setContainer( new AContainer( this, 1, 5 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 687 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );
  
  ac1->add( new Text( _aguix,
                      0,
                      0,
                      catalog.getLocale( 134 )
                      ), 0, 1, AContainer::CO_INCWNR );

  sg = (StringGadget*)ac1->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getTerminalBin(), 0 ),
                                0, 2, AContainer::CO_INCW );
  sg->connect( this );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ),
                                0, 3 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  validtext = (Text*)ac1_1->add( new Text( _aguix,
                                           0,
                                           0,
                                           catalog.getLocale( 720 )
                                           ),
                                 0, 0, AContainer::CO_INCW );
  defaultbutton = (Button*)ac1_1->add( new Button( _aguix,
                                                   0,
                                                   0,
                                                   catalog.getLocale( 196 ),
                                                   0 ),
                                       1, 0, AContainer::CO_FIX );
  defaultbutton->connect( this );

  m_terminal_returns_early = ac1->addWidget( new ChooseButton( _aguix,
                                                               0, 0, _baseconfig.getTerminalReturnsEarly(),
                                                               catalog.getLocale( 1328 ), LABEL_RIGHT, 0 ),
                                             0, 4, AContainer::CO_INCWNR );
  
  contMaximize( true );
  return 0;
}

int TermConfPanel::saveValues()
{
  _baseconfig.setTerminalBin( sg->getText() );
  _baseconfig.setTerminalReturnsEarly( m_terminal_returns_early->getState() );
  return 0;
}

void TermConfPanel::run( Widget *elem, const AGMessage &msg )
{
  if ( msg.type == AG_STRINGGADGET_CONTENTCHANGE ) {
    if ( msg.stringgadget.sg == sg ) {
      if ( WConfig::isCorrectTerminalBin( sg->getText() ) == false ) {
        validtext->setText( catalog.getLocale( 721 ) );
      } else {
        validtext->setText( catalog.getLocale( 720 ) );
      }
    }
  } else if ( msg.type == AG_BUTTONCLICKED ) {
    if ( msg.button.button == defaultbutton ) {
      sg->setText( TERMINAL_BIN );
      validtext->setText( catalog.getLocale( 720 ) );
    }
  }
}
