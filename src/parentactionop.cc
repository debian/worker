/* parentactionop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2004-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "parentactionop.h"
#include "listermode.h"
#include "worker.h"
#include "wconfig.h"
#include "wcfiletype.hh"
#include "worker_locale.h"

const char *ParentActionOp::name = "ParentActionOp";

ParentActionOp::ParentActionOp() : FunctionProto()
{
    m_category = FunctionProto::CAT_FILETYPE;
}

ParentActionOp::~ParentActionOp()
{
}

ParentActionOp*
ParentActionOp::duplicate() const
{
  ParentActionOp *ta=new ParentActionOp();
  return ta;
}

bool
ParentActionOp::isName( const char *str )
{
  if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *
ParentActionOp::getName()
{
  return name;
}

int
ParentActionOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  WCFiletype *ft;

  if ( msg->filetype != NULL ) {
    ft = msg->filetype->getParentType();
    if ( ft != NULL ) {
      ActionMessage amsg2( msg->getWorker() );
      command_list_t l1;
 
      amsg2.mode = msg->mode;
      amsg2.startLister = msg->startLister;
      // FE is already a copy of the real FE so we don't need to make another copy
      //amsg2.fe = msg->fe;
      
      // but currently I do it anyway
      amsg2.setFE( msg->getFE() );

      if ( msg->mode == ActionMessage::AM_MODE_SPECIFIC_ENTRIES ) {
          amsg2.setEntriesToConsider( msg->getEntriesToConsider() );
      }
      
      amsg2.filetype = ft;
      amsg2.m_action_descr = msg->m_action_descr;

      if ( msg->m_action_descr.get() != NULL ) {
          l1 = ft->getActionList( false, *(msg->m_action_descr) );
      }

      msg->getWorker()->interpret( l1, &amsg2 );
    }
  }

  return 0;
}

const char *
ParentActionOp::getDescription()
{
  return catalog.getLocale( 1287 );
}
