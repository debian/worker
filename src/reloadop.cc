/* reloadop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "reloadop.h"
#include "listermode.h"
#include "worker.h"
#include "datei.h"
#include "worker_locale.h"
#include "aguix/cyclebutton.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "dnd.h"
#include "virtualdirmode.hh"

const char *ReloadOp::name="ReloadOp";

ReloadOp::ReloadOp() : FunctionProto()
{
  reloadside=RELOADOP_THISSIDE;
  reset_dirsizes=false;
  keep_filetypes = false;
  hasConfigure = true;
}

ReloadOp::~ReloadOp()
{
}

ReloadOp*
ReloadOp::duplicate() const
{
  ReloadOp *ta=new ReloadOp();
  ta->reloadside=reloadside;
  ta->reset_dirsizes=reset_dirsizes;
  ta->keep_filetypes = keep_filetypes;
  return ta;
}

bool
ReloadOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
ReloadOp::getName()
{
  return name;
}

int
ReloadOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      normalmodereload( msg );
    }
  } else normalmodereload( msg );
  return 0;
}

bool
ReloadOp::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  switch(reloadside) {
    case RELOADOP_OTHERSIDE:
      fh->configPutPair( "mode", "other" );
      break;
    case RELOADOP_LEFTSIDE:
      fh->configPutPair( "mode", "left" );
      break;
    case RELOADOP_RIGHTSIDE:
      fh->configPutPair( "mode", "right" );
      break;
    default:
      fh->configPutPair( "mode", "current" );
      break;
  }
  fh->configPutPairBool( "resetdirsizes", reset_dirsizes );
  fh->configPutPairBool( "keepfiletypes", keep_filetypes );
  return true;
}

const char *
ReloadOp::getDescription()
{
  return catalog.getLocale(1267);
}

void
ReloadOp::normalmodereload( ActionMessage *am )
{
  ListerMode *lm1=NULL;
  Lister *ll,*lr,*tl;
  
  ll = am->getWorker()->getLister(0);
  lr = am->getWorker()->getLister(1);
  
  tl=NULL;
  switch(reloadside) {
    case RELOADOP_OTHERSIDE:
      if(am->mode==am->AM_MODE_DNDACTION) {
        tl = am->dndmsg->getDestLister();
      } else {
        if(startlister==ll) tl=lr;
        else tl=ll;
      }
      break;
    case RELOADOP_LEFTSIDE:
      tl=ll;
      break;
    case RELOADOP_RIGHTSIDE:
      tl=lr;
      break;
    default:
      if(am->mode==am->AM_MODE_DNDACTION) {
        tl = am->dndmsg->getSourceLister();
      } else tl=startlister;
      break;
  }
  if(tl!=NULL) {
    lm1=tl->getActiveMode();
    if(lm1!=NULL) {
      if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
          vdm->update( reset_dirsizes, keep_filetypes );
      } else {
          lm1->not_supported();
      }
    }
  }
}

int
ReloadOp::configure()
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  CycleButton *rcyb;
  ChooseButton *rdscb, *kftcb;
  AGMessage *msg;
  int endmode=-1;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr=(char*)_allocsafe(strlen(catalog.getLocale(293))+strlen(getDescription())+1);
  sprintf(tstr,catalog.getLocale(293),getDescription());
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 4 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  ac1_1->setBorderWidth( 0 );

  ac1_1->add( new Text( aguix, 0, 0, catalog.getLocale( 339 ) ), 0, 0, cfix );
  rcyb = (CycleButton*)ac1_1->add( new CycleButton( aguix, 0, 0, 100, 0 ), 1, 0, cincw );
  rcyb->addOption(catalog.getLocale(324));
  rcyb->addOption(catalog.getLocale(325));
  rcyb->addOption(catalog.getLocale(326));
  rcyb->addOption(catalog.getLocale(327));
  rcyb->resize(rcyb->getMaxSize(),rcyb->getHeight());
  ac1_1->readLimits();
  switch(reloadside) {
    case RELOADOP_OTHERSIDE:
      rcyb->setOption(1);
      break;
    case RELOADOP_LEFTSIDE:
      rcyb->setOption(2);
      break;
    case RELOADOP_RIGHTSIDE:
      rcyb->setOption(3);
      break;
    default:
      rcyb->setOption(0);
      break;
  }
  
  rdscb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, ( reset_dirsizes == true ) ? 1 : 0,
						     catalog.getLocale( 385 ), LABEL_RIGHT, 0 ), 0, 1, cincwnr );

  kftcb = (ChooseButton*)ac1->add( new ChooseButton( aguix,
                                                     0,
                                                     0,
                                                     ( keep_filetypes == true ) ? 1 : 0,
                                                     catalog.getLocale( 434 ),
                                                     LABEL_RIGHT,
                                                     0 ), 0, 2, cincwnr  );
  
  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 3 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( -1 );
  ac1_2->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_2->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );
  
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) endmode=0;
          else if(msg->button.button==cb) endmode=1;
          break;
        case AG_KEYPRESSED:
            if ( win->isParent( msg->key.window, false ) == true ) {
                switch ( msg->key.key ) {
                    case XK_Escape:
                        endmode = 1;
                        break;
                }
            }
            break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  
  if(endmode==0) {
    // ok
    switch(rcyb->getSelectedOption()) {
      case 1:
        reloadside=RELOADOP_OTHERSIDE;
        break;
      case 2:
        reloadside=RELOADOP_LEFTSIDE;
        break;
      case 3:
        reloadside=RELOADOP_RIGHTSIDE;
        break;
      default:
        reloadside=RELOADOP_THISSIDE;
        break;
    }
    reset_dirsizes = rdscb->getState();
    keep_filetypes = kftcb->getState();
  }
  
  delete win;

  return endmode;
}

void ReloadOp::setReloadside(reload_t nv)
{
  reloadside=nv;
}

void ReloadOp::setResetDirSizes(bool nv)
{
  reset_dirsizes=nv;
}

void ReloadOp::setKeepFiletypes( bool nv )
{
  keep_filetypes = nv;
}

