/* searchentryop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SEARCHENTRYOP_H
#define SEARCHENTRYOP_H

#include "wdefines.h"
#include "functionproto.h"

class SearchEntryOp:public FunctionProto
{
public:
  SearchEntryOp();
  ~SearchEntryOp();
  SearchEntryOp( const SearchEntryOp &other );
  SearchEntryOp &operator=( const SearchEntryOp &other );

  SearchEntryOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;
  bool save(Datei*) override;

  void setIgnoreCase( bool );
  void setReverseSearch( bool );
  void setInfixSearch( bool );
  void setFlexibleMatch( bool );
  void setStartSearchString( const std::string &str );
  void setApplyOnly( bool );
  int configure() override;
protected:
  static const char *name;
  // Infos to save
    bool ignore_case, reverse_search, m_infix_search, m_flexible_match;
    std::string m_start_search_string;
    bool m_apply_only = false;

  // temp variables
  Lister *startlister;
  
    int vdm_searchentry();
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
