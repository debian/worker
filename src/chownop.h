/* chownop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHOWNOP_H
#define CHOWNOP_H

#include "wdefines.h"
#include "functionproto.h"

class ChOwnOp:public FunctionProto
{
public:
  ChOwnOp();
  virtual ~ChOwnOp();
  ChOwnOp( const ChOwnOp &other );
  ChOwnOp &operator=( const ChOwnOp &other );

  ChOwnOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;
  
  int doconfigure(int mode);
  
  void setOnFiles(bool);
  void setOnDirs(bool);
  void setRecursive(bool);
  bool isInteractiveRun() const;
  void setInteractiveRun();
protected:
  static const char *name;
  // Infos to save

  bool onfiles,
       ondirs,
       recursive;
  
  // temp variables
  Lister *startlister;

  bool tonfiles,
       tondirs,
       trecursive;
  
  int normalmodechown( ActionMessage* );
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
