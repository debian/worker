/* persdeeppathstore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2012-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PERSDEEPPATHSTORE_HH
#define PERSDEEPPATHSTORE_HH

#include "wdefines.h"
#include "deeppathstore.hh"
#include <list>
#include <string>
#include <thread>
#include "aguix/condvar.h"
#include <future>
#include <map>

class PersDeepPathStore
{
public:
    PersDeepPathStore( const std::string &filename );
    ~PersDeepPathStore();

    void storePath( const std::string &path, time_t ts );
    void storePathByKeyword( const std::string &keyword,
                             const std::string &path,
                             time_t ts );
    DeepPathStore getCopy();
    std::map< std::string, DeepPathStore > getKeywordCopy();

    void relocateEntries( const std::string &dest,
                          const std::string &source,
                          time_t ts,
                          bool move );

    void removeEntry( const std::string &path );
    void removeKeyword( const std::string &keyword );

    loff_t getLastSize() const;
    loff_t getLastProgSize() const;
private:
    DeepPathStore m_store;
    std::map< std::string, DeepPathStore > m_store_by_keyword;
    std::string m_main_db_file;
    std::string m_old_main_db_file;
    std::string m_program_db_file;
    std::string m_lock_file;
    time_t m_lastmod = 0;
    loff_t m_lastsize = 0;
    time_t m_lastmod_prog = 0;
    loff_t m_lastsize_prog = 0;
    bool m_prog_dirty = false;

    struct bg_threading {
        bg_threading() : stop_thread( false )
        {}

        std::thread thread_id;
        CondVar order_cv;

        std::list< std::promise< DeepPathStore > > results;
        std::list< std::promise< std::map< std::string, DeepPathStore > > > keyword_results;
        std::list< std::pair< std::string, time_t > > push_list;
        std::list< std::tuple< std::string, std::string, time_t > > keyword_push_list;
        std::list< std::tuple< std::string, std::string, time_t, bool > > relocate_list;
        std::list< std::string > remove_list;
        std::list< std::string > remove_list_keyword;
        bool stop_thread;
    } m_bg_threading;

    void read();
    void write();

    void bg_thread();

    void addPaths( const std::list< std::pair< std::string, time_t > > &list );
    void addPathsByKeyword( const std::list< std::tuple< std::string, std::string, time_t > > &list );

    void processRelocateOrders( const std::list< std::tuple< std::string, std::string, time_t, bool > > &relocate_list );

    void removeEntries( const std::list< std::string > &paths );
    void removeKeywords( const std::list< std::string > &keywords );
};

#endif
