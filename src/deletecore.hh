/* deletecore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DELETECORE_HH
#define DELETECORE_HH

#include "wdefines.h"
#include <list>
#include <functional>
#include <string>
#include "fileentry.hh"

class DeleteOpWin;
struct NM_deleteorder;
class FileEntry;
class NM_CopyOp_Dir;

class DeleteCore
{
public:
    DeleteCore( struct deleteorder *delorder );
    ~DeleteCore();

    void setCancel( bool nv );
    bool getCancel() const;

    void registerFEToDelete( const FileEntry &fe,
                             int row );

    int buildDeleteDatabase();

    int executeDelete();

    typedef enum { NM_DELETE_OK,
                   NM_DELETE_SKIP,
                   NM_DELETE_CANCEL } nm_delete_t;

    void setPreDeleteCallback( std::function< void( const FileEntry &fe, int row, const NM_CopyOp_Dir *cod ) > cb );
    void setPostDeleteCallback( std::function< void( const FileEntry &fe, int row,
                                                     nm_delete_t res, const NM_CopyOp_Dir *cod ) > cb );
private:
    struct deleteorder *m_delorder;

    bool m_cancel;

    class delete_base_entry {
    public:
        delete_base_entry( const FileEntry &tfe, int row );
        ~delete_base_entry();
        const FileEntry &entry() const
        {
            return m_fe;
        }
        int m_row;  // corresponding row or -1
        NM_CopyOp_Dir *m_cod; // when fe is dir, this will be the corresponding structure
    private:
        FileEntry m_fe;  // element to copy
    };

    std::list< delete_base_entry > m_del_list;

    std::function< void( const FileEntry &fe, int row, const NM_CopyOp_Dir *cod ) > m_pre_cb;
    std::function< void( const FileEntry &fe, int row,
                         nm_delete_t res, const NM_CopyOp_Dir *cod ) > m_post_cb;

    nm_delete_t delfile( const FileEntry *fe, struct deleteorder *delorder, bool dir );

    nm_delete_t deldir( const FileEntry *fe, NM_CopyOp_Dir *cod, struct deleteorder *delorder );
};

#endif
