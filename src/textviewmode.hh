/* textviewmode.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TEXTVIEWMODE_HH
#define TEXTVIEWMODE_HH

#include "wdefines.h"
#include "listermode.h"
#include <cinttypes>

class Lister;
class AContainer;
class TextView;
class FileEntry;
class TextStorageFile;
class TextStorageString;
class Text;
class AWidth;

class TextViewMode:public ListerMode
{
public:
    TextViewMode( Lister * );
    virtual ~TextViewMode();
    TextViewMode( const TextViewMode &other );
    TextViewMode &operator=( const TextViewMode &other );

    void messageHandler( AGMessage * ) override;
    void on() override;
    void off() override;
    void activate() override;
    void deactivate() override;
    bool isType( const char * ) override;
    const char *getType() override;
    int configure() override;
    void cyclicfunc( cyclicfunc_mode_t mode ) override;
    const char* getLocaleName() override;
    int load() override;
    bool save( Datei * ) override;
    void relayout() override;

    static const char *getStaticLocaleName();
    static const char *getStaticType();

    void command_up();
    void command_down();
    void command_first();
    void command_last();
    void command_pageup();
    void command_pagedown();

    enum source_mode {
        ACTIVE_FILE_OTHER_SIDE,
        COMMAND_STR,
        BUFFER,
        FILE_TYPE_ACTION
    };

    enum options {
        FOLLOW_ACTIVE = 1 << 0,
        WATCH_FILE = 1 << 1
    };

    void setCommandString( const std::string &cmd,
                           const std::string &basedir,
                           uint32_t options );
    void setBuffer( const std::string &buffer,
                    uint32_t options );
    void setFileTypeAction( const std::string &file_type_action,
                            uint32_t options );
    void setViewActiveFile( uint32_t options );

    bool pathsChanged( const std::set< std::string > changed_paths ) override;
private:
    static const char *type;
    void setName();
    TextView *m_tv;
    RefCount<TextStorageFile> m_ts_file;
    std::shared_ptr<TextStorageString> m_ts_str;
    RefCount<AWidth> m_lencalc;

    enum source_mode m_source_mode = ACTIVE_FILE_OTHER_SIDE;
    uint32_t m_options = 0;
    std::string m_command_str_unparsed;
    std::string m_command_str_parsed;
    std::string m_command_base_dir;
    std::string m_file_type_action;
  
    FileEntry *m_lastactivefe;
  
    void update( bool force );
  
    void clearLastActiveFE();

    void showFile();

    void setFilePercentage();

    void processCommand();
    void processFileTypeAction();

    void on( bool keep_state );

    void setInfoLine( const std::string &info,
                      const std::string &file_info );

    AContainer *m_cont;
    AContainer *m_ac1;
    Text *m_file_name_text;
    Text *m_file_percentage_text;
    Text *m_info_text;

    struct filewatch {
        std::string watched_path;
        bool path_watch_enabled = false;
    } m_filewatch_data;

    uint32_t m_source_mode_change_count = 0;
    bool m_skip_info_update = false;
    bool m_start_over = false;
};

#endif
