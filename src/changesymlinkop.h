/* changesymlinkop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHANGESYMLINKOP_H
#define CHANGESYMLINKOP_H

#include "wdefines.h"
#include "functionproto.h"

class ChangeSymlinkOp:public FunctionProto
{
public:
  ChangeSymlinkOp();
  virtual ~ChangeSymlinkOp();
  ChangeSymlinkOp( const ChangeSymlinkOp &other );
  ChangeSymlinkOp &operator=( const ChangeSymlinkOp &other );

  ChangeSymlinkOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;

  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;

  const char *getDescription() override;
  int configure() override;
  bool save(Datei*) override;

  typedef enum { CHANGESYMLINK_EDIT,
                 CHANGESYMLINK_MAKE_ABSOLUTE,
                 CHANGESYMLINK_MAKE_RELATIVE
  } changesymlink_mode_t;

  void setMode( changesymlink_mode_t nv );
protected:
  static const char *name;
  // Infos to save
  changesymlink_mode_t m_mode;

  // temp variables
  Lister *startlister;
  changesymlink_mode_t tmode;

  int normalmodechangesl( ActionMessage* );
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
