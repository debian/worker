/* wconfig_initial_command.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_INITIAL_COMMAND_HH
#define WCONFIG_INITIAL_COMMAND_HH

#include "wdefines.h"
#include "worker_types.h"
#include <string>

class WConfigInitialCommand
{
public:
    enum command_type {
          NO_OP,
          OPEN_NEW_FILE_TYPE,
          SELECT_FILE_TYPE,
    };

    WConfigInitialCommand()
    {}

    WConfigInitialCommand( command_type type ) :
        m_type( type )
    {}

    command_type type() const
    {
        return m_type;
    }

    void setUserData( const std::string &str )
    {
        m_user_data_str = str;
    }

    std::string getUserData() const
    {
        return m_user_data_str;
    }
private:
    command_type m_type = NO_OP;
    std::string m_user_data_str;
};

#endif /* WCONFIG_INITIAL_COMMAND_HH */
