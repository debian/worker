/* fileentry_typecheck.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILEENTRY_TYPECHECK_HH
#define FILEENTRY_TYPECHECK_HH

#include "wdefines.h"
#include <list>
#include <string>

class WCFiletype;

class FileEntryTypeCheck
{
public:
    FileEntryTypeCheck();
    ~FileEntryTypeCheck();
    FileEntryTypeCheck( const FileEntryTypeCheck &other );
    FileEntryTypeCheck &operator=( const FileEntryTypeCheck &other );

    static WCFiletype* checkFiletype( class FileEntry &fe, class List *ftlist, bool skipcontent, class CondParser *condparser = NULL );
private:
    typedef struct _typecandidate_t {
        _typecandidate_t()
        {
            accuracy = 0;
            tests_done = 0;
            type = NULL;
            priority = 0;
        }
        unsigned int accuracy;
        WCFiletype *type;
        int tests_done;
        int priority;
    } typecandidate_t;
    
    typedef struct _typecheck_temp_results_t {
        _typecheck_temp_results_t() : magic_avail{ false, false, false, false } {}
        
        bool magic_avail[4];
        std::string magic_result[4];
    } typecheck_temp_results_t;

    static typecandidate_t runTests( class FileEntry &fe,
                                     WCFiletype *type,
                                     bool skipcontent,
                                     CondParser *condparser,
                                     unsigned char fbuffer[64],
                                     int reads,
                                     typecheck_temp_results_t &temp_results );
    static typecandidate_t checkFiletype( class FileEntry &fe, 
                                          const std::list<WCFiletype*> *ftlist,
                                          bool skipcontent,
                                          CondParser *condparser,
                                          unsigned char fbuffer[64],
                                          int reads,
                                          typecheck_temp_results_t &temp_results );
};

#endif
