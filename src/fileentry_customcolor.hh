/* fileentry_customcolor.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILEENTRY_CUSTOMCOLOR_HH
#define FILEENTRY_CUSTOMCOLOR_HH

#include "wdefines.h"

class FileEntryCustomColor
{
public:
    FileEntryCustomColor();

    enum {
        NORMAL = 1,
        SELECT = 2,
        ACTIVE = 4,
        SELACT = 8
    };
    static const int ALL_COLORS = NORMAL + SELECT + ACTIVE + SELACT;

    int getColorSet() const;
    void setColorSet( int v );

    void setColor( int type, int fg, int bg );
    int getColor( int type, int *fg_return, int *bg_return ) const;
    void setColors( const int fg[4], const int bg[4] );
    const int *getColors( bool bg ) const;
private:
    int m_colors_fg[4];
    int m_colors_bg[4];
    int m_color_set;
};

#endif
