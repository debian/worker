/* wconfig_mainui.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2006 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_MAINUI_HH
#define WCONFIG_MAINUI_HH

#include "wdefines.h"
#include "wconfig_panel.hh"

class WConfig;

class MainUIPanel : public WConfigPanel
{
public:
  MainUIPanel( AWindow &basewin, WConfig &baseconfig );
  ~MainUIPanel();
  int create();
  int saveValues();
protected:
};
 
#endif
