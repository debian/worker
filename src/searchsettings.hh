/* searchsettings.hh
 * This file belongs to Worker, a file manager for UNIX/X11.
 * Copyright (C) 2006-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SEARCHSETTINGS_HH
#define SEARCHSETTINGS_HH

#include "wdefines.h"
#include <string>

class SearchSettings
{
public:
    SearchSettings();
    SearchSettings( const std::string &basedir,
                    const std::string &matchname,
                    const std::string &matchname_separator,
                    bool matchname_re,
                    bool matchname_fullpath,
                    bool matchname_case,
                    const std::string &matchcontent,
                    bool matchcontent_case,
                    bool follow_symlinks,
                    bool search_vfs,
                    bool search_same_fs,
                    const std::string &exlude_label,
                    const std::string &match_expression,
                    bool check_younger,
                    const std::string &younger_than,
                    bool check_older,
                    const std::string &older_than );
    ~SearchSettings();

    std::string getBaseDir() const;
    std::string getMatchName() const;
    std::string getMatchNameSeparator() const;
    bool getMatchNameRE() const;
    bool getMatchNameFullPath() const;
    bool getMatchNameCase() const;
    std::string getMatchContent() const;
    bool getMatchContentCase() const;
    int getActiveRow() const;
    bool getFollowSymlinks() const;
    bool getSearchVFS() const;
    bool getSearchSameFS() const;
    std::string getExcludeLabel() const;
    std::string getMatchExpression() const;
    bool getCheckYounger() const;
    std::string getYoungerThan() const;
    bool getCheckOlder() const;
    std::string getOlderThan() const;
    bool getCheckSizeGEQ() const;
    bool getCheckSizeLEQ() const;
    std::string getSizeGEQ() const;
    std::string getSizeLEQ() const;

    time_t getYoungerThanDate() const;
    time_t getOlderThanDate() const;
    loff_t getSizeGEQAsInt() const;
    loff_t getSizeLEQAsInt() const;

    void setCheckSizeGEQ( bool nv );
    void setCheckSizeLEQ( bool nv );
    void setSizeGEQ( const std::string &size );
    void setSizeLEQ( const std::string &size );

    void setActiveRow( int nv );

    void setMaxVFSDepth( int nv );
    int getMaxVFSDepth() const;

    void setMatchAlsoDirectories( bool nv );
    bool getMatchAlsoDirectories() const;

    const std::vector< std::string > &getSeparatedMatchNames() const;
private:
    std::string _basedir;
    std::string _matchname;
    std::string m_matchname_separator;
    bool _matchname_re;
    bool _matchname_fullpath;
    bool _matchname_case;
    std::string _matchcontent;
    bool _matchcontent_case;
    int _active_row;
    bool _follow_symlinks;
    bool _search_vfs;
    bool _search_same_fs;
    std::string m_exclude_label;
    std::string m_match_expression;
    bool m_check_younger;
    std::string m_younger_than;
    bool m_check_older;
    std::string m_older_than;

    bool m_check_size_geq, m_check_size_leq;
    std::string m_size_geq, m_size_leq;

    // will be calculated
    time_t m_younger_date, m_older_date;
    loff_t m_size_geq_int, m_size_leq_int;

    int m_max_vfs_depth;

    bool m_match_also_directories;

    std::vector< std::string > m_separated_matchnames;

    time_t convertDateString( const std::string &date ) const;
    bool convertDates();

    void create_separated_names();
};

#endif
