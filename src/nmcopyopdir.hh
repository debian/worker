/* nmcopyopdir.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NMCOPYOPDIR_HH
#define NMCOPYOPDIR_HH

#include "wdefines.h"
#include <memory>

class Verzeichnis;
class FileEntry;
class List;

struct deleteorder;

class NM_CopyOp_Dir
{
public:
  NM_CopyOp_Dir(const FileEntry *fe);
  ~NM_CopyOp_Dir();
  NM_CopyOp_Dir( const NM_CopyOp_Dir &other );
  NM_CopyOp_Dir &operator=( const NM_CopyOp_Dir &other );

    int createSubDirs( std::shared_ptr< struct copyorder > co, unsigned long *gf,unsigned long *gd);
  int createSubDirs(struct deleteorder*delorder,unsigned long *gf,unsigned long *gd);
  
  List *subdirs; // contains elements of this class
  Verzeichnis *verz;
  const FileEntry *fileentry;
  unsigned long files,dirs;
  loff_t bytes;
  bool ok; // only use this dir if ok==true
  bool user_abort; // true when user selected cancel from one of
                   // the requesters used inside
  
  unsigned long error_counter; // files/dirs not finished (recursive)
};

#endif
