/* nmexternfe.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "nmexternfe.hh"
#include "aguix/lowlevelfunc.h"
#include "datei.h"
#include "fileentry.hh"

NM_extern_fe::NM_extern_fe( const char *nfn )
{
  fullname[0]=dupstring(nfn);
  fullname[1]=NULL;
  name[0]=NULL;
  name[1]=NULL;
  fe=NULL;
  dirfinished=0;
}

NM_extern_fe::~NM_extern_fe()
{
  if(fullname[0]!=NULL) _freesafe(fullname[0]);
  if(fullname[1]!=NULL) _freesafe(fullname[1]);
  if(name[0]!=NULL) _freesafe(name[0]);
  if(name[1]!=NULL) _freesafe(name[1]);
  if ( fe != NULL ) delete fe;
}

char *NM_extern_fe::getFullname(bool noext)
{
  if(noext==false) return fullname[0];
  else {
    if(fullname[1]==NULL) fullname[1]=Datei::getNameWithoutExt(fullname[0]);
    return fullname[1];
  }
}

char *NM_extern_fe::getName(bool noext)
{
  if(noext==false) {
    if(name[0]==NULL) name[0]=Datei::getFilenameFromPath(fullname[0]);
    return name[0];
  } else {
    if(name[0]==NULL) name[0]=Datei::getFilenameFromPath(fullname[0]);
    if(name[1]==NULL) name[1]=Datei::getNameWithoutExt(name[0]);
    return name[1];
  }
}

std::optional< std::string > NM_extern_fe::getExtension() const
{
    const char *ext = Datei::getExtension( fullname[0] );
    if ( ext ) return ext;
    return {};
}

NM_extern_fe::NM_extern_fe( const char *nfn, const FileEntry *tfe )
{
  fullname[0]=dupstring(nfn);
  fullname[1]=NULL;
  name[0]=NULL;
  name[1]=NULL;
  if ( tfe != NULL ) {
    this->fe = new FileEntry( *tfe );
  } else {
    this->fe = NULL;
  }
  dirfinished=0;
}

void NM_extern_fe::setDirFinished(int newv)
{
  dirfinished=newv;
}

int NM_extern_fe::getDirFinished()
{
  return dirfinished;
}

const FileEntry *NM_extern_fe::getFE()
{
  return fe;
}
