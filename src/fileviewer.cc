/* fileviewer.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "fileviewer.hh"
#include "fileviewerbg.hh"
#include "worker.h"
#include "aguix/textview.h"
#include "textstoragefile.h"
#include "nwc_fsentry.hh"
#include "aguix/util.h"
#include "aguix/refcount.hh"
#include "filenameshrinker.hh"
#include "aguix/searchtextstorage.hh"
#include "aguix/backgroundmessagehandler.hh"

bool FileViewer::global_wrap_mode = false;
bool FileViewer::global_show_line_numbers_mode = false;
bool FileViewer::global_use_alt_font_mode = false;

FileViewerDestroyCallback::~FileViewerDestroyCallback()
{
}

FileViewer::FileViewer( Worker *worker ) : m_use_wrap_mode( global_wrap_mode ),
                                           m_use_show_line_numbers_mode( global_show_line_numbers_mode ),
                                           m_use_alt_font_mode( global_use_alt_font_mode ),
                                           m_worker( worker )
{
}

FileViewer::~FileViewer()
{
}

void FileViewer::view( const std::list<std::string> &filelist,
                       RefCount< FileViewerDestroyCallback > destroy_callback,
                       int initial_line_number,
                       bool highlight_initial_line )
{
    RefCount< FileViewerBG > fvbg( new FileViewerBG( m_worker ) );
    AGUIX *aguix = Worker::getAGUIX();

    fvbg->setLineWrap( m_use_wrap_mode );
    fvbg->setShowLineNumbers( m_use_show_line_numbers_mode );
    fvbg->setUseAltFont( m_use_alt_font_mode );

    fvbg->view( filelist,
		initial_line_number,
		highlight_initial_line,
		destroy_callback );

    if ( fvbg->getAWindow() != NULL ) {
	aguix->registerBGHandler( fvbg->getAWindow(), fvbg );
    }
}

void FileViewer::setGlobalLineWrap( bool nv )
{
    global_wrap_mode = nv;
}

bool FileViewer::getGlobalLineWrap()
{
    return global_wrap_mode;
}

void FileViewer::setGlobalShowLineNumbers( bool nv )
{
    global_show_line_numbers_mode = nv;
}

bool FileViewer::getGlobalShowLineNumbers()
{
    return global_show_line_numbers_mode;
}

void FileViewer::setGlobalUseAltFont( bool nv )
{
    global_use_alt_font_mode = nv;
}

void FileViewer::setLineWrap( bool nv )
{
    m_use_wrap_mode = nv;
}

bool FileViewer::getLineWrap()
{
    return m_use_wrap_mode;
}

void FileViewer::setShowLineNumbers( bool nv )
{
    m_use_show_line_numbers_mode = nv;
}

bool FileViewer::getShowLineNumbers()
{
    return m_use_show_line_numbers_mode;
}
