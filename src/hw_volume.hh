/* hw_volume.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2010 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef HW_VOLUME_HH
#define HW_VOLUME_HH

#include "wdefines.h"
#include <string>

class HWVolume
{
protected:
    friend class HWVolumeManager;
    HWVolume( const std::string &udi );
    void setIsMounted( bool nv );
    void setDevice( const std::string &nv );
    void setMountPoint( const std::string &nv );
    void setIsAvailable( bool nv );
    void setIsInFStab( bool nv );
    void setSupposedMountPoint( const std::string &nv );
    void setLabel( const std::string &nv );
    void setSize( loff_t nv );
public:
    HWVolume();
    ~HWVolume();
    std::string getUDI() const;
    bool isMounted() const;
    std::string getMountPoint() const;
    std::string getDevice() const;
    bool isAvailable() const;
    bool isInFStab() const;
    std::string getSupposedMountPoint() const;
    std::string getLabel() const;
    loff_t getSize() const;

    bool operator==( const HWVolume &rhs ) const;
    bool operator!=( const HWVolume &rhs ) const;
    bool sameDevice( const HWVolume &rhs ) const;
    bool operator<( const HWVolume &rhs ) const;
private:
    std::string m_udi;
    bool m_mounted;
    std::string m_dev;
    std::string m_mp;
    bool m_is_available;
    bool m_is_in_fstab;
    std::string m_supposed_mp;
    std::string m_label;
    loff_t m_size;
};

#endif
