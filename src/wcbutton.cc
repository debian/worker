/* wcbutton.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wcbutton.hh"
#include "datei.h"
#include "aguix/util.h"
#include "wcdoubleshortkey.hh"
#include "functionproto.h"
#include "simplelist.hh"
 
WCButton::WCButton()
{
  text=dupstring("");
  fg=1;
  bg=0;
  check=false;
}

WCButton::~WCButton()
{
  if(text!=NULL) _freesafe(text);
}

WCButton *WCButton::duplicate() const
{
  WCButton *tbut=new WCButton();
  tbut->setText(text);
  tbut->setFG(fg);
  tbut->setBG(bg);
  tbut->setComs(com);
  tbut->setCheck(check);
  tbut->setDoubleKeys( dkeys );
  return tbut;
}

void WCButton::setText(const char *ntext)
{
  if(text!=NULL) _freesafe(text);
  if(ntext==NULL) {
    text=dupstring("");
  } else {
    text=dupstring(ntext);
  }
}

void WCButton::setFG(int nfg)
{
  fg=nfg;
}

void WCButton::setBG(int nbg)
{
  bg=nbg;
}

char *WCButton::getText()
{
  return text;
}

int WCButton::getFG() const
{
  return fg;
}

int WCButton::getBG() const
{
  return bg;
}

command_list_t WCButton::getComs()
{
    return com;
}

void WCButton::setComs( const command_list_t &ncoms )
{
    com.clear();
    for ( auto &fp : ncoms ) {
        com.push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

void WCButton::setCheck(bool ncheck)
{
  check=ncheck;
}

bool WCButton::getCheck() const
{
  return check;
}

bool WCButton::save(Datei *fh) const
{
  int id;
  WCDoubleShortkey *dk;

  if ( fh == NULL ) return false;
  fh->configPutPairString( "title", text );
  fh->configPutPairNum( "color", fg, bg );

  fh->configOpenSection( "shortkeys" );
  id = dkeys->initEnum();
  dk = (WCDoubleShortkey*)dkeys->getFirstElement( id );
  while ( dk != NULL ) {
    dk->save( fh );
    dk = (WCDoubleShortkey*)dkeys->getNextElement( id );
  }
  dkeys->closeEnum( id );
  fh->configCloseSection(); //shortkeys

  fh->configOpenSection( "commands" );
  for ( auto &f1 : com ) {
      FunctionProto::presave( fh, f1.get() );
  }
  fh->configCloseSection(); //commands
  return true;
}
