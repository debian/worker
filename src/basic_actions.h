/* basic_actions.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BASIC_ACTIONS_H
#define BASIC_ACTIONS_H

#include "wdefines.h"
#include "functionproto.h"

class DNDAction:public FunctionProto
{
public:
  DNDAction();
  virtual ~DNDAction();
  DNDAction( const DNDAction &other );
  DNDAction &operator=( const DNDAction &other );

  DNDAction *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  virtual const char *getDescription();

  class DNDActionDescr : public ActionDescr
  {
  public:
      command_list_t get_action_list( WCFiletype *ft ) const override;
  };
protected:
  static const char *name;
  void normalmodedndaction( ActionMessage *am );
};

class DoubleClickAction:public FunctionProto
{
public:
  DoubleClickAction();
  virtual ~DoubleClickAction();
  DoubleClickAction( const DoubleClickAction &other );
  DoubleClickAction &operator=( const DoubleClickAction &other );

  DoubleClickAction *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;

  class DoubleClickActionDescr : public ActionDescr
  {
  public:
      command_list_t get_action_list( WCFiletype *ft ) const override;
  };
protected:
  static const char *name;
  void normalmodeddaction( ActionMessage *am );
};

class ShowAction:public FunctionProto
{
public:
  ShowAction();
  virtual ~ShowAction();
  ShowAction( const ShowAction &other );
  ShowAction &operator=( const ShowAction &other );

  ShowAction *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;

  class ShowActionDescr : public ActionDescr
  {
  public:
      command_list_t get_action_list( WCFiletype *ft ) const override;
  };
protected:
  static const char *name;
  void normalmodeshow( ActionMessage *am );
};

class RawShowAction:public FunctionProto
{
public:
  RawShowAction();
  virtual ~RawShowAction();
  RawShowAction( const RawShowAction &other );
  RawShowAction &operator=( const RawShowAction &other );

  RawShowAction *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;

  class RawShowActionDescr : public ActionDescr
  {
  public:
      command_list_t get_action_list( WCFiletype *ft ) const override;
  };
protected:
  static const char *name;
  void normalmoderawshow( ActionMessage *am );
};

class UserAction:public FunctionProto
{
public:
  UserAction();
  virtual ~UserAction();
  UserAction( const UserAction &other );
  UserAction &operator=( const UserAction &other );

  UserAction *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;
  bool save(Datei*) override;
  int configure() override;
  int getNr() const;
  void setNr( int n );

  class UserActionDescr : public ActionDescr
  {
  public:
      UserActionDescr( int id );
      command_list_t get_action_list( WCFiletype *ft ) const override;
  private:
      int m_id;
  };
protected:
  static const char *name;
  int nr;
  void normalmodeuseraction( ActionMessage *am );
};

class ChangeHiddenFlag:public FunctionProto
{
public:
  ChangeHiddenFlag();
  virtual ~ChangeHiddenFlag();
  ChangeHiddenFlag( const ChangeHiddenFlag &other );
  ChangeHiddenFlag &operator=( const ChangeHiddenFlag &other );

  ChangeHiddenFlag *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;
  
  int getMode() const;
  void setMode( int m );
protected:
  static const char *name;
  int mode;
};

#endif
