/* exprfilter_eval.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "exprfilter_eval.hh"
#include "exprfilter_evalatoms.hh"

void ExprFilterEval::pushAtom( std::shared_ptr< ExprFilterEvalAtom > atom )
{
    m_atoms.push_back( atom );
}

ExprFilterEval::result_item ExprFilterEval::popValue()
{
    if ( m_eval_values.empty() ) throw 1;
        
    result_item v = m_eval_values.back();
    m_eval_values.pop_back();

    return v;
}

void ExprFilterEval::pushValue( bool v )
{
    m_eval_values.push_back( { v, {} } );
}

void ExprFilterEval::pushValue( const ExprFilterEval::result_item &v )
{
    m_eval_values.push_back( v );
}

ExprFilterEval::result_item ExprFilterEval::eval( NWCEntrySelectionState &element )
{
    m_eval_values.clear();
    m_error_occured = false;

    size_t skip_atoms = 0;
        
    for ( auto &a : m_atoms ) {
        if ( skip_atoms > 0 ) {
            skip_atoms--;
            continue;
        }

        std::shared_ptr< ExprAtomPseudoSkip > pseudo_atom = std::dynamic_pointer_cast< ExprAtomPseudoSkip >( a );
        if ( pseudo_atom ) {
            // if necessary, this can be turned into a more generic
            // extended atom which eval return value indicates the
            // number of atoms to be skipped.
            result_item v = m_eval_values.back();
            if ( ! v.result ) {
                skip_atoms = pseudo_atom->getSkipAtoms();
            }
        }

        if ( a ) {
            a->eval( element, *this );
        }

        if ( m_error_occured ) break;
    }

    return popValue();
}

void ExprFilterEval::setErrorOccured()
{
    m_error_occured = true;
}

bool ExprFilterEval::getErrorOccured() const
{
    return m_error_occured;
}

void ExprFilterEval::setCheckInterrupt( std::function< bool () > interrupt_cb )
{
    m_interrupt_cb = interrupt_cb;
}

void ExprFilterEval::setFeedbackCallback( std::function< void ( const std::string & ) > feedback_cb )
{
    m_feedback_cb = feedback_cb;
}

bool ExprFilterEval::checkInterrupt()
{
    if ( m_interrupt_cb ) return m_interrupt_cb();
    return false;
}

void ExprFilterEval::reportFeedback( const std::string &feedback )
{
    if ( m_feedback_cb ) m_feedback_cb( feedback );
}

size_t ExprFilterEval::getAtomsLength() const
{
    return m_atoms.size();
}
