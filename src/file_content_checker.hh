/* file_content_checker.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILE_CONTENT_CHECKER_HH
#define FILE_CONTENT_CHECKER_HH

#include "wdefines.h"
#include <string>
#include <functional>
#include "nwc_fsentry.hh"
#include "aguix/compregex.hh"

class PDatei;

class FileContentChecker {
public:
    void setRegex( const std::string &regex, bool case_sensitive, loff_t size_limit );
    int checkContent( const NWC::FSEntry &file,
                      std::function< bool (void) > interrupt_test,
                      std::function< void ( const std::string & ) > feedback_cb,
                      std::function< void ( const NWC::FSEntry &,
                                            int ) > result_cb );
private:
    struct input_buffer {
        char buf[4096];
        int left, pos;
        PDatei *pd;
        bool eof_reached;
        loff_t size_read;
    };

    struct line_buffer {
        char buf[1024];
        int size;
        int used;
    };

    int matchContentLine( const char *line );
    void fillLineBuffer( struct input_buffer &inbuf, struct line_buffer &linebuf, bool &newline_found );
    std::string m_regex;
    bool m_case_sensitive = false;
    CompRegEx re;
    loff_t m_size_limit = 0;
};

#endif /* FILE_CONTENT_CHECKER_HH */
