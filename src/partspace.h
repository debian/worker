/* partspace.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2006,2008,2010 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PARTSPACE_H
#define PARTSPACE_H

#include "wdefines.h"
#include "aguix/mutex.h"
#include "aguix/condvar.h"
#include <list>
#include <map>
#include <string>

class PartSpace
{
public:
    PartSpace();
    ~PartSpace();
    PartSpace( const PartSpace &other );
    PartSpace &operator=( const PartSpace &other );

    int readSpace( const char *name );
  
    loff_t getBlocksize();
    loff_t getFreeSpace();
    loff_t getSpace();
  
    std::string getFreeSpaceH() const;
    std::string getSpaceH() const;

    void slavehandler();
  
    void setLifetime( double t );
private:
    loff_t blocksize;
    loff_t freespace;
    loff_t space;
  
    double m_info_lifetime;
    int m_maxsize;
  
    struct {
        int running;
        bool stop;
        pthread_t th;
    
        CondVar waitvar;
    } m_slave;

#ifdef HAVE_STATVFS
    int getSpace( const char *name, worker_struct_statvfs *statbuf );
#elif defined( HAVE_STATFS )
    int getSpace( const char *name, struct statfs *statbuf );
#else
    int getSpace( const char *name, int *statbuf );
#endif

    struct SpaceData
    {
        SpaceData() : m_stats_valid( INVALID ),
                      m_last_update( 0 )
        {}
        
#ifdef HAVE_STATVFS
        worker_struct_statvfs m_fsstatistic;
#elif defined( HAVE_STATFS )
        struct statfs m_fsstatistic;
#else
        int m_fsstatistic;
#endif
        typedef enum {
            INVALID,
            PENDING_INVALID,
            PENDING_VALID,
            VALID,
            UNAVAIL
        } stat_valid_t;

        stat_valid_t m_stats_valid;
        
        time_t m_last_update;
    };
    MutEx m_data_lock;
    std::map< dev_t, SpaceData > m_space_data;
    std::list< std::pair< dev_t, std::string > > m_requests;
    CondVar m_requests_lock;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
