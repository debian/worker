/* searchentryop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "searchentryop.h"
#include "listermode.h"
#include "worker.h"
#include "worker_locale.h"
#include "datei.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "aguix/stringgadget.h"
#include "virtualdirmode.hh"

const char *SearchEntryOp::name="SearchEntryOp";

SearchEntryOp::SearchEntryOp() : FunctionProto()
{
    ignore_case = true;
    reverse_search = false;
    m_infix_search = false;
    hasConfigure = true;
    m_category = FunctionProto::CAT_CURSOR;
    m_flexible_match = true;
}

SearchEntryOp::~SearchEntryOp()
{
}

SearchEntryOp*
SearchEntryOp::duplicate() const
{
  SearchEntryOp *ta=new SearchEntryOp();
  ta->ignore_case = ignore_case;
  ta->reverse_search = reverse_search;
  ta->m_infix_search = m_infix_search;
  ta->m_flexible_match = m_flexible_match;
  ta->m_start_search_string = m_start_search_string;
  ta->m_apply_only = m_apply_only;
  return ta;
}

bool
SearchEntryOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
SearchEntryOp::getName()
{
  return name;
}

int
SearchEntryOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if ( dynamic_cast< VirtualDirMode * >( lm1 ) ) {
              vdm_searchentry();
          }
      }
    }
  }
  return 0;
}

const char *
SearchEntryOp::getDescription()
{
  return catalog.getLocale(1274);
}

int SearchEntryOp::vdm_searchentry()
{
    ListerMode *lm1 = NULL;
  
    if ( startlister == NULL ) return 1;

    lm1 = startlister->getActiveMode();
    if ( lm1 == NULL ) return 1;

    if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
        vdm->setFlexibleMatchingMode( m_flexible_match );
        vdm->searchentry( ignore_case, reverse_search, m_infix_search,
                          m_start_search_string,
                          m_apply_only );
    }
    return 0;
}

bool SearchEntryOp::save( Datei *fh )
{
  if ( fh == NULL ) return false;
  fh->configPutPairBool( "ignorecase", ignore_case );
  fh->configPutPairBool( "reversesearch", reverse_search );

  if ( m_infix_search == true ) {
      fh->configPutPairBool( "infixsearch", m_infix_search );
  }

  if ( m_flexible_match == false ) {
      fh->configPutPairBool( "flexiblematch", m_flexible_match );
  }

  if ( ! m_start_search_string.empty() ) {
      fh->configPutPairString( "startsearchstring", m_start_search_string.c_str() );
  }

  if ( m_apply_only == true ) {
      fh->configPutPairBool( "applyonly", m_apply_only );
  }

  return true;
}

void SearchEntryOp::setIgnoreCase( bool nv )
{
  ignore_case = nv;
}

void SearchEntryOp::setReverseSearch( bool nv )
{
  reverse_search = nv;
}

void SearchEntryOp::setInfixSearch( bool nv )
{
    m_infix_search = nv;
}

void SearchEntryOp::setFlexibleMatch( bool nv )
{
    m_flexible_match = nv;
}

void SearchEntryOp::setStartSearchString( const std::string &str )
{
    m_start_search_string = str;
}

void SearchEntryOp::setApplyOnly( bool nv )
{
    m_apply_only = nv;
}

int SearchEntryOp::configure()
{
  ChooseButton *cb, *rscb;
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  char *tstr;
  int ende = 0;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;

  tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) + strlen( getDescription() ) + 1 );
  sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe( tstr );
  
  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 7 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  cb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( ignore_case == true ) ? 1 : 0,
						  catalog.getLocale( 501 ), LABEL_RIGHT, 0 ), 0, 0, cincwnr );

  rscb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( reverse_search == true ) ? 1 : 0,
						    catalog.getLocale( 648 ), LABEL_RIGHT, 0 ), 0, 1, cincwnr );
  
  ChooseButton *iscb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, m_infix_search == true,
                                                                  catalog.getLocale( 862 ), LABEL_RIGHT, 0 ),
                                                0, 2, AContainer::CO_INCWNR );
  
  ChooseButton *fmcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, m_flexible_match == true,
                                                                  catalog.getLocale( 955 ), LABEL_RIGHT, 0 ),
                                                0, 3, AContainer::CO_INCWNR );

  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 4 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( 5 );
  ac1_2->setBorderWidth( 0 );

  ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 1553 ) ), 0, 0, cfix );
  auto *start_search_string_sg = ac1_2->addWidget( new StringGadget( aguix, 0, 0, 100,
                                                    m_start_search_string.c_str(),
                                                    0 ), 1, 0, cincw );

  ChooseButton *apply_only_cb = ac1->addWidget( new ChooseButton( aguix, 0, 0, 20, 20, m_apply_only,
                                                                  catalog.getLocale( 1554 ), LABEL_RIGHT, 0 ),
                                                0, 5, AContainer::CO_INCWNR );

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 6 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( -1 );
  ac1_1->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_1->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cancelb = (Button*)ac1_1->add( new Button( aguix,
						     0,
						     0,
						     catalog.getLocale( 8 ),
						     0 ), 1, 0, cfix );

  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();

  AGMessage *msg;
  while ( ( msg = aguix->GetMessage( NULL ) ) != NULL ) aguix->ReplyMessage( msg );
  while ( ende == 0 ) {
    msg = aguix->WaitMessage( win );
    if ( msg != NULL ) {
      if ( msg->type == AG_CLOSEWINDOW ) {
        if ( msg->closewindow.window == win->getWindow() ) ende = -1;
      } else if ( msg->type == AG_BUTTONCLICKED ) {
        if ( msg->button.button == okb ) ende = 1;
        else if ( msg->button.button == cancelb ) ende = -1;
      } else if ( msg->type == AG_KEYPRESSED ) {
          if ( win->isParent( msg->key.window, false ) == true ) {
              switch ( msg->key.key ) {
                  case XK_Escape:
                      ende = -1;
                      break;
              }
          }
      }
      aguix->ReplyMessage( msg );
    }
  }
  if ( ende == 1 ) {
    ignore_case = cb->getState();
    reverse_search = rscb->getState();
    m_infix_search = iscb->getState();
    m_flexible_match = fmcb->getState();
    m_start_search_string = start_search_string_sg->getText();
    m_apply_only = apply_only_cb->getState();
  }
  delete win;
  return ( ende == 1 ) ? 0 : 1;
}

