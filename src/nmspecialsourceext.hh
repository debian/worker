/* nmspecialsourceext.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2007,2011,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NMSPECIALSOURCEEXT_HH
#define NMSPECIALSOURCEEXT_HH

#include "wdefines.h"

class FileEntry;

/*
 * NM_specialsourceExt always stores a duplicate FE
 */
class NM_specialsourceExt {
public:
    NM_specialsourceExt();
    NM_specialsourceExt( const FileEntry *tfe, int id = -1 );
    NM_specialsourceExt( const NM_specialsourceExt &other );
    NM_specialsourceExt( NM_specialsourceExt &&other );
    ~NM_specialsourceExt();

    NM_specialsourceExt &operator=( const NM_specialsourceExt &rhs );
    NM_specialsourceExt &operator=( NM_specialsourceExt &&rhs );

    const FileEntry *entry() const
    {
        return fe;
    }

    void setID( int id )
    {
        m_id = id;
    }

    int getID() const
    {
        return m_id;
    }
private:
    FileEntry *fe;  // element to copy
    int m_id;
};

#endif
