/* argclass.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "argclass.hh"

ArgClass::~ArgClass()
{
}

std::pair< IntArg, bool > StringArg::convertToInt() const
{
    const char *s1;
    char *s2;

    s1 = m_value.c_str();

    auto l = strtol( s1, &s2, 10 );

    if ( *s1 != '\0' && *s2 == '\0' &&
         l >= INT32_MIN && l <= INT32_MAX ) {
        return std::make_pair( (int)l, true );
    }

    return std::make_pair( 0, false );
}
