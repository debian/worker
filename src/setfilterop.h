/* setfilterop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SETFILTEROP_H
#define SETFILTEROP_H

#include "wdefines.h"
#include "functionproto.h"
#include <string>
#include "dirfiltersettings.hh"
#include <map>
#include "aguix/message.h"

class Worker;
class PopUpMenu;
class SetFilterCB;
class PersistentStringList;

class SetFilterOp:public FunctionProto
{
public:
  SetFilterOp();
  virtual ~SetFilterOp();
  SetFilterOp( const SetFilterOp &other );
  SetFilterOp &operator=( const SetFilterOp &other );

  SetFilterOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;

  int doconfigure(int mode);
  typedef enum {INCLUDE_FILTER=0,EXCLUDE_FILTER,UNSET_FILTER,UNSET_ALL} setfilterop_mode;
  
  void setFilter( const char * );
  void setFiltermode( setfilterop_mode );

  typedef enum {
      SET_OPTION,
      INVERT_OPTION
  } apply_option_t;

  void setOptionMode( apply_option_t v );
  void setBookmarkLabel( const std::string &l );
  void setBookmarkFilter( DirFilterSettings::bookmark_filter_t f );

  void setChangeFilters( bool v );
  void setChangeBookmarks( bool v );
  void setQueryLabel( bool v );

  void popupCallback( AGMessage *msg );

    static std::list< std::string > getHistory();

    bool isInteractiveRun() const;
    void setInteractiveRun();
protected:
  static const char *name;
  // Infos to save
  std::string filter;
  setfilterop_mode filtermode;
  std::string m_bookmark_label;
  DirFilterSettings::bookmark_filter_t m_bookmark_filter;
  apply_option_t m_option_mode;
  bool m_change_filters, m_change_bookmarks;
  bool m_query_label;
  
  // temp variables
  Lister *startlister,*endlister;
  
  // temp values filled when request_flags==true
  std::string tfilter;
  setfilterop_mode tfiltermode;
  std::string m_t_bookmark_label;
  DirFilterSettings::bookmark_filter_t m_t_bookmark_filter;
  apply_option_t m_t_option_mode;
  bool m_t_change_filters, m_t_change_bookmarks;
  bool m_t_query_label;

  int normalmodesf( ActionMessage* );

  void addHistoryItem( const std::string &str );
  static std::unique_ptr< PersistentStringList > m_history;
private:
  static int instance_counter;
  static size_t history_size;
  void initHistory();
  void closeHistory();

  PopUpMenu *m_popup_menu;
  RefCount<SetFilterCB> m_setfilter_cb;
  void openQueryPopUp( ActionMessage *am );
  Worker *m_worker;

  typedef struct _query_popup_t {
      _query_popup_t() : type( DirFilterSettings::SHOW_ALL ),
                         label( "" )
      {}
      _query_popup_t( DirFilterSettings::bookmark_filter_t t,
                      const std::string &l ) : type( t ),
                                               label( l )
      {}
      
      DirFilterSettings::bookmark_filter_t type;
      std::string label;
  } query_popup_t;
  
  std::map<int, query_popup_t> m_label_id_to_action;
  void cleanUpPopUp();
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
