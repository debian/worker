/* copybgmessage.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COPYBGMESSAGE_HH
#define COPYBGMESSAGE_HH

#include "wdefines.h"
#include <string>
#include "copycore.hh"

class CopyState;

class CopyBGMessage
{
public:
    CopyBGMessage( int copy_state_id,
                   loff_t size, int id, bool is_dir )
        : m_type( PRE_MESSAGE ),
          m_copy_state_id( copy_state_id ),
          m_size( size ),
          m_id( id ),
          m_is_dir( is_dir ),
          m_res( CopyCore::NM_COPY_OK ),
          m_dir_error_counter( 0 )
    {}
    CopyBGMessage( int copy_state_id,
                   const std::string &source_fullname, int id,
                   CopyCore::nm_copy_t res,
                   bool is_dir, unsigned long dir_error_counter,
                   const std::string &target_fullname )
        : m_type( POST_MESSAGE),
          m_copy_state_id( copy_state_id ),
          m_size( 0 ),
          m_id( id ),
          m_is_dir( is_dir ),
          m_source_fullname( source_fullname ),
          m_res( res ),
          m_dir_error_counter( dir_error_counter ),
          m_target_fullname( target_fullname )
    {}
    ~CopyBGMessage() {}

    typedef enum {
        PRE_MESSAGE,
        POST_MESSAGE
    } bg_message_t;

    bg_message_t m_type;
    int m_copy_state_id;
    loff_t m_size;
    int m_id;
    bool m_is_dir;
    std::string m_source_fullname;
    CopyCore::nm_copy_t m_res;
    unsigned long m_dir_error_counter;
    std::string m_target_fullname;
};

#endif
