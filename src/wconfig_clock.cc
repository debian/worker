/* wconfig_clock.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_clock.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "aguix/cyclebutton.h"
#include "aguix/stringgadget.h"
#include "aguix/choosebutton.h"

ClockPanel::ClockPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

ClockPanel::~ClockPanel()
{
}

int ClockPanel::create()
{
  char *tstr;
  
  Panel::create();

  AContainer *ac1 = setContainer( new AContainer( this, 1, 5 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  addMultiLineText( catalog.getLocale( 689 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );
  
  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 369 ) ),
              0, 0, AContainer::CO_FIX );
  
  cyb = (CycleButton*)ac1_1->add( new CycleButton( _aguix, 0, 0, 100, 0 ),
                                1, 0, AContainer::CO_INCW );
  cyb->connect( this );
  cyb->addOption( catalog.getLocale( 370 ) );
  cyb->addOption( catalog.getLocale( 371 ) );
  cyb->addOption( catalog.getLocale( 372 ) );
  cyb->addOption( catalog.getLocale( 373 ) );
  cyb->resize( cyb->getMaxSize(), cyb->getHeight() );
  switch( _baseconfig.getClockbarMode() ) {
    case WConfig::CLOCKBAR_MODE_TIME:
      cyb->setOption( 1 );
      break;
    case WConfig::CLOCKBAR_MODE_VERSION:
      cyb->setOption( 2 );
      break;
    case WConfig::CLOCKBAR_MODE_EXTERN:
      cyb->setOption( 3 );
      break;
    default:
      cyb->setOption( 0 );
      break;
  }
  ac1_1->readLimits();
  
  cuwin = new AWindow( _aguix, 0, 0, 10, 10, "" );
  ac1->add( cuwin, 0, 2, AContainer::CO_INCW );
  cuwin->create();

  AContainer *ac1_cuwin = cuwin->setContainer( new AContainer( cuwin, 2, 1 ), true );
  ac1_cuwin->setBorderWidth( 0 );
  ac1_cuwin->setMinSpace( 5 );
  ac1_cuwin->setMaxSpace( 5 );

  ac1_cuwin->add( new Text( _aguix, 0, 0, catalog.getLocale(129) ), 0, 0, AContainer::CO_FIX );

  tstr = (char*)_allocsafe( A_BYTESFORNUMBER( int ) );
  sprintf( tstr, "%d", _baseconfig.getClockbarUpdatetime() );
  cusg = (StringGadget*)ac1_cuwin->add( new StringGadget( _aguix, 0, 0, 200, tstr, 0 ), 1, 0, AContainer::CO_INCW );
  _freesafe( tstr );
  cusg->connect( this );
  cuwin->contMaximize( true );
  cuwin->show();
  
  ccwin = new AWindow( _aguix, 0, 0, 10, 10, "" );
  ac1->add( ccwin, 0, 3, AContainer::CO_INCW );
  ccwin->create();

  AContainer *ac1_ccwin = ccwin->setContainer( new AContainer( ccwin, 2, 1 ), true );
  ac1_ccwin->setBorderWidth( 0 );
  ac1_ccwin->setMinSpace( 5 );
  ac1_ccwin->setMaxSpace( 5 );

  ac1_ccwin->add( new Text( _aguix, 0, 0, catalog.getLocale(374) ), 0, 0, AContainer::CO_FIX );
  ccsg = (StringGadget*)ac1_ccwin->add( new StringGadget( _aguix, 0, 0, 200, _baseconfig.getClockbarCommand(), 0 ), 1, 0, AContainer::CO_INCW );
  ccsg->connect( this );

  ccwin->contMaximize( true );
  ccwin->show();

  ac1->readLimits();

  m_hint_cb = (ChooseButton*)ac1->add( new ChooseButton( _aguix, 0, 0,
                                                         _baseconfig.getClockbarHints(),
                                                         catalog.getLocale( 987 ), LABEL_RIGHT, 0 ),
                                       0, 4, AContainer::CO_INCWNR );

  // now check which extra fields are visible
  if( _baseconfig.getClockbarMode() != WConfig::CLOCKBAR_MODE_EXTERN ) {
    ccwin->hide();
  }
  switch( _baseconfig.getClockbarMode() ) {
    case WConfig::CLOCKBAR_MODE_VERSION:
      cuwin->hide();
      break;
    case WConfig::CLOCKBAR_MODE_TIME:
    case WConfig::CLOCKBAR_MODE_EXTERN:
    default:
      break;
  }

  contMaximize( true );
  return 0;
}

int ClockPanel::saveValues()
{
  switch(cyb->getSelectedOption()) {
    case 1:
      _baseconfig.setClockbarMode( WConfig::CLOCKBAR_MODE_TIME );
      break;
    case 2:
      _baseconfig.setClockbarMode( WConfig::CLOCKBAR_MODE_VERSION );
      break;
    case 3:
      _baseconfig.setClockbarMode( WConfig::CLOCKBAR_MODE_EXTERN );
      break;
    default:
      _baseconfig.setClockbarMode( WConfig::CLOCKBAR_MODE_TIMERAM );
      break;
  }
  _baseconfig.setClockbarUpdatetime( atoi( cusg->getText() ) );
  _baseconfig.setClockbarCommand( ccsg->getText() );
  _baseconfig.setClockbarHints( m_hint_cb->getState() );
  return 0;
}

void ClockPanel::run( Widget *elem, const AGMessage &msg )
{
  if ( msg.type == AG_CYCLEBUTTONCLICKED ) {
    if ( msg.cyclebutton.cyclebutton == cyb ) {
      if ( msg.cyclebutton.option == 3 ) ccwin->show();
      else ccwin->hide();
      switch ( msg.cyclebutton.option ) {
        case WConfig::CLOCKBAR_MODE_VERSION:
          cuwin->hide();
          break;
        case WConfig::CLOCKBAR_MODE_TIME:
        case WConfig::CLOCKBAR_MODE_EXTERN:
        default:
          cuwin->show();
          break;
      }
    }
  } else if ( msg.type == AG_STRINGGADGET_DEACTIVATE ) {
    if ( msg.stringgadget.sg == cusg ) {
      int val = atoi( cusg->getText() );
      if ( val < 1 ) cusg->setText( "1" );
    }
  }
}
