/* datei.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DATEI_H
#define DATEI_H

#include "wdefines.h"
#include <string>

class AWidth;

class CopyfileProgressCallback
{
public:
    virtual ~CopyfileProgressCallback() = 0;

    typedef enum {
        COPYFILE_CONTINUE,
        COPYFILE_ABORT
    } callback_return_t;

    virtual callback_return_t progress_callback( loff_t current_bytes,
                                                 loff_t total_bytes ) = 0;
};

class Datei
{
public:
  Datei();
  ~Datei();
  Datei( const Datei &other );
  Datei &operator=( const Datei &other );

  int open( const char *name, const char *mode );
  int close();
  int putUChar(unsigned char value);
  int putUShort(unsigned short int value);
  int putInt(int);
  int putULong(unsigned long value);
  int putString( const char *str1 );
  unsigned char getUChar();
  int getCharAsInt();
  unsigned short int getUShort();
  int getInt();
  unsigned long getULong();
  char *getString(int count);
  int getString(char *,int count);
  void seek(long offset,int mode);
  int overreadChunk();
  long errors();
  int putStringExt(const char *format,const char *str);
  int putLine( const char *str );
  char *getLine();
  bool isEOF();
  
  static int getIntSize();
  static int getUCharSize();
  static int getUShortSize();
  static int getULongSize();
  static bool fileExists(const char*);
  typedef enum {D_FE_NOFILE,D_FE_FILE,D_FE_DIR,D_FE_LINK} d_fe_t;
  static d_fe_t fileExistsExt(const char*);
  static d_fe_t lfileExistsExt(const char*);
  static char *getFilenameFromPath(const char*);
  static char *getNameWithoutExt(const char*);
    static const char *getExtension( const char *filename );
    static char *createTMPName( const char *infix = NULL,
                                const char *suffix = NULL );
  static std::string createTMPHardlink( const std::string &name );
  static loff_t fileSize( const char * );
  static char *getRelativePath( const char *source, const char *destdir, bool use_realpath );

    /**
     * the same as getRelativePath, but it will test both source and
     * realpath(source) and return the shorter relative path.
     */
    static char *getRelativePathExt( const char *source,
                                     const char *destdir,
                                     bool use_realpath );

    static int copyfile( const char *dest, const char *source,
                         CopyfileProgressCallback *progress_callback = NULL,
                         bool follow_symlinks = false );

  void configPutPair( const char *id, const char *info );
  void configPutPairNum( const char *id, int num );
  void configPutPairNum( const char *id, int num1, int num2 );
  void configPutPairString( const char *id, const char *str, bool escape_newlines = false);
  void configPutPairBool( const char *id, bool val );
  void configPutInfo( const char *str, bool semicolon );
  void configPutString( const char *str, bool semicolon, bool escape_newlines = false );
  void configOpenSection( const char *sectioname );
  void configCloseSection();

  ssize_t read( char *buffer, size_t len );
private:
  int fd;
  long error;
  int putCharacter( int c );
  int getCharacter();
  bool iseof;
  
  int sectiondepth;
};

int getOwnerStringLen(uid_t,gid_t);
char* getOwnerString(uid_t,gid_t);

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
