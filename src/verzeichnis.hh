/* verzeichnis.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef VERZEICHNIS_HH
#define VERZEICHNIS_HH

#include "wdefines.h"
#include <vector>
#include <string>

class FileEntry;

enum { SORT_NAME = 0,
       SORT_SIZE,
       SORT_ACCTIME,
       SORT_MODTIME,
       SORT_CHGTIME,
       SORT_TYPE,
       SORT_OWNER,
       SORT_INODE,
       SORT_NLINK,
       SORT_PERMISSION,
       SORT_STRICT_NAME,
       SORT_EXTENSION,
       SORT_CUSTOM_ATTRIBUTE };

/* for checking valid sortmode */
#define ISVALID_SORTMODE(sortmode) ( ( ( sortmode & 0xff ) >= SORT_NAME ) && ( ( sortmode & 0xff ) <= SORT_CUSTOM_ATTRIBUTE ) )

/* high bits used for some flags ("or" them with sort mode)*/
#define SORT_REVERSE 256
enum {SORT_DIRLAST=512,SORT_DIRMIXED=1024};

class Verzeichnis
{
public:
    Verzeichnis();
    ~Verzeichnis();
    Verzeichnis( const Verzeichnis &other );
    Verzeichnis &operator=( const Verzeichnis &other );

    int readDir( const char * );
    void closeDir();
    int sort(int);
    int sort(int,bool force);
    int getSize();
    const char *getDir();

    int getUseSize();
    static int sortfunction(void*,void*,int);

    FileEntry *getEntryByID( int ID );
    void removeElement( const FileEntry *fe );
    int getSerialOfIDs();
    static int getInvalidSerial();

    typedef std::vector<FileEntry*>::iterator verz_it;
    verz_it begin();
    verz_it end();
    bool dirOpened() const;
protected:

    std::vector<FileEntry*> _files;
    bool _dir_opened;

    std::string _dir_name;
    
    std::vector<FileEntry*> _id2fe;
    void buildID2FE();
    void createIDs();

    int _highest_ID_plus_one;
    int _id_serial;
};

#endif

