/* createsymlinkop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "createsymlinkop.h"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "datei.h"
#include "dnd.h"
#include "worker_locale.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "createsymlinkorder.hh"
#include "virtualdirmode.hh"

const char *CreateSymlinkOp::name="CreateSymlinkOp";

CreateSymlinkOp::CreateSymlinkOp() : FunctionProto()
{
  same_dir=false;
  local=false;
  hasConfigure = true;
    m_category = FunctionProto::CAT_FILEOPS;
    setRequestParameters( false );
}

CreateSymlinkOp::~CreateSymlinkOp()
{
}

CreateSymlinkOp*
CreateSymlinkOp::duplicate() const
{
  CreateSymlinkOp *ta=new CreateSymlinkOp();
  ta->same_dir=same_dir;
  ta->local=local;
  ta->setRequestParameters( requestParameters() );
  return ta;
}

bool
CreateSymlinkOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
CreateSymlinkOp::getName()
{
  return name;
}

int
CreateSymlinkOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      endlister = msg->getWorker()->getOtherLister(startlister);
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if(dynamic_cast< VirtualDirMode *>( lm1 ) ) {
              normalmodecreatesl( msg );
          } else {
              lm1->not_supported();
          }
      }
    }
  } else normalmodecreatesl( msg );
  return 0;
}

bool
CreateSymlinkOp::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  fh->configPutPairBool( "samedir", same_dir );
  fh->configPutPairBool( "relative", local );
  fh->configPutPairBool( "requestflags", requestParameters() );
  return true;
}

const char *
CreateSymlinkOp::getDescription()
{
  return catalog.getLocale(1277);
}

int
CreateSymlinkOp::normalmodecreatesl( ActionMessage *am )
{
    ListerMode *lm1 = NULL, *lm2 = NULL;
    NM_specialsourceExt *specialsource = NULL;
    bool cont = true;
    char *dest = NULL;
    struct NM_createsymlinkorder cslorder;
    bool takesamedir;
  
    if ( am->mode == am->AM_MODE_DNDACTION ) {
        lm1 = am->dndmsg->getSourceMode();
        if ( lm1 == NULL ) return 1;

        lm2 = am->dndmsg->getDestMode();
    } else {
        if ( startlister == NULL ) return 1;
        lm1 = startlister->getActiveMode();
        if ( lm1 == NULL ) return 1;

        if ( endlister != NULL ) {
            lm2 = endlister->getActiveMode();
        }
    }

    if ( requestParameters() == true ) {
        if ( doconfigure( 1 ) != 0 ) cont = false;
    } else {
        // set values in t* variables
        tsame_dir = same_dir;
        tlocal = local;
    }
  
    if ( cont == true ) {
        memset( &cslorder, 0, sizeof( cslorder ) );
        // detect what to copy
        if ( am->mode==am->AM_MODE_ONLYACTIVE ) {
            cslorder.source = cslorder.NM_ONLYACTIVE;
        } else if ( am->mode == am->AM_MODE_DNDACTION ) {
            // insert DND-element into list
            cslorder.source = cslorder.NM_SPECIAL;
            cslorder.sources = new std::list<NM_specialsourceExt*>;
            specialsource = new NM_specialsourceExt( am->dndmsg->getFE() );
            cslorder.sources->push_back( specialsource );
        } else if ( am->mode == am->AM_MODE_SPECIAL ) {
            cslorder.source = cslorder.NM_SPECIAL;
            cslorder.sources = new std::list<NM_specialsourceExt*>;
            specialsource = new NM_specialsourceExt( am->getFE() );
            cslorder.sources->push_back( specialsource );
        } else {
            cslorder.source = cslorder.NM_ALLENTRIES;
        }

        takesamedir = true;
        if ( am->mode == am->AM_MODE_DNDACTION ) {
            std::string ddstr = am->dndmsg->getDestDir();
            if ( ! ddstr.empty() ) dest = dupstring( ddstr.c_str() );
            else dest = NULL;

            takesamedir = false;
        } else if ( tsame_dir == false ) {
            if ( lm2 != NULL ) {
                std::string tstr = lm2->getCurrentDirectory();
                if ( ! tstr.empty() ) {
                    dest = dupstring( tstr.c_str() );
                    takesamedir = false;
                }
            }
        }

        if ( takesamedir == true ) {
            std::string tstr = lm1->getCurrentDirectory();
            if ( ! tstr.empty() ) {
                dest = dupstring( tstr.c_str() );
            }
        }
    
        if ( dest != NULL ) {
            cslorder.local = tlocal;
            cslorder.destdir = dest;

            if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
                vdm->createsymlink( &cslorder );
            }
        }

        _freesafe( dest );

        if ( cslorder.source == cslorder.NM_SPECIAL ) {
            if ( specialsource != NULL ) delete specialsource;
            delete cslorder.sources;
        }
    }
    return 0;
}

int
CreateSymlinkOp::configure()
{
  return doconfigure(0);
}

int
CreateSymlinkOp::doconfigure(int mode)
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  ChooseButton *sdcb,*gcb,*rfcb=NULL;
  AGMessage *msg;
  int endmode=-1;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;

  tstr=(char*)_allocsafe(strlen(catalog.getLocale(293))+strlen(getDescription())+1);
  sprintf(tstr,catalog.getLocale(293),getDescription());
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 5 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  ac1->add( new Text( aguix, 0, 0, getDescription() ), 0, 0, cincwnr );

  sdcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( same_dir == true ) ? 1 : 0,
						    catalog.getLocale( 315 ), LABEL_RIGHT, 0 ), 0, 1, cincwnr );

  gcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( local == true ) ? 1 : 0,
						   catalog.getLocale( 533 ), LABEL_RIGHT, 0 ), 0, 2, cincwnr );

  if(mode==0) {
    rfcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( requestParameters() == true ) ? 1 : 0,
						      catalog.getLocale( 294 ), LABEL_RIGHT, 0 ), 0, 3, cincwnr );
  }

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 4 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( -1 );
  ac1_1->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_1->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_1->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );
  
  okb->takeFocus();
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) endmode=0;
          else if(msg->button.button==cb) endmode=1;
          break;
        case AG_KEYPRESSED:
          if(win->isParent(msg->key.window,false)==true) {
            switch(msg->key.key) {
              case XK_1:
                sdcb->setState((sdcb->getState() == true ) ? false : true );
                break;
              case XK_2:
                gcb->setState((gcb->getState() == true ) ? false : true );
                break;
              case XK_Return:
                if ( cb->getHasFocus() == false ) {
                  endmode=0;
                }
                break;
              case XK_Escape:
                endmode=1;
                break;
            }
          }
          break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  
  if(endmode==0) {
    // ok
    if(mode==1) {
      tsame_dir = sdcb->getState();
      tlocal = gcb->getState();
    } else {
      same_dir = sdcb->getState();
      local = gcb->getState();
      setRequestParameters( rfcb->getState() );
    }
  }
  
  delete win;

  return endmode;
}

void CreateSymlinkOp::setSameDir(bool nv)
{
  same_dir=nv;
}

void CreateSymlinkOp::setLocal(bool nv)
{
  local=nv;
}

bool CreateSymlinkOp::isInteractiveRun() const
{
    return true;
}

void CreateSymlinkOp::setInteractiveRun()
{
    setRequestParameters( true );
}
