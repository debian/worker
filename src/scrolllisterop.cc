/* scrolllisterop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "scrolllisterop.h"
#include "listermode.h"
#include "worker.h"
#include "worker_locale.h"
#include "datei.h"
#include "aguix/cyclebutton.h"
#include "aguix/button.h"
#include "virtualdirmode.hh"

const char *ScrollListerOp::name="ScrollListerOp";

ScrollListerOp::ScrollListerOp() : FunctionProto()
{
  dir=SCROLLLISTEROP_LEFT;
  hasConfigure = true;
    m_category = FunctionProto::CAT_CURSOR;
}

ScrollListerOp::~ScrollListerOp()
{
}

ScrollListerOp*
ScrollListerOp::duplicate() const
{
  ScrollListerOp *ta=new ScrollListerOp();
  ta->dir=dir;
  return ta;
}

bool
ScrollListerOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
ScrollListerOp::getName()
{
  return name;
}

int
ScrollListerOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if(dynamic_cast< VirtualDirMode * >( lm1 ) ) {
              normalmodescrolllister( msg );
          } else {
              lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

bool
ScrollListerOp::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  switch(dir) {
    case SCROLLLISTEROP_RIGHT:
      fh->configPutPair( "mode", "right" );
      break;
    default:
      fh->configPutPair( "mode", "left" );
      break;
  }
  return true;
}

const char *
ScrollListerOp::getDescription()
{
  return catalog.getLocale(1276);
}

void
ScrollListerOp::normalmodescrolllister( ActionMessage *am )
{
  ListerMode *lm1 = NULL;
  
  if ( startlister == NULL ) return;
  lm1 = startlister->getActiveMode();
  if ( lm1 == NULL ) return;

  if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
      if ( dir == SCROLLLISTEROP_RIGHT ) {
          vdm->scrollListView( 1 );
      } else {
          vdm->scrollListView( -1 );
      }
  }
}

int
ScrollListerOp::configure()
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  CycleButton *rcyb;
  AGMessage *msg;
  int endmode=-1;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr=(char*)_allocsafe(strlen(catalog.getLocale(293))+strlen(getDescription())+1);
  sprintf(tstr,catalog.getLocale(293),getDescription());
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 2 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  ac1_1->setBorderWidth( 0 );

  ac1_1->add( new Text( aguix, 0, 0, catalog.getLocale( 340 ) ), 0, 0, cfix );
  rcyb = (CycleButton*)ac1_1->add( new CycleButton( aguix, 0, 0, 100, 0 ), 1, 0, cincw );
  rcyb->addOption(catalog.getLocale(341));
  rcyb->addOption(catalog.getLocale(342));
  rcyb->resize(rcyb->getMaxSize(),rcyb->getHeight());
  ac1_1->readLimits();
  switch(dir) {
    case SCROLLLISTEROP_RIGHT:
      rcyb->setOption(1);
      break;
    default:
      rcyb->setOption(0);
      break;
  }
  
  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( -1 );
  ac1_2->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_2->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );
  
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) endmode=0;
          else if(msg->button.button==cb) endmode=1;
          break;
        case AG_KEYPRESSED:
            if ( win->isParent( msg->key.window, false ) == true ) {
                switch ( msg->key.key ) {
                    case XK_Escape:
                        endmode = 1;
                        break;
                }
            }
            break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  
  if(endmode==0) {
    // ok
    switch(rcyb->getSelectedOption()) {
      case 1:
        dir=SCROLLLISTEROP_RIGHT;
        break;
      default:
        dir=SCROLLLISTEROP_LEFT;
        break;
    }
  }
  
  delete win;

  return endmode;
}

void ScrollListerOp::setDir(scrolllister_t nv)
{
  dir=nv;
}

