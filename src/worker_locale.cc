/* worker_locale.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2025 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "worker_locale.h"
#include "wconfig.h"
#include "worker.h"
#include "aguix/utf8.hh"
#include "workerinitialsettings.hh"
#include "nwc_path.hh"
#include "datei.h"

#define CATENTRY 1573
#define CATFLAGENTRY 69

WorkerLocale catalog;

WorkerLocale::WorkerLocale()
{
  int i;

  catalog=new char*[CATENTRY];
  catalogflag=new char*[CATFLAGENTRY];
  for(i=0;i<CATENTRY;i++) {
    catalog[i] = NULL;
  }
  for(i=0;i<CATFLAGENTRY;i++) {
    catalogflag[i] = NULL;
  }
  initBuiltinLang();
}

void WorkerLocale::initBuiltinLang()
{
  freeLanguage();

  catalog[0]=dupstring( "Worker Configuration" );
  catalog[1]=dupstring( "" );
  catalog[2]=dupstring( "" );
  catalog[3]=dupstring( "" );
  catalog[4]=dupstring( "Button settings" );
  catalog[5]=dupstring( "" );
  catalog[6]=dupstring( "File types" );
  catalog[7]=dupstring( "Save" );
  catalog[8]=dupstring( "Cancel" );
  catalog[9]=dupstring( "Select Language:" );
  catalog[10]=dupstring( "english (builtin)" );
  catalog[11]=dupstring( "Okay" );
  catalog[12]=dupstring( "Welcome to Worker" );
  catalog[13]=dupstring( "Bytes" );
  catalog[14]=dupstring( "Number of colors:" );
  catalog[15]=dupstring( "Palette" );
  catalog[16]=dupstring( "File lister settings" );
  catalog[17]=dupstring( "Number of rows:" );
  catalog[18]=dupstring( "Number of columns:" );
  catalog[19]=dupstring( "Left list view" );
  catalog[20]=dupstring( "Right list view" );
  catalog[21]=dupstring( "Horizontal scroller:" );
  catalog[22]=dupstring( "Top:" );
  catalog[23]=dupstring( "Height:" );
  catalog[24]=dupstring( "Vertical scroller:" );
  catalog[25]=dupstring( "Left:" );
  catalog[26]=dupstring( "Width:" );
  catalog[27]=dupstring( "Palette entry:" );
  catalog[28]=dupstring( "Red:" );
  catalog[29]=dupstring( "Green:" );
  catalog[30]=dupstring( "Blue:" );
  catalog[31]=dupstring( "Status bar" );
  catalog[32]=dupstring( "Foreground" );
  catalog[33]=dupstring( "Background" );
  catalog[34]=dupstring( "Selected list view bar" );
  catalog[35]=dupstring( "Unselected list view bar" );
  catalog[36]=dupstring( "unselected directory" );
  catalog[37]=dupstring( "selected directory" );
  catalog[38]=dupstring( "unselected file" );
  catalog[39]=dupstring( "selected file" );
  catalog[40]=dupstring( "Can't save the configuration to %s!|Please check the worker installation!" );
  catalog[41]=dupstring( "Retry" );
  catalog[42]=dupstring( "A button named %s already uses this shortcut!|Click Okay to remove the reference to this shortcut|from the button and use it here instead" );
  catalog[43]=dupstring( "Clock bar" );
  catalog[44]=dupstring( "Requester" );
  catalog[45]=dupstring( "Choose color:" );
  catalog[46]=dupstring( "Left startup path:" );
  catalog[47]=dupstring( "Right startup path:" );
  catalog[48]=dupstring( "Copy button" );
  catalog[49]=dupstring( "Swap button" );
  catalog[50]=dupstring( "Delete button" );
  catalog[51]=dupstring( "left mouse button" );
  catalog[52]=dupstring( "right mouse button" );
  catalog[53]=dupstring( "Choose button to edit or choose action" );
  catalog[54]=dupstring( "Select button to copy (or press ESC to cancel)" );
  catalog[55]=dupstring( "Select destination button (will be overwritten) (or press ESC to cancel)" );
  catalog[56]=dupstring( "Select first button to swap (or press ESC to cancel)" );
  catalog[57]=dupstring( "Select second button to swap (or press ESC to cancel)" );
  catalog[58]=dupstring( "Select button to delete (or press ESC to cancel)" );
  catalog[59]=dupstring( "Edit button" );
  catalog[60]=dupstring( "Button title:" );
  catalog[61]=dupstring( "Choose shortcut" );
  catalog[62]=dupstring( "Delete shortcut" );
  catalog[63]=dupstring( "A path named %s already uses this shortcut!|Click Okay to remove the reference to this shortcut|from the path and use it here instead" );
  catalog[64]=dupstring( "" );
  catalog[65]=dupstring( "A hotkey named %s already uses this shortcut!|Click Okay to remove the reference to this shortcut|from the hotkey and use it here instead" );
  catalog[66]=dupstring( "Please press the key for the shortcut!" );
  catalog[67]=dupstring( "The destination directory %s doesn't exists!" );
  catalog[68]=dupstring( "Bank no.:" );
  catalog[69]=dupstring( "Next bank" );
  catalog[70]=dupstring( "Prev bank" );
  catalog[71]=dupstring( "New bank" );
  catalog[72]=dupstring( "Delete bank" );
  catalog[73]=dupstring( "Copy path" );
  catalog[74]=dupstring( "Swap path" );
  catalog[75]=dupstring( "Delete path" );
  catalog[76]=dupstring( "Choose path to edit or one of the action buttons" );
  catalog[77]=dupstring( "Select path to copy (or press ESC to cancel)" );
  catalog[78]=dupstring( "Select destination path (will be overwritten) (or press ESC to cancel)" );
  catalog[79]=dupstring( "Select first path to swap (or press ESC to cancel)" );
  catalog[80]=dupstring( "Select second path to swap (or press ESC to cancel)" );
  catalog[81]=dupstring( "Select path to delete (or press ESC to cancel)" );
  catalog[82]=dupstring( "Edit path" );
  catalog[83]=dupstring( "Path title:" );
  catalog[84]=dupstring( "Enter path:" );
  catalog[85]=dupstring( "New type" );
  catalog[86]=dupstring( "Duplicate type" );
  catalog[87]=dupstring( "Delete type" );
  catalog[88]=dupstring( "Edit type" );
  catalog[89]=dupstring( "Name:" );
  catalog[90]=dupstring( "Average rate:" );
  catalog[91]=dupstring( "Hotkeys" );
  catalog[92]=dupstring( "Hotkey name:" );
  catalog[93]=dupstring( "Pattern:" );
  catalog[94]=dupstring( "Use pattern" );
  catalog[95]=dupstring( "File content" );
  catalog[96]=dupstring( "Use file content" );
  catalog[97]=dupstring( "Byte" );
  catalog[98]=dupstring( "Edit byte" );
  catalog[99]=dupstring( "Clear byte" );
  catalog[100]=dupstring( "Automatic creation" );
  catalog[101]=dupstring( "Edit file content" );
  catalog[102]=dupstring( "Enter code or press key" );
  catalog[103]=dupstring( "File" );
  catalog[104]=dupstring( "" );
  catalog[105]=dupstring( "Free RAM" );
  catalog[106]=dupstring( "Free Swap" );
  catalog[107]=dupstring( "Files" );
  catalog[108]=dupstring( "Dirs" );
  catalog[109]=dupstring( "Dir" );
  catalog[110]=dupstring( "Set to default viewer" );
  catalog[111]=dupstring( "" );
  catalog[112]=dupstring( "Choose the mode to toggle:" );
  catalog[113]=dupstring( "Error while copying %s|Source and destination are the same, skipping!" );
  catalog[114]=dupstring( "Quitting Worker" );
  catalog[115]=dupstring( "Do you really wanna quit Worker?" );
  catalog[116]=dupstring( "Copy file %s" );
  catalog[117]=dupstring( "to %s" );
  catalog[118]=dupstring( "files to copy:" );
  catalog[119]=dupstring( "dirs to copy:" );
  catalog[120]=dupstring( "bytes copied" );
  catalog[121]=dupstring( "Remaining time:" );
  catalog[122]=dupstring( "Creating database for copy process" );
  catalog[123]=dupstring( "Worker Request" );
  catalog[124]=dupstring( "Worker Message" );
  catalog[125]=dupstring( "Worker Warning" );
  catalog[126]=dupstring( "Can't create the directory %s|All subfiles and subdirs are ignored" );
  catalog[127]=dupstring( "Can't create the directory %s!" );
  catalog[128]=dupstring( "Show free disk space" );
  catalog[129]=dupstring( "Update time:" );
  catalog[130]=dupstring( "Format of owner output:" );
  catalog[131]=dupstring( "Owner @ group" );
  catalog[132]=dupstring( "Owner.group" );
  catalog[133]=dupstring( "Filter action:" );
  catalog[134]=dupstring( "Terminal program (with %s for file to execute in the terminal, must support multiple arguments):" );
  catalog[135]=dupstring( "Overwrite all" );
  catalog[136]=dupstring( "Overwrite none" );
  catalog[137]=dupstring( "| Target size: %s| Source size: %s|| Target time: %s| Source time: %s" );
  catalog[138]=dupstring( "Failed to remove the file %s|I will skip this file!" );
  catalog[139]=dupstring( "Failed to remove the destination file|The file %s is probably incomplete!" );
  catalog[140]=dupstring( "Creating database for deleting" );
  catalog[141]=dupstring( "Deleting %s" );
  catalog[142]=dupstring( "remaining files:" );
  catalog[143]=dupstring( "remaining dirs:" );
  catalog[144]=dupstring( "" );
  catalog[145]=dupstring( "Do you really want to delete %ld files and %ld dirs recursively?" );
  catalog[146]=dupstring( "The directory %s is not empty|Delete it recursively?" );
  catalog[147]=dupstring( "Can't remove the directory %s!" );
  catalog[148]=dupstring( "Enter new name for %s:" );
  catalog[149]=dupstring( "Rename" );
  catalog[150]=dupstring( "Preserve attributes" );
  catalog[151]=dupstring( "Error while renaming!" );
  catalog[152]=dupstring( "Enter name for directory:" );
  catalog[153]=dupstring( "Delete active entry if there are no other entries selected" );
  catalog[154]=dupstring( "New hotkey" );
  catalog[155]=dupstring( "Duplicate hotkey" );
  catalog[156]=dupstring( "Delete hotkey" );
  catalog[157]=dupstring( "Edit hotkey" );
  catalog[158]=dupstring( "Sort mode" );
  catalog[159]=dupstring( "Filter" );
  catalog[160]=dupstring( "Name sort" );
  catalog[161]=dupstring( "Size sort" );
  catalog[162]=dupstring( "Access time" );
  catalog[163]=dupstring( "Modification time" );
  catalog[164]=dupstring( "Change time" );
  catalog[165]=dupstring( "Reverse sort order" );
  catalog[166]=dupstring( "Listing mode" );
  catalog[167]=dupstring( "New filter" );
  catalog[168]=dupstring( "Edit filter(s)" );
  catalog[169]=dupstring( "Remove selected filter(s)" );
  catalog[170]=dupstring( "include" );
  catalog[171]=dupstring( "exclude" );
  catalog[172]=dupstring( "Edit filter" );
  catalog[173]=dupstring( "directory mode" );
  catalog[174]=dupstring( "show image mode" );
  catalog[175]=dupstring( "information mode" );
  catalog[176]=dupstring( "Name" );
  catalog[177]=dupstring( "Size" );
  catalog[178]=dupstring( "Type" );
  catalog[179]=dupstring( "Destination" );
  catalog[180]=dupstring( "Permissions" );
  catalog[181]=dupstring( "Add dir" );
  catalog[182]=dupstring( "Edit entry" );
  catalog[183]=dupstring( "Remove dir from list" );
  catalog[184]=dupstring( "Owner" );
  catalog[185]=dupstring( "Position" );
  catalog[186]=dupstring( "real position" );
  catalog[187]=dupstring( "Free space" );
  catalog[188]=dupstring( "of" );
  catalog[189]=dupstring( "No information available" );
  catalog[190]=dupstring( "There is already a dir named %s" );
  catalog[191]=dupstring( "Include filter" );
  catalog[192]=dupstring( "Exclude filter" );
  catalog[193]=dupstring( "Unset filter" );
  catalog[194]=dupstring( "Unset all filters" );
  catalog[195]=dupstring( "" );
  catalog[196]=dupstring( "Set to default" );
  catalog[197]=dupstring( "" );
  catalog[198]=dupstring( "Can't open destination file %s!" );
  catalog[199]=dupstring( "Enter directory to ignore for file type check:" );
  catalog[200]=dupstring( "Enter text:" );
  catalog[201]=dupstring( "Press RETURN to continue!" );
  catalog[202]=dupstring( "" );
  catalog[203]=dupstring( "Can't get information about %s!|This file (or directory) will not be visible!" );
  catalog[204]=dupstring( "Font requester" );
  catalog[205]=dupstring( "The symlink %s couldn't be created!" );
  catalog[206]=dupstring( "Enter the destination of the symlink %s:" );
  catalog[207]=dupstring( "Enter the name of the link|which will point to %s:" );
  catalog[208]=dupstring( "About Worker" );
  catalog[209]=dupstring( "" );
  catalog[210]=dupstring( "" );
  catalog[211]=dupstring( "" );
  catalog[212]=dupstring( "Enter destination directory:" );
  catalog[213]=dupstring( "Permissions for %s:" );
  catalog[214]=dupstring( "Owner:" );
  catalog[215]=dupstring( "Group:" );
  catalog[216]=dupstring( "Other:" );
  catalog[217]=dupstring( "Special:" );
  catalog[218]=dupstring( "Read" );
  catalog[219]=dupstring( "Write" );
  catalog[220]=dupstring( "Execute" );
  catalog[221]=dupstring( "Set UID" );
  catalog[222]=dupstring( "Set GID" );
  catalog[223]=dupstring( "Sticky" );
  catalog[224]=dupstring( "Ok to all" );
  catalog[225]=dupstring( "Skip" );
  catalog[226]=dupstring( "Can't change permissions on %s!" );
  catalog[227]=dupstring( "Action mode:" );
  catalog[228]=dupstring( "basic (action is used once)" );
  catalog[229]=dupstring( "extended (action can used until ESC or you click on the action again)" );
  catalog[230]=dupstring( "Enter new name" );
  catalog[231]=dupstring( "" );
  catalog[232]=dupstring( "" );
  catalog[233]=dupstring( "Number of directory buffers:" );
  catalog[234]=dupstring( "" );
  catalog[235]=dupstring( "" );
  catalog[236]=dupstring( "Up" );
  catalog[237]=dupstring( "Down" );
  catalog[238]=dupstring( "Used options" );
  catalog[239]=dupstring( "Remove >>" );
  catalog[240]=dupstring( "<< Add" );
  catalog[241]=dupstring( "Global font" );
  catalog[242]=dupstring( "Font for action and path buttons" );
  catalog[243]=dupstring( "Font for left list view" );
  catalog[244]=dupstring( "Font for right list view" );
  catalog[245]=dupstring( "Commands" );
  catalog[246]=dupstring( "Add command" );
  catalog[247]=dupstring( "Remove command" );
  catalog[248]=dupstring( "Configure" );
  catalog[249]=dupstring( "Choose command" );
  catalog[250]=dupstring( "Actions" );
  catalog[251]=dupstring( "Drag'n'Drop action" );
  catalog[252]=dupstring( "Doubleclick action" );
  catalog[253]=dupstring( "Show action" );
  catalog[254]=dupstring( "RawShow action" );
  catalog[255]=dupstring( "User0 action" );
  catalog[256]=dupstring( "User1 action" );
  catalog[257]=dupstring( "User2 action" );
  catalog[258]=dupstring( "User3 action" );
  catalog[259]=dupstring( "User4 action" );
  catalog[260]=dupstring( "User5 action" );
  catalog[261]=dupstring( "User6 action" );
  catalog[262]=dupstring( "User7 action" );
  catalog[263]=dupstring( "User8 action" );
  catalog[264]=dupstring( "User9 action" );
  catalog[265]=dupstring( "Select file" );
  catalog[266]=dupstring( "Select the file of the file type you want to create:" );
  catalog[267]=dupstring( "Please select three existing files for automatic creation!" );
  catalog[268]=dupstring( "Select entries" );
  catalog[269]=dupstring( "Deselect entries" );
  catalog[270]=dupstring( "Enter the pattern the matching entries will be selected for:" );
  catalog[271]=dupstring( "Enter the pattern the matching entries will be deselected for:" );
  catalog[272]=dupstring( "Want to handle directories instead of files? Enter \"/\" as first character!" );
  catalog[273]=dupstring( "Use this directory" );
  catalog[274]=dupstring( "Delete link" );
  catalog[275]=dupstring( "There is already a symlink named %s, which points to a directory|You can use it so the destination directory is used|or delete the link to create a real directory" );
  catalog[276]=dupstring( "Failed to remove this file.|Please enter new name!" );
  catalog[277]=dupstring( "Delete file" );
  catalog[278]=dupstring( "There is already a link named %s" );
  catalog[279]=dupstring( "There is already a file named %s" );
  catalog[280]=dupstring( "Overwrite" );
  catalog[281]=dupstring( "Failed to open the file %s" );
  catalog[282]=dupstring( "Can't overwrite the file %s.|Shall I try to delete it?" );
  catalog[283]=dupstring( "" );
  catalog[284]=dupstring( "copied files" );
  catalog[285]=dupstring( "copied dirs" );
  catalog[286]=dupstring( "Failed to remove the file %s!" );
  catalog[287]=dupstring( "Delete all" );
  catalog[288]=dupstring( "Delete only empty dirs" );
  catalog[289]=dupstring( "The directory %s is not completely deleted!" );
  catalog[290]=dupstring( "Enter directory you want to cd to:" );
  catalog[291]=dupstring( "Can't remove %s!" );
  catalog[292]=dupstring( "No options available" );
  catalog[293]=dupstring( "Configure \"%s\"" );
  catalog[294]=dupstring( "Request these flags" );
  catalog[295]=dupstring( "do changes on files" );
  catalog[296]=dupstring( "do changes on dirs" );
  catalog[297]=dupstring( "do recursively" );
  catalog[298]=dupstring( "Follow symlinks" );
  catalog[299]=dupstring( "Move" );
  catalog[300]=dupstring( "Rename" );
  catalog[301]=dupstring( "Same directory" );
  catalog[302]=dupstring( "Request destination" );
  catalog[303]=dupstring( "Overwrite mode" );
  catalog[304]=dupstring( "request" );
  catalog[305]=dupstring( "always" );
  catalog[306]=dupstring( "never" );
  catalog[307]=dupstring( "copy mode" );
  catalog[308]=dupstring( "normal" );
  catalog[309]=dupstring( "fast" );
  catalog[310]=dupstring( "" );
  catalog[311]=dupstring( "" );
  catalog[312]=dupstring( "value not allowed|Please change it or leave it blank" );
  catalog[313]=dupstring( "Change" );
  catalog[314]=dupstring( "Blank" );
  catalog[315]=dupstring( "Create symlink in same directory" );
  catalog[316]=dupstring( "" );
  catalog[317]=dupstring( "Action" );
  catalog[318]=dupstring( "enter active entry" );
  catalog[319]=dupstring( "enter active entry on other side" );
  catalog[320]=dupstring( "special directory" );
  catalog[321]=dupstring( "request directory" );
  catalog[322]=dupstring( "special directory:" );
  catalog[323]=dupstring( "Side to edit" );
  catalog[324]=dupstring( "current side" );
  catalog[325]=dupstring( "other side" );
  catalog[326]=dupstring( "left side" );
  catalog[327]=dupstring( "right side" );
  catalog[328]=dupstring( "" );
  catalog[329]=dupstring( "Separate each entry" );
  catalog[330]=dupstring( "Recursive" );
  catalog[331]=dupstring( "Start mode" );
  catalog[332]=dupstring( "normal mode" );
  catalog[333]=dupstring( "in terminal" );
  catalog[334]=dupstring( "in terminal and wait for key" );
  catalog[335]=dupstring( "show output" );
  catalog[336]=dupstring( "program:" );
  catalog[337]=dupstring( "program for viewing output:" );
  catalog[338]=dupstring( "Select flag:" );
  catalog[339]=dupstring( "Side to reload" );
  catalog[340]=dupstring( "Direction" );
  catalog[341]=dupstring( "left" );
  catalog[342]=dupstring( "right" );
  catalog[343]=dupstring( "global program" );
  catalog[344]=dupstring( "Not yet checked" );
  catalog[345]=dupstring( "Unknown file type" );
  catalog[346]=dupstring( "NoSelect type" );
  catalog[347]=dupstring( "Error" );
  catalog[348]=dupstring( "Side:" );
  catalog[349]=dupstring( "Enter default filter:" );
  catalog[350]=dupstring( "Worker quit mode" );
  catalog[351]=dupstring( "request" );
  catalog[352]=dupstring( "quick" );
  catalog[353]=dupstring( "Dir positions:" );
  catalog[354]=dupstring( "Directories first" );
  catalog[355]=dupstring( "Directories last" );
  catalog[356]=dupstring( "Directories mixed" );
  catalog[357]=dupstring( "Show hidden entries" );
  catalog[358]=dupstring( "Don't use" );
  catalog[359]=dupstring( "Error while writing %s !" );
  catalog[360]=dupstring( "toggle Hidden flag" );
  catalog[361]=dupstring( "don't show hidden files" );
  catalog[362]=dupstring( "show hidden files" );
  catalog[363]=dupstring( "" );
  catalog[364]=dupstring( "" );
  catalog[365]=dupstring( "Sorry, there is no such font!" );
  catalog[366]=dupstring( "" );
  catalog[367]=dupstring( "" );
  catalog[368]=dupstring( "Clock bar settings" );
  catalog[369]=dupstring( "Clock bar mode:" );
  catalog[370]=dupstring( "Show current time and free RAM/Swap (on GNU/Linux)" );
  catalog[371]=dupstring( "Show current time" );
  catalog[372]=dupstring( "Show worker version" );
  catalog[373]=dupstring( "Show output of an external program" );
  catalog[374]=dupstring( "program:" );
  catalog[375]=dupstring( "unselected active directory" );
  catalog[376]=dupstring( "selected active directory" );
  catalog[377]=dupstring( "unselected active file" );
  catalog[378]=dupstring( "selected active file" );
  catalog[379]=dupstring( "list view background" );
  catalog[380]=dupstring( "Select font:" );
  catalog[381]=dupstring( "Enter own font (with X name)" );
  catalog[382]=dupstring( "Font example" );
  catalog[383]=dupstring( "X font name:" );
  catalog[384]=dupstring( "" );
  catalog[385]=dupstring( "Reset directory sizes" );
  catalog[386]=dupstring( "Use <DIR> for unknown dir sizes" );
  catalog[387]=dupstring( "Activate shortkey" );
  catalog[388]=dupstring( "Select shortkey:" );
  catalog[389]=dupstring( "Button:" );
  catalog[390]=dupstring( "Path:" );
  catalog[391]=dupstring( "Hotkey:" );
  catalog[392]=dupstring( "Activate" );
  catalog[393]=dupstring( "The destination file %s is incomplete!|Keep this file or delete it?" );
  catalog[394]=dupstring( "Keep" );
  catalog[395]=dupstring( "Delete" );
  catalog[396]=dupstring( "Import/Export" );
  catalog[397]=dupstring( "Import" );
  catalog[398]=dupstring( "Export" );
  catalog[399]=dupstring( "Changes made here only affect the exported configuration|not your real configuration!" );
  catalog[400]=dupstring( "" );
  catalog[401]=dupstring( "configure buttons to export" );
  catalog[402]=dupstring( "configure hotkeys to export" );
  catalog[403]=dupstring( "configure file types to export" );
  catalog[404]=dupstring( "File name:" );
  catalog[405]=dupstring( "" );
  catalog[406]=dupstring( "Please select or enter file to export to:" );
  catalog[407]=dupstring( "Please mark which elements you want to export|and enter the filename!" );
  catalog[408]=dupstring( "%s is a directory, please enter another name" );
  catalog[409]=dupstring( "%s already exists, overwrite?" );
  catalog[410]=dupstring( "Export to %s finished." );
  catalog[411]=dupstring( "Can't open %s for writing, please select another file" );
  catalog[412]=dupstring( "%s isn't a file!" );
  catalog[413]=dupstring( "" );
  catalog[414]=dupstring( "The configuration contains %d button bank(s), %d hotkey(s) and %d file type(s).|You can now change these parts before importing them." );
  catalog[415]=dupstring( "Please enter the file name to import from." );
  catalog[416]=dupstring( "You can change the import configuration before|the selected parts will be imported." );
  catalog[417]=dupstring( "Please mark which parts you want to import." );
  catalog[418]=dupstring( "There are no elements in the selected configuration!" );
  catalog[419]=dupstring( "The new button %s uses the same shortkey as the button %s!" );
  catalog[420]=dupstring( "The new hotkey %s uses the same shortkey as the button %s!" );
  catalog[421]=dupstring( "The new button %s uses the same shortkey as the hotkey %s!" );
  catalog[422]=dupstring( "The new hotkey %s uses the same shortkey as the hotkey %s!" );
  catalog[423]=dupstring( "The new button %s uses the same shortkey as the path %s!" );
  catalog[424]=dupstring( "The new hotkey %s uses the same shortkey as the path %s!" );
  catalog[425]=dupstring( "Leave shortkey at original element" );
  catalog[426]=dupstring( "Use shortkey for new element" );
  catalog[427]=dupstring( "Use original" );
  catalog[428]=dupstring( "Use new" );
  catalog[429]=dupstring( "The import configuration contains the special file type %s|which would overwrite the original type %s!|Which one do you want to use?" );
  catalog[430]=dupstring( "Import successful" );
  catalog[431]=dupstring( "Never use shortkey for new element" );
  catalog[432]=dupstring( "File type sort" );
  catalog[433]=dupstring( "Owner sort" );
  catalog[434]=dupstring( "Keep file types" );
  catalog[435]=dupstring( "Run in background" );
  catalog[436]=dupstring( "Yesterday" );
  catalog[437]=dupstring( "Today" );
  catalog[438]=dupstring( "Tomorrow" );
  catalog[439]=dupstring( "Date/Time settings" );
  catalog[440]=dupstring( "Date:" );
  catalog[441]=dupstring( "Own format (strftime style):" );
  catalog[442]=dupstring( "Name substitution (Today, Yesterday, ...)" );
  catalog[443]=dupstring( "own format" );
  catalog[444]=dupstring( "Time:" );
  catalog[445]=dupstring( "Date before time" );
  catalog[446]=dupstring( "Can't change owner for %s!|Permission denied!" );
  catalog[447]=dupstring( "Error while changing owner for %s!" );
  catalog[448]=dupstring( "Owner for %s:" );
  catalog[449]=dupstring( "Shortkeys:" );
  catalog[450]=dupstring( "Add shortkey" );
  catalog[451]=dupstring( "Remove shortkey" );
  catalog[452]=dupstring( "Add normal or double shortkey?" );
  catalog[453]=dupstring( "Normal" );
  catalog[454]=dupstring( "Double" );
  catalog[455]=dupstring( "Please press the first key for the shortcut!" );
  catalog[456]=dupstring( "Please press the second key for the shortcut!" );
  catalog[457]=dupstring( "This shortkey conflicts with a key used here!" );
  catalog[458]=dupstring( "Find" );
  catalog[459]=dupstring( "Label %s not found for if statement at line %d!|Aborting" );
  catalog[460]=dupstring( "Error in if statement at line %d!|Aborting" );
  catalog[461]=dupstring( "Script command:" );
  catalog[462]=dupstring( "nop" );
  catalog[463]=dupstring( "push" );
  catalog[464]=dupstring( "pop" );
  catalog[465]=dupstring( "label" );
  catalog[466]=dupstring( "if" );
  catalog[467]=dupstring( "end" );
  catalog[468]=dupstring( "Push a string on a stack" );
  catalog[469]=dupstring( "Push the output of the command" );
  catalog[470]=dupstring( "Stack number:" );
  catalog[471]=dupstring( "String:" );
  catalog[472]=dupstring( "Take a string from a stack" );
  catalog[473]=dupstring( "" );
  catalog[474]=dupstring( "Run in debug mode" );
  catalog[475]=dupstring( "Label:" );
  catalog[476]=dupstring( "This does nothing" );
  catalog[477]=dupstring( "This will immediately finish the execution of the command list" );
  catalog[478]=dupstring( "Jump to label when condition is true" );
  catalog[479]=dupstring( "Condition:" );
  catalog[480]=dupstring( "" );
  catalog[481]=dupstring( "Choose flag:|The flags beginning at {Rs} are only allowed|inside \"\" or in ?{}/${}!" );
  catalog[482]=dupstring( "You can choose flags with the requester by clicking at \"F\".|You can use parentheses if necessary, && means both sides have to be true, \\|\\| one of both.|\
use <, <=, ==, >=, >, != as usual; when both sides are strings (\"\" or ${}) they are compared as strings, otherwise with the|\
numerical value. So ( \"5\" > \"06\" ) is true while ( 5 > \"06\" ) is false!" );
  catalog[483]=dupstring( "Help" );
  catalog[484]=dupstring( "if debug: Result of %s is %d" );
  catalog[485]=dupstring( "push debug: pushing %s at stack %d" );
  catalog[486]=dupstring( "settings" );
  catalog[487]=dupstring( "Choose \"Recursive\" to collect all files recursively (for selected dirs)|  This can only be changed as the very first command!|Choose \"Also use dirs...\" if directories should be part of flags too." );
  catalog[488]=dupstring( "change user window" );
  catalog[489]=dupstring( "Here you can change the window to show some information to the user.|You can change the progress bar by any string which can contain flags and|you can also use the output\
 of this string for the progress.|The only limitation is that the resulting string can be transformed to a number between 0 and 100!|You can also change the text shown above the progress\
bar and this string can contain \\||for a new line (as many as you want). Of course this string can contain any valid flags and|you can also use the output instead." );
  catalog[490]=dupstring( "Change window visibility" );
  catalog[491]=dupstring( "Open window" );
  catalog[492]=dupstring( "Close window" );
  catalog[493]=dupstring( "Change progress bar" );
  catalog[494]=dupstring( "Progress:" );
  catalog[495]=dupstring( "Use the output for the progress" );
  catalog[496]=dupstring( "Change text" );
  catalog[497]=dupstring( "Text:" );
  catalog[498]=dupstring( "Use the output for the text" );
  catalog[499]=dupstring( "Worker script window" );
  catalog[500]=dupstring( "Copy dir %s" );
  catalog[501]=dupstring( "Ignore case" );
  catalog[502]=dupstring( "Use regular expression" );
  catalog[503]=dupstring( "Command execution stopped by USR1 signal!" );  // request text
  catalog[504]=dupstring( "" );
  catalog[505]=dupstring( "" );
  catalog[506]=dupstring( "File handled with %s" );
  catalog[507]=dupstring( "File handled as normal file" );
  catalog[508]=dupstring( "Show complete license" );
  catalog[509]=dupstring( "Worker license" );
  catalog[510]=dupstring( "The configuration has been changed!|Shall I reload it?" );
  catalog[511]=dupstring( "Reload" );
  catalog[512]=dupstring( "Don't reload" );
  catalog[513]=dupstring( "Also use dirs for flag replacing (when collecting recursively)" );
  catalog[514]=dupstring( "Label %s not found for goto statement at line %d!|Aborting" );
  catalog[515]=dupstring( "goto" );
  catalog[516]=dupstring( "Can't copy|  %s|to|  %s|because I can't remove the destination!|Error:%s" );
  catalog[517]=dupstring( "" );
  catalog[518]=dupstring( "Can't copy|  %s|to|  %s|Can't get information about source!|Error:%s" );
  catalog[519]=dupstring( "Can't copy|  %s|to|  %s|Can't get symlink destination!|Error:%s" );
  catalog[520]=dupstring( "Can't copy|  %s|because it's a special file and I can't handle it yet!" );
  catalog[521]=dupstring( "Enter the image viewing program.|This program has to accept 7 arguments: the first 4 are: x, y, width height.|The 5th is the hex number of the window.|\
Then the file name follows and the last argument is the background color in the X11 format \"rgb:rr/gg/bb\"." );
  catalog[522]=dupstring( "" );
  catalog[523]=dupstring( "Directory cache list" );
  catalog[524]=dupstring( "Select or enter a directory:" );
  catalog[525]=dupstring( "Error while reading from \"%s\"|Error:%s" );
  catalog[526]=dupstring( "Error while writing to \"%s\"|Error:%s" );
  catalog[527]=dupstring( "octal:" );
  catalog[528]=dupstring( "Move was canceled but some files were correctly copied!|Shall I delete these files from the source?" );
  catalog[529]=dupstring( "" );
  catalog[530]=dupstring( "" );
  catalog[531]=dupstring( "" );
  catalog[532]=dupstring( "Can't read directory|%s|because of this error:|%s" );
  catalog[533]=dupstring( "Create relative symlink" );
  catalog[534]=dupstring( "Select program" );
  catalog[535]=dupstring( "Select on of the shown programs, make sure the corresponding|program is available!" );
  catalog[536]=dupstring( "I didn't restore the SUID/SGUID bit for the file|%s|because chown to old owner failed (probably you are not|the owner or not root)." );
  catalog[537]=dupstring( "Ignore further warnings" );
  catalog[538]=dupstring( "You changed the configuration!|Do you really want to discard your changes?" );
  catalog[539]=dupstring( "Error while reading configuration!|The following error occurred in line %d:|%s|I will continue with the partly read configuration." );
  catalog[540]=dupstring( "configure buttons to import" );
  catalog[541]=dupstring( "configure hotkeys to import" );
  catalog[542]=dupstring( "configure file types to import" );
  catalog[543]=dupstring( "Do you really want to abort?" );
  catalog[544]=dupstring( "Continue with copying" );
  catalog[545]=dupstring( "There is no space left for file %s|in directory %s !" );
  catalog[546]=dupstring( "The external program failed with following error:" );
  catalog[547]=dupstring( "Waiting for program (stop waiting with Escape or middle click)" );
  catalog[548]=dupstring( "Inode" );
  catalog[549]=dupstring( "Hard links" );
  catalog[550]=dupstring( "Blocks" );
  catalog[551]=dupstring( "%s blocks" );
  catalog[552]=dupstring( "Inode sort" );
  catalog[553]=dupstring( "Hard links sort" );
  catalog[554]=dupstring( "Show header line:" );
  catalog[555]=dupstring( "list view header" );
  catalog[556]=dupstring( "State bar" );
  catalog[557]=dupstring( "Clock bar" );
  catalog[558]=dupstring( "Buttons" );
  catalog[559]=dupstring( "List views" );
  catalog[560]=dupstring( "Buttons, list view, list view" );
  catalog[561]=dupstring( "List view, buttons, list view" );
  catalog[562]=dupstring( "List view, list view, buttons" );
  catalog[563]=dupstring( "Buttons, list views" );
  catalog[564]=dupstring( "List views, Buttons" );
  catalog[565]=dupstring( "Select button and list view orientation:" );
  catalog[566]=dupstring( "" );
  catalog[567]=dupstring( "Buttons vertical" );
  catalog[568]=dupstring( "List views vertical" );
  catalog[569]=dupstring( "Layout configuration" );
  catalog[570]=dupstring( "Match full file name" );
  catalog[571]=dupstring( "Swap next" );
  catalog[572]=dupstring( "Swap prev" );
  catalog[573]=dupstring( "extended test:" );
  catalog[574]=dupstring( "use extended test" );
  catalog[575]=dupstring( "Pattern" );
  catalog[576]=dupstring( "Extended test" );
  catalog[577]=dupstring( "Without using regular expressions, you can use the common shell \
wildcards:| * for matching any string| ? for matching a single character|\
 [...] for matching a character class|See the shell manual for more information.||\
You can also use basic regular expressions, some examples follow:| .* for matching any string|\
 \\+ matches one or more occurrences| \\? matches zero or one occurrence|\
 ^ matches the beginning of the line| $ matches the end| \\( and \\) can be used for sub\
 expressions| \\\\\\| matches one of the sub expressions|For more information see the regular \
expression manual (man 7 regex)" );
  catalog[578]=dupstring( "Choose flag:" );
  catalog[579]=dupstring( "true if entry is a regular file" );
  catalog[580]=dupstring( "true if entry is a socket" );
  catalog[581]=dupstring( "true if entry is a FIFO (pipe)" );
  catalog[582]=dupstring( "true if entry is a symlink" );
  catalog[583]=dupstring( "file size" );
  catalog[584]=dupstring( "permission (numerical) bitwise-and'd with mask" );
  catalog[585]=dupstring( "lowers string" );
  catalog[586]=dupstring( "uppers string" );
  catalog[587]=dupstring( "replaces with file content from start to optional end offset" );
  catalog[588]=dupstring( "replaces with file content read as number from start to optional \
end offset" );
  catalog[589]=dupstring( "file name" );
  catalog[590]=dupstring( "full file name" );
  catalog[591]=dupstring( "file name (only valid inside strings)" );
  catalog[592]=dupstring( "full file name (only valid inside strings)" );
  catalog[593]=dupstring( "You can choose flags with the requester by clicking at the \"F\". Start \
the flag identifier|with a \"-\" if you don't want to quote the string (e.g. {-f}). The functions \
\"contentStr\"|and \"contentNum\" can be called without a second argument which will \
use only one character.|You \
can use parentheses if necessary, && means both sides have to be true, \\|\\| one of both.|\
use <, <=, ==, >=, >, != as usual and ~= for string comparison using regular|expressions; \
when both sides are strings (\"\" or ${}) they are compared as strings,|otherwise with the \
numerical value. So ( \"5\" > \"06\" ) is true while ( 5 > \"06\" ) is false!" );
  catalog[594]=dupstring( "Move file %s" );
  catalog[595]=dupstring( "Configuring Worker..." );
  catalog[596]=dupstring( "New child type" );
  catalog[597]=dupstring( "This file type contains child file types!|Really delete this and all child types?" );
  catalog[598]=dupstring( "Cut" );
  catalog[599]=dupstring( "Paste as root type" );
  catalog[600]=dupstring( "Paste as child type" );
  catalog[601]=dupstring( "Select color mode:" );
  catalog[602]=dupstring( "Default colors" );
  catalog[603]=dupstring( "Custom colors" );
  catalog[604]=dupstring( "Colors from external program" );
  catalog[605]=dupstring( "Colors from parent type" );
  catalog[606]=dupstring( "Select custom colors" );
  catalog[607]=dupstring( "unselected" );
  catalog[608]=dupstring( "selected" );
  catalog[609]=dupstring( "unselected, active" );
  catalog[610]=dupstring( "selected, active" );
  catalog[611]=dupstring( "External program:" );
  catalog[612]=dupstring( "Colors" );
  catalog[613]=dupstring( "Enter the program you want to use for the color selection.|\
You can use the usual flags {f} and {F} for the file name and the file name|\
with full path. The program you enter needs to output a single line with|\
8 integer values which describe the colors. The number is the color number|\
from the palette configuration starting with 0. The colors describe in this|\
order: the color for the unselected state (foreground followed by background),|\
the selected color (fg and bg), the color for the active state (fg and bg) and|\
the color while active and selected (fg and bg). As an example, the output for|\
the default colors would be: 1 0 2 1 1 7 2 7" );
  catalog[614]=dupstring( "Add files of same file type:" );
  catalog[615]=dupstring( "Add file" );
  catalog[616]=dupstring( "Remove from list" );
  catalog[617]=dupstring( "Do not enter directory" );
  catalog[618]=dupstring( "Could not create temporary copy of %s!" );
  catalog[619]=dupstring( "Don't check content of virtual files" );
  catalog[620]=dupstring( "Host name:" );
  catalog[621]=dupstring( "User name:" );
  catalog[622]=dupstring( "Password:" );
  catalog[623]=dupstring( "Store password but don't open ftp connection" );
  catalog[624]=dupstring( "The password will be stored without encryption|in the worker configuration file!|Do really want to do this?" );
  catalog[625]=dupstring( "No host name or user name was entered. Cannot continue!" );
  catalog[626]=dupstring( "1 if entry is a local file, 0 for virtual file, -1 for non existing file or error" );
  catalog[627]=dupstring( "Store password even if password was already stored previously" );
  catalog[628]=dupstring( "The temporary copy of %s|has been changed but the original file will not be updated!" );
  catalog[629]=dupstring( "Continue" );
  catalog[630]=dupstring( "You are about to make a ssh connection. You should only continue if you|\
don't need a password for a connection to the destination. Otherwise it|\
will not work at all or you will be asked for the password many times|\
if you have the ssh_askpass tool. Do you really want to continue?" );
  catalog[631]=dupstring( "Don't ask me again" );
  catalog[632]=dupstring( "Show mode:" );
  catalog[633]=dupstring( "Close" );
  catalog[634]=dupstring( "Show active file" );
  catalog[635]=dupstring( "Show custom file" );
  catalog[636]=dupstring( "Custom file:" );
  catalog[637]=dupstring( "Word wrapping" );
  catalog[638]=dupstring( "Settings:" );
  catalog[639]=dupstring( "Enter command:" );
  catalog[640]=dupstring( "Enter archive handler or choose from list:" );
  catalog[641]=dupstring( "Execute command" );
  catalog[642]=dupstring( "Handle like file type" );
  catalog[643]=dupstring( "Handle as archive" );
  catalog[644]=dupstring( "Select file type. Its double click action will be executed on the active file." );
  catalog[645]=dupstring( "Start program or apply different action to the current file:" );
  catalog[646]=dupstring( "The action list of the file type is empty, you can select a different action here:" );
  catalog[647]=dupstring( "Error while writing command file %s!|Can't execute external program." );
  catalog[648]=dupstring( "Search for next entry in reverse order" );
  catalog[649]=dupstring( "Enable quick search" );
  catalog[650]=dupstring( "Mouse button configuration" );
  catalog[651]=dupstring( "Mouse configuration presets:" );
  catalog[652]=dupstring( "worker style" );
  catalog[653]=dupstring( "mc style" );
  catalog[654]=dupstring( "kde style" );
  catalog[655]=dupstring( "custom" );
  catalog[656]=dupstring( "Select button:" );
  catalog[657]=dupstring( "Left mouse button" );
  catalog[658]=dupstring( "Middle mouse button" );
  catalog[659]=dupstring( "Right mouse button" );
  catalog[660]=dupstring( "Activate button:" );
  catalog[661]=dupstring( "Scroll button:" );
  catalog[662]=dupstring( "Select method:" );
  catalog[663]=dupstring( "Normal (click toggles select state)" );
  catalog[664]=dupstring( "Alternative (click selects element and deselects all other)" );
  catalog[665]=dupstring( "" );
  catalog[666]=dupstring( "Do you want to cancel the delete operation?" );
  catalog[667]=dupstring( "Abort deletion" );
  catalog[668]=dupstring( "Continue deletion" );
  catalog[669]=dupstring( "Please select a sub category for the user interface configuration." );
  catalog[670]=dupstring( "Enter the number of different colors you want to use in the user interface." );
  catalog[671]=dupstring( "You can configure the different colors.|Select a color by clicking on it and modify the red, green and blue values." );
  catalog[672]=dupstring( "Configure the colors of different elements of the user interface:" );
  catalog[673]=dupstring( "Configure the fonts by clicking on the corresponding buttons:" );
  catalog[674]=dupstring( "Configure the behaviour of the mouse buttons for the list view element.|You can select one of the predefined styles or configure the buttons individually." );
  catalog[675]=dupstring( "Configure the appearance of the file list views." );
  catalog[676]=dupstring( "Configure the visible columns used in the file list views.|Add or remove the fields and configure the position, the first column is the top position." );
  catalog[677]=dupstring( "Available options" );
  catalog[678]=dupstring( "Configure the appearance of the owner field in the list view." );
  catalog[679]=dupstring( "Worker shows the size of directory as returned by the operation system if the real|size is unknown. Worker can display a string instead by choosing this option." );
  catalog[680]=dupstring( "Configure the appearance of the date and time in the list views." );
  catalog[681]=dupstring( "Enter the initial directories for both list views.|Leave it empty if you don't want to load a directory on startup or enter any path name.|Environment variables like $HOME or . for the current directory can also be used." );
  catalog[682]=dupstring( "Enter the number of rows and columns you want to use for the buttons." );
  catalog[683]=dupstring( "Create new file types or modify existing types.|File types are determined by the file name, the file content or other details.|The file type tests for child types are only checked if the tests of the parent type applies to|the actual file. This way child types need only to test specialised details for a certain file type." );
  catalog[684]=dupstring( "Add all directories to the list in which files should not be checked by content. For example this|can be useful if access to files in this directory would be very slow or generally unwanted." );
  catalog[685]=dupstring( "Hotkeys can be used for commands which are only started by a key press and not by using the mouse.|Basically they are buttons without a graphical representation." );
  catalog[686]=dupstring( "The expert configuration contains advanced options. The inexperienced Worker user|normally don't need to change these options. Select a sub category for more information|about it." );
  catalog[687]=dupstring( "The terminal program entered here is used if external programs should be started|in an own terminal. The default is the \"xterm\" terminal program available in almost every|X11 environment but you can enter your favorite terminal program with its options as long as|it can execute a given program." );
  catalog[688]=dupstring( "Worker keeps already read directories in a cache. Information about the selected|entries and their file type is also stored. You can enter any limit of different directories|to be stored inside the cache but of course a larger cache needs more memory.|If you want to disable this feature completely, set the limit to 1." );
  catalog[689]=dupstring( "The information shown in the clock bar can be configured." );
  catalog[690]=dupstring( "The layout of the Worker main window can be configured. You can select the orientation|of the buttons and list views and select their position from top to bottom." );
  catalog[691]=dupstring( "Parts of the configuration can be exported to a file or imported into the current|configuration. The file to import from can also be a complete Worker configuration file.|Before the actual import/export, you can modify the configuration which does NOT|modify your real configuration.|Note: The exported configuration is a copy of the configuration when the configuration|window was opened, any changes done without saving are not available for export." );
  catalog[692]=dupstring( "Enter the file name to import from or export to:" );
  catalog[693]=dupstring( "Choose file" );
  catalog[694]=dupstring( "Nothing to import" );
  catalog[695]=dupstring( "Please enter the file name to export to." );
  catalog[696]=dupstring( "User interface settings" );
  catalog[697]=dupstring( "Language selection" );
  catalog[698]=dupstring( "Color settings" );
  catalog[699]=dupstring( "User interface colors" );
  catalog[700]=dupstring( "Font settings" );
  catalog[701]=dupstring( "List view configuration" );
  catalog[702]=dupstring( "Column configuration" );
  catalog[703]=dupstring( "Owner column format" );
  catalog[704]=dupstring( "Directory size settings" );
  catalog[705]=dupstring( "Initial directories" );
  catalog[706]=dupstring( "Button configuration" );
  catalog[707]=dupstring( "Path configuration" );
  catalog[708]=dupstring( "Ignored directories" );
  catalog[709]=dupstring( "Expert settings" );
  catalog[710]=dupstring( "Terminal program" );
  catalog[711]=dupstring( "Directory caches" );
  catalog[712]=dupstring( "Add frequently used directories here to quickly access these directories by clicking|on the button or pressing the corresponding key." );
  catalog[713]=dupstring( "Missing charsets:" );
  catalog[714]=dupstring( "For UTF8 texts the font actually consist of several fonts for different character|\
sets. The number of missing charsets shown here is the number of character sets|\
for which no font was found matching the given font name. This means that not all|\
UTF8 characters will be visible. However, this doesn't mean you can't use this|\
font, it's just a problem when you can't see some characters you have to see.|\
You can select a different font (or size) to see whether the number of missing|\
charsets is lower but you can also enter a comma separated list of font names.|\
For example, you can select your favorite font from the list, then click on the|\
button enter an own font name and add \",*\" to the font name which will|\
match any font." );
  catalog[715]=dupstring( "Show line numbers" );
  catalog[716]=dupstring( "Jump to line" );
  catalog[717]=dupstring( "Enter line number:" );
  catalog[718]=dupstring( "Line number" );
  catalog[719]=dupstring( "Command used to edit file:" );
  catalog[720]=dupstring( "Input is valid." );
  catalog[721]=dupstring( "Input is NOT valid!" );
  catalog[722]=dupstring( "Start directory:" );
  catalog[723]=dupstring( "Search results will be used for further searches!" );
  catalog[724]=dupstring( "File name:" );
  catalog[725]=dupstring( "Case sensitive" );
  catalog[726]=dupstring( "Match content:" );
  catalog[727]=dupstring( "Search content of archives too" );
  catalog[728]=dupstring( "Start new search - F5" );
  catalog[729]=dupstring( "File name" );
  catalog[730]=dupstring( "Change directory to entry - F1" );
  catalog[731]=dupstring( "View - F3" );
  catalog[732]=dupstring( "Edit - F4" );
  catalog[733]=dupstring( "Remove selected results from list" );
  catalog[734]=dupstring( "Previous results:" );
  catalog[735]=dupstring( "No. of results" );
  catalog[736]=dupstring( "Content pattern" );
  catalog[737]=dupstring( "Search started at" );
  catalog[738]=dupstring( "Clear store" );
  catalog[739]=dupstring( "State:" );
  catalog[740]=dupstring( "Idle" );
  catalog[741]=dupstring( "Start search" );
  catalog[742]=dupstring( "Stop" );
  catalog[743]=dupstring( "Continue search" );
  catalog[744]=dupstring( "Suspend search" );
  catalog[745]=dupstring( "Suspended" );
  catalog[746]=dupstring( "Show previous results instead of new search" );
  catalog[747]=dupstring( "Searching in " );
  catalog[748]=dupstring( "Grepping in " );
  catalog[749]=dupstring( "Cannot continue \"if\" parsing because the following command writes something to stderr\nand I treat this as an error:\n%s\nThe error was:\n" );
  catalog[750]=dupstring( "Cannot push the output of the following command because it writes something to stderr\nand I treat this as an error:\n%s\nThe error was:\n" );
  catalog[751]=dupstring( "I don't update the user window because the following command wrotes something to stderr:\n%s\nThe error was:\n" );
  catalog[752]=dupstring( "Text view color" );
  catalog[753]=dupstring( "Text view color for highlighted text" );
  catalog[754]=dupstring( "Enable filtered search" );
  catalog[755]=dupstring( "Anonymous login" );
  catalog[756]=dupstring( "Add \"%s\" to bookmarks." );
  catalog[757]=dupstring( "Make \"%s\" active when jumping to this bookmark." );
  catalog[758]=dupstring( "Find as you type:" );
  catalog[759]=dupstring( "Add bookmark" );
  catalog[760]=dupstring( "Delete bookmark" );
  catalog[761]=dupstring( "Jump to directory" );
  catalog[762]=dupstring( "Do you really want to delete the selected bookmark?" );
  catalog[763]=dupstring( "Read more" );
  catalog[764]=dupstring( "The size of the file is %s. Shall I show only the first %s (faster) or|do you want to see the complete file (slower)?|If you choose to view only the first part of the file, you can still read more of it on demand.|If text wrapping was enabled you can also choose to disable it which is much faster when reading large files." );
  catalog[765]=dupstring( "Read partial file" );
  catalog[766]=dupstring( "Read complete file" );
  catalog[767]=dupstring( "Read complete file but disable wrapping" );
  catalog[768]=dupstring( "Activate action" );
  catalog[769]=dupstring( "Without modifier key" );
  catalog[770]=dupstring( "Shift" );
  catalog[771]=dupstring( "Ctrl" );
  catalog[772]=dupstring( "Mod1" );
  catalog[773]=dupstring( "Context menu:" );
  catalog[774]=dupstring( "Name of custom action" );
  catalog[775]=dupstring( "Enter new action name:" );
  catalog[776]=dupstring( "Or select an existing name:" );
  catalog[777]=dupstring( "New action" );
  catalog[778]=dupstring( "Remove action" );
  catalog[779]=dupstring( "Enter name of the action to activate:" );
  catalog[780]=dupstring( "ChMod mode:" );
  catalog[781]=dupstring( "Set permission(s)" );
  catalog[782]=dupstring( "Add permission(s)" );
  catalog[783]=dupstring( "Remove permission(s)" );
  catalog[784]=dupstring( "General settings" );
  catalog[785]=dupstring( "Save Worker state on exit" );
  catalog[786]=dupstring( "Open Worker menu" );
  catalog[787]=dupstring( "Worker menu" );
  catalog[788]=dupstring( "Save Worker state" );
  catalog[789]=dupstring( "Quit Worker" );
  catalog[790]=dupstring( "Search:" );
  catalog[791]=dupstring( "Text view color for selection" );
  catalog[792]=dupstring( "Same file system only" );
  catalog[793]=dupstring( "Filter:" );
  catalog[794]=dupstring( "Show all" );
  catalog[795]=dupstring( "Libmagic pattern:" );
  catalog[796]=dupstring( "Use libmagic pattern" );
  catalog[797]=dupstring( "Test with file:" );
  catalog[798]=dupstring( "Libmagic output:" );
  catalog[799]=dupstring( "Output matches pattern:" );
  catalog[800]=dupstring( "Check" );
  catalog[801]=dupstring( "yes" );
  catalog[802]=dupstring( "no" );
  catalog[803]=dupstring( "Libmagic" );
  catalog[804]=dupstring( "Worker can recognize the file type by using the libmagic database which comes|with the stand-alone program \"file\". You can enter a pattern with wildcards|to match the libmagic output. Use the test text field to check the libmagic|output and whether the pattern matches or not. Just enter the file name and|click on \"Check\"." );
  catalog[805]=dupstring( "Edit bookmark" );
  catalog[806]=dupstring( "Bookmark path:" );
  catalog[807]=dupstring( "Bookmark alias:" );
  catalog[808]=dupstring( "Use parent directory and activate entry" );
  catalog[809]=dupstring( "Path" );
  catalog[810]=dupstring( "Alias" );
  catalog[811]=dupstring( "Decompress file" );
  catalog[812]=dupstring( "(in order of access [newest is bottom])" );
  catalog[813]=dupstring( "Modified after" );
  catalog[814]=dupstring( "Modified before" );
  catalog[815]=dupstring( "Basic options" );
  catalog[816]=dupstring( "More options" );
  catalog[817]=dupstring( "File size >=" );
  catalog[818]=dupstring( "File size <=" );
  catalog[819]=dupstring( "Calendar" );
  catalog[820]=dupstring( "The file size can be entered with the common suffixes kb, mb or gb|(or the SI units kib, mib, gib both with powers of 1024) and k, m or g (powers of 1000).|You may also enter floating-point numbers like 1.5mb." );
  catalog[821]=dupstring( "Command execution aborted! The maximum recursion limit has been reached.|This usually happens due to a bug in your configuration when one of the|callback commands like show-action is used for a file type action.|If you think this is a bug in Worker then please contact the mailing list!" );
  catalog[822]=dupstring( "Current label:" );
  catalog[823]=dupstring( "Label" );
  catalog[824]=dupstring( "all" );
  catalog[825]=dupstring( "Change the colors of various bookmark related components" );
  catalog[826]=dupstring( "List view colors for labeled entries:" );
  catalog[827]=dupstring( "Add" );
  catalog[828]=dupstring( "Remove" );
  catalog[829]=dupstring( "Select label to set or remove on active entry:" );
  catalog[830]=dupstring( "Ask for label at runtime" );
  catalog[831]=dupstring( "Choose label:" );
  catalog[832]=dupstring( "Bookmark/Label colors" );
  catalog[833]=dupstring( "Change label" );
  catalog[834]=dupstring( "Remove label" );
  catalog[835]=dupstring( "Choose to set or invert the selected options:" );
  catalog[836]=dupstring( "Set settings" );
  catalog[837]=dupstring( "Invert settings" );
  catalog[838]=dupstring( "Change pattern based filters" );
  catalog[839]=dupstring( "Change bookmark filter" );
  catalog[840]=dupstring( "Bookmark filter:" );
  catalog[841]=dupstring( "Show all entries" );
  catalog[842]=dupstring( "Show only bookmarks" );
  catalog[843]=dupstring( "Show only bookmark with given label" );
  catalog[844]=dupstring( "Show bookmarks with label:" );
  catalog[845]=dupstring( "Bookmark specific settings" );
  catalog[846]=dupstring( "Also apply colors to base directories of a bookmark" );
  catalog[847]=dupstring( "Add to bookmarks without label" );
  catalog[848]=dupstring( "Show bookmarks with empty label" );
  catalog[849]=dupstring( " - %s/%s free" );
  catalog[850]=dupstring( "Query label on runtime" );
  catalog[851]=dupstring( "Tab action" );
  catalog[852]=dupstring( "New tab" );
  catalog[853]=dupstring( "Close current tab" );
  catalog[854]=dupstring( "Switch to next tab" );
  catalog[855]=dupstring( "Switch to prev tab" );
  catalog[856]=dupstring( "Select the order of the elements.|An example view is shown at the right hande side.|Keep the list empty to use default view." );
  catalog[857]=dupstring( "Available elements:" );
  catalog[858]=dupstring( "Used elements:" );
  catalog[859]=dupstring( "Add element" );
  catalog[860]=dupstring( "Remove element" );
  catalog[861]=dupstring( "List view weight:" );
  catalog[862]=dupstring( "Match inside file name (implicit leading *)" );
  catalog[863]=dupstring( "Volume manager settings" );
  catalog[864]=dupstring( "New devices available:" );
  catalog[865]=dupstring( "This section contains the settings for the volume manager." );
  catalog[866]=dupstring( "Command to mount fstab entries:" );
  catalog[867]=dupstring( "Command to unmount fstab entries:" );
  catalog[868]=dupstring( "fstab file:" );
  catalog[869]=dupstring( "mtab file:" );
  catalog[870]=dupstring( "File with available partitions:" );
  catalog[871]=dupstring( "Volume manager" );
  catalog[872]=dupstring( "List of available volumes:" );
  catalog[873]=dupstring( "Supposed mount point" );
  catalog[874]=dupstring( "Device" );
  catalog[875]=dupstring( "Mount point" );
  catalog[876]=dupstring( "Status:" );
  catalog[877]=dupstring( "Go to mount point" );
  catalog[878]=dupstring( "Mounting..." );
  catalog[879]=dupstring( "Mount successful" );
  catalog[880]=dupstring( "Unmounting..." );
  catalog[881]=dupstring( "Unmount successful" );
  catalog[882]=dupstring( "Edit list of hidden devices" );
  catalog[883]=dupstring( "List of devices:" );
  catalog[884]=dupstring( "Remove selected entries from list" );
  catalog[885]=dupstring( "Mount volume - m" );
  catalog[886]=dupstring( "Unmount volume - u" );
  catalog[887]=dupstring( "Hide volume" );
  catalog[888]=dupstring( "Edit hidden volume list" );
  catalog[889]=dupstring( "Mount failed: Already mounted" );
  catalog[890]=dupstring( "Mount failed: Can't mount fstab entries with HAL" );
  catalog[891]=dupstring( "Mount failed: %s: %s" );
  catalog[892]=dupstring( "Mount failed: Not in fstab" );
  catalog[893]=dupstring( "Unmount failed: Not mounted" );
  catalog[894]=dupstring( "Unmount failed: Can't unmount fstab entries with HAL" );
  catalog[895]=dupstring( "Unmount failed: %s: %s" );
  catalog[896]=dupstring( "Unmount failed: Not in fstab" );
  catalog[897]=dupstring( "Available flags" );
  catalog[898]=dupstring( "Command used if empty: %s" );
  catalog[899]=dupstring( "File used if empty: %s" );
  catalog[900]=dupstring( "The following new devices are available:" );
  catalog[901]=dupstring( "Mount and CD" );
  catalog[902]=dupstring( "Mount" );
  catalog[903]=dupstring( "Open volume manager" );
  catalog[904]=dupstring( "Do nothing" );
  catalog[905]=dupstring( "Mount failed: %s" );
  catalog[906]=dupstring( "Ask what to do with new devices" );
  catalog[907]=dupstring( "Permission sort" );
  catalog[908]=dupstring( "Set weight relative to active side" );
  catalog[909]=dupstring( "No information about %s could be retrieved!|This entry will be skipped." );
  catalog[910]=dupstring( "<new label>" );
  catalog[911]=dupstring( "show output (internal viewer)" );
  catalog[912]=dupstring( "Volume label" );
  catalog[913]=dupstring( "Volume size" );
  catalog[914]=dupstring( "Size (human readable)" );
  catalog[915]=dupstring( "Text view font" );
  catalog[916]=dupstring( "Closing tray..." );
  catalog[917]=dupstring( "Closing tray successful" );
  catalog[918]=dupstring( "Ejecting..." );
  catalog[919]=dupstring( "Eject successful" );
  catalog[920]=dupstring( "Eject" );
  catalog[921]=dupstring( "Close tray" );
  catalog[922]=dupstring( "Update list" );
  catalog[923]=dupstring( "Eject: Can't eject non-HAL device with HAL" );
  catalog[924]=dupstring( "Close tray: Can't close tray of non-HAL device with HAL" );
  catalog[925]=dupstring( "Eject failed: %s: %s" );
  catalog[926]=dupstring( "Close tray failed: %s: %s" );
  catalog[927]=dupstring( "Command to eject non-HAL devices:" );
  catalog[928]=dupstring( "Command to close tray of non-HAL devices:" );
  catalog[929]=dupstring( "Eject (from menu)" );
  catalog[930]=dupstring( "Close tray (from menu)" );
  catalog[931]=dupstring( "Don't know what device to eject, choose from menu" );
  catalog[932]=dupstring( "Don't know what device to close tray for, choose from menu" );
  catalog[933]=dupstring( "AVFS module:" );
  catalog[934]=dupstring( "No device found for opening/closing." );
  catalog[935]=dupstring( "Key %d" );
  catalog[936]=dupstring( "Type of shortkey" );
  catalog[937]=dupstring( "Name of shortkey" );
  catalog[938]=dupstring( "Because of length limitations the following command string couldn't be built. Not all file names could be inserted for the chosen flags but you can decide to continue with as many files replaced as possible.\n\n" );
  catalog[939]=dupstring( "Text view mode" );
  catalog[940]=dupstring( "Maximum VFS depth:" );
  catalog[941]=dupstring( "External command exited with code %d" );
  catalog[942]=dupstring( "File type" );
  catalog[943]=dupstring( "Cursor" );
  catalog[944]=dupstring( "File list" );
  catalog[945]=dupstring( "File operations" );
  catalog[946]=dupstring( "Selections" );
  catalog[947]=dupstring( "Settings" );
  catalog[948]=dupstring( "Scripting" );
  catalog[949]=dupstring( "Other" );
  catalog[950]=dupstring( "Category" );
  catalog[951]=dupstring( "Command name" );
  catalog[952]=dupstring( "Close tab" );
  catalog[953]=dupstring( "Tab to other side" );
  catalog[954]=dupstring( "Tab to other side as new tab" );
  catalog[955]=dupstring( "Use flexible matching" );
  catalog[956]=dupstring( "Use version string compare" );
  catalog[957]=dupstring( "Switch mode:" );
  catalog[958]=dupstring( "Switch to next bank" );
  catalog[959]=dupstring( "Switch to prev bank" );
  catalog[960]=dupstring( "Switch to bank number" );
  catalog[961]=dupstring( "Bank number:" );
  catalog[962]=dupstring( "Search result is already shown by a different search window, creating a new one" );
  catalog[963]=dupstring( "Locked" );
  catalog[964]=dupstring( "Use temporary copies for all virtual files used for external programs" );
  catalog[965]=dupstring( "Eject - o" );
  catalog[966]=dupstring( "Close tray - c" );
  catalog[967]=dupstring( "Go to entry but keep window" );
  catalog[968]=dupstring( "Creating temporary file: %3.0f%% done..." );
  catalog[969]=dupstring( "Creating temporary file: %3.0f%% done (total: %3.0f%%)..." );
  catalog[970]=dupstring( "additional actions defined for current entry" );
  catalog[971]=dupstring( "Unmount failed: %s" );
  catalog[972]=dupstring( "Eject failed: %s" );
  catalog[973]=dupstring( "%s (%ld:%02ld at current rate)" );
  catalog[974]=dupstring( "Path jump" );
  catalog[975]=dupstring( "List of allowed directories to track for path jump command.|All those directories and their sub-directories are allowed to store in a local database|to allow faster access to often used directories. It's a prefix comparison|so add a trailing slash if necessary to avoid mismatches." );
  catalog[976]=dupstring( "Enter directory to allow it to be tracked by the path jump command:" );
  catalog[977]=dupstring( "Also show entries from bookmarks and persistent path store (Ctrl+a)" );
  catalog[978]=dupstring( "Match quality" );
  catalog[979]=dupstring( "String copied to clipboard:" );
  catalog[980]=dupstring( "Display mode (Ctrl+f):" );
  catalog[981]=dupstring( "All entries" );
  catalog[982]=dupstring( "Only subdirectories" );
  catalog[983]=dupstring( "Only direct subdirectories" );
  catalog[984]=dupstring( "Cleanup persistent list..." );
  catalog[985]=dupstring( "%d of %d entries in the persistent list of visited paths|do no longer exists, remove them from the list?" );
  catalog[986]=dupstring( "All entries still exist, nothing to remove." );
  catalog[987]=dupstring( "Show hints" );
  catalog[988]=dupstring( "evaluate command" );
  catalog[989]=dupstring( "Evaluate an internally registered command" );
  catalog[990]=dupstring( "Command:" );
  catalog[991]=dupstring( "Watch directories for changes" );
  catalog[992]=dupstring( "There is not enough space on target %s.|Free space is %s. Still continuing?" );
  catalog[993]=dupstring( "Command %s not found" );
  catalog[994]=dupstring( "" );
  catalog[995]=dupstring( "Panelize - F2" );
  catalog[996]=dupstring( "Panelize (keep window)" );
  catalog[997]=dupstring( "Switch mode" );
  catalog[998]=dupstring( "Configure mode" );
  catalog[999]=dupstring( "Apply dialog type to X windows" );
  catalog[1000]=dupstring( "Command not supported" );
  catalog[1001]=dupstring( "Flags without path are relative to base dir" );
  catalog[1002]=dupstring( "The target directory is a virtual directory. If you continue,|the files will be copied to|%s." );
  catalog[1003]=dupstring( "Match directories too" );
  catalog[1004]=dupstring( "Menu entries" );
  catalog[1005]=dupstring( "bank %d" );
  catalog[1006]=dupstring( "Paths" );
  catalog[1007]=dupstring( "Command menu: You can browse the entries with cursor keys, enter categories with right key.\nYou can enter a filter to quickly find a matching entry." );
  catalog[1008]=dupstring( "Current sub-tree:" );
  catalog[1009]=dupstring( "Element name:" );
  catalog[1010]=dupstring( "Assigned command:" );
  catalog[1011]=dupstring( "Reading directory content..." );
  catalog[1012]=dupstring( "A configuration update to version %d.%d.%d has been found.|You can decide to import the new elements or skip this update.|The update contains %%d button bank(s), %%d hotkey(s) and %%d file type(s).|You can now also change these parts before importing them." );
  catalog[1013]=dupstring( "Use MIME description" );
  catalog[1014]=dupstring( "Assigned shortkey:" );
  catalog[1015]=dupstring( "Activate search mode when pressing keys" );
  catalog[1016]=dupstring( "List view modes" );
  catalog[1017]=dupstring( "switch to normal mode" );
  catalog[1018]=dupstring( "switch to virtual dir mode" );
  catalog[1019]=dupstring( "switch to show image mode" );
  catalog[1020]=dupstring( "switch to information mode" );
  catalog[1021]=dupstring( "switch to text view mode" );
  catalog[1022]=dupstring( "open configuration of current mode" );
  catalog[1023]=dupstring( "Registered commands" );
  catalog[1024]=dupstring( "directory view" );
  catalog[1025]=dupstring( "virtual directory" );
  catalog[1026]=dupstring( "Directory mode" );
  catalog[1027]=dupstring( "inactive" );
  catalog[1028]=dupstring( "active (release with Ctrl-0)" );
  catalog[1029]=dupstring( "Hold status:" );
  catalog[1030]=dupstring( "Hold filter:" );
  catalog[1031]=dupstring( "Entries" );
  catalog[1032]=dupstring( "Key to hold entries" );
  catalog[1033]=dupstring( "You can hold the currently visible entries and refine the search with a new filter. You can press Ctrl 1 to 9 to hold the entries corresponding to their match level. Ctrl 1 will only keep the best matches, Ctrl 2 also the second best matches, and so on. You can also double click on the the entry in the right list view. To release the held entries, press Ctrl 0 or click on the 'active' button." );
  catalog[1034]=dupstring( "Panelize" );
  catalog[1035]=dupstring( "Command history:" );
  catalog[1036]=dupstring( "true if entry is a broken symlink" );
  catalog[1037]=dupstring( "Do you really want to delete all selected file types|and its sub-types?" );
  catalog[1038]=dupstring( "Select registered command:" );
  catalog[1039]=dupstring( "Continue in background" );
  catalog[1040]=dupstring( "You are going to copy %s|which is currently (partly) a destination for a background copy.|This may lead to unwanted results!" );
  catalog[1041]=dupstring( "Path entry on top" );
  catalog[1042]=dupstring( "The source file %s has been changed while copying.|Do you want to continue until the file is complete,|or skip this file and copy the other remaining files,|or cancel the operation at all?" );
  catalog[1043]=dupstring( "Always store used files in database" );
  catalog[1044]=dupstring( "Also show \"..\" entry" );
  catalog[1045]=dupstring( "Show breadcrumb navigation" );
  catalog[1046]=dupstring( "Copy finished" );
  catalog[1047]=dupstring( "Keep window opened" );

  catalog[1048]=dupstring( "Faces (for all other GUI elements)" );
  catalog[1049]=dupstring( "Changes to (some) faces only take effect after restarting Worker!" );
  catalog[1050]=dupstring( "Name" );
  catalog[1051]=dupstring( "Palette entry" );
  catalog[1052]=dupstring( "Parent face" );
  catalog[1053]=dupstring( "Change palette entry" );
  catalog[1054]=dupstring( "Unset palette entry" );
  catalog[1055]=dupstring( "Destination of symbolic link" );
  catalog[1056]=dupstring( "true if file is symbolic link" );
  catalog[1057]=dupstring( "Enable info line" );
  catalog[1058]=dupstring( "Use LUA to evaluate info line content" );
  catalog[1059]=dupstring( "Info line content:" );
  catalog[1060]=dupstring( "user name" );
  catalog[1061]=dupstring( "group name" );
  catalog[1062]=dupstring( "Do not ask again unless path changed" );
  catalog[1063]=dupstring( "Restore previous tab directories?" );
  catalog[1064]=dupstring( "Restore previously opened tabs:" );
  catalog[1065]=dupstring( "Never restore tabs" );
  catalog[1066]=dupstring( "Always restore tabs" );
  catalog[1067]=dupstring( "Ask on startup" );
  catalog[1068]=dupstring( "Store tab directories on exit:" );
  catalog[1069]=dupstring( "Never store directories" );
  catalog[1070]=dupstring( "Always store directories" );
  catalog[1071]=dupstring( "According to \"Save Worker state on exit\"" );
  catalog[1072]=dupstring( "Ask on exit" );
  catalog[1073]=dupstring( "Store currently opened tabs for next session?" );
  catalog[1074]=dupstring( "Waiting for file type check..." );
  catalog[1075]=dupstring( "Expression filter: next expected strings: " );
  catalog[1076]=dupstring( "Enter command and additional arguments:\nThis string will be executed in a shell but not all shell features \
are allowed!\nYou can use \"&&\", \"||\", \"|\", \";\" and \"&\"  (all as a single word) just as in a shell and redirections \
can also be used (also as single word, so e.g. 2>/dev/null have to be 2> /dev/null), but \
all other shell features are not allowed!\n\
It is also possible to use special flags ({f}...) like in the \"own command\", select them by clicking on the \"F\" button.\n\
You have to protect special characters in file names with a backslash or quote them correctly.\n\
The default value here is the active entry and it's already correctly protected!" );
  catalog[1077]=dupstring( "Use extended regular expressions" );
  catalog[1078]=dupstring( "Highlight first user action when opening menu" );
  catalog[1079]=dupstring( "Text view of %s" );
  catalog[1080]=dupstring( "Year:" );
  catalog[1081]=dupstring( "Month:" );
  catalog[1082]=dupstring( "Can't change time settings on %s!" );
  catalog[1083]=dupstring( "Change file time for %s" );
  catalog[1084]=dupstring( "Change modification time:" );
  catalog[1085]=dupstring( "Change last access time:" );
  catalog[1086]=dupstring( "Invalid modification time!" );
  catalog[1087]=dupstring( "Invalid last access time!" );
  catalog[1088]=dupstring( "Adjust relative symlinks" );
  catalog[1089]=dupstring( "outside destination dir" );
  catalog[1090]=dupstring( "Can't copy|  %s|to|  %s|Failed operation:%s|Error:%s" );
  catalog[1091]=dupstring( "Toggle symlink type" );
  catalog[1092]=dupstring( "Change mode:" );
  catalog[1093]=dupstring( "Edit symlink" );
  catalog[1094]=dupstring( "Make absolute" );
  catalog[1095]=dupstring( "Make relative" );
  catalog[1096]=dupstring( "The config file has been modified after being loaded.|Do you want to continue saving this config and overwrite the file?" );

  catalog[1097]=dupstring( "Ensure file permissions:" );
  catalog[1098]=dupstring( "leave unmodified" );
  catalog[1099]=dupstring( "user read/write" );
  catalog[1100]=dupstring( "user read/write, group read" );
  catalog[1101]=dupstring( "user read/write, all read" );
  catalog[1102]=dupstring( "Current time" );
  catalog[1103]=dupstring( "%d minute ago" );
  catalog[1104]=dupstring( "%d minutes ago" );
  catalog[1105]=dupstring( "%d hour ago" );
  catalog[1106]=dupstring( "%d hours ago" );
  catalog[1107]=dupstring( "%d day ago" );
  catalog[1108]=dupstring( "%d days ago" );
  catalog[1109]=dupstring( "%d week ago" );
  catalog[1110]=dupstring( "%d weeks ago" );
  catalog[1111]=dupstring( "Ignore last access time when sorting (Ctrl-t)" );
  catalog[1112]=dupstring( "Last accessed" );
  catalog[1113]=dupstring( "Progress: %.0f %%" );
  catalog[1114]=dupstring( "" );
  catalog[1115]=dupstring( "Hide non-existing entries" );
  catalog[1116]=dupstring( "Prefer udisks version:" );
  catalog[1117]=dupstring( "udisks1" );
  catalog[1118]=dupstring( "udisks2" );
  catalog[1119]=dupstring( "Do you really want to delete %ld files and %ld dirs recursively?|WARNING: %ld entries are outside of the visible area!" );
  catalog[1120]=dupstring( "Configure the visible columns used in the file list view. Add or remove the fields and configure the order, the first column is the top position.\nSetting the same columns again for a list view will switch back to the default columns configured in the global configuration." );
  catalog[1121]=dupstring( "Open configuration window" );
  catalog[1122]=dupstring( "Right-click to open menu" );
  catalog[1123]=dupstring( "Left-click: switch to next button bank\nRight-click: switch to previous button bank\nMouse wheel: switch button bank" );
  catalog[1124]=dupstring( "Right-click: open list view settings" );
  catalog[1125]=dupstring( "Change to parent directory" );
  catalog[1126]=dupstring( "Change to previously visited directory" );
  catalog[1127]=dupstring( "Change to oldest visited directory" );
  catalog[1128]=dupstring( "Open directory mode settings" );
  catalog[1129]=dupstring( "Open a new tab" );
  catalog[1130]=dupstring( "Close current tab" );
  catalog[1131]=dupstring( "Select flags as placeholders to be replaced with selected files" );
  catalog[1132]=dupstring( "Insert flags to operate commands on selected files and other variables" );
  catalog[1133]=dupstring( "Comma separates multiple patterns" );
  catalog[1134]=dupstring( "Executed command: %s" );
  catalog[1135]=dupstring( "Show Worker version, license, and compile time parameter" );
  catalog[1136]=dupstring( "Next match" );
  catalog[1137]=dupstring( "Previous match" );
  catalog[1138]=dupstring( "Restart search from top" );
  catalog[1139]=dupstring( "Matched position:" );
  catalog[1140]=dupstring( "String not found" );
  catalog[1141]=dupstring( "String found at line %d" );
  catalog[1142]=dupstring( "Jump to line number\nKey: Ctrl-l" );
  catalog[1143]=dupstring( "Close window\nKey: q, F3, F10" );
  catalog[1144]=dupstring( "Jump to next match\nKey: Enter or n" );
  catalog[1145]=dupstring( "Jump to previous match\nKey: p" );
  catalog[1146]=dupstring( "Read more lines from file\nKey: r" );
  catalog[1147]=dupstring( "Wrap lines at window width\nKey: w" );
  catalog[1148]=dupstring( "Show line numbers\nKey: l" );
  catalog[1149]=dupstring( "Enter search string\nKey to activate: /, Ctrl-s, Ctrl-f" );
  catalog[1150]=dupstring( "String compare mode:" );
  catalog[1151]=dupstring( "regular string comparison" );
  catalog[1152]=dupstring( "number optimized comparison" );
  catalog[1153]=dupstring( "compare ignoring case" );
  catalog[1154]=dupstring( "Accessed earlier than:" );
  catalog[1155]=dupstring( "+1 hour (Ctrl+h)" );
  catalog[1156]=dupstring( "+1 day (Ctrl+d)" );
  catalog[1157]=dupstring( "+1 week (Ctrl+w)" );
  catalog[1158]=dupstring( "Reset" );
  catalog[1159]=dupstring( "same day" );
  catalog[1160]=dupstring( "Modified:" );
  catalog[1161]=dupstring( "past" );
  catalog[1162]=dupstring( "future" );
  catalog[1163]=dupstring( "Results:" );
  catalog[1164]=dupstring( "Results: scanning..." );
  catalog[1165]=dupstring( "Waiting..." );
  catalog[1166]=dupstring( "<Cursor up> to increase number of days" );
  catalog[1167]=dupstring( "<Cursor down> to decrease number of days" );
  catalog[1168]=dupstring( "In this window you see all files modified at the same day as the newest file in the current directory. The results can be viewed as a virtual directory in the main window. You can also select only specific entries to be panelized. Also, you may choose to list files modified more days in the past." );
  catalog[1169]=dupstring( "Show recently used entries only" );
  catalog[1170]=dupstring( "Show recently used entries only (Ctrl-r)" );
  catalog[1171]=dupstring( "Change filter" );
  catalog[1172]=dupstring( "hide %s" );
  catalog[1173]=dupstring( "disable all filters" );
  catalog[1174]=dupstring( "disable filter %s" );
  catalog[1175]=dupstring( "Reload file\nKey: Ctrl-r" );
  catalog[1176]=dupstring( "Show non-printable" );
  catalog[1177]=dupstring( "Show non-printable characters as hexadecimal representation\ninstead of a simple dot replacement" );
  catalog[1178]=dupstring( "Show in list view" );
  catalog[1179]=dupstring( "Open a new tab in the current list view and highlight the currently viewed file" );
  catalog[1180]=dupstring( "Compare the directory in the active and inactive panel based on the criterion selected below.\n\nActive side: The equal entries will be selected (and the unequal entries will be unselected).\n\nInactive side: All equal entries will be deselected and all unequal ones will be selected (inverse to active side).\n\nYou can also choose to show the results in virtual directories. In this case the active panel will be a virtual directory will all equal files and directories while the inactive panel will be a virtual directory with all unequal elements." );
  catalog[1181]=dupstring( "" );
  catalog[1182]=dupstring( "" );
  catalog[1183]=dupstring( "Watch file" );
  catalog[1184]=dupstring( "Follow active entry" );
  catalog[1185]=dupstring( "How to use result:" );
  catalog[1186]=dupstring( "Change selection state" );
  catalog[1187]=dupstring( "Show as virtual directories" );
  catalog[1188]=dupstring( "Remaining entries to compare:" );
  catalog[1189]=dupstring( "File compare progress:" );
  catalog[1190]=dupstring( "Current directory:" );
  catalog[1191]=dupstring( "%lu of %lu" );

  catalog[1192]=dupstring( "left hand side:" );
  catalog[1193]=dupstring( "right hand side:" );
  catalog[1194]=dupstring( "Error writing tab profile %s" );
  catalog[1195]=dupstring( "Enter a profile name to store currently opened tabs or select a existing profile to restore given set of tabs. You may also choose to remove the profile after opening it successfully." );
  catalog[1196]=dupstring( "Save tabs currently opened as new profile:" );
  catalog[1197]=dupstring( "Existing profiles:" );
  catalog[1198]=dupstring( "Available profiles:" );
  catalog[1199]=dupstring( "Profile content:" );
  catalog[1200]=dupstring( "Open" );
  catalog[1201]=dupstring( "Open and remove" );
  catalog[1202]=dupstring( "Load" );
  catalog[1203]=dupstring( "Remove" );
  catalog[1204]=dupstring( "Invalid profile name %s" );
  catalog[1205]=dupstring( "Failed to execute the following command:\n%s\nThe return code was:%d\n and the error output was:\n" );
  catalog[1206]=dupstring( "The given program will be executed and the output lines will be added to a new virtual directory as files. Each output line is considered one file name. Absolute paths will be used as is, relative paths will be interpreted relatively to the current directory." );
  catalog[1207]=dupstring( "Program and arguments:" );
  catalog[1208]=dupstring( "State bar font" );
  catalog[1209]=dupstring( "Preserve base directories in virtual dirs" );
  catalog[1210]=dupstring( "Failed to create some of the intermediate directories of %s" );
  catalog[1211]=dupstring( "Failed to prepare the intermediate directory information for %s" );
  catalog[1212]=dupstring( "Toggle lock state" );
  catalog[1213]=dupstring( "Lock tab" );
  catalog[1214]=dupstring( "Unlock tab" );
  catalog[1215]=dupstring( "Move to the left" );
  catalog[1216]=dupstring( "Move to the right" );
  catalog[1217]=dupstring( "Sort settings" );
  catalog[1218]=dupstring( "Filter settings" );
  catalog[1219]=dupstring( "Other settings" );
  catalog[1220]=dupstring( "Entry filter settings" );
  catalog[1221]=dupstring( "Configure allowed paths in Path Jump configuration to see data!" );
  catalog[1222]=dupstring( "Last accesses by time - F1" );
  catalog[1223]=dupstring( "Last accesses by filter - F2" );
  catalog[1224]=dupstring( "Initial tab:" );
  catalog[1225]=dupstring( "Show accesses by time" );
  catalog[1226]=dupstring( "Show accesses by filter" );
  catalog[1227]=dupstring( "Clean unused paths from database" );
  catalog[1228]=dupstring( "Base path to use: %s" );
  catalog[1229]=dupstring( "Unused since" );
  catalog[1230]=dupstring( "days" );
  catalog[1231]=dupstring( "1 week" );
  catalog[1232]=dupstring( "1 month" );
  catalog[1233]=dupstring( "1 year" );
  catalog[1234]=dupstring( "Check matching entries" );
  catalog[1235]=dupstring( "Clean unused filters" );
  catalog[1236]=dupstring( "never used" );
  catalog[1237]=dupstring( "No entries match the selected time period" );
  catalog[1238]=dupstring( "%d of %d entries are older than the selected time period|Remove them from the list?" );
  catalog[1239]=dupstring( "File extension" );
  catalog[1240]=dupstring( "File extension sort" );
  catalog[1241]=dupstring( "Drag'N'Drop-Action" );
  catalog[1242]=dupstring( "DoubleClick-Action" );
  catalog[1243]=dupstring( "Show-Action" );
  catalog[1244]=dupstring( "RawShow-Action" );
  catalog[1245]=dupstring( "User-Action" );
  catalog[1246]=dupstring( "One row up" );
  catalog[1247]=dupstring( "One row down" );
  catalog[1248]=dupstring( "Change hidden flag" );
  catalog[1249]=dupstring( "Copy" );
  catalog[1250]=dupstring( "First Entry" );
  catalog[1251]=dupstring( "Last Entry" );
  catalog[1252]=dupstring( "Page Up" );
  catalog[1253]=dupstring( "Page Down" );
  catalog[1254]=dupstring( "(De)Select active entry" );
  catalog[1255]=dupstring( "Select all entries" );
  catalog[1256]=dupstring( "Deselect all entries" );
  catalog[1257]=dupstring( "invert selection" );
  catalog[1258]=dupstring( "go to parent dir" );
  catalog[1259]=dupstring( "go to dir" );
  catalog[1260]=dupstring( "Change list view settings" );
  catalog[1261]=dupstring( "switch to other list view" );
  catalog[1262]=dupstring( "pattern select" );
  catalog[1263]=dupstring( "pattern unselect" );
  catalog[1264]=dupstring( "path to other side" );
  catalog[1265]=dupstring( "Quit worker" );
  catalog[1266]=dupstring( "Delete" );
  catalog[1267]=dupstring( "Reload" );
  catalog[1268]=dupstring( "Make dir" );
  catalog[1269]=dupstring( "own command" );
  catalog[1270]=dupstring( "rename" );
  catalog[1271]=dupstring( "directory size" );
  catalog[1272]=dupstring( "simulate doubleclick" );
  catalog[1273]=dupstring( "start prog" );
  catalog[1274]=dupstring( "search entry" );
  catalog[1275]=dupstring( "edit path (in the text field)" );
  catalog[1276]=dupstring( "scroll listview" );
  catalog[1277]=dupstring( "create symlink" );
  catalog[1278]=dupstring( "change symlink" );
  catalog[1279]=dupstring( "change permissions" );
  catalog[1280]=dupstring( "toggle list view mode" );
  catalog[1281]=dupstring( "set sort mode" );
  catalog[1282]=dupstring( "set filter" );
  catalog[1283]=dupstring( "activate shortkey from list" );
  catalog[1284]=dupstring( "change owner" );
  catalog[1285]=dupstring( "script" );
  catalog[1286]=dupstring( "show directory cache" );
  catalog[1287]=dupstring( "start action of parent type" );
  catalog[1288]=dupstring( "do nothing" );
  catalog[1289]=dupstring( "FTP connection" );
  catalog[1290]=dupstring( "Internal text viewer" );
  catalog[1291]=dupstring( "Search" );
  catalog[1292]=dupstring( "Directory bookmarks" );
  catalog[1293]=dupstring( "Open context menu" );
  catalog[1294]=dupstring( "Run custom action" );
  catalog[1295]=dupstring( "Open Worker menu" );
  catalog[1296]=dupstring( "Change file label" );
  catalog[1297]=dupstring( "Modify tabs" );
  catalog[1298]=dupstring( "Change layout" );
  catalog[1299]=dupstring( "Volume manager" );
  catalog[1300]=dupstring( "Switch button bank" );
  catalog[1301]=dupstring( "Path Jump" );
  catalog[1302]=dupstring( "Copy string to clipboard" );
  catalog[1303]=dupstring( "Command menu" );
  catalog[1304]=dupstring( "Change file time" );
  catalog[1305]=dupstring( "Change columns" );
  catalog[1306]=dupstring( "View newest files" );
  catalog[1307]=dupstring( "Compare directories" );
  catalog[1308]=dupstring( "Tab profiles" );
  catalog[1309]=dupstring( "External virtual dir" );
  catalog[1310]=dupstring( "Open tab menu" );
  catalog[1311]=dupstring( "Lock/unlock current tab" );
  catalog[1312]=dupstring( "Open popup menu for labels" );
  catalog[1313]=dupstring( "Recenter list view" );
  catalog[1314]=dupstring( "Create flat view of directory" );
  catalog[1315]=dupstring( "Create flat view of directory following symlinks" );
  catalog[1316]=dupstring( "Virtual dir from selected entries" );
  catalog[1317]=dupstring( "Add selected entries from other side to virtual dir" );
  catalog[1318]=dupstring( "Select command" );
  catalog[1319]=dupstring( "Description" );
  catalog[1320]=dupstring( "Remove non-existing entries from the database. A non-zero 'unused days' value will only remove non-existing entries not used for longer than the given number of days." );
  catalog[1321]=dupstring( "(Filetype)" );
  catalog[1322]=dupstring( "(All)" );
  catalog[1323]=dupstring( "<enter filter>" );
  catalog[1324]=dupstring( "Clean unused programs" );
  catalog[1325]=dupstring( "Program" );
  catalog[1326]=dupstring( "Last accesses by external program - F3" );
  catalog[1327]=dupstring( "Show accesses by external program" );
  catalog[1328]=dupstring( "Terminal returns early" );
  catalog[1329]=dupstring( "Apply color theme presets for a light or dark theme:|Your current palette and UI color settings will be overwritten!|You need to restart Worker to make theme changes active!" );
  catalog[1330]=dupstring( "Apply light theme" );
  catalog[1331]=dupstring( "Apply dark theme" );
  catalog[1332]=dupstring( "Directory presets" );
  catalog[1333]=dupstring( "Save current settings for current dir" );
  catalog[1334]=dupstring( "Edit saved settings" );
  catalog[1335]=dupstring( "Paths with sort and filter presets" );
  catalog[1336]=dupstring( "Sort settings:" );
  catalog[1337]=dupstring( "Filter settings:" );
  catalog[1338]=dupstring( "Sort mode:" );
  catalog[1339]=dupstring( "Remove filter" );
  catalog[1340]=dupstring( "Enter directory for which separate sort and|filter settings should be applied to." );
  catalog[1341]=dupstring( "(Mime type \"%s\" for unknown filetype)" );
  catalog[1342]=dupstring( "Mime type" );
  catalog[1343]=dupstring( "Create new filetype" );
  catalog[1344]=dupstring( "Edit filetype" );
  catalog[1345]=dupstring( "Toggle help state" );
  catalog[1346]=dupstring( "Click button or press key to get help about assigned commands" );
  catalog[1347]=dupstring( "Help about assigned commands" );
  catalog[1348]=dupstring( "Assigned command for clicked element:" );
  catalog[1349]=dupstring( "Assigned command for key (prefix) %s:" );
  catalog[1350]=dupstring( "Element type" );
  catalog[1351]=dupstring( "Short key" );
  catalog[1352]=dupstring( "Assigned command" );
  catalog[1353]=dupstring( "Button '%s'" );
  catalog[1354]=dupstring( "Hotkey '%s'" );
  catalog[1355]=dupstring( "Path button '%s'" );
  catalog[1356]=dupstring( "High" );
  catalog[1357]=dupstring( "Default" );
  catalog[1358]=dupstring( "Low" );
  catalog[1359]=dupstring( "File type priority:" );
  catalog[1360]=dupstring( "Priority" );
  catalog[1361]=dupstring( "File name and content" );
  catalog[1362]=dupstring( "File size" );
  catalog[1363]=dupstring( "File checksum" );
  catalog[1364]=dupstring( "Max number of bytes to check:" );
  catalog[1365]=dupstring( "Checksum feature not available due to missing OpenSSL" );
  catalog[1366]=dupstring( "Files are equal on same:" );
  catalog[1367]=dupstring( "File name" );
  catalog[1368]=dupstring( "File name and file size" );
  catalog[1369]=dupstring( "File name, size, and modification time" );
  catalog[1370]=dupstring( "use best matching filter from history or file ending" );
  catalog[1371]=dupstring( "Primary function:" );
  catalog[1372]=dupstring( "Secondary function:" );
  catalog[1373]=dupstring( "Key:" );
  catalog[1374]=dupstring( "reverse order" );
  catalog[1375]=dupstring( "interactive" );
  catalog[1376]=dupstring( "Status indicators" );
  catalog[1377]=dupstring( "Mode info" );
  catalog[1378]=dupstring( "Free space info" );
  catalog[1379]=dupstring( "Custom directory info" );
  catalog[1380]=dupstring( "Use custom list view bar line" );
  catalog[1381]=dupstring( "Custom LVB format:" );
  catalog[1382]=dupstring( "Custom directory info command:" );
  catalog[1383]=dupstring( "%s/%s free" );
  catalog[1384]=dupstring( "Apply filter immediately" );
  catalog[1385]=dupstring( "Disable background file existence checks for prefixes:" );
  catalog[1386]=dupstring( "Show relative paths" );
  catalog[1387]=dupstring( "Display mode:" );
  catalog[1388]=dupstring( "Include all data sources" );
  catalog[1389]=dupstring( "show entry" );
  catalog[1390]=dupstring( "Command %s requires arguments" );
  catalog[1391]=dupstring( "Arguments" );
  catalog[1392]=dupstring( "The command '%s' requires arguments.|Enter the argument string to be used (flags can be used):" );
  catalog[1393]=dupstring( "%s (requires arguments)" );
  catalog[1394]=dupstring( "Activate entry" );
  catalog[1395]=dupstring( "Show cache entry" );
  catalog[1396]=dupstring( "Switch tab to direction" );
  catalog[1397]=dupstring( "Move tab to direction" );
  catalog[1398]=dupstring( "(De)Select entries" );
  catalog[1399]=dupstring( "The string must contain the program name (either found in $PATH or via full path),|and the placeholder %s which is a temporary file containing the output to be shown." );
  catalog[1400]=dupstring( "Mode:" );
  catalog[1401]=dupstring( "Ask for permissions" );
  catalog[1402]=dupstring( "Set permissions" );
  catalog[1403]=dupstring( "Add permissions" );
  catalog[1404]=dupstring( "Remove permissions" );
  catalog[1405]=dupstring( "Octal value:" );
  catalog[1406]=dupstring( "Virtual dir from stack entries" );
  catalog[1407]=dupstring( "Add entry to virtual dir" );
  catalog[1408]=dupstring( "Hide non-existing entries (Ctrl+e)" );
  catalog[1409]=dupstring( "View command log" );
  catalog[1410]=dupstring( "Command log" );
  catalog[1411]=dupstring( "Log of file operations:" );
  catalog[1412]=dupstring( "Time" );
  catalog[1413]=dupstring( "Go to location" );
  catalog[1414]=dupstring( "Clear log" );
  catalog[1415]=dupstring( "Command" );
  catalog[1416]=dupstring( "use in own command" );
  catalog[1417]=dupstring( "change file permission" );
  catalog[1418]=dupstring( "change file time" );
  catalog[1419]=dupstring( "copy" );
  catalog[1420]=dupstring( "move" );
  catalog[1421]=dupstring( "%s to %s" );
  catalog[1422]=dupstring( "delete" );
  catalog[1423]=dupstring( "makedir" );
  catalog[1424]=dupstring( "rename" );
  catalog[1425]=dupstring( "change symlink" );
  catalog[1426]=dupstring( "create symlink" );
  catalog[1427]=dupstring( "%d of %d entries in the persistent list of visited paths|do no longer exists, remove them from the list or archive them?" );
  catalog[1428]=dupstring( "move to archive" );
  catalog[1429]=dupstring( "remove from list" );
  catalog[1430]=dupstring( "Cleanup archive..." );
  catalog[1431]=dupstring( "Remove old entries from the archive. A non-zero 'unused days' value will only remove entries not used for longer than the given number of days. Optionally, existing entries can be removed too." );
  catalog[1432]=dupstring( "Also remove existing entries" );
  catalog[1433]=dupstring( "%d of %d entries in the archive of visited paths|do no longer exists, remove them from the list?" );
  catalog[1434]=dupstring( "Include archive (Ctrl+A)" );
  catalog[1435]=dupstring( "Lock elements to current dir" );
  catalog[1436]=dupstring( "Lock to directory (Ctrl-l):" );
  catalog[1437]=dupstring( "active" );
  catalog[1438]=dupstring( "Directory:" );
  catalog[1439]=dupstring( "Warning: The copy operation is configured to always overwrite existing files. Please confirm!" );
  catalog[1440]=dupstring( "Warning: The copy operation is configured to never overwrite existing files. Please confirm!" );
  catalog[1441]=dupstring( "Ignore file name case" );
  catalog[1442]=dupstring( "Go to location & close window" );
  catalog[1443]=dupstring( "Matching filters" );
  catalog[1444]=dupstring( "Mismatching filters" );
  catalog[1445]=dupstring( "Stash current filters" );
  catalog[1446]=dupstring( "Apply stashed filters" );
  catalog[1447]=dupstring( "Apply" );
  catalog[1448]=dupstring( "Apply and remove" );
  catalog[1449]=dupstring( "Apply lastly stashed filters" );
  catalog[1450]=dupstring( "Apply and remove lastly stashed filters" );
  catalog[1451]=dupstring( "Increase limit" );
  catalog[1452]=dupstring( "It looks like opening a XZ file failed because of a too small memory limit for the decompressor.|The current limit is %s. Should I double the limit? You need to repeat the action again manually." );
  catalog[1453]=dupstring( "Text view alternative font" );
  catalog[1454]=dupstring( "Alternative font" );
  catalog[1455]=dupstring( "Toggle between text fonts\nKey: f" );
  catalog[1456]=dupstring( "Subdirectory %s could not be read completely!|Shall I continue as much as possible, skip this directory or abort?" );
  catalog[1457]=dupstring( "List of incompletely copied entries" );
  catalog[1458]=dupstring( "The following files and directories could not be copied completely.|Files which are explicitly skipped to not overwrite existing files are not part of this list!" );
  catalog[1459]=dupstring( "copy selected entries to clipboard" );
  catalog[1460]=dupstring( "copy or cut selected entries to clipboard" );
  catalog[1461]=dupstring( "create virtual dir from file list in clipboard" );
  catalog[1462]=dupstring( "Pasting files from clipboard failed because there were no usable elements" );
  catalog[1463]=dupstring( "Create virtual directory" );
  catalog[1464]=dupstring( "Paste files from clipboard by copying them into current directory,|or create a virtual directory with just those files?" );
  catalog[1465]=dupstring( "Can't copy|  %s|because it does not exist!" );
  catalog[1466]=dupstring( "Copy entry to clipboard" );
  catalog[1467]=dupstring( "Cut entry to clipboard" );
  catalog[1468]=dupstring( "Paste from clipboard" );
  catalog[1469]=dupstring( "Copy selected to clipboard" );
  catalog[1470]=dupstring( "Cut selected to clipboard" );
  catalog[1471]=dupstring( "Entry info to clipboard" );
  catalog[1472]=dupstring( "All entries to clipboard" );
  catalog[1473]=dupstring( "Directory view for stored selection state could not be restored." );
  catalog[1474]=dupstring( "Revert selection" );
  catalog[1475]=dupstring( "Select selection state to revert to:" );
  catalog[1476]=dupstring( "Directory" );
  catalog[1477]=dupstring( "Number of selected entries" );
  catalog[1478]=dupstring( "Input help:" );
  catalog[1479]=dupstring( "'pageup'/'-p'/'pagedown'/'p'/n/-n select whole page or +/-n elements" );
  catalog[1480]=dupstring( "number n for nth entry" );
  catalog[1481]=dupstring( "'-p' or negative number" );
  catalog[1482]=dupstring( "'-p' selects whole page upwards" );
  catalog[1483]=dupstring( "'menu' for interactive menu" );
  catalog[1484]=dupstring( "empty input for last selection, number n for nth entry, 'menu' for interactive menu" );
  catalog[1485]=dupstring( "number of elements" );
  catalog[1486]=dupstring( "directory to enter" );
  catalog[1487]=dupstring( "entry to activate" );
  catalog[1488]=dupstring( "cache entry element to show" );
  catalog[1489]=dupstring( "number of tabs to switch (positive to right, negative to left)" );
  catalog[1490]=dupstring( "amount of positions to move tab to (positive to right, negative to left)" );
  catalog[1491]=dupstring( "number addresses stack position" );
  catalog[1492]=dupstring( "entry to add" );
  catalog[1493]=dupstring( "path to show (directory or file)" );
  catalog[1494]=dupstring( "'cut' or anything else for copy operation" );
  catalog[1495]=dupstring( "issues cut operation" );
  catalog[1496]=dupstring( "issues copy operation" );
  catalog[1497]=dupstring( "'cut' or 'copy'" );
  catalog[1498]=dupstring( "Top level menu" );
  catalog[1499]=dupstring( "List view commands" );
  catalog[1500]=dupstring( "Activate Text View Mode" );
  catalog[1501]=dupstring( "Use current panel instead of other side" );
  catalog[1502]=dupstring( "Get content from" );
  catalog[1503]=dupstring( "Active file" );
  catalog[1504]=dupstring( "Command output" );
  catalog[1505]=dupstring( "File type action" );
  catalog[1506]=dupstring( "File type action:" );
  catalog[1507]=dupstring( "Select from list of existing file type actions" );
  catalog[1508]=dupstring( "invalid settings" );
  catalog[1509]=dupstring( "failed to parse command string" );
  catalog[1510]=dupstring( "empty command string after parsing" );
  catalog[1511]=dupstring( "<no content set>" );
  catalog[1512]=dupstring( "show output in other panel" );
  catalog[1513]=dupstring( "output from file type action:" );
  catalog[1514]=dupstring( "command output:" );
  catalog[1515]=dupstring( "Select how to execute command and how to handle output.\nSwitch option with Ctrl-1." );
  catalog[1516]=dupstring( "Run command as a global command from PATH.\nToggle option with Ctrl-2." );
  catalog[1517]=dupstring( "Run command in background.\nToggle option with Ctrl-3." );
  catalog[1518]=dupstring( "Do not enter current directory so it is not busy during execution.\nToggle option with Ctrl-4." );
  catalog[1519]=dupstring( "Follow the active file and re-run command when changed.\nToggle option with Ctrl-5." );
  catalog[1520]=dupstring( "Watch the file content for changes and re-run command when changd.\nToggle option with Ctrl-6." );
  catalog[1521]=dupstring( "Switch to text view mode in current panel instead of the opposite panel.\nToggle option with Ctrl-1." );
  catalog[1522]=dupstring( "Select which source is used to display the content from.\nSwitch option with Ctrl-2." );
  catalog[1523]=dupstring( "Follow the active file and re-evaluate output when changed.\nToggle option with Ctrl-3." );
  catalog[1524]=dupstring( "Watch the file content for changes and re-evalute output when changd.\nToggle option with Ctrl-4." );
  catalog[1525]=dupstring( "Reconnect successful" );
  catalog[1526]=dupstring( "Reconnect failed" );
  catalog[1527]=dupstring( "Reconnect" );
  catalog[1528]=dupstring( "DBus connection not available" );
  catalog[1529]=dupstring( "also push return code on top of stack" );
  catalog[1530]=dupstring( "Path prefix or pattern" );
  catalog[1531]=dupstring( "Filter type" );
  catalog[1532]=dupstring( "Prefix" );
  catalog[1533]=dupstring( "Use pattern matching" );
  catalog[1534]=dupstring( "Custom attribute" );
  catalog[1535]=dupstring( "show output in custom attribute column" );
  catalog[1536]=dupstring( "Execute command for each entry separately.\nToggle option with Ctrl-7." );
  catalog[1537]=dupstring( "Set custom attribute column" );
  catalog[1538]=dupstring( "Clear all custom attributes" );
  catalog[1539]=dupstring( "Custom attribute set but column not visible in LV config!" );
  catalog[1540]=dupstring( "wait in terminal" );
  catalog[1541]=dupstring( "output external" );
  catalog[1542]=dupstring( "output internal" );
  catalog[1543]=dupstring( "output in panel" );
  catalog[1544]=dupstring( "output as custom attr" );
  catalog[1545]=dupstring( "without terminal" );
  catalog[1546]=dupstring( "in background" );
  catalog[1547]=dupstring( "separate" );
  catalog[1548]=dupstring( "follow" );
  catalog[1549]=dupstring( "watch" );
  catalog[1550]=dupstring( "Do you really want to discard your changes?" );
  catalog[1551]=dupstring( "Jump to full entry" );
  catalog[1552]=dupstring( "Jump to current path element (Key Enter)\nJump to full element (Key Ctrl-Enter)" );
  catalog[1553]=dupstring( "Start with search string" );
  catalog[1554]=dupstring( "Apply search string without editing" );
  catalog[1555]=dupstring( "Remove by base dir..." );
  catalog[1556]=dupstring( "Select base path to remove results" );
  catalog[1557]=dupstring( "Select the directory for which all results within that directory|are removed from the list of results (and skip new results)." );
  catalog[1558]=dupstring( "Start search with selected entries" );
  catalog[1559]=dupstring( "Separator:" );
  catalog[1560]=dupstring( "Exclude label:" );
  catalog[1561]=dupstring( "Apply colors only to exact paths" );
  catalog[1562]=dupstring( "Excluded" );
  catalog[1563]=dupstring( "Removed" );
  catalog[1564]=dupstring( "Pre-select file name including its extension" );
  catalog[1565]=dupstring( "History" );
  catalog[1566]=dupstring( "Apply dark theme (warm)" );
  catalog[1567]=dupstring( "Expression" );
  catalog[1568]=dupstring( "(valid)" );
  catalog[1569]=dupstring( "(invalid)" );
  catalog[1570]=dupstring( "Match expression:" );
  catalog[1571]=dupstring( "Available expressions:" );
  catalog[1572]=dupstring( "Open context menu after holding activation button for a delay" );
  //catalog[1573]=dupstring( "" );

//  catalog[]=dupstring( "" );
  
  catalogflag[0]=dupstring( "String requester" );
  catalogflag[1]=dupstring( "path" );
  catalogflag[2]=dupstring( "other path" );
  catalogflag[3]=dupstring( "left path" );
  catalogflag[4]=dupstring( "right path" );
  catalogflag[5]=dupstring( "first selected entry without path" );
  catalogflag[6]=dupstring( "first selected entry with path" );
  catalogflag[7]=dupstring( "all selected entry without path" );
  catalogflag[8]=dupstring( "all selected entry with path" );
  catalogflag[9]=dupstring( "first selected entry without path (no extension)" );
  catalogflag[10]=dupstring( "first selected entry with path (no extension)" );
  catalogflag[11]=dupstring( "all selected entry without path (no extension)" );
  catalogflag[12]=dupstring( "all selected entry with path (no extension)" );
  catalogflag[13]=dupstring( "first selected entry without path (no unselect)" );
  catalogflag[14]=dupstring( "first selected entry with path (no unselect)" );
  catalogflag[15]=dupstring( "all selected entry without path (no unselect)" );
  catalogflag[16]=dupstring( "all selected entry with path (no unselect)" );
  catalogflag[17]=dupstring( "first selected entry without path (no unselect, no extension)" );
  catalogflag[18]=dupstring( "first selected entry with path (no unselect, no extension)" );
  catalogflag[19]=dupstring( "all selected entry without path (no unselect, no extension)" );
  catalogflag[20]=dupstring( "all selected entry with path (no unselect, no extension)" );
  catalogflag[21]=dupstring( "other side: first selected entry without path" );
  catalogflag[22]=dupstring( "other side: first selected entry with path" );
  catalogflag[23]=dupstring( "other side: all selected entry without path" );
  catalogflag[24]=dupstring( "other side: all selected entry with path" );
  catalogflag[25]=dupstring( "other side: first selected entry without path (no extension)" );
  catalogflag[26]=dupstring( "other side: first selected entry with path (no extension)" );
  catalogflag[27]=dupstring( "other side: all selected entry without path (no extension)" );
  catalogflag[28]=dupstring( "other side: all selected entry with path (no extension)" );
  catalogflag[29]=dupstring( "other side: first selected entry without path (no unselect)" );
  catalogflag[30]=dupstring( "other side: first selected entry with path (no unselect)" );
  catalogflag[31]=dupstring( "other side: all selected entry without path (no unselect)" );
  catalogflag[32]=dupstring( "other side: all selected entry with path (no unselect)" );
  catalogflag[33]=dupstring( "other side: first selected entry without path (no unselect, no extension)" );
  catalogflag[34]=dupstring( "other side: first selected entry with path (no unselect, no extension)" );
  catalogflag[35]=dupstring( "other side: all selected entry without path (no unselect, no extension)" );
  catalogflag[36]=dupstring( "other side: all selected entry with path (no unselect, no extension)" );
  catalogflag[37]=dupstring( "destination path (for DND)" );
  catalogflag[38]=dupstring( "remove top element of given stack (default 0)" );
  catalogflag[39]=dupstring( "get (not remove) top element of given stack (default 0)" );
  catalogflag[40]=dupstring( "true when stack is empty" );
  catalogflag[41]=dupstring( "size of stack" );
  catalogflag[42]=dupstring( "last error (0 means no error)" );
  catalogflag[43]=dupstring( "true when file list is empty (no more {f})" );
  catalogflag[44]=dupstring( "always true" );
  catalogflag[45]=dupstring( "always false" );
  catalogflag[46]=dupstring( "string (flags from own command can be used)" );
  catalogflag[47]=dupstring( "replaces with return value of the command inside the brace (flags can be used)" );
  catalogflag[48]=dupstring( "replaces with output (first line) of the command inside the brace (flags can be used)" );
  catalogflag[49]=dupstring( "replaces with the size of given stack (default 0)" );
  catalogflag[50]=dupstring( "replaces with value of the given env-variable" );
  catalogflag[51]=dupstring( "convert the expression inside the parentheses into a number" );
  catalogflag[52]=dupstring( "convert the expression inside the parentheses into a string" );
  catalogflag[53]=dupstring( "path to the \"scripts\" directory" );
  catalogflag[54]=dupstring( "path to temporary copy of first file" );
  catalogflag[55]=dupstring( "path to temporary copy of first file (no unselect)" );
  catalogflag[56]=dupstring( "other side: path to temporary copy of first file" );
  catalogflag[57]=dupstring( "other side: path to temporary copy of first file (no unselect)" );
  catalogflag[58]=dupstring( "1 if string argument is a local file, 0 for virtual file, -1 for non existing file or error" );
  catalogflag[59]=dupstring( "file name extension of first file (no unselect)" );
  catalogflag[60]=dupstring( "other side: file name extension of first file (no unselect)" );
  catalogflag[61]=dupstring( "file size of first file (no unselect)" );
  catalogflag[62]=dupstring( "other side: file size of first file (no unselect)" );
  catalogflag[63]=dupstring( "last modification time of first file (no unselect)" );
  catalogflag[64]=dupstring( "other side: last modification time of first file (no unselect)" );
  catalogflag[65]=dupstring( "last change time of first file (no unselect)" );
  catalogflag[66]=dupstring( "other side: last change time of first file (no unselect)" );
  catalogflag[67]=dupstring( "current time (unix time)" );
  catalogflag[68]=dupstring( "random number (32 bit)" );
}

WorkerLocale::~WorkerLocale()
{
  freeLanguage();
  delete [] catalog;
  delete [] catalogflag;
}

int WorkerLocale::openCatFile(Datei*file,const char *name,const char *ext)
{
  bool ok=false;
  std::string mainstr;
  std::string home = WorkerInitialSettings::getInstance().getConfigBaseDir();
  if ( Worker::getDataDir().length() > 0 ) {
    mainstr = Worker::getDataDir();
    mainstr += "/catalogs/";
    mainstr += name;
  #ifdef USEOWNCONFIGFILES
    mainstr += ".catalog2";
  #else
    mainstr += ".catalog";
  #endif
    mainstr += ext;

    if ( UTF8::getUTF8Mode() != UTF8::UTF8_DISABLED && UTF8::getCurrentEncoding() == UTF8::ENCODING_UTF8 ) {
      mainstr += ".utf8";
    }

    if ( file->open( mainstr.c_str(), "r" ) == 0 ) ok = true;
  }
  if(ok==false) {
    if ( home.length() < 1 ) return 1;
    mainstr = home;
    mainstr += "/catalogs/";
    mainstr += name;
  #ifdef USEOWNCONFIGFILES
    mainstr += ".catalog2";
  #else
    mainstr += ".catalog";
  #endif
    mainstr += ext;
    
    if ( UTF8::getUTF8Mode() != UTF8::UTF8_DISABLED && UTF8::getCurrentEncoding() == UTF8::ENCODING_UTF8 ) {
      mainstr += ".utf8";
    }

    if ( file->open( mainstr.c_str(), "r" ) == 0 ) ok = true;
  }
  if(ok==true) return 0;
  return 1;
}

int WorkerLocale::loadLanguage(const char *name)
{
  freeLanguage();
  initBuiltinLang();
  Datei *file=new Datei();

  if(openCatFile(file,name,"")==0) {
    parse(file,0);
    file->close();

    if(openCatFile(file,name,".flags")==0) {
      parse(file,2);
      file->close();
    }
  } else debugmsg("selected language not available, using builtin!\n");
  delete file;
  return 0;
}

/* this function replaces backslash n with a real newline
 * recently I added support for using \n in translations but the
 * file format doesn't support it natively
 * Perhaps it's time to switch to getext
 */
static char *replaceNewline( const char *buffer )
{
    char *res = dupstring( buffer );
    const char *src;
    char *dest;

    src = buffer;
    dest = res;
    while ( *src != '\0' ) {
        if ( *src == '\\' && *( src + 1 ) == 'n' ) {
            *dest++ = '\n';
            src += 2;
        } else
            *dest++ = *src++;
    }
    *dest = '\0';
    return res;
}

void WorkerLocale::parse(Datei *file,int cat)
{
  char *buffer=(char*)_allocsafe(128*1024);
  int mode,bufpos;
  int entrynr;
  char ch;
  char *tstr;
  char filebuffer[1024];
  int filebuffer_len = 0;
  int filebuffer_offset = 0;

  mode=0;
  bufpos=0;
  entrynr=0;
  
  for(;;) {
    if ( filebuffer_offset >= filebuffer_len ) {
        filebuffer_len = file->read( filebuffer, 1024 );
        filebuffer_offset = 0;
    }
    if ( filebuffer_offset >= filebuffer_len ) {
        break;
    }
    ch = filebuffer[filebuffer_offset++];
    switch(mode) {
      case 1:
        if(ch=='\n') {
          mode=0;
          bufpos=0;
          entrynr=0;
        }
        break;
      case 2:
        // old style
        if(ch=='\n') {
          buffer[bufpos]=0;
          entrynr++;
          bufpos=0;
        } else {
          buffer[ w_min( 128 * 1024 - 1, bufpos ) ] = ch;
          bufpos++;
        }
        break;
      case 3:
        if(ch=='\\') {
          mode=4;
        } else if((ch=='\n')||(ch=='#')) {
          buffer[bufpos]=0;
          if(strlen(buffer)>0) {
            // store buffer in entrynr
            tstr = replaceNewline( buffer );
            switch(cat) {
              case 0:
                // normal
                if(entrynr<CATENTRY) {
                  if ( catalog[entrynr] != NULL ) _freesafe( catalog[entrynr] );
                  catalog[entrynr]=tstr;
                } else _freesafe(tstr);
                break;
              case 2:
                // flag
                if(entrynr<CATFLAGENTRY) {
                  if ( catalogflag[entrynr] != NULL ) _freesafe( catalogflag[entrynr] );
                  catalogflag[entrynr]=tstr;
                } else _freesafe(tstr);
                break;
              default:
                _freesafe(tstr);
                break;
            }
          }
          if(ch=='#') mode=1;
          else {
            mode=0;
            bufpos=0;
            entrynr=0;
          }
        } else {
          buffer[ w_min( 128 * 1024 - 1, bufpos ) ] = ch;
          bufpos++;
        }
        break;
      case 4:
        if(ch!='\n') {
          buffer[ w_min( 128 * 1024 - 1, bufpos ) ] = ch;
          bufpos++;
        }
        mode=3;
        break;
      default:
        if(isdigit(ch)!=0) {
          buffer[ w_min( 128 * 1024 - 1, bufpos ) ] = ch;
          bufpos++;
        } else if(ch=='#') {
          mode=1;
        } else if(ch==':') {
          buffer[bufpos]=0;
          entrynr=0;
          sscanf(buffer,"%d",&entrynr);
          mode=3;
          bufpos=0;
        } else if(ch=='\n') {
        } else {
          // old style
          //TODO: bessere Meldung
          printf("oldstyle-catalog-format not supported\n");
          mode=2;
          buffer[ w_min( 128 * 1024 - 1, bufpos ) ] = ch;
          bufpos++;
          entrynr=0;
        }
        break;
    }
  }
  _freesafe(buffer);
}

void WorkerLocale::resetLanguage()
{
  freeLanguage();
  initBuiltinLang();
}

void WorkerLocale::freeLanguage()
{
  int i;
  for(i=0;i<CATENTRY;i++) {
    if ( catalog[i] != NULL ) _freesafe(catalog[i]);
    catalog[i] = NULL;
  }
  for(i=0;i<CATFLAGENTRY;i++) {
    if ( catalogflag[i] != NULL ) _freesafe(catalogflag[i]);
    catalogflag[i] = NULL;
  }
}

const char *WorkerLocale::getLocale(int index2)
{
  if((index2<0)||(index2>=CATENTRY)) return "?";
  if ( catalog[index2] == NULL ) return "?";
  return catalog[index2];
}

const char *WorkerLocale::getLocaleFlag(int index2)
{
  if((index2<0)||(index2>=CATFLAGENTRY)) return "?";
  if ( catalogflag[index2] == NULL ) return "?";
  return catalogflag[index2];
}
