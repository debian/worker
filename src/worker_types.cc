/* worker_types.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2003-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "worker_types.h"

struct WorkerTypes::availListCols_t WorkerTypes::availListCols[] = {
  { WorkerTypes::LISTCOL_NAME, 176 },
  { WorkerTypes::LISTCOL_SIZE, 177 },
  { WorkerTypes::LISTCOL_TYPE, 178 },
  { WorkerTypes::LISTCOL_PERM, 180 },
  { WorkerTypes::LISTCOL_OWNER, 184 },
  { WorkerTypes::LISTCOL_DEST, 179 },
  { WorkerTypes::LISTCOL_MOD, 163 },
  { WorkerTypes::LISTCOL_ACC, 162 },
  { WorkerTypes::LISTCOL_CHANGE, 164 },
  { WorkerTypes::LISTCOL_INODE, 548 },
  { WorkerTypes::LISTCOL_NLINK, 549 },
  { WorkerTypes::LISTCOL_BLOCKS, 550 },
  { WorkerTypes::LISTCOL_SIZEH, 914 },
  { WorkerTypes::LISTCOL_EXTENSION, 1239 },
  { WorkerTypes::LISTCOL_CUSTOM_ATTR, 1534 }
};

unsigned int WorkerTypes::getAvailListColsSize()
{
  return ( sizeof( availListCols ) / sizeof( availListCols[0] ) );
}

int WorkerTypes::getAvailListColEntry( WorkerTypes::listcol_t type )
{
  int i, found = -1;
  for ( i = getAvailListColsSize() - 1; i >= 0; i-- ) {
    if ( availListCols[i].type == type ) {
      found = i;
      break;
    }
  }
  return found;
}
