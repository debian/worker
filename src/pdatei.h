/* pdatei.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2002-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PDATEI_H
#define PDATEI_H

#include "wdefines.h"
#include <string>

class PDatei
{
 public:
  PDatei();
  ~PDatei();
  PDatei( const PDatei &other );
  PDatei &operator=( const PDatei &other );

  enum pdatei_handler { PDATEI_UNKNOWN, PDATEI_NORMAL, PDATEI_GZIP, PDATEI_BZIP2, PDATEI_XPK };

  typedef enum {
      NORMAL_OPEN,
      NONBLOCKING_FOR_NONREGULAR
  } open_param_t;

  int open( const char *filename, open_param_t mode = NORMAL_OPEN );
  ssize_t read( void *buf, size_t size );
  void close();
  static enum pdatei_handler getCruncher( const char *filename );
  enum pdatei_handler getHandler();
    static loff_t getFileSize( const std::string &filename );
 protected:
  enum pdatei_handler type;
  std::string name;
  int fd;

    static const std::string getVFSName( const char *filename, enum pdatei_handler type );
};

#endif
