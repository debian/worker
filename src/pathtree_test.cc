#include <check.h>
#include <stdlib.h>
#include "pathtree.hh"

START_TEST(test1)
{
    PathTree< int> pt( "/" );

    pt.addPath( "/a", 1 );
    pt.addPath( "/a/b/c", 2 );
    pt.addPath( "/a/b/c/d/e", 3 );

    auto n1 = pt.outerFind( "/c", true );
    auto n2 = pt.outerFind( "/a/b", true );
    auto n3 = pt.outerFind( "/a/b/c", true );
    auto n4 = pt.outerFind( "/a/b/c/d", true );
    auto n5 = pt.outerFind( "/a/f", true );
    auto n6 = pt.outerFind( "/a/b/c/g", true );
    auto n7 = pt.outerFind( "/a/b/c/d/e/f", true );

    ck_assert_ptr_eq( n1, NULL );
    ck_assert_ptr_ne( n2, NULL );
    ck_assert_int_eq( n2->getNodeData(), 1 );
    ck_assert_ptr_ne( n3, NULL );
    ck_assert_int_eq( n3->getNodeData(), 2 );
    ck_assert_ptr_ne( n4, NULL );
    ck_assert_int_eq( n4->getNodeData(), 2 );
    ck_assert_ptr_ne( n5, NULL );
    ck_assert_int_eq( n5->getNodeData(), 1 );
    ck_assert_ptr_ne( n6, NULL );
    ck_assert_int_eq( n6->getNodeData(), 2 );
    ck_assert_ptr_ne( n7, NULL );
    ck_assert_int_eq( n7->getNodeData(), 3 );
}
END_TEST

Suite * pathtree_suite( void )
{
    Suite *s;
    TCase *tc_pathtree;

    s = suite_create("pathtree test");

    /* Core test case */
    tc_pathtree = tcase_create( "test1" );

    tcase_add_test( tc_pathtree, test1 );
    suite_add_tcase( s, tc_pathtree );

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = pathtree_suite();
    sr = srunner_create( s );

    srunner_run_all( sr, CK_NORMAL );
    number_failed = srunner_ntests_failed( sr );
    srunner_free( sr );
    return ( number_failed == 0 ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
