/* import.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2008,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef IMPORT_H
#define IMPORT_H

#define DNDACTION_NUMBER 0
#define DOUBLECLICKACTION_NUMBER 1
#define SHOWACTION_NUMBER 2
#define RAWSHOWACTION_NUMBER 3
#define USERACTION_NUMBER 4
#define ROWUP_NUMBER 5
#define ROWDOWN_NUMBER 6
#define CHANGEHIDDENFLAG_NUMBER 7
#define COPYOP_NUMBER 8
#define FIRSTROW_NUMBER 9
#define LASTROW_NUMBER 10
#define PAGEUP_NUMBER 11
#define PAGEDOWN_NUMBER 12
#define SELECTOP_NUMBER 13
#define SELECTALLOP_NUMBER 14
#define SELECTNONEOP_NUMBER 15
#define INVERTALLOP_NUMBER 16
#define PARENTDIROP_NUMBER 17
#define ENTERDIROP_NUMBER 18
#define CHANGELISTERSETOP_NUMBER 19
#define SWITCHLISTEROP_NUMBER 20
#define FILTERSELECTOP_NUMBER 21
#define FILTERUNSELECTOP_NUMBER 22
#define PATH2OSIDEOP_NUMBER 23
#define QUITOP_NUMBER 24
#define DELETEOP_NUMBER 25
#define RELOADOP_NUMBER 26
#define MAKEDIROP_NUMBER 27
#define OWNOP_NUMBER 28
#define RENAMEOP_NUMBER 29
#define DIRSIZEOP_NUMBER 30
#define SIMDDOP_NUMBER 31
#define STARTPROGOP_NUMBER 32
#define SEARCHENTRYOP_NUMBER 33
#define ENTERPATHOP_NUMBER 34
#define SCROLLLISTEROP_NUMBER 35
#define CREATESYMLINKOP_NUMBER 36
#define CHANGESYMLINKOP_NUMBER 37
#define CHMODOP_NUMBER 38
#define TOGGLELISTERMODEOP_NUMBER 39
#define SETSORTMODEOP_NUMBER 40
#define SETFILTEROP_NUMBER 41
#define SHORTKEYFROMLISTOP_NUMBER 42
#define CHOWNOP_NUMBER 43
#define SCRIPTOP_NUMBER 44
#define SHOWDIRCACHEOP_NUMBER 45

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
