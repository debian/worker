/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "configparser.yy"

/* parser for config file (bison generated)
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2003-2025 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wdefines.h"
#include "wconfig.h"
#include "wcdoubleshortkey.hh"
#include "wcpath.hh"
#include "wcfiletype.hh"
#include "wchotkey.hh"
#include "wcbutton.hh"
#include <stdlib.h>
#include <string.h>
#include "worker_commands.h"
#include "configheader.h"
#include <list>
#include "verzeichnis.hh"
#include <map>
#include "dirfiltersettings.hh"
#include "layoutsettings.hh"
#include "simplelist.hh"
#include "worker_types.h"
#include "wconfig_types.hh"

using namespace CopyParams;

int lconfig_side;
int lconfig_pos;
List *lconfig_listp,
     *lconfig_listb,
     *lconfig_listft,
     *lconfig_listh,
     *lconfig_listsk;
std::list< WConfigTypes::dont_check_dir > lconfig_ignorelist;
WConfigTypes::dont_check_dir lconfig_ignore_dir;
command_list_t lconfig_listcom;
std::list< std::string > lconfig_pathjump_allow;
std::map< std::string, struct directory_presets > dir_presets;
std::string path;
struct directory_presets dir_preset;
NM_Filter dir_preset_filter;
WCButton *lconfig_bt1;
WCFiletype *lconfig_ft1, *old_lconfig_ft1;
std::list<WCFiletype*> filetypehier;
WCHotkey *lconfig_hk1;
WCDoubleShortkey *lconfig_dk;
WCPath *lconfig_p1;
int lconfig_id;
KeySym lconfig_nkeysym;
unsigned int lconfig_keymod;
char *lconfig_tstr;
char lconfig_error[256];
std::map<std::string, WConfig::ColorDef::label_colors_t> lconfig_labelcolors;
WConfig::ColorDef::label_colors_t lconfig_labelcolor;
std::string lconfig_labelcolor_name;
LayoutSettings layout_sets;
std::string lconfig_face_name;
int lconfig_face_color;
std::list< std::pair< std::string, int > > lconfig_faces_list;
std::vector< WorkerTypes::listcol_t > columns;

class ParseState {
public:
    void clear()
    {
        lconfig_fp1.reset();
    }

    std::shared_ptr< FunctionProto > lconfig_fp1;
};

static ParseState ps;

extern WConfig *lconfig;

extern int yyerror( const char *s );

void lconfig_parseinit(void)
{
  lconfig_listp = NULL;
  lconfig_listb = NULL;
  lconfig_listft = NULL;
  lconfig_listh = NULL;
  lconfig_listsk = NULL;
  lconfig_listcom = {};
  lconfig_ignorelist.clear();
  lconfig_bt1 = NULL;
  lconfig_ft1 = NULL;
  lconfig_hk1 = NULL;
  lconfig_dk = NULL;
  lconfig_p1 = NULL;
  lconfig_tstr = NULL;
  ps.clear();
}

void lconfig_cleanup(void)
{
  ps.clear();

  if ( lconfig_tstr != NULL ) {
    _freesafe( lconfig_tstr );
    lconfig_tstr = NULL;
  }
  if ( lconfig_p1 != NULL ) {
    delete lconfig_p1;
    lconfig_p1 = NULL;
  }
  if ( lconfig_dk != NULL ) {
    delete lconfig_dk;
    lconfig_dk = NULL;
  }
  if ( lconfig_hk1 != NULL ) {
    delete lconfig_hk1;
    lconfig_hk1 = NULL;
  }
  if ( lconfig_ft1 != NULL ) {
    delete lconfig_ft1;
    lconfig_ft1 = NULL;
  }
  if ( lconfig_bt1 != NULL ) {
    delete lconfig_bt1;
    lconfig_bt1 = NULL;
  }
  if ( lconfig_listp != NULL ) {
    lconfig_id = lconfig_listp->initEnum();
    lconfig_p1 = (WCPath*)lconfig_listp->getFirstElement( lconfig_id );
    while ( lconfig_p1 != NULL ) {
      delete lconfig_p1;
      lconfig_p1 = (WCPath*)lconfig_listp->getNextElement( lconfig_id );
    }
    lconfig_listp->closeEnum( lconfig_id );
    delete lconfig_listp;
    lconfig_listp = NULL;
  }
  if ( lconfig_listb != NULL ) {
    lconfig_id = lconfig_listb->initEnum();
    lconfig_bt1 = (WCButton*)lconfig_listb->getFirstElement( lconfig_id );
    while ( lconfig_bt1 != NULL ) {
      delete lconfig_bt1;
      lconfig_bt1 = (WCButton*)lconfig_listb->getNextElement( lconfig_id );
    }
    lconfig_listb->closeEnum( lconfig_id );
    delete lconfig_listb;
    lconfig_listb = NULL;
  }
  if ( lconfig_listft != NULL ) {
    lconfig_id = lconfig_listft->initEnum();
    lconfig_ft1 = (WCFiletype*)lconfig_listft->getFirstElement( lconfig_id );
    while ( lconfig_ft1 != NULL ) {
      delete lconfig_ft1;
      lconfig_ft1 = (WCFiletype*)lconfig_listft->getNextElement( lconfig_id );
    }
    lconfig_listft->closeEnum( lconfig_id );
    delete lconfig_listft;
    lconfig_listft = NULL;
  }

  while ( filetypehier.size() > 0 ) {
      lconfig_ft1 = filetypehier.back();
      filetypehier.pop_back();
      delete lconfig_ft1;
      lconfig_ft1 = NULL;
  }

  if ( lconfig_listh != NULL ) {
    lconfig_id = lconfig_listh->initEnum();
    lconfig_hk1 = (WCHotkey*)lconfig_listh->getFirstElement( lconfig_id );
    while ( lconfig_hk1 != NULL ) {
      delete lconfig_hk1;
      lconfig_hk1 = (WCHotkey*)lconfig_listh->getNextElement( lconfig_id );
    }
    lconfig_listh->closeEnum( lconfig_id );
    delete lconfig_listh;
    lconfig_listh = NULL;
  }
  if ( lconfig_listsk != NULL ) {
    lconfig_id = lconfig_listsk->initEnum();
    lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
    while ( lconfig_dk != NULL ) {
      delete lconfig_dk;
      lconfig_listsk->removeFirstElement();
      lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
    }
    lconfig_listsk->closeEnum( lconfig_id );
    delete lconfig_listsk;
    lconfig_listsk = NULL;
  }
  lconfig_listcom.clear();
  lconfig_ignorelist.clear();
}

/*#define YYERROR_VERBOSE*/


#line 284 "configparser.cc"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_CONFIGPARSER_HH_INCLUDED
# define YY_YY_CONFIGPARSER_HH_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    LEFTBRACE_WCP = 258,           /* LEFTBRACE_WCP  */
    RIGHTBRACE_WCP = 259,          /* RIGHTBRACE_WCP  */
    GLOBAL_WCP = 260,              /* GLOBAL_WCP  */
    COLORS_WCP = 261,              /* COLORS_WCP  */
    LANG_WCP = 262,                /* LANG_WCP  */
    PALETTE_WCP = 263,             /* PALETTE_WCP  */
    OWNOP_WCP = 264,               /* OWNOP_WCP  */
    LISTERSETS_WCP = 265,          /* LISTERSETS_WCP  */
    YES_WCP = 266,                 /* YES_WCP  */
    NO_WCP = 267,                  /* NO_WCP  */
    LEFT_WCP = 268,                /* LEFT_WCP  */
    RIGHT_WCP = 269,               /* RIGHT_WCP  */
    HBARTOP_WCP = 270,             /* HBARTOP_WCP  */
    HBARHEIGHT_WCP = 271,          /* HBARHEIGHT_WCP  */
    VBARLEFT_WCP = 272,            /* VBARLEFT_WCP  */
    VBARWIDTH_WCP = 273,           /* VBARWIDTH_WCP  */
    DISPLAYSETS_WCP = 274,         /* DISPLAYSETS_WCP  */
    NAME_WCP = 275,                /* NAME_WCP  */
    SIZE_WCP = 276,                /* SIZE_WCP  */
    TYPE_WCP = 277,                /* TYPE_WCP  */
    PERMISSION_WCP = 278,          /* PERMISSION_WCP  */
    OWNER_WCP = 279,               /* OWNER_WCP  */
    DESTINATION_WCP = 280,         /* DESTINATION_WCP  */
    MODTIME_WCP = 281,             /* MODTIME_WCP  */
    ACCTIME_WCP = 282,             /* ACCTIME_WCP  */
    CHGTIME_WCP = 283,             /* CHGTIME_WCP  */
    ROWS_WCP = 284,                /* ROWS_WCP  */
    COLUMNS_WCP = 285,             /* COLUMNS_WCP  */
    CACHESIZE_WCP = 286,           /* CACHESIZE_WCP  */
    OWNERSTYLE_WCP = 287,          /* OWNERSTYLE_WCP  */
    TERMINAL_WCP = 288,            /* TERMINAL_WCP  */
    USESTRINGFORDIRSIZE_WCP = 289, /* USESTRINGFORDIRSIZE_WCP  */
    TIMESETS_WCP = 290,            /* TIMESETS_WCP  */
    STYLE1_WCP = 291,              /* STYLE1_WCP  */
    STYLE2_WCP = 292,              /* STYLE2_WCP  */
    STYLE3_WCP = 293,              /* STYLE3_WCP  */
    DATE_WCP = 294,                /* DATE_WCP  */
    TIME_WCP = 295,                /* TIME_WCP  */
    DATESUBSTITUTION_WCP = 296,    /* DATESUBSTITUTION_WCP  */
    DATEBEFORETIME_WCP = 297,      /* DATEBEFORETIME_WCP  */
    STATEBAR_WCP = 298,            /* STATEBAR_WCP  */
    SELLVBAR_WCP = 299,            /* SELLVBAR_WCP  */
    UNSELLVBAR_WCP = 300,          /* UNSELLVBAR_WCP  */
    CLOCKBAR_WCP = 301,            /* CLOCKBAR_WCP  */
    REQUESTER_WCP = 302,           /* REQUESTER_WCP  */
    UNSELDIR_WCP = 303,            /* UNSELDIR_WCP  */
    SELDIR_WCP = 304,              /* SELDIR_WCP  */
    UNSELFILE_WCP = 305,           /* UNSELFILE_WCP  */
    SELFILE_WCP = 306,             /* SELFILE_WCP  */
    UNSELACTDIR_WCP = 307,         /* UNSELACTDIR_WCP  */
    SELACTDIR_WCP = 308,           /* SELACTDIR_WCP  */
    UNSELACTFILE_WCP = 309,        /* UNSELACTFILE_WCP  */
    SELACTFILE_WCP = 310,          /* SELACTFILE_WCP  */
    LVBG_WCP = 311,                /* LVBG_WCP  */
    STARTUP_WCP = 312,             /* STARTUP_WCP  */
    BUTTON_WCP = 313,              /* BUTTON_WCP  */
    BUTTONS_WCP = 314,             /* BUTTONS_WCP  */
    POSITION_WCP = 315,            /* POSITION_WCP  */
    TITLE_WCP = 316,               /* TITLE_WCP  */
    COMMANDS_WCP = 317,            /* COMMANDS_WCP  */
    SHORTKEYS_WCP = 318,           /* SHORTKEYS_WCP  */
    PATHS_WCP = 319,               /* PATHS_WCP  */
    PATH_WCP = 320,                /* PATH_WCP  */
    FILETYPES_WCP = 321,           /* FILETYPES_WCP  */
    FILETYPE_WCP = 322,            /* FILETYPE_WCP  */
    NORMAL_WCP = 323,              /* NORMAL_WCP  */
    NOTYETCHECKED_WCP = 324,       /* NOTYETCHECKED_WCP  */
    UNKNOWN_WCP = 325,             /* UNKNOWN_WCP  */
    NOSELECT_WCP = 326,            /* NOSELECT_WCP  */
    DIR_WCP = 327,                 /* DIR_WCP  */
    USEPATTERN_WCP = 328,          /* USEPATTERN_WCP  */
    USECONTENT_WCP = 329,          /* USECONTENT_WCP  */
    PATTERN_WCP = 330,             /* PATTERN_WCP  */
    CONTENT_WCP = 331,             /* CONTENT_WCP  */
    FTCOMMANDS_WCP = 332,          /* FTCOMMANDS_WCP  */
    DND_WCP = 333,                 /* DND_WCP  */
    DC_WCP = 334,                  /* DC_WCP  */
    SHOW_WCP = 335,                /* SHOW_WCP  */
    RAWSHOW_WCP = 336,             /* RAWSHOW_WCP  */
    USER_WCP = 337,                /* USER_WCP  */
    FLAGS_WCP = 338,               /* FLAGS_WCP  */
    IGNOREDIRS_WCP = 339,          /* IGNOREDIRS_WCP  */
    HOTKEYS_WCP = 340,             /* HOTKEYS_WCP  */
    HOTKEY_WCP = 341,              /* HOTKEY_WCP  */
    FONTS_WCP = 342,               /* FONTS_WCP  */
    GLOBALFONT_WCP = 343,          /* GLOBALFONT_WCP  */
    BUTTONFONT_WCP = 344,          /* BUTTONFONT_WCP  */
    LEFTFONT_WCP = 345,            /* LEFTFONT_WCP  */
    RIGHTFONT_WCP = 346,           /* RIGHTFONT_WCP  */
    TEXTVIEWFONT_WCP = 347,        /* TEXTVIEWFONT_WCP  */
    STATEBARFONT_WCP = 348,        /* STATEBARFONT_WCP  */
    TEXTVIEWALTFONT_WCP = 349,     /* TEXTVIEWALTFONT_WCP  */
    CLOCKBARSETS_WCP = 350,        /* CLOCKBARSETS_WCP  */
    MODUS_WCP = 351,               /* MODUS_WCP  */
    TIMESPACE_WCP = 352,           /* TIMESPACE_WCP  */
    VERSION_WCP = 353,             /* VERSION_WCP  */
    EXTERN_WCP = 354,              /* EXTERN_WCP  */
    UPDATETIME_WCP = 355,          /* UPDATETIME_WCP  */
    PROGRAM_WCP = 356,             /* PROGRAM_WCP  */
    SHOWHINTS_WCP = 357,           /* SHOWHINTS_WCP  */
    KEY_WCP = 358,                 /* KEY_WCP  */
    DOUBLE_WCP = 359,              /* DOUBLE_WCP  */
    MOD_WCP = 360,                 /* MOD_WCP  */
    CONTROL_WCP = 361,             /* CONTROL_WCP  */
    SHIFT_WCP = 362,               /* SHIFT_WCP  */
    LOCK_WCP = 363,                /* LOCK_WCP  */
    MOD1_WCP = 364,                /* MOD1_WCP  */
    MOD2_WCP = 365,                /* MOD2_WCP  */
    MOD3_WCP = 366,                /* MOD3_WCP  */
    MOD4_WCP = 367,                /* MOD4_WCP  */
    MOD5_WCP = 368,                /* MOD5_WCP  */
    DNDACTION_WCP = 369,           /* DNDACTION_WCP  */
    DCACTION_WCP = 370,            /* DCACTION_WCP  */
    SHOWACTION_WCP = 371,          /* SHOWACTION_WCP  */
    RSHOWACTION_WCP = 372,         /* RSHOWACTION_WCP  */
    USERACTION_WCP = 373,          /* USERACTION_WCP  */
    ROWUP_WCP = 374,               /* ROWUP_WCP  */
    ROWDOWN_WCP = 375,             /* ROWDOWN_WCP  */
    CHANGEHIDDENFLAG_WCP = 376,    /* CHANGEHIDDENFLAG_WCP  */
    COPYOP_WCP = 377,              /* COPYOP_WCP  */
    FIRSTROW_WCP = 378,            /* FIRSTROW_WCP  */
    LASTROW_WCP = 379,             /* LASTROW_WCP  */
    PAGEUP_WCP = 380,              /* PAGEUP_WCP  */
    PAGEDOWN_WCP = 381,            /* PAGEDOWN_WCP  */
    SELECTOP_WCP = 382,            /* SELECTOP_WCP  */
    SELECTALLOP_WCP = 383,         /* SELECTALLOP_WCP  */
    SELECTNONEOP_WCP = 384,        /* SELECTNONEOP_WCP  */
    INVERTALLOP_WCP = 385,         /* INVERTALLOP_WCP  */
    PARENTDIROP_WCP = 386,         /* PARENTDIROP_WCP  */
    ENTERDIROP_WCP = 387,          /* ENTERDIROP_WCP  */
    CHANGELISTERSETOP_WCP = 388,   /* CHANGELISTERSETOP_WCP  */
    SWITCHLISTEROP_WCP = 389,      /* SWITCHLISTEROP_WCP  */
    FILTERSELECTOP_WCP = 390,      /* FILTERSELECTOP_WCP  */
    FILTERUNSELECTOP_WCP = 391,    /* FILTERUNSELECTOP_WCP  */
    PATHTOOTHERSIDEOP_WCP = 392,   /* PATHTOOTHERSIDEOP_WCP  */
    QUITOP_WCP = 393,              /* QUITOP_WCP  */
    DELETEOP_WCP = 394,            /* DELETEOP_WCP  */
    RELOADOP_WCP = 395,            /* RELOADOP_WCP  */
    MAKEDIROP_WCP = 396,           /* MAKEDIROP_WCP  */
    RENAMEOP_WCP = 397,            /* RENAMEOP_WCP  */
    DIRSIZEOP_WCP = 398,           /* DIRSIZEOP_WCP  */
    SIMDDOP_WCP = 399,             /* SIMDDOP_WCP  */
    STARTPROGOP_WCP = 400,         /* STARTPROGOP_WCP  */
    SEARCHENTRYOP_WCP = 401,       /* SEARCHENTRYOP_WCP  */
    ENTERPATHOP_WCP = 402,         /* ENTERPATHOP_WCP  */
    SCROLLLISTEROP_WCP = 403,      /* SCROLLLISTEROP_WCP  */
    CREATESYMLINKOP_WCP = 404,     /* CREATESYMLINKOP_WCP  */
    CHANGESYMLINKOP_WCP = 405,     /* CHANGESYMLINKOP_WCP  */
    CHMODOP_WCP = 406,             /* CHMODOP_WCP  */
    TOGGLELISTERMODEOP_WCP = 407,  /* TOGGLELISTERMODEOP_WCP  */
    SETSORTMODEOP_WCP = 408,       /* SETSORTMODEOP_WCP  */
    SETFILTEROP_WCP = 409,         /* SETFILTEROP_WCP  */
    SHORTKEYFROMLISTOP_WCP = 410,  /* SHORTKEYFROMLISTOP_WCP  */
    CHOWNOP_WCP = 411,             /* CHOWNOP_WCP  */
    WORKERCONFIG_WCP = 412,        /* WORKERCONFIG_WCP  */
    USERSTYLE_WCP = 413,           /* USERSTYLE_WCP  */
    DATESTRING_WCP = 414,          /* DATESTRING_WCP  */
    TIMESTRING_WCP = 415,          /* TIMESTRING_WCP  */
    COLOR_WCP = 416,               /* COLOR_WCP  */
    PATTERNIGNORECASE_WCP = 417,   /* PATTERNIGNORECASE_WCP  */
    PATTERNUSEREGEXP_WCP = 418,    /* PATTERNUSEREGEXP_WCP  */
    PATTERNUSEFULLNAME_WCP = 419,  /* PATTERNUSEFULLNAME_WCP  */
    COM_WCP = 420,                 /* COM_WCP  */
    SEPARATEEACHENTRY_WCP = 421,   /* SEPARATEEACHENTRY_WCP  */
    RECURSIVE_WCP = 422,           /* RECURSIVE_WCP  */
    START_WCP = 423,               /* START_WCP  */
    TERMINALWAIT_WCP = 424,        /* TERMINALWAIT_WCP  */
    SHOWOUTPUT_WCP = 425,          /* SHOWOUTPUT_WCP  */
    VIEWSTR_WCP = 426,             /* VIEWSTR_WCP  */
    SHOWOUTPUTINT_WCP = 427,       /* SHOWOUTPUTINT_WCP  */
    SHOWOUTPUTOTHERSIDE_WCP = 428, /* SHOWOUTPUTOTHERSIDE_WCP  */
    SHOWOUTPUTCUSTOMATTRIBUTE_WCP = 429, /* SHOWOUTPUTCUSTOMATTRIBUTE_WCP  */
    INBACKGROUND_WCP = 430,        /* INBACKGROUND_WCP  */
    TAKEDIRS_WCP = 431,            /* TAKEDIRS_WCP  */
    ACTIONNUMBER_WCP = 432,        /* ACTIONNUMBER_WCP  */
    HIDDENFILES_WCP = 433,         /* HIDDENFILES_WCP  */
    HIDE_WCP = 434,                /* HIDE_WCP  */
    TOGGLE_WCP = 435,              /* TOGGLE_WCP  */
    FOLLOWSYMLINKS_WCP = 436,      /* FOLLOWSYMLINKS_WCP  */
    MOVE_WCP = 437,                /* MOVE_WCP  */
    RENAME_WCP = 438,              /* RENAME_WCP  */
    SAMEDIR_WCP = 439,             /* SAMEDIR_WCP  */
    REQUESTDEST_WCP = 440,         /* REQUESTDEST_WCP  */
    REQUESTFLAGS_WCP = 441,        /* REQUESTFLAGS_WCP  */
    OVERWRITE_WCP = 442,           /* OVERWRITE_WCP  */
    ALWAYS_WCP = 443,              /* ALWAYS_WCP  */
    NEVER_WCP = 444,               /* NEVER_WCP  */
    COPYMODE_WCP = 445,            /* COPYMODE_WCP  */
    FAST_WCP = 446,                /* FAST_WCP  */
    PRESERVEATTR_WCP = 447,        /* PRESERVEATTR_WCP  */
    MODE_WCP = 448,                /* MODE_WCP  */
    ACTIVE_WCP = 449,              /* ACTIVE_WCP  */
    ACTIVE2OTHER_WCP = 450,        /* ACTIVE2OTHER_WCP  */
    SPECIAL_WCP = 451,             /* SPECIAL_WCP  */
    REQUEST_WCP = 452,             /* REQUEST_WCP  */
    CURRENT_WCP = 453,             /* CURRENT_WCP  */
    OTHER_WCP = 454,               /* OTHER_WCP  */
    FILTER_WCP = 455,              /* FILTER_WCP  */
    QUICK_WCP = 456,               /* QUICK_WCP  */
    ALSOACTIVE_WCP = 457,          /* ALSOACTIVE_WCP  */
    RESETDIRSIZES_WCP = 458,       /* RESETDIRSIZES_WCP  */
    KEEPFILETYPES_WCP = 459,       /* KEEPFILETYPES_WCP  */
    RELATIVE_WCP = 460,            /* RELATIVE_WCP  */
    ONFILES_WCP = 461,             /* ONFILES_WCP  */
    ONDIRS_WCP = 462,              /* ONDIRS_WCP  */
    SORTBY_WCP = 463,              /* SORTBY_WCP  */
    SORTFLAG_WCP = 464,            /* SORTFLAG_WCP  */
    REVERSE_WCP = 465,             /* REVERSE_WCP  */
    DIRLAST_WCP = 466,             /* DIRLAST_WCP  */
    DIRMIXED_WCP = 467,            /* DIRMIXED_WCP  */
    EXCLUDE_WCP = 468,             /* EXCLUDE_WCP  */
    INCLUDE_WCP = 469,             /* INCLUDE_WCP  */
    UNSET_WCP = 470,               /* UNSET_WCP  */
    UNSETALL_WCP = 471,            /* UNSETALL_WCP  */
    SCRIPTOP_WCP = 472,            /* SCRIPTOP_WCP  */
    NOP_WCP = 473,                 /* NOP_WCP  */
    PUSH_WCP = 474,                /* PUSH_WCP  */
    LABEL_WCP = 475,               /* LABEL_WCP  */
    IF_WCP = 476,                  /* IF_WCP  */
    END_WCP = 477,                 /* END_WCP  */
    POP_WCP = 478,                 /* POP_WCP  */
    SETTINGS_WCP = 479,            /* SETTINGS_WCP  */
    WINDOW_WCP = 480,              /* WINDOW_WCP  */
    GOTO_WCP = 481,                /* GOTO_WCP  */
    PUSHUSEOUTPUT_WCP = 482,       /* PUSHUSEOUTPUT_WCP  */
    DODEBUG_WCP = 483,             /* DODEBUG_WCP  */
    WPURECURSIVE_WCP = 484,        /* WPURECURSIVE_WCP  */
    WPUTAKEDIRS_WCP = 485,         /* WPUTAKEDIRS_WCP  */
    STACKNR_WCP = 486,             /* STACKNR_WCP  */
    PUSHSTRING_WCP = 487,          /* PUSHSTRING_WCP  */
    PUSHOUTPUTRETURNCODE_WCP = 488, /* PUSHOUTPUTRETURNCODE_WCP  */
    IFTEST_WCP = 489,              /* IFTEST_WCP  */
    IFLABEL_WCP = 490,             /* IFLABEL_WCP  */
    WINTYPE_WCP = 491,             /* WINTYPE_WCP  */
    OPEN_WCP = 492,                /* OPEN_WCP  */
    CLOSE_WCP = 493,               /* CLOSE_WCP  */
    LEAVE_WCP = 494,               /* LEAVE_WCP  */
    CHANGEPROGRESS_WCP = 495,      /* CHANGEPROGRESS_WCP  */
    CHANGETEXT_WCP = 496,          /* CHANGETEXT_WCP  */
    PROGRESSUSEOUTPUT_WCP = 497,   /* PROGRESSUSEOUTPUT_WCP  */
    WINTEXTUSEOUTPUT_WCP = 498,    /* WINTEXTUSEOUTPUT_WCP  */
    PROGRESS_WCP = 499,            /* PROGRESS_WCP  */
    WINTEXT_WCP = 500,             /* WINTEXT_WCP  */
    SHOWDIRCACHEOP_WCP = 501,      /* SHOWDIRCACHEOP_WCP  */
    INODE_WCP = 502,               /* INODE_WCP  */
    NLINK_WCP = 503,               /* NLINK_WCP  */
    BLOCKS_WCP = 504,              /* BLOCKS_WCP  */
    SHOWHEADER_WCP = 505,          /* SHOWHEADER_WCP  */
    LVHEADER_WCP = 506,            /* LVHEADER_WCP  */
    IGNORECASE_WCP = 507,          /* IGNORECASE_WCP  */
    LISTVIEWS_WCP = 508,           /* LISTVIEWS_WCP  */
    BLL_WCP = 509,                 /* BLL_WCP  */
    LBL_WCP = 510,                 /* LBL_WCP  */
    LLB_WCP = 511,                 /* LLB_WCP  */
    BL_WCP = 512,                  /* BL_WCP  */
    LB_WCP = 513,                  /* LB_WCP  */
    LAYOUT_WCP = 514,              /* LAYOUT_WCP  */
    BUTTONSVERT_WCP = 515,         /* BUTTONSVERT_WCP  */
    LISTVIEWSVERT_WCP = 516,       /* LISTVIEWSVERT_WCP  */
    LISTVIEWWEIGHT_WCP = 517,      /* LISTVIEWWEIGHT_WCP  */
    WEIGHTTOACTIVE_WCP = 518,      /* WEIGHTTOACTIVE_WCP  */
    EXTCOND_WCP = 519,             /* EXTCOND_WCP  */
    USEEXTCOND_WCP = 520,          /* USEEXTCOND_WCP  */
    SUBTYPE_WCP = 521,             /* SUBTYPE_WCP  */
    PARENTACTIONOP_WCP = 522,      /* PARENTACTIONOP_WCP  */
    NOOPERATIONOP_WCP = 523,       /* NOOPERATIONOP_WCP  */
    COLORMODE_WCP = 524,           /* COLORMODE_WCP  */
    DEFAULT_WCP = 525,             /* DEFAULT_WCP  */
    CUSTOM_WCP = 526,              /* CUSTOM_WCP  */
    UNSELECTCOLOR_WCP = 527,       /* UNSELECTCOLOR_WCP  */
    SELECTCOLOR_WCP = 528,         /* SELECTCOLOR_WCP  */
    UNSELECTACTIVECOLOR_WCP = 529, /* UNSELECTACTIVECOLOR_WCP  */
    SELECTACTIVECOLOR_WCP = 530,   /* SELECTACTIVECOLOR_WCP  */
    COLOREXTERNPROG_WCP = 531,     /* COLOREXTERNPROG_WCP  */
    PARENT_WCP = 532,              /* PARENT_WCP  */
    DONTCD_WCP = 533,              /* DONTCD_WCP  */
    DONTCHECKVIRTUAL_WCP = 534,    /* DONTCHECKVIRTUAL_WCP  */
    GOFTPOP_WCP = 535,             /* GOFTPOP_WCP  */
    HOSTNAME_WCP = 536,            /* HOSTNAME_WCP  */
    USERNAME_WCP = 537,            /* USERNAME_WCP  */
    PASSWORD_WCP = 538,            /* PASSWORD_WCP  */
    DONTENTERFTP_WCP = 539,        /* DONTENTERFTP_WCP  */
    ALWAYSSTOREPW_WCP = 540,       /* ALWAYSSTOREPW_WCP  */
    INTERNALVIEWOP_WCP = 541,      /* INTERNALVIEWOP_WCP  */
    CUSTOMFILES_WCP = 542,         /* CUSTOMFILES_WCP  */
    SHOWMODE_WCP = 543,            /* SHOWMODE_WCP  */
    SELECTED_WCP = 544,            /* SELECTED_WCP  */
    REVERSESEARCH_WCP = 545,       /* REVERSESEARCH_WCP  */
    MOUSECONF_WCP = 546,           /* MOUSECONF_WCP  */
    SELECTBUTTON_WCP = 547,        /* SELECTBUTTON_WCP  */
    ACTIVATEBUTTON_WCP = 548,      /* ACTIVATEBUTTON_WCP  */
    SCROLLBUTTON_WCP = 549,        /* SCROLLBUTTON_WCP  */
    SELECTMETHOD_WCP = 550,        /* SELECTMETHOD_WCP  */
    ALTERNATIVE_WCP = 551,         /* ALTERNATIVE_WCP  */
    DELAYEDCONTEXTMENU_WCP = 552,  /* DELAYEDCONTEXTMENU_WCP  */
    SEARCHOP_WCP = 553,            /* SEARCHOP_WCP  */
    EDITCOMMAND_WCP = 554,         /* EDITCOMMAND_WCP  */
    SHOWPREVRESULTS_WCP = 555,     /* SHOWPREVRESULTS_WCP  */
    TEXTVIEW_WCP = 556,            /* TEXTVIEW_WCP  */
    TEXTVIEWHIGHLIGHTED_WCP = 557, /* TEXTVIEWHIGHLIGHTED_WCP  */
    TEXTVIEWSELECTION_WCP = 558,   /* TEXTVIEWSELECTION_WCP  */
    DIRBOOKMARKOP_WCP = 559,       /* DIRBOOKMARKOP_WCP  */
    OPENCONTEXTMENUOP_WCP = 560,   /* OPENCONTEXTMENUOP_WCP  */
    RUNCUSTOMACTION_WCP = 561,     /* RUNCUSTOMACTION_WCP  */
    CUSTOMNAME_WCP = 562,          /* CUSTOMNAME_WCP  */
    CONTEXTBUTTON_WCP = 563,       /* CONTEXTBUTTON_WCP  */
    ACTIVATEMOD_WCP = 564,         /* ACTIVATEMOD_WCP  */
    SCROLLMOD_WCP = 565,           /* SCROLLMOD_WCP  */
    CONTEXTMOD_WCP = 566,          /* CONTEXTMOD_WCP  */
    NONE_WCP = 567,                /* NONE_WCP  */
    CUSTOMACTION_WCP = 568,        /* CUSTOMACTION_WCP  */
    SAVE_WORKER_STATE_ON_EXIT_WCP = 569, /* SAVE_WORKER_STATE_ON_EXIT_WCP  */
    OPENWORKERMENUOP_WCP = 570,    /* OPENWORKERMENUOP_WCP  */
    CHANGELABELOP_WCP = 571,       /* CHANGELABELOP_WCP  */
    ASKFORLABEL_WCP = 572,         /* ASKFORLABEL_WCP  */
    LABELCOLORS_WCP = 573,         /* LABELCOLORS_WCP  */
    BOOKMARKLABEL_WCP = 574,       /* BOOKMARKLABEL_WCP  */
    BOOKMARKFILTER_WCP = 575,      /* BOOKMARKFILTER_WCP  */
    SHOWONLYBOOKMARKS_WCP = 576,   /* SHOWONLYBOOKMARKS_WCP  */
    SHOWONLYLABEL_WCP = 577,       /* SHOWONLYLABEL_WCP  */
    SHOWALL_WCP = 578,             /* SHOWALL_WCP  */
    OPTIONMODE_WCP = 579,          /* OPTIONMODE_WCP  */
    INVERT_WCP = 580,              /* INVERT_WCP  */
    SET_WCP = 581,                 /* SET_WCP  */
    CHANGEFILTERS_WCP = 582,       /* CHANGEFILTERS_WCP  */
    CHANGEBOOKMARKS_WCP = 583,     /* CHANGEBOOKMARKS_WCP  */
    QUERYLABEL_WCP = 584,          /* QUERYLABEL_WCP  */
    MODIFYTABSOP_WCP = 585,        /* MODIFYTABSOP_WCP  */
    TABACTION_WCP = 586,           /* TABACTION_WCP  */
    NEWTAB_WCP = 587,              /* NEWTAB_WCP  */
    CLOSECURRENTTAB_WCP = 588,     /* CLOSECURRENTTAB_WCP  */
    NEXTTAB_WCP = 589,             /* NEXTTAB_WCP  */
    PREVTAB_WCP = 590,             /* PREVTAB_WCP  */
    TOGGLELOCKSTATE_WCP = 591,     /* TOGGLELOCKSTATE_WCP  */
    MOVETABLEFT_WCP = 592,         /* MOVETABLEFT_WCP  */
    MOVETABRIGHT_WCP = 593,        /* MOVETABRIGHT_WCP  */
    CHANGELAYOUTOP_WCP = 594,      /* CHANGELAYOUTOP_WCP  */
    INFIXSEARCH_WCP = 595,         /* INFIXSEARCH_WCP  */
    VOLUMEMANAGEROP_WCP = 596,     /* VOLUMEMANAGEROP_WCP  */
    ERROR = 597,                   /* ERROR  */
    ACTIVESIDE_WCP = 598,          /* ACTIVESIDE_WCP  */
    SHOWFREESPACE_WCP = 599,       /* SHOWFREESPACE_WCP  */
    ACTIVEMODE_WCP = 600,          /* ACTIVEMODE_WCP  */
    ASK_WCP = 601,                 /* ASK_WCP  */
    SSHALLOW_WCP = 602,            /* SSHALLOW_WCP  */
    FIELD_WIDTH_WCP = 603,         /* FIELD_WIDTH_WCP  */
    QUICKSEARCHENABLED_WCP = 604,  /* QUICKSEARCHENABLED_WCP  */
    FILTEREDSEARCHENABLED_WCP = 605, /* FILTEREDSEARCHENABLED_WCP  */
    XFTFONTS_WCP = 606,            /* XFTFONTS_WCP  */
    USEMAGIC_WCP = 607,            /* USEMAGIC_WCP  */
    MAGICPATTERN_WCP = 608,        /* MAGICPATTERN_WCP  */
    MAGICCOMPRESSED_WCP = 609,     /* MAGICCOMPRESSED_WCP  */
    MAGICMIME_WCP = 610,           /* MAGICMIME_WCP  */
    VOLUMEMANAGER_WCP = 611,       /* VOLUMEMANAGER_WCP  */
    MOUNTCOMMAND_WCP = 612,        /* MOUNTCOMMAND_WCP  */
    UNMOUNTCOMMAND_WCP = 613,      /* UNMOUNTCOMMAND_WCP  */
    FSTABFILE_WCP = 614,           /* FSTABFILE_WCP  */
    MTABFILE_WCP = 615,            /* MTABFILE_WCP  */
    PARTFILE_WCP = 616,            /* PARTFILE_WCP  */
    REQUESTACTION_WCP = 617,       /* REQUESTACTION_WCP  */
    SIZEH_WCP = 618,               /* SIZEH_WCP  */
    NEXTAGING_WCP = 619,           /* NEXTAGING_WCP  */
    ENTRY_WCP = 620,               /* ENTRY_WCP  */
    PREFIX_WCP = 621,              /* PREFIX_WCP  */
    VALUE_WCP = 622,               /* VALUE_WCP  */
    EXTENSION_WCP = 623,           /* EXTENSION_WCP  */
    EJECTCOMMAND_WCP = 624,        /* EJECTCOMMAND_WCP  */
    CLOSETRAYCOMMAND_WCP = 625,    /* CLOSETRAYCOMMAND_WCP  */
    AVFSMODULE_WCP = 626,          /* AVFSMODULE_WCP  */
    FLEXIBLEMATCH_WCP = 627,       /* FLEXIBLEMATCH_WCP  */
    USE_VERSION_STRING_COMPARE_WCP = 628, /* USE_VERSION_STRING_COMPARE_WCP  */
    SWITCHBUTTONBANKOP_WCP = 629,  /* SWITCHBUTTONBANKOP_WCP  */
    SWITCHTONEXTBANK_WCP = 630,    /* SWITCHTONEXTBANK_WCP  */
    SWITCHTOPREVBANK_WCP = 631,    /* SWITCHTOPREVBANK_WCP  */
    SWITCHTOBANKNR_WCP = 632,      /* SWITCHTOBANKNR_WCP  */
    BANKNR_WCP = 633,              /* BANKNR_WCP  */
    USE_VIRTUAL_TEMP_COPIES_WCP = 634, /* USE_VIRTUAL_TEMP_COPIES_WCP  */
    PATHJUMPOP_WCP = 635,          /* PATHJUMPOP_WCP  */
    PATHJUMPALLOWDIRS_WCP = 636,   /* PATHJUMPALLOWDIRS_WCP  */
    CLIPBOARDOP_WCP = 637,         /* CLIPBOARDOP_WCP  */
    CLIPBOARDSTRING_WCP = 638,     /* CLIPBOARDSTRING_WCP  */
    COMMANDSTRING_WCP = 639,       /* COMMANDSTRING_WCP  */
    EVALCOMMAND_WCP = 640,         /* EVALCOMMAND_WCP  */
    WATCHMODE_WCP = 641,           /* WATCHMODE_WCP  */
    APPLY_WINDOW_DIALOG_TYPE_WCP = 642, /* APPLY_WINDOW_DIALOG_TYPE_WCP  */
    FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP = 643, /* FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP  */
    COMMANDMENUOP_WCP = 644,       /* COMMANDMENUOP_WCP  */
    SEARCHMODEONKEYPRESS_WCP = 645, /* SEARCHMODEONKEYPRESS_WCP  */
    PATHENTRYONTOP_WCP = 646,      /* PATHENTRYONTOP_WCP  */
    STORE_FILES_ALWAYS_WCP = 647,  /* STORE_FILES_ALWAYS_WCP  */
    PATHJUMPSETS_WCP = 648,        /* PATHJUMPSETS_WCP  */
    SHOWDOTDOT_WCP = 649,          /* SHOWDOTDOT_WCP  */
    SHOWBREADCRUMB_WCP = 650,      /* SHOWBREADCRUMB_WCP  */
    FACES_WCP = 651,               /* FACES_WCP  */
    FACE_WCP = 652,                /* FACE_WCP  */
    ENABLE_INFO_LINE_WCP = 653,    /* ENABLE_INFO_LINE_WCP  */
    INFO_LINE_LUA_MODE_WCP = 654,  /* INFO_LINE_LUA_MODE_WCP  */
    INFO_LINE_CONTENT_WCP = 655,   /* INFO_LINE_CONTENT_WCP  */
    RESTORE_TABS_MODE_WCP = 656,   /* RESTORE_TABS_MODE_WCP  */
    STORE_TABS_MODE_WCP = 657,     /* STORE_TABS_MODE_WCP  */
    AS_EXIT_STATE_WCP = 658,       /* AS_EXIT_STATE_WCP  */
    USE_EXTENDED_REGEX_WCP = 659,  /* USE_EXTENDED_REGEX_WCP  */
    HIGHLIGHT_USER_ACTION_WCP = 660, /* HIGHLIGHT_USER_ACTION_WCP  */
    CHTIMEOP_WCP = 661,            /* CHTIMEOP_WCP  */
    ADJUSTRELATIVESYMLINKS_WCP = 662, /* ADJUSTRELATIVESYMLINKS_WCP  */
    OUTSIDE_WCP = 663,             /* OUTSIDE_WCP  */
    EDIT_WCP = 664,                /* EDIT_WCP  */
    MAKEABSOLUTE_WCP = 665,        /* MAKEABSOLUTE_WCP  */
    MAKERELATIVE_WCP = 666,        /* MAKERELATIVE_WCP  */
    MAGICIGNORECASE_WCP = 667,     /* MAGICIGNORECASE_WCP  */
    ENSURE_FILE_PERMISSIONS_WCP = 668, /* ENSURE_FILE_PERMISSIONS_WCP  */
    USER_RW_WCP = 669,             /* USER_RW_WCP  */
    USER_RW_GROUP_R_WCP = 670,     /* USER_RW_GROUP_R_WCP  */
    USER_RW_ALL_R_WCP = 671,       /* USER_RW_ALL_R_WCP  */
    LEAVE_UNMODIFIED_WCP = 672,    /* LEAVE_UNMODIFIED_WCP  */
    PREFERUDISKSVERSION_WCP = 673, /* PREFERUDISKSVERSION_WCP  */
    CHANGECOLUMNSOP_WCP = 674,     /* CHANGECOLUMNSOP_WCP  */
    PATTERNISCOMMASEPARATED_WCP = 675, /* PATTERNISCOMMASEPARATED_WCP  */
    STRCASECMP_WCP = 676,          /* STRCASECMP_WCP  */
    USE_STRING_COMPARE_MODE_WCP = 677, /* USE_STRING_COMPARE_MODE_WCP  */
    VIEWNEWESTFILESOP_WCP = 678,   /* VIEWNEWESTFILESOP_WCP  */
    SHOWRECENT_WCP = 679,          /* SHOWRECENT_WCP  */
    DIRCOMPAREOP_WCP = 680,        /* DIRCOMPAREOP_WCP  */
    TABPROFILESOP_WCP = 681,       /* TABPROFILESOP_WCP  */
    EXTERNALVDIROP_WCP = 682,      /* EXTERNALVDIROP_WCP  */
    VDIR_PRESERVE_DIR_STRUCTURE_WCP = 683, /* VDIR_PRESERVE_DIR_STRUCTURE_WCP  */
    OPENTABMENUOP_WCP = 684,       /* OPENTABMENUOP_WCP  */
    INITIALTAB_WCP = 685,          /* INITIALTAB_WCP  */
    SHOWBYTIME_WCP = 686,          /* SHOWBYTIME_WCP  */
    SHOWBYFILTER_WCP = 687,        /* SHOWBYFILTER_WCP  */
    SHOWBYPROGRAM_WCP = 688,       /* SHOWBYPROGRAM_WCP  */
    TERMINAL_RETURNS_EARLY_WCP = 689, /* TERMINAL_RETURNS_EARLY_WCP  */
    DIRECTORYPRESETS_WCP = 690,    /* DIRECTORYPRESETS_WCP  */
    DIRECTORYPRESET_WCP = 691,     /* DIRECTORYPRESET_WCP  */
    HELPOP_WCP = 692,              /* HELPOP_WCP  */
    PRIORITY_WCP = 693,            /* PRIORITY_WCP  */
    AUTOFILTER_WCP = 694,          /* AUTOFILTER_WCP  */
    ENABLE_CUSTOM_LVB_LINE_WCP = 695, /* ENABLE_CUSTOM_LVB_LINE_WCP  */
    CUSTOM_LVB_LINE_WCP = 696,     /* CUSTOM_LVB_LINE_WCP  */
    CUSTOM_DIRECTORY_INFO_COMMAND_WCP = 697, /* CUSTOM_DIRECTORY_INFO_COMMAND_WCP  */
    IMMEDIATE_FILTER_APPLY_WCP = 698, /* IMMEDIATE_FILTER_APPLY_WCP  */
    DISABLEBGCHECKPREFIX_WCP = 699, /* DISABLEBGCHECKPREFIX_WCP  */
    DISPLAYMODE_WCP = 700,         /* DISPLAYMODE_WCP  */
    SHOWSUBDIRS_WCP = 701,         /* SHOWSUBDIRS_WCP  */
    SHOWDIRECTSUBDIRS_WCP = 702,   /* SHOWDIRECTSUBDIRS_WCP  */
    INCLUDEALL_WCP = 703,          /* INCLUDEALL_WCP  */
    APPLYMODE_WCP = 704,           /* APPLYMODE_WCP  */
    ADD_WCP = 705,                 /* ADD_WCP  */
    REMOVE_WCP = 706,              /* REMOVE_WCP  */
    HIDENONEXISTING_WCP = 707,     /* HIDENONEXISTING_WCP  */
    VIEWCOMMANDLOGOP_WCP = 708,    /* VIEWCOMMANDLOGOP_WCP  */
    LOCKONCURRENTDIR_WCP = 709,    /* LOCKONCURRENTDIR_WCP  */
    STARTNODE_WCP = 710,           /* STARTNODE_WCP  */
    MENUS_WCP = 711,               /* MENUS_WCP  */
    LVMODES_WCP = 712,             /* LVMODES_WCP  */
    LVCOMMANDS_WCP = 713,          /* LVCOMMANDS_WCP  */
    TOPLEVEL_WCP = 714,            /* TOPLEVEL_WCP  */
    FOLLOWACTIVE_WCP = 715,        /* FOLLOWACTIVE_WCP  */
    WATCHFILE_WCP = 716,           /* WATCHFILE_WCP  */
    ACTIVATETEXTVIEWMODEOP_WCP = 717, /* ACTIVATETEXTVIEWMODEOP_WCP  */
    SOURCEMODE_WCP = 718,          /* SOURCEMODE_WCP  */
    ACTIVEFILE_WCP = 719,          /* ACTIVEFILE_WCP  */
    FILETYPEACTION_WCP = 720,      /* FILETYPEACTION_WCP  */
    IGNOREDIRS2_WCP = 721,         /* IGNOREDIRS2_WCP  */
    IGNOREDIR_WCP = 722,           /* IGNOREDIR_WCP  */
    CUSTOMATTRIBUTE_WCP = 723,     /* CUSTOMATTRIBUTE_WCP  */
    STARTSEARCHSTRING_WCP = 724,   /* STARTSEARCHSTRING_WCP  */
    APPLYONLY_WCP = 725,           /* APPLYONLY_WCP  */
    USESELECTEDENTRIES_WCP = 726,  /* USESELECTEDENTRIES_WCP  */
    ONLYEXACTPATH_WCP = 727,       /* ONLYEXACTPATH_WCP  */
    ALSOSELECTEXTENSION_WCP = 728, /* ALSOSELECTEXTENSION_WCP  */
    STRING_WCP = 729,              /* STRING_WCP  */
    NUM_WCP = 730                  /* NUM_WCP  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define LEFTBRACE_WCP 258
#define RIGHTBRACE_WCP 259
#define GLOBAL_WCP 260
#define COLORS_WCP 261
#define LANG_WCP 262
#define PALETTE_WCP 263
#define OWNOP_WCP 264
#define LISTERSETS_WCP 265
#define YES_WCP 266
#define NO_WCP 267
#define LEFT_WCP 268
#define RIGHT_WCP 269
#define HBARTOP_WCP 270
#define HBARHEIGHT_WCP 271
#define VBARLEFT_WCP 272
#define VBARWIDTH_WCP 273
#define DISPLAYSETS_WCP 274
#define NAME_WCP 275
#define SIZE_WCP 276
#define TYPE_WCP 277
#define PERMISSION_WCP 278
#define OWNER_WCP 279
#define DESTINATION_WCP 280
#define MODTIME_WCP 281
#define ACCTIME_WCP 282
#define CHGTIME_WCP 283
#define ROWS_WCP 284
#define COLUMNS_WCP 285
#define CACHESIZE_WCP 286
#define OWNERSTYLE_WCP 287
#define TERMINAL_WCP 288
#define USESTRINGFORDIRSIZE_WCP 289
#define TIMESETS_WCP 290
#define STYLE1_WCP 291
#define STYLE2_WCP 292
#define STYLE3_WCP 293
#define DATE_WCP 294
#define TIME_WCP 295
#define DATESUBSTITUTION_WCP 296
#define DATEBEFORETIME_WCP 297
#define STATEBAR_WCP 298
#define SELLVBAR_WCP 299
#define UNSELLVBAR_WCP 300
#define CLOCKBAR_WCP 301
#define REQUESTER_WCP 302
#define UNSELDIR_WCP 303
#define SELDIR_WCP 304
#define UNSELFILE_WCP 305
#define SELFILE_WCP 306
#define UNSELACTDIR_WCP 307
#define SELACTDIR_WCP 308
#define UNSELACTFILE_WCP 309
#define SELACTFILE_WCP 310
#define LVBG_WCP 311
#define STARTUP_WCP 312
#define BUTTON_WCP 313
#define BUTTONS_WCP 314
#define POSITION_WCP 315
#define TITLE_WCP 316
#define COMMANDS_WCP 317
#define SHORTKEYS_WCP 318
#define PATHS_WCP 319
#define PATH_WCP 320
#define FILETYPES_WCP 321
#define FILETYPE_WCP 322
#define NORMAL_WCP 323
#define NOTYETCHECKED_WCP 324
#define UNKNOWN_WCP 325
#define NOSELECT_WCP 326
#define DIR_WCP 327
#define USEPATTERN_WCP 328
#define USECONTENT_WCP 329
#define PATTERN_WCP 330
#define CONTENT_WCP 331
#define FTCOMMANDS_WCP 332
#define DND_WCP 333
#define DC_WCP 334
#define SHOW_WCP 335
#define RAWSHOW_WCP 336
#define USER_WCP 337
#define FLAGS_WCP 338
#define IGNOREDIRS_WCP 339
#define HOTKEYS_WCP 340
#define HOTKEY_WCP 341
#define FONTS_WCP 342
#define GLOBALFONT_WCP 343
#define BUTTONFONT_WCP 344
#define LEFTFONT_WCP 345
#define RIGHTFONT_WCP 346
#define TEXTVIEWFONT_WCP 347
#define STATEBARFONT_WCP 348
#define TEXTVIEWALTFONT_WCP 349
#define CLOCKBARSETS_WCP 350
#define MODUS_WCP 351
#define TIMESPACE_WCP 352
#define VERSION_WCP 353
#define EXTERN_WCP 354
#define UPDATETIME_WCP 355
#define PROGRAM_WCP 356
#define SHOWHINTS_WCP 357
#define KEY_WCP 358
#define DOUBLE_WCP 359
#define MOD_WCP 360
#define CONTROL_WCP 361
#define SHIFT_WCP 362
#define LOCK_WCP 363
#define MOD1_WCP 364
#define MOD2_WCP 365
#define MOD3_WCP 366
#define MOD4_WCP 367
#define MOD5_WCP 368
#define DNDACTION_WCP 369
#define DCACTION_WCP 370
#define SHOWACTION_WCP 371
#define RSHOWACTION_WCP 372
#define USERACTION_WCP 373
#define ROWUP_WCP 374
#define ROWDOWN_WCP 375
#define CHANGEHIDDENFLAG_WCP 376
#define COPYOP_WCP 377
#define FIRSTROW_WCP 378
#define LASTROW_WCP 379
#define PAGEUP_WCP 380
#define PAGEDOWN_WCP 381
#define SELECTOP_WCP 382
#define SELECTALLOP_WCP 383
#define SELECTNONEOP_WCP 384
#define INVERTALLOP_WCP 385
#define PARENTDIROP_WCP 386
#define ENTERDIROP_WCP 387
#define CHANGELISTERSETOP_WCP 388
#define SWITCHLISTEROP_WCP 389
#define FILTERSELECTOP_WCP 390
#define FILTERUNSELECTOP_WCP 391
#define PATHTOOTHERSIDEOP_WCP 392
#define QUITOP_WCP 393
#define DELETEOP_WCP 394
#define RELOADOP_WCP 395
#define MAKEDIROP_WCP 396
#define RENAMEOP_WCP 397
#define DIRSIZEOP_WCP 398
#define SIMDDOP_WCP 399
#define STARTPROGOP_WCP 400
#define SEARCHENTRYOP_WCP 401
#define ENTERPATHOP_WCP 402
#define SCROLLLISTEROP_WCP 403
#define CREATESYMLINKOP_WCP 404
#define CHANGESYMLINKOP_WCP 405
#define CHMODOP_WCP 406
#define TOGGLELISTERMODEOP_WCP 407
#define SETSORTMODEOP_WCP 408
#define SETFILTEROP_WCP 409
#define SHORTKEYFROMLISTOP_WCP 410
#define CHOWNOP_WCP 411
#define WORKERCONFIG_WCP 412
#define USERSTYLE_WCP 413
#define DATESTRING_WCP 414
#define TIMESTRING_WCP 415
#define COLOR_WCP 416
#define PATTERNIGNORECASE_WCP 417
#define PATTERNUSEREGEXP_WCP 418
#define PATTERNUSEFULLNAME_WCP 419
#define COM_WCP 420
#define SEPARATEEACHENTRY_WCP 421
#define RECURSIVE_WCP 422
#define START_WCP 423
#define TERMINALWAIT_WCP 424
#define SHOWOUTPUT_WCP 425
#define VIEWSTR_WCP 426
#define SHOWOUTPUTINT_WCP 427
#define SHOWOUTPUTOTHERSIDE_WCP 428
#define SHOWOUTPUTCUSTOMATTRIBUTE_WCP 429
#define INBACKGROUND_WCP 430
#define TAKEDIRS_WCP 431
#define ACTIONNUMBER_WCP 432
#define HIDDENFILES_WCP 433
#define HIDE_WCP 434
#define TOGGLE_WCP 435
#define FOLLOWSYMLINKS_WCP 436
#define MOVE_WCP 437
#define RENAME_WCP 438
#define SAMEDIR_WCP 439
#define REQUESTDEST_WCP 440
#define REQUESTFLAGS_WCP 441
#define OVERWRITE_WCP 442
#define ALWAYS_WCP 443
#define NEVER_WCP 444
#define COPYMODE_WCP 445
#define FAST_WCP 446
#define PRESERVEATTR_WCP 447
#define MODE_WCP 448
#define ACTIVE_WCP 449
#define ACTIVE2OTHER_WCP 450
#define SPECIAL_WCP 451
#define REQUEST_WCP 452
#define CURRENT_WCP 453
#define OTHER_WCP 454
#define FILTER_WCP 455
#define QUICK_WCP 456
#define ALSOACTIVE_WCP 457
#define RESETDIRSIZES_WCP 458
#define KEEPFILETYPES_WCP 459
#define RELATIVE_WCP 460
#define ONFILES_WCP 461
#define ONDIRS_WCP 462
#define SORTBY_WCP 463
#define SORTFLAG_WCP 464
#define REVERSE_WCP 465
#define DIRLAST_WCP 466
#define DIRMIXED_WCP 467
#define EXCLUDE_WCP 468
#define INCLUDE_WCP 469
#define UNSET_WCP 470
#define UNSETALL_WCP 471
#define SCRIPTOP_WCP 472
#define NOP_WCP 473
#define PUSH_WCP 474
#define LABEL_WCP 475
#define IF_WCP 476
#define END_WCP 477
#define POP_WCP 478
#define SETTINGS_WCP 479
#define WINDOW_WCP 480
#define GOTO_WCP 481
#define PUSHUSEOUTPUT_WCP 482
#define DODEBUG_WCP 483
#define WPURECURSIVE_WCP 484
#define WPUTAKEDIRS_WCP 485
#define STACKNR_WCP 486
#define PUSHSTRING_WCP 487
#define PUSHOUTPUTRETURNCODE_WCP 488
#define IFTEST_WCP 489
#define IFLABEL_WCP 490
#define WINTYPE_WCP 491
#define OPEN_WCP 492
#define CLOSE_WCP 493
#define LEAVE_WCP 494
#define CHANGEPROGRESS_WCP 495
#define CHANGETEXT_WCP 496
#define PROGRESSUSEOUTPUT_WCP 497
#define WINTEXTUSEOUTPUT_WCP 498
#define PROGRESS_WCP 499
#define WINTEXT_WCP 500
#define SHOWDIRCACHEOP_WCP 501
#define INODE_WCP 502
#define NLINK_WCP 503
#define BLOCKS_WCP 504
#define SHOWHEADER_WCP 505
#define LVHEADER_WCP 506
#define IGNORECASE_WCP 507
#define LISTVIEWS_WCP 508
#define BLL_WCP 509
#define LBL_WCP 510
#define LLB_WCP 511
#define BL_WCP 512
#define LB_WCP 513
#define LAYOUT_WCP 514
#define BUTTONSVERT_WCP 515
#define LISTVIEWSVERT_WCP 516
#define LISTVIEWWEIGHT_WCP 517
#define WEIGHTTOACTIVE_WCP 518
#define EXTCOND_WCP 519
#define USEEXTCOND_WCP 520
#define SUBTYPE_WCP 521
#define PARENTACTIONOP_WCP 522
#define NOOPERATIONOP_WCP 523
#define COLORMODE_WCP 524
#define DEFAULT_WCP 525
#define CUSTOM_WCP 526
#define UNSELECTCOLOR_WCP 527
#define SELECTCOLOR_WCP 528
#define UNSELECTACTIVECOLOR_WCP 529
#define SELECTACTIVECOLOR_WCP 530
#define COLOREXTERNPROG_WCP 531
#define PARENT_WCP 532
#define DONTCD_WCP 533
#define DONTCHECKVIRTUAL_WCP 534
#define GOFTPOP_WCP 535
#define HOSTNAME_WCP 536
#define USERNAME_WCP 537
#define PASSWORD_WCP 538
#define DONTENTERFTP_WCP 539
#define ALWAYSSTOREPW_WCP 540
#define INTERNALVIEWOP_WCP 541
#define CUSTOMFILES_WCP 542
#define SHOWMODE_WCP 543
#define SELECTED_WCP 544
#define REVERSESEARCH_WCP 545
#define MOUSECONF_WCP 546
#define SELECTBUTTON_WCP 547
#define ACTIVATEBUTTON_WCP 548
#define SCROLLBUTTON_WCP 549
#define SELECTMETHOD_WCP 550
#define ALTERNATIVE_WCP 551
#define DELAYEDCONTEXTMENU_WCP 552
#define SEARCHOP_WCP 553
#define EDITCOMMAND_WCP 554
#define SHOWPREVRESULTS_WCP 555
#define TEXTVIEW_WCP 556
#define TEXTVIEWHIGHLIGHTED_WCP 557
#define TEXTVIEWSELECTION_WCP 558
#define DIRBOOKMARKOP_WCP 559
#define OPENCONTEXTMENUOP_WCP 560
#define RUNCUSTOMACTION_WCP 561
#define CUSTOMNAME_WCP 562
#define CONTEXTBUTTON_WCP 563
#define ACTIVATEMOD_WCP 564
#define SCROLLMOD_WCP 565
#define CONTEXTMOD_WCP 566
#define NONE_WCP 567
#define CUSTOMACTION_WCP 568
#define SAVE_WORKER_STATE_ON_EXIT_WCP 569
#define OPENWORKERMENUOP_WCP 570
#define CHANGELABELOP_WCP 571
#define ASKFORLABEL_WCP 572
#define LABELCOLORS_WCP 573
#define BOOKMARKLABEL_WCP 574
#define BOOKMARKFILTER_WCP 575
#define SHOWONLYBOOKMARKS_WCP 576
#define SHOWONLYLABEL_WCP 577
#define SHOWALL_WCP 578
#define OPTIONMODE_WCP 579
#define INVERT_WCP 580
#define SET_WCP 581
#define CHANGEFILTERS_WCP 582
#define CHANGEBOOKMARKS_WCP 583
#define QUERYLABEL_WCP 584
#define MODIFYTABSOP_WCP 585
#define TABACTION_WCP 586
#define NEWTAB_WCP 587
#define CLOSECURRENTTAB_WCP 588
#define NEXTTAB_WCP 589
#define PREVTAB_WCP 590
#define TOGGLELOCKSTATE_WCP 591
#define MOVETABLEFT_WCP 592
#define MOVETABRIGHT_WCP 593
#define CHANGELAYOUTOP_WCP 594
#define INFIXSEARCH_WCP 595
#define VOLUMEMANAGEROP_WCP 596
#define ERROR 597
#define ACTIVESIDE_WCP 598
#define SHOWFREESPACE_WCP 599
#define ACTIVEMODE_WCP 600
#define ASK_WCP 601
#define SSHALLOW_WCP 602
#define FIELD_WIDTH_WCP 603
#define QUICKSEARCHENABLED_WCP 604
#define FILTEREDSEARCHENABLED_WCP 605
#define XFTFONTS_WCP 606
#define USEMAGIC_WCP 607
#define MAGICPATTERN_WCP 608
#define MAGICCOMPRESSED_WCP 609
#define MAGICMIME_WCP 610
#define VOLUMEMANAGER_WCP 611
#define MOUNTCOMMAND_WCP 612
#define UNMOUNTCOMMAND_WCP 613
#define FSTABFILE_WCP 614
#define MTABFILE_WCP 615
#define PARTFILE_WCP 616
#define REQUESTACTION_WCP 617
#define SIZEH_WCP 618
#define NEXTAGING_WCP 619
#define ENTRY_WCP 620
#define PREFIX_WCP 621
#define VALUE_WCP 622
#define EXTENSION_WCP 623
#define EJECTCOMMAND_WCP 624
#define CLOSETRAYCOMMAND_WCP 625
#define AVFSMODULE_WCP 626
#define FLEXIBLEMATCH_WCP 627
#define USE_VERSION_STRING_COMPARE_WCP 628
#define SWITCHBUTTONBANKOP_WCP 629
#define SWITCHTONEXTBANK_WCP 630
#define SWITCHTOPREVBANK_WCP 631
#define SWITCHTOBANKNR_WCP 632
#define BANKNR_WCP 633
#define USE_VIRTUAL_TEMP_COPIES_WCP 634
#define PATHJUMPOP_WCP 635
#define PATHJUMPALLOWDIRS_WCP 636
#define CLIPBOARDOP_WCP 637
#define CLIPBOARDSTRING_WCP 638
#define COMMANDSTRING_WCP 639
#define EVALCOMMAND_WCP 640
#define WATCHMODE_WCP 641
#define APPLY_WINDOW_DIALOG_TYPE_WCP 642
#define FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP 643
#define COMMANDMENUOP_WCP 644
#define SEARCHMODEONKEYPRESS_WCP 645
#define PATHENTRYONTOP_WCP 646
#define STORE_FILES_ALWAYS_WCP 647
#define PATHJUMPSETS_WCP 648
#define SHOWDOTDOT_WCP 649
#define SHOWBREADCRUMB_WCP 650
#define FACES_WCP 651
#define FACE_WCP 652
#define ENABLE_INFO_LINE_WCP 653
#define INFO_LINE_LUA_MODE_WCP 654
#define INFO_LINE_CONTENT_WCP 655
#define RESTORE_TABS_MODE_WCP 656
#define STORE_TABS_MODE_WCP 657
#define AS_EXIT_STATE_WCP 658
#define USE_EXTENDED_REGEX_WCP 659
#define HIGHLIGHT_USER_ACTION_WCP 660
#define CHTIMEOP_WCP 661
#define ADJUSTRELATIVESYMLINKS_WCP 662
#define OUTSIDE_WCP 663
#define EDIT_WCP 664
#define MAKEABSOLUTE_WCP 665
#define MAKERELATIVE_WCP 666
#define MAGICIGNORECASE_WCP 667
#define ENSURE_FILE_PERMISSIONS_WCP 668
#define USER_RW_WCP 669
#define USER_RW_GROUP_R_WCP 670
#define USER_RW_ALL_R_WCP 671
#define LEAVE_UNMODIFIED_WCP 672
#define PREFERUDISKSVERSION_WCP 673
#define CHANGECOLUMNSOP_WCP 674
#define PATTERNISCOMMASEPARATED_WCP 675
#define STRCASECMP_WCP 676
#define USE_STRING_COMPARE_MODE_WCP 677
#define VIEWNEWESTFILESOP_WCP 678
#define SHOWRECENT_WCP 679
#define DIRCOMPAREOP_WCP 680
#define TABPROFILESOP_WCP 681
#define EXTERNALVDIROP_WCP 682
#define VDIR_PRESERVE_DIR_STRUCTURE_WCP 683
#define OPENTABMENUOP_WCP 684
#define INITIALTAB_WCP 685
#define SHOWBYTIME_WCP 686
#define SHOWBYFILTER_WCP 687
#define SHOWBYPROGRAM_WCP 688
#define TERMINAL_RETURNS_EARLY_WCP 689
#define DIRECTORYPRESETS_WCP 690
#define DIRECTORYPRESET_WCP 691
#define HELPOP_WCP 692
#define PRIORITY_WCP 693
#define AUTOFILTER_WCP 694
#define ENABLE_CUSTOM_LVB_LINE_WCP 695
#define CUSTOM_LVB_LINE_WCP 696
#define CUSTOM_DIRECTORY_INFO_COMMAND_WCP 697
#define IMMEDIATE_FILTER_APPLY_WCP 698
#define DISABLEBGCHECKPREFIX_WCP 699
#define DISPLAYMODE_WCP 700
#define SHOWSUBDIRS_WCP 701
#define SHOWDIRECTSUBDIRS_WCP 702
#define INCLUDEALL_WCP 703
#define APPLYMODE_WCP 704
#define ADD_WCP 705
#define REMOVE_WCP 706
#define HIDENONEXISTING_WCP 707
#define VIEWCOMMANDLOGOP_WCP 708
#define LOCKONCURRENTDIR_WCP 709
#define STARTNODE_WCP 710
#define MENUS_WCP 711
#define LVMODES_WCP 712
#define LVCOMMANDS_WCP 713
#define TOPLEVEL_WCP 714
#define FOLLOWACTIVE_WCP 715
#define WATCHFILE_WCP 716
#define ACTIVATETEXTVIEWMODEOP_WCP 717
#define SOURCEMODE_WCP 718
#define ACTIVEFILE_WCP 719
#define FILETYPEACTION_WCP 720
#define IGNOREDIRS2_WCP 721
#define IGNOREDIR_WCP 722
#define CUSTOMATTRIBUTE_WCP 723
#define STARTSEARCHSTRING_WCP 724
#define APPLYONLY_WCP 725
#define USESELECTEDENTRIES_WCP 726
#define ONLYEXACTPATH_WCP 727
#define ALSOSELECTEXTENSION_WCP 728
#define STRING_WCP 729
#define NUM_WCP 730

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 214 "configparser.yy"

  int num;
  char *strptr;

#line 1292 "configparser.cc"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_CONFIGPARSER_HH_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_LEFTBRACE_WCP = 3,              /* LEFTBRACE_WCP  */
  YYSYMBOL_RIGHTBRACE_WCP = 4,             /* RIGHTBRACE_WCP  */
  YYSYMBOL_GLOBAL_WCP = 5,                 /* GLOBAL_WCP  */
  YYSYMBOL_COLORS_WCP = 6,                 /* COLORS_WCP  */
  YYSYMBOL_LANG_WCP = 7,                   /* LANG_WCP  */
  YYSYMBOL_PALETTE_WCP = 8,                /* PALETTE_WCP  */
  YYSYMBOL_OWNOP_WCP = 9,                  /* OWNOP_WCP  */
  YYSYMBOL_LISTERSETS_WCP = 10,            /* LISTERSETS_WCP  */
  YYSYMBOL_YES_WCP = 11,                   /* YES_WCP  */
  YYSYMBOL_NO_WCP = 12,                    /* NO_WCP  */
  YYSYMBOL_LEFT_WCP = 13,                  /* LEFT_WCP  */
  YYSYMBOL_RIGHT_WCP = 14,                 /* RIGHT_WCP  */
  YYSYMBOL_HBARTOP_WCP = 15,               /* HBARTOP_WCP  */
  YYSYMBOL_HBARHEIGHT_WCP = 16,            /* HBARHEIGHT_WCP  */
  YYSYMBOL_VBARLEFT_WCP = 17,              /* VBARLEFT_WCP  */
  YYSYMBOL_VBARWIDTH_WCP = 18,             /* VBARWIDTH_WCP  */
  YYSYMBOL_DISPLAYSETS_WCP = 19,           /* DISPLAYSETS_WCP  */
  YYSYMBOL_NAME_WCP = 20,                  /* NAME_WCP  */
  YYSYMBOL_SIZE_WCP = 21,                  /* SIZE_WCP  */
  YYSYMBOL_TYPE_WCP = 22,                  /* TYPE_WCP  */
  YYSYMBOL_PERMISSION_WCP = 23,            /* PERMISSION_WCP  */
  YYSYMBOL_OWNER_WCP = 24,                 /* OWNER_WCP  */
  YYSYMBOL_DESTINATION_WCP = 25,           /* DESTINATION_WCP  */
  YYSYMBOL_MODTIME_WCP = 26,               /* MODTIME_WCP  */
  YYSYMBOL_ACCTIME_WCP = 27,               /* ACCTIME_WCP  */
  YYSYMBOL_CHGTIME_WCP = 28,               /* CHGTIME_WCP  */
  YYSYMBOL_ROWS_WCP = 29,                  /* ROWS_WCP  */
  YYSYMBOL_COLUMNS_WCP = 30,               /* COLUMNS_WCP  */
  YYSYMBOL_CACHESIZE_WCP = 31,             /* CACHESIZE_WCP  */
  YYSYMBOL_OWNERSTYLE_WCP = 32,            /* OWNERSTYLE_WCP  */
  YYSYMBOL_TERMINAL_WCP = 33,              /* TERMINAL_WCP  */
  YYSYMBOL_USESTRINGFORDIRSIZE_WCP = 34,   /* USESTRINGFORDIRSIZE_WCP  */
  YYSYMBOL_TIMESETS_WCP = 35,              /* TIMESETS_WCP  */
  YYSYMBOL_STYLE1_WCP = 36,                /* STYLE1_WCP  */
  YYSYMBOL_STYLE2_WCP = 37,                /* STYLE2_WCP  */
  YYSYMBOL_STYLE3_WCP = 38,                /* STYLE3_WCP  */
  YYSYMBOL_DATE_WCP = 39,                  /* DATE_WCP  */
  YYSYMBOL_TIME_WCP = 40,                  /* TIME_WCP  */
  YYSYMBOL_DATESUBSTITUTION_WCP = 41,      /* DATESUBSTITUTION_WCP  */
  YYSYMBOL_DATEBEFORETIME_WCP = 42,        /* DATEBEFORETIME_WCP  */
  YYSYMBOL_STATEBAR_WCP = 43,              /* STATEBAR_WCP  */
  YYSYMBOL_SELLVBAR_WCP = 44,              /* SELLVBAR_WCP  */
  YYSYMBOL_UNSELLVBAR_WCP = 45,            /* UNSELLVBAR_WCP  */
  YYSYMBOL_CLOCKBAR_WCP = 46,              /* CLOCKBAR_WCP  */
  YYSYMBOL_REQUESTER_WCP = 47,             /* REQUESTER_WCP  */
  YYSYMBOL_UNSELDIR_WCP = 48,              /* UNSELDIR_WCP  */
  YYSYMBOL_SELDIR_WCP = 49,                /* SELDIR_WCP  */
  YYSYMBOL_UNSELFILE_WCP = 50,             /* UNSELFILE_WCP  */
  YYSYMBOL_SELFILE_WCP = 51,               /* SELFILE_WCP  */
  YYSYMBOL_UNSELACTDIR_WCP = 52,           /* UNSELACTDIR_WCP  */
  YYSYMBOL_SELACTDIR_WCP = 53,             /* SELACTDIR_WCP  */
  YYSYMBOL_UNSELACTFILE_WCP = 54,          /* UNSELACTFILE_WCP  */
  YYSYMBOL_SELACTFILE_WCP = 55,            /* SELACTFILE_WCP  */
  YYSYMBOL_LVBG_WCP = 56,                  /* LVBG_WCP  */
  YYSYMBOL_STARTUP_WCP = 57,               /* STARTUP_WCP  */
  YYSYMBOL_BUTTON_WCP = 58,                /* BUTTON_WCP  */
  YYSYMBOL_BUTTONS_WCP = 59,               /* BUTTONS_WCP  */
  YYSYMBOL_POSITION_WCP = 60,              /* POSITION_WCP  */
  YYSYMBOL_TITLE_WCP = 61,                 /* TITLE_WCP  */
  YYSYMBOL_COMMANDS_WCP = 62,              /* COMMANDS_WCP  */
  YYSYMBOL_SHORTKEYS_WCP = 63,             /* SHORTKEYS_WCP  */
  YYSYMBOL_PATHS_WCP = 64,                 /* PATHS_WCP  */
  YYSYMBOL_PATH_WCP = 65,                  /* PATH_WCP  */
  YYSYMBOL_FILETYPES_WCP = 66,             /* FILETYPES_WCP  */
  YYSYMBOL_FILETYPE_WCP = 67,              /* FILETYPE_WCP  */
  YYSYMBOL_NORMAL_WCP = 68,                /* NORMAL_WCP  */
  YYSYMBOL_NOTYETCHECKED_WCP = 69,         /* NOTYETCHECKED_WCP  */
  YYSYMBOL_UNKNOWN_WCP = 70,               /* UNKNOWN_WCP  */
  YYSYMBOL_NOSELECT_WCP = 71,              /* NOSELECT_WCP  */
  YYSYMBOL_DIR_WCP = 72,                   /* DIR_WCP  */
  YYSYMBOL_USEPATTERN_WCP = 73,            /* USEPATTERN_WCP  */
  YYSYMBOL_USECONTENT_WCP = 74,            /* USECONTENT_WCP  */
  YYSYMBOL_PATTERN_WCP = 75,               /* PATTERN_WCP  */
  YYSYMBOL_CONTENT_WCP = 76,               /* CONTENT_WCP  */
  YYSYMBOL_FTCOMMANDS_WCP = 77,            /* FTCOMMANDS_WCP  */
  YYSYMBOL_DND_WCP = 78,                   /* DND_WCP  */
  YYSYMBOL_DC_WCP = 79,                    /* DC_WCP  */
  YYSYMBOL_SHOW_WCP = 80,                  /* SHOW_WCP  */
  YYSYMBOL_RAWSHOW_WCP = 81,               /* RAWSHOW_WCP  */
  YYSYMBOL_USER_WCP = 82,                  /* USER_WCP  */
  YYSYMBOL_FLAGS_WCP = 83,                 /* FLAGS_WCP  */
  YYSYMBOL_IGNOREDIRS_WCP = 84,            /* IGNOREDIRS_WCP  */
  YYSYMBOL_HOTKEYS_WCP = 85,               /* HOTKEYS_WCP  */
  YYSYMBOL_HOTKEY_WCP = 86,                /* HOTKEY_WCP  */
  YYSYMBOL_FONTS_WCP = 87,                 /* FONTS_WCP  */
  YYSYMBOL_GLOBALFONT_WCP = 88,            /* GLOBALFONT_WCP  */
  YYSYMBOL_BUTTONFONT_WCP = 89,            /* BUTTONFONT_WCP  */
  YYSYMBOL_LEFTFONT_WCP = 90,              /* LEFTFONT_WCP  */
  YYSYMBOL_RIGHTFONT_WCP = 91,             /* RIGHTFONT_WCP  */
  YYSYMBOL_TEXTVIEWFONT_WCP = 92,          /* TEXTVIEWFONT_WCP  */
  YYSYMBOL_STATEBARFONT_WCP = 93,          /* STATEBARFONT_WCP  */
  YYSYMBOL_TEXTVIEWALTFONT_WCP = 94,       /* TEXTVIEWALTFONT_WCP  */
  YYSYMBOL_CLOCKBARSETS_WCP = 95,          /* CLOCKBARSETS_WCP  */
  YYSYMBOL_MODUS_WCP = 96,                 /* MODUS_WCP  */
  YYSYMBOL_TIMESPACE_WCP = 97,             /* TIMESPACE_WCP  */
  YYSYMBOL_VERSION_WCP = 98,               /* VERSION_WCP  */
  YYSYMBOL_EXTERN_WCP = 99,                /* EXTERN_WCP  */
  YYSYMBOL_UPDATETIME_WCP = 100,           /* UPDATETIME_WCP  */
  YYSYMBOL_PROGRAM_WCP = 101,              /* PROGRAM_WCP  */
  YYSYMBOL_SHOWHINTS_WCP = 102,            /* SHOWHINTS_WCP  */
  YYSYMBOL_KEY_WCP = 103,                  /* KEY_WCP  */
  YYSYMBOL_DOUBLE_WCP = 104,               /* DOUBLE_WCP  */
  YYSYMBOL_MOD_WCP = 105,                  /* MOD_WCP  */
  YYSYMBOL_CONTROL_WCP = 106,              /* CONTROL_WCP  */
  YYSYMBOL_SHIFT_WCP = 107,                /* SHIFT_WCP  */
  YYSYMBOL_LOCK_WCP = 108,                 /* LOCK_WCP  */
  YYSYMBOL_MOD1_WCP = 109,                 /* MOD1_WCP  */
  YYSYMBOL_MOD2_WCP = 110,                 /* MOD2_WCP  */
  YYSYMBOL_MOD3_WCP = 111,                 /* MOD3_WCP  */
  YYSYMBOL_MOD4_WCP = 112,                 /* MOD4_WCP  */
  YYSYMBOL_MOD5_WCP = 113,                 /* MOD5_WCP  */
  YYSYMBOL_DNDACTION_WCP = 114,            /* DNDACTION_WCP  */
  YYSYMBOL_DCACTION_WCP = 115,             /* DCACTION_WCP  */
  YYSYMBOL_SHOWACTION_WCP = 116,           /* SHOWACTION_WCP  */
  YYSYMBOL_RSHOWACTION_WCP = 117,          /* RSHOWACTION_WCP  */
  YYSYMBOL_USERACTION_WCP = 118,           /* USERACTION_WCP  */
  YYSYMBOL_ROWUP_WCP = 119,                /* ROWUP_WCP  */
  YYSYMBOL_ROWDOWN_WCP = 120,              /* ROWDOWN_WCP  */
  YYSYMBOL_CHANGEHIDDENFLAG_WCP = 121,     /* CHANGEHIDDENFLAG_WCP  */
  YYSYMBOL_COPYOP_WCP = 122,               /* COPYOP_WCP  */
  YYSYMBOL_FIRSTROW_WCP = 123,             /* FIRSTROW_WCP  */
  YYSYMBOL_LASTROW_WCP = 124,              /* LASTROW_WCP  */
  YYSYMBOL_PAGEUP_WCP = 125,               /* PAGEUP_WCP  */
  YYSYMBOL_PAGEDOWN_WCP = 126,             /* PAGEDOWN_WCP  */
  YYSYMBOL_SELECTOP_WCP = 127,             /* SELECTOP_WCP  */
  YYSYMBOL_SELECTALLOP_WCP = 128,          /* SELECTALLOP_WCP  */
  YYSYMBOL_SELECTNONEOP_WCP = 129,         /* SELECTNONEOP_WCP  */
  YYSYMBOL_INVERTALLOP_WCP = 130,          /* INVERTALLOP_WCP  */
  YYSYMBOL_PARENTDIROP_WCP = 131,          /* PARENTDIROP_WCP  */
  YYSYMBOL_ENTERDIROP_WCP = 132,           /* ENTERDIROP_WCP  */
  YYSYMBOL_CHANGELISTERSETOP_WCP = 133,    /* CHANGELISTERSETOP_WCP  */
  YYSYMBOL_SWITCHLISTEROP_WCP = 134,       /* SWITCHLISTEROP_WCP  */
  YYSYMBOL_FILTERSELECTOP_WCP = 135,       /* FILTERSELECTOP_WCP  */
  YYSYMBOL_FILTERUNSELECTOP_WCP = 136,     /* FILTERUNSELECTOP_WCP  */
  YYSYMBOL_PATHTOOTHERSIDEOP_WCP = 137,    /* PATHTOOTHERSIDEOP_WCP  */
  YYSYMBOL_QUITOP_WCP = 138,               /* QUITOP_WCP  */
  YYSYMBOL_DELETEOP_WCP = 139,             /* DELETEOP_WCP  */
  YYSYMBOL_RELOADOP_WCP = 140,             /* RELOADOP_WCP  */
  YYSYMBOL_MAKEDIROP_WCP = 141,            /* MAKEDIROP_WCP  */
  YYSYMBOL_RENAMEOP_WCP = 142,             /* RENAMEOP_WCP  */
  YYSYMBOL_DIRSIZEOP_WCP = 143,            /* DIRSIZEOP_WCP  */
  YYSYMBOL_SIMDDOP_WCP = 144,              /* SIMDDOP_WCP  */
  YYSYMBOL_STARTPROGOP_WCP = 145,          /* STARTPROGOP_WCP  */
  YYSYMBOL_SEARCHENTRYOP_WCP = 146,        /* SEARCHENTRYOP_WCP  */
  YYSYMBOL_ENTERPATHOP_WCP = 147,          /* ENTERPATHOP_WCP  */
  YYSYMBOL_SCROLLLISTEROP_WCP = 148,       /* SCROLLLISTEROP_WCP  */
  YYSYMBOL_CREATESYMLINKOP_WCP = 149,      /* CREATESYMLINKOP_WCP  */
  YYSYMBOL_CHANGESYMLINKOP_WCP = 150,      /* CHANGESYMLINKOP_WCP  */
  YYSYMBOL_CHMODOP_WCP = 151,              /* CHMODOP_WCP  */
  YYSYMBOL_TOGGLELISTERMODEOP_WCP = 152,   /* TOGGLELISTERMODEOP_WCP  */
  YYSYMBOL_SETSORTMODEOP_WCP = 153,        /* SETSORTMODEOP_WCP  */
  YYSYMBOL_SETFILTEROP_WCP = 154,          /* SETFILTEROP_WCP  */
  YYSYMBOL_SHORTKEYFROMLISTOP_WCP = 155,   /* SHORTKEYFROMLISTOP_WCP  */
  YYSYMBOL_CHOWNOP_WCP = 156,              /* CHOWNOP_WCP  */
  YYSYMBOL_WORKERCONFIG_WCP = 157,         /* WORKERCONFIG_WCP  */
  YYSYMBOL_USERSTYLE_WCP = 158,            /* USERSTYLE_WCP  */
  YYSYMBOL_DATESTRING_WCP = 159,           /* DATESTRING_WCP  */
  YYSYMBOL_TIMESTRING_WCP = 160,           /* TIMESTRING_WCP  */
  YYSYMBOL_COLOR_WCP = 161,                /* COLOR_WCP  */
  YYSYMBOL_PATTERNIGNORECASE_WCP = 162,    /* PATTERNIGNORECASE_WCP  */
  YYSYMBOL_PATTERNUSEREGEXP_WCP = 163,     /* PATTERNUSEREGEXP_WCP  */
  YYSYMBOL_PATTERNUSEFULLNAME_WCP = 164,   /* PATTERNUSEFULLNAME_WCP  */
  YYSYMBOL_COM_WCP = 165,                  /* COM_WCP  */
  YYSYMBOL_SEPARATEEACHENTRY_WCP = 166,    /* SEPARATEEACHENTRY_WCP  */
  YYSYMBOL_RECURSIVE_WCP = 167,            /* RECURSIVE_WCP  */
  YYSYMBOL_START_WCP = 168,                /* START_WCP  */
  YYSYMBOL_TERMINALWAIT_WCP = 169,         /* TERMINALWAIT_WCP  */
  YYSYMBOL_SHOWOUTPUT_WCP = 170,           /* SHOWOUTPUT_WCP  */
  YYSYMBOL_VIEWSTR_WCP = 171,              /* VIEWSTR_WCP  */
  YYSYMBOL_SHOWOUTPUTINT_WCP = 172,        /* SHOWOUTPUTINT_WCP  */
  YYSYMBOL_SHOWOUTPUTOTHERSIDE_WCP = 173,  /* SHOWOUTPUTOTHERSIDE_WCP  */
  YYSYMBOL_SHOWOUTPUTCUSTOMATTRIBUTE_WCP = 174, /* SHOWOUTPUTCUSTOMATTRIBUTE_WCP  */
  YYSYMBOL_INBACKGROUND_WCP = 175,         /* INBACKGROUND_WCP  */
  YYSYMBOL_TAKEDIRS_WCP = 176,             /* TAKEDIRS_WCP  */
  YYSYMBOL_ACTIONNUMBER_WCP = 177,         /* ACTIONNUMBER_WCP  */
  YYSYMBOL_HIDDENFILES_WCP = 178,          /* HIDDENFILES_WCP  */
  YYSYMBOL_HIDE_WCP = 179,                 /* HIDE_WCP  */
  YYSYMBOL_TOGGLE_WCP = 180,               /* TOGGLE_WCP  */
  YYSYMBOL_FOLLOWSYMLINKS_WCP = 181,       /* FOLLOWSYMLINKS_WCP  */
  YYSYMBOL_MOVE_WCP = 182,                 /* MOVE_WCP  */
  YYSYMBOL_RENAME_WCP = 183,               /* RENAME_WCP  */
  YYSYMBOL_SAMEDIR_WCP = 184,              /* SAMEDIR_WCP  */
  YYSYMBOL_REQUESTDEST_WCP = 185,          /* REQUESTDEST_WCP  */
  YYSYMBOL_REQUESTFLAGS_WCP = 186,         /* REQUESTFLAGS_WCP  */
  YYSYMBOL_OVERWRITE_WCP = 187,            /* OVERWRITE_WCP  */
  YYSYMBOL_ALWAYS_WCP = 188,               /* ALWAYS_WCP  */
  YYSYMBOL_NEVER_WCP = 189,                /* NEVER_WCP  */
  YYSYMBOL_COPYMODE_WCP = 190,             /* COPYMODE_WCP  */
  YYSYMBOL_FAST_WCP = 191,                 /* FAST_WCP  */
  YYSYMBOL_PRESERVEATTR_WCP = 192,         /* PRESERVEATTR_WCP  */
  YYSYMBOL_MODE_WCP = 193,                 /* MODE_WCP  */
  YYSYMBOL_ACTIVE_WCP = 194,               /* ACTIVE_WCP  */
  YYSYMBOL_ACTIVE2OTHER_WCP = 195,         /* ACTIVE2OTHER_WCP  */
  YYSYMBOL_SPECIAL_WCP = 196,              /* SPECIAL_WCP  */
  YYSYMBOL_REQUEST_WCP = 197,              /* REQUEST_WCP  */
  YYSYMBOL_CURRENT_WCP = 198,              /* CURRENT_WCP  */
  YYSYMBOL_OTHER_WCP = 199,                /* OTHER_WCP  */
  YYSYMBOL_FILTER_WCP = 200,               /* FILTER_WCP  */
  YYSYMBOL_QUICK_WCP = 201,                /* QUICK_WCP  */
  YYSYMBOL_ALSOACTIVE_WCP = 202,           /* ALSOACTIVE_WCP  */
  YYSYMBOL_RESETDIRSIZES_WCP = 203,        /* RESETDIRSIZES_WCP  */
  YYSYMBOL_KEEPFILETYPES_WCP = 204,        /* KEEPFILETYPES_WCP  */
  YYSYMBOL_RELATIVE_WCP = 205,             /* RELATIVE_WCP  */
  YYSYMBOL_ONFILES_WCP = 206,              /* ONFILES_WCP  */
  YYSYMBOL_ONDIRS_WCP = 207,               /* ONDIRS_WCP  */
  YYSYMBOL_SORTBY_WCP = 208,               /* SORTBY_WCP  */
  YYSYMBOL_SORTFLAG_WCP = 209,             /* SORTFLAG_WCP  */
  YYSYMBOL_REVERSE_WCP = 210,              /* REVERSE_WCP  */
  YYSYMBOL_DIRLAST_WCP = 211,              /* DIRLAST_WCP  */
  YYSYMBOL_DIRMIXED_WCP = 212,             /* DIRMIXED_WCP  */
  YYSYMBOL_EXCLUDE_WCP = 213,              /* EXCLUDE_WCP  */
  YYSYMBOL_INCLUDE_WCP = 214,              /* INCLUDE_WCP  */
  YYSYMBOL_UNSET_WCP = 215,                /* UNSET_WCP  */
  YYSYMBOL_UNSETALL_WCP = 216,             /* UNSETALL_WCP  */
  YYSYMBOL_SCRIPTOP_WCP = 217,             /* SCRIPTOP_WCP  */
  YYSYMBOL_NOP_WCP = 218,                  /* NOP_WCP  */
  YYSYMBOL_PUSH_WCP = 219,                 /* PUSH_WCP  */
  YYSYMBOL_LABEL_WCP = 220,                /* LABEL_WCP  */
  YYSYMBOL_IF_WCP = 221,                   /* IF_WCP  */
  YYSYMBOL_END_WCP = 222,                  /* END_WCP  */
  YYSYMBOL_POP_WCP = 223,                  /* POP_WCP  */
  YYSYMBOL_SETTINGS_WCP = 224,             /* SETTINGS_WCP  */
  YYSYMBOL_WINDOW_WCP = 225,               /* WINDOW_WCP  */
  YYSYMBOL_GOTO_WCP = 226,                 /* GOTO_WCP  */
  YYSYMBOL_PUSHUSEOUTPUT_WCP = 227,        /* PUSHUSEOUTPUT_WCP  */
  YYSYMBOL_DODEBUG_WCP = 228,              /* DODEBUG_WCP  */
  YYSYMBOL_WPURECURSIVE_WCP = 229,         /* WPURECURSIVE_WCP  */
  YYSYMBOL_WPUTAKEDIRS_WCP = 230,          /* WPUTAKEDIRS_WCP  */
  YYSYMBOL_STACKNR_WCP = 231,              /* STACKNR_WCP  */
  YYSYMBOL_PUSHSTRING_WCP = 232,           /* PUSHSTRING_WCP  */
  YYSYMBOL_PUSHOUTPUTRETURNCODE_WCP = 233, /* PUSHOUTPUTRETURNCODE_WCP  */
  YYSYMBOL_IFTEST_WCP = 234,               /* IFTEST_WCP  */
  YYSYMBOL_IFLABEL_WCP = 235,              /* IFLABEL_WCP  */
  YYSYMBOL_WINTYPE_WCP = 236,              /* WINTYPE_WCP  */
  YYSYMBOL_OPEN_WCP = 237,                 /* OPEN_WCP  */
  YYSYMBOL_CLOSE_WCP = 238,                /* CLOSE_WCP  */
  YYSYMBOL_LEAVE_WCP = 239,                /* LEAVE_WCP  */
  YYSYMBOL_CHANGEPROGRESS_WCP = 240,       /* CHANGEPROGRESS_WCP  */
  YYSYMBOL_CHANGETEXT_WCP = 241,           /* CHANGETEXT_WCP  */
  YYSYMBOL_PROGRESSUSEOUTPUT_WCP = 242,    /* PROGRESSUSEOUTPUT_WCP  */
  YYSYMBOL_WINTEXTUSEOUTPUT_WCP = 243,     /* WINTEXTUSEOUTPUT_WCP  */
  YYSYMBOL_PROGRESS_WCP = 244,             /* PROGRESS_WCP  */
  YYSYMBOL_WINTEXT_WCP = 245,              /* WINTEXT_WCP  */
  YYSYMBOL_SHOWDIRCACHEOP_WCP = 246,       /* SHOWDIRCACHEOP_WCP  */
  YYSYMBOL_INODE_WCP = 247,                /* INODE_WCP  */
  YYSYMBOL_NLINK_WCP = 248,                /* NLINK_WCP  */
  YYSYMBOL_BLOCKS_WCP = 249,               /* BLOCKS_WCP  */
  YYSYMBOL_SHOWHEADER_WCP = 250,           /* SHOWHEADER_WCP  */
  YYSYMBOL_LVHEADER_WCP = 251,             /* LVHEADER_WCP  */
  YYSYMBOL_IGNORECASE_WCP = 252,           /* IGNORECASE_WCP  */
  YYSYMBOL_LISTVIEWS_WCP = 253,            /* LISTVIEWS_WCP  */
  YYSYMBOL_BLL_WCP = 254,                  /* BLL_WCP  */
  YYSYMBOL_LBL_WCP = 255,                  /* LBL_WCP  */
  YYSYMBOL_LLB_WCP = 256,                  /* LLB_WCP  */
  YYSYMBOL_BL_WCP = 257,                   /* BL_WCP  */
  YYSYMBOL_LB_WCP = 258,                   /* LB_WCP  */
  YYSYMBOL_LAYOUT_WCP = 259,               /* LAYOUT_WCP  */
  YYSYMBOL_BUTTONSVERT_WCP = 260,          /* BUTTONSVERT_WCP  */
  YYSYMBOL_LISTVIEWSVERT_WCP = 261,        /* LISTVIEWSVERT_WCP  */
  YYSYMBOL_LISTVIEWWEIGHT_WCP = 262,       /* LISTVIEWWEIGHT_WCP  */
  YYSYMBOL_WEIGHTTOACTIVE_WCP = 263,       /* WEIGHTTOACTIVE_WCP  */
  YYSYMBOL_EXTCOND_WCP = 264,              /* EXTCOND_WCP  */
  YYSYMBOL_USEEXTCOND_WCP = 265,           /* USEEXTCOND_WCP  */
  YYSYMBOL_SUBTYPE_WCP = 266,              /* SUBTYPE_WCP  */
  YYSYMBOL_PARENTACTIONOP_WCP = 267,       /* PARENTACTIONOP_WCP  */
  YYSYMBOL_NOOPERATIONOP_WCP = 268,        /* NOOPERATIONOP_WCP  */
  YYSYMBOL_COLORMODE_WCP = 269,            /* COLORMODE_WCP  */
  YYSYMBOL_DEFAULT_WCP = 270,              /* DEFAULT_WCP  */
  YYSYMBOL_CUSTOM_WCP = 271,               /* CUSTOM_WCP  */
  YYSYMBOL_UNSELECTCOLOR_WCP = 272,        /* UNSELECTCOLOR_WCP  */
  YYSYMBOL_SELECTCOLOR_WCP = 273,          /* SELECTCOLOR_WCP  */
  YYSYMBOL_UNSELECTACTIVECOLOR_WCP = 274,  /* UNSELECTACTIVECOLOR_WCP  */
  YYSYMBOL_SELECTACTIVECOLOR_WCP = 275,    /* SELECTACTIVECOLOR_WCP  */
  YYSYMBOL_COLOREXTERNPROG_WCP = 276,      /* COLOREXTERNPROG_WCP  */
  YYSYMBOL_PARENT_WCP = 277,               /* PARENT_WCP  */
  YYSYMBOL_DONTCD_WCP = 278,               /* DONTCD_WCP  */
  YYSYMBOL_DONTCHECKVIRTUAL_WCP = 279,     /* DONTCHECKVIRTUAL_WCP  */
  YYSYMBOL_GOFTPOP_WCP = 280,              /* GOFTPOP_WCP  */
  YYSYMBOL_HOSTNAME_WCP = 281,             /* HOSTNAME_WCP  */
  YYSYMBOL_USERNAME_WCP = 282,             /* USERNAME_WCP  */
  YYSYMBOL_PASSWORD_WCP = 283,             /* PASSWORD_WCP  */
  YYSYMBOL_DONTENTERFTP_WCP = 284,         /* DONTENTERFTP_WCP  */
  YYSYMBOL_ALWAYSSTOREPW_WCP = 285,        /* ALWAYSSTOREPW_WCP  */
  YYSYMBOL_INTERNALVIEWOP_WCP = 286,       /* INTERNALVIEWOP_WCP  */
  YYSYMBOL_CUSTOMFILES_WCP = 287,          /* CUSTOMFILES_WCP  */
  YYSYMBOL_SHOWMODE_WCP = 288,             /* SHOWMODE_WCP  */
  YYSYMBOL_SELECTED_WCP = 289,             /* SELECTED_WCP  */
  YYSYMBOL_REVERSESEARCH_WCP = 290,        /* REVERSESEARCH_WCP  */
  YYSYMBOL_MOUSECONF_WCP = 291,            /* MOUSECONF_WCP  */
  YYSYMBOL_SELECTBUTTON_WCP = 292,         /* SELECTBUTTON_WCP  */
  YYSYMBOL_ACTIVATEBUTTON_WCP = 293,       /* ACTIVATEBUTTON_WCP  */
  YYSYMBOL_SCROLLBUTTON_WCP = 294,         /* SCROLLBUTTON_WCP  */
  YYSYMBOL_SELECTMETHOD_WCP = 295,         /* SELECTMETHOD_WCP  */
  YYSYMBOL_ALTERNATIVE_WCP = 296,          /* ALTERNATIVE_WCP  */
  YYSYMBOL_DELAYEDCONTEXTMENU_WCP = 297,   /* DELAYEDCONTEXTMENU_WCP  */
  YYSYMBOL_SEARCHOP_WCP = 298,             /* SEARCHOP_WCP  */
  YYSYMBOL_EDITCOMMAND_WCP = 299,          /* EDITCOMMAND_WCP  */
  YYSYMBOL_SHOWPREVRESULTS_WCP = 300,      /* SHOWPREVRESULTS_WCP  */
  YYSYMBOL_TEXTVIEW_WCP = 301,             /* TEXTVIEW_WCP  */
  YYSYMBOL_TEXTVIEWHIGHLIGHTED_WCP = 302,  /* TEXTVIEWHIGHLIGHTED_WCP  */
  YYSYMBOL_TEXTVIEWSELECTION_WCP = 303,    /* TEXTVIEWSELECTION_WCP  */
  YYSYMBOL_DIRBOOKMARKOP_WCP = 304,        /* DIRBOOKMARKOP_WCP  */
  YYSYMBOL_OPENCONTEXTMENUOP_WCP = 305,    /* OPENCONTEXTMENUOP_WCP  */
  YYSYMBOL_RUNCUSTOMACTION_WCP = 306,      /* RUNCUSTOMACTION_WCP  */
  YYSYMBOL_CUSTOMNAME_WCP = 307,           /* CUSTOMNAME_WCP  */
  YYSYMBOL_CONTEXTBUTTON_WCP = 308,        /* CONTEXTBUTTON_WCP  */
  YYSYMBOL_ACTIVATEMOD_WCP = 309,          /* ACTIVATEMOD_WCP  */
  YYSYMBOL_SCROLLMOD_WCP = 310,            /* SCROLLMOD_WCP  */
  YYSYMBOL_CONTEXTMOD_WCP = 311,           /* CONTEXTMOD_WCP  */
  YYSYMBOL_NONE_WCP = 312,                 /* NONE_WCP  */
  YYSYMBOL_CUSTOMACTION_WCP = 313,         /* CUSTOMACTION_WCP  */
  YYSYMBOL_SAVE_WORKER_STATE_ON_EXIT_WCP = 314, /* SAVE_WORKER_STATE_ON_EXIT_WCP  */
  YYSYMBOL_OPENWORKERMENUOP_WCP = 315,     /* OPENWORKERMENUOP_WCP  */
  YYSYMBOL_CHANGELABELOP_WCP = 316,        /* CHANGELABELOP_WCP  */
  YYSYMBOL_ASKFORLABEL_WCP = 317,          /* ASKFORLABEL_WCP  */
  YYSYMBOL_LABELCOLORS_WCP = 318,          /* LABELCOLORS_WCP  */
  YYSYMBOL_BOOKMARKLABEL_WCP = 319,        /* BOOKMARKLABEL_WCP  */
  YYSYMBOL_BOOKMARKFILTER_WCP = 320,       /* BOOKMARKFILTER_WCP  */
  YYSYMBOL_SHOWONLYBOOKMARKS_WCP = 321,    /* SHOWONLYBOOKMARKS_WCP  */
  YYSYMBOL_SHOWONLYLABEL_WCP = 322,        /* SHOWONLYLABEL_WCP  */
  YYSYMBOL_SHOWALL_WCP = 323,              /* SHOWALL_WCP  */
  YYSYMBOL_OPTIONMODE_WCP = 324,           /* OPTIONMODE_WCP  */
  YYSYMBOL_INVERT_WCP = 325,               /* INVERT_WCP  */
  YYSYMBOL_SET_WCP = 326,                  /* SET_WCP  */
  YYSYMBOL_CHANGEFILTERS_WCP = 327,        /* CHANGEFILTERS_WCP  */
  YYSYMBOL_CHANGEBOOKMARKS_WCP = 328,      /* CHANGEBOOKMARKS_WCP  */
  YYSYMBOL_QUERYLABEL_WCP = 329,           /* QUERYLABEL_WCP  */
  YYSYMBOL_MODIFYTABSOP_WCP = 330,         /* MODIFYTABSOP_WCP  */
  YYSYMBOL_TABACTION_WCP = 331,            /* TABACTION_WCP  */
  YYSYMBOL_NEWTAB_WCP = 332,               /* NEWTAB_WCP  */
  YYSYMBOL_CLOSECURRENTTAB_WCP = 333,      /* CLOSECURRENTTAB_WCP  */
  YYSYMBOL_NEXTTAB_WCP = 334,              /* NEXTTAB_WCP  */
  YYSYMBOL_PREVTAB_WCP = 335,              /* PREVTAB_WCP  */
  YYSYMBOL_TOGGLELOCKSTATE_WCP = 336,      /* TOGGLELOCKSTATE_WCP  */
  YYSYMBOL_MOVETABLEFT_WCP = 337,          /* MOVETABLEFT_WCP  */
  YYSYMBOL_MOVETABRIGHT_WCP = 338,         /* MOVETABRIGHT_WCP  */
  YYSYMBOL_CHANGELAYOUTOP_WCP = 339,       /* CHANGELAYOUTOP_WCP  */
  YYSYMBOL_INFIXSEARCH_WCP = 340,          /* INFIXSEARCH_WCP  */
  YYSYMBOL_VOLUMEMANAGEROP_WCP = 341,      /* VOLUMEMANAGEROP_WCP  */
  YYSYMBOL_ERROR = 342,                    /* ERROR  */
  YYSYMBOL_ACTIVESIDE_WCP = 343,           /* ACTIVESIDE_WCP  */
  YYSYMBOL_SHOWFREESPACE_WCP = 344,        /* SHOWFREESPACE_WCP  */
  YYSYMBOL_ACTIVEMODE_WCP = 345,           /* ACTIVEMODE_WCP  */
  YYSYMBOL_ASK_WCP = 346,                  /* ASK_WCP  */
  YYSYMBOL_SSHALLOW_WCP = 347,             /* SSHALLOW_WCP  */
  YYSYMBOL_FIELD_WIDTH_WCP = 348,          /* FIELD_WIDTH_WCP  */
  YYSYMBOL_QUICKSEARCHENABLED_WCP = 349,   /* QUICKSEARCHENABLED_WCP  */
  YYSYMBOL_FILTEREDSEARCHENABLED_WCP = 350, /* FILTEREDSEARCHENABLED_WCP  */
  YYSYMBOL_XFTFONTS_WCP = 351,             /* XFTFONTS_WCP  */
  YYSYMBOL_USEMAGIC_WCP = 352,             /* USEMAGIC_WCP  */
  YYSYMBOL_MAGICPATTERN_WCP = 353,         /* MAGICPATTERN_WCP  */
  YYSYMBOL_MAGICCOMPRESSED_WCP = 354,      /* MAGICCOMPRESSED_WCP  */
  YYSYMBOL_MAGICMIME_WCP = 355,            /* MAGICMIME_WCP  */
  YYSYMBOL_VOLUMEMANAGER_WCP = 356,        /* VOLUMEMANAGER_WCP  */
  YYSYMBOL_MOUNTCOMMAND_WCP = 357,         /* MOUNTCOMMAND_WCP  */
  YYSYMBOL_UNMOUNTCOMMAND_WCP = 358,       /* UNMOUNTCOMMAND_WCP  */
  YYSYMBOL_FSTABFILE_WCP = 359,            /* FSTABFILE_WCP  */
  YYSYMBOL_MTABFILE_WCP = 360,             /* MTABFILE_WCP  */
  YYSYMBOL_PARTFILE_WCP = 361,             /* PARTFILE_WCP  */
  YYSYMBOL_REQUESTACTION_WCP = 362,        /* REQUESTACTION_WCP  */
  YYSYMBOL_SIZEH_WCP = 363,                /* SIZEH_WCP  */
  YYSYMBOL_NEXTAGING_WCP = 364,            /* NEXTAGING_WCP  */
  YYSYMBOL_ENTRY_WCP = 365,                /* ENTRY_WCP  */
  YYSYMBOL_PREFIX_WCP = 366,               /* PREFIX_WCP  */
  YYSYMBOL_VALUE_WCP = 367,                /* VALUE_WCP  */
  YYSYMBOL_EXTENSION_WCP = 368,            /* EXTENSION_WCP  */
  YYSYMBOL_EJECTCOMMAND_WCP = 369,         /* EJECTCOMMAND_WCP  */
  YYSYMBOL_CLOSETRAYCOMMAND_WCP = 370,     /* CLOSETRAYCOMMAND_WCP  */
  YYSYMBOL_AVFSMODULE_WCP = 371,           /* AVFSMODULE_WCP  */
  YYSYMBOL_FLEXIBLEMATCH_WCP = 372,        /* FLEXIBLEMATCH_WCP  */
  YYSYMBOL_USE_VERSION_STRING_COMPARE_WCP = 373, /* USE_VERSION_STRING_COMPARE_WCP  */
  YYSYMBOL_SWITCHBUTTONBANKOP_WCP = 374,   /* SWITCHBUTTONBANKOP_WCP  */
  YYSYMBOL_SWITCHTONEXTBANK_WCP = 375,     /* SWITCHTONEXTBANK_WCP  */
  YYSYMBOL_SWITCHTOPREVBANK_WCP = 376,     /* SWITCHTOPREVBANK_WCP  */
  YYSYMBOL_SWITCHTOBANKNR_WCP = 377,       /* SWITCHTOBANKNR_WCP  */
  YYSYMBOL_BANKNR_WCP = 378,               /* BANKNR_WCP  */
  YYSYMBOL_USE_VIRTUAL_TEMP_COPIES_WCP = 379, /* USE_VIRTUAL_TEMP_COPIES_WCP  */
  YYSYMBOL_PATHJUMPOP_WCP = 380,           /* PATHJUMPOP_WCP  */
  YYSYMBOL_PATHJUMPALLOWDIRS_WCP = 381,    /* PATHJUMPALLOWDIRS_WCP  */
  YYSYMBOL_CLIPBOARDOP_WCP = 382,          /* CLIPBOARDOP_WCP  */
  YYSYMBOL_CLIPBOARDSTRING_WCP = 383,      /* CLIPBOARDSTRING_WCP  */
  YYSYMBOL_COMMANDSTRING_WCP = 384,        /* COMMANDSTRING_WCP  */
  YYSYMBOL_EVALCOMMAND_WCP = 385,          /* EVALCOMMAND_WCP  */
  YYSYMBOL_WATCHMODE_WCP = 386,            /* WATCHMODE_WCP  */
  YYSYMBOL_APPLY_WINDOW_DIALOG_TYPE_WCP = 387, /* APPLY_WINDOW_DIALOG_TYPE_WCP  */
  YYSYMBOL_FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP = 388, /* FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP  */
  YYSYMBOL_COMMANDMENUOP_WCP = 389,        /* COMMANDMENUOP_WCP  */
  YYSYMBOL_SEARCHMODEONKEYPRESS_WCP = 390, /* SEARCHMODEONKEYPRESS_WCP  */
  YYSYMBOL_PATHENTRYONTOP_WCP = 391,       /* PATHENTRYONTOP_WCP  */
  YYSYMBOL_STORE_FILES_ALWAYS_WCP = 392,   /* STORE_FILES_ALWAYS_WCP  */
  YYSYMBOL_PATHJUMPSETS_WCP = 393,         /* PATHJUMPSETS_WCP  */
  YYSYMBOL_SHOWDOTDOT_WCP = 394,           /* SHOWDOTDOT_WCP  */
  YYSYMBOL_SHOWBREADCRUMB_WCP = 395,       /* SHOWBREADCRUMB_WCP  */
  YYSYMBOL_FACES_WCP = 396,                /* FACES_WCP  */
  YYSYMBOL_FACE_WCP = 397,                 /* FACE_WCP  */
  YYSYMBOL_ENABLE_INFO_LINE_WCP = 398,     /* ENABLE_INFO_LINE_WCP  */
  YYSYMBOL_INFO_LINE_LUA_MODE_WCP = 399,   /* INFO_LINE_LUA_MODE_WCP  */
  YYSYMBOL_INFO_LINE_CONTENT_WCP = 400,    /* INFO_LINE_CONTENT_WCP  */
  YYSYMBOL_RESTORE_TABS_MODE_WCP = 401,    /* RESTORE_TABS_MODE_WCP  */
  YYSYMBOL_STORE_TABS_MODE_WCP = 402,      /* STORE_TABS_MODE_WCP  */
  YYSYMBOL_AS_EXIT_STATE_WCP = 403,        /* AS_EXIT_STATE_WCP  */
  YYSYMBOL_USE_EXTENDED_REGEX_WCP = 404,   /* USE_EXTENDED_REGEX_WCP  */
  YYSYMBOL_HIGHLIGHT_USER_ACTION_WCP = 405, /* HIGHLIGHT_USER_ACTION_WCP  */
  YYSYMBOL_CHTIMEOP_WCP = 406,             /* CHTIMEOP_WCP  */
  YYSYMBOL_ADJUSTRELATIVESYMLINKS_WCP = 407, /* ADJUSTRELATIVESYMLINKS_WCP  */
  YYSYMBOL_OUTSIDE_WCP = 408,              /* OUTSIDE_WCP  */
  YYSYMBOL_EDIT_WCP = 409,                 /* EDIT_WCP  */
  YYSYMBOL_MAKEABSOLUTE_WCP = 410,         /* MAKEABSOLUTE_WCP  */
  YYSYMBOL_MAKERELATIVE_WCP = 411,         /* MAKERELATIVE_WCP  */
  YYSYMBOL_MAGICIGNORECASE_WCP = 412,      /* MAGICIGNORECASE_WCP  */
  YYSYMBOL_ENSURE_FILE_PERMISSIONS_WCP = 413, /* ENSURE_FILE_PERMISSIONS_WCP  */
  YYSYMBOL_USER_RW_WCP = 414,              /* USER_RW_WCP  */
  YYSYMBOL_USER_RW_GROUP_R_WCP = 415,      /* USER_RW_GROUP_R_WCP  */
  YYSYMBOL_USER_RW_ALL_R_WCP = 416,        /* USER_RW_ALL_R_WCP  */
  YYSYMBOL_LEAVE_UNMODIFIED_WCP = 417,     /* LEAVE_UNMODIFIED_WCP  */
  YYSYMBOL_PREFERUDISKSVERSION_WCP = 418,  /* PREFERUDISKSVERSION_WCP  */
  YYSYMBOL_CHANGECOLUMNSOP_WCP = 419,      /* CHANGECOLUMNSOP_WCP  */
  YYSYMBOL_PATTERNISCOMMASEPARATED_WCP = 420, /* PATTERNISCOMMASEPARATED_WCP  */
  YYSYMBOL_STRCASECMP_WCP = 421,           /* STRCASECMP_WCP  */
  YYSYMBOL_USE_STRING_COMPARE_MODE_WCP = 422, /* USE_STRING_COMPARE_MODE_WCP  */
  YYSYMBOL_VIEWNEWESTFILESOP_WCP = 423,    /* VIEWNEWESTFILESOP_WCP  */
  YYSYMBOL_SHOWRECENT_WCP = 424,           /* SHOWRECENT_WCP  */
  YYSYMBOL_DIRCOMPAREOP_WCP = 425,         /* DIRCOMPAREOP_WCP  */
  YYSYMBOL_TABPROFILESOP_WCP = 426,        /* TABPROFILESOP_WCP  */
  YYSYMBOL_EXTERNALVDIROP_WCP = 427,       /* EXTERNALVDIROP_WCP  */
  YYSYMBOL_VDIR_PRESERVE_DIR_STRUCTURE_WCP = 428, /* VDIR_PRESERVE_DIR_STRUCTURE_WCP  */
  YYSYMBOL_OPENTABMENUOP_WCP = 429,        /* OPENTABMENUOP_WCP  */
  YYSYMBOL_INITIALTAB_WCP = 430,           /* INITIALTAB_WCP  */
  YYSYMBOL_SHOWBYTIME_WCP = 431,           /* SHOWBYTIME_WCP  */
  YYSYMBOL_SHOWBYFILTER_WCP = 432,         /* SHOWBYFILTER_WCP  */
  YYSYMBOL_SHOWBYPROGRAM_WCP = 433,        /* SHOWBYPROGRAM_WCP  */
  YYSYMBOL_TERMINAL_RETURNS_EARLY_WCP = 434, /* TERMINAL_RETURNS_EARLY_WCP  */
  YYSYMBOL_DIRECTORYPRESETS_WCP = 435,     /* DIRECTORYPRESETS_WCP  */
  YYSYMBOL_DIRECTORYPRESET_WCP = 436,      /* DIRECTORYPRESET_WCP  */
  YYSYMBOL_HELPOP_WCP = 437,               /* HELPOP_WCP  */
  YYSYMBOL_PRIORITY_WCP = 438,             /* PRIORITY_WCP  */
  YYSYMBOL_AUTOFILTER_WCP = 439,           /* AUTOFILTER_WCP  */
  YYSYMBOL_ENABLE_CUSTOM_LVB_LINE_WCP = 440, /* ENABLE_CUSTOM_LVB_LINE_WCP  */
  YYSYMBOL_CUSTOM_LVB_LINE_WCP = 441,      /* CUSTOM_LVB_LINE_WCP  */
  YYSYMBOL_CUSTOM_DIRECTORY_INFO_COMMAND_WCP = 442, /* CUSTOM_DIRECTORY_INFO_COMMAND_WCP  */
  YYSYMBOL_IMMEDIATE_FILTER_APPLY_WCP = 443, /* IMMEDIATE_FILTER_APPLY_WCP  */
  YYSYMBOL_DISABLEBGCHECKPREFIX_WCP = 444, /* DISABLEBGCHECKPREFIX_WCP  */
  YYSYMBOL_DISPLAYMODE_WCP = 445,          /* DISPLAYMODE_WCP  */
  YYSYMBOL_SHOWSUBDIRS_WCP = 446,          /* SHOWSUBDIRS_WCP  */
  YYSYMBOL_SHOWDIRECTSUBDIRS_WCP = 447,    /* SHOWDIRECTSUBDIRS_WCP  */
  YYSYMBOL_INCLUDEALL_WCP = 448,           /* INCLUDEALL_WCP  */
  YYSYMBOL_APPLYMODE_WCP = 449,            /* APPLYMODE_WCP  */
  YYSYMBOL_ADD_WCP = 450,                  /* ADD_WCP  */
  YYSYMBOL_REMOVE_WCP = 451,               /* REMOVE_WCP  */
  YYSYMBOL_HIDENONEXISTING_WCP = 452,      /* HIDENONEXISTING_WCP  */
  YYSYMBOL_VIEWCOMMANDLOGOP_WCP = 453,     /* VIEWCOMMANDLOGOP_WCP  */
  YYSYMBOL_LOCKONCURRENTDIR_WCP = 454,     /* LOCKONCURRENTDIR_WCP  */
  YYSYMBOL_STARTNODE_WCP = 455,            /* STARTNODE_WCP  */
  YYSYMBOL_MENUS_WCP = 456,                /* MENUS_WCP  */
  YYSYMBOL_LVMODES_WCP = 457,              /* LVMODES_WCP  */
  YYSYMBOL_LVCOMMANDS_WCP = 458,           /* LVCOMMANDS_WCP  */
  YYSYMBOL_TOPLEVEL_WCP = 459,             /* TOPLEVEL_WCP  */
  YYSYMBOL_FOLLOWACTIVE_WCP = 460,         /* FOLLOWACTIVE_WCP  */
  YYSYMBOL_WATCHFILE_WCP = 461,            /* WATCHFILE_WCP  */
  YYSYMBOL_ACTIVATETEXTVIEWMODEOP_WCP = 462, /* ACTIVATETEXTVIEWMODEOP_WCP  */
  YYSYMBOL_SOURCEMODE_WCP = 463,           /* SOURCEMODE_WCP  */
  YYSYMBOL_ACTIVEFILE_WCP = 464,           /* ACTIVEFILE_WCP  */
  YYSYMBOL_FILETYPEACTION_WCP = 465,       /* FILETYPEACTION_WCP  */
  YYSYMBOL_IGNOREDIRS2_WCP = 466,          /* IGNOREDIRS2_WCP  */
  YYSYMBOL_IGNOREDIR_WCP = 467,            /* IGNOREDIR_WCP  */
  YYSYMBOL_CUSTOMATTRIBUTE_WCP = 468,      /* CUSTOMATTRIBUTE_WCP  */
  YYSYMBOL_STARTSEARCHSTRING_WCP = 469,    /* STARTSEARCHSTRING_WCP  */
  YYSYMBOL_APPLYONLY_WCP = 470,            /* APPLYONLY_WCP  */
  YYSYMBOL_USESELECTEDENTRIES_WCP = 471,   /* USESELECTEDENTRIES_WCP  */
  YYSYMBOL_ONLYEXACTPATH_WCP = 472,        /* ONLYEXACTPATH_WCP  */
  YYSYMBOL_ALSOSELECTEXTENSION_WCP = 473,  /* ALSOSELECTEXTENSION_WCP  */
  YYSYMBOL_STRING_WCP = 474,               /* STRING_WCP  */
  YYSYMBOL_NUM_WCP = 475,                  /* NUM_WCP  */
  YYSYMBOL_476_ = 476,                     /* '.'  */
  YYSYMBOL_477_ = 477,                     /* ';'  */
  YYSYMBOL_478_ = 478,                     /* '='  */
  YYSYMBOL_479_ = 479,                     /* ','  */
  YYSYMBOL_YYACCEPT = 480,                 /* $accept  */
  YYSYMBOL_S = 481,                        /* S  */
  YYSYMBOL_482_1 = 482,                    /* $@1  */
  YYSYMBOL_483_2 = 483,                    /* $@2  */
  YYSYMBOL_start = 484,                    /* start  */
  YYSYMBOL_485_3 = 485,                    /* $@3  */
  YYSYMBOL_486_4 = 486,                    /* $@4  */
  YYSYMBOL_487_5 = 487,                    /* $@5  */
  YYSYMBOL_488_6 = 488,                    /* $@6  */
  YYSYMBOL_489_7 = 489,                    /* $@7  */
  YYSYMBOL_490_8 = 490,                    /* $@8  */
  YYSYMBOL_491_9 = 491,                    /* $@9  */
  YYSYMBOL_492_10 = 492,                   /* $@10  */
  YYSYMBOL_493_11 = 493,                   /* $@11  */
  YYSYMBOL_494_12 = 494,                   /* $@12  */
  YYSYMBOL_495_13 = 495,                   /* $@13  */
  YYSYMBOL_496_14 = 496,                   /* $@14  */
  YYSYMBOL_497_15 = 497,                   /* $@15  */
  YYSYMBOL_498_16 = 498,                   /* $@16  */
  YYSYMBOL_pjallowdirs = 499,              /* pjallowdirs  */
  YYSYMBOL_500_17 = 500,                   /* $@17  */
  YYSYMBOL_pathjumpsets = 501,             /* pathjumpsets  */
  YYSYMBOL_502_18 = 502,                   /* $@18  */
  YYSYMBOL_glb = 503,                      /* glb  */
  YYSYMBOL_504_19 = 504,                   /* $@19  */
  YYSYMBOL_505_20 = 505,                   /* $@20  */
  YYSYMBOL_506_21 = 506,                   /* $@21  */
  YYSYMBOL_507_22 = 507,                   /* $@22  */
  YYSYMBOL_508_23 = 508,                   /* $@23  */
  YYSYMBOL_509_24 = 509,                   /* $@24  */
  YYSYMBOL_510_25 = 510,                   /* $@25  */
  YYSYMBOL_511_26 = 511,                   /* $@26  */
  YYSYMBOL_512_27 = 512,                   /* $@27  */
  YYSYMBOL_513_28 = 513,                   /* $@28  */
  YYSYMBOL_514_29 = 514,                   /* $@29  */
  YYSYMBOL_515_30 = 515,                   /* $@30  */
  YYSYMBOL_516_31 = 516,                   /* $@31  */
  YYSYMBOL_517_32 = 517,                   /* $@32  */
  YYSYMBOL_518_33 = 518,                   /* $@33  */
  YYSYMBOL_519_34 = 519,                   /* $@34  */
  YYSYMBOL_520_35 = 520,                   /* $@35  */
  YYSYMBOL_layout = 521,                   /* layout  */
  YYSYMBOL_522_36 = 522,                   /* $@36  */
  YYSYMBOL_523_37 = 523,                   /* $@37  */
  YYSYMBOL_524_38 = 524,                   /* $@38  */
  YYSYMBOL_525_39 = 525,                   /* $@39  */
  YYSYMBOL_526_40 = 526,                   /* $@40  */
  YYSYMBOL_527_41 = 527,                   /* $@41  */
  YYSYMBOL_528_42 = 528,                   /* $@42  */
  YYSYMBOL_529_43 = 529,                   /* $@43  */
  YYSYMBOL_530_44 = 530,                   /* $@44  */
  YYSYMBOL_531_45 = 531,                   /* $@45  */
  YYSYMBOL_532_46 = 532,                   /* $@46  */
  YYSYMBOL_533_47 = 533,                   /* $@47  */
  YYSYMBOL_534_48 = 534,                   /* $@48  */
  YYSYMBOL_pal = 535,                      /* pal  */
  YYSYMBOL_536_49 = 536,                   /* $@49  */
  YYSYMBOL_listersets = 537,               /* listersets  */
  YYSYMBOL_538_50 = 538,                   /* $@50  */
  YYSYMBOL_539_51 = 539,                   /* $@51  */
  YYSYMBOL_listersets2 = 540,              /* listersets2  */
  YYSYMBOL_541_52 = 541,                   /* $@52  */
  YYSYMBOL_542_53 = 542,                   /* $@53  */
  YYSYMBOL_543_54 = 543,                   /* $@54  */
  YYSYMBOL_544_55 = 544,                   /* $@55  */
  YYSYMBOL_545_56 = 545,                   /* $@56  */
  YYSYMBOL_546_57 = 546,                   /* $@57  */
  YYSYMBOL_547_58 = 547,                   /* $@58  */
  YYSYMBOL_displaysets = 548,              /* displaysets  */
  YYSYMBOL_549_59 = 549,                   /* $@59  */
  YYSYMBOL_550_60 = 550,                   /* $@60  */
  YYSYMBOL_551_61 = 551,                   /* $@61  */
  YYSYMBOL_552_62 = 552,                   /* $@62  */
  YYSYMBOL_553_63 = 553,                   /* $@63  */
  YYSYMBOL_554_64 = 554,                   /* $@64  */
  YYSYMBOL_555_65 = 555,                   /* $@65  */
  YYSYMBOL_556_66 = 556,                   /* $@66  */
  YYSYMBOL_557_67 = 557,                   /* $@67  */
  YYSYMBOL_558_68 = 558,                   /* $@68  */
  YYSYMBOL_559_69 = 559,                   /* $@69  */
  YYSYMBOL_560_70 = 560,                   /* $@70  */
  YYSYMBOL_561_71 = 561,                   /* $@71  */
  YYSYMBOL_562_72 = 562,                   /* $@72  */
  YYSYMBOL_563_73 = 563,                   /* $@73  */
  YYSYMBOL_timesets = 564,                 /* timesets  */
  YYSYMBOL_565_74 = 565,                   /* $@74  */
  YYSYMBOL_566_75 = 566,                   /* $@75  */
  YYSYMBOL_567_76 = 567,                   /* $@76  */
  YYSYMBOL_568_77 = 568,                   /* $@77  */
  YYSYMBOL_569_78 = 569,                   /* $@78  */
  YYSYMBOL_570_79 = 570,                   /* $@79  */
  YYSYMBOL_571_80 = 571,                   /* $@80  */
  YYSYMBOL_572_81 = 572,                   /* $@81  */
  YYSYMBOL_573_82 = 573,                   /* $@82  */
  YYSYMBOL_574_83 = 574,                   /* $@83  */
  YYSYMBOL_575_84 = 575,                   /* $@84  */
  YYSYMBOL_mouseconf = 576,                /* mouseconf  */
  YYSYMBOL_577_85 = 577,                   /* $@85  */
  YYSYMBOL_578_86 = 578,                   /* $@86  */
  YYSYMBOL_579_87 = 579,                   /* $@87  */
  YYSYMBOL_580_88 = 580,                   /* $@88  */
  YYSYMBOL_581_89 = 581,                   /* $@89  */
  YYSYMBOL_582_90 = 582,                   /* $@90  */
  YYSYMBOL_583_91 = 583,                   /* $@91  */
  YYSYMBOL_584_92 = 584,                   /* $@92  */
  YYSYMBOL_585_93 = 585,                   /* $@93  */
  YYSYMBOL_586_94 = 586,                   /* $@94  */
  YYSYMBOL_587_95 = 587,                   /* $@95  */
  YYSYMBOL_588_96 = 588,                   /* $@96  */
  YYSYMBOL_589_97 = 589,                   /* $@97  */
  YYSYMBOL_cols = 590,                     /* cols  */
  YYSYMBOL_591_98 = 591,                   /* $@98  */
  YYSYMBOL_592_99 = 592,                   /* $@99  */
  YYSYMBOL_593_100 = 593,                  /* $@100  */
  YYSYMBOL_594_101 = 594,                  /* $@101  */
  YYSYMBOL_595_102 = 595,                  /* $@102  */
  YYSYMBOL_596_103 = 596,                  /* $@103  */
  YYSYMBOL_597_104 = 597,                  /* $@104  */
  YYSYMBOL_598_105 = 598,                  /* $@105  */
  YYSYMBOL_599_106 = 599,                  /* $@106  */
  YYSYMBOL_600_107 = 600,                  /* $@107  */
  YYSYMBOL_601_108 = 601,                  /* $@108  */
  YYSYMBOL_602_109 = 602,                  /* $@109  */
  YYSYMBOL_603_110 = 603,                  /* $@110  */
  YYSYMBOL_604_111 = 604,                  /* $@111  */
  YYSYMBOL_605_112 = 605,                  /* $@112  */
  YYSYMBOL_606_113 = 606,                  /* $@113  */
  YYSYMBOL_607_114 = 607,                  /* $@114  */
  YYSYMBOL_608_115 = 608,                  /* $@115  */
  YYSYMBOL_609_116 = 609,                  /* $@116  */
  YYSYMBOL_610_117 = 610,                  /* $@117  */
  YYSYMBOL_labelcolors = 611,              /* labelcolors  */
  YYSYMBOL_612_118 = 612,                  /* $@118  */
  YYSYMBOL_613_119 = 613,                  /* $@119  */
  YYSYMBOL_labelcolor = 614,               /* labelcolor  */
  YYSYMBOL_615_120 = 615,                  /* $@120  */
  YYSYMBOL_616_121 = 616,                  /* $@121  */
  YYSYMBOL_617_122 = 617,                  /* $@122  */
  YYSYMBOL_618_123 = 618,                  /* $@123  */
  YYSYMBOL_faces = 619,                    /* faces  */
  YYSYMBOL_620_124 = 620,                  /* $@124  */
  YYSYMBOL_621_125 = 621,                  /* $@125  */
  YYSYMBOL_face = 622,                     /* face  */
  YYSYMBOL_623_126 = 623,                  /* $@126  */
  YYSYMBOL_624_127 = 624,                  /* $@127  */
  YYSYMBOL_startup = 625,                  /* startup  */
  YYSYMBOL_626_128 = 626,                  /* $@128  */
  YYSYMBOL_627_129 = 627,                  /* $@129  */
  YYSYMBOL_628_130 = 628,                  /* $@130  */
  YYSYMBOL_629_131 = 629,                  /* $@131  */
  YYSYMBOL_630_132 = 630,                  /* $@132  */
  YYSYMBOL_631_133 = 631,                  /* $@133  */
  YYSYMBOL_632_134 = 632,                  /* $@134  */
  YYSYMBOL_633_135 = 633,                  /* $@135  */
  YYSYMBOL_634_136 = 634,                  /* $@136  */
  YYSYMBOL_paths = 635,                    /* paths  */
  YYSYMBOL_636_137 = 636,                  /* $@137  */
  YYSYMBOL_637_138 = 637,                  /* $@138  */
  YYSYMBOL_path = 638,                     /* path  */
  YYSYMBOL_639_139 = 639,                  /* $@139  */
  YYSYMBOL_640_140 = 640,                  /* $@140  */
  YYSYMBOL_641_141 = 641,                  /* $@141  */
  YYSYMBOL_642_142 = 642,                  /* $@142  */
  YYSYMBOL_643_143 = 643,                  /* $@143  */
  YYSYMBOL_644_144 = 644,                  /* $@144  */
  YYSYMBOL_keylist = 645,                  /* keylist  */
  YYSYMBOL_646_145 = 646,                  /* $@145  */
  YYSYMBOL_647_146 = 647,                  /* $@146  */
  YYSYMBOL_648_147 = 648,                  /* $@147  */
  YYSYMBOL_649_148 = 649,                  /* $@148  */
  YYSYMBOL_650_149 = 650,                  /* $@149  */
  YYSYMBOL_651_150 = 651,                  /* $@150  */
  YYSYMBOL_652_151 = 652,                  /* $@151  */
  YYSYMBOL_653_152 = 653,                  /* $@152  */
  YYSYMBOL_654_153 = 654,                  /* $@153  */
  YYSYMBOL_655_154 = 655,                  /* $@154  */
  YYSYMBOL_mods = 656,                     /* mods  */
  YYSYMBOL_657_155 = 657,                  /* $@155  */
  YYSYMBOL_658_156 = 658,                  /* $@156  */
  YYSYMBOL_659_157 = 659,                  /* $@157  */
  YYSYMBOL_660_158 = 660,                  /* $@158  */
  YYSYMBOL_661_159 = 661,                  /* $@159  */
  YYSYMBOL_662_160 = 662,                  /* $@160  */
  YYSYMBOL_663_161 = 663,                  /* $@161  */
  YYSYMBOL_664_162 = 664,                  /* $@162  */
  YYSYMBOL_filetypes = 665,                /* filetypes  */
  YYSYMBOL_666_163 = 666,                  /* $@163  */
  YYSYMBOL_667_164 = 667,                  /* $@164  */
  YYSYMBOL_668_165 = 668,                  /* $@165  */
  YYSYMBOL_669_166 = 669,                  /* $@166  */
  YYSYMBOL_670_167 = 670,                  /* $@167  */
  YYSYMBOL_671_168 = 671,                  /* $@168  */
  YYSYMBOL_672_169 = 672,                  /* $@169  */
  YYSYMBOL_ignoredirs = 673,               /* ignoredirs  */
  YYSYMBOL_674_170 = 674,                  /* $@170  */
  YYSYMBOL_ignoredirs2 = 675,              /* ignoredirs2  */
  YYSYMBOL_676_171 = 676,                  /* $@171  */
  YYSYMBOL_677_172 = 677,                  /* $@172  */
  YYSYMBOL_ignoredir = 678,                /* ignoredir  */
  YYSYMBOL_679_173 = 679,                  /* $@173  */
  YYSYMBOL_subfiletypes = 680,             /* subfiletypes  */
  YYSYMBOL_681_174 = 681,                  /* $@174  */
  YYSYMBOL_682_175 = 682,                  /* $@175  */
  YYSYMBOL_filetype = 683,                 /* filetype  */
  YYSYMBOL_684_176 = 684,                  /* $@176  */
  YYSYMBOL_685_177 = 685,                  /* $@177  */
  YYSYMBOL_686_178 = 686,                  /* $@178  */
  YYSYMBOL_687_179 = 687,                  /* $@179  */
  YYSYMBOL_688_180 = 688,                  /* $@180  */
  YYSYMBOL_689_181 = 689,                  /* $@181  */
  YYSYMBOL_690_182 = 690,                  /* $@182  */
  YYSYMBOL_691_183 = 691,                  /* $@183  */
  YYSYMBOL_692_184 = 692,                  /* $@184  */
  YYSYMBOL_693_185 = 693,                  /* $@185  */
  YYSYMBOL_694_186 = 694,                  /* $@186  */
  YYSYMBOL_695_187 = 695,                  /* $@187  */
  YYSYMBOL_696_188 = 696,                  /* $@188  */
  YYSYMBOL_697_189 = 697,                  /* $@189  */
  YYSYMBOL_698_190 = 698,                  /* $@190  */
  YYSYMBOL_699_191 = 699,                  /* $@191  */
  YYSYMBOL_700_192 = 700,                  /* $@192  */
  YYSYMBOL_701_193 = 701,                  /* $@193  */
  YYSYMBOL_702_194 = 702,                  /* $@194  */
  YYSYMBOL_703_195 = 703,                  /* $@195  */
  YYSYMBOL_704_196 = 704,                  /* $@196  */
  YYSYMBOL_705_197 = 705,                  /* $@197  */
  YYSYMBOL_706_198 = 706,                  /* $@198  */
  YYSYMBOL_707_199 = 707,                  /* $@199  */
  YYSYMBOL_708_200 = 708,                  /* $@200  */
  YYSYMBOL_709_201 = 709,                  /* $@201  */
  YYSYMBOL_710_202 = 710,                  /* $@202  */
  YYSYMBOL_filecontent = 711,              /* filecontent  */
  YYSYMBOL_712_203 = 712,                  /* $@203  */
  YYSYMBOL_ft_type = 713,                  /* ft_type  */
  YYSYMBOL_ftcommands = 714,               /* ftcommands  */
  YYSYMBOL_715_204 = 715,                  /* $@204  */
  YYSYMBOL_716_205 = 716,                  /* $@205  */
  YYSYMBOL_717_206 = 717,                  /* $@206  */
  YYSYMBOL_718_207 = 718,                  /* $@207  */
  YYSYMBOL_719_208 = 719,                  /* $@208  */
  YYSYMBOL_720_209 = 720,                  /* $@209  */
  YYSYMBOL_721_210 = 721,                  /* $@210  */
  YYSYMBOL_722_211 = 722,                  /* $@211  */
  YYSYMBOL_723_212 = 723,                  /* $@212  */
  YYSYMBOL_724_213 = 724,                  /* $@213  */
  YYSYMBOL_725_214 = 725,                  /* $@214  */
  YYSYMBOL_726_215 = 726,                  /* $@215  */
  YYSYMBOL_commands = 727,                 /* commands  */
  YYSYMBOL_flags = 728,                    /* flags  */
  YYSYMBOL_hotkeys = 729,                  /* hotkeys  */
  YYSYMBOL_730_216 = 730,                  /* $@216  */
  YYSYMBOL_731_217 = 731,                  /* $@217  */
  YYSYMBOL_hotkey = 732,                   /* hotkey  */
  YYSYMBOL_733_218 = 733,                  /* $@218  */
  YYSYMBOL_734_219 = 734,                  /* $@219  */
  YYSYMBOL_735_220 = 735,                  /* $@220  */
  YYSYMBOL_736_221 = 736,                  /* $@221  */
  YYSYMBOL_737_222 = 737,                  /* $@222  */
  YYSYMBOL_buttons = 738,                  /* buttons  */
  YYSYMBOL_739_223 = 739,                  /* $@223  */
  YYSYMBOL_740_224 = 740,                  /* $@224  */
  YYSYMBOL_button = 741,                   /* button  */
  YYSYMBOL_742_225 = 742,                  /* $@225  */
  YYSYMBOL_743_226 = 743,                  /* $@226  */
  YYSYMBOL_744_227 = 744,                  /* $@227  */
  YYSYMBOL_745_228 = 745,                  /* $@228  */
  YYSYMBOL_746_229 = 746,                  /* $@229  */
  YYSYMBOL_747_230 = 747,                  /* $@230  */
  YYSYMBOL_748_231 = 748,                  /* $@231  */
  YYSYMBOL_fonts = 749,                    /* fonts  */
  YYSYMBOL_750_232 = 750,                  /* $@232  */
  YYSYMBOL_751_233 = 751,                  /* $@233  */
  YYSYMBOL_752_234 = 752,                  /* $@234  */
  YYSYMBOL_753_235 = 753,                  /* $@235  */
  YYSYMBOL_754_236 = 754,                  /* $@236  */
  YYSYMBOL_755_237 = 755,                  /* $@237  */
  YYSYMBOL_756_238 = 756,                  /* $@238  */
  YYSYMBOL_xftfonts = 757,                 /* xftfonts  */
  YYSYMBOL_758_239 = 758,                  /* $@239  */
  YYSYMBOL_759_240 = 759,                  /* $@240  */
  YYSYMBOL_760_241 = 760,                  /* $@241  */
  YYSYMBOL_761_242 = 761,                  /* $@242  */
  YYSYMBOL_762_243 = 762,                  /* $@243  */
  YYSYMBOL_763_244 = 763,                  /* $@244  */
  YYSYMBOL_764_245 = 764,                  /* $@245  */
  YYSYMBOL_volumemanager = 765,            /* volumemanager  */
  YYSYMBOL_766_246 = 766,                  /* $@246  */
  YYSYMBOL_767_247 = 767,                  /* $@247  */
  YYSYMBOL_768_248 = 768,                  /* $@248  */
  YYSYMBOL_769_249 = 769,                  /* $@249  */
  YYSYMBOL_770_250 = 770,                  /* $@250  */
  YYSYMBOL_771_251 = 771,                  /* $@251  */
  YYSYMBOL_772_252 = 772,                  /* $@252  */
  YYSYMBOL_773_253 = 773,                  /* $@253  */
  YYSYMBOL_774_254 = 774,                  /* $@254  */
  YYSYMBOL_clockbarsets = 775,             /* clockbarsets  */
  YYSYMBOL_776_255 = 776,                  /* $@255  */
  YYSYMBOL_777_256 = 777,                  /* $@256  */
  YYSYMBOL_778_257 = 778,                  /* $@257  */
  YYSYMBOL_779_258 = 779,                  /* $@258  */
  YYSYMBOL_780_259 = 780,                  /* $@259  */
  YYSYMBOL_781_260 = 781,                  /* $@260  */
  YYSYMBOL_782_261 = 782,                  /* $@261  */
  YYSYMBOL_bool = 783,                     /* bool  */
  YYSYMBOL_command = 784,                  /* command  */
  YYSYMBOL_ownop = 785,                    /* ownop  */
  YYSYMBOL_786_262 = 786,                  /* $@262  */
  YYSYMBOL_ownopbody = 787,                /* ownopbody  */
  YYSYMBOL_788_263 = 788,                  /* $@263  */
  YYSYMBOL_789_264 = 789,                  /* $@264  */
  YYSYMBOL_790_265 = 790,                  /* $@265  */
  YYSYMBOL_791_266 = 791,                  /* $@266  */
  YYSYMBOL_792_267 = 792,                  /* $@267  */
  YYSYMBOL_793_268 = 793,                  /* $@268  */
  YYSYMBOL_794_269 = 794,                  /* $@269  */
  YYSYMBOL_795_270 = 795,                  /* $@270  */
  YYSYMBOL_796_271 = 796,                  /* $@271  */
  YYSYMBOL_797_272 = 797,                  /* $@272  */
  YYSYMBOL_798_273 = 798,                  /* $@273  */
  YYSYMBOL_799_274 = 799,                  /* $@274  */
  YYSYMBOL_800_275 = 800,                  /* $@275  */
  YYSYMBOL_801_276 = 801,                  /* $@276  */
  YYSYMBOL_802_277 = 802,                  /* $@277  */
  YYSYMBOL_803_278 = 803,                  /* $@278  */
  YYSYMBOL_804_279 = 804,                  /* $@279  */
  YYSYMBOL_805_280 = 805,                  /* $@280  */
  YYSYMBOL_dndaction = 806,                /* dndaction  */
  YYSYMBOL_807_281 = 807,                  /* $@281  */
  YYSYMBOL_dcaction = 808,                 /* dcaction  */
  YYSYMBOL_809_282 = 809,                  /* $@282  */
  YYSYMBOL_showaction = 810,               /* showaction  */
  YYSYMBOL_811_283 = 811,                  /* $@283  */
  YYSYMBOL_rshowaction = 812,              /* rshowaction  */
  YYSYMBOL_813_284 = 813,                  /* $@284  */
  YYSYMBOL_useraction = 814,               /* useraction  */
  YYSYMBOL_815_285 = 815,                  /* $@285  */
  YYSYMBOL_useractionbody = 816,           /* useractionbody  */
  YYSYMBOL_817_286 = 817,                  /* $@286  */
  YYSYMBOL_rowup = 818,                    /* rowup  */
  YYSYMBOL_819_287 = 819,                  /* $@287  */
  YYSYMBOL_rowdown = 820,                  /* rowdown  */
  YYSYMBOL_821_288 = 821,                  /* $@288  */
  YYSYMBOL_changehiddenflag = 822,         /* changehiddenflag  */
  YYSYMBOL_823_289 = 823,                  /* $@289  */
  YYSYMBOL_changehiddenflagbody = 824,     /* changehiddenflagbody  */
  YYSYMBOL_825_290 = 825,                  /* $@290  */
  YYSYMBOL_826_291 = 826,                  /* $@291  */
  YYSYMBOL_827_292 = 827,                  /* $@292  */
  YYSYMBOL_copyop = 828,                   /* copyop  */
  YYSYMBOL_829_293 = 829,                  /* $@293  */
  YYSYMBOL_copyopbody = 830,               /* copyopbody  */
  YYSYMBOL_831_294 = 831,                  /* $@294  */
  YYSYMBOL_832_295 = 832,                  /* $@295  */
  YYSYMBOL_833_296 = 833,                  /* $@296  */
  YYSYMBOL_834_297 = 834,                  /* $@297  */
  YYSYMBOL_835_298 = 835,                  /* $@298  */
  YYSYMBOL_836_299 = 836,                  /* $@299  */
  YYSYMBOL_837_300 = 837,                  /* $@300  */
  YYSYMBOL_838_301 = 838,                  /* $@301  */
  YYSYMBOL_839_302 = 839,                  /* $@302  */
  YYSYMBOL_840_303 = 840,                  /* $@303  */
  YYSYMBOL_841_304 = 841,                  /* $@304  */
  YYSYMBOL_842_305 = 842,                  /* $@305  */
  YYSYMBOL_843_306 = 843,                  /* $@306  */
  YYSYMBOL_844_307 = 844,                  /* $@307  */
  YYSYMBOL_845_308 = 845,                  /* $@308  */
  YYSYMBOL_846_309 = 846,                  /* $@309  */
  YYSYMBOL_847_310 = 847,                  /* $@310  */
  YYSYMBOL_848_311 = 848,                  /* $@311  */
  YYSYMBOL_849_312 = 849,                  /* $@312  */
  YYSYMBOL_850_313 = 850,                  /* $@313  */
  YYSYMBOL_firstrow = 851,                 /* firstrow  */
  YYSYMBOL_852_314 = 852,                  /* $@314  */
  YYSYMBOL_lastrow = 853,                  /* lastrow  */
  YYSYMBOL_854_315 = 854,                  /* $@315  */
  YYSYMBOL_pageup = 855,                   /* pageup  */
  YYSYMBOL_856_316 = 856,                  /* $@316  */
  YYSYMBOL_pagedown = 857,                 /* pagedown  */
  YYSYMBOL_858_317 = 858,                  /* $@317  */
  YYSYMBOL_selectop = 859,                 /* selectop  */
  YYSYMBOL_860_318 = 860,                  /* $@318  */
  YYSYMBOL_selectallop = 861,              /* selectallop  */
  YYSYMBOL_862_319 = 862,                  /* $@319  */
  YYSYMBOL_selectnoneop = 863,             /* selectnoneop  */
  YYSYMBOL_864_320 = 864,                  /* $@320  */
  YYSYMBOL_invertallop = 865,              /* invertallop  */
  YYSYMBOL_866_321 = 866,                  /* $@321  */
  YYSYMBOL_parentdirop = 867,              /* parentdirop  */
  YYSYMBOL_868_322 = 868,                  /* $@322  */
  YYSYMBOL_enterdirop = 869,               /* enterdirop  */
  YYSYMBOL_870_323 = 870,                  /* $@323  */
  YYSYMBOL_enterdiropbody = 871,           /* enterdiropbody  */
  YYSYMBOL_872_324 = 872,                  /* $@324  */
  YYSYMBOL_873_325 = 873,                  /* $@325  */
  YYSYMBOL_874_326 = 874,                  /* $@326  */
  YYSYMBOL_875_327 = 875,                  /* $@327  */
  YYSYMBOL_876_328 = 876,                  /* $@328  */
  YYSYMBOL_changelistersetop = 877,        /* changelistersetop  */
  YYSYMBOL_878_329 = 878,                  /* $@329  */
  YYSYMBOL_changelistersetopbody = 879,    /* changelistersetopbody  */
  YYSYMBOL_880_330 = 880,                  /* $@330  */
  YYSYMBOL_881_331 = 881,                  /* $@331  */
  YYSYMBOL_882_332 = 882,                  /* $@332  */
  YYSYMBOL_883_333 = 883,                  /* $@333  */
  YYSYMBOL_switchlisterop = 884,           /* switchlisterop  */
  YYSYMBOL_885_334 = 885,                  /* $@334  */
  YYSYMBOL_filterselectop = 886,           /* filterselectop  */
  YYSYMBOL_887_335 = 887,                  /* $@335  */
  YYSYMBOL_filterselectopbody = 888,       /* filterselectopbody  */
  YYSYMBOL_889_336 = 889,                  /* $@336  */
  YYSYMBOL_890_337 = 890,                  /* $@337  */
  YYSYMBOL_891_338 = 891,                  /* $@338  */
  YYSYMBOL_filterunselectop = 892,         /* filterunselectop  */
  YYSYMBOL_893_339 = 893,                  /* $@339  */
  YYSYMBOL_filterunselectopbody = 894,     /* filterunselectopbody  */
  YYSYMBOL_895_340 = 895,                  /* $@340  */
  YYSYMBOL_896_341 = 896,                  /* $@341  */
  YYSYMBOL_897_342 = 897,                  /* $@342  */
  YYSYMBOL_pathtoothersideop = 898,        /* pathtoothersideop  */
  YYSYMBOL_899_343 = 899,                  /* $@343  */
  YYSYMBOL_quitop = 900,                   /* quitop  */
  YYSYMBOL_901_344 = 901,                  /* $@344  */
  YYSYMBOL_quitopbody = 902,               /* quitopbody  */
  YYSYMBOL_903_345 = 903,                  /* $@345  */
  YYSYMBOL_904_346 = 904,                  /* $@346  */
  YYSYMBOL_deleteop = 905,                 /* deleteop  */
  YYSYMBOL_906_347 = 906,                  /* $@347  */
  YYSYMBOL_deleteopbody = 907,             /* deleteopbody  */
  YYSYMBOL_908_348 = 908,                  /* $@348  */
  YYSYMBOL_reloadop = 909,                 /* reloadop  */
  YYSYMBOL_910_349 = 910,                  /* $@349  */
  YYSYMBOL_reloadopbody = 911,             /* reloadopbody  */
  YYSYMBOL_912_350 = 912,                  /* $@350  */
  YYSYMBOL_913_351 = 913,                  /* $@351  */
  YYSYMBOL_914_352 = 914,                  /* $@352  */
  YYSYMBOL_915_353 = 915,                  /* $@353  */
  YYSYMBOL_916_354 = 916,                  /* $@354  */
  YYSYMBOL_917_355 = 917,                  /* $@355  */
  YYSYMBOL_makedirop = 918,                /* makedirop  */
  YYSYMBOL_919_356 = 919,                  /* $@356  */
  YYSYMBOL_renameop = 920,                 /* renameop  */
  YYSYMBOL_921_357 = 921,                  /* $@357  */
  YYSYMBOL_renameopbody = 922,             /* renameopbody  */
  YYSYMBOL_923_358 = 923,                  /* $@358  */
  YYSYMBOL_dirsizeop = 924,                /* dirsizeop  */
  YYSYMBOL_925_359 = 925,                  /* $@359  */
  YYSYMBOL_simddop = 926,                  /* simddop  */
  YYSYMBOL_927_360 = 927,                  /* $@360  */
  YYSYMBOL_startprogop = 928,              /* startprogop  */
  YYSYMBOL_929_361 = 929,                  /* $@361  */
  YYSYMBOL_startprogopbody = 930,          /* startprogopbody  */
  YYSYMBOL_931_362 = 931,                  /* $@362  */
  YYSYMBOL_932_363 = 932,                  /* $@363  */
  YYSYMBOL_933_364 = 933,                  /* $@364  */
  YYSYMBOL_934_365 = 934,                  /* $@365  */
  YYSYMBOL_935_366 = 935,                  /* $@366  */
  YYSYMBOL_936_367 = 936,                  /* $@367  */
  YYSYMBOL_937_368 = 937,                  /* $@368  */
  YYSYMBOL_938_369 = 938,                  /* $@369  */
  YYSYMBOL_939_370 = 939,                  /* $@370  */
  YYSYMBOL_940_371 = 940,                  /* $@371  */
  YYSYMBOL_941_372 = 941,                  /* $@372  */
  YYSYMBOL_942_373 = 942,                  /* $@373  */
  YYSYMBOL_943_374 = 943,                  /* $@374  */
  YYSYMBOL_944_375 = 944,                  /* $@375  */
  YYSYMBOL_945_376 = 945,                  /* $@376  */
  YYSYMBOL_searchentryop = 946,            /* searchentryop  */
  YYSYMBOL_947_377 = 947,                  /* $@377  */
  YYSYMBOL_searchentryopbody = 948,        /* searchentryopbody  */
  YYSYMBOL_949_378 = 949,                  /* $@378  */
  YYSYMBOL_950_379 = 950,                  /* $@379  */
  YYSYMBOL_951_380 = 951,                  /* $@380  */
  YYSYMBOL_952_381 = 952,                  /* $@381  */
  YYSYMBOL_953_382 = 953,                  /* $@382  */
  YYSYMBOL_954_383 = 954,                  /* $@383  */
  YYSYMBOL_enterpathop = 955,              /* enterpathop  */
  YYSYMBOL_956_384 = 956,                  /* $@384  */
  YYSYMBOL_enterpathopbody = 957,          /* enterpathopbody  */
  YYSYMBOL_958_385 = 958,                  /* $@385  */
  YYSYMBOL_959_386 = 959,                  /* $@386  */
  YYSYMBOL_960_387 = 960,                  /* $@387  */
  YYSYMBOL_961_388 = 961,                  /* $@388  */
  YYSYMBOL_scrolllisterop = 962,           /* scrolllisterop  */
  YYSYMBOL_963_389 = 963,                  /* $@389  */
  YYSYMBOL_scrolllisteropbody = 964,       /* scrolllisteropbody  */
  YYSYMBOL_965_390 = 965,                  /* $@390  */
  YYSYMBOL_966_391 = 966,                  /* $@391  */
  YYSYMBOL_createsymlinkop = 967,          /* createsymlinkop  */
  YYSYMBOL_968_392 = 968,                  /* $@392  */
  YYSYMBOL_createsymlinkopbody = 969,      /* createsymlinkopbody  */
  YYSYMBOL_970_393 = 970,                  /* $@393  */
  YYSYMBOL_971_394 = 971,                  /* $@394  */
  YYSYMBOL_972_395 = 972,                  /* $@395  */
  YYSYMBOL_changesymlinkop = 973,          /* changesymlinkop  */
  YYSYMBOL_974_396 = 974,                  /* $@396  */
  YYSYMBOL_changesymlinkopbody = 975,      /* changesymlinkopbody  */
  YYSYMBOL_976_397 = 976,                  /* $@397  */
  YYSYMBOL_977_398 = 977,                  /* $@398  */
  YYSYMBOL_978_399 = 978,                  /* $@399  */
  YYSYMBOL_chmodop = 979,                  /* chmodop  */
  YYSYMBOL_980_400 = 980,                  /* $@400  */
  YYSYMBOL_chmodopbody = 981,              /* chmodopbody  */
  YYSYMBOL_982_401 = 982,                  /* $@401  */
  YYSYMBOL_983_402 = 983,                  /* $@402  */
  YYSYMBOL_984_403 = 984,                  /* $@403  */
  YYSYMBOL_985_404 = 985,                  /* $@404  */
  YYSYMBOL_986_405 = 986,                  /* $@405  */
  YYSYMBOL_987_406 = 987,                  /* $@406  */
  YYSYMBOL_988_407 = 988,                  /* $@407  */
  YYSYMBOL_989_408 = 989,                  /* $@408  */
  YYSYMBOL_990_409 = 990,                  /* $@409  */
  YYSYMBOL_chtimeop = 991,                 /* chtimeop  */
  YYSYMBOL_992_410 = 992,                  /* $@410  */
  YYSYMBOL_chtimeopbody = 993,             /* chtimeopbody  */
  YYSYMBOL_994_411 = 994,                  /* $@411  */
  YYSYMBOL_995_412 = 995,                  /* $@412  */
  YYSYMBOL_996_413 = 996,                  /* $@413  */
  YYSYMBOL_997_414 = 997,                  /* $@414  */
  YYSYMBOL_togglelistermodeop = 998,       /* togglelistermodeop  */
  YYSYMBOL_999_415 = 999,                  /* $@415  */
  YYSYMBOL_togglelistermodeopbody = 1000,  /* togglelistermodeopbody  */
  YYSYMBOL_1001_416 = 1001,                /* $@416  */
  YYSYMBOL_setsortmodeop = 1002,           /* setsortmodeop  */
  YYSYMBOL_1003_417 = 1003,                /* $@417  */
  YYSYMBOL_setsortmodeopbody = 1004,       /* setsortmodeopbody  */
  YYSYMBOL_1005_418 = 1005,                /* $@418  */
  YYSYMBOL_1006_419 = 1006,                /* $@419  */
  YYSYMBOL_1007_420 = 1007,                /* $@420  */
  YYSYMBOL_1008_421 = 1008,                /* $@421  */
  YYSYMBOL_1009_422 = 1009,                /* $@422  */
  YYSYMBOL_1010_423 = 1010,                /* $@423  */
  YYSYMBOL_1011_424 = 1011,                /* $@424  */
  YYSYMBOL_1012_425 = 1012,                /* $@425  */
  YYSYMBOL_1013_426 = 1013,                /* $@426  */
  YYSYMBOL_1014_427 = 1014,                /* $@427  */
  YYSYMBOL_1015_428 = 1015,                /* $@428  */
  YYSYMBOL_1016_429 = 1016,                /* $@429  */
  YYSYMBOL_1017_430 = 1017,                /* $@430  */
  YYSYMBOL_1018_431 = 1018,                /* $@431  */
  YYSYMBOL_1019_432 = 1019,                /* $@432  */
  YYSYMBOL_setfilterop = 1020,             /* setfilterop  */
  YYSYMBOL_1021_433 = 1021,                /* $@433  */
  YYSYMBOL_setfilteropbody = 1022,         /* setfilteropbody  */
  YYSYMBOL_1023_434 = 1023,                /* $@434  */
  YYSYMBOL_1024_435 = 1024,                /* $@435  */
  YYSYMBOL_1025_436 = 1025,                /* $@436  */
  YYSYMBOL_1026_437 = 1026,                /* $@437  */
  YYSYMBOL_1027_438 = 1027,                /* $@438  */
  YYSYMBOL_1028_439 = 1028,                /* $@439  */
  YYSYMBOL_1029_440 = 1029,                /* $@440  */
  YYSYMBOL_1030_441 = 1030,                /* $@441  */
  YYSYMBOL_1031_442 = 1031,                /* $@442  */
  YYSYMBOL_1032_443 = 1032,                /* $@443  */
  YYSYMBOL_1033_444 = 1033,                /* $@444  */
  YYSYMBOL_1034_445 = 1034,                /* $@445  */
  YYSYMBOL_1035_446 = 1035,                /* $@446  */
  YYSYMBOL_1036_447 = 1036,                /* $@447  */
  YYSYMBOL_1037_448 = 1037,                /* $@448  */
  YYSYMBOL_shortkeyfromlistop = 1038,      /* shortkeyfromlistop  */
  YYSYMBOL_1039_449 = 1039,                /* $@449  */
  YYSYMBOL_chownop = 1040,                 /* chownop  */
  YYSYMBOL_1041_450 = 1041,                /* $@450  */
  YYSYMBOL_chownopbody = 1042,             /* chownopbody  */
  YYSYMBOL_1043_451 = 1043,                /* $@451  */
  YYSYMBOL_1044_452 = 1044,                /* $@452  */
  YYSYMBOL_1045_453 = 1045,                /* $@453  */
  YYSYMBOL_1046_454 = 1046,                /* $@454  */
  YYSYMBOL_scriptop = 1047,                /* scriptop  */
  YYSYMBOL_1048_455 = 1048,                /* $@455  */
  YYSYMBOL_scriptopbody = 1049,            /* scriptopbody  */
  YYSYMBOL_1050_456 = 1050,                /* $@456  */
  YYSYMBOL_1051_457 = 1051,                /* $@457  */
  YYSYMBOL_1052_458 = 1052,                /* $@458  */
  YYSYMBOL_1053_459 = 1053,                /* $@459  */
  YYSYMBOL_1054_460 = 1054,                /* $@460  */
  YYSYMBOL_1055_461 = 1055,                /* $@461  */
  YYSYMBOL_1056_462 = 1056,                /* $@462  */
  YYSYMBOL_1057_463 = 1057,                /* $@463  */
  YYSYMBOL_1058_464 = 1058,                /* $@464  */
  YYSYMBOL_1059_465 = 1059,                /* $@465  */
  YYSYMBOL_1060_466 = 1060,                /* $@466  */
  YYSYMBOL_1061_467 = 1061,                /* $@467  */
  YYSYMBOL_1062_468 = 1062,                /* $@468  */
  YYSYMBOL_1063_469 = 1063,                /* $@469  */
  YYSYMBOL_1064_470 = 1064,                /* $@470  */
  YYSYMBOL_1065_471 = 1065,                /* $@471  */
  YYSYMBOL_1066_472 = 1066,                /* $@472  */
  YYSYMBOL_1067_473 = 1067,                /* $@473  */
  YYSYMBOL_1068_474 = 1068,                /* $@474  */
  YYSYMBOL_1069_475 = 1069,                /* $@475  */
  YYSYMBOL_1070_476 = 1070,                /* $@476  */
  YYSYMBOL_1071_477 = 1071,                /* $@477  */
  YYSYMBOL_1072_478 = 1072,                /* $@478  */
  YYSYMBOL_1073_479 = 1073,                /* $@479  */
  YYSYMBOL_1074_480 = 1074,                /* $@480  */
  YYSYMBOL_1075_481 = 1075,                /* $@481  */
  YYSYMBOL_1076_482 = 1076,                /* $@482  */
  YYSYMBOL_1077_483 = 1077,                /* $@483  */
  YYSYMBOL_1078_484 = 1078,                /* $@484  */
  YYSYMBOL_1079_485 = 1079,                /* $@485  */
  YYSYMBOL_showdircacheop = 1080,          /* showdircacheop  */
  YYSYMBOL_1081_486 = 1081,                /* $@486  */
  YYSYMBOL_parentactionop = 1082,          /* parentactionop  */
  YYSYMBOL_1083_487 = 1083,                /* $@487  */
  YYSYMBOL_nooperationop = 1084,           /* nooperationop  */
  YYSYMBOL_1085_488 = 1085,                /* $@488  */
  YYSYMBOL_goftpop = 1086,                 /* goftpop  */
  YYSYMBOL_1087_489 = 1087,                /* $@489  */
  YYSYMBOL_goftpopbody = 1088,             /* goftpopbody  */
  YYSYMBOL_1089_490 = 1089,                /* $@490  */
  YYSYMBOL_1090_491 = 1090,                /* $@491  */
  YYSYMBOL_1091_492 = 1091,                /* $@492  */
  YYSYMBOL_1092_493 = 1092,                /* $@493  */
  YYSYMBOL_1093_494 = 1093,                /* $@494  */
  YYSYMBOL_1094_495 = 1094,                /* $@495  */
  YYSYMBOL_1095_496 = 1095,                /* $@496  */
  YYSYMBOL_internalviewop = 1096,          /* internalviewop  */
  YYSYMBOL_1097_497 = 1097,                /* $@497  */
  YYSYMBOL_internalviewopbody = 1098,      /* internalviewopbody  */
  YYSYMBOL_1099_498 = 1099,                /* $@498  */
  YYSYMBOL_1100_499 = 1100,                /* $@499  */
  YYSYMBOL_1101_500 = 1101,                /* $@500  */
  YYSYMBOL_1102_501 = 1102,                /* $@501  */
  YYSYMBOL_clipboardop = 1103,             /* clipboardop  */
  YYSYMBOL_1104_502 = 1104,                /* $@502  */
  YYSYMBOL_clipboardopbody = 1105,         /* clipboardopbody  */
  YYSYMBOL_1106_503 = 1106,                /* $@503  */
  YYSYMBOL_searchop = 1107,                /* searchop  */
  YYSYMBOL_1108_504 = 1108,                /* $@504  */
  YYSYMBOL_searchopbody = 1109,            /* searchopbody  */
  YYSYMBOL_1110_505 = 1110,                /* $@505  */
  YYSYMBOL_1111_506 = 1111,                /* $@506  */
  YYSYMBOL_1112_507 = 1112,                /* $@507  */
  YYSYMBOL_dirbookmarkop = 1113,           /* dirbookmarkop  */
  YYSYMBOL_1114_508 = 1114,                /* $@508  */
  YYSYMBOL_opencontextmenuop = 1115,       /* opencontextmenuop  */
  YYSYMBOL_1116_509 = 1116,                /* $@509  */
  YYSYMBOL_opencontextmenuopbody = 1117,   /* opencontextmenuopbody  */
  YYSYMBOL_1118_510 = 1118,                /* $@510  */
  YYSYMBOL_runcustomaction = 1119,         /* runcustomaction  */
  YYSYMBOL_1120_511 = 1120,                /* $@511  */
  YYSYMBOL_runcustomactionbody = 1121,     /* runcustomactionbody  */
  YYSYMBOL_1122_512 = 1122,                /* $@512  */
  YYSYMBOL_openworkermenuop = 1123,        /* openworkermenuop  */
  YYSYMBOL_1124_513 = 1124,                /* $@513  */
  YYSYMBOL_changelabelop = 1125,           /* changelabelop  */
  YYSYMBOL_1126_514 = 1126,                /* $@514  */
  YYSYMBOL_changelabelopbody = 1127,       /* changelabelopbody  */
  YYSYMBOL_1128_515 = 1128,                /* $@515  */
  YYSYMBOL_1129_516 = 1129,                /* $@516  */
  YYSYMBOL_modifytabsop = 1130,            /* modifytabsop  */
  YYSYMBOL_1131_517 = 1131,                /* $@517  */
  YYSYMBOL_modifytabsopbody = 1132,        /* modifytabsopbody  */
  YYSYMBOL_1133_518 = 1133,                /* $@518  */
  YYSYMBOL_1134_519 = 1134,                /* $@519  */
  YYSYMBOL_1135_520 = 1135,                /* $@520  */
  YYSYMBOL_1136_521 = 1136,                /* $@521  */
  YYSYMBOL_1137_522 = 1137,                /* $@522  */
  YYSYMBOL_1138_523 = 1138,                /* $@523  */
  YYSYMBOL_1139_524 = 1139,                /* $@524  */
  YYSYMBOL_changelayoutop = 1140,          /* changelayoutop  */
  YYSYMBOL_1141_525 = 1141,                /* $@525  */
  YYSYMBOL_changelayoutopbody = 1142,      /* changelayoutopbody  */
  YYSYMBOL_1143_526 = 1143,                /* $@526  */
  YYSYMBOL_1144_527 = 1144,                /* $@527  */
  YYSYMBOL_1145_528 = 1145,                /* $@528  */
  YYSYMBOL_1146_529 = 1146,                /* $@529  */
  YYSYMBOL_1147_530 = 1147,                /* $@530  */
  YYSYMBOL_1148_531 = 1148,                /* $@531  */
  YYSYMBOL_1149_532 = 1149,                /* $@532  */
  YYSYMBOL_1150_533 = 1150,                /* $@533  */
  YYSYMBOL_1151_534 = 1151,                /* $@534  */
  YYSYMBOL_1152_535 = 1152,                /* $@535  */
  YYSYMBOL_1153_536 = 1153,                /* $@536  */
  YYSYMBOL_1154_537 = 1154,                /* $@537  */
  YYSYMBOL_1155_538 = 1155,                /* $@538  */
  YYSYMBOL_changecolumnsop = 1156,         /* changecolumnsop  */
  YYSYMBOL_1157_539 = 1157,                /* $@539  */
  YYSYMBOL_changecolumnsopbody = 1158,     /* changecolumnsopbody  */
  YYSYMBOL_1159_540 = 1159,                /* $@540  */
  YYSYMBOL_1160_541 = 1160,                /* $@541  */
  YYSYMBOL_1161_542 = 1161,                /* $@542  */
  YYSYMBOL_1162_543 = 1162,                /* $@543  */
  YYSYMBOL_1163_544 = 1163,                /* $@544  */
  YYSYMBOL_1164_545 = 1164,                /* $@545  */
  YYSYMBOL_1165_546 = 1165,                /* $@546  */
  YYSYMBOL_1166_547 = 1166,                /* $@547  */
  YYSYMBOL_1167_548 = 1167,                /* $@548  */
  YYSYMBOL_1168_549 = 1168,                /* $@549  */
  YYSYMBOL_1169_550 = 1169,                /* $@550  */
  YYSYMBOL_1170_551 = 1170,                /* $@551  */
  YYSYMBOL_1171_552 = 1171,                /* $@552  */
  YYSYMBOL_1172_553 = 1172,                /* $@553  */
  YYSYMBOL_1173_554 = 1173,                /* $@554  */
  YYSYMBOL_volumemanagerop = 1174,         /* volumemanagerop  */
  YYSYMBOL_1175_555 = 1175,                /* $@555  */
  YYSYMBOL_switchbuttonbankop = 1176,      /* switchbuttonbankop  */
  YYSYMBOL_1177_556 = 1177,                /* $@556  */
  YYSYMBOL_switchbuttonbankopbody = 1178,  /* switchbuttonbankopbody  */
  YYSYMBOL_1179_557 = 1179,                /* $@557  */
  YYSYMBOL_1180_558 = 1180,                /* $@558  */
  YYSYMBOL_1181_559 = 1181,                /* $@559  */
  YYSYMBOL_1182_560 = 1182,                /* $@560  */
  YYSYMBOL_pathjumpop = 1183,              /* pathjumpop  */
  YYSYMBOL_1184_561 = 1184,                /* $@561  */
  YYSYMBOL_pathjumpopbody = 1185,          /* pathjumpopbody  */
  YYSYMBOL_1186_562 = 1186,                /* $@562  */
  YYSYMBOL_1187_563 = 1187,                /* $@563  */
  YYSYMBOL_1188_564 = 1188,                /* $@564  */
  YYSYMBOL_1189_565 = 1189,                /* $@565  */
  YYSYMBOL_1190_566 = 1190,                /* $@566  */
  YYSYMBOL_1191_567 = 1191,                /* $@567  */
  YYSYMBOL_1192_568 = 1192,                /* $@568  */
  YYSYMBOL_1193_569 = 1193,                /* $@569  */
  YYSYMBOL_1194_570 = 1194,                /* $@570  */
  YYSYMBOL_commandmenuop = 1195,           /* commandmenuop  */
  YYSYMBOL_1196_571 = 1196,                /* $@571  */
  YYSYMBOL_commandmenuopbody = 1197,       /* commandmenuopbody  */
  YYSYMBOL_1198_572 = 1198,                /* $@572  */
  YYSYMBOL_1199_573 = 1199,                /* $@573  */
  YYSYMBOL_1200_574 = 1200,                /* $@574  */
  YYSYMBOL_1201_575 = 1201,                /* $@575  */
  YYSYMBOL_1202_576 = 1202,                /* $@576  */
  YYSYMBOL_1203_577 = 1203,                /* $@577  */
  YYSYMBOL_1204_578 = 1204,                /* $@578  */
  YYSYMBOL_1205_579 = 1205,                /* $@579  */
  YYSYMBOL_1206_580 = 1206,                /* $@580  */
  YYSYMBOL_viewnewestfilesop = 1207,       /* viewnewestfilesop  */
  YYSYMBOL_1208_581 = 1208,                /* $@581  */
  YYSYMBOL_dircompareop = 1209,            /* dircompareop  */
  YYSYMBOL_1210_582 = 1210,                /* $@582  */
  YYSYMBOL_tabprofilesop = 1211,           /* tabprofilesop  */
  YYSYMBOL_1212_583 = 1212,                /* $@583  */
  YYSYMBOL_externalvdirop = 1213,          /* externalvdirop  */
  YYSYMBOL_1214_584 = 1214,                /* $@584  */
  YYSYMBOL_externalvdiropbody = 1215,      /* externalvdiropbody  */
  YYSYMBOL_1216_585 = 1216,                /* $@585  */
  YYSYMBOL_1217_586 = 1217,                /* $@586  */
  YYSYMBOL_opentabmenuop = 1218,           /* opentabmenuop  */
  YYSYMBOL_1219_587 = 1219,                /* $@587  */
  YYSYMBOL_directorypresets = 1220,        /* directorypresets  */
  YYSYMBOL_1221_588 = 1221,                /* $@588  */
  YYSYMBOL_1222_589 = 1222,                /* $@589  */
  YYSYMBOL_directorypreset = 1223,         /* directorypreset  */
  YYSYMBOL_1224_590 = 1224,                /* $@590  */
  YYSYMBOL_1225_591 = 1225,                /* $@591  */
  YYSYMBOL_1226_592 = 1226,                /* $@592  */
  YYSYMBOL_1227_593 = 1227,                /* $@593  */
  YYSYMBOL_1228_594 = 1228,                /* $@594  */
  YYSYMBOL_1229_595 = 1229,                /* $@595  */
  YYSYMBOL_1230_596 = 1230,                /* $@596  */
  YYSYMBOL_1231_597 = 1231,                /* $@597  */
  YYSYMBOL_1232_598 = 1232,                /* $@598  */
  YYSYMBOL_1233_599 = 1233,                /* $@599  */
  YYSYMBOL_1234_600 = 1234,                /* $@600  */
  YYSYMBOL_1235_601 = 1235,                /* $@601  */
  YYSYMBOL_1236_602 = 1236,                /* $@602  */
  YYSYMBOL_1237_603 = 1237,                /* $@603  */
  YYSYMBOL_1238_604 = 1238,                /* $@604  */
  YYSYMBOL_1239_605 = 1239,                /* $@605  */
  YYSYMBOL_1240_606 = 1240,                /* $@606  */
  YYSYMBOL_1241_607 = 1241,                /* $@607  */
  YYSYMBOL_1242_608 = 1242,                /* $@608  */
  YYSYMBOL_1243_609 = 1243,                /* $@609  */
  YYSYMBOL_directorypreset_filter = 1244,  /* directorypreset_filter  */
  YYSYMBOL_1245_610 = 1245,                /* $@610  */
  YYSYMBOL_1246_611 = 1246,                /* $@611  */
  YYSYMBOL_1247_612 = 1247,                /* $@612  */
  YYSYMBOL_helpop = 1248,                  /* helpop  */
  YYSYMBOL_1249_613 = 1249,                /* $@613  */
  YYSYMBOL_viewcommandlogop = 1250,        /* viewcommandlogop  */
  YYSYMBOL_1251_614 = 1251,                /* $@614  */
  YYSYMBOL_activatetextviewmodeop = 1252,  /* activatetextviewmodeop  */
  YYSYMBOL_1253_615 = 1253,                /* $@615  */
  YYSYMBOL_activatetextviewmodeopbody = 1254, /* activatetextviewmodeopbody  */
  YYSYMBOL_1255_616 = 1255,                /* $@616  */
  YYSYMBOL_1256_617 = 1256,                /* $@617  */
  YYSYMBOL_1257_618 = 1257,                /* $@618  */
  YYSYMBOL_1258_619 = 1258,                /* $@619  */
  YYSYMBOL_1259_620 = 1259,                /* $@620  */
  YYSYMBOL_1260_621 = 1260,                /* $@621  */
  YYSYMBOL_1261_622 = 1261,                /* $@622  */
  YYSYMBOL_1262_623 = 1262,                /* $@623  */
  YYSYMBOL_1263_624 = 1263                 /* $@624  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2806

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  480
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  784
/* YYNRULES -- Number of rules.  */
#define YYNRULES  1383
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  3290

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   730


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int16 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   479,     2,   476,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,   477,
       2,   478,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   343,   343,   343,   343,   346,   347,   348,   347,   351,
     352,   353,   352,   364,   365,   364,   376,   377,   376,   388,
     389,   388,   400,   401,   402,   403,   404,   405,   404,   408,
     409,   408,   411,   413,   413,   414,   416,   416,   417,   419,
     419,   420,   420,   421,   421,   422,   422,   423,   424,   424,
     425,   425,   426,   426,   427,   427,   428,   428,   430,   431,
     431,   432,   432,   433,   434,   434,   435,   435,   436,   436,
     437,   437,   438,   438,   439,   440,   440,   441,   443,   443,
     444,   444,   445,   445,   446,   446,   447,   447,   448,   448,
     449,   449,   450,   450,   451,   451,   452,   452,   453,   453,
     454,   454,   455,   455,   456,   458,   458,   459,   461,   461,
     462,   462,   463,   465,   465,   466,   466,   467,   467,   468,
     468,   469,   469,   470,   470,   471,   471,   472,   474,   474,
     475,   475,   476,   476,   477,   477,   478,   478,   479,   479,
     480,   480,   481,   481,   482,   482,   483,   483,   484,   484,
     485,   485,   486,   486,   487,   487,   488,   488,   489,   491,
     491,   492,   492,   493,   493,   494,   494,   495,   495,   496,
     496,   497,   497,   498,   498,   499,   499,   500,   500,   501,
     501,   502,   504,   504,   505,   505,   506,   506,   507,   507,
     508,   508,   509,   509,   510,   511,   510,   512,   513,   512,
     514,   515,   514,   516,   516,   517,   519,   519,   523,   523,
     527,   527,   531,   531,   535,   535,   539,   539,   543,   543,
     547,   547,   551,   551,   555,   555,   559,   559,   563,   563,
     567,   567,   571,   571,   575,   575,   578,   578,   582,   582,
     586,   586,   590,   591,   590,   596,   598,   600,   608,   600,
     611,   613,   613,   617,   617,   621,   621,   622,   622,   623,
     625,   629,   625,   632,   634,   634,   637,   637,   638,   640,
     640,   641,   641,   642,   642,   643,   643,   644,   644,   645,
     645,   646,   646,   647,   647,   648,   648,   649,   651,   652,
     651,   658,   660,   660,   664,   664,   665,   665,   668,   668,
     669,   670,   669,   682,   684,   687,   692,   693,   684,   695,
     698,   703,   704,   709,   710,   695,   712,   714,   714,   715,
     715,   716,   716,   717,   717,   718,   718,   719,   719,   720,
     720,   721,   721,   722,   724,   727,   724,   730,   731,   730,
     733,   734,   733,   736,   736,   737,   739,   739,   740,   742,
     743,   742,   744,   746,   746,   747,   749,   752,   749,   761,
     763,   763,   764,   764,   765,   765,   766,   766,   767,   767,
     769,   769,   770,   770,   771,   771,   772,   772,   773,   774,
     774,   775,   775,   776,   777,   777,   780,   780,   783,   783,
     786,   786,   789,   789,   790,   790,   791,   791,   792,   792,
     793,   793,   794,   794,   795,   795,   796,   796,   797,   797,
     798,   798,   799,   799,   800,   800,   801,   803,   803,   804,
     806,   807,   808,   809,   810,   813,   814,   813,   817,   818,
     817,   821,   822,   821,   825,   826,   825,   829,   830,   829,
     833,   836,   833,   839,   841,   842,   843,   845,   847,   848,
     847,   851,   853,   853,   854,   855,   854,   858,   859,   858,
     871,   873,   874,   873,   880,   882,   882,   886,   886,   887,
     887,   890,   891,   890,   894,   895,   894,   907,   910,   909,
     917,   916,   924,   923,   931,   930,   938,   937,   945,   944,
     952,   951,   958,   961,   960,   968,   967,   975,   974,   982,
     981,   989,   988,   996,   995,  1003,  1002,  1009,  1012,  1011,
    1016,  1015,  1020,  1019,  1024,  1023,  1028,  1027,  1032,  1031,
    1036,  1035,  1040,  1039,  1044,  1043,  1047,  1049,  1049,  1050,
    1050,  1051,  1051,  1052,  1052,  1053,  1053,  1054,  1054,  1055,
    1055,  1056,  1058,  1059,  1061,  1062,  1063,  1064,  1065,  1066,
    1067,  1068,  1069,  1070,  1071,  1072,  1073,  1074,  1075,  1076,
    1077,  1078,  1079,  1080,  1081,  1082,  1083,  1084,  1085,  1086,
    1087,  1088,  1089,  1090,  1091,  1092,  1093,  1094,  1095,  1096,
    1097,  1098,  1099,  1100,  1101,  1102,  1103,  1104,  1105,  1106,
    1107,  1108,  1109,  1110,  1111,  1112,  1113,  1114,  1115,  1116,
    1117,  1118,  1119,  1120,  1121,  1122,  1123,  1124,  1125,  1126,
    1127,  1128,  1129,  1130,  1131,  1132,  1133,  1135,  1135,  1138,
    1138,  1139,  1139,  1140,  1140,  1141,  1141,  1142,  1142,  1143,
    1143,  1144,  1144,  1145,  1145,  1146,  1146,  1147,  1147,  1148,
    1148,  1149,  1149,  1150,  1150,  1151,  1151,  1152,  1152,  1153,
    1153,  1154,  1154,  1155,  1155,  1156,  1158,  1158,  1161,  1161,
    1164,  1164,  1167,  1167,  1170,  1170,  1173,  1173,  1174,  1176,
    1176,  1183,  1183,  1190,  1190,  1193,  1193,  1194,  1194,  1195,
    1195,  1196,  1198,  1198,  1201,  1201,  1202,  1202,  1203,  1203,
    1204,  1204,  1205,  1205,  1206,  1206,  1207,  1207,  1208,  1208,
    1209,  1209,  1210,  1210,  1211,  1211,  1212,  1212,  1213,  1213,
    1214,  1214,  1215,  1215,  1216,  1216,  1217,  1217,  1218,  1218,
    1219,  1219,  1220,  1220,  1221,  1223,  1223,  1230,  1230,  1237,
    1237,  1244,  1244,  1251,  1251,  1258,  1258,  1265,  1265,  1272,
    1272,  1279,  1279,  1286,  1286,  1289,  1289,  1290,  1290,  1291,
    1291,  1292,  1292,  1293,  1293,  1294,  1296,  1296,  1299,  1299,
    1300,  1300,  1301,  1301,  1302,  1302,  1303,  1305,  1305,  1308,
    1308,  1311,  1311,  1312,  1312,  1313,  1313,  1314,  1316,  1316,
    1319,  1319,  1320,  1320,  1321,  1321,  1322,  1324,  1324,  1327,
    1327,  1330,  1330,  1331,  1331,  1332,  1334,  1334,  1337,  1337,
    1338,  1340,  1340,  1343,  1343,  1344,  1344,  1345,  1345,  1346,
    1346,  1347,  1347,  1348,  1348,  1349,  1351,  1351,  1354,  1354,
    1357,  1357,  1358,  1360,  1360,  1363,  1363,  1370,  1370,  1373,
    1373,  1374,  1374,  1375,  1375,  1376,  1376,  1377,  1377,  1378,
    1378,  1379,  1379,  1380,  1380,  1381,  1381,  1382,  1382,  1383,
    1383,  1384,  1384,  1385,  1385,  1386,  1386,  1387,  1387,  1388,
    1390,  1390,  1393,  1393,  1394,  1394,  1395,  1395,  1396,  1396,
    1397,  1397,  1398,  1398,  1399,  1401,  1401,  1404,  1404,  1405,
    1405,  1406,  1406,  1407,  1407,  1408,  1410,  1410,  1413,  1413,
    1414,  1414,  1415,  1417,  1417,  1420,  1420,  1421,  1421,  1422,
    1422,  1423,  1425,  1425,  1428,  1428,  1429,  1429,  1430,  1430,
    1431,  1433,  1433,  1436,  1436,  1437,  1437,  1438,  1438,  1439,
    1439,  1440,  1440,  1441,  1441,  1442,  1442,  1443,  1443,  1444,
    1444,  1445,  1447,  1447,  1450,  1450,  1451,  1451,  1452,  1452,
    1453,  1453,  1454,  1456,  1456,  1459,  1459,  1460,  1462,  1462,
    1467,  1467,  1468,  1468,  1469,  1469,  1470,  1470,  1471,  1471,
    1472,  1472,  1473,  1473,  1474,  1474,  1475,  1475,  1476,  1476,
    1477,  1477,  1478,  1478,  1479,  1479,  1480,  1480,  1481,  1481,
    1482,  1484,  1484,  1487,  1487,  1488,  1488,  1489,  1489,  1490,
    1490,  1491,  1491,  1492,  1492,  1493,  1493,  1494,  1494,  1495,
    1495,  1496,  1496,  1497,  1497,  1498,  1498,  1499,  1499,  1500,
    1500,  1501,  1501,  1502,  1504,  1504,  1507,  1507,  1510,  1510,
    1511,  1511,  1512,  1512,  1513,  1513,  1514,  1516,  1516,  1519,
    1519,  1520,  1520,  1521,  1521,  1522,  1522,  1523,  1523,  1524,
    1524,  1525,  1525,  1526,  1526,  1527,  1527,  1528,  1528,  1529,
    1529,  1530,  1530,  1531,  1531,  1532,  1532,  1533,  1533,  1534,
    1534,  1535,  1535,  1536,  1536,  1537,  1537,  1538,  1538,  1539,
    1539,  1540,  1540,  1541,  1541,  1542,  1542,  1543,  1543,  1544,
    1544,  1545,  1545,  1546,  1546,  1547,  1547,  1548,  1548,  1549,
    1551,  1551,  1554,  1554,  1557,  1557,  1560,  1560,  1563,  1563,
    1564,  1564,  1565,  1565,  1566,  1566,  1567,  1567,  1568,  1568,
    1569,  1569,  1570,  1572,  1572,  1575,  1575,  1576,  1576,  1577,
    1577,  1578,  1578,  1580,  1582,  1582,  1585,  1585,  1586,  1588,
    1588,  1591,  1591,  1592,  1592,  1593,  1593,  1594,  1596,  1596,
    1599,  1599,  1602,  1602,  1603,  1605,  1605,  1608,  1608,  1609,
    1611,  1611,  1614,  1614,  1617,  1617,  1618,  1618,  1619,  1621,
    1621,  1624,  1624,  1625,  1625,  1626,  1626,  1627,  1627,  1628,
    1628,  1629,  1629,  1630,  1630,  1631,  1633,  1633,  1641,  1641,
    1642,  1642,  1643,  1643,  1644,  1644,  1645,  1645,  1646,  1646,
    1647,  1647,  1648,  1648,  1649,  1649,  1650,  1650,  1651,  1651,
    1652,  1652,  1653,  1653,  1654,  1656,  1656,  1664,  1664,  1665,
    1665,  1666,  1666,  1667,  1667,  1668,  1668,  1669,  1669,  1670,
    1670,  1671,  1671,  1672,  1672,  1673,  1673,  1674,  1674,  1675,
    1675,  1676,  1676,  1677,  1677,  1678,  1678,  1679,  1681,  1681,
    1684,  1684,  1687,  1687,  1688,  1688,  1689,  1689,  1690,  1690,
    1691,  1693,  1693,  1696,  1696,  1697,  1697,  1698,  1698,  1699,
    1699,  1700,  1700,  1701,  1701,  1702,  1702,  1703,  1703,  1704,
    1704,  1705,  1707,  1707,  1710,  1710,  1711,  1711,  1712,  1712,
    1713,  1713,  1714,  1714,  1715,  1715,  1716,  1716,  1717,  1717,
    1718,  1718,  1719,  1721,  1721,  1724,  1724,  1727,  1727,  1730,
    1730,  1733,  1733,  1734,  1734,  1735,  1737,  1737,  1744,  1745,
    1744,  1747,  1749,  1749,  1750,  1750,  1751,  1751,  1752,  1752,
    1753,  1753,  1754,  1754,  1755,  1755,  1756,  1756,  1757,  1757,
    1758,  1758,  1759,  1759,  1760,  1760,  1761,  1761,  1762,  1762,
    1763,  1763,  1764,  1764,  1765,  1765,  1766,  1766,  1767,  1768,
    1767,  1769,  1771,  1771,  1772,  1772,  1773,  1773,  1774,  1776,
    1776,  1781,  1781,  1786,  1786,  1789,  1789,  1790,  1790,  1791,
    1791,  1792,  1792,  1793,  1793,  1794,  1794,  1795,  1795,  1796,
    1796,  1797,  1797,  1798
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "LEFTBRACE_WCP",
  "RIGHTBRACE_WCP", "GLOBAL_WCP", "COLORS_WCP", "LANG_WCP", "PALETTE_WCP",
  "OWNOP_WCP", "LISTERSETS_WCP", "YES_WCP", "NO_WCP", "LEFT_WCP",
  "RIGHT_WCP", "HBARTOP_WCP", "HBARHEIGHT_WCP", "VBARLEFT_WCP",
  "VBARWIDTH_WCP", "DISPLAYSETS_WCP", "NAME_WCP", "SIZE_WCP", "TYPE_WCP",
  "PERMISSION_WCP", "OWNER_WCP", "DESTINATION_WCP", "MODTIME_WCP",
  "ACCTIME_WCP", "CHGTIME_WCP", "ROWS_WCP", "COLUMNS_WCP", "CACHESIZE_WCP",
  "OWNERSTYLE_WCP", "TERMINAL_WCP", "USESTRINGFORDIRSIZE_WCP",
  "TIMESETS_WCP", "STYLE1_WCP", "STYLE2_WCP", "STYLE3_WCP", "DATE_WCP",
  "TIME_WCP", "DATESUBSTITUTION_WCP", "DATEBEFORETIME_WCP", "STATEBAR_WCP",
  "SELLVBAR_WCP", "UNSELLVBAR_WCP", "CLOCKBAR_WCP", "REQUESTER_WCP",
  "UNSELDIR_WCP", "SELDIR_WCP", "UNSELFILE_WCP", "SELFILE_WCP",
  "UNSELACTDIR_WCP", "SELACTDIR_WCP", "UNSELACTFILE_WCP", "SELACTFILE_WCP",
  "LVBG_WCP", "STARTUP_WCP", "BUTTON_WCP", "BUTTONS_WCP", "POSITION_WCP",
  "TITLE_WCP", "COMMANDS_WCP", "SHORTKEYS_WCP", "PATHS_WCP", "PATH_WCP",
  "FILETYPES_WCP", "FILETYPE_WCP", "NORMAL_WCP", "NOTYETCHECKED_WCP",
  "UNKNOWN_WCP", "NOSELECT_WCP", "DIR_WCP", "USEPATTERN_WCP",
  "USECONTENT_WCP", "PATTERN_WCP", "CONTENT_WCP", "FTCOMMANDS_WCP",
  "DND_WCP", "DC_WCP", "SHOW_WCP", "RAWSHOW_WCP", "USER_WCP", "FLAGS_WCP",
  "IGNOREDIRS_WCP", "HOTKEYS_WCP", "HOTKEY_WCP", "FONTS_WCP",
  "GLOBALFONT_WCP", "BUTTONFONT_WCP", "LEFTFONT_WCP", "RIGHTFONT_WCP",
  "TEXTVIEWFONT_WCP", "STATEBARFONT_WCP", "TEXTVIEWALTFONT_WCP",
  "CLOCKBARSETS_WCP", "MODUS_WCP", "TIMESPACE_WCP", "VERSION_WCP",
  "EXTERN_WCP", "UPDATETIME_WCP", "PROGRAM_WCP", "SHOWHINTS_WCP",
  "KEY_WCP", "DOUBLE_WCP", "MOD_WCP", "CONTROL_WCP", "SHIFT_WCP",
  "LOCK_WCP", "MOD1_WCP", "MOD2_WCP", "MOD3_WCP", "MOD4_WCP", "MOD5_WCP",
  "DNDACTION_WCP", "DCACTION_WCP", "SHOWACTION_WCP", "RSHOWACTION_WCP",
  "USERACTION_WCP", "ROWUP_WCP", "ROWDOWN_WCP", "CHANGEHIDDENFLAG_WCP",
  "COPYOP_WCP", "FIRSTROW_WCP", "LASTROW_WCP", "PAGEUP_WCP",
  "PAGEDOWN_WCP", "SELECTOP_WCP", "SELECTALLOP_WCP", "SELECTNONEOP_WCP",
  "INVERTALLOP_WCP", "PARENTDIROP_WCP", "ENTERDIROP_WCP",
  "CHANGELISTERSETOP_WCP", "SWITCHLISTEROP_WCP", "FILTERSELECTOP_WCP",
  "FILTERUNSELECTOP_WCP", "PATHTOOTHERSIDEOP_WCP", "QUITOP_WCP",
  "DELETEOP_WCP", "RELOADOP_WCP", "MAKEDIROP_WCP", "RENAMEOP_WCP",
  "DIRSIZEOP_WCP", "SIMDDOP_WCP", "STARTPROGOP_WCP", "SEARCHENTRYOP_WCP",
  "ENTERPATHOP_WCP", "SCROLLLISTEROP_WCP", "CREATESYMLINKOP_WCP",
  "CHANGESYMLINKOP_WCP", "CHMODOP_WCP", "TOGGLELISTERMODEOP_WCP",
  "SETSORTMODEOP_WCP", "SETFILTEROP_WCP", "SHORTKEYFROMLISTOP_WCP",
  "CHOWNOP_WCP", "WORKERCONFIG_WCP", "USERSTYLE_WCP", "DATESTRING_WCP",
  "TIMESTRING_WCP", "COLOR_WCP", "PATTERNIGNORECASE_WCP",
  "PATTERNUSEREGEXP_WCP", "PATTERNUSEFULLNAME_WCP", "COM_WCP",
  "SEPARATEEACHENTRY_WCP", "RECURSIVE_WCP", "START_WCP",
  "TERMINALWAIT_WCP", "SHOWOUTPUT_WCP", "VIEWSTR_WCP", "SHOWOUTPUTINT_WCP",
  "SHOWOUTPUTOTHERSIDE_WCP", "SHOWOUTPUTCUSTOMATTRIBUTE_WCP",
  "INBACKGROUND_WCP", "TAKEDIRS_WCP", "ACTIONNUMBER_WCP",
  "HIDDENFILES_WCP", "HIDE_WCP", "TOGGLE_WCP", "FOLLOWSYMLINKS_WCP",
  "MOVE_WCP", "RENAME_WCP", "SAMEDIR_WCP", "REQUESTDEST_WCP",
  "REQUESTFLAGS_WCP", "OVERWRITE_WCP", "ALWAYS_WCP", "NEVER_WCP",
  "COPYMODE_WCP", "FAST_WCP", "PRESERVEATTR_WCP", "MODE_WCP", "ACTIVE_WCP",
  "ACTIVE2OTHER_WCP", "SPECIAL_WCP", "REQUEST_WCP", "CURRENT_WCP",
  "OTHER_WCP", "FILTER_WCP", "QUICK_WCP", "ALSOACTIVE_WCP",
  "RESETDIRSIZES_WCP", "KEEPFILETYPES_WCP", "RELATIVE_WCP", "ONFILES_WCP",
  "ONDIRS_WCP", "SORTBY_WCP", "SORTFLAG_WCP", "REVERSE_WCP", "DIRLAST_WCP",
  "DIRMIXED_WCP", "EXCLUDE_WCP", "INCLUDE_WCP", "UNSET_WCP",
  "UNSETALL_WCP", "SCRIPTOP_WCP", "NOP_WCP", "PUSH_WCP", "LABEL_WCP",
  "IF_WCP", "END_WCP", "POP_WCP", "SETTINGS_WCP", "WINDOW_WCP", "GOTO_WCP",
  "PUSHUSEOUTPUT_WCP", "DODEBUG_WCP", "WPURECURSIVE_WCP",
  "WPUTAKEDIRS_WCP", "STACKNR_WCP", "PUSHSTRING_WCP",
  "PUSHOUTPUTRETURNCODE_WCP", "IFTEST_WCP", "IFLABEL_WCP", "WINTYPE_WCP",
  "OPEN_WCP", "CLOSE_WCP", "LEAVE_WCP", "CHANGEPROGRESS_WCP",
  "CHANGETEXT_WCP", "PROGRESSUSEOUTPUT_WCP", "WINTEXTUSEOUTPUT_WCP",
  "PROGRESS_WCP", "WINTEXT_WCP", "SHOWDIRCACHEOP_WCP", "INODE_WCP",
  "NLINK_WCP", "BLOCKS_WCP", "SHOWHEADER_WCP", "LVHEADER_WCP",
  "IGNORECASE_WCP", "LISTVIEWS_WCP", "BLL_WCP", "LBL_WCP", "LLB_WCP",
  "BL_WCP", "LB_WCP", "LAYOUT_WCP", "BUTTONSVERT_WCP", "LISTVIEWSVERT_WCP",
  "LISTVIEWWEIGHT_WCP", "WEIGHTTOACTIVE_WCP", "EXTCOND_WCP",
  "USEEXTCOND_WCP", "SUBTYPE_WCP", "PARENTACTIONOP_WCP",
  "NOOPERATIONOP_WCP", "COLORMODE_WCP", "DEFAULT_WCP", "CUSTOM_WCP",
  "UNSELECTCOLOR_WCP", "SELECTCOLOR_WCP", "UNSELECTACTIVECOLOR_WCP",
  "SELECTACTIVECOLOR_WCP", "COLOREXTERNPROG_WCP", "PARENT_WCP",
  "DONTCD_WCP", "DONTCHECKVIRTUAL_WCP", "GOFTPOP_WCP", "HOSTNAME_WCP",
  "USERNAME_WCP", "PASSWORD_WCP", "DONTENTERFTP_WCP", "ALWAYSSTOREPW_WCP",
  "INTERNALVIEWOP_WCP", "CUSTOMFILES_WCP", "SHOWMODE_WCP", "SELECTED_WCP",
  "REVERSESEARCH_WCP", "MOUSECONF_WCP", "SELECTBUTTON_WCP",
  "ACTIVATEBUTTON_WCP", "SCROLLBUTTON_WCP", "SELECTMETHOD_WCP",
  "ALTERNATIVE_WCP", "DELAYEDCONTEXTMENU_WCP", "SEARCHOP_WCP",
  "EDITCOMMAND_WCP", "SHOWPREVRESULTS_WCP", "TEXTVIEW_WCP",
  "TEXTVIEWHIGHLIGHTED_WCP", "TEXTVIEWSELECTION_WCP", "DIRBOOKMARKOP_WCP",
  "OPENCONTEXTMENUOP_WCP", "RUNCUSTOMACTION_WCP", "CUSTOMNAME_WCP",
  "CONTEXTBUTTON_WCP", "ACTIVATEMOD_WCP", "SCROLLMOD_WCP",
  "CONTEXTMOD_WCP", "NONE_WCP", "CUSTOMACTION_WCP",
  "SAVE_WORKER_STATE_ON_EXIT_WCP", "OPENWORKERMENUOP_WCP",
  "CHANGELABELOP_WCP", "ASKFORLABEL_WCP", "LABELCOLORS_WCP",
  "BOOKMARKLABEL_WCP", "BOOKMARKFILTER_WCP", "SHOWONLYBOOKMARKS_WCP",
  "SHOWONLYLABEL_WCP", "SHOWALL_WCP", "OPTIONMODE_WCP", "INVERT_WCP",
  "SET_WCP", "CHANGEFILTERS_WCP", "CHANGEBOOKMARKS_WCP", "QUERYLABEL_WCP",
  "MODIFYTABSOP_WCP", "TABACTION_WCP", "NEWTAB_WCP", "CLOSECURRENTTAB_WCP",
  "NEXTTAB_WCP", "PREVTAB_WCP", "TOGGLELOCKSTATE_WCP", "MOVETABLEFT_WCP",
  "MOVETABRIGHT_WCP", "CHANGELAYOUTOP_WCP", "INFIXSEARCH_WCP",
  "VOLUMEMANAGEROP_WCP", "ERROR", "ACTIVESIDE_WCP", "SHOWFREESPACE_WCP",
  "ACTIVEMODE_WCP", "ASK_WCP", "SSHALLOW_WCP", "FIELD_WIDTH_WCP",
  "QUICKSEARCHENABLED_WCP", "FILTEREDSEARCHENABLED_WCP", "XFTFONTS_WCP",
  "USEMAGIC_WCP", "MAGICPATTERN_WCP", "MAGICCOMPRESSED_WCP",
  "MAGICMIME_WCP", "VOLUMEMANAGER_WCP", "MOUNTCOMMAND_WCP",
  "UNMOUNTCOMMAND_WCP", "FSTABFILE_WCP", "MTABFILE_WCP", "PARTFILE_WCP",
  "REQUESTACTION_WCP", "SIZEH_WCP", "NEXTAGING_WCP", "ENTRY_WCP",
  "PREFIX_WCP", "VALUE_WCP", "EXTENSION_WCP", "EJECTCOMMAND_WCP",
  "CLOSETRAYCOMMAND_WCP", "AVFSMODULE_WCP", "FLEXIBLEMATCH_WCP",
  "USE_VERSION_STRING_COMPARE_WCP", "SWITCHBUTTONBANKOP_WCP",
  "SWITCHTONEXTBANK_WCP", "SWITCHTOPREVBANK_WCP", "SWITCHTOBANKNR_WCP",
  "BANKNR_WCP", "USE_VIRTUAL_TEMP_COPIES_WCP", "PATHJUMPOP_WCP",
  "PATHJUMPALLOWDIRS_WCP", "CLIPBOARDOP_WCP", "CLIPBOARDSTRING_WCP",
  "COMMANDSTRING_WCP", "EVALCOMMAND_WCP", "WATCHMODE_WCP",
  "APPLY_WINDOW_DIALOG_TYPE_WCP", "FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP",
  "COMMANDMENUOP_WCP", "SEARCHMODEONKEYPRESS_WCP", "PATHENTRYONTOP_WCP",
  "STORE_FILES_ALWAYS_WCP", "PATHJUMPSETS_WCP", "SHOWDOTDOT_WCP",
  "SHOWBREADCRUMB_WCP", "FACES_WCP", "FACE_WCP", "ENABLE_INFO_LINE_WCP",
  "INFO_LINE_LUA_MODE_WCP", "INFO_LINE_CONTENT_WCP",
  "RESTORE_TABS_MODE_WCP", "STORE_TABS_MODE_WCP", "AS_EXIT_STATE_WCP",
  "USE_EXTENDED_REGEX_WCP", "HIGHLIGHT_USER_ACTION_WCP", "CHTIMEOP_WCP",
  "ADJUSTRELATIVESYMLINKS_WCP", "OUTSIDE_WCP", "EDIT_WCP",
  "MAKEABSOLUTE_WCP", "MAKERELATIVE_WCP", "MAGICIGNORECASE_WCP",
  "ENSURE_FILE_PERMISSIONS_WCP", "USER_RW_WCP", "USER_RW_GROUP_R_WCP",
  "USER_RW_ALL_R_WCP", "LEAVE_UNMODIFIED_WCP", "PREFERUDISKSVERSION_WCP",
  "CHANGECOLUMNSOP_WCP", "PATTERNISCOMMASEPARATED_WCP", "STRCASECMP_WCP",
  "USE_STRING_COMPARE_MODE_WCP", "VIEWNEWESTFILESOP_WCP", "SHOWRECENT_WCP",
  "DIRCOMPAREOP_WCP", "TABPROFILESOP_WCP", "EXTERNALVDIROP_WCP",
  "VDIR_PRESERVE_DIR_STRUCTURE_WCP", "OPENTABMENUOP_WCP", "INITIALTAB_WCP",
  "SHOWBYTIME_WCP", "SHOWBYFILTER_WCP", "SHOWBYPROGRAM_WCP",
  "TERMINAL_RETURNS_EARLY_WCP", "DIRECTORYPRESETS_WCP",
  "DIRECTORYPRESET_WCP", "HELPOP_WCP", "PRIORITY_WCP", "AUTOFILTER_WCP",
  "ENABLE_CUSTOM_LVB_LINE_WCP", "CUSTOM_LVB_LINE_WCP",
  "CUSTOM_DIRECTORY_INFO_COMMAND_WCP", "IMMEDIATE_FILTER_APPLY_WCP",
  "DISABLEBGCHECKPREFIX_WCP", "DISPLAYMODE_WCP", "SHOWSUBDIRS_WCP",
  "SHOWDIRECTSUBDIRS_WCP", "INCLUDEALL_WCP", "APPLYMODE_WCP", "ADD_WCP",
  "REMOVE_WCP", "HIDENONEXISTING_WCP", "VIEWCOMMANDLOGOP_WCP",
  "LOCKONCURRENTDIR_WCP", "STARTNODE_WCP", "MENUS_WCP", "LVMODES_WCP",
  "LVCOMMANDS_WCP", "TOPLEVEL_WCP", "FOLLOWACTIVE_WCP", "WATCHFILE_WCP",
  "ACTIVATETEXTVIEWMODEOP_WCP", "SOURCEMODE_WCP", "ACTIVEFILE_WCP",
  "FILETYPEACTION_WCP", "IGNOREDIRS2_WCP", "IGNOREDIR_WCP",
  "CUSTOMATTRIBUTE_WCP", "STARTSEARCHSTRING_WCP", "APPLYONLY_WCP",
  "USESELECTEDENTRIES_WCP", "ONLYEXACTPATH_WCP", "ALSOSELECTEXTENSION_WCP",
  "STRING_WCP", "NUM_WCP", "'.'", "';'", "'='", "','", "$accept", "S",
  "$@1", "$@2", "start", "$@3", "$@4", "$@5", "$@6", "$@7", "$@8", "$@9",
  "$@10", "$@11", "$@12", "$@13", "$@14", "$@15", "$@16", "pjallowdirs",
  "$@17", "pathjumpsets", "$@18", "glb", "$@19", "$@20", "$@21", "$@22",
  "$@23", "$@24", "$@25", "$@26", "$@27", "$@28", "$@29", "$@30", "$@31",
  "$@32", "$@33", "$@34", "$@35", "layout", "$@36", "$@37", "$@38", "$@39",
  "$@40", "$@41", "$@42", "$@43", "$@44", "$@45", "$@46", "$@47", "$@48",
  "pal", "$@49", "listersets", "$@50", "$@51", "listersets2", "$@52",
  "$@53", "$@54", "$@55", "$@56", "$@57", "$@58", "displaysets", "$@59",
  "$@60", "$@61", "$@62", "$@63", "$@64", "$@65", "$@66", "$@67", "$@68",
  "$@69", "$@70", "$@71", "$@72", "$@73", "timesets", "$@74", "$@75",
  "$@76", "$@77", "$@78", "$@79", "$@80", "$@81", "$@82", "$@83", "$@84",
  "mouseconf", "$@85", "$@86", "$@87", "$@88", "$@89", "$@90", "$@91",
  "$@92", "$@93", "$@94", "$@95", "$@96", "$@97", "cols", "$@98", "$@99",
  "$@100", "$@101", "$@102", "$@103", "$@104", "$@105", "$@106", "$@107",
  "$@108", "$@109", "$@110", "$@111", "$@112", "$@113", "$@114", "$@115",
  "$@116", "$@117", "labelcolors", "$@118", "$@119", "labelcolor", "$@120",
  "$@121", "$@122", "$@123", "faces", "$@124", "$@125", "face", "$@126",
  "$@127", "startup", "$@128", "$@129", "$@130", "$@131", "$@132", "$@133",
  "$@134", "$@135", "$@136", "paths", "$@137", "$@138", "path", "$@139",
  "$@140", "$@141", "$@142", "$@143", "$@144", "keylist", "$@145", "$@146",
  "$@147", "$@148", "$@149", "$@150", "$@151", "$@152", "$@153", "$@154",
  "mods", "$@155", "$@156", "$@157", "$@158", "$@159", "$@160", "$@161",
  "$@162", "filetypes", "$@163", "$@164", "$@165", "$@166", "$@167",
  "$@168", "$@169", "ignoredirs", "$@170", "ignoredirs2", "$@171", "$@172",
  "ignoredir", "$@173", "subfiletypes", "$@174", "$@175", "filetype",
  "$@176", "$@177", "$@178", "$@179", "$@180", "$@181", "$@182", "$@183",
  "$@184", "$@185", "$@186", "$@187", "$@188", "$@189", "$@190", "$@191",
  "$@192", "$@193", "$@194", "$@195", "$@196", "$@197", "$@198", "$@199",
  "$@200", "$@201", "$@202", "filecontent", "$@203", "ft_type",
  "ftcommands", "$@204", "$@205", "$@206", "$@207", "$@208", "$@209",
  "$@210", "$@211", "$@212", "$@213", "$@214", "$@215", "commands",
  "flags", "hotkeys", "$@216", "$@217", "hotkey", "$@218", "$@219",
  "$@220", "$@221", "$@222", "buttons", "$@223", "$@224", "button",
  "$@225", "$@226", "$@227", "$@228", "$@229", "$@230", "$@231", "fonts",
  "$@232", "$@233", "$@234", "$@235", "$@236", "$@237", "$@238",
  "xftfonts", "$@239", "$@240", "$@241", "$@242", "$@243", "$@244",
  "$@245", "volumemanager", "$@246", "$@247", "$@248", "$@249", "$@250",
  "$@251", "$@252", "$@253", "$@254", "clockbarsets", "$@255", "$@256",
  "$@257", "$@258", "$@259", "$@260", "$@261", "bool", "command", "ownop",
  "$@262", "ownopbody", "$@263", "$@264", "$@265", "$@266", "$@267",
  "$@268", "$@269", "$@270", "$@271", "$@272", "$@273", "$@274", "$@275",
  "$@276", "$@277", "$@278", "$@279", "$@280", "dndaction", "$@281",
  "dcaction", "$@282", "showaction", "$@283", "rshowaction", "$@284",
  "useraction", "$@285", "useractionbody", "$@286", "rowup", "$@287",
  "rowdown", "$@288", "changehiddenflag", "$@289", "changehiddenflagbody",
  "$@290", "$@291", "$@292", "copyop", "$@293", "copyopbody", "$@294",
  "$@295", "$@296", "$@297", "$@298", "$@299", "$@300", "$@301", "$@302",
  "$@303", "$@304", "$@305", "$@306", "$@307", "$@308", "$@309", "$@310",
  "$@311", "$@312", "$@313", "firstrow", "$@314", "lastrow", "$@315",
  "pageup", "$@316", "pagedown", "$@317", "selectop", "$@318",
  "selectallop", "$@319", "selectnoneop", "$@320", "invertallop", "$@321",
  "parentdirop", "$@322", "enterdirop", "$@323", "enterdiropbody", "$@324",
  "$@325", "$@326", "$@327", "$@328", "changelistersetop", "$@329",
  "changelistersetopbody", "$@330", "$@331", "$@332", "$@333",
  "switchlisterop", "$@334", "filterselectop", "$@335",
  "filterselectopbody", "$@336", "$@337", "$@338", "filterunselectop",
  "$@339", "filterunselectopbody", "$@340", "$@341", "$@342",
  "pathtoothersideop", "$@343", "quitop", "$@344", "quitopbody", "$@345",
  "$@346", "deleteop", "$@347", "deleteopbody", "$@348", "reloadop",
  "$@349", "reloadopbody", "$@350", "$@351", "$@352", "$@353", "$@354",
  "$@355", "makedirop", "$@356", "renameop", "$@357", "renameopbody",
  "$@358", "dirsizeop", "$@359", "simddop", "$@360", "startprogop",
  "$@361", "startprogopbody", "$@362", "$@363", "$@364", "$@365", "$@366",
  "$@367", "$@368", "$@369", "$@370", "$@371", "$@372", "$@373", "$@374",
  "$@375", "$@376", "searchentryop", "$@377", "searchentryopbody", "$@378",
  "$@379", "$@380", "$@381", "$@382", "$@383", "enterpathop", "$@384",
  "enterpathopbody", "$@385", "$@386", "$@387", "$@388", "scrolllisterop",
  "$@389", "scrolllisteropbody", "$@390", "$@391", "createsymlinkop",
  "$@392", "createsymlinkopbody", "$@393", "$@394", "$@395",
  "changesymlinkop", "$@396", "changesymlinkopbody", "$@397", "$@398",
  "$@399", "chmodop", "$@400", "chmodopbody", "$@401", "$@402", "$@403",
  "$@404", "$@405", "$@406", "$@407", "$@408", "$@409", "chtimeop",
  "$@410", "chtimeopbody", "$@411", "$@412", "$@413", "$@414",
  "togglelistermodeop", "$@415", "togglelistermodeopbody", "$@416",
  "setsortmodeop", "$@417", "setsortmodeopbody", "$@418", "$@419", "$@420",
  "$@421", "$@422", "$@423", "$@424", "$@425", "$@426", "$@427", "$@428",
  "$@429", "$@430", "$@431", "$@432", "setfilterop", "$@433",
  "setfilteropbody", "$@434", "$@435", "$@436", "$@437", "$@438", "$@439",
  "$@440", "$@441", "$@442", "$@443", "$@444", "$@445", "$@446", "$@447",
  "$@448", "shortkeyfromlistop", "$@449", "chownop", "$@450",
  "chownopbody", "$@451", "$@452", "$@453", "$@454", "scriptop", "$@455",
  "scriptopbody", "$@456", "$@457", "$@458", "$@459", "$@460", "$@461",
  "$@462", "$@463", "$@464", "$@465", "$@466", "$@467", "$@468", "$@469",
  "$@470", "$@471", "$@472", "$@473", "$@474", "$@475", "$@476", "$@477",
  "$@478", "$@479", "$@480", "$@481", "$@482", "$@483", "$@484", "$@485",
  "showdircacheop", "$@486", "parentactionop", "$@487", "nooperationop",
  "$@488", "goftpop", "$@489", "goftpopbody", "$@490", "$@491", "$@492",
  "$@493", "$@494", "$@495", "$@496", "internalviewop", "$@497",
  "internalviewopbody", "$@498", "$@499", "$@500", "$@501", "clipboardop",
  "$@502", "clipboardopbody", "$@503", "searchop", "$@504", "searchopbody",
  "$@505", "$@506", "$@507", "dirbookmarkop", "$@508", "opencontextmenuop",
  "$@509", "opencontextmenuopbody", "$@510", "runcustomaction", "$@511",
  "runcustomactionbody", "$@512", "openworkermenuop", "$@513",
  "changelabelop", "$@514", "changelabelopbody", "$@515", "$@516",
  "modifytabsop", "$@517", "modifytabsopbody", "$@518", "$@519", "$@520",
  "$@521", "$@522", "$@523", "$@524", "changelayoutop", "$@525",
  "changelayoutopbody", "$@526", "$@527", "$@528", "$@529", "$@530",
  "$@531", "$@532", "$@533", "$@534", "$@535", "$@536", "$@537", "$@538",
  "changecolumnsop", "$@539", "changecolumnsopbody", "$@540", "$@541",
  "$@542", "$@543", "$@544", "$@545", "$@546", "$@547", "$@548", "$@549",
  "$@550", "$@551", "$@552", "$@553", "$@554", "volumemanagerop", "$@555",
  "switchbuttonbankop", "$@556", "switchbuttonbankopbody", "$@557",
  "$@558", "$@559", "$@560", "pathjumpop", "$@561", "pathjumpopbody",
  "$@562", "$@563", "$@564", "$@565", "$@566", "$@567", "$@568", "$@569",
  "$@570", "commandmenuop", "$@571", "commandmenuopbody", "$@572", "$@573",
  "$@574", "$@575", "$@576", "$@577", "$@578", "$@579", "$@580",
  "viewnewestfilesop", "$@581", "dircompareop", "$@582", "tabprofilesop",
  "$@583", "externalvdirop", "$@584", "externalvdiropbody", "$@585",
  "$@586", "opentabmenuop", "$@587", "directorypresets", "$@588", "$@589",
  "directorypreset", "$@590", "$@591", "$@592", "$@593", "$@594", "$@595",
  "$@596", "$@597", "$@598", "$@599", "$@600", "$@601", "$@602", "$@603",
  "$@604", "$@605", "$@606", "$@607", "$@608", "$@609",
  "directorypreset_filter", "$@610", "$@611", "$@612", "helpop", "$@613",
  "viewcommandlogop", "$@614", "activatetextviewmodeop", "$@615",
  "activatetextviewmodeopbody", "$@616", "$@617", "$@618", "$@619",
  "$@620", "$@621", "$@622", "$@623", "$@624", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-2518)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
   -2518,   154,    14, -2518,  -302,  -298,  -281,  -276,  -259,  -258,
   -2518,    54,   218,   230,   248,   264,   269,   273,   288,   292,
     295,   321,   323,   327,   330, -2518,    37, -2518,    49, -2518,
   -2518, -2518, -2518,   838,   259,  1006, -2518,   -45, -2518,  -156,
     404,   406,   -63,   -59,   -35,   -29,   -12,   -10,   467,   479,
     485,    27,   500,    46,    59,    65,    85,    87,   156,   502,
     543,   164,   166,   175,   176,   659,   606,   609,     7,   590,
     199,   207,   209,   210,   212,   214,   215,   693,   221,   222,
     224,   225,   704,   231,   250,   283,   284,   286,   294,   301,
     722,   275,   302,   858,   439,   407, -2518,   130,   411,   412,
     420,   132,   422,   201,   157, -2518,   597,   201,   507,   201,
     201,   201,   472,   201,   427,    54,   424,   445,   446,   459,
     460,   474,   487,   488,   494,   505,   506,   517,   518,   522,
     523,   535,   536,   537,   950,   968, -2518,   542,   553,   -72,
     -85,    54,  1054,  1055,  1057,  1058,  1063,  1065,   580,  1067,
    1068,  1070,  1071,   600,   603,   604,   614,   615,   616,   617,
      54,   324,   596,   618,   201,    54,   627,   628,   629,   630,
     631,   633,   634,    54,   632,  1107,   201,    54,  1112,  1144,
     815,   818, -2518, -2518,  1290,   819,   820,   821,   822,   823,
     824, -2518, -2518,   825,   817,   826,   827,   828,   829,   830,
    1299,   134,   831,   832,   833,   834,   835,   836,  1312,  1313,
    1314,  1315,   841,   842,   843,   844,   845,   846,   847,   848,
     849,   850,  1325,   853,   854,   855,   856,   857,   859, -2518,
     860,   862,   863,   864,   865,   866,   867,   868,   869,   870,
     871,   872,   873,   874,   875,   876,   877,   878, -2518,   957,
    1351,   879,   880,   881,   882,   883,   884,   885,   886,   887,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518,   201, -2518, -2518,
   -2518, -2518,   888,   889,   890,   891,   892,   893,   894, -2518,
     895,   896,   897,   898,   899,   900,   901, -2518,   902,   903,
     904,   905,   906,   907,   908, -2518, -2518, -2518,   909, -2518,
   -2518, -2518, -2518,   910,  1383,  1386,  1387,    37, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,   151,   111,   201,   201,   917,
     918,    37,   916,   919,   920,   921,   922,   923,   924,   925,
     926,   927,   928,   929,   930,  1390,   934,   935,   936,    -4,
     201,   937, -2518, -2518, -2518,    37, -2518,   939,   940,   941,
     942,   943,   201,   944,   945,   946,    37, -2518, -2518, -2518,
   -2518, -2518, -2518,   947,   948,   949,   951,   952,   953,   954,
     955,   956,   958,   959,   960,   961,   964,   963,   965,   966,
     967,  1234,  1401,  1416,    54, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518,   319,    54,   343,    54,   266,   962,
     970,   976,    54,   314,    54, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518,   275,    54, -2518,   267,
      54,    37,   973,    37,    81,    81, -2518,    37,    37,    37,
      37,    37,    37,    37,   972,   975,   977,   978,   979,   980,
     981,   982,   983,   984,   985, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,   201,   201,   988,   201,    37,
     987,   989,   990,   991,   992,   993,   994,  1317,  1317,  1317,
   -2518,    37,   995,   996,   997,   998,   999,  1000,  1001,  1002,
    1003, -2518,    37,    37,    37,    37,    37,    37,  1007,  1008,
    1009,  1010,  1011,  1012,  1013,  1014,  1015,  1016,  1017,  1018,
    1019, -2518,  1020,  1021,  1022,  1023,  1420,  1421, -2518,   543,
   -2518,    49,    49,    49,    49,    49,    49,    49,    49,    49,
    1024,  1025,  1426,  1447,  1026,  1449, -2518,  1027,  1028,  1448,
    1029,  1030,  1461, -2518,  1031,  1032,  1033,  1034,  1035,  1478,
    1496,  1036,  1037,  1038,  1039,  1040,  1497,  1041,  1042,  1043,
    1044,  1045,  1046,  1047,  1048,  1049,  1050,  1051,  1052,  1053,
    1528,  1056,  1530, -2518,  1498,  1531, -2518,  1059,  1533,  1535,
    1536, -2518,   838,   838,   838,   838,   838,   838,   838,   259,
     259,   259,   259,   259,   259,   259,  1006,  1006,  1006,  1006,
    1006,  1006,  1006, -2518, -2518,   -45,  1061,  1064,  1538,  1066,
    1069,  1539, -2518, -2518,  1072, -2518,  1074,  1075,  1076,  1077,
    1542,  1078,  1079,  1544,  1545, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,   134,   134,   134,   134,   134,   134,   134,
     134,   134,  1073,  1081,  1082,  1083, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518,  1084,  1557,  1559,  1560, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518,  1088,  1089,  1090,  1091,  1092,  1093,
    1094,  1095,  1096,  1097,  1098,  1099,  1100,   543,  1101,  1102,
    1103,  1104, -2518, -2518,   120, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,  1108,  1110, -2518, -2518,  1111,
   -2518,  1113,  1115, -2518,  1116,  1117, -2518,   384,  1119,   201,
     201,  1120, -2518,    56,   201,   201,   201,  1121,   201,  1357,
      75,  1122,  1123,  1124,  1125,  1127,   201,  1128,   201,   201,
     201,   201,  1129, -2518, -2518, -2518,     7, -2518, -2518,  1131,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,  1132,    39, -2518,    30,   390,
   -2518,  1133,   201,  1134,   201,  1135, -2518,   201,   201,   130,
     130,   157,   157,   157,   157,   157,   157,   157,   157,   157,
     157,   157, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,   597,   597,   597,   597,   597,
     597,   597,   974, -2518, -2518, -2518,   507,   507,   507,   507,
     507,   507,   507,   507,   507, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,    -3,   543,  1109,  1118,  1578,  1114,  1126,
     687,    82,  1106,   606,  1130,  1136,    82,  1137,  1138,   609,
   -2518, -2518, -2518, -2518, -2518,  1139,  1141,  1142,  1143,  1145,
    1140,  1543,  1608,  1609,  1618,  1148,  1621,  1622,  1150,  1151,
    1152,  1153,  1154,  1629,  1630,  1156,  1158,  1159,  1160,  1146,
    1161,  1162,  1163,  1166,  1167,  1168,  1169,  1170,  1171,  1172,
    1173,     7,   962,     7, -2518,  1563,     7,  1174,   687,    82,
     590,  1175,  1176,  1177,   -26,  1178,  1179,  1180,  1181,  1182,
    1183,  1184,  1185,  1186,  1187,  1188,  1189,  1190,  1191,  1192,
     439,  1193,  1194,  1196,  1197,  1198,    15,  1199,  1200, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518,   134,   134,   134,   134, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,  1201,  1202,  1203,  1204,  1205,  1206,  1207,
    1208,   597,   597,   597, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,   543,   543,   543,   543,   543,   543,   543,
     543,   543,   543,   543,   543,   543,   543,   543,   543,   543,
    1209,  1210,  1211,  1212,  1635, -2518,  1217,  1195, -2518, -2518,
   -2518,  1683,  1689,  1690,  1691,  1692,  1693,  1694,  1695,  1696,
    1697,  1698,  1699,  1700,  1701,  1702,  1703,  1704,  1705,  1706,
    1707,  1708,  1709,  1710,  1711,  1712,  1713,  1714,  1715,  1716,
    1717,  1718,  1719,  1720,  1721,  1722,  1723,  1724,  1725,  1726,
    1727,  1728,  1729,  1730,  1731,  1732,  1733,  1734,  1735,  1736,
    1737,  1738,  1739,  1740,  1741,  1742,  1743,  1744,  1745,  1746,
    1747,  1748,  1749,  1750,  1751,  1752,  1753,  1754,  1755,  1756,
    1757,  1758,  1759,  1760,  1761,  1762,   687, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
    1764,  1765,  1766,  1294, -2518, -2518, -2518,  1767, -2518,  1297,
   -2518, -2518, -2518, -2518, -2518, -2518,  1287,  1769, -2518, -2518,
   -2518, -2518,  1771, -2518,   266, -2518, -2518, -2518, -2518, -2518,
   -2518,   266, -2518, -2518, -2518, -2518,  1300,  1301,  1302,  1303,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518,  1304,  1775, -2518, -2518,  1776,  1777, -2518, -2518, -2518,
   -2518,  1305,  1306,  1781, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
    1311, -2518, -2518, -2518, -2518,  1310,  1316,  1318,  1319,  1320,
    1321,  1322,  1323,  1324,  1326,  1327,  1328,  1329,  1330,  1331,
    1784, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,  1335,  1336,  1337,   201, -2518,
    1333,  1338,   957,   319,   319, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518,  1339,   343,   343, -2518,   343,  1340,
     266,   266,   266,   266,   266,  1343,   266,   687,   687,   687,
     687, -2518,  1770, -2518,   266,   266,   266,   266,   266,   266,
   -2518,   266,   266,   266,   266,  1342,  1344,  1345,  1346,   266,
     266,   266,   266,   266,   266,   266,   266,  1350, -2518,   314,
   -2518, -2518,   267,   267,   267,  1352,    22, -2518,   267,   267,
     267,   267,   267,   267,   267,   267,   267,   267,   267,   267,
     267,   267,   267,  1348,    81,    81,    81,    81, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,    81,    81,    81,  1317,  1317,  1317,  1317,
    1317,  1317,  1317,  1317,  1353,  1334,  1341,  1354,  1234, -2518,
   -2518, -2518, -2518, -2518,   -44,  1785,  1787,  1788,  1790,  1798,
    1637,  1823,  1824,  1651,   475,  1828,  1829,  1830,  1831,  1832,
    1833,  1834,  1835,  1836,    94,  1648,  1838,  -124,  -105,  1839,
    1652,  1642,     2,  1842,  1374,  1844,  1845,     4,   -70,  1657,
    1658,   256,  1659,    25,  1660,   158,   670,  1850,    16,   326,
    1851,  1852,  1853,    89,   126,  -170,  1854,  1454,  1553,  1857,
     -67,  1532,   490,  1858,   -81,   333,  1481,  -279,   251,    57,
    1861,  1862,  1863,  -111,  1864,  1865,  1866,  -140,   319,  1768,
    1772,   319, -2518, -2518, -2518,   343, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,  1395, -2518,  1869,  1870,  1872,  1873,   687,
    1400, -2518, -2518, -2518, -2518, -2518,  1875, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,  1403,   976, -2518,   314,   314, -2518, -2518,
   -2518,  1404,  1405,  1406,   267, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,    15,    15,    15,    15,    15,
      15,    15,    15,    15,    15,    15,    15,    15,    15,    15,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518,  1409,  1410, -2518, -2518,   120,   120,  1408,  1411,
    1412,  1415,  1424,  1425,  1427,  1428,  1429,  1436,  1437,  1439,
    1883,   687, -2518, -2518, -2518, -2518,  1440,  1884, -2518, -2518,
    1442,  1890,  1443,  1444,  1445,  1446,  1451,  1452,  1455,  1456,
    1457,  1459,  1460,  1463,  1900, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518,  1465,  1468,  1921,  1469,  1922, -2518,
    1475,  1480,  1482,  1957,  1484,  1485,  1486,  1964, -2518,  1491,
    1966,  1493,  1972,  1499,  1500,  1501,  1976, -2518,  1503,  1978,
   -2518, -2518,  1506,  1507,  1508,  1509,  1510,  1514,  1519,  1520,
    1521,  2000,  1527,  1529,  1537,  1541,  1546,  1547,  2004,  1549,
    2005,  1550,  2010,  1551,  1552,  1555,  2012,  1558,  2013,  1562,
    1564,  1565,  1566,  1567,  1568,  2016,  1569,  2017,  1570,  1571,
    2031,  1572,  1573,  1576,  1579,  1580,  1581,  1582,  1583,  1587,
    2033, -2518,  1588,  1589,  1591,  1592,  2034,  1597,  1598,  1599,
    1600,  1601,  1602,  1603,  1604,  1605,  1606,  1607,  1610,  1611,
    1612,  1613,  1615,  1616,  1619,  1624,  2037, -2518, -2518, -2518,
    1625,  1626,  1627,  1628,  1631,  1632,  1633,  2052,  1634,  1636,
    1638,  2082,  1639,  1640,  1641,  2083, -2518,  1643,  2096,  1644,
    2103, -2518,  1645,  1646,  2104,  1647,  2109,  1649,  1650,  1653,
    1654,  1655,  1656,  1661,  1662,  1663,  1664,  1665,  1666,  1667,
    2111, -2518,  1668,  1669,  2116,  1670,  1671,  1672,  1673,  1674,
    2124,  1675,  2125,  1676,  1677,  2130,  1678,  1679,  1680,  1681,
    2131,  1684,  1685,  1686,  1687,  1688,  1763,  1773,  1774,  1778,
    1779,  1780,  1782,  1783,  1786,  1789,  2132, -2518, -2518, -2518,
    1682,  1791,  2133, -2518, -2518, -2518,  1792,  1793,  1794,  1795,
    1796,  1797,  1799,  2137, -2518,  1800,  1801, -2518,   319, -2518,
     343, -2518, -2518, -2518, -2518, -2518,  2162,  1802, -2518,   266,
     266,   266,   266, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518,   818, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518,    -3,  1803,  1804,
      -3, -2518, -2518,  1808,   201,   201,   184,  1809,   201,   201,
     201,   201,   201,   201,   201, -2518, -2518,  1810, -2518,    28,
   -2518,   201,   201,   201,   201,   201,   201,   161,    74,   201,
     -79,   168,   201, -2518,  1812,   332, -2518,   101, -2518,  1813,
     201,   201, -2518,  1814,   201,   201, -2518,    78, -2518,   201,
   -2518,   112,   201,   201, -2518,   201, -2518,   201,   201,   378,
    1815,   201,   201,   201,   201,   201, -2518,   201,   201,   201,
     201,  1816,   201, -2518,   138, -2518,   371, -2518,   201,   201,
     201, -2518,    68, -2518,  1817,   201,   201,   201,   201,  -161,
   -2518,  1818, -2518,    66,   418, -2518,   201,   400,  1819,  1820,
     328,    76,   201,   201,   201, -2518,   201,   201,   201,   201,
   -2518,   386,  1821,   201,   201,   201,   201,  1822,  1825,   201,
    1826,  1827,   493,   201,   201,   201,   201,  1837,  1840,  1841,
   -2518,   201,  1843,  1846,  1847,   201,   201,  1848, -2518,   201,
    1849,   -61, -2518,  1855,   201,   201, -2518,   201, -2518,  1856,
   -2518,  1859,   201, -2518,   785, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,   201,   201,  1860,   201, -2518,
     365,  1867, -2518,   351,  -139,   201,   201,   201, -2518,  1871,
   -2518,   201,    43, -2518,   201,   201,   201,   201, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,   201,  1874, -2518,   201,   201,
    1876,   201,   201,  -180,  1877, -2518,  1878,  1879, -2518, -2518,
    1140,    56,    56,    56,    56, -2518,  1807,  1357, -2518, -2518,
   -2518, -2518,  1563,   -26,   -26,   -26, -2518, -2518, -2518, -2518,
   -2518,  1880,  1881,  1882,  1885,  1886,  1887,  1888,  1889,  1891,
    1892,  1893,  1894,  1895,  1896,  1897,  1898,  1899,  1901,  1902,
    1903,  1904,  1905,  1906,  1907,  1908,  1909,  1910,  1911,  1912,
    1913,  1914,  1915,  1916,  1917,  1918,  1919,  1920,  1923,  1924,
    1925,  1927,  1928,  1929,  1930,  1931,  1932,  1933,  1934,  1935,
    1936,  1938,  1939,  1940,  1941,  1942,  1943,  1944,  1945,  1946,
    1947,  1948,  1949,  1950,  1951,  1952,  1953,  1954,  1955,  1956,
    1958,  1959,  1960,  1961,  1962,  1963,  1965,  1967,  1968,  1969,
    1970,  1971,  1973,  1974,  1975,  1977,  1979,  1980,  1981,  1982,
    1983,  1984,  1985,  1986,  1987,  1988,  1989,  1990,  1991,  1992,
    1993,  1994,  1995,  1996,  1997,  1998,  1999,  2001,  2002,  2003,
    2006,  2007,  2008,  2009,  2011,  2014,  2015,  2018,  2019,  2020,
    2021,  2022,  2023,  2024,  2025,  2026,  2027,  2028,  2029,  2030,
    2032,  2035,  2036,  2038,  2039,  2040,  2041,  2042,  2043,  2044,
    2045,  2046,  2047,  2048,  2049,  2050,  2051,  2053,  2054,  2055,
    2056,  2057,  2058,  2059,  2060,  2061,  2062,  2063,  2064,  2065,
    2066,  2067,  2068,  2071,  2072,  2073,  2074,  2075,  2076,  2077,
    2078,  2079,  2080,  2081,  2084,  2085,  2086,  2087,  2088,  2089,
    2090,  2091,  2092,  2093,  2094,  2095,  2097,  2098,  2099,  2100,
    2101,  2102,  2105,  2106,  2107,  2108,  2110,  2112,  2113,  2114,
     490,   490,   490,   490,   490,   490,   490,   490,   490,  2115,
    2117,  2118,  2119,  2120,  2121,  2122,  2123,  2126,  2127,  2128,
    2129,  2134,  2135,  2136,  2138,  2139,  2140,  2141,  2142,  2143,
    2144,  2145,  2146,  2147,  2148,  2149,  2150,  2151,  2152,  2153,
      57,    57,    57,    57,    57,    57,    57,    57,    57,    57,
      57,    57,    57,    57,    57,  2154,  2155,  2156,  2157,  2158,
    2159,  2160,  2161,  2163,  2164,  2165,  2166,  2167, -2518, -2518,
   -2518, -2518, -2518,    56,  2168, -2518, -2518, -2518, -2518, -2518,
      -3,    -3, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
    2169, -2518, -2518,   -44,   -44,   -44,   -44,   -44,   -44,   -44,
     -44,   -44,   -44,   -44,   -44,   -44,   -44,   -44,   -44,   -44,
     -44,  1637,  1651,  1651,  1651,   475,   475,   475,   475,   475,
     475,   475,   475,   475,   475,   475,   475,   475,   475,   475,
     475,   475,   475,   475,   475,    94,    94,    94,    94,    94,
    1648,  1648,  1648,  1648,  -124,  -124,  -124,  -105,  -105,  -105,
    1652,  1652,  1642,     2,     2,     2,     2,     2,     2,  1374,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,   -70,   -70,   -70,   -70,   -70,
     -70,  1657,  1657,  1657,  1657,  1658,  1658,   256,   256,   256,
    1659,  1659,  1659,    25,    25,    25,    25,    25,    25,    25,
      25,    25,  1660,   158,   158,   158,   158,   158,   158,   158,
     158,   158,   158,   158,   158,   158,   158,   158,   670,   670,
     670,   670,   670,   670,   670,   670,   670,   670,   670,   670,
     670,   670,   670,    16,    16,    16,    16,   326,   326,   326,
     326,   326,   326,   326,   326,   326,   326,   326,   326,   326,
     326,   326,   326,   326,   326,   326,   326,   326,   326,   326,
     326,   326,   326,   326,   326,   326,   326,    89,    89,    89,
      89,    89,    89,    89,   126,   126,   126,   126,  -170,  -170,
    -170,  1454,  1553,   -67,   -67,  1532,  1532,  1532,  1532,  1532,
    1532,  1532,   490,   490,   490,   490,   -81,   -81,   -81,   -81,
     333,   333,   333,   333,   333,   333,   333,   333,   333,  1481,
    -279,  -279,  -279,  -279,  -279,  -279,  -279,  -279,  -279,   251,
     251,   251,   251,  -111,  -111,  -140,  -140,  -140,  -140,  -140,
    -140,  -140,  -140,  -140,  1317,  1317,   687, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
    2170,  2171,  2070,  2172, -2518,  1868, -2518,    82,  2173,    56,
   -2518,  2174, -2518, -2518,  1317, -2518,  2175, -2518,    82, -2518
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       2,     0,     0,     1,     0,     0,     0,     0,     0,     0,
       3,    32,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     4,    77,     6,   287,    19,
      10,    13,    16,   492,   541,   507,    26,    38,    29,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     246,     0,     0,     0,     0,     0,   464,   291,   345,   451,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    35,     0,     0,  1311,     0,    59,   112,     0,     0,
       0,     0,     0,     0,   181,    61,   205,     0,   526,     0,
       0,     0,     0,     0,     0,    32,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     7,     0,     0,     0,
       0,    32,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      32,     0,     0,     0,     0,    32,     0,     0,     0,     0,
       0,     0,     0,    32,     0,     0,     0,    32,     0,     0,
       0,   107,   108,   110,     0,     0,     0,     0,     0,     0,
       0,   542,   543,     0,     0,     0,     0,     0,     0,     0,
       0,   104,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     5,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   242,   263,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       9,   461,    20,   288,    11,   334,   337,     0,   340,    14,
     448,    17,     0,     0,     0,     0,     0,     0,     0,    22,
       0,     0,     0,     0,     0,     0,     0,    24,     0,     0,
       0,     0,     0,     0,     0,    23,    33,    27,     0,    25,
    1308,    30,    39,     0,     0,     0,     0,    77,    41,    43,
      45,    48,    50,    52,    56,     0,     0,     0,     0,     0,
       0,    77,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   194,   197,   200,    77,    64,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    77,    66,    70,    72,
      68,    54,    75,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   250,     0,     0,    32,   269,   271,   275,   273,   277,
     281,   279,   285,   283,   477,    32,   303,    32,   416,   348,
       0,   352,    32,   460,    32,   478,   480,   482,   484,   486,
     488,   490,   529,   527,   531,   533,   535,   537,   539,   493,
     495,   497,   499,   501,   503,   505,    35,    32,    36,  1351,
      32,    77,     0,    77,   127,   127,    47,    77,    77,    77,
      77,    77,    77,    77,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    58,    78,    80,    82,    84,
      86,    88,    90,    92,    94,     0,     0,     0,     0,    77,
       0,     0,     0,     0,     0,     0,     0,   333,   333,   333,
      63,    77,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    74,    77,    77,    77,    77,    77,    77,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   234,     0,     0,     0,     0,     0,     0,   260,   246,
       8,   287,   287,   287,   287,   287,   287,   287,   287,   287,
       0,     0,     0,     0,     0,     0,    21,     0,     0,     0,
       0,     0,     0,    12,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   343,     0,     0,    15,     0,     0,     0,
       0,    18,   492,   492,   492,   492,   492,   492,   492,   541,
     541,   541,   541,   541,   541,   541,   507,   507,   507,   507,
     507,   507,   507,    34,    28,    38,     0,     0,     0,     0,
       0,     0,    31,    40,     0,    60,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    42,    44,    46,    49,    51,
      53,    57,   159,   161,   163,   165,   171,   173,   175,   169,
     179,   167,   177,   104,   104,   104,   104,   104,   104,   104,
     104,   104,     0,     0,     0,     0,    62,   182,   184,   186,
     188,   190,   203,   192,     0,     0,     0,     0,    65,   508,
     510,   516,   518,   520,   522,   512,   514,   524,    67,    71,
      73,    69,    55,    76,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   246,     0,     0,
       0,     0,   247,   243,   268,   245,   270,   272,   276,   274,
     278,   282,   280,   286,   284,     0,     0,   471,   474,     0,
     462,     0,     0,   300,     0,     0,   289,     0,     0,     0,
       0,     0,   368,   443,     0,     0,     0,     0,     0,   359,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   335,   346,   338,   345,   349,   341,     0,
     454,   457,   449,   479,   481,   483,   485,   487,   489,   491,
     530,   528,   532,   534,   536,   538,   540,   494,   496,   498,
     500,   502,   504,   506,    37,     0,     0,  1348,     0,     0,
    1309,     0,     0,     0,     0,     0,   121,     0,     0,   112,
     112,   181,   181,   181,   181,   181,   181,   181,   181,   181,
     181,   181,    79,    81,    83,    85,    87,    89,    91,    93,
      95,    96,    98,   100,   102,   205,   205,   205,   205,   205,
     205,   205,     0,   195,   198,   201,   526,   526,   526,   526,
     526,   526,   526,   526,   526,   206,   208,   210,   220,   222,
     214,   212,   218,   216,   226,   224,   230,   228,   235,   232,
     236,   238,   240,   259,   246,     0,     0,     0,     0,     0,
     446,   316,     0,   464,     0,     0,   316,     0,     0,   291,
     420,   421,   422,   423,   424,     0,     0,     0,     0,     0,
     419,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   345,   348,   345,   344,   355,   345,     0,   446,   316,
     451,     0,     0,     0,  1358,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1311,     0,     0,     0,     0,     0,   158,     0,     0,   109,
     111,   160,   162,   164,   166,   172,   174,   176,   170,   180,
     168,   178,   104,   104,   104,   104,   183,   185,   187,   189,
     191,   204,   193,     0,     0,     0,     0,     0,     0,     0,
       0,   205,   205,   205,   509,   511,   517,   519,   521,   523,
     513,   515,   525,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,   246,
       0,     0,     0,     0,     0,   244,     0,     0,   261,   465,
     467,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   446,   544,   545,   546,
     547,   548,   549,   550,   551,   552,   553,   554,   555,   556,
     557,   558,   559,   560,   561,   562,   563,   564,   565,   566,
     567,   568,   569,   570,   571,   572,   573,   574,   575,   576,
     577,   578,   579,   580,   581,   582,   607,   583,   584,   585,
     586,   587,   588,   589,   590,   591,   592,   593,   605,   594,
     595,   596,   597,   598,   599,   600,   601,   608,   602,   603,
     604,   606,   609,   610,   611,   612,   613,   614,   615,   616,
       0,     0,     0,     0,   463,   292,   294,     0,   298,     0,
     290,   360,   362,   366,   370,   364,     0,     0,   425,   428,
     431,   434,     0,   440,   416,   372,   374,   376,   379,   381,
     356,   416,   398,   394,   396,   400,     0,     0,     0,     0,
     392,   404,   402,   406,   408,   410,   412,   414,   336,   347,
     339,     0,     0,   342,   452,     0,     0,   450,  1312,  1344,
    1346,     0,     0,     0,  1314,  1316,  1324,  1332,  1326,  1320,
    1318,  1322,  1328,  1330,  1340,  1342,  1334,  1336,  1338,  1310,
       0,   113,   115,   117,   119,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   123,   125,    97,    99,   101,   103,   317,   319,   321,
     323,   325,   327,   329,   331,   196,   199,   202,   207,   209,
     211,   221,   223,   215,   213,   219,   217,   227,   225,   231,
     229,   233,   237,   239,   241,     0,     0,     0,     0,   248,
       0,     0,   263,   477,   477,   617,   447,   656,   658,   660,
     662,   664,   669,   671,   673,   682,   725,   727,   729,   731,
     733,   735,   737,   739,   741,   743,   756,   767,   769,   778,
     787,   789,   796,   801,   816,   818,   823,   825,   827,   860,
     875,   886,   893,   902,   911,   943,   948,   981,  1014,  1016,
    1027,  1090,  1092,  1094,  1096,  1113,  1129,  1138,  1140,  1145,
    1150,  1152,  1159,  1176,  1238,  1240,  1251,  1124,  1272,   932,
    1205,  1293,  1295,  1297,  1299,  1306,  1359,  1361,  1363,   472,
     444,   304,   309,   475,     0,   303,   303,   301,   303,     0,
     416,   416,   416,   416,   416,     0,   416,   446,   446,   446,
     446,   437,     0,   378,   416,   416,   416,   416,   416,   416,
     383,   416,   416,   416,   416,     0,     0,     0,     0,   416,
     416,   416,   416,   416,   416,   416,   416,     0,   350,   460,
     455,   458,  1351,  1351,  1351,     0,     0,  1349,  1351,  1351,
    1351,  1351,  1351,  1351,  1351,  1351,  1351,  1351,  1351,  1351,
    1351,  1351,  1351,     0,   127,   127,   127,   127,   128,   130,
     132,   134,   136,   138,   140,   142,   144,   146,   148,   150,
     152,   154,   156,   127,   127,   127,   333,   333,   333,   333,
     333,   333,   333,   333,     0,     0,     0,     0,   250,   266,
     264,   262,   466,   468,   655,     0,     0,     0,     0,     0,
     668,     0,     0,   681,   724,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   755,   766,     0,   777,   786,     0,
     795,   800,   815,     0,   822,     0,     0,   859,   874,   885,
     892,   901,   910,   931,   947,   980,  1013,     0,  1026,  1089,
       0,     0,     0,  1112,  1123,  1137,     0,  1144,  1149,     0,
    1158,  1175,  1204,     0,  1250,  1271,  1128,  1292,   942,  1237,
       0,     0,     0,  1305,     0,     0,     0,  1383,   477,     0,
       0,   477,   469,   293,   295,   303,   299,   296,   361,   363,
     367,   371,   365,     0,   369,     0,     0,     0,     0,   446,
       0,   373,   375,   377,   380,   382,     0,   399,   395,   397,
     401,   384,   386,   388,   390,   393,   405,   403,   407,   409,
     411,   413,   415,     0,   352,   453,   460,   460,  1313,  1345,
    1347,     0,     0,     0,  1351,  1315,  1317,  1325,  1333,  1327,
    1321,  1319,  1323,  1329,  1331,  1341,  1343,  1335,  1337,  1339,
     105,   114,   116,   118,   120,   158,   158,   158,   158,   158,
     158,   158,   158,   158,   158,   158,   158,   158,   158,   158,
     122,   124,   126,   318,   320,   322,   324,   326,   328,   330,
     332,   255,     0,     0,   257,   249,   268,   268,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   446,   657,   659,   661,   663,     0,     0,   670,   672,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   726,   728,   730,   732,   734,
     736,   738,   740,   742,     0,     0,     0,     0,     0,   768,
       0,     0,     0,     0,     0,     0,     0,     0,   788,     0,
       0,     0,     0,     0,     0,     0,     0,   817,     0,     0,
     824,   826,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1015,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1091,  1093,  1095,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1139,     0,     0,     0,
       0,  1151,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1239,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1294,  1296,  1298,
       0,     0,     0,  1307,  1360,  1362,     0,     0,     0,     0,
       0,     0,     0,     0,   473,     0,     0,   476,   477,   302,
     303,   417,   426,   429,   432,   435,     0,     0,   357,   416,
     416,   416,   416,   353,   351,   456,   459,  1352,  1354,  1356,
    1350,   107,   129,   131,   133,   135,   137,   139,   141,   143,
     145,   147,   149,   151,   153,   155,   157,   259,     0,     0,
     259,   267,   265,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   618,   445,     0,   665,     0,
     674,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   683,     0,     0,   744,     0,   757,     0,
       0,     0,   770,     0,     0,     0,   779,     0,   790,     0,
     797,     0,     0,     0,   802,     0,   819,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   828,     0,     0,     0,
       0,     0,     0,   861,     0,   876,     0,   887,     0,     0,
       0,   894,     0,   903,     0,     0,     0,     0,     0,     0,
     912,     0,   944,     0,     0,   949,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   982,     0,     0,     0,     0,
    1017,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1028,     0,     0,     0,     0,     0,     0,     0,  1097,     0,
       0,     0,  1114,     0,     0,     0,  1130,     0,  1141,     0,
    1146,     0,     0,  1153,     0,  1160,  1178,  1180,  1182,  1184,
    1186,  1188,  1190,  1192,  1194,     0,     0,     0,     0,  1177,
       0,     0,  1241,     0,     0,     0,     0,     0,  1252,     0,
    1125,     0,     0,  1273,     0,     0,     0,     0,   933,  1207,
    1209,  1211,  1213,  1215,  1217,  1219,  1221,  1223,  1225,  1227,
    1229,  1231,  1233,  1235,  1206,     0,     0,  1300,     0,     0,
       0,     0,     0,     0,     0,  1364,     0,     0,   470,   297,
     419,   443,   443,   443,   443,   438,     0,   359,   385,   387,
     389,   391,   355,  1358,  1358,  1358,   106,   256,   251,   253,
     258,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1204,  1204,  1204,  1204,  1204,  1204,  1204,  1204,  1204,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1237,  1237,  1237,  1237,  1237,  1237,  1237,  1237,  1237,  1237,
    1237,  1237,  1237,  1237,  1237,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   418,   427,
     430,   433,   436,   443,     0,   358,   354,  1353,  1355,  1357,
     259,   259,   619,   621,   623,   625,   637,   627,   629,   631,
     633,   635,   639,   641,   643,   645,   647,   649,   651,   653,
     666,   675,   677,   679,   684,   686,   688,   690,   692,   694,
     700,   696,   698,   702,   704,   706,   712,   708,   710,   716,
     718,   720,   714,   722,   745,   747,   749,   751,   753,   758,
     760,   762,   764,   771,   773,   775,   780,   782,   784,   791,
     793,   798,   803,   805,   807,   809,   811,   813,   820,   845,
     857,   829,   841,   831,   833,   835,   837,   839,   843,   849,
     847,   851,   853,   855,   862,   864,   866,   868,   870,   872,
     877,   879,   881,   883,   888,   890,   895,   899,   897,   904,
     906,   908,   929,   917,   919,   913,   915,   923,   921,   925,
     927,   945,   950,   952,   960,   968,   962,   956,   954,   958,
     964,   966,   976,   978,   970,   972,   974,   983,   985,   987,
     989,   991,   993,   995,   997,   999,  1001,  1005,  1003,  1007,
    1009,  1011,  1022,  1024,  1018,  1020,  1029,  1031,  1033,  1035,
    1037,  1039,  1041,  1043,  1045,  1047,  1061,  1049,  1053,  1055,
    1057,  1059,  1063,  1051,  1065,  1067,  1069,  1071,  1073,  1075,
    1077,  1079,  1081,  1083,  1085,  1087,  1098,  1100,  1102,  1104,
    1106,  1108,  1110,  1115,  1117,  1119,  1121,  1131,  1133,  1135,
    1142,  1147,  1156,  1154,  1161,  1163,  1165,  1167,  1169,  1171,
    1173,  1179,  1181,  1183,  1185,  1187,  1189,  1191,  1193,  1195,
    1196,  1198,  1200,  1202,  1242,  1244,  1246,  1248,  1253,  1255,
    1257,  1259,  1261,  1263,  1265,  1267,  1269,  1126,  1274,  1278,
    1288,  1282,  1280,  1276,  1284,  1286,  1290,   938,   940,   934,
     936,  1208,  1210,  1212,  1214,  1216,  1218,  1220,  1222,  1224,
    1226,  1228,  1230,  1232,  1234,  1236,  1303,  1301,  1381,  1365,
    1373,  1377,  1379,  1369,  1367,  1371,  1375,   305,   310,   439,
       0,   252,   254,   655,   655,   655,   655,   655,   655,   655,
     655,   655,   655,   655,   655,   655,   655,   655,   655,   655,
     655,   668,   681,   681,   681,   724,   724,   724,   724,   724,
     724,   724,   724,   724,   724,   724,   724,   724,   724,   724,
     724,   724,   724,   724,   724,   755,   755,   755,   755,   755,
     766,   766,   766,   766,   777,   777,   777,   786,   786,   786,
     795,   795,   800,   815,   815,   815,   815,   815,   815,   822,
     859,   859,   859,   859,   859,   859,   859,   859,   859,   859,
     859,   859,   859,   859,   859,   874,   874,   874,   874,   874,
     874,   885,   885,   885,   885,   892,   892,   901,   901,   901,
     910,   910,   910,   931,   931,   931,   931,   931,   931,   931,
     931,   931,   947,   980,   980,   980,   980,   980,   980,   980,
     980,   980,   980,   980,   980,   980,   980,   980,  1013,  1013,
    1013,  1013,  1013,  1013,  1013,  1013,  1013,  1013,  1013,  1013,
    1013,  1013,  1013,  1026,  1026,  1026,  1026,  1089,  1089,  1089,
    1089,  1089,  1089,  1089,  1089,  1089,  1089,  1089,  1089,  1089,
    1089,  1089,  1089,  1089,  1089,  1089,  1089,  1089,  1089,  1089,
    1089,  1089,  1089,  1089,  1089,  1089,  1089,  1112,  1112,  1112,
    1112,  1112,  1112,  1112,  1123,  1123,  1123,  1123,  1137,  1137,
    1137,  1144,  1149,  1158,  1158,  1175,  1175,  1175,  1175,  1175,
    1175,  1175,  1204,  1204,  1204,  1204,  1250,  1250,  1250,  1250,
    1271,  1271,  1271,  1271,  1271,  1271,  1271,  1271,  1271,  1128,
    1292,  1292,  1292,  1292,  1292,  1292,  1292,  1292,  1292,   942,
     942,   942,   942,  1305,  1305,  1383,  1383,  1383,  1383,  1383,
    1383,  1383,  1383,  1383,   333,   333,   446,   620,   622,   624,
     626,   638,   628,   630,   632,   634,   636,   640,   642,   644,
     646,   648,   650,   652,   654,   667,   676,   678,   680,   685,
     687,   689,   691,   693,   695,   701,   697,   699,   703,   705,
     707,   713,   709,   711,   717,   719,   721,   715,   723,   746,
     748,   750,   752,   754,   759,   761,   763,   765,   772,   774,
     776,   781,   783,   785,   792,   794,   799,   804,   806,   808,
     810,   812,   814,   821,   846,   858,   830,   842,   832,   834,
     836,   838,   840,   844,   850,   848,   852,   854,   856,   863,
     865,   867,   869,   871,   873,   878,   880,   882,   884,   889,
     891,   896,   900,   898,   905,   907,   909,   930,   918,   920,
     914,   916,   924,   922,   926,   928,   946,   951,   953,   961,
     969,   963,   957,   955,   959,   965,   967,   977,   979,   971,
     973,   975,   984,   986,   988,   990,   992,   994,   996,   998,
    1000,  1002,  1006,  1004,  1008,  1010,  1012,  1023,  1025,  1019,
    1021,  1030,  1032,  1034,  1036,  1038,  1040,  1042,  1044,  1046,
    1048,  1062,  1050,  1054,  1056,  1058,  1060,  1064,  1052,  1066,
    1068,  1070,  1072,  1074,  1076,  1078,  1080,  1082,  1084,  1086,
    1088,  1099,  1101,  1103,  1105,  1107,  1109,  1111,  1116,  1118,
    1120,  1122,  1132,  1134,  1136,  1143,  1148,  1157,  1155,  1162,
    1164,  1166,  1168,  1170,  1172,  1174,  1197,  1199,  1201,  1203,
    1243,  1245,  1247,  1249,  1254,  1256,  1258,  1260,  1262,  1264,
    1266,  1268,  1270,  1127,  1275,  1279,  1289,  1283,  1281,  1277,
    1285,  1287,  1291,   939,   941,   935,   937,  1304,  1302,  1382,
    1366,  1374,  1378,  1380,  1370,  1368,  1372,  1376,   306,   311,
       0,     0,     0,     0,   307,     0,   441,   316,     0,   443,
     308,     0,   442,   312,   333,   313,     0,   314,   316,   315
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -2518, -2518, -2518, -2518,   271, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,  1805,
   -2518,  1575, -2518,   296, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518,  -488, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,   196, -2518,  -373, -2518, -2518,
    -430, -2518, -2518, -2518, -2518, -2518, -2518, -2518,  -636, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518,   108, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518,   -71, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
    -496, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518,   660, -2518, -2518, -1966, -2518, -2518, -2518, -2518,   861,
   -2518, -2518, -1264, -2518, -2518,   419, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,  1291, -2518, -2518, -1386, -2518,
   -2518, -2518, -2518, -2518, -2518,  -874, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518,  -478, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,  -655, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,  1247, -2518,   544, -2518, -2518,   -51, -2518,
     -40, -2518, -2518, -1186, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518,   -28, -2518, -2518, -2200, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518,  -925, -2518,  1251,
   -2518, -2518, -1343, -2518, -2518, -2518, -2518, -2518,  1332, -2518,
   -2518, -1308, -2518, -2518, -2518, -2518, -2518, -2518, -2518,   559,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518,   668, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,    11, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518,   682, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518,  -103, -2518, -2518, -2518, -1567, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518,  -617, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -1924, -2518, -2518, -2518, -2518, -2518,
   -1625, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2146, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2208,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -1951, -2518,
   -2518, -2518, -2518, -2518, -1939, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2402, -2518, -2518, -2518, -2518,  -657, -2518, -2518,
   -2518, -2271, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518,  -663, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -1725, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2070, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2235, -2518, -2518,
   -2518, -2518, -2518, -2518, -2413, -2518, -2518, -2518, -2518, -1943,
   -2518, -2518, -2518, -2518, -2518, -1931, -2518, -2518, -2518, -2518,
   -2518, -1927, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2340, -2518, -2518, -2518, -2518, -2518, -2518,
    -705, -2518, -2518, -2518, -1693, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -1674, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2157, -2518, -2518, -2518, -2518, -2518, -2518, -1777,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -1679, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2107, -2518,
   -2518, -2518, -2518, -2518, -2518,  -811, -2518, -2518, -2518, -2000,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518,  -782, -2518, -2518,
   -2518,  -781, -2518, -2518, -2518, -2518, -2518, -2483, -2518, -2518,
   -2518, -2518, -1690, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2404, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -1221, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2115, -2518,
   -2518, -2518, -2518, -2518, -2518, -2016, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -1996, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2517, -2518, -2518, -2518, -2518,
    1240, -2518, -2518,  -747, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -1237, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518, -1967, -2518, -2518, -2518, -2518, -2518,
   -2518, -2518, -2518, -2518
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,     1,     2,    11,    25,    60,   250,    67,   397,    68,
     402,    69,   404,    66,   395,    91,   427,    94,   430,   175,
     426,    93,   605,    59,   431,   437,   438,   439,   440,   441,
     442,   496,   443,   181,   201,   481,   492,   495,   493,   494,
     497,   335,   643,   644,   645,   646,   647,   648,   649,   650,
     651,   972,   973,   974,   975,   304,  1971,   184,   305,   306,
     623,  1474,  1475,  1476,  1477,   956,  1494,  1495,  1280,  1665,
    1666,  1667,  1668,  1669,  1670,  1671,  1672,  1673,  1674,  1675,
    1676,  1677,  1678,  1679,   200,   801,   802,   803,   804,   810,
     808,   805,   806,   807,   811,   809,   211,   825,   826,   827,
     828,   829,   831,   477,   991,   478,   992,   479,   993,   830,
     136,  1003,  1004,  1005,  1009,  1008,  1011,  1010,  1006,  1007,
    1013,  1012,  1015,  1014,  1016,   697,  1017,  1018,  1019,   381,
     864,   517,   863,  1508,  1024,  2510,  2511,  1987,  1990,   383,
     704,  1322,   867,  1697,  1696,    65,   521,   522,   524,   523,
     525,   527,   526,   529,   528,   145,   396,   879,   542,  1405,
    1406,  1950,  1408,   876,  1595,  1182,  1589,  3024,  3271,  3277,
    1590,  3025,  3272,  3284,  3286,  3288,   665,  1496,  1497,  1498,
    1499,  1500,  1501,  1502,  1503,   150,   398,   921,   399,   923,
     401,   926,   756,   572,   922,   575,   925,  1634,  1232,  2222,
     904,  1429,  2217,   570,  1410,  1411,  1414,  1412,   890,  1413,
    1424,  1425,  1426,  1427,  1428,  1959,  1960,  1961,  1962,  1439,
    1432,  1433,  1431,  1434,  1441,  1440,  1442,  1443,  1444,  1445,
    1446,  1197,  2210,   885,   897,  1417,  2211,  1418,  2212,  1419,
    2213,  1420,  2214,  1609,  2503,  1422,  3279,  1105,  1515,   152,
     403,   930,   580,  1449,   928,  1636,   929,  1637,   143,   394,
     873,   535,  1323,  1324,  1948,   870,  1588,   871,  1591,    77,
     582,   583,   584,   585,   586,   587,   588,    90,   596,   597,
     598,   599,   600,   601,   602,   222,   836,   837,   842,   843,
     838,   839,   840,   841,   844,    82,   590,   589,   591,   592,
     593,   594,   595,   193,  1106,  1107,  1514,  1710,  2783,  2784,
    2785,  2786,  2788,  2789,  2790,  2791,  2792,  2787,  2793,  2794,
    2795,  2796,  2797,  2798,  2799,  2800,  1108,  1516,  1109,  1517,
    1110,  1518,  1111,  1519,  1112,  1520,  1717,  2801,  1113,  1521,
    1114,  1522,  1115,  1523,  1721,  2802,  2803,  2804,  1116,  1524,
    1734,  2805,  2806,  2807,  2808,  2809,  2810,  2812,  2813,  2811,
    2814,  2815,  2816,  2818,  2819,  2817,  2823,  2820,  2821,  2822,
    2824,  1117,  1525,  1118,  1526,  1119,  1527,  1120,  1528,  1121,
    1529,  1122,  1530,  1123,  1531,  1124,  1532,  1125,  1533,  1126,
    1534,  1746,  2825,  2826,  2827,  2828,  2829,  1127,  1535,  1748,
    2830,  2831,  2832,  2833,  1128,  1536,  1129,  1537,  1753,  2834,
    2835,  2836,  1130,  1538,  1757,  2837,  2838,  2839,  1131,  1539,
    1132,  1540,  1760,  2840,  2841,  1133,  1541,  1762,  2842,  1134,
    1542,  1766,  2843,  2844,  2845,  2846,  2847,  2848,  1135,  1543,
    1136,  1544,  1769,  2849,  1137,  1545,  1138,  1546,  1139,  1547,
    1781,  2852,  2854,  2855,  2856,  2857,  2858,  2853,  2859,  2850,
    2861,  2860,  2862,  2863,  2864,  2851,  1140,  1548,  1788,  2865,
    2866,  2867,  2868,  2869,  2870,  1141,  1549,  1790,  2871,  2872,
    2873,  2874,  1142,  1550,  1792,  2875,  2876,  1143,  1551,  1796,
    2877,  2879,  2878,  1144,  1552,  1798,  2880,  2881,  2882,  1145,
    1553,  1805,  2886,  2887,  2884,  2885,  2889,  2888,  2890,  2891,
    2883,  1146,  1578,  1910,  3011,  3012,  3009,  3010,  1147,  1554,
    1807,  2892,  1148,  1555,  1810,  2893,  2894,  2899,  2898,  2900,
    2895,  2897,  2901,  2902,  2896,  2905,  2906,  2907,  2903,  2904,
    1149,  1556,  1820,  2908,  2909,  2910,  2911,  2912,  2913,  2914,
    2915,  2916,  2917,  2919,  2918,  2920,  2921,  2922,  1150,  1557,
    1151,  1558,  1826,  2925,  2926,  2923,  2924,  1152,  1559,  1846,
    2927,  2928,  2929,  2930,  2931,  2932,  2933,  2934,  2935,  2936,
    2938,  2944,  2939,  2940,  2941,  2942,  2937,  2943,  2945,  2946,
    2947,  2948,  2949,  2950,  2951,  2952,  2953,  2954,  2955,  2956,
    1153,  1560,  1154,  1561,  1155,  1562,  1156,  1563,  1857,  2957,
    2958,  2959,  2960,  2961,  2962,  2963,  1157,  1564,  1861,  2964,
    2965,  2966,  2967,  1158,  1576,  1902,  2999,  1159,  1565,  1865,
    2968,  2969,  2970,  1160,  1566,  1161,  1567,  1868,  2971,  1162,
    1568,  1870,  2972,  1163,  1569,  1164,  1570,  1874,  2974,  2973,
    1165,  1571,  1876,  2975,  2976,  2977,  2978,  2979,  2980,  2981,
    1166,  1572,  1890,  2430,  2431,  2432,  2433,  2434,  2435,  2436,
    2437,  2438,  2982,  2983,  2984,  2985,  1167,  1579,  1926,  2470,
    2471,  2472,  2473,  2474,  2475,  2476,  2477,  2478,  2479,  2480,
    2481,  2482,  2483,  2484,  1168,  1573,  1169,  1574,  1894,  2986,
    2987,  2988,  2989,  1170,  1575,  1900,  2990,  2991,  2992,  2993,
    2994,  2995,  2996,  2997,  2998,  1171,  1577,  1905,  3000,  3005,
    3001,  3004,  3003,  3006,  3007,  3002,  3008,  1172,  1580,  1173,
    1581,  1174,  1582,  1175,  1583,  1932,  3014,  3013,  1176,  1584,
     179,   429,   950,   611,  1452,  1458,  1459,  1464,  1463,  1465,
    1460,  1462,  1466,  1467,  1461,  1470,  1471,  1472,  1468,  1469,
    1453,  1454,   934,  1644,  1243,  2223,  2224,  2225,  1177,  1585,
    1178,  1586,  1179,  1587,  1943,  3016,  3021,  3020,  3022,  3017,
    3023,  3018,  3019,  3015
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     666,   667,  1187,  1235,   212,   624,   223,   224,   225,  1772,
     227,  2499,  2500,  2501,  2502,  1512,  1513,  1020,  1423,  1593,
    1594,  2227,  1596,   705,  2230,  1430,  2711,  2712,  2713,  2714,
    2715,  2716,  2717,  2718,  2719,  1265,  1266,  1267,  1268,  1269,
    1270,  1271,  1272,  1273,    39,    40,  1936,    41,  1799,  1241,
     935,   936,   937,   938,   939,  1236,   940,   941,   942,    12,
      13,   286,    61,    62,   473,  1021,    42,    43,    44,    45,
      46,    47,    48,   298,   146,  1930,  1750,  1911,  1912,  1913,
    1914,  1915,  1916,  1917,  1918,  1919,  2341,  2342,  2343,  2344,
    2345,   147,  2346,  2347,  2348,  1754,   616,   617,   618,   619,
     620,   924,  2458,   256,   257,  2459,  1635,  2460,  2250,  2265,
    2266,    14,  1892,    15,  2278,  2279,   253,   254,    16,   932,
      17,  1698,  1699,  1700,  1701,  2291,  2292,  1702,  2461,  1862,
    1863,  1703,  1704,  2414,   891,   892,   893,   894,   895,    18,
     865,    19,  2262,   182,   183,  1903,  2288,   448,   449,    20,
    1180,  2319,  2320,  1872,     3,   812,   813,   814,   815,   816,
     817,   818,   819,   820,   400,  2336,  1744,  1242,   188,   189,
    1773,     4,  1774,     5,   905,  1775,  1904,   322,     6,  1776,
     323,  1400,  1782,  1822,  2450,  2337,  1181,   444,   445,   446,
    1777,  1022,  1800,   324,     7,  1763,   194,   195,   196,   197,
       8,   858,  1823,  1937,  2492,  1764,  1765,  2251,  2252,  1949,
    2415,  1801,   191,   192,   451,   452,     9,  2234,   933,    10,
    1783,    26,  1824,  1825,  1598,  1599,  1600,  1601,  1602,  2259,
    1604,  1802,  1803,    27,  1705,  1642,  1643,   475,  1611,  1612,
    1613,  1614,  1615,  1616,  1938,  1617,  1618,  1619,  1620,   487,
    1873,    28,  2235,  1625,  1626,  1627,  1628,  1629,  1630,  1631,
    1632,   258,  1274,  1275,  1276,  2263,  1228,    29,  1230,   450,
    1784,  1233,    30,  1931,   255,  1850,    31,   943,   944,  2289,
    1944,   866,  1778,  1947,  2493,  2494,   148,  1745,   544,  2338,
    2339,    32,   474,  1965,  1966,    33,    49,  1893,    34,  2280,
    2281,  1864,  1785,  2779,  1920,  1921,  1922,  2451,  2452,   447,
    2293,  2294,  1858,  2349,  2350,  1751,   198,   199,   259,  1752,
    1939,  1940,    95,  1941,    35,  1942,    36,   545,    50,  2267,
      37,   621,   606,    38,  1755,  1706,  2321,  2322,  1756,   546,
     547,   548,   549,   550,  1707,   906,   907,    92,  1827,  2260,
    2261,    51,   908,  2236,  2237,    78,  2238,  2239,  2240,    79,
      80,    81,   652,   653,   280,   655,  1808,  1809,  1025,   896,
    1851,  1852,  1853,  1854,  1855,   577,   578,   579,  1277,   530,
     531,   532,   533,  1278,  2323,  2324,   229,   325,   326,   327,
     328,   329,   330,    52,   331,   332,   333,   334,   945,  1786,
    1787,  2366,  2367,   537,   538,    21,   539,    96,   540,    97,
      53,  2300,   260,  1859,  1860,    98,  1708,  1709,  1906,    99,
    1923,   281,   282,   283,    54,  1924,   959,   960,   551,   552,
     553,   279,  1991,  1992,  2351,    22,   287,  1907,  3084,  3085,
    1793,    55,  1794,   100,   295,   607,  2301,    23,   299,   101,
      63,    64,   880,   881,   882,   883,   884,  1908,  1909,    56,
    1856,  1795,  3119,  3120,  1779,  1780,   102,   608,   103,  1023,
     104,    57,   622,   149,  1804,   609,   610,  2328,  2329,  2330,
     534,    58,   105,  1279,  1283,  1284,  1285,  1286,   106,    24,
    3217,  3218,  1605,  1606,  1607,  1608,  3257,  3258,   946,  2462,
    2463,  2464,  2465,   108,   541,   107,   115,  1298,  1299,  1300,
    1301,  1302,  1303,  1304,  1305,  1306,  1307,  1308,  1309,  1310,
    1311,  1312,  1313,  1314,   109,  1925,  2274,  2275,  2276,  2277,
     554,   555,   556,  1877,  2352,   557,  1878,   110,   558,   559,
     560,   561,   562,   111,  2781,  2782,  1828,  2302,  2303,  1879,
    2304,  2305,  2306,  1829,  1830,  1831,  1832,  1833,  1834,  1835,
    1836,  1837,  1838,   112,  2209,   113,  1839,  1840,  1841,  1842,
    1843,  1844,  3087,  3088,  3089,  3090,  3091,  3092,  3226,  3227,
    3228,  3229,  2268,  2269,  2270,  2271,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     947,   948,   949,   436,  2375,  2376,  2377,  2378,  2379,  2380,
    2381,  2382,  2383,  2357,  2358,  2359,  2360,   455,   563,   564,
     565,   566,  3074,  3075,  3076,  3077,   887,   888,  2353,  2354,
    2355,   898,   899,   900,   114,   902,  3115,  3116,  3117,  3118,
    2208,   480,   137,   914,   138,   916,   917,   918,   919,  2363,
    2364,  2365,   491,   139,   140,   520,  1722,  1723,  1724,  1725,
    1726,  1727,  1728,   141,   142,  1729,   536,  1730,   543,  3253,
    3254,  3255,  3256,   576,   144,   581,   151,   153,   567,  3069,
    3070,  3071,  3072,  3073,  1956,   154,   568,   155,   156,   952,
     157,   954,   158,   159,   957,   958,  1031,   160,   604,   161,
     162,   612,   163,   164,   569,  1638,  1639,  1640,   165,   166,
    1845,  1645,  1646,  1647,  1648,  1649,  1650,  1651,  1652,  1653,
    1654,  1655,  1656,  1657,  1658,  1659,   173,   613,   167,   615,
    2395,  2396,  2397,   625,   626,   627,   628,   629,   630,   631,
    2443,  2444,  2445,  1880,  1881,  1882,  1883,  1884,  1885,   174,
    1886,  1887,  1888,  1889,   976,   977,   978,   979,   980,   981,
     982,   168,   169,  1895,   170,   656,  3167,  3168,  3169,  3170,
    1032,  2384,   171,  2218,  2219,  2220,  2221,   668,  1896,   172,
     176,  1897,  2447,  2448,  2449,  1898,  2006,  1899,   678,   679,
     680,   681,   682,   683,   130,  3109,  3110,  3111,  3112,  3113,
    3114,  1033,  1034,  1035,  1036,  1037,  1038,  1039,  1040,  1041,
    1042,  1043,  1044,  1045,  1046,  1047,  1048,  1049,  1050,  1051,
    1052,  1053,  1054,  1055,  1056,  1057,  1058,  1059,  1060,  1061,
    1062,  1063,  1064,  1065,  1066,  1067,  1068,  1069,  1070,  1071,
    1072,  1073,  1074,  1075,   131,   132,   133,   994,   995,   996,
     997,   998,   999,  1000,  1001,  1002,  1811,  3208,  3209,  3210,
    3211,   134,   177,  1812,   213,   214,   215,   216,   217,   218,
    1813,  3230,  3231,  3232,  3233,   178,   219,   220,  3046,  3047,
    3048,   180,  1731,  3078,  3079,  3080,   185,   186,  1732,   202,
     203,   204,   205,   226,   206,   187,   190,  1970,  3081,  3082,
    3083,   228,   230,  1733,  1076,   207,   208,   209,   210,   961,
     962,   963,   964,   965,   966,   967,   968,   969,   970,   971,
    1295,  1296,  1297,   231,   232,   221,    70,    71,    72,    73,
      74,    75,    76,  1077,  3121,  3122,  3123,   233,   234,   135,
     706,   707,   708,   709,   710,   711,   712,   713,   714,  3124,
    3125,  3126,   235,   248,  1078,  1079,  3127,  3128,  3129,  3130,
    3131,  3132,  3133,  3134,  3135,   236,   237,  1080,  3212,  3213,
    3214,   249,   238,  1081,  3234,  3235,  3236,  3237,  3238,  3239,
    3240,  3241,  3242,   239,   240,  1082,  2507,  2508,  2509,  1814,
    1815,  1083,  1084,  1085,  1816,   241,   242,  1817,  1818,  1819,
     243,   244,  1086,  1087,  3244,  3245,  3246,  3247,  3248,  3249,
    3250,  3251,  3252,   245,   246,   247,   251,  1088,  1683,  1684,
    1685,  1686,  1687,  1688,  1689,  1690,  1089,   252,  1090,  1972,
    1973,  1974,  1975,  1976,  1977,  1978,  1979,  1980,  1981,  1982,
    1983,  1984,  1985,  1986,  1661,  1662,  1663,  1664,  3259,  3260,
    3261,  3262,  3263,  3264,  3265,  3266,  3267,   261,   267,   262,
     263,  1091,   264,  1680,  1681,  1682,   265,  1092,   266,  1093,
     268,   284,   269,   270,   272,   271,  1094,   273,   274,  3282,
     983,   984,   985,   986,   987,   988,   989,   990,   275,   276,
     277,   278,   285,  1095,    83,    84,    85,    86,    87,    88,
      89,   288,   289,   290,   291,   292,  1096,   293,   294,   296,
    1097,   297,  1098,  1099,  1100,   300,  1101,  2423,  2424,  2425,
    2426,  2427,  2428,  2429,  1102,  3094,  3095,  3096,  3097,  3098,
    3099,  3100,  3101,  3102,  3103,  3104,  3105,  3106,  3107,  3108,
    1103,   763,   764,   765,   766,   767,   768,   769,   301,  1104,
    3171,  3172,  3173,  3174,  3175,  3176,  3177,  3178,  3179,  3180,
    3181,  3182,  3183,  3184,  3185,  3186,  3187,  3188,  3189,  3190,
    3191,  3192,  3193,  3194,  3195,  3196,  3197,  3198,  3199,  3200,
    3049,  3050,  3051,  3052,  3053,  3054,  3055,  3056,  3057,  3058,
    3059,  3060,  3061,  3062,  3063,  3064,  3065,  3066,  3067,  3068,
    3137,  3138,  3139,  3140,  3141,  3142,  3143,  3144,  3145,  3146,
    3147,  3148,  3149,  3150,  3151,  1507,  3027,  3028,  3029,  3030,
    3031,  3032,  3033,  3034,  3035,  3036,  3037,  3038,  3039,  3040,
    3041,  3042,  3043,  3044,  3152,  3153,  3154,  3155,  3156,  3157,
    3158,  3159,  3160,  3161,  3162,  3163,  3164,  3165,  3166,  2751,
    2752,  2753,  2754,  2755,  2756,  2757,  2758,  2759,  2760,  2761,
    2762,  2763,  2764,  2765,   777,   778,   779,   780,   781,   782,
     783,   770,   771,   772,   773,   774,   775,   776,  3201,  3202,
    3203,  3204,  3205,  3206,  3207,  3219,  3220,  3221,  3222,  3223,
    3224,  3225,   302,   303,   307,   315,   308,   309,   310,   311,
     312,   313,   314,   321,   316,   317,   318,   319,   320,   336,
     337,   338,   339,   340,   341,   342,   343,   344,   346,   345,
     347,   348,   349,   350,   351,   352,   353,   354,   355,   356,
     357,   358,   359,   360,   361,   363,   362,   364,   365,   366,
     367,   368,   369,   370,   371,   372,   373,   374,   375,   376,
     377,   378,   379,   380,   382,   384,   385,   386,   387,   388,
     389,   390,   391,   392,   393,   405,   406,   407,   408,   409,
     410,   411,   412,   413,   414,   415,   416,   417,   418,   419,
     420,   421,   422,   423,   424,   425,   428,   433,   432,   434,
     435,   453,   454,   456,   469,   516,   457,   458,   459,   460,
     461,   462,   463,   464,   518,   465,   466,   467,   468,   470,
     471,   472,   476,   482,   483,   484,   485,   486,   488,   489,
     519,   490,   664,   702,   903,   703,   498,   499,   500,   717,
     501,   502,   503,   504,   505,   506,   571,   507,   508,   509,
     510,   511,   512,   574,   513,   514,   515,   573,   614,   632,
     718,   723,   633,   720,   634,   635,   636,   637,   638,   639,
     640,   641,   642,   654,   657,   726,   658,   659,   660,   661,
     662,   663,   669,   670,   671,   672,   673,   674,   675,   676,
     677,   732,   684,   685,   686,   687,   688,   689,   690,   691,
     692,   693,   694,   695,   696,   698,   699,   700,   701,   733,
     739,   757,   715,   716,   719,   721,   722,   724,   725,   727,
     728,   729,   730,   731,   734,   735,   736,   737,   738,   740,
     741,   742,   743,   744,   745,   746,   747,   748,   749,   750,
     751,   752,   753,   754,   755,   758,   760,   759,   761,   785,
     762,   787,   786,   790,   788,   796,  1198,   789,   799,   800,
     821,   791,   792,   793,   794,   795,   797,   798,   822,   823,
     824,   833,   832,   834,   835,   845,   846,   847,   848,   849,
     850,   851,   852,   853,   854,   855,   856,   857,   859,   860,
     861,   862,  1028,   868,   869,  1183,   872,  1026,   874,   875,
     877,  1029,   878,   886,   889,   901,  1027,   909,   910,   911,
     912,   913,   915,  1030,   920,   927,   931,  1185,   951,   953,
     955,  1199,  1200,  1186,  1188,  1196,  1191,  1189,  1192,  1193,
    1194,  1201,  1195,  1202,  1203,  1216,  1204,  1205,  1206,  1207,
    1208,  1209,  1210,  1212,  1211,  1213,  1214,  1215,  1231,  1319,
    1217,  1218,  1219,  1220,  1221,  1222,  1223,  1224,  1225,  1226,
    1227,  1234,  1238,  1239,  1240,  1244,  1245,  1246,  1247,  1248,
    1249,  1250,  1251,  1252,  1253,  1254,  1255,  1256,  1257,  1258,
    1321,  1261,  1260,  1262,  1263,  1264,  1281,  1282,  1287,  1288,
    1289,  1290,  1291,  1292,  1293,  1294,  1325,  1315,  1316,  1317,
    1318,  1320,  1326,  1327,  1328,  1329,  1330,  1331,  1332,  1333,
    1334,  1335,  1336,  1337,  1338,  1339,  1340,  1341,  1342,  1343,
    1344,  1345,  1346,  1347,  1348,  1349,  1350,  1351,  1352,  1353,
    1354,  1355,  1356,  1357,  1358,  1359,  1360,  1361,  1362,  1363,
    1364,  1365,  1366,  1367,  1368,  1369,  1370,  1371,  1372,  1373,
    1374,  1375,  1376,  1377,  1378,  1379,  1380,  1381,  1382,  1383,
    1384,  1385,  1386,  1387,  1388,  1389,  1390,  1391,  1392,  1393,
    1394,  1395,  1396,  1397,  1398,  1415,  1399,  1401,  1402,  1404,
    1403,  1407,  1409,  1416,  1421,  1435,  1436,  1437,  1438,  1448,
    1450,  1451,  1447,  1455,  1456,  1457,  1473,  1478,  1493,  1711,
    1610,  1712,  1713,  1479,  1714,  1480,  1481,  1482,  1483,  1484,
    1485,  1486,  1715,  1487,  1488,  1489,  1490,  1491,  1492,  1504,
    1509,  1505,  1506,  1692,  1716,  1510,  1592,  1597,  1603,  1621,
    1693,  1622,  1623,  1624,  1633,  1660,  1641,  1718,  1719,  1720,
    1691,  1694,  1735,  1736,  1737,  1738,  1739,  1740,  1741,  1742,
    1743,  1747,  1749,  1758,  1761,  1759,  1767,  1768,  1770,  1771,
    1789,  1791,  1797,  1806,  1821,  1847,  1848,  1849,  1866,  1867,
    1869,  1871,  1891,  1875,  1901,  1927,  1928,  1929,  1933,  1934,
    1935,  1945,  1951,  1952,  1953,  1946,  1954,  1955,  1957,  1958,
    1963,  1967,  1968,  1969,  1988,  1989,  1993,  2005,  2008,  1994,
    1995,  2232,  2233,  1996,  2010,  2242,  2243,  2244,  2245,  2246,
    2247,  2248,  1997,  1998,  2023,  1999,  2000,  2001,  2253,  2254,
    2255,  2256,  2257,  2258,  2002,  2003,  2264,  2004,  2007,  2272,
    2009,  2011,  2012,  2013,  2014,  2026,  2028,  2283,  2284,  2015,
    2016,  2286,  2287,  2017,  2018,  2019,  2290,  2020,  2021,  2295,
    2296,  2022,  2297,  2024,  2298,  2299,  2025,  2027,  2308,  2309,
    2310,  2311,  2312,  2029,  2313,  2314,  2315,  2316,  2030,  2318,
    2031,  2032,  2033,  2034,  2035,  2325,  2326,  2327,  2036,  2037,
    2038,  2039,  2332,  2333,  2334,  2335,  2040,  2041,  2042,  2043,
    2044,  2045,  2046,  2356,  2047,  2048,  2049,  2050,  2051,  2368,
    2369,  2370,  2052,  2371,  2372,  2373,  2374,  2053,  2054,  2055,
    2386,  2387,  2388,  2389,  2056,  2057,  2392,  2058,  2063,  2065,
    2398,  2399,  2400,  2401,  2067,  2059,  2071,  2073,  2405,  2060,
    2080,  2082,  2409,  2410,  2061,  2062,  2412,  2064,  2066,  2068,
    2069,  2417,  2418,  2070,  2419,  2085,  2072,  2095,  2100,  2422,
    2074,  2120,  2075,  2076,  2077,  2078,  2079,  2081,  2083,  2084,
    2086,  2087,  2439,  2440,  2088,  2442,  2128,  2089,  2090,  2091,
    2092,  2093,  2453,  2454,  2455,  2094,  2096,  2097,  2457,  2098,
    2099,  2466,  2467,  2468,  2469,  2101,  2102,  2103,  2104,  2105,
    2106,  2107,  2108,  2109,  2110,  2111,  2132,  2136,  2112,  2113,
    2114,  2115,  2485,  2116,  2117,  2487,  2488,  2118,  2490,  2491,
    2138,  3270,  2119,  2121,  2122,  2123,  2124,  2140,  2143,  2125,
    2126,  2127,  2129,  2145,  2130,  2159,  2131,  2133,  2134,  2135,
    2162,  2137,  2139,  2141,  2142,  2144,  2146,  2147,  2168,  2170,
    2148,  2149,  2150,  2151,  2173,  2178,  2194,  2197,  2152,  2153,
    2154,  2205,  2155,  2156,  2157,  2158,  2160,  2161,  2163,  2164,
    2165,  2166,  2167,  2169,  2171,  2172,  2174,  2175,  2176,  2177,
    2195,  2179,  2180,  2181,  2182,  2183,  2215,  2226,  1695,  1229,
    1190,  2506,  3026,  3275,  3273,  3274,  3276,  2505,  1964,  3287,
     784,  1237,  2498,  1511,  3045,  3086,  3093,  3136,  3243,  3215,
    1259,  3216,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  1184,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    2780,   603,     0,     0,     0,     0,     0,     0,     0,     0,
    2184,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    2185,  2186,     0,     0,     0,  2187,  2188,  2189,     0,  2190,
    2191,     0,     0,  2192,     0,     0,  2193,     0,     0,  2196,
    2198,  2199,  2200,  2201,  2202,  2203,  2216,  2204,  2206,  2207,
    2228,  2229,  2231,  2241,  2504,  2249,  2273,  2282,  2285,  2307,
    2317,  2331,  2340,  2361,  2362,  2385,     0,  2390,     0,  2391,
    2393,  2394,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  2402,     0,     0,  2403,  2404,     0,  2406,     0,     0,
    2407,  2408,  2411,  2413,     0,     0,     0,     0,     0,  2416,
    2420,     0,     0,  2421,     0,  2441,     0,     0,     0,     0,
       0,     0,  2446,     0,     0,  2456,  3278,     0,  2486,     0,
    2489,  2495,  2496,  2497,     0,     0,     0,  2512,  2513,  2514,
       0,     0,  2515,  2516,  2517,  2518,  2519,     0,  2520,  2521,
    2522,  2523,  2524,  2525,  2526,  2527,  2528,     0,  2529,  2530,
    2531,  2532,  2533,  2534,  2535,  2536,  2537,  2538,  2539,  2540,
    2541,  2542,  2543,  2544,  2545,  2546,  2547,  2548,     0,     0,
    2549,  2550,  2551,  3280,  2552,  2553,  2554,  2555,  2556,  2557,
    2558,  2559,  2560,  2561,  3289,  2562,  2563,  2564,  2565,  2566,
    2567,  2568,  2569,  2570,  2571,  2572,  2573,  2574,  2575,  2576,
    2577,  2578,  2579,  2580,     0,  2581,  2582,  2583,  2584,  2585,
    2586,     0,  2587,     0,  2588,  2589,  2590,  2591,  2592,     0,
    2593,  2594,  2595,     0,  2596,     0,  2597,  2598,  2599,  2600,
    2601,  2602,  2603,  2604,  2605,  2606,  2607,  2608,  2609,  2610,
    2611,  2612,  2613,  2614,  2615,  2616,  2617,     0,  2618,  2619,
    2620,     0,     0,  2621,  2622,  2623,  2624,     0,  2625,     0,
       0,  2626,  2627,     0,     0,  2628,  2629,  2630,  2631,  2632,
    2633,  2634,  2635,  2636,  2637,  2638,  2639,  2640,     0,  2641,
       0,     0,  2642,  2643,     0,  2644,  2645,  2646,  2647,  2648,
    2649,  2650,  2651,  2652,  2653,  2654,  2655,  2656,  2657,     0,
    2658,  2659,  2660,  2661,  2662,  2663,  2664,  2665,  2666,  2667,
    2668,  2669,  2670,  2671,  2672,  2673,  3268,  3269,  2674,  2675,
    2676,  2677,  2678,  2679,  2680,  2681,  2682,  2683,  2684,     0,
       0,  2685,  2686,  2687,  2688,  2689,  2690,  2691,  2692,  2693,
    2694,  2695,  2696,     0,  2697,  2698,  2699,  2700,  2701,  2702,
       0,     0,  2703,  2704,  2705,  2706,     0,  2707,     0,  2708,
    2709,  2710,  2720,     0,  2721,  2722,  2723,  2724,  2725,  2726,
    2727,     0,     0,  2728,  2729,  2730,  2731,     0,     0,     0,
       0,  2732,  2733,  2734,     0,  2735,  2736,  2737,  2738,  2739,
    2740,  2741,  2742,  2743,  2744,  2745,  2746,  2747,  2748,  2749,
    2750,  2766,  2767,  2768,  2769,  2770,  2771,  2772,  2773,     0,
    2774,  2775,  2776,  2777,  2778,     0,     0,  3281,     0,     0,
       0,  3283,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  3285
};

static const yytype_int16 yycheck[] =
{
     478,   479,   876,   928,   107,   435,   109,   110,   111,     5,
     113,  2211,  2212,  2213,  2214,  1323,  1324,    20,  1204,  1405,
    1406,  1987,  1408,   519,  1990,  1211,  2430,  2431,  2432,  2433,
    2434,  2435,  2436,  2437,  2438,    20,    21,    22,    23,    24,
      25,    26,    27,    28,     7,     8,   186,    10,    23,    75,
      20,    21,    22,    23,    24,   929,    26,    27,    28,     5,
       6,   164,    13,    14,    68,    68,    29,    30,    31,    32,
      33,    34,    35,   176,    67,   186,   200,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    20,    21,    22,    23,
      24,    84,    26,    27,    28,   200,    15,    16,    17,    18,
      19,   756,    59,   188,   189,    62,  1449,    64,    80,   188,
     189,    57,   193,    59,    13,    14,   188,   189,    64,    80,
      66,   165,   166,   167,   168,    13,    14,   171,    85,   299,
     300,   175,   176,   194,    78,    79,    80,    81,    82,    85,
      20,    87,    68,    13,    14,   424,    68,    36,    37,    95,
      68,    13,    14,   220,     0,   643,   644,   645,   646,   647,
     648,   649,   650,   651,   267,   326,    72,   193,    36,    37,
     166,   157,   168,   475,    99,   171,   455,    43,   476,   175,
      46,  1106,   252,   167,   323,   346,   104,    36,    37,    38,
     186,   194,   167,    59,   475,   193,    39,    40,    41,    42,
     476,   697,   186,   343,   384,   203,   204,   179,   180,  1595,
     271,   186,    11,    12,   317,   318,   475,    33,   179,   477,
     290,     3,   206,   207,  1410,  1411,  1412,  1413,  1414,    68,
    1416,   206,   207,     3,   278,   213,   214,   340,  1424,  1425,
    1426,  1427,  1428,  1429,   384,  1431,  1432,  1433,  1434,   352,
     317,     3,    68,  1439,  1440,  1441,  1442,  1443,  1444,  1445,
    1446,   346,   247,   248,   249,   191,   921,     3,   923,   158,
     340,   926,     3,   384,   346,   186,     3,   247,   248,   201,
    1588,   161,   278,  1591,   464,   465,   279,   193,    22,   450,
     451,     3,   296,  1636,  1637,     3,   259,   378,     3,   198,
     199,   471,   372,  2503,   247,   248,   249,   446,   447,   158,
     198,   199,   186,   247,   248,   439,   159,   160,   403,   443,
     460,   461,   478,   463,     3,   465,     3,    61,   291,   408,
       3,   250,    65,     3,   439,   379,   198,   199,   443,    73,
      74,    75,    76,    77,   388,   270,   271,   392,    22,   188,
     189,   314,   277,   169,   170,    96,   172,   173,   174,   100,
     101,   102,   465,   466,    40,   468,   208,   209,   864,   313,
     281,   282,   283,   284,   285,    61,    62,    63,   363,    60,
      61,    62,    63,   368,    13,    14,   115,   253,   254,   255,
     256,   257,   258,   356,   260,   261,   262,   263,   368,   469,
     470,   325,   326,    60,    61,   351,    63,     3,    65,     3,
     373,    33,   141,   287,   288,   478,   460,   461,   167,   478,
     363,    97,    98,    99,   387,   368,   799,   800,   162,   163,
     164,   160,  1696,  1697,   368,   381,   165,   186,  2840,  2841,
     184,   404,   186,   478,   173,   178,    68,   393,   177,   478,
     401,   402,    68,    69,    70,    71,    72,   206,   207,   422,
     371,   205,  2875,  2876,   460,   461,   478,   200,   478,   472,
       3,   434,   391,   466,   449,   208,   209,   409,   410,   411,
     161,   444,     3,   468,   972,   973,   974,   975,     3,   435,
    2973,  2974,  1417,  1418,  1419,  1420,  3013,  3014,   468,   456,
     457,   458,   459,     3,   161,   478,     4,  1003,  1004,  1005,
    1006,  1007,  1008,  1009,  1010,  1011,  1012,  1013,  1014,  1015,
    1016,  1017,  1018,  1019,   478,   468,   194,   195,   196,   197,
     264,   265,   266,    43,   468,   269,    46,   478,   272,   273,
     274,   275,   276,   478,  2510,  2511,   220,   169,   170,    59,
     172,   173,   174,   227,   228,   229,   230,   231,   232,   233,
     234,   235,   236,   478,  1950,   478,   240,   241,   242,   243,
     244,   245,  2843,  2844,  2845,  2846,  2847,  2848,  2982,  2983,
    2984,  2985,   414,   415,   416,   417,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
     210,   211,   212,   307,   218,   219,   220,   221,   222,   223,
     224,   225,   226,   213,   214,   215,   216,   321,   352,   353,
     354,   355,  2830,  2831,  2832,  2833,   729,   730,   210,   211,
     212,   734,   735,   736,   478,   738,  2871,  2872,  2873,  2874,
    1948,   345,   478,   746,   478,   748,   749,   750,   751,   321,
     322,   323,   356,   478,   478,   384,   181,   182,   183,   184,
     185,   186,   187,     4,    58,   190,   395,   192,   397,  3009,
    3010,  3011,  3012,   402,    65,   404,    86,   478,   412,  2825,
    2826,  2827,  2828,  2829,  1609,   478,   420,   478,   478,   792,
     478,   794,   478,   478,   797,   798,     9,     4,   427,   478,
     478,   430,   478,   478,   438,  1452,  1453,  1454,     4,   478,
     384,  1458,  1459,  1460,  1461,  1462,  1463,  1464,  1465,  1466,
    1467,  1468,  1469,  1470,  1471,  1472,     4,   431,   478,   433,
     237,   238,   239,   437,   438,   439,   440,   441,   442,   443,
     375,   376,   377,   253,   254,   255,   256,   257,   258,   474,
     260,   261,   262,   263,   825,   826,   827,   828,   829,   830,
     831,   478,   478,   430,   478,   469,  2923,  2924,  2925,  2926,
      83,   385,   478,  1959,  1960,  1961,  1962,   481,   445,   478,
     478,   448,   431,   432,   433,   452,  1711,   454,   492,   493,
     494,   495,   496,   497,   251,  2865,  2866,  2867,  2868,  2869,
    2870,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   301,   302,   303,   836,   837,   838,
     839,   840,   841,   842,   843,   844,   186,  2964,  2965,  2966,
    2967,   318,     4,   193,   357,   358,   359,   360,   361,   362,
     200,  2986,  2987,  2988,  2989,   436,   369,   370,  2802,  2803,
    2804,   474,   407,  2834,  2835,  2836,   475,   475,   413,   292,
     293,   294,   295,   421,   297,   475,   474,  1644,  2837,  2838,
    2839,   474,   478,   428,   217,   308,   309,   310,   311,   801,
     802,   803,   804,   805,   806,   807,   808,   809,   810,   811,
     991,   992,   993,   478,   478,   418,    88,    89,    90,    91,
      92,    93,    94,   246,  2877,  2878,  2879,   478,   478,   396,
     521,   522,   523,   524,   525,   526,   527,   528,   529,  2880,
    2881,  2882,   478,     3,   267,   268,  2883,  2884,  2885,  2886,
    2887,  2888,  2889,  2890,  2891,   478,   478,   280,  2968,  2969,
    2970,     3,   478,   286,  2990,  2991,  2992,  2993,  2994,  2995,
    2996,  2997,  2998,   478,   478,   298,  2223,  2224,  2225,   319,
     320,   304,   305,   306,   324,   478,   478,   327,   328,   329,
     478,   478,   315,   316,  3000,  3001,  3002,  3003,  3004,  3005,
    3006,  3007,  3008,   478,   478,   478,   474,   330,  1496,  1497,
    1498,  1499,  1500,  1501,  1502,  1503,   339,   474,   341,  1665,
    1666,  1667,  1668,  1669,  1670,  1671,  1672,  1673,  1674,  1675,
    1676,  1677,  1678,  1679,  1474,  1475,  1476,  1477,  3015,  3016,
    3017,  3018,  3019,  3020,  3021,  3022,  3023,     3,   478,     4,
       3,   374,     4,  1493,  1494,  1495,     3,   380,     3,   382,
       3,   475,     4,     3,   474,     4,   389,   474,   474,  3279,
     106,   107,   108,   109,   110,   111,   112,   113,   474,   474,
     474,   474,   474,   406,    88,    89,    90,    91,    92,    93,
      94,   474,   474,   474,   474,   474,   419,   474,   474,   477,
     423,     4,   425,   426,   427,     3,   429,   332,   333,   334,
     335,   336,   337,   338,   437,  2850,  2851,  2852,  2853,  2854,
    2855,  2856,  2857,  2858,  2859,  2860,  2861,  2862,  2863,  2864,
     453,   582,   583,   584,   585,   586,   587,   588,     4,   462,
    2927,  2928,  2929,  2930,  2931,  2932,  2933,  2934,  2935,  2936,
    2937,  2938,  2939,  2940,  2941,  2942,  2943,  2944,  2945,  2946,
    2947,  2948,  2949,  2950,  2951,  2952,  2953,  2954,  2955,  2956,
    2805,  2806,  2807,  2808,  2809,  2810,  2811,  2812,  2813,  2814,
    2815,  2816,  2817,  2818,  2819,  2820,  2821,  2822,  2823,  2824,
    2893,  2894,  2895,  2896,  2897,  2898,  2899,  2900,  2901,  2902,
    2903,  2904,  2905,  2906,  2907,  1318,  2783,  2784,  2785,  2786,
    2787,  2788,  2789,  2790,  2791,  2792,  2793,  2794,  2795,  2796,
    2797,  2798,  2799,  2800,  2908,  2909,  2910,  2911,  2912,  2913,
    2914,  2915,  2916,  2917,  2918,  2919,  2920,  2921,  2922,  2470,
    2471,  2472,  2473,  2474,  2475,  2476,  2477,  2478,  2479,  2480,
    2481,  2482,  2483,  2484,   596,   597,   598,   599,   600,   601,
     602,   589,   590,   591,   592,   593,   594,   595,  2957,  2958,
    2959,  2960,  2961,  2962,  2963,  2975,  2976,  2977,  2978,  2979,
    2980,  2981,   477,   475,     4,   478,   477,   477,   477,   477,
     477,   477,   477,     4,   478,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,     3,     3,     3,   477,     4,
     478,   478,   478,   478,   478,   478,   478,   478,   478,     4,
     477,   477,   477,   477,   477,   475,   477,   475,   475,   475,
     475,   475,   475,   475,   475,   475,   475,   475,   475,   475,
     475,   475,   475,   475,   397,     4,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,     4,   478,     3,
       3,   474,   474,   477,     4,   161,   477,   477,   477,   477,
     477,   477,   477,   477,     3,   478,   478,   478,   478,   475,
     475,   475,   475,   474,   474,   474,   474,   474,   474,   474,
       4,   475,   105,     3,    67,     4,   479,   479,   479,     3,
     479,   479,   479,   479,   479,   479,   474,   479,   479,   479,
     479,   477,   479,   467,   479,   479,   479,   477,   475,   477,
       3,     3,   477,     4,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   475,   477,     4,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,     3,   475,   475,   475,   475,   475,   475,   475,   475,
     475,   475,   475,   475,   475,   475,   475,   475,   475,     3,
       3,     3,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,     4,   477,     4,     4,     3,   478,     3,   478,
       4,     3,   478,     4,   478,     3,     3,   478,     4,     4,
     477,   479,   478,   478,   478,   478,   478,   478,   477,   477,
     477,     4,   478,     4,     4,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,     4,   475,   474,   479,   475,   478,   475,   474,
     474,   477,   475,   474,   474,   474,   478,   475,   475,   475,
     475,   474,   474,   477,   475,   474,   474,   477,   475,   475,
     475,     3,     3,   477,   477,   475,   477,   479,   477,   477,
     477,     3,   477,   475,     3,   479,     4,   477,   477,   477,
     477,   477,     3,   477,     4,   477,   477,   477,    75,     4,
     479,   479,   479,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     475,   477,   479,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,     3,   478,   478,   478,
     478,   474,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,   478,     4,     3,     3,   475,
       4,     4,   475,     4,     3,   475,   475,   475,   475,     4,
       4,     4,   478,   478,   478,     4,   475,   477,     4,     4,
      20,     4,     4,   477,     4,   477,   477,   477,   477,   477,
     477,   477,     4,   477,   477,   477,   477,   477,   477,   474,
     477,   475,   475,   479,   177,   477,   477,   477,   475,   477,
     479,   477,   477,   477,   474,   477,   474,     4,     4,   178,
     477,   477,     4,     4,     4,     4,     4,     4,     4,     4,
       4,   193,     4,     4,   202,   193,     4,   473,     4,     4,
     193,   193,   193,   193,     4,     4,     4,     4,     4,   405,
     307,     4,     4,   331,   383,     4,     4,     4,     4,     4,
       4,   103,   477,     4,     4,   103,     4,     4,   478,     4,
     477,   477,   477,   477,   475,   475,   478,     4,     4,   478,
     478,  1994,  1995,   478,     4,  1998,  1999,  2000,  2001,  2002,
    2003,  2004,   478,   478,     4,   478,   478,   478,  2011,  2012,
    2013,  2014,  2015,  2016,   478,   478,  2019,   478,   478,  2022,
     478,   478,   478,   478,   478,     4,     4,  2030,  2031,   478,
     478,  2034,  2035,   478,   478,   478,  2039,   478,   478,  2042,
    2043,   478,  2045,   478,  2047,  2048,   478,   478,  2051,  2052,
    2053,  2054,  2055,   478,  2057,  2058,  2059,  2060,   478,  2062,
     478,     4,   478,   478,   478,  2068,  2069,  2070,     4,   478,
       4,   478,  2075,  2076,  2077,  2078,     4,   478,   478,   478,
       4,   478,     4,  2086,   478,   478,   478,   478,   478,  2092,
    2093,  2094,   478,  2096,  2097,  2098,  2099,   478,   478,   478,
    2103,  2104,  2105,  2106,     4,   478,  2109,   478,     4,     4,
    2113,  2114,  2115,  2116,     4,   478,     4,     4,  2121,   478,
       4,     4,  2125,  2126,   478,   478,  2129,   478,   478,   478,
     478,  2134,  2135,   478,  2137,     4,   478,     4,     4,  2142,
     478,     4,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,  2155,  2156,   478,  2158,     4,   478,   478,   478,
     478,   478,  2165,  2166,  2167,   478,   478,   478,  2171,   478,
     478,  2174,  2175,  2176,  2177,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,   478,     4,     4,   478,   478,
     478,   478,  2195,   478,   478,  2198,  2199,   478,  2201,  2202,
       4,  3026,   478,   478,   478,   478,   478,     4,     4,   478,
     478,   478,   478,     4,   478,     4,   478,   478,   478,   478,
       4,   478,   478,   478,   478,   478,   477,   477,     4,     4,
     477,   477,   477,   477,     4,     4,     4,     4,   477,   477,
     477,     4,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   477,   477,   477,   477,   477,     4,  1971,  1508,   922,
     879,  2222,     3,   103,     4,     4,     4,  2217,  1634,     4,
     605,   930,  2210,  1322,  2801,  2842,  2849,  2892,  2999,  2971,
     950,  2972,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   873,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      62,   426,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     477,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     477,   477,    -1,    -1,    -1,   477,   477,   477,    -1,   477,
     477,    -1,    -1,   477,    -1,    -1,   477,    -1,    -1,   478,
     478,   478,   478,   478,   478,   478,   474,   478,   478,   478,
     477,   477,   474,   474,   477,   475,   474,   474,   474,   474,
     474,   474,   474,   474,   474,   474,    -1,   475,    -1,   474,
     474,   474,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   474,    -1,    -1,   474,   474,    -1,   474,    -1,    -1,
     474,   474,   474,   474,    -1,    -1,    -1,    -1,    -1,   474,
     474,    -1,    -1,   474,    -1,   475,    -1,    -1,    -1,    -1,
      -1,    -1,   475,    -1,    -1,   474,   478,    -1,   474,    -1,
     474,   474,   474,   474,    -1,    -1,    -1,   477,   477,   477,
      -1,    -1,   477,   477,   477,   477,   477,    -1,   477,   477,
     477,   477,   477,   477,   477,   477,   477,    -1,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,    -1,    -1,
     477,   477,   477,  3277,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,  3288,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,    -1,   477,   477,   477,   477,   477,
     477,    -1,   477,    -1,   477,   477,   477,   477,   477,    -1,
     477,   477,   477,    -1,   477,    -1,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,    -1,   477,   477,
     477,    -1,    -1,   477,   477,   477,   477,    -1,   477,    -1,
      -1,   477,   477,    -1,    -1,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,    -1,   477,
      -1,    -1,   477,   477,    -1,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,    -1,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,  3024,  3025,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,    -1,
      -1,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,    -1,   477,   477,   477,   477,   477,   477,
      -1,    -1,   477,   477,   477,   477,    -1,   477,    -1,   477,
     477,   477,   477,    -1,   477,   477,   477,   477,   477,   477,
     477,    -1,    -1,   477,   477,   477,   477,    -1,    -1,    -1,
      -1,   477,   477,   477,    -1,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,    -1,
     477,   477,   477,   477,   477,    -1,    -1,   474,    -1,    -1,
      -1,   477,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  3284
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int16 yystos[] =
{
       0,   481,   482,     0,   157,   475,   476,   475,   476,   475,
     477,   483,     5,     6,    57,    59,    64,    66,    85,    87,
      95,   351,   381,   393,   435,   484,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     7,
       8,    10,    29,    30,    31,    32,    33,    34,    35,   259,
     291,   314,   356,   373,   387,   404,   422,   434,   444,   503,
     485,    13,    14,   401,   402,   625,   493,   487,   489,   491,
      88,    89,    90,    91,    92,    93,    94,   749,    96,   100,
     101,   102,   775,    88,    89,    90,    91,    92,    93,    94,
     757,   495,   392,   501,   497,   478,     3,     3,   478,   478,
     478,   478,   478,   478,     3,     3,     3,   478,     3,   478,
     478,   478,   478,   478,   478,     4,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
     251,   301,   302,   303,   318,   396,   590,   478,   478,   478,
     478,     4,    58,   738,    65,   635,    67,    84,   279,   466,
     665,    86,   729,   478,   478,   478,   478,   478,   478,   478,
       4,   478,   478,   478,   478,     4,   478,   478,   478,   478,
     478,   478,   478,     4,   474,   499,   478,     4,   436,  1220,
     474,   513,    13,    14,   537,   475,   475,   475,    36,    37,
     474,    11,    12,   783,    39,    40,    41,    42,   159,   160,
     564,   514,   292,   293,   294,   295,   297,   308,   309,   310,
     311,   576,   783,   357,   358,   359,   360,   361,   362,   369,
     370,   418,   765,   783,   783,   783,   421,   783,   474,   484,
     478,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,   478,   478,   478,     3,     3,
     486,   474,   474,   188,   189,   346,   188,   189,   346,   403,
     484,     3,     4,     3,     4,     3,     3,   478,     3,     4,
       3,     4,   474,   474,   474,   474,   474,   474,   474,   484,
      40,    97,    98,    99,   475,   474,   783,   484,   474,   474,
     474,   474,   474,   474,   474,   484,   477,     4,   783,   484,
       3,     4,   477,   475,   535,   538,   539,     4,   477,   477,
     477,   477,   477,   477,   477,   478,   478,   478,   478,   478,
     478,     4,    43,    46,    59,   253,   254,   255,   256,   257,
     258,   260,   261,   262,   263,   521,   478,   478,   478,   478,
     478,   478,     3,     3,     3,     4,   477,   478,   478,   478,
     478,   478,   478,   478,   478,   478,     4,   477,   477,   477,
     477,   477,   477,   475,   475,   475,   475,   475,   475,   475,
     475,   475,   475,   475,   475,   475,   475,   475,   475,   475,
     475,   609,   397,   619,     4,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   739,   494,   636,   488,   666,   668,
     783,   670,   490,   730,   492,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   500,   496,   477,  1221,
     498,   504,   478,     4,     3,     3,   503,   505,   506,   507,
     508,   509,   510,   512,    36,    37,    38,   158,    36,    37,
     158,   783,   783,   474,   474,   503,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   478,   478,   478,   478,     4,
     475,   475,   475,    68,   296,   783,   475,   583,   585,   587,
     503,   515,   474,   474,   474,   474,   474,   783,   474,   474,
     475,   503,   516,   518,   519,   517,   511,   520,   479,   479,
     479,   479,   479,   479,   479,   479,   479,   479,   479,   479,
     479,   477,   479,   479,   479,   479,   161,   611,     3,     4,
     484,   626,   627,   629,   628,   630,   632,   631,   634,   633,
      60,    61,    62,    63,   161,   741,   484,    60,    61,    63,
      65,   161,   638,   484,    22,    61,    73,    74,    75,    76,
      77,   162,   163,   164,   264,   265,   266,   269,   272,   273,
     274,   275,   276,   352,   353,   354,   355,   412,   420,   438,
     683,   474,   673,   477,   467,   675,   484,    61,    62,    63,
     732,   484,   750,   751,   752,   753,   754,   755,   756,   777,
     776,   778,   779,   780,   781,   782,   758,   759,   760,   761,
     762,   763,   764,   499,   484,   502,    65,   178,   200,   208,
     209,  1223,   484,   503,   475,   503,    15,    16,    17,    18,
      19,   250,   391,   540,   540,   503,   503,   503,   503,   503,
     503,   503,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   522,   523,   524,   525,   526,   527,   528,
     529,   530,   783,   783,   475,   783,   503,   477,   477,   477,
     477,   477,   477,   477,   105,   656,   656,   656,   503,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   503,   503,
     503,   503,   503,   503,   475,   475,   475,   475,   475,   475,
     475,   475,   475,   475,   475,   475,   475,   605,   475,   475,
     475,   475,     3,     4,   620,   590,   625,   625,   625,   625,
     625,   625,   625,   625,   625,   478,   478,     3,     3,   478,
       4,   478,   478,     3,   478,   478,     4,   478,   478,   478,
     478,   478,     3,     3,   478,   478,   478,   478,   478,     3,
     478,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,     4,   477,     4,   672,     3,     4,   478,
       3,     3,     4,   749,   749,   749,   749,   749,   749,   749,
     775,   775,   775,   775,   775,   775,   775,   757,   757,   757,
     757,   757,   757,   757,   501,   478,   478,     3,   478,   478,
       4,   479,   478,   478,   478,   478,     3,   478,   478,     4,
       4,   565,   566,   567,   568,   571,   572,   573,   570,   575,
     569,   574,   521,   521,   521,   521,   521,   521,   521,   521,
     521,   477,   477,   477,   477,   577,   578,   579,   580,   581,
     589,   582,   478,     4,     4,     4,   766,   767,   770,   771,
     772,   773,   768,   769,   774,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   590,   477,
     477,   477,   477,   612,   610,    20,   161,   622,   475,   474,
     745,   747,   475,   740,   475,   474,   643,   474,   475,   637,
      68,    69,    70,    71,    72,   713,   474,   783,   783,   474,
     688,    78,    79,    80,    81,    82,   313,   714,   783,   783,
     783,   474,   783,    67,   680,    99,   270,   271,   277,   475,
     475,   475,   475,   474,   783,   474,   783,   783,   783,   783,
     475,   667,   674,   669,   665,   676,   671,   474,   734,   736,
     731,   474,    80,   179,  1242,    20,    21,    22,    23,    24,
      26,    27,    28,   247,   248,   368,   468,   210,   211,   212,
    1222,   475,   783,   475,   783,   475,   545,   783,   783,   537,
     537,   564,   564,   564,   564,   564,   564,   564,   564,   564,
     564,   564,   531,   532,   533,   534,   576,   576,   576,   576,
     576,   576,   576,   106,   107,   108,   109,   110,   111,   112,
     113,   584,   586,   588,   765,   765,   765,   765,   765,   765,
     765,   765,   765,   591,   592,   593,   598,   599,   595,   594,
     597,   596,   601,   600,   603,   602,   604,   606,   607,   608,
      20,    68,   194,   472,   614,   590,   478,   478,     4,   477,
     477,     9,    83,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   217,   246,   267,   268,
     280,   286,   298,   304,   305,   306,   315,   316,   330,   339,
     341,   374,   380,   382,   389,   406,   419,   423,   425,   426,
     427,   429,   437,   453,   462,   727,   784,   785,   806,   808,
     810,   812,   814,   818,   820,   822,   828,   851,   853,   855,
     857,   859,   861,   863,   865,   867,   869,   877,   884,   886,
     892,   898,   900,   905,   909,   918,   920,   924,   926,   928,
     946,   955,   962,   967,   973,   979,   991,   998,  1002,  1020,
    1038,  1040,  1047,  1080,  1082,  1084,  1086,  1096,  1103,  1107,
    1113,  1115,  1119,  1123,  1125,  1130,  1140,  1156,  1174,  1176,
    1183,  1195,  1207,  1209,  1211,  1213,  1218,  1248,  1250,  1252,
      68,   104,   645,   479,   738,   477,   477,   645,   477,   479,
     635,   477,   477,   477,   477,   477,   475,   711,     3,     3,
       3,     3,   475,     3,     4,   477,   477,   477,   477,   477,
       3,     4,   477,   477,   477,   477,   479,   479,   479,   479,
     477,   477,   477,   477,   477,   477,   477,   477,   665,   673,
     665,    75,   678,   665,   477,   727,   645,   729,   477,   477,
     477,    75,   193,  1244,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,  1220,
     479,   477,   477,   477,   477,    20,    21,    22,    23,    24,
      25,    26,    27,    28,   247,   248,   249,   363,   368,   468,
     548,   477,   477,   521,   521,   521,   521,   477,   477,   477,
     477,   477,   477,   477,   477,   576,   576,   576,   590,   590,
     590,   590,   590,   590,   590,   590,   590,   590,   590,   590,
     590,   590,   590,   590,   590,   478,   478,   478,   478,     4,
     474,   475,   621,   742,   743,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     4,
     727,     3,     3,     4,   475,   639,   640,     4,   642,   475,
     684,   685,   687,   689,   686,   478,     4,   715,   717,   719,
     721,     3,   725,   683,   690,   691,   692,   693,   694,   681,
     683,   702,   700,   701,   703,   475,   475,   475,   475,   699,
     705,   704,   706,   707,   708,   709,   710,   478,     4,   733,
       4,     4,  1224,  1240,  1241,   478,   478,     4,  1225,  1226,
    1230,  1234,  1231,  1228,  1227,  1229,  1232,  1233,  1238,  1239,
    1235,  1236,  1237,   475,   541,   542,   543,   544,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,     4,   546,   547,   657,   658,   659,   660,
     661,   662,   663,   664,   474,   475,   475,   783,   613,   477,
     477,   619,   741,   741,   786,   728,   807,   809,   811,   813,
     815,   819,   821,   823,   829,   852,   854,   856,   858,   860,
     862,   864,   866,   868,   870,   878,   885,   887,   893,   899,
     901,   906,   910,   919,   921,   925,   927,   929,   947,   956,
     963,   968,   974,   980,   999,  1003,  1021,  1039,  1041,  1048,
    1081,  1083,  1085,  1087,  1097,  1108,  1114,  1116,  1120,  1124,
    1126,  1131,  1141,  1175,  1177,  1184,  1104,  1196,   992,  1157,
    1208,  1210,  1212,  1214,  1219,  1249,  1251,  1253,   746,   646,
     650,   748,   477,   638,   638,   644,   638,   477,   683,   683,
     683,   683,   683,   475,   683,   727,   727,   727,   727,   723,
      20,   683,   683,   683,   683,   683,   683,   683,   683,   683,
     683,   477,   477,   477,   477,   683,   683,   683,   683,   683,
     683,   683,   683,   474,   677,   732,   735,   737,  1223,  1223,
    1223,   474,   213,   214,  1243,  1223,  1223,  1223,  1223,  1223,
    1223,  1223,  1223,  1223,  1223,  1223,  1223,  1223,  1223,  1223,
     477,   540,   540,   540,   540,   549,   550,   551,   552,   553,
     554,   555,   556,   557,   558,   559,   560,   561,   562,   563,
     540,   540,   540,   656,   656,   656,   656,   656,   656,   656,
     656,   477,   479,   479,   477,   611,   624,   623,   165,   166,
     167,   168,   171,   175,   176,   278,   379,   388,   460,   461,
     787,     4,     4,     4,     4,     4,   177,   816,     4,     4,
     178,   824,   181,   182,   183,   184,   185,   186,   187,   190,
     192,   407,   413,   428,   830,     4,     4,     4,     4,     4,
       4,     4,     4,     4,    72,   193,   871,   193,   879,     4,
     200,   439,   443,   888,   200,   439,   443,   894,     4,   193,
     902,   202,   907,   193,   203,   204,   911,     4,   473,   922,
       4,     4,     5,   166,   168,   171,   175,   186,   278,   460,
     461,   930,   252,   290,   340,   372,   469,   470,   948,   193,
     957,   193,   964,   184,   186,   205,   969,   193,   975,    23,
     167,   186,   206,   207,   449,   981,   193,  1000,   208,   209,
    1004,   186,   193,   200,   319,   320,   324,   327,   328,   329,
    1022,     4,   167,   186,   206,   207,  1042,    22,   220,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   240,
     241,   242,   243,   244,   245,   384,  1049,     4,     4,     4,
     186,   281,   282,   283,   284,   285,   371,  1088,   186,   287,
     288,  1098,   299,   300,   471,  1109,     4,   405,  1117,   307,
    1121,     4,   220,   317,  1127,   331,  1132,    43,    46,    59,
     253,   254,   255,   256,   257,   258,   260,   261,   262,   263,
    1142,     4,   193,   378,  1178,   430,   445,   448,   452,   454,
    1185,   383,  1105,   424,   455,  1197,   167,   186,   206,   207,
     993,    20,    21,    22,    23,    24,    25,    26,    27,    28,
     247,   248,   249,   363,   368,   468,  1158,     4,     4,     4,
     186,   384,  1215,     4,     4,     4,   186,   343,   384,   460,
     461,   463,   465,  1254,   741,   103,   103,   741,   744,   638,
     641,   477,     4,     4,     4,     4,   727,   478,     4,   695,
     696,   697,   698,   477,   675,   732,   732,   477,   477,   477,
    1223,   536,   548,   548,   548,   548,   548,   548,   548,   548,
     548,   548,   548,   548,   548,   548,   548,   617,   475,   475,
     618,   622,   622,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,     4,   727,   478,     4,   478,
       4,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,     4,   478,   478,     4,   478,     4,   478,
     478,   478,     4,   478,   478,   478,     4,   478,     4,   478,
       4,   478,   478,   478,     4,   478,     4,   478,   478,   478,
     478,   478,   478,   478,   478,   478,     4,   478,   478,   478,
     478,   478,   478,     4,   478,     4,   478,     4,   478,   478,
     478,     4,   478,     4,   478,   478,   478,   478,   478,   478,
       4,   478,     4,   478,   478,     4,   478,   478,   478,   478,
     478,   478,   478,   478,   478,     4,   478,   478,   478,   478,
       4,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   478,   478,   478,   478,   478,   478,   478,   478,
       4,   478,   478,   478,   478,   478,   478,   478,     4,   478,
     478,   478,     4,   478,   478,   478,     4,   478,     4,   478,
       4,   478,   478,     4,   478,     4,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   478,   478,   478,   478,     4,
     478,   478,     4,   478,   478,   478,   478,   478,     4,   478,
       4,   478,   478,     4,   478,   478,   478,   478,     4,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,     4,   478,   478,     4,   478,   478,
     478,   478,   478,   478,   478,     4,   478,   478,   741,   638,
     712,   716,   718,   720,   722,     4,   474,   682,   683,   683,
     683,   683,   679,  1245,  1246,  1247,   535,   614,   477,   477,
     614,   474,   783,   783,    33,    68,   169,   170,   172,   173,
     174,   474,   783,   783,   783,   783,   783,   783,   783,   475,
      80,   179,   180,   783,   783,   783,   783,   783,   783,    68,
     188,   189,    68,   191,   783,   188,   189,   408,   414,   415,
     416,   417,   783,   474,   194,   195,   196,   197,    13,    14,
     198,   199,   474,   783,   783,   474,   783,   783,    68,   201,
     783,    13,    14,   198,   199,   783,   783,   783,   783,   783,
      33,    68,   169,   170,   172,   173,   174,   474,   783,   783,
     783,   783,   783,   783,   783,   783,   783,   474,   783,    13,
      14,   198,   199,    13,    14,   783,   783,   783,   409,   410,
     411,   474,   783,   783,   783,   783,   326,   346,   450,   451,
     474,    20,    21,    22,    23,    24,    26,    27,    28,   247,
     248,   368,   468,   210,   211,   212,   783,   213,   214,   215,
     216,   474,   474,   321,   322,   323,   325,   326,   783,   783,
     783,   783,   783,   783,   783,   218,   219,   220,   221,   222,
     223,   224,   225,   226,   385,   474,   783,   783,   783,   783,
     475,   474,   783,   474,   474,   237,   238,   239,   783,   783,
     783,   783,   474,   474,   474,   783,   474,   474,   474,   783,
     783,   474,   783,   474,   194,   271,   474,   783,   783,   783,
     474,   474,   783,   332,   333,   334,   335,   336,   337,   338,
    1143,  1144,  1145,  1146,  1147,  1148,  1149,  1150,  1151,   783,
     783,   475,   783,   375,   376,   377,   475,   431,   432,   433,
     323,   446,   447,   783,   783,   783,   474,   783,    59,    62,
      64,    85,   456,   457,   458,   459,   783,   783,   783,   783,
    1159,  1160,  1161,  1162,  1163,  1164,  1165,  1166,  1167,  1168,
    1169,  1170,  1171,  1172,  1173,   783,   474,   783,   783,   474,
     783,   783,   384,   464,   465,   474,   474,   474,   711,   714,
     714,   714,   714,   724,   477,   680,   678,  1244,  1244,  1244,
     615,   616,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,  1142,  1142,  1142,  1142,  1142,  1142,  1142,  1142,  1142,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   477,
     477,  1158,  1158,  1158,  1158,  1158,  1158,  1158,  1158,  1158,
    1158,  1158,  1158,  1158,  1158,  1158,   477,   477,   477,   477,
     477,   477,   477,   477,   477,   477,   477,   477,   477,   714,
      62,   614,   614,   788,   789,   790,   791,   797,   792,   793,
     794,   795,   796,   798,   799,   800,   801,   802,   803,   804,
     805,   817,   825,   826,   827,   831,   832,   833,   834,   835,
     836,   839,   837,   838,   840,   841,   842,   845,   843,   844,
     847,   848,   849,   846,   850,   872,   873,   874,   875,   876,
     880,   881,   882,   883,   889,   890,   891,   895,   896,   897,
     903,   904,   908,   912,   913,   914,   915,   916,   917,   923,
     939,   945,   931,   937,   932,   933,   934,   935,   936,   938,
     941,   940,   942,   943,   944,   949,   950,   951,   952,   953,
     954,   958,   959,   960,   961,   965,   966,   970,   972,   971,
     976,   977,   978,   990,   984,   985,   982,   983,   987,   986,
     988,   989,  1001,  1005,  1006,  1010,  1014,  1011,  1008,  1007,
    1009,  1012,  1013,  1018,  1019,  1015,  1016,  1017,  1023,  1024,
    1025,  1026,  1027,  1028,  1029,  1030,  1031,  1032,  1034,  1033,
    1035,  1036,  1037,  1045,  1046,  1043,  1044,  1050,  1051,  1052,
    1053,  1054,  1055,  1056,  1057,  1058,  1059,  1066,  1060,  1062,
    1063,  1064,  1065,  1067,  1061,  1068,  1069,  1070,  1071,  1072,
    1073,  1074,  1075,  1076,  1077,  1078,  1079,  1089,  1090,  1091,
    1092,  1093,  1094,  1095,  1099,  1100,  1101,  1102,  1110,  1111,
    1112,  1118,  1122,  1129,  1128,  1133,  1134,  1135,  1136,  1137,
    1138,  1139,  1152,  1153,  1154,  1155,  1179,  1180,  1181,  1182,
    1186,  1187,  1188,  1189,  1190,  1191,  1192,  1193,  1194,  1106,
    1198,  1200,  1205,  1202,  1201,  1199,  1203,  1204,  1206,   996,
     997,   994,   995,  1217,  1216,  1263,  1255,  1259,  1261,  1262,
    1257,  1256,  1258,  1260,   647,   651,     3,   787,   787,   787,
     787,   787,   787,   787,   787,   787,   787,   787,   787,   787,
     787,   787,   787,   787,   787,   816,   824,   824,   824,   830,
     830,   830,   830,   830,   830,   830,   830,   830,   830,   830,
     830,   830,   830,   830,   830,   830,   830,   830,   830,   871,
     871,   871,   871,   871,   879,   879,   879,   879,   888,   888,
     888,   894,   894,   894,   902,   902,   907,   911,   911,   911,
     911,   911,   911,   922,   930,   930,   930,   930,   930,   930,
     930,   930,   930,   930,   930,   930,   930,   930,   930,   948,
     948,   948,   948,   948,   948,   957,   957,   957,   957,   964,
     964,   969,   969,   969,   975,   975,   975,   981,   981,   981,
     981,   981,   981,   981,   981,   981,  1000,  1004,  1004,  1004,
    1004,  1004,  1004,  1004,  1004,  1004,  1004,  1004,  1004,  1004,
    1004,  1004,  1022,  1022,  1022,  1022,  1022,  1022,  1022,  1022,
    1022,  1022,  1022,  1022,  1022,  1022,  1022,  1042,  1042,  1042,
    1042,  1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,
    1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,
    1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,  1049,
    1049,  1088,  1088,  1088,  1088,  1088,  1088,  1088,  1098,  1098,
    1098,  1098,  1109,  1109,  1109,  1117,  1121,  1127,  1127,  1132,
    1132,  1132,  1132,  1132,  1132,  1132,  1142,  1142,  1142,  1142,
    1178,  1178,  1178,  1178,  1185,  1185,  1185,  1185,  1185,  1185,
    1185,  1185,  1185,  1105,  1197,  1197,  1197,  1197,  1197,  1197,
    1197,  1197,  1197,   993,   993,   993,   993,  1215,  1215,  1254,
    1254,  1254,  1254,  1254,  1254,  1254,  1254,  1254,   656,   656,
     727,   648,   652,     4,     4,   103,     4,   649,   478,   726,
     645,   474,   714,   477,   653,   656,   654,     4,   655,   645
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int16 yyr1[] =
{
       0,   480,   482,   483,   481,   484,   485,   486,   484,   484,
     487,   488,   484,   489,   490,   484,   491,   492,   484,   493,
     494,   484,   484,   484,   484,   484,   495,   496,   484,   497,
     498,   484,   484,   500,   499,   499,   502,   501,   501,   504,
     503,   505,   503,   506,   503,   507,   503,   503,   508,   503,
     509,   503,   510,   503,   511,   503,   512,   503,   503,   513,
     503,   514,   503,   503,   515,   503,   516,   503,   517,   503,
     518,   503,   519,   503,   503,   520,   503,   503,   522,   521,
     523,   521,   524,   521,   525,   521,   526,   521,   527,   521,
     528,   521,   529,   521,   530,   521,   531,   521,   532,   521,
     533,   521,   534,   521,   521,   536,   535,   535,   538,   537,
     539,   537,   537,   541,   540,   542,   540,   543,   540,   544,
     540,   545,   540,   546,   540,   547,   540,   540,   549,   548,
     550,   548,   551,   548,   552,   548,   553,   548,   554,   548,
     555,   548,   556,   548,   557,   548,   558,   548,   559,   548,
     560,   548,   561,   548,   562,   548,   563,   548,   548,   565,
     564,   566,   564,   567,   564,   568,   564,   569,   564,   570,
     564,   571,   564,   572,   564,   573,   564,   574,   564,   575,
     564,   564,   577,   576,   578,   576,   579,   576,   580,   576,
     581,   576,   582,   576,   583,   584,   576,   585,   586,   576,
     587,   588,   576,   589,   576,   576,   591,   590,   592,   590,
     593,   590,   594,   590,   595,   590,   596,   590,   597,   590,
     598,   590,   599,   590,   600,   590,   601,   590,   602,   590,
     603,   590,   604,   590,   605,   590,   606,   590,   607,   590,
     608,   590,   609,   610,   590,   590,   590,   612,   613,   611,
     611,   615,   614,   616,   614,   617,   614,   618,   614,   614,
     620,   621,   619,   619,   623,   622,   624,   622,   622,   626,
     625,   627,   625,   628,   625,   629,   625,   630,   625,   631,
     625,   632,   625,   633,   625,   634,   625,   625,   636,   637,
     635,   635,   639,   638,   640,   638,   641,   638,   642,   638,
     643,   644,   638,   638,   646,   647,   648,   649,   645,   650,
     651,   652,   653,   654,   655,   645,   645,   657,   656,   658,
     656,   659,   656,   660,   656,   661,   656,   662,   656,   663,
     656,   664,   656,   656,   666,   667,   665,   668,   669,   665,
     670,   671,   665,   672,   665,   665,   674,   673,   673,   676,
     677,   675,   675,   679,   678,   678,   681,   682,   680,   680,
     684,   683,   685,   683,   686,   683,   687,   683,   688,   683,
     689,   683,   690,   683,   691,   683,   692,   683,   683,   693,
     683,   694,   683,   683,   695,   683,   696,   683,   697,   683,
     698,   683,   699,   683,   700,   683,   701,   683,   702,   683,
     703,   683,   704,   683,   705,   683,   706,   683,   707,   683,
     708,   683,   709,   683,   710,   683,   683,   712,   711,   711,
     713,   713,   713,   713,   713,   715,   716,   714,   717,   718,
     714,   719,   720,   714,   721,   722,   714,   723,   724,   714,
     725,   726,   714,   714,   727,   727,   727,   728,   730,   731,
     729,   729,   733,   732,   734,   735,   732,   736,   737,   732,
     732,   739,   740,   738,   738,   742,   741,   743,   741,   744,
     741,   745,   746,   741,   747,   748,   741,   741,   750,   749,
     751,   749,   752,   749,   753,   749,   754,   749,   755,   749,
     756,   749,   749,   758,   757,   759,   757,   760,   757,   761,
     757,   762,   757,   763,   757,   764,   757,   757,   766,   765,
     767,   765,   768,   765,   769,   765,   770,   765,   771,   765,
     772,   765,   773,   765,   774,   765,   765,   776,   775,   777,
     775,   778,   775,   779,   775,   780,   775,   781,   775,   782,
     775,   775,   783,   783,   784,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   784,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   784,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   784,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   784,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   784,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   784,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   784,   784,   784,   786,   785,   788,
     787,   789,   787,   790,   787,   791,   787,   792,   787,   793,
     787,   794,   787,   795,   787,   796,   787,   797,   787,   798,
     787,   799,   787,   800,   787,   801,   787,   802,   787,   803,
     787,   804,   787,   805,   787,   787,   807,   806,   809,   808,
     811,   810,   813,   812,   815,   814,   817,   816,   816,   819,
     818,   821,   820,   823,   822,   825,   824,   826,   824,   827,
     824,   824,   829,   828,   831,   830,   832,   830,   833,   830,
     834,   830,   835,   830,   836,   830,   837,   830,   838,   830,
     839,   830,   840,   830,   841,   830,   842,   830,   843,   830,
     844,   830,   845,   830,   846,   830,   847,   830,   848,   830,
     849,   830,   850,   830,   830,   852,   851,   854,   853,   856,
     855,   858,   857,   860,   859,   862,   861,   864,   863,   866,
     865,   868,   867,   870,   869,   872,   871,   873,   871,   874,
     871,   875,   871,   876,   871,   871,   878,   877,   880,   879,
     881,   879,   882,   879,   883,   879,   879,   885,   884,   887,
     886,   889,   888,   890,   888,   891,   888,   888,   893,   892,
     895,   894,   896,   894,   897,   894,   894,   899,   898,   901,
     900,   903,   902,   904,   902,   902,   906,   905,   908,   907,
     907,   910,   909,   912,   911,   913,   911,   914,   911,   915,
     911,   916,   911,   917,   911,   911,   919,   918,   921,   920,
     923,   922,   922,   925,   924,   927,   926,   929,   928,   931,
     930,   932,   930,   933,   930,   934,   930,   935,   930,   936,
     930,   937,   930,   938,   930,   939,   930,   940,   930,   941,
     930,   942,   930,   943,   930,   944,   930,   945,   930,   930,
     947,   946,   949,   948,   950,   948,   951,   948,   952,   948,
     953,   948,   954,   948,   948,   956,   955,   958,   957,   959,
     957,   960,   957,   961,   957,   957,   963,   962,   965,   964,
     966,   964,   964,   968,   967,   970,   969,   971,   969,   972,
     969,   969,   974,   973,   976,   975,   977,   975,   978,   975,
     975,   980,   979,   982,   981,   983,   981,   984,   981,   985,
     981,   986,   981,   987,   981,   988,   981,   989,   981,   990,
     981,   981,   992,   991,   994,   993,   995,   993,   996,   993,
     997,   993,   993,   999,   998,  1001,  1000,  1000,  1003,  1002,
    1005,  1004,  1006,  1004,  1007,  1004,  1008,  1004,  1009,  1004,
    1010,  1004,  1011,  1004,  1012,  1004,  1013,  1004,  1014,  1004,
    1015,  1004,  1016,  1004,  1017,  1004,  1018,  1004,  1019,  1004,
    1004,  1021,  1020,  1023,  1022,  1024,  1022,  1025,  1022,  1026,
    1022,  1027,  1022,  1028,  1022,  1029,  1022,  1030,  1022,  1031,
    1022,  1032,  1022,  1033,  1022,  1034,  1022,  1035,  1022,  1036,
    1022,  1037,  1022,  1022,  1039,  1038,  1041,  1040,  1043,  1042,
    1044,  1042,  1045,  1042,  1046,  1042,  1042,  1048,  1047,  1050,
    1049,  1051,  1049,  1052,  1049,  1053,  1049,  1054,  1049,  1055,
    1049,  1056,  1049,  1057,  1049,  1058,  1049,  1059,  1049,  1060,
    1049,  1061,  1049,  1062,  1049,  1063,  1049,  1064,  1049,  1065,
    1049,  1066,  1049,  1067,  1049,  1068,  1049,  1069,  1049,  1070,
    1049,  1071,  1049,  1072,  1049,  1073,  1049,  1074,  1049,  1075,
    1049,  1076,  1049,  1077,  1049,  1078,  1049,  1079,  1049,  1049,
    1081,  1080,  1083,  1082,  1085,  1084,  1087,  1086,  1089,  1088,
    1090,  1088,  1091,  1088,  1092,  1088,  1093,  1088,  1094,  1088,
    1095,  1088,  1088,  1097,  1096,  1099,  1098,  1100,  1098,  1101,
    1098,  1102,  1098,  1098,  1104,  1103,  1106,  1105,  1105,  1108,
    1107,  1110,  1109,  1111,  1109,  1112,  1109,  1109,  1114,  1113,
    1116,  1115,  1118,  1117,  1117,  1120,  1119,  1122,  1121,  1121,
    1124,  1123,  1126,  1125,  1128,  1127,  1129,  1127,  1127,  1131,
    1130,  1133,  1132,  1134,  1132,  1135,  1132,  1136,  1132,  1137,
    1132,  1138,  1132,  1139,  1132,  1132,  1141,  1140,  1143,  1142,
    1144,  1142,  1145,  1142,  1146,  1142,  1147,  1142,  1148,  1142,
    1149,  1142,  1150,  1142,  1151,  1142,  1152,  1142,  1153,  1142,
    1154,  1142,  1155,  1142,  1142,  1157,  1156,  1159,  1158,  1160,
    1158,  1161,  1158,  1162,  1158,  1163,  1158,  1164,  1158,  1165,
    1158,  1166,  1158,  1167,  1158,  1168,  1158,  1169,  1158,  1170,
    1158,  1171,  1158,  1172,  1158,  1173,  1158,  1158,  1175,  1174,
    1177,  1176,  1179,  1178,  1180,  1178,  1181,  1178,  1182,  1178,
    1178,  1184,  1183,  1186,  1185,  1187,  1185,  1188,  1185,  1189,
    1185,  1190,  1185,  1191,  1185,  1192,  1185,  1193,  1185,  1194,
    1185,  1185,  1196,  1195,  1198,  1197,  1199,  1197,  1200,  1197,
    1201,  1197,  1202,  1197,  1203,  1197,  1204,  1197,  1205,  1197,
    1206,  1197,  1197,  1208,  1207,  1210,  1209,  1212,  1211,  1214,
    1213,  1216,  1215,  1217,  1215,  1215,  1219,  1218,  1221,  1222,
    1220,  1220,  1224,  1223,  1225,  1223,  1226,  1223,  1227,  1223,
    1228,  1223,  1229,  1223,  1230,  1223,  1231,  1223,  1232,  1223,
    1233,  1223,  1234,  1223,  1235,  1223,  1236,  1223,  1237,  1223,
    1238,  1223,  1239,  1223,  1240,  1223,  1241,  1223,  1242,  1243,
    1223,  1223,  1245,  1244,  1246,  1244,  1247,  1244,  1244,  1249,
    1248,  1251,  1250,  1253,  1252,  1255,  1254,  1256,  1254,  1257,
    1254,  1258,  1254,  1259,  1254,  1260,  1254,  1261,  1254,  1262,
    1254,  1263,  1254,  1254
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     0,    10,     5,     0,     0,     7,     5,
       0,     0,     7,     0,     0,     7,     0,     0,     7,     0,
       0,     7,     5,     5,     5,     5,     0,     0,     7,     0,
       0,     7,     0,     0,     4,     0,     0,     6,     0,     0,
       6,     0,     6,     0,     6,     0,     6,     5,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     5,     0,
       6,     0,     6,     5,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     5,     0,     6,     0,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,    10,     0,     0,     6,
       0,     6,     0,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,     7,     0,     0,     7,
       0,     0,     7,     0,     6,     0,     0,     8,     0,     8,
       0,     8,     0,     8,     0,     8,     0,     8,     0,     8,
       0,     8,     0,     8,     0,     8,     0,     8,     0,     8,
       0,     8,     0,     8,     0,     6,     0,     8,     0,     8,
       0,     8,     0,     0,     7,     5,     0,     0,     0,     7,
       0,     0,     8,     0,     8,     0,     6,     0,     6,     0,
       0,     0,     7,     0,     0,     6,     0,     6,     0,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     0,     0,
       7,     0,     0,     6,     0,     6,     0,     8,     0,     6,
       0,     0,     7,     0,     0,     0,     0,     0,    13,     0,
       0,     0,     0,     0,     0,    20,     0,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     0,     0,     7,     0,     0,     7,
       0,     0,     7,     0,     6,     0,     0,     4,     0,     0,
       0,     7,     0,     0,     6,     0,     0,     0,     7,     0,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     5,     0,
       6,     0,     6,     5,     0,     8,     0,     8,     0,     8,
       0,     8,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     0,     6,     0,
       1,     1,     1,     1,     1,     0,     0,     7,     0,     0,
       7,     0,     0,     7,     0,     0,     7,     0,     0,     8,
       0,     0,    14,     0,     2,     5,     0,     0,     0,     0,
       7,     0,     0,     6,     0,     0,     7,     0,     0,     7,
       0,     0,     0,     7,     0,     0,     6,     0,     6,     0,
       8,     0,     0,     7,     0,     0,     7,     0,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     5,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     5,     0,     6,     0,     0,
       4,     0,     4,     0,     5,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     5,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     0,     5,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     0,     4,     0,
       5,     0,     6,     0,     6,     0,     6,     0,     0,     5,
       0,     6,     0,     6,     0,     6,     0,     0,     4,     0,
       5,     0,     6,     0,     6,     0,     0,     5,     0,     6,
       0,     0,     5,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     0,     4,     0,     5,
       0,     6,     0,     0,     4,     0,     4,     0,     5,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       0,     5,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,     5,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     0,     5,     0,     6,
       0,     6,     0,     0,     5,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     6,     0,     6,
       0,     0,     5,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     5,     0,     6,     0,     0,     5,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     0,     5,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     0,     4,     0,     5,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     0,     5,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       0,     4,     0,     4,     0,     4,     0,     5,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     5,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     0,     5,     0,     6,     0,     0,
       5,     0,     6,     0,     6,     0,     6,     0,     0,     4,
       0,     5,     0,     6,     0,     0,     5,     0,     6,     0,
       0,     4,     0,     5,     0,     6,     0,     6,     0,     0,
       5,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     0,     5,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,     5,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     0,     0,     4,
       0,     5,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     0,     5,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     4,     0,     4,     0,     4,     0,
       5,     0,     6,     0,     6,     0,     0,     4,     0,     0,
       7,     0,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     0,
       7,     0,     0,     6,     0,     6,     0,     6,     0,     0,
       4,     0,     4,     0,     5,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* $@1: %empty  */
#line 343 "configparser.yy"
  { lconfig_parseinit();}
#line 5929 "configparser.cc"
    break;

  case 3: /* $@2: %empty  */
#line 343 "configparser.yy"
                                                                        { lconfig->setLoadedVersion( (yyvsp[-5].num), (yyvsp[-3].num), (yyvsp[-1].num) ); }
#line 5935 "configparser.cc"
    break;

  case 6: /* $@3: %empty  */
#line 347 "configparser.yy"
                               { lconfig_faces_list.clear(); }
#line 5941 "configparser.cc"
    break;

  case 7: /* $@4: %empty  */
#line 348 "configparser.yy"
      {
        lconfig->applyNewFacesFromList( lconfig_faces_list );
      }
#line 5949 "configparser.cc"
    break;

  case 10: /* $@5: %empty  */
#line 352 "configparser.yy"
                              { lconfig_listp = new List(); }
#line 5955 "configparser.cc"
    break;

  case 11: /* $@6: %empty  */
#line 353 "configparser.yy"
                           { lconfig->setPaths( lconfig_listp );
                             lconfig_id = lconfig_listp->initEnum();
                             lconfig_p1 = (WCPath*)lconfig_listp->getFirstElement( lconfig_id );
                             while ( lconfig_p1 != NULL ) {
                               delete lconfig_p1;
                               lconfig_p1 = (WCPath*)lconfig_listp->getNextElement( lconfig_id );
                             }
                             lconfig_listp->closeEnum( lconfig_id );
                             delete lconfig_listp;
                             lconfig_listp = NULL;
                           }
#line 5971 "configparser.cc"
    break;

  case 13: /* $@7: %empty  */
#line 364 "configparser.yy"
                                  { lconfig_listft = new List(); }
#line 5977 "configparser.cc"
    break;

  case 14: /* $@8: %empty  */
#line 365 "configparser.yy"
                               { lconfig->setFiletypes( lconfig_listft );
                                 lconfig_id = lconfig_listft->initEnum();
                                 lconfig_ft1 = (WCFiletype*)lconfig_listft->getFirstElement( lconfig_id );
                                 while ( lconfig_ft1 != NULL ) {
                                   delete lconfig_ft1;
                                   lconfig_ft1 = (WCFiletype*)lconfig_listft->getNextElement( lconfig_id );
                                 }
                                 lconfig_listft->closeEnum( lconfig_id );
                                 delete lconfig_listft;
                                 lconfig_listft = NULL;
                               }
#line 5993 "configparser.cc"
    break;

  case 16: /* $@9: %empty  */
#line 376 "configparser.yy"
                                { lconfig_listh = new List(); }
#line 5999 "configparser.cc"
    break;

  case 17: /* $@10: %empty  */
#line 377 "configparser.yy"
                             { lconfig->setHotkeys( lconfig_listh );
                               lconfig_id = lconfig_listh->initEnum();
                               lconfig_hk1 = (WCHotkey*)lconfig_listh->getFirstElement( lconfig_id );
                               while ( lconfig_hk1 != NULL ) {
                                 delete lconfig_hk1;
                                 lconfig_hk1 = (WCHotkey*)lconfig_listh->getNextElement( lconfig_id );
                               }
                               lconfig_listh->closeEnum( lconfig_id );
                               delete lconfig_listh;
                               lconfig_listh = NULL;
                             }
#line 6015 "configparser.cc"
    break;

  case 19: /* $@11: %empty  */
#line 388 "configparser.yy"
                                { lconfig_listb = new List(); }
#line 6021 "configparser.cc"
    break;

  case 20: /* $@12: %empty  */
#line 389 "configparser.yy"
                             { lconfig->setButtons( lconfig_listb );
                               lconfig_id = lconfig_listb->initEnum();
                               lconfig_bt1 = (WCButton*)lconfig_listb->getFirstElement( lconfig_id );
                               while ( lconfig_bt1 != NULL ) {
                                 delete lconfig_bt1;
                                 lconfig_bt1 = (WCButton*)lconfig_listb->getNextElement( lconfig_id );
                               }
                               lconfig_listb->closeEnum( lconfig_id );
                               delete lconfig_listb;
                               lconfig_listb = NULL;
                             }
#line 6037 "configparser.cc"
    break;

  case 26: /* $@13: %empty  */
#line 404 "configparser.yy"
                                          { lconfig_pathjump_allow.clear(); }
#line 6043 "configparser.cc"
    break;

  case 27: /* $@14: %empty  */
#line 405 "configparser.yy"
                                 { lconfig->setPathJumpAllowDirs( lconfig_pathjump_allow );
                                   lconfig_pathjump_allow.clear();
                                 }
#line 6051 "configparser.cc"
    break;

  case 29: /* $@15: %empty  */
#line 408 "configparser.yy"
                                         { dir_presets.clear(); }
#line 6057 "configparser.cc"
    break;

  case 30: /* $@16: %empty  */
#line 409 "configparser.yy"
                                      { lconfig->setDirectoryPresets( dir_presets );
                                 }
#line 6064 "configparser.cc"
    break;

  case 33: /* $@17: %empty  */
#line 413 "configparser.yy"
                          { lconfig_pathjump_allow.push_back( (yyvsp[-1].strptr) ); }
#line 6070 "configparser.cc"
    break;

  case 36: /* $@18: %empty  */
#line 416 "configparser.yy"
                                             { lconfig->setPathJumpStoreFilesAlways( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6076 "configparser.cc"
    break;

  case 39: /* $@19: %empty  */
#line 419 "configparser.yy"
                             { lconfig->setLang( (yyvsp[-1].strptr) ); }
#line 6082 "configparser.cc"
    break;

  case 41: /* $@20: %empty  */
#line 420 "configparser.yy"
                         { lconfig->setRows( (yyvsp[-1].num) ); }
#line 6088 "configparser.cc"
    break;

  case 43: /* $@21: %empty  */
#line 421 "configparser.yy"
                            { lconfig->setColumns( (yyvsp[-1].num) ); }
#line 6094 "configparser.cc"
    break;

  case 45: /* $@22: %empty  */
#line 422 "configparser.yy"
                              { lconfig->setCacheSize( (yyvsp[-1].num) ); }
#line 6100 "configparser.cc"
    break;

  case 48: /* $@23: %empty  */
#line 424 "configparser.yy"
                                   { lconfig->setOwnerstringtype( 0 ); }
#line 6106 "configparser.cc"
    break;

  case 50: /* $@24: %empty  */
#line 425 "configparser.yy"
                                  { lconfig->setOwnerstringtype( 1 ); }
#line 6112 "configparser.cc"
    break;

  case 52: /* $@25: %empty  */
#line 426 "configparser.yy"
                                { lconfig->setTerminalBin( (yyvsp[-1].strptr) ); }
#line 6118 "configparser.cc"
    break;

  case 54: /* $@26: %empty  */
#line 427 "configparser.yy"
                                         { lconfig->setTerminalReturnsEarly( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6124 "configparser.cc"
    break;

  case 56: /* $@27: %empty  */
#line 428 "configparser.yy"
                                     { lconfig->setShowStringForDirSize( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6130 "configparser.cc"
    break;

  case 59: /* $@28: %empty  */
#line 431 "configparser.yy"
                               { lconfig->resetColors(); }
#line 6136 "configparser.cc"
    break;

  case 61: /* $@29: %empty  */
#line 432 "configparser.yy"
                              { lconfig->clearLayoutOrders(); }
#line 6142 "configparser.cc"
    break;

  case 64: /* $@30: %empty  */
#line 434 "configparser.yy"
                                            { lconfig->setSaveWorkerStateOnExit( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6148 "configparser.cc"
    break;

  case 66: /* $@31: %empty  */
#line 435 "configparser.yy"
                                             { lconfig->setUseStringCompareMode( ( ( (yyvsp[-1].num) ) == 1 ) ? StringComparator::STRING_COMPARE_VERSION : StringComparator::STRING_COMPARE_REGULAR ); }
#line 6154 "configparser.cc"
    break;

  case 68: /* $@32: %empty  */
#line 436 "configparser.yy"
                                                    { lconfig->setUseStringCompareMode( StringComparator::STRING_COMPARE_NOCASE ); }
#line 6160 "configparser.cc"
    break;

  case 70: /* $@33: %empty  */
#line 437 "configparser.yy"
                                           { lconfig->setApplyWindowDialogType( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6166 "configparser.cc"
    break;

  case 72: /* $@34: %empty  */
#line 438 "configparser.yy"
                                     { lconfig->setUseExtendedRegEx( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6172 "configparser.cc"
    break;

  case 75: /* $@35: %empty  */
#line 440 "configparser.yy"
                                             { lconfig->setDisableBGCheckPrefix( (yyvsp[-1].strptr) ); }
#line 6178 "configparser.cc"
    break;

  case 78: /* $@36: %empty  */
#line 443 "configparser.yy"
                       { lconfig->layoutAddEntry( LayoutSettings::LO_STATEBAR ); }
#line 6184 "configparser.cc"
    break;

  case 80: /* $@37: %empty  */
#line 444 "configparser.yy"
                       { lconfig->layoutAddEntry( LayoutSettings::LO_CLOCKBAR ); }
#line 6190 "configparser.cc"
    break;

  case 82: /* $@38: %empty  */
#line 445 "configparser.yy"
                      { lconfig->layoutAddEntry( LayoutSettings::LO_BUTTONS ); }
#line 6196 "configparser.cc"
    break;

  case 84: /* $@39: %empty  */
#line 446 "configparser.yy"
                        { lconfig->layoutAddEntry( LayoutSettings::LO_LISTVIEWS ); }
#line 6202 "configparser.cc"
    break;

  case 86: /* $@40: %empty  */
#line 447 "configparser.yy"
                  { lconfig->layoutAddEntry( LayoutSettings::LO_BLL ); }
#line 6208 "configparser.cc"
    break;

  case 88: /* $@41: %empty  */
#line 448 "configparser.yy"
                  { lconfig->layoutAddEntry( LayoutSettings::LO_LBL ); }
#line 6214 "configparser.cc"
    break;

  case 90: /* $@42: %empty  */
#line 449 "configparser.yy"
                  { lconfig->layoutAddEntry( LayoutSettings::LO_LLB ); }
#line 6220 "configparser.cc"
    break;

  case 92: /* $@43: %empty  */
#line 450 "configparser.yy"
                 { lconfig->layoutAddEntry( LayoutSettings::LO_BL ); }
#line 6226 "configparser.cc"
    break;

  case 94: /* $@44: %empty  */
#line 451 "configparser.yy"
                 { lconfig->layoutAddEntry( LayoutSettings::LO_LB ); }
#line 6232 "configparser.cc"
    break;

  case 96: /* $@45: %empty  */
#line 452 "configparser.yy"
                                { lconfig->setLayoutButtonVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6238 "configparser.cc"
    break;

  case 98: /* $@46: %empty  */
#line 453 "configparser.yy"
                                  { lconfig->setLayoutListviewVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6244 "configparser.cc"
    break;

  case 100: /* $@47: %empty  */
#line 454 "configparser.yy"
                                      { lconfig->setLayoutListViewWeight( (yyvsp[-1].num) ); }
#line 6250 "configparser.cc"
    break;

  case 102: /* $@48: %empty  */
#line 455 "configparser.yy"
                                   { lconfig->setLayoutWeightRelToActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6256 "configparser.cc"
    break;

  case 105: /* $@49: %empty  */
#line 458 "configparser.yy"
                                            { lconfig->setColor( (yyvsp[-7].num), (yyvsp[-5].num), (yyvsp[-3].num), (yyvsp[-1].num) ); }
#line 6262 "configparser.cc"
    break;

  case 108: /* $@50: %empty  */
#line 461 "configparser.yy"
                   { lconfig_side = 0; }
#line 6268 "configparser.cc"
    break;

  case 110: /* $@51: %empty  */
#line 462 "configparser.yy"
                    { lconfig_side = 1; }
#line 6274 "configparser.cc"
    break;

  case 113: /* $@52: %empty  */
#line 465 "configparser.yy"
                                 { lconfig->setHBarTop( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6280 "configparser.cc"
    break;

  case 115: /* $@53: %empty  */
#line 466 "configparser.yy"
                                       { lconfig->setHBarHeight( lconfig_side, (yyvsp[-1].num) ); }
#line 6286 "configparser.cc"
    break;

  case 117: /* $@54: %empty  */
#line 467 "configparser.yy"
                                  { lconfig->setVBarLeft( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6292 "configparser.cc"
    break;

  case 119: /* $@55: %empty  */
#line 468 "configparser.yy"
                                      { lconfig->setVBarWidth( lconfig_side, (yyvsp[-1].num) ); }
#line 6298 "configparser.cc"
    break;

  case 121: /* $@56: %empty  */
#line 469 "configparser.yy"
                                          { lconfig->clearVisCols( lconfig_side ); }
#line 6304 "configparser.cc"
    break;

  case 123: /* $@57: %empty  */
#line 470 "configparser.yy"
                                    { lconfig->setShowHeader( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6310 "configparser.cc"
    break;

  case 125: /* $@58: %empty  */
#line 471 "configparser.yy"
                                        { lconfig->setPathEntryOnTop( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6316 "configparser.cc"
    break;

  case 128: /* $@59: %empty  */
#line 474 "configparser.yy"
                       { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_NAME ); }
#line 6322 "configparser.cc"
    break;

  case 130: /* $@60: %empty  */
#line 475 "configparser.yy"
                       { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_SIZE ); }
#line 6328 "configparser.cc"
    break;

  case 132: /* $@61: %empty  */
#line 476 "configparser.yy"
                       { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_TYPE ); }
#line 6334 "configparser.cc"
    break;

  case 134: /* $@62: %empty  */
#line 477 "configparser.yy"
                             { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_PERM ); }
#line 6340 "configparser.cc"
    break;

  case 136: /* $@63: %empty  */
#line 478 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_OWNER ); }
#line 6346 "configparser.cc"
    break;

  case 138: /* $@64: %empty  */
#line 479 "configparser.yy"
                              { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_DEST ); }
#line 6352 "configparser.cc"
    break;

  case 140: /* $@65: %empty  */
#line 480 "configparser.yy"
                          { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_MOD ); }
#line 6358 "configparser.cc"
    break;

  case 142: /* $@66: %empty  */
#line 481 "configparser.yy"
                          { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_ACC ); }
#line 6364 "configparser.cc"
    break;

  case 144: /* $@67: %empty  */
#line 482 "configparser.yy"
                          { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_CHANGE ); }
#line 6370 "configparser.cc"
    break;

  case 146: /* $@68: %empty  */
#line 483 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_INODE ); }
#line 6376 "configparser.cc"
    break;

  case 148: /* $@69: %empty  */
#line 484 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_NLINK ); }
#line 6382 "configparser.cc"
    break;

  case 150: /* $@70: %empty  */
#line 485 "configparser.yy"
                         { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_BLOCKS ); }
#line 6388 "configparser.cc"
    break;

  case 152: /* $@71: %empty  */
#line 486 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_SIZEH ); }
#line 6394 "configparser.cc"
    break;

  case 154: /* $@72: %empty  */
#line 487 "configparser.yy"
                            { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_EXTENSION ); }
#line 6400 "configparser.cc"
    break;

  case 156: /* $@73: %empty  */
#line 488 "configparser.yy"
                                  { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_CUSTOM_ATTR ); }
#line 6406 "configparser.cc"
    break;

  case 159: /* $@74: %empty  */
#line 491 "configparser.yy"
                                  { lconfig->setDateFormat( 0 ); }
#line 6412 "configparser.cc"
    break;

  case 161: /* $@75: %empty  */
#line 492 "configparser.yy"
                                  { lconfig->setDateFormat( 1 ); }
#line 6418 "configparser.cc"
    break;

  case 163: /* $@76: %empty  */
#line 493 "configparser.yy"
                                  { lconfig->setDateFormat( 2 ); }
#line 6424 "configparser.cc"
    break;

  case 165: /* $@77: %empty  */
#line 494 "configparser.yy"
                                     { lconfig->setDateFormat( -1 ); }
#line 6430 "configparser.cc"
    break;

  case 167: /* $@78: %empty  */
#line 495 "configparser.yy"
                                        { lconfig->setDateFormatString( (yyvsp[-1].strptr) ); }
#line 6436 "configparser.cc"
    break;

  case 169: /* $@79: %empty  */
#line 496 "configparser.yy"
                                        { lconfig->setDateSubst( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6442 "configparser.cc"
    break;

  case 171: /* $@80: %empty  */
#line 497 "configparser.yy"
                                  { lconfig->setTimeFormat( 0 ); }
#line 6448 "configparser.cc"
    break;

  case 173: /* $@81: %empty  */
#line 498 "configparser.yy"
                                  { lconfig->setTimeFormat( 1 ); }
#line 6454 "configparser.cc"
    break;

  case 175: /* $@82: %empty  */
#line 499 "configparser.yy"
                                     { lconfig->setTimeFormat( -1 ); }
#line 6460 "configparser.cc"
    break;

  case 177: /* $@83: %empty  */
#line 500 "configparser.yy"
                                        { lconfig->setTimeFormatString( (yyvsp[-1].strptr) ); }
#line 6466 "configparser.cc"
    break;

  case 179: /* $@84: %empty  */
#line 501 "configparser.yy"
                                      { lconfig->setDateBeforeTime( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6472 "configparser.cc"
    break;

  case 182: /* $@85: %empty  */
#line 504 "configparser.yy"
                                        { lconfig->setMouseSelectButton( (yyvsp[-1].num) ); }
#line 6478 "configparser.cc"
    break;

  case 184: /* $@86: %empty  */
#line 505 "configparser.yy"
                                          { lconfig->setMouseActivateButton( (yyvsp[-1].num) ); }
#line 6484 "configparser.cc"
    break;

  case 186: /* $@87: %empty  */
#line 506 "configparser.yy"
                                        { lconfig->setMouseScrollButton( (yyvsp[-1].num) ); }
#line 6490 "configparser.cc"
    break;

  case 188: /* $@88: %empty  */
#line 507 "configparser.yy"
                                           { lconfig->setMouseSelectMethod( WConfig::MOUSECONF_NORMAL_MODE ); }
#line 6496 "configparser.cc"
    break;

  case 190: /* $@89: %empty  */
#line 508 "configparser.yy"
                                                { lconfig->setMouseSelectMethod( WConfig::MOUSECONF_ALT_MODE ); }
#line 6502 "configparser.cc"
    break;

  case 192: /* $@90: %empty  */
#line 509 "configparser.yy"
                                         { lconfig->setMouseContextButton( (yyvsp[-1].num) ); }
#line 6508 "configparser.cc"
    break;

  case 194: /* $@91: %empty  */
#line 510 "configparser.yy"
                                        { lconfig_keymod = 0; }
#line 6514 "configparser.cc"
    break;

  case 195: /* $@92: %empty  */
#line 511 "configparser.yy"
                              { lconfig->setMouseActivateMod( lconfig_keymod ); }
#line 6520 "configparser.cc"
    break;

  case 197: /* $@93: %empty  */
#line 512 "configparser.yy"
                                      { lconfig_keymod = 0; }
#line 6526 "configparser.cc"
    break;

  case 198: /* $@94: %empty  */
#line 513 "configparser.yy"
                              { lconfig->setMouseScrollMod( lconfig_keymod ); }
#line 6532 "configparser.cc"
    break;

  case 200: /* $@95: %empty  */
#line 514 "configparser.yy"
                                       { lconfig_keymod = 0; }
#line 6538 "configparser.cc"
    break;

  case 201: /* $@96: %empty  */
#line 515 "configparser.yy"
                              { lconfig->setMouseContextMod( lconfig_keymod ); }
#line 6544 "configparser.cc"
    break;

  case 203: /* $@97: %empty  */
#line 516 "configparser.yy"
                                           { lconfig->setMouseContextMenuWithDelayedActivate( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6550 "configparser.cc"
    break;

  case 206: /* $@98: %empty  */
#line 519 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "statusbar-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "statusbar-bg", (yyvsp[-1].num) ) );
        }
#line 6559 "configparser.cc"
    break;

  case 208: /* $@99: %empty  */
#line 523 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "lvb-active-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "lvb-active-bg", (yyvsp[-1].num) ) );
        }
#line 6568 "configparser.cc"
    break;

  case 210: /* $@100: %empty  */
#line 527 "configparser.yy"
                                           {
        lconfig_faces_list.push_back( std::make_pair( "lvb-inactive-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "lvb-inactive-bg", (yyvsp[-1].num) ) );
        }
#line 6577 "configparser.cc"
    break;

  case 212: /* $@101: %empty  */
#line 531 "configparser.yy"
                                       {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-select-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-select-bg", (yyvsp[-1].num) ) );
        }
#line 6586 "configparser.cc"
    break;

  case 214: /* $@102: %empty  */
#line 535 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-normal-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-normal-bg", (yyvsp[-1].num) ) );
        }
#line 6595 "configparser.cc"
    break;

  case 216: /* $@103: %empty  */
#line 539 "configparser.yy"
                                        {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-select-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-select-bg", (yyvsp[-1].num) ) );
        }
#line 6604 "configparser.cc"
    break;

  case 218: /* $@104: %empty  */
#line 543 "configparser.yy"
                                          {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-normal-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-normal-bg", (yyvsp[-1].num) ) );
        }
#line 6613 "configparser.cc"
    break;

  case 220: /* $@105: %empty  */
#line 547 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "clockbar-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "clockbar-bg", (yyvsp[-1].num) ) );
        }
#line 6622 "configparser.cc"
    break;

  case 222: /* $@106: %empty  */
#line 551 "configparser.yy"
                                          {
        lconfig_faces_list.push_back( std::make_pair( "request-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "request-bg", (yyvsp[-1].num) ) );
        }
#line 6631 "configparser.cc"
    break;

  case 224: /* $@107: %empty  */
#line 555 "configparser.yy"
                                          {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-selact-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-selact-bg", (yyvsp[-1].num) ) );
        }
#line 6640 "configparser.cc"
    break;

  case 226: /* $@108: %empty  */
#line 559 "configparser.yy"
                                            {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-active-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-active-bg", (yyvsp[-1].num) ) );
        }
#line 6649 "configparser.cc"
    break;

  case 228: /* $@109: %empty  */
#line 563 "configparser.yy"
                                           {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-selact-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-selact-bg", (yyvsp[-1].num) ) );
        }
#line 6658 "configparser.cc"
    break;

  case 230: /* $@110: %empty  */
#line 567 "configparser.yy"
                                             {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-active-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-active-bg", (yyvsp[-1].num) ) );
        }
#line 6667 "configparser.cc"
    break;

  case 232: /* $@111: %empty  */
#line 571 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "dirview-header-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-header-bg", (yyvsp[-1].num) ) );
        }
#line 6676 "configparser.cc"
    break;

  case 234: /* $@112: %empty  */
#line 575 "configparser.yy"
                           {
        lconfig_faces_list.push_back( std::make_pair( "dirview-bg", (yyvsp[-1].num) ) );
        }
#line 6684 "configparser.cc"
    break;

  case 236: /* $@113: %empty  */
#line 578 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "textview-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-bg", (yyvsp[-1].num) ) );
        }
#line 6693 "configparser.cc"
    break;

  case 238: /* $@114: %empty  */
#line 582 "configparser.yy"
                                                    {
        lconfig_faces_list.push_back( std::make_pair( "textview-highlight-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-highlight-bg", (yyvsp[-1].num) ) );
        }
#line 6702 "configparser.cc"
    break;

  case 240: /* $@115: %empty  */
#line 586 "configparser.yy"
                                                  {
        lconfig_faces_list.push_back( std::make_pair( "textview-selection-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-selection-bg", (yyvsp[-1].num) ) );
        }
#line 6711 "configparser.cc"
    break;

  case 242: /* $@116: %empty  */
#line 590 "configparser.yy"
                                   { lconfig_labelcolors.clear(); }
#line 6717 "configparser.cc"
    break;

  case 243: /* $@117: %empty  */
#line 591 "configparser.yy"
                                   {
          WConfig::ColorDef c = lconfig->getColorDefs();
          c.setLabelColors( lconfig_labelcolors );
          lconfig->setColorDefs( c );
        }
#line 6727 "configparser.cc"
    break;

  case 247: /* $@118: %empty  */
#line 600 "configparser.yy"
                                    {
              lconfig_labelcolor.normal_fg = -1;
              lconfig_labelcolor.normal_bg = -1;
              lconfig_labelcolor.active_fg = -1;
              lconfig_labelcolor.active_bg = -1;
              lconfig_labelcolor_name = "";
              lconfig_labelcolor.only_exact_path = false;
            }
#line 6740 "configparser.cc"
    break;

  case 248: /* $@119: %empty  */
#line 608 "configparser.yy"
                                      {
              lconfig_labelcolors[lconfig_labelcolor_name] = lconfig_labelcolor;
            }
#line 6748 "configparser.cc"
    break;

  case 251: /* $@120: %empty  */
#line 613 "configparser.yy"
                                             {
             lconfig_labelcolor.normal_fg = (yyvsp[-3].num);
             lconfig_labelcolor.normal_bg = (yyvsp[-1].num);
            }
#line 6757 "configparser.cc"
    break;

  case 253: /* $@121: %empty  */
#line 617 "configparser.yy"
                                             {
             lconfig_labelcolor.active_fg = (yyvsp[-3].num);
             lconfig_labelcolor.active_bg = (yyvsp[-1].num);
            }
#line 6766 "configparser.cc"
    break;

  case 255: /* $@122: %empty  */
#line 621 "configparser.yy"
                                      { lconfig_labelcolor_name = (yyvsp[-1].strptr); }
#line 6772 "configparser.cc"
    break;

  case 257: /* $@123: %empty  */
#line 622 "configparser.yy"
                                       { lconfig_labelcolor.only_exact_path = ( ( (yyvsp[-1].num) ) == 1 ) ? true : false; }
#line 6778 "configparser.cc"
    break;

  case 260: /* $@124: %empty  */
#line 625 "configparser.yy"
                             {
              lconfig_face_name.clear();
              lconfig_face_color = -1;
            }
#line 6787 "configparser.cc"
    break;

  case 261: /* $@125: %empty  */
#line 629 "configparser.yy"
                                {
              lconfig_faces_list.push_back( std::make_pair( lconfig_face_name, lconfig_face_color ) );
            }
#line 6795 "configparser.cc"
    break;

  case 264: /* $@126: %empty  */
#line 634 "configparser.yy"
                            {
             lconfig_face_color = (yyvsp[-1].num);
            }
#line 6803 "configparser.cc"
    break;

  case 266: /* $@127: %empty  */
#line 637 "configparser.yy"
                                      { lconfig_face_name = (yyvsp[-1].strptr); }
#line 6809 "configparser.cc"
    break;

  case 269: /* $@128: %empty  */
#line 640 "configparser.yy"
                                 { lconfig->setStartDir( 0, (yyvsp[-1].strptr) ); }
#line 6815 "configparser.cc"
    break;

  case 271: /* $@129: %empty  */
#line 641 "configparser.yy"
                                  { lconfig->setStartDir( 1, (yyvsp[-1].strptr) ); }
#line 6821 "configparser.cc"
    break;

  case 273: /* $@130: %empty  */
#line 642 "configparser.yy"
                                             { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_NEVER ); }
#line 6827 "configparser.cc"
    break;

  case 275: /* $@131: %empty  */
#line 643 "configparser.yy"
                                              { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_ALWAYS ); }
#line 6833 "configparser.cc"
    break;

  case 277: /* $@132: %empty  */
#line 644 "configparser.yy"
                                           { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_ASK ); }
#line 6839 "configparser.cc"
    break;

  case 279: /* $@133: %empty  */
#line 645 "configparser.yy"
                                           { lconfig->setStoreTabsMode( WConfig::STORE_TABS_NEVER ); }
#line 6845 "configparser.cc"
    break;

  case 281: /* $@134: %empty  */
#line 646 "configparser.yy"
                                            { lconfig->setStoreTabsMode( WConfig::STORE_TABS_ALWAYS ); }
#line 6851 "configparser.cc"
    break;

  case 283: /* $@135: %empty  */
#line 647 "configparser.yy"
                                                   { lconfig->setStoreTabsMode( WConfig::STORE_TABS_AS_EXIT_STATE ); }
#line 6857 "configparser.cc"
    break;

  case 285: /* $@136: %empty  */
#line 648 "configparser.yy"
                                         { lconfig->setStoreTabsMode( WConfig::STORE_TABS_ASK ); }
#line 6863 "configparser.cc"
    break;

  case 288: /* $@137: %empty  */
#line 651 "configparser.yy"
                             { lconfig_p1 = new WCPath(); }
#line 6869 "configparser.cc"
    break;

  case 289: /* $@138: %empty  */
#line 652 "configparser.yy"
                          { lconfig_p1->setCheck( true );
                        while ( lconfig_listp->size() < ( lconfig_pos + 1 ) ) lconfig_listp->addElement( new WCPath() );
                        lconfig_p1 = (WCPath*)lconfig_listp->exchangeElement( lconfig_pos, lconfig_p1 );
                        delete lconfig_p1;
                        lconfig_p1 = NULL;
                      }
#line 6880 "configparser.cc"
    break;

  case 292: /* $@139: %empty  */
#line 660 "configparser.yy"
                               { lconfig_pos = (yyvsp[-1].num);
                         if ( lconfig_pos < 0 ) lconfig_pos = 0;
                         if ( lconfig_pos > 10240 ) lconfig_pos = 10240;
                       }
#line 6889 "configparser.cc"
    break;

  case 294: /* $@140: %empty  */
#line 664 "configparser.yy"
                               { lconfig_p1->setName( (yyvsp[-1].strptr) ); }
#line 6895 "configparser.cc"
    break;

  case 296: /* $@141: %empty  */
#line 665 "configparser.yy"
                                      { lconfig_p1->setFG( (yyvsp[-3].num) );
                            lconfig_p1->setBG( (yyvsp[-1].num) );
                          }
#line 6903 "configparser.cc"
    break;

  case 298: /* $@142: %empty  */
#line 668 "configparser.yy"
                              { lconfig_p1->setPath( (yyvsp[-1].strptr) ); }
#line 6909 "configparser.cc"
    break;

  case 300: /* $@143: %empty  */
#line 669 "configparser.yy"
                                 { lconfig_listsk = new List(); }
#line 6915 "configparser.cc"
    break;

  case 301: /* $@144: %empty  */
#line 670 "configparser.yy"
                            { lconfig_p1->setDoubleKeys( lconfig_listsk );
                              lconfig_id = lconfig_listsk->initEnum();
                              lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                              while ( lconfig_dk != NULL ) {
                                delete lconfig_dk;
                                lconfig_listsk->removeFirstElement();
                                lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                              }
                              lconfig_listsk->closeEnum( lconfig_id );
                              delete lconfig_listsk;
                              lconfig_listsk = NULL;
                            }
#line 6932 "configparser.cc"
    break;

  case 304: /* $@145: %empty  */
#line 684 "configparser.yy"
                                 { lconfig_dk = new WCDoubleShortkey();
                                   lconfig_dk->setType( WCDoubleShortkey::WCDS_NORMAL );
                                 }
#line 6940 "configparser.cc"
    break;

  case 305: /* $@146: %empty  */
#line 687 "configparser.yy"
                                { lconfig_nkeysym = AGUIX::getKeySymForString( (yyvsp[-1].strptr) );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 0 );
                          else lconfig_dk->setKeySym( 0, 0 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
#line 6950 "configparser.cc"
    break;

  case 306: /* $@147: %empty  */
#line 692 "configparser.yy"
             { lconfig_dk->setMod( lconfig_keymod, 0 ); }
#line 6956 "configparser.cc"
    break;

  case 307: /* $@148: %empty  */
#line 693 "configparser.yy"
                       { lconfig_listsk->addElement( lconfig_dk ); }
#line 6962 "configparser.cc"
    break;

  case 309: /* $@149: %empty  */
#line 695 "configparser.yy"
                                 { lconfig_dk = new WCDoubleShortkey();
                                   lconfig_dk->setType( WCDoubleShortkey::WCDS_DOUBLE );
                                  }
#line 6970 "configparser.cc"
    break;

  case 310: /* $@150: %empty  */
#line 698 "configparser.yy"
                                { lconfig_nkeysym = AGUIX::getKeySymForString( (yyvsp[-1].strptr) );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 0 );
                          else lconfig_dk->setKeySym( 0, 0 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
#line 6980 "configparser.cc"
    break;

  case 311: /* $@151: %empty  */
#line 703 "configparser.yy"
             { lconfig_dk->setMod( lconfig_keymod, 0 ); /*TODO:finzt das so mit dem lconfig_keymod?*/}
#line 6986 "configparser.cc"
    break;

  case 312: /* $@152: %empty  */
#line 704 "configparser.yy"
                                { lconfig_nkeysym = AGUIX::getKeySymForString( (yyvsp[-1].strptr) );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 1 );
                          else lconfig_dk->setKeySym( 0, 1 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
#line 6996 "configparser.cc"
    break;

  case 313: /* $@153: %empty  */
#line 709 "configparser.yy"
             { lconfig_dk->setMod( lconfig_keymod, 1 ); }
#line 7002 "configparser.cc"
    break;

  case 314: /* $@154: %empty  */
#line 710 "configparser.yy"
                       { lconfig_listsk->addElement( lconfig_dk ); }
#line 7008 "configparser.cc"
    break;

  case 317: /* $@155: %empty  */
#line 714 "configparser.yy"
                              { lconfig_keymod |= ControlMask; }
#line 7014 "configparser.cc"
    break;

  case 319: /* $@156: %empty  */
#line 715 "configparser.yy"
                            { lconfig_keymod |= ShiftMask; }
#line 7020 "configparser.cc"
    break;

  case 321: /* $@157: %empty  */
#line 716 "configparser.yy"
                           { lconfig_keymod |= LockMask; }
#line 7026 "configparser.cc"
    break;

  case 323: /* $@158: %empty  */
#line 717 "configparser.yy"
                           { lconfig_keymod |= Mod1Mask; }
#line 7032 "configparser.cc"
    break;

  case 325: /* $@159: %empty  */
#line 718 "configparser.yy"
                           { lconfig_keymod |= Mod2Mask; }
#line 7038 "configparser.cc"
    break;

  case 327: /* $@160: %empty  */
#line 719 "configparser.yy"
                           { lconfig_keymod |= Mod3Mask; }
#line 7044 "configparser.cc"
    break;

  case 329: /* $@161: %empty  */
#line 720 "configparser.yy"
                           { lconfig_keymod |= Mod4Mask; }
#line 7050 "configparser.cc"
    break;

  case 331: /* $@162: %empty  */
#line 721 "configparser.yy"
                           { lconfig_keymod |= Mod5Mask; }
#line 7056 "configparser.cc"
    break;

  case 334: /* $@163: %empty  */
#line 724 "configparser.yy"
                                     { lconfig_ft1 = new WCFiletype();
                                       lconfig_ignorelist.clear();
                                     }
#line 7064 "configparser.cc"
    break;

  case 335: /* $@164: %empty  */
#line 727 "configparser.yy"
                                  { lconfig_listft->addElement( lconfig_ft1 );
                                lconfig_ft1 = NULL;
                              }
#line 7072 "configparser.cc"
    break;

  case 337: /* $@165: %empty  */
#line 730 "configparser.yy"
                                       { lconfig_ignorelist.remove_if( []( const auto &d ){ return ! d.is_pattern; } ); }
#line 7078 "configparser.cc"
    break;

  case 338: /* $@166: %empty  */
#line 731 "configparser.yy"
                                    { lconfig->setDontCheckDirs( lconfig_ignorelist );
                                    }
#line 7085 "configparser.cc"
    break;

  case 340: /* $@167: %empty  */
#line 733 "configparser.yy"
                                        { lconfig_ignorelist.remove_if( []( const auto &d ){ return d.is_pattern; } ); }
#line 7091 "configparser.cc"
    break;

  case 341: /* $@168: %empty  */
#line 734 "configparser.yy"
                                     { lconfig->setDontCheckDirs( lconfig_ignorelist );
                                     }
#line 7098 "configparser.cc"
    break;

  case 343: /* $@169: %empty  */
#line 736 "configparser.yy"
                                             { lconfig->setDontCheckVirtual( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7104 "configparser.cc"
    break;

  case 346: /* $@170: %empty  */
#line 739 "configparser.yy"
                         { lconfig_ignorelist.push_back( { .is_pattern = false, .dir = (yyvsp[-1].strptr) } ); }
#line 7110 "configparser.cc"
    break;

  case 349: /* $@171: %empty  */
#line 742 "configparser.yy"
                                        { lconfig_ignore_dir = {}; }
#line 7116 "configparser.cc"
    break;

  case 350: /* $@172: %empty  */
#line 743 "configparser.yy"
                                     { lconfig_ignorelist.push_back( lconfig_ignore_dir ); }
#line 7122 "configparser.cc"
    break;

  case 353: /* $@173: %empty  */
#line 746 "configparser.yy"
                                      { lconfig_ignore_dir = { .is_pattern = true, .dir = (yyvsp[-1].strptr) }; }
#line 7128 "configparser.cc"
    break;

  case 356: /* $@174: %empty  */
#line 749 "configparser.yy"
                                        { filetypehier.push_back( lconfig_ft1 );
                                          lconfig_ft1 = new WCFiletype();
                                        }
#line 7136 "configparser.cc"
    break;

  case 357: /* $@175: %empty  */
#line 752 "configparser.yy"
                                     { old_lconfig_ft1 = filetypehier.back();
	                               filetypehier.pop_back();
				       if ( old_lconfig_ft1 != NULL ) {
					 old_lconfig_ft1->addSubType( lconfig_ft1 );
				       } else {
					 delete lconfig_ft1;
				       }
				       lconfig_ft1 = old_lconfig_ft1;
	                             }
#line 7150 "configparser.cc"
    break;

  case 360: /* $@176: %empty  */
#line 763 "configparser.yy"
                               { lconfig_ft1->setinternID( (yyvsp[-1].num) ); }
#line 7156 "configparser.cc"
    break;

  case 362: /* $@177: %empty  */
#line 764 "configparser.yy"
                                   { lconfig_ft1->setName( (yyvsp[-1].strptr) ); }
#line 7162 "configparser.cc"
    break;

  case 364: /* $@178: %empty  */
#line 765 "configparser.yy"
                                     { lconfig_ft1->setPattern( (yyvsp[-1].strptr) ); }
#line 7168 "configparser.cc"
    break;

  case 366: /* $@179: %empty  */
#line 766 "configparser.yy"
                                  { lconfig_ft1->setUsePattern( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7174 "configparser.cc"
    break;

  case 368: /* $@180: %empty  */
#line 767 "configparser.yy"
                                   { lconfig_ft1->clearFiledesc(); }
#line 7180 "configparser.cc"
    break;

  case 370: /* $@181: %empty  */
#line 769 "configparser.yy"
                                  { lconfig_ft1->setUseFiledesc( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7186 "configparser.cc"
    break;

  case 372: /* $@182: %empty  */
#line 770 "configparser.yy"
                                         { lconfig_ft1->setPatternIgnoreCase( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7192 "configparser.cc"
    break;

  case 374: /* $@183: %empty  */
#line 771 "configparser.yy"
                                        { lconfig_ft1->setPatternUseRegExp( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7198 "configparser.cc"
    break;

  case 376: /* $@184: %empty  */
#line 772 "configparser.yy"
                                          { lconfig_ft1->setPatternUseFullname( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7204 "configparser.cc"
    break;

  case 379: /* $@185: %empty  */
#line 774 "configparser.yy"
                                     { lconfig_ft1->setExtCond( (yyvsp[-1].strptr) ); }
#line 7210 "configparser.cc"
    break;

  case 381: /* $@186: %empty  */
#line 775 "configparser.yy"
                                  { lconfig_ft1->setUseExtCond( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7216 "configparser.cc"
    break;

  case 384: /* $@187: %empty  */
#line 777 "configparser.yy"
                                                  { lconfig_ft1->setCustomColor( 0, 0, (yyvsp[-3].num) );
	                                            lconfig_ft1->setCustomColor( 1, 0, (yyvsp[-1].num) );
                                                  }
#line 7224 "configparser.cc"
    break;

  case 386: /* $@188: %empty  */
#line 780 "configparser.yy"
                                                { lconfig_ft1->setCustomColor( 0, 1, (yyvsp[-3].num) );
	                                          lconfig_ft1->setCustomColor( 1, 1, (yyvsp[-1].num) );
                                                }
#line 7232 "configparser.cc"
    break;

  case 388: /* $@189: %empty  */
#line 783 "configparser.yy"
                                                        { lconfig_ft1->setCustomColor( 0, 2, (yyvsp[-3].num) );
	                                                  lconfig_ft1->setCustomColor( 1, 2, (yyvsp[-1].num) );
                                                        }
#line 7240 "configparser.cc"
    break;

  case 390: /* $@190: %empty  */
#line 786 "configparser.yy"
                                                      { lconfig_ft1->setCustomColor( 0, 3, (yyvsp[-3].num) );
	                                                lconfig_ft1->setCustomColor( 1, 3, (yyvsp[-1].num) );
                                                      }
#line 7248 "configparser.cc"
    break;

  case 392: /* $@191: %empty  */
#line 789 "configparser.yy"
                                             { lconfig_ft1->setColorExt( (yyvsp[-1].strptr) ); }
#line 7254 "configparser.cc"
    break;

  case 394: /* $@192: %empty  */
#line 790 "configparser.yy"
                                        { lconfig_ft1->setColorMode( WCFiletype::DEFAULTCOL ); }
#line 7260 "configparser.cc"
    break;

  case 396: /* $@193: %empty  */
#line 791 "configparser.yy"
                                       { lconfig_ft1->setColorMode( WCFiletype::CUSTOMCOL ); }
#line 7266 "configparser.cc"
    break;

  case 398: /* $@194: %empty  */
#line 792 "configparser.yy"
                                       { lconfig_ft1->setColorMode( WCFiletype::EXTERNALCOL ); }
#line 7272 "configparser.cc"
    break;

  case 400: /* $@195: %empty  */
#line 793 "configparser.yy"
                                       { lconfig_ft1->setColorMode( WCFiletype::PARENTCOL ); }
#line 7278 "configparser.cc"
    break;

  case 402: /* $@196: %empty  */
#line 794 "configparser.yy"
                                          { lconfig_ft1->setMagicPattern( (yyvsp[-1].strptr) ); }
#line 7284 "configparser.cc"
    break;

  case 404: /* $@197: %empty  */
#line 795 "configparser.yy"
                                { lconfig_ft1->setUseMagic( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7290 "configparser.cc"
    break;

  case 406: /* $@198: %empty  */
#line 796 "configparser.yy"
                                       { lconfig_ft1->setMagicCompressed( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7296 "configparser.cc"
    break;

  case 408: /* $@199: %empty  */
#line 797 "configparser.yy"
                                 { lconfig_ft1->setMagicMime( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7302 "configparser.cc"
    break;

  case 410: /* $@200: %empty  */
#line 798 "configparser.yy"
                                       { lconfig_ft1->setMagicIgnoreCase( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7308 "configparser.cc"
    break;

  case 412: /* $@201: %empty  */
#line 799 "configparser.yy"
                                               { lconfig_ft1->setPatternIsCommaSeparated( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7314 "configparser.cc"
    break;

  case 414: /* $@202: %empty  */
#line 800 "configparser.yy"
                                   { lconfig_ft1->setPriority( (yyvsp[-1].num) ); }
#line 7320 "configparser.cc"
    break;

  case 417: /* $@203: %empty  */
#line 803 "configparser.yy"
                                 { lconfig_ft1->setFiledesc( (yyvsp[-3].num), (yyvsp[-1].num) ); }
#line 7326 "configparser.cc"
    break;

  case 420: /* ft_type: NORMAL_WCP  */
#line 806 "configparser.yy"
                   { (yyval.num) = NORMALTYPE; }
#line 7332 "configparser.cc"
    break;

  case 421: /* ft_type: NOTYETCHECKED_WCP  */
#line 807 "configparser.yy"
                          { (yyval.num) = NOTYETTYPE; }
#line 7338 "configparser.cc"
    break;

  case 422: /* ft_type: UNKNOWN_WCP  */
#line 808 "configparser.yy"
                    { (yyval.num) = UNKNOWNTYPE; }
#line 7344 "configparser.cc"
    break;

  case 423: /* ft_type: NOSELECT_WCP  */
#line 809 "configparser.yy"
                     { (yyval.num) = VOIDTYPE; }
#line 7350 "configparser.cc"
    break;

  case 424: /* ft_type: DIR_WCP  */
#line 810 "configparser.yy"
                { (yyval.num) = DIRTYPE; }
#line 7356 "configparser.cc"
    break;

  case 425: /* $@204: %empty  */
#line 813 "configparser.yy"
                                 { lconfig_listcom.clear(); }
#line 7362 "configparser.cc"
    break;

  case 426: /* $@205: %empty  */
#line 814 "configparser.yy"
                                   { lconfig_ft1->setDNDActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7370 "configparser.cc"
    break;

  case 428: /* $@206: %empty  */
#line 817 "configparser.yy"
                                { lconfig_listcom.clear(); }
#line 7376 "configparser.cc"
    break;

  case 429: /* $@207: %empty  */
#line 818 "configparser.yy"
                                   { lconfig_ft1->setDoubleClickActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7384 "configparser.cc"
    break;

  case 431: /* $@208: %empty  */
#line 821 "configparser.yy"
                                  { lconfig_listcom.clear(); }
#line 7390 "configparser.cc"
    break;

  case 432: /* $@209: %empty  */
#line 822 "configparser.yy"
                                   { lconfig_ft1->setShowActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7398 "configparser.cc"
    break;

  case 434: /* $@210: %empty  */
#line 825 "configparser.yy"
                                     { lconfig_listcom.clear(); }
#line 7404 "configparser.cc"
    break;

  case 435: /* $@211: %empty  */
#line 826 "configparser.yy"
                                   { lconfig_ft1->setRawShowActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7412 "configparser.cc"
    break;

  case 437: /* $@212: %empty  */
#line 829 "configparser.yy"
                                          { lconfig_listcom.clear(); }
#line 7418 "configparser.cc"
    break;

  case 438: /* $@213: %empty  */
#line 830 "configparser.yy"
                                   { lconfig_ft1->setUserActions( (yyvsp[-4].num), lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7426 "configparser.cc"
    break;

  case 440: /* $@214: %empty  */
#line 833 "configparser.yy"
                                          { lconfig_listcom.clear(); }
#line 7432 "configparser.cc"
    break;

  case 441: /* $@215: %empty  */
#line 836 "configparser.yy"
                                                  { lconfig_ft1->setCustomActions( (yyvsp[-6].strptr), lconfig_listcom );
                                                    lconfig_listcom.clear();
                               }
#line 7440 "configparser.cc"
    break;

  case 448: /* $@216: %empty  */
#line 847 "configparser.yy"
                                 { lconfig_hk1 = new WCHotkey(); }
#line 7446 "configparser.cc"
    break;

  case 449: /* $@217: %empty  */
#line 848 "configparser.yy"
                              { lconfig_listh->addElement( lconfig_hk1 );
                            lconfig_hk1 = NULL;
                          }
#line 7454 "configparser.cc"
    break;

  case 452: /* $@218: %empty  */
#line 853 "configparser.yy"
                                 { lconfig_hk1->setName( (yyvsp[-1].strptr) ); }
#line 7460 "configparser.cc"
    break;

  case 454: /* $@219: %empty  */
#line 854 "configparser.yy"
                                  { lconfig_listcom.clear(); }
#line 7466 "configparser.cc"
    break;

  case 455: /* $@220: %empty  */
#line 855 "configparser.yy"
                               { lconfig_hk1->setComs( lconfig_listcom );
                                 lconfig_listcom.clear();
                           }
#line 7474 "configparser.cc"
    break;

  case 457: /* $@221: %empty  */
#line 858 "configparser.yy"
                                   { lconfig_listsk = new List(); }
#line 7480 "configparser.cc"
    break;

  case 458: /* $@222: %empty  */
#line 859 "configparser.yy"
                              { lconfig_hk1->setDoubleKeys( lconfig_listsk );
                            lconfig_id = lconfig_listsk->initEnum();
                            lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            while ( lconfig_dk != NULL ) {
                              delete lconfig_dk;
                              lconfig_listsk->removeFirstElement();
                              lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            }
                            lconfig_listsk->closeEnum( lconfig_id );
                            delete lconfig_listsk;
                            lconfig_listsk = NULL;
                          }
#line 7497 "configparser.cc"
    break;

  case 461: /* $@223: %empty  */
#line 873 "configparser.yy"
                                 { lconfig_bt1 = new WCButton(); }
#line 7503 "configparser.cc"
    break;

  case 462: /* $@224: %empty  */
#line 874 "configparser.yy"
                              { lconfig_bt1->setCheck( true );
                            while ( lconfig_listb->size() < ( lconfig_pos + 1 ) ) lconfig_listb->addElement( new WCButton() );
                            lconfig_bt1 = (WCButton*)lconfig_listb->exchangeElement( lconfig_pos, lconfig_bt1 );
                            delete lconfig_bt1;
                            lconfig_bt1 = NULL;
                          }
#line 7514 "configparser.cc"
    break;

  case 465: /* $@225: %empty  */
#line 882 "configparser.yy"
                                 { lconfig_pos = (yyvsp[-1].num);
                           if ( lconfig_pos < 0 ) lconfig_pos = 0;
                           if ( lconfig_pos > 100000 ) lconfig_pos = 100000;
                         }
#line 7523 "configparser.cc"
    break;

  case 467: /* $@226: %empty  */
#line 886 "configparser.yy"
                                { lconfig_bt1->setText( (yyvsp[-1].strptr) ); }
#line 7529 "configparser.cc"
    break;

  case 469: /* $@227: %empty  */
#line 887 "configparser.yy"
                                        { lconfig_bt1->setFG( (yyvsp[-3].num) );
                              lconfig_bt1->setBG( (yyvsp[-1].num) );
                            }
#line 7537 "configparser.cc"
    break;

  case 471: /* $@228: %empty  */
#line 890 "configparser.yy"
                                  { lconfig_listcom.clear(); }
#line 7543 "configparser.cc"
    break;

  case 472: /* $@229: %empty  */
#line 891 "configparser.yy"
                               { lconfig_bt1->setComs( lconfig_listcom );
                                 lconfig_listcom.clear();
                           }
#line 7551 "configparser.cc"
    break;

  case 474: /* $@230: %empty  */
#line 894 "configparser.yy"
                                   { lconfig_listsk = new List(); }
#line 7557 "configparser.cc"
    break;

  case 475: /* $@231: %empty  */
#line 895 "configparser.yy"
                              { lconfig_bt1->setDoubleKeys( lconfig_listsk );
                            lconfig_id = lconfig_listsk->initEnum();
                            lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            while ( lconfig_dk != NULL ) {
                              delete lconfig_dk;
                              lconfig_listsk->removeFirstElement();
                             lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            }
                            lconfig_listsk->closeEnum( lconfig_id );
                            delete lconfig_listsk;
                            lconfig_listsk = NULL;
                          }
#line 7574 "configparser.cc"
    break;

  case 478: /* $@232: %empty  */
#line 910 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 0, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_GLOBAL, (yyvsp[-1].strptr) );
      }
#line 7585 "configparser.cc"
    break;

  case 480: /* $@233: %empty  */
#line 917 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 1, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_BUTTONS, (yyvsp[-1].strptr) );
      }
#line 7596 "configparser.cc"
    break;

  case 482: /* $@234: %empty  */
#line 924 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 2, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_LEFT, (yyvsp[-1].strptr) );
      }
#line 7607 "configparser.cc"
    break;

  case 484: /* $@235: %empty  */
#line 931 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 3, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_RIGHT, (yyvsp[-1].strptr) );
      }
#line 7618 "configparser.cc"
    break;

  case 486: /* $@236: %empty  */
#line 938 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 4, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_TEXTVIEW, (yyvsp[-1].strptr) );
      }
#line 7629 "configparser.cc"
    break;

  case 488: /* $@237: %empty  */
#line 945 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 5, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_STATEBAR, (yyvsp[-1].strptr) );
      }
#line 7640 "configparser.cc"
    break;

  case 490: /* $@238: %empty  */
#line 952 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 6, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_TEXTVIEW_ALT, (yyvsp[-1].strptr) );
      }
#line 7651 "configparser.cc"
    break;

  case 493: /* $@239: %empty  */
#line 961 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 0, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_GLOBAL, (yyvsp[-1].strptr) );
         }
#line 7662 "configparser.cc"
    break;

  case 495: /* $@240: %empty  */
#line 968 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 1, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_BUTTONS, (yyvsp[-1].strptr) );
         }
#line 7673 "configparser.cc"
    break;

  case 497: /* $@241: %empty  */
#line 975 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 2, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_LEFT, (yyvsp[-1].strptr) );
         }
#line 7684 "configparser.cc"
    break;

  case 499: /* $@242: %empty  */
#line 982 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 3, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_RIGHT, (yyvsp[-1].strptr) );
         }
#line 7695 "configparser.cc"
    break;

  case 501: /* $@243: %empty  */
#line 989 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 4, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_TEXTVIEW, (yyvsp[-1].strptr) );
         }
#line 7706 "configparser.cc"
    break;

  case 503: /* $@244: %empty  */
#line 996 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 5, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_STATEBAR, (yyvsp[-1].strptr) );
         }
#line 7717 "configparser.cc"
    break;

  case 505: /* $@245: %empty  */
#line 1003 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 6, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_TEXTVIEW_ALT, (yyvsp[-1].strptr) );
         }
#line 7728 "configparser.cc"
    break;

  case 508: /* $@246: %empty  */
#line 1012 "configparser.yy"
              {
                  lconfig->setVMMountCommand( (yyvsp[-1].strptr) );
              }
#line 7736 "configparser.cc"
    break;

  case 510: /* $@247: %empty  */
#line 1016 "configparser.yy"
              {
                  lconfig->setVMUnmountCommand( (yyvsp[-1].strptr) );
              }
#line 7744 "configparser.cc"
    break;

  case 512: /* $@248: %empty  */
#line 1020 "configparser.yy"
              {
                  lconfig->setVMEjectCommand( (yyvsp[-1].strptr) );
              }
#line 7752 "configparser.cc"
    break;

  case 514: /* $@249: %empty  */
#line 1024 "configparser.yy"
              {
                  lconfig->setVMCloseTrayCommand( (yyvsp[-1].strptr) );
              }
#line 7760 "configparser.cc"
    break;

  case 516: /* $@250: %empty  */
#line 1028 "configparser.yy"
              {
                  lconfig->setVMFStabFile( (yyvsp[-1].strptr) );
              }
#line 7768 "configparser.cc"
    break;

  case 518: /* $@251: %empty  */
#line 1032 "configparser.yy"
              {
                  lconfig->setVMMtabFile( (yyvsp[-1].strptr) );
              }
#line 7776 "configparser.cc"
    break;

  case 520: /* $@252: %empty  */
#line 1036 "configparser.yy"
              {
                  lconfig->setVMPartitionFile( (yyvsp[-1].strptr) );
              }
#line 7784 "configparser.cc"
    break;

  case 522: /* $@253: %empty  */
#line 1040 "configparser.yy"
              {
                  lconfig->setVMRequestAction( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false );
              }
#line 7792 "configparser.cc"
    break;

  case 524: /* $@254: %empty  */
#line 1044 "configparser.yy"
              {
                  lconfig->setVMPreferredUdisksVersion( (yyvsp[-1].num) );
              }
#line 7800 "configparser.cc"
    break;

  case 527: /* $@255: %empty  */
#line 1049 "configparser.yy"
                                          { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_TIMERAM ); }
#line 7806 "configparser.cc"
    break;

  case 529: /* $@256: %empty  */
#line 1050 "configparser.yy"
                                     { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_TIME ); }
#line 7812 "configparser.cc"
    break;

  case 531: /* $@257: %empty  */
#line 1051 "configparser.yy"
                                        { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_VERSION ); }
#line 7818 "configparser.cc"
    break;

  case 533: /* $@258: %empty  */
#line 1052 "configparser.yy"
                                       { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_EXTERN ); }
#line 7824 "configparser.cc"
    break;

  case 535: /* $@259: %empty  */
#line 1053 "configparser.yy"
                                         { lconfig->setClockbarUpdatetime( (yyvsp[-1].num) ); }
#line 7830 "configparser.cc"
    break;

  case 537: /* $@260: %empty  */
#line 1054 "configparser.yy"
                                         { lconfig->setClockbarCommand( (yyvsp[-1].strptr) ); }
#line 7836 "configparser.cc"
    break;

  case 539: /* $@261: %empty  */
#line 1055 "configparser.yy"
                                     { lconfig->setClockbarHints( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7842 "configparser.cc"
    break;

  case 542: /* bool: YES_WCP  */
#line 1058 "configparser.yy"
             { (yyval.num) = 1; }
#line 7848 "configparser.cc"
    break;

  case 543: /* bool: NO_WCP  */
#line 1059 "configparser.yy"
            { (yyval.num) = 0; }
#line 7854 "configparser.cc"
    break;

  case 617: /* $@262: %empty  */
#line 1135 "configparser.yy"
                              { ps.lconfig_fp1 = std::make_shared< OwnOp >(); }
#line 7860 "configparser.cc"
    break;

  case 618: /* ownop: OWNOP_WCP LEFTBRACE_WCP $@262 ownopbody RIGHTBRACE_WCP  */
#line 1136 "configparser.yy"
                               { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 7866 "configparser.cc"
    break;

  case 619: /* $@263: %empty  */
#line 1138 "configparser.yy"
                                  { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setComStr( (yyvsp[-1].strptr) ); }
#line 7872 "configparser.cc"
    break;

  case 621: /* $@264: %empty  */
#line 1139 "configparser.yy"
                                          { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setSeparateEachEntry( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7878 "configparser.cc"
    break;

  case 623: /* $@265: %empty  */
#line 1140 "configparser.yy"
                                  { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7884 "configparser.cc"
    break;

  case 625: /* $@266: %empty  */
#line 1141 "configparser.yy"
                                      { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_IN_TERMINAL ); }
#line 7890 "configparser.cc"
    break;

  case 627: /* $@267: %empty  */
#line 1142 "configparser.yy"
                                          { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_IN_TERMINAL_AND_WAIT4KEY ); }
#line 7896 "configparser.cc"
    break;

  case 629: /* $@268: %empty  */
#line 1143 "configparser.yy"
                                        { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT ); }
#line 7902 "configparser.cc"
    break;

  case 631: /* $@269: %empty  */
#line 1144 "configparser.yy"
                                           { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_INT ); }
#line 7908 "configparser.cc"
    break;

  case 633: /* $@270: %empty  */
#line 1145 "configparser.yy"
                                                 { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_OTHER_SIDE ); }
#line 7914 "configparser.cc"
    break;

  case 635: /* $@271: %empty  */
#line 1146 "configparser.yy"
                                                       { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_CUSTOM_ATTRIBUTE ); }
#line 7920 "configparser.cc"
    break;

  case 637: /* $@272: %empty  */
#line 1147 "configparser.yy"
                                    { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_NORMAL ); }
#line 7926 "configparser.cc"
    break;

  case 639: /* $@273: %empty  */
#line 1148 "configparser.yy"
                                      { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setViewStr( (yyvsp[-1].strptr) ); }
#line 7932 "configparser.cc"
    break;

  case 641: /* $@274: %empty  */
#line 1149 "configparser.yy"
                                     { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setInBackground( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7938 "configparser.cc"
    break;

  case 643: /* $@275: %empty  */
#line 1150 "configparser.yy"
                                 { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setTakeDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7944 "configparser.cc"
    break;

  case 645: /* $@276: %empty  */
#line 1151 "configparser.yy"
                               { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setDontCD( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7950 "configparser.cc"
    break;

  case 647: /* $@277: %empty  */
#line 1152 "configparser.yy"
                                                { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setUseVirtualTempCopies( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7956 "configparser.cc"
    break;

  case 649: /* $@278: %empty  */
#line 1153 "configparser.yy"
                                                      { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setFlagsAreRelativeToBaseDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7962 "configparser.cc"
    break;

  case 651: /* $@279: %empty  */
#line 1154 "configparser.yy"
                                     { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setFollowActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7968 "configparser.cc"
    break;

  case 653: /* $@280: %empty  */
#line 1155 "configparser.yy"
                                  { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setWatchFile( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7974 "configparser.cc"
    break;

  case 656: /* $@281: %empty  */
#line 1158 "configparser.yy"
                                      { ps.lconfig_fp1 = std::make_shared<DNDAction>(); }
#line 7980 "configparser.cc"
    break;

  case 657: /* dndaction: DNDACTION_WCP LEFTBRACE_WCP $@281 RIGHTBRACE_WCP  */
#line 1159 "configparser.yy"
                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 7986 "configparser.cc"
    break;

  case 658: /* $@282: %empty  */
#line 1161 "configparser.yy"
                                    { ps.lconfig_fp1 = std::make_shared<DoubleClickAction>(); }
#line 7992 "configparser.cc"
    break;

  case 659: /* dcaction: DCACTION_WCP LEFTBRACE_WCP $@282 RIGHTBRACE_WCP  */
#line 1162 "configparser.yy"
                        { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 7998 "configparser.cc"
    break;

  case 660: /* $@283: %empty  */
#line 1164 "configparser.yy"
                                        { ps.lconfig_fp1 = std::make_shared<ShowAction>(); }
#line 8004 "configparser.cc"
    break;

  case 661: /* showaction: SHOWACTION_WCP LEFTBRACE_WCP $@283 RIGHTBRACE_WCP  */
#line 1165 "configparser.yy"
                          { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8010 "configparser.cc"
    break;

  case 662: /* $@284: %empty  */
#line 1167 "configparser.yy"
                                          { ps.lconfig_fp1 = std::make_shared<RawShowAction>(); }
#line 8016 "configparser.cc"
    break;

  case 663: /* rshowaction: RSHOWACTION_WCP LEFTBRACE_WCP $@284 RIGHTBRACE_WCP  */
#line 1168 "configparser.yy"
                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8022 "configparser.cc"
    break;

  case 664: /* $@285: %empty  */
#line 1170 "configparser.yy"
                                        { ps.lconfig_fp1 = std::make_shared<UserAction>(); }
#line 8028 "configparser.cc"
    break;

  case 665: /* useraction: USERACTION_WCP LEFTBRACE_WCP $@285 useractionbody RIGHTBRACE_WCP  */
#line 1171 "configparser.yy"
                                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8034 "configparser.cc"
    break;

  case 666: /* $@286: %empty  */
#line 1173 "configparser.yy"
                                             { std::dynamic_pointer_cast<UserAction>(ps.lconfig_fp1)->setNr( (yyvsp[-1].num) ); }
#line 8040 "configparser.cc"
    break;

  case 669: /* $@287: %empty  */
#line 1176 "configparser.yy"
                              { auto tfp = std::make_shared< ScriptOp >();
                                tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                tfp->setCommandStr( "up" );
                                ps.lconfig_fp1 = tfp;
                              }
#line 8050 "configparser.cc"
    break;

  case 670: /* rowup: ROWUP_WCP LEFTBRACE_WCP $@287 RIGHTBRACE_WCP  */
#line 1181 "configparser.yy"
                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8056 "configparser.cc"
    break;

  case 671: /* $@288: %empty  */
#line 1183 "configparser.yy"
                                  { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "down" );
                                    ps.lconfig_fp1 = tfp;
                                  }
#line 8066 "configparser.cc"
    break;

  case 672: /* rowdown: ROWDOWN_WCP LEFTBRACE_WCP $@288 RIGHTBRACE_WCP  */
#line 1188 "configparser.yy"
                       { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8072 "configparser.cc"
    break;

  case 673: /* $@289: %empty  */
#line 1190 "configparser.yy"
                                                    { ps.lconfig_fp1 = std::make_shared<ChangeHiddenFlag>(); }
#line 8078 "configparser.cc"
    break;

  case 674: /* changehiddenflag: CHANGEHIDDENFLAG_WCP LEFTBRACE_WCP $@289 changehiddenflagbody RIGHTBRACE_WCP  */
#line 1191 "configparser.yy"
                                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8084 "configparser.cc"
    break;

  case 675: /* $@290: %empty  */
#line 1193 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeHiddenFlag>(ps.lconfig_fp1)->setMode( 2 ); }
#line 8090 "configparser.cc"
    break;

  case 677: /* $@291: %empty  */
#line 1194 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeHiddenFlag>(ps.lconfig_fp1)->setMode( 1 ); }
#line 8096 "configparser.cc"
    break;

  case 679: /* $@292: %empty  */
#line 1195 "configparser.yy"
                                                     { std::dynamic_pointer_cast<ChangeHiddenFlag>(ps.lconfig_fp1)->setMode( 0 ); }
#line 8102 "configparser.cc"
    break;

  case 682: /* $@293: %empty  */
#line 1198 "configparser.yy"
                                { ps.lconfig_fp1 = std::make_shared<CopyOp>(); }
#line 8108 "configparser.cc"
    break;

  case 683: /* copyop: COPYOP_WCP LEFTBRACE_WCP $@293 copyopbody RIGHTBRACE_WCP  */
#line 1199 "configparser.yy"
                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8114 "configparser.cc"
    break;

  case 684: /* $@294: %empty  */
#line 1201 "configparser.yy"
                                        { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setFollowSymlinks( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8120 "configparser.cc"
    break;

  case 686: /* $@295: %empty  */
#line 1202 "configparser.yy"
                              { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setMove( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8126 "configparser.cc"
    break;

  case 688: /* $@296: %empty  */
#line 1203 "configparser.yy"
                                { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setRename( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8132 "configparser.cc"
    break;

  case 690: /* $@297: %empty  */
#line 1204 "configparser.yy"
                                 { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setSameDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8138 "configparser.cc"
    break;

  case 692: /* $@298: %empty  */
#line 1205 "configparser.yy"
                                     { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setRequestDest( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8144 "configparser.cc"
    break;

  case 694: /* $@299: %empty  */
#line 1206 "configparser.yy"
                                      { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8150 "configparser.cc"
    break;

  case 696: /* $@300: %empty  */
#line 1207 "configparser.yy"
                                         { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_ALWAYS ); }
#line 8156 "configparser.cc"
    break;

  case 698: /* $@301: %empty  */
#line 1208 "configparser.yy"
                                        { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_NEVER ); }
#line 8162 "configparser.cc"
    break;

  case 700: /* $@302: %empty  */
#line 1209 "configparser.yy"
                                         { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_NORMAL ); }
#line 8168 "configparser.cc"
    break;

  case 702: /* $@303: %empty  */
#line 1210 "configparser.yy"
                                        {}
#line 8174 "configparser.cc"
    break;

  case 704: /* $@304: %empty  */
#line 1211 "configparser.yy"
                                      {}
#line 8180 "configparser.cc"
    break;

  case 706: /* $@305: %empty  */
#line 1212 "configparser.yy"
                                      { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setPreserveAttr( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8186 "configparser.cc"
    break;

  case 708: /* $@306: %empty  */
#line 1213 "configparser.yy"
                                                     { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_NEVER ); }
#line 8192 "configparser.cc"
    break;

  case 710: /* $@307: %empty  */
#line 1214 "configparser.yy"
                                                       { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_OUTSIDE ); }
#line 8198 "configparser.cc"
    break;

  case 712: /* $@308: %empty  */
#line 1215 "configparser.yy"
                                                      { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_ALWAYS ); }
#line 8204 "configparser.cc"
    break;

  case 714: /* $@309: %empty  */
#line 1216 "configparser.yy"
                                                                 { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( LEAVE_PERMISSIONS_UNMODIFIED ); }
#line 8210 "configparser.cc"
    break;

  case 716: /* $@310: %empty  */
#line 1217 "configparser.yy"
                                                        { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_PERMISSION ); }
#line 8216 "configparser.cc"
    break;

  case 718: /* $@311: %empty  */
#line 1218 "configparser.yy"
                                                                { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_GROUP_R_PERMISSION ); }
#line 8222 "configparser.cc"
    break;

  case 720: /* $@312: %empty  */
#line 1219 "configparser.yy"
                                                              { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_ALL_R_PERMISSION ); }
#line 8228 "configparser.cc"
    break;

  case 722: /* $@313: %empty  */
#line 1220 "configparser.yy"
                                                     { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setVdirPreserveDirStructure( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8234 "configparser.cc"
    break;

  case 725: /* $@314: %empty  */
#line 1223 "configparser.yy"
                                    { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "first" );
                                      ps.lconfig_fp1 = tfp;
                                    }
#line 8244 "configparser.cc"
    break;

  case 726: /* firstrow: FIRSTROW_WCP LEFTBRACE_WCP $@314 RIGHTBRACE_WCP  */
#line 1228 "configparser.yy"
                        { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8250 "configparser.cc"
    break;

  case 727: /* $@315: %empty  */
#line 1230 "configparser.yy"
                                  { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "last" );
                                    ps.lconfig_fp1 = tfp;
                                  }
#line 8260 "configparser.cc"
    break;

  case 728: /* lastrow: LASTROW_WCP LEFTBRACE_WCP $@315 RIGHTBRACE_WCP  */
#line 1235 "configparser.yy"
                       { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8266 "configparser.cc"
    break;

  case 729: /* $@316: %empty  */
#line 1237 "configparser.yy"
                                { auto tfp = std::make_shared< ScriptOp >();
                                  tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                  tfp->setCommandStr( "pageup" );
                                  ps.lconfig_fp1 = tfp;
                                }
#line 8276 "configparser.cc"
    break;

  case 730: /* pageup: PAGEUP_WCP LEFTBRACE_WCP $@316 RIGHTBRACE_WCP  */
#line 1242 "configparser.yy"
                      { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8282 "configparser.cc"
    break;

  case 731: /* $@317: %empty  */
#line 1244 "configparser.yy"
                                    { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "pagedown" );
                                      ps.lconfig_fp1 = tfp;
                                    }
#line 8292 "configparser.cc"
    break;

  case 732: /* pagedown: PAGEDOWN_WCP LEFTBRACE_WCP $@317 RIGHTBRACE_WCP  */
#line 1249 "configparser.yy"
                        { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8298 "configparser.cc"
    break;

  case 733: /* $@318: %empty  */
#line 1251 "configparser.yy"
                                    { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "selectentry" );
                                      ps.lconfig_fp1 = tfp;
                                    }
#line 8308 "configparser.cc"
    break;

  case 734: /* selectop: SELECTOP_WCP LEFTBRACE_WCP $@318 RIGHTBRACE_WCP  */
#line 1256 "configparser.yy"
                        { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8314 "configparser.cc"
    break;

  case 735: /* $@319: %empty  */
#line 1258 "configparser.yy"
                                          { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "selectall" );
                                            ps.lconfig_fp1 = tfp;
                                          }
#line 8324 "configparser.cc"
    break;

  case 736: /* selectallop: SELECTALLOP_WCP LEFTBRACE_WCP $@319 RIGHTBRACE_WCP  */
#line 1263 "configparser.yy"
                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8330 "configparser.cc"
    break;

  case 737: /* $@320: %empty  */
#line 1265 "configparser.yy"
                                            { auto tfp = std::make_shared< ScriptOp >();
                                              tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                              tfp->setCommandStr( "selectnone" );
                                              ps.lconfig_fp1 = tfp;
                                            }
#line 8340 "configparser.cc"
    break;

  case 738: /* selectnoneop: SELECTNONEOP_WCP LEFTBRACE_WCP $@320 RIGHTBRACE_WCP  */
#line 1270 "configparser.yy"
                            { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8346 "configparser.cc"
    break;

  case 739: /* $@321: %empty  */
#line 1272 "configparser.yy"
                                          { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "invertall" );
                                            ps.lconfig_fp1 = tfp;
                                          }
#line 8356 "configparser.cc"
    break;

  case 740: /* invertallop: INVERTALLOP_WCP LEFTBRACE_WCP $@321 RIGHTBRACE_WCP  */
#line 1277 "configparser.yy"
                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8362 "configparser.cc"
    break;

  case 741: /* $@322: %empty  */
#line 1279 "configparser.yy"
                                          { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "parentdir" );
                                            ps.lconfig_fp1 = tfp;
                                          }
#line 8372 "configparser.cc"
    break;

  case 742: /* parentdirop: PARENTDIROP_WCP LEFTBRACE_WCP $@322 RIGHTBRACE_WCP  */
#line 1284 "configparser.yy"
                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8378 "configparser.cc"
    break;

  case 743: /* $@323: %empty  */
#line 1286 "configparser.yy"
                                        { ps.lconfig_fp1 = std::make_shared<EnterDirOp>(); }
#line 8384 "configparser.cc"
    break;

  case 744: /* enterdirop: ENTERDIROP_WCP LEFTBRACE_WCP $@323 enterdiropbody RIGHTBRACE_WCP  */
#line 1287 "configparser.yy"
                                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8390 "configparser.cc"
    break;

  case 745: /* $@324: %empty  */
#line 1289 "configparser.yy"
                                       { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setDir( (yyvsp[-1].strptr) ); }
#line 8396 "configparser.cc"
    break;

  case 747: /* $@325: %empty  */
#line 1290 "configparser.yy"
                                        { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_ACTIVE ); }
#line 8402 "configparser.cc"
    break;

  case 749: /* $@326: %empty  */
#line 1291 "configparser.yy"
                                              { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_ACTIVE2OTHER ); }
#line 8408 "configparser.cc"
    break;

  case 751: /* $@327: %empty  */
#line 1292 "configparser.yy"
                                         { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_SPECIAL ); }
#line 8414 "configparser.cc"
    break;

  case 753: /* $@328: %empty  */
#line 1293 "configparser.yy"
                                         { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_REQUEST ); }
#line 8420 "configparser.cc"
    break;

  case 756: /* $@329: %empty  */
#line 1296 "configparser.yy"
                                                      { ps.lconfig_fp1 = std::make_shared<ChangeListerSetOp>(); }
#line 8426 "configparser.cc"
    break;

  case 757: /* changelistersetop: CHANGELISTERSETOP_WCP LEFTBRACE_WCP $@329 changelistersetopbody RIGHTBRACE_WCP  */
#line 1297 "configparser.yy"
                                                       { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8432 "configparser.cc"
    break;

  case 758: /* $@330: %empty  */
#line 1299 "configparser.yy"
                                             { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_LEFT_LISTER ); }
#line 8438 "configparser.cc"
    break;

  case 760: /* $@331: %empty  */
#line 1300 "configparser.yy"
                                              { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_RIGHT_LISTER ); }
#line 8444 "configparser.cc"
    break;

  case 762: /* $@332: %empty  */
#line 1301 "configparser.yy"
                                                { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_CURRENT_LISTER ); }
#line 8450 "configparser.cc"
    break;

  case 764: /* $@333: %empty  */
#line 1302 "configparser.yy"
                                              { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_OTHER_LISTER ); }
#line 8456 "configparser.cc"
    break;

  case 767: /* $@334: %empty  */
#line 1305 "configparser.yy"
                                                { ps.lconfig_fp1 = std::make_shared<SwitchListerOp>(); }
#line 8462 "configparser.cc"
    break;

  case 768: /* switchlisterop: SWITCHLISTEROP_WCP LEFTBRACE_WCP $@334 RIGHTBRACE_WCP  */
#line 1306 "configparser.yy"
                              { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8468 "configparser.cc"
    break;

  case 769: /* $@335: %empty  */
#line 1308 "configparser.yy"
                                                { ps.lconfig_fp1 = std::make_shared<FilterSelectOp>(); }
#line 8474 "configparser.cc"
    break;

  case 770: /* filterselectop: FILTERSELECTOP_WCP LEFTBRACE_WCP $@335 filterselectopbody RIGHTBRACE_WCP  */
#line 1309 "configparser.yy"
                                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8480 "configparser.cc"
    break;

  case 771: /* $@336: %empty  */
#line 1311 "configparser.yy"
                                              { std::dynamic_pointer_cast<FilterSelectOp>(ps.lconfig_fp1)->setFilter( (yyvsp[-1].strptr) ); }
#line 8486 "configparser.cc"
    break;

  case 773: /* $@337: %empty  */
#line 1312 "configparser.yy"
                                            { std::dynamic_pointer_cast<FilterSelectOp>(ps.lconfig_fp1)->setAutoFilter( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8492 "configparser.cc"
    break;

  case 775: /* $@338: %empty  */
#line 1313 "configparser.yy"
                                                        { std::dynamic_pointer_cast<FilterSelectOp>(ps.lconfig_fp1)->setImmediateFilterApply( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8498 "configparser.cc"
    break;

  case 778: /* $@339: %empty  */
#line 1316 "configparser.yy"
                                                    { ps.lconfig_fp1 = std::make_shared<FilterUnSelectOp>(); }
#line 8504 "configparser.cc"
    break;

  case 779: /* filterunselectop: FILTERUNSELECTOP_WCP LEFTBRACE_WCP $@339 filterunselectopbody RIGHTBRACE_WCP  */
#line 1317 "configparser.yy"
                                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8510 "configparser.cc"
    break;

  case 780: /* $@340: %empty  */
#line 1319 "configparser.yy"
                                                { std::dynamic_pointer_cast<FilterUnSelectOp>(ps.lconfig_fp1)->setFilter( (yyvsp[-1].strptr) ); }
#line 8516 "configparser.cc"
    break;

  case 782: /* $@341: %empty  */
#line 1320 "configparser.yy"
                                              { std::dynamic_pointer_cast<FilterUnSelectOp>(ps.lconfig_fp1)->setAutoFilter( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8522 "configparser.cc"
    break;

  case 784: /* $@342: %empty  */
#line 1321 "configparser.yy"
                                                          { std::dynamic_pointer_cast<FilterUnSelectOp>(ps.lconfig_fp1)->setImmediateFilterApply( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8528 "configparser.cc"
    break;

  case 787: /* $@343: %empty  */
#line 1324 "configparser.yy"
                                                      { ps.lconfig_fp1 = std::make_shared<Path2OSideOp>(); }
#line 8534 "configparser.cc"
    break;

  case 788: /* pathtoothersideop: PATHTOOTHERSIDEOP_WCP LEFTBRACE_WCP $@343 RIGHTBRACE_WCP  */
#line 1325 "configparser.yy"
                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8540 "configparser.cc"
    break;

  case 789: /* $@344: %empty  */
#line 1327 "configparser.yy"
                                { ps.lconfig_fp1 = std::make_shared<QuitOp>(); }
#line 8546 "configparser.cc"
    break;

  case 790: /* quitop: QUITOP_WCP LEFTBRACE_WCP $@344 quitopbody RIGHTBRACE_WCP  */
#line 1328 "configparser.yy"
                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8552 "configparser.cc"
    break;

  case 791: /* $@345: %empty  */
#line 1330 "configparser.yy"
                                    { std::dynamic_pointer_cast<QuitOp>(ps.lconfig_fp1)->setMode( QuitOp::Q_NORMAL_QUIT ); }
#line 8558 "configparser.cc"
    break;

  case 793: /* $@346: %empty  */
#line 1331 "configparser.yy"
                                   { std::dynamic_pointer_cast<QuitOp>(ps.lconfig_fp1)->setMode( QuitOp::Q_QUICK_QUIT ); }
#line 8564 "configparser.cc"
    break;

  case 796: /* $@347: %empty  */
#line 1334 "configparser.yy"
                                    { ps.lconfig_fp1 = std::make_shared<DeleteOp>(); }
#line 8570 "configparser.cc"
    break;

  case 797: /* deleteop: DELETEOP_WCP LEFTBRACE_WCP $@347 deleteopbody RIGHTBRACE_WCP  */
#line 1335 "configparser.yy"
                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8576 "configparser.cc"
    break;

  case 798: /* $@348: %empty  */
#line 1337 "configparser.yy"
                                      { std::dynamic_pointer_cast<DeleteOp>(ps.lconfig_fp1)->setAlsoActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8582 "configparser.cc"
    break;

  case 801: /* $@349: %empty  */
#line 1340 "configparser.yy"
                                    { ps.lconfig_fp1 = std::make_shared<ReloadOp>(); }
#line 8588 "configparser.cc"
    break;

  case 802: /* reloadop: RELOADOP_WCP LEFTBRACE_WCP $@349 reloadopbody RIGHTBRACE_WCP  */
#line 1341 "configparser.yy"
                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8594 "configparser.cc"
    break;

  case 803: /* $@350: %empty  */
#line 1343 "configparser.yy"
                                    { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_LEFTSIDE ); }
#line 8600 "configparser.cc"
    break;

  case 805: /* $@351: %empty  */
#line 1344 "configparser.yy"
                                     { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_RIGHTSIDE ); }
#line 8606 "configparser.cc"
    break;

  case 807: /* $@352: %empty  */
#line 1345 "configparser.yy"
                                       { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_THISSIDE ); }
#line 8612 "configparser.cc"
    break;

  case 809: /* $@353: %empty  */
#line 1346 "configparser.yy"
                                     { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_OTHERSIDE ); }
#line 8618 "configparser.cc"
    break;

  case 811: /* $@354: %empty  */
#line 1347 "configparser.yy"
                                         { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setResetDirSizes( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8624 "configparser.cc"
    break;

  case 813: /* $@355: %empty  */
#line 1348 "configparser.yy"
                                         { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setKeepFiletypes( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8630 "configparser.cc"
    break;

  case 816: /* $@356: %empty  */
#line 1351 "configparser.yy"
                                      { ps.lconfig_fp1 = std::make_shared<MakeDirOp>(); }
#line 8636 "configparser.cc"
    break;

  case 817: /* makedirop: MAKEDIROP_WCP LEFTBRACE_WCP $@356 RIGHTBRACE_WCP  */
#line 1352 "configparser.yy"
                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8642 "configparser.cc"
    break;

  case 818: /* $@357: %empty  */
#line 1354 "configparser.yy"
                                    { ps.lconfig_fp1 = std::make_shared<RenameOp>(); }
#line 8648 "configparser.cc"
    break;

  case 819: /* renameop: RENAMEOP_WCP LEFTBRACE_WCP $@357 renameopbody RIGHTBRACE_WCP  */
#line 1355 "configparser.yy"
                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8654 "configparser.cc"
    break;

  case 820: /* $@358: %empty  */
#line 1357 "configparser.yy"
                                               { std::dynamic_pointer_cast<RenameOp>(ps.lconfig_fp1)->setAlsoSelectFileExtension( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8660 "configparser.cc"
    break;

  case 823: /* $@359: %empty  */
#line 1360 "configparser.yy"
                                      { ps.lconfig_fp1 = std::make_shared<DirSizeOp>(); }
#line 8666 "configparser.cc"
    break;

  case 824: /* dirsizeop: DIRSIZEOP_WCP LEFTBRACE_WCP $@359 RIGHTBRACE_WCP  */
#line 1361 "configparser.yy"
                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8672 "configparser.cc"
    break;

  case 825: /* $@360: %empty  */
#line 1363 "configparser.yy"
                                  { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "simulate_doubleclick" );
                                    ps.lconfig_fp1 = tfp;
                                  }
#line 8682 "configparser.cc"
    break;

  case 826: /* simddop: SIMDDOP_WCP LEFTBRACE_WCP $@360 RIGHTBRACE_WCP  */
#line 1368 "configparser.yy"
                       { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8688 "configparser.cc"
    break;

  case 827: /* $@361: %empty  */
#line 1370 "configparser.yy"
                                          { ps.lconfig_fp1 = std::make_shared<StartProgOp>(); }
#line 8694 "configparser.cc"
    break;

  case 828: /* startprogop: STARTPROGOP_WCP LEFTBRACE_WCP $@361 startprogopbody RIGHTBRACE_WCP  */
#line 1371 "configparser.yy"
                                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8700 "configparser.cc"
    break;

  case 829: /* $@362: %empty  */
#line 1373 "configparser.yy"
                                            { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_IN_TERMINAL ); }
#line 8706 "configparser.cc"
    break;

  case 831: /* $@363: %empty  */
#line 1374 "configparser.yy"
                                                { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_IN_TERMINAL_AND_WAIT4KEY ); }
#line 8712 "configparser.cc"
    break;

  case 833: /* $@364: %empty  */
#line 1375 "configparser.yy"
                                              { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT ); }
#line 8718 "configparser.cc"
    break;

  case 835: /* $@365: %empty  */
#line 1376 "configparser.yy"
                                                 { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_INT ); }
#line 8724 "configparser.cc"
    break;

  case 837: /* $@366: %empty  */
#line 1377 "configparser.yy"
                                                       { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_OTHER_SIDE ); }
#line 8730 "configparser.cc"
    break;

  case 839: /* $@367: %empty  */
#line 1378 "configparser.yy"
                                                             { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_CUSTOM_ATTRIBUTE ); }
#line 8736 "configparser.cc"
    break;

  case 841: /* $@368: %empty  */
#line 1379 "configparser.yy"
                                          { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_NORMAL ); }
#line 8742 "configparser.cc"
    break;

  case 843: /* $@369: %empty  */
#line 1380 "configparser.yy"
                                            { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setViewStr( (yyvsp[-1].strptr) ); }
#line 8748 "configparser.cc"
    break;

  case 845: /* $@370: %empty  */
#line 1381 "configparser.yy"
                                     { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setGlobal( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8754 "configparser.cc"
    break;

  case 847: /* $@371: %empty  */
#line 1382 "configparser.yy"
                                           { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8760 "configparser.cc"
    break;

  case 849: /* $@372: %empty  */
#line 1383 "configparser.yy"
                                           { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setInBackground( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8766 "configparser.cc"
    break;

  case 851: /* $@373: %empty  */
#line 1384 "configparser.yy"
                                     { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setDontCD( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8772 "configparser.cc"
    break;

  case 853: /* $@374: %empty  */
#line 1385 "configparser.yy"
                                           { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setFollowActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8778 "configparser.cc"
    break;

  case 855: /* $@375: %empty  */
#line 1386 "configparser.yy"
                                        { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setWatchFile( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8784 "configparser.cc"
    break;

  case 857: /* $@376: %empty  */
#line 1387 "configparser.yy"
                                                { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setSeparateEachEntry( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8790 "configparser.cc"
    break;

  case 860: /* $@377: %empty  */
#line 1390 "configparser.yy"
                                              { ps.lconfig_fp1 = std::make_shared<SearchEntryOp>(); }
#line 8796 "configparser.cc"
    break;

  case 861: /* searchentryop: SEARCHENTRYOP_WCP LEFTBRACE_WCP $@377 searchentryopbody RIGHTBRACE_WCP  */
#line 1391 "configparser.yy"
                                               { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8802 "configparser.cc"
    break;

  case 862: /* $@378: %empty  */
#line 1393 "configparser.yy"
                                           { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setIgnoreCase( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8808 "configparser.cc"
    break;

  case 864: /* $@379: %empty  */
#line 1394 "configparser.yy"
                                              { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setReverseSearch( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8814 "configparser.cc"
    break;

  case 866: /* $@380: %empty  */
#line 1395 "configparser.yy"
                                            { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setInfixSearch( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8820 "configparser.cc"
    break;

  case 868: /* $@381: %empty  */
#line 1396 "configparser.yy"
                                              { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setFlexibleMatch( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8826 "configparser.cc"
    break;

  case 870: /* $@382: %empty  */
#line 1397 "configparser.yy"
                                                        { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setStartSearchString( (yyvsp[-1].strptr) ); }
#line 8832 "configparser.cc"
    break;

  case 872: /* $@383: %empty  */
#line 1398 "configparser.yy"
                                          { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setApplyOnly( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8838 "configparser.cc"
    break;

  case 875: /* $@384: %empty  */
#line 1401 "configparser.yy"
                                          { ps.lconfig_fp1 = std::make_shared<EnterPathOp>(); }
#line 8844 "configparser.cc"
    break;

  case 876: /* enterpathop: ENTERPATHOP_WCP LEFTBRACE_WCP $@384 enterpathopbody RIGHTBRACE_WCP  */
#line 1402 "configparser.yy"
                                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8850 "configparser.cc"
    break;

  case 877: /* $@385: %empty  */
#line 1404 "configparser.yy"
                                       { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_LEFTSIDE ); }
#line 8856 "configparser.cc"
    break;

  case 879: /* $@386: %empty  */
#line 1405 "configparser.yy"
                                        { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_RIGHTSIDE ); }
#line 8862 "configparser.cc"
    break;

  case 881: /* $@387: %empty  */
#line 1406 "configparser.yy"
                                          { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_THISSIDE ); }
#line 8868 "configparser.cc"
    break;

  case 883: /* $@388: %empty  */
#line 1407 "configparser.yy"
                                        { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_OTHERSIDE ); }
#line 8874 "configparser.cc"
    break;

  case 886: /* $@389: %empty  */
#line 1410 "configparser.yy"
                                                { ps.lconfig_fp1 = std::make_shared<ScrollListerOp>(); }
#line 8880 "configparser.cc"
    break;

  case 887: /* scrolllisterop: SCROLLLISTEROP_WCP LEFTBRACE_WCP $@389 scrolllisteropbody RIGHTBRACE_WCP  */
#line 1411 "configparser.yy"
                                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8886 "configparser.cc"
    break;

  case 888: /* $@390: %empty  */
#line 1413 "configparser.yy"
                                          { std::dynamic_pointer_cast<ScrollListerOp>(ps.lconfig_fp1)->setDir( ScrollListerOp::SCROLLLISTEROP_LEFT ); }
#line 8892 "configparser.cc"
    break;

  case 890: /* $@391: %empty  */
#line 1414 "configparser.yy"
                                           { std::dynamic_pointer_cast<ScrollListerOp>(ps.lconfig_fp1)->setDir( ScrollListerOp::SCROLLLISTEROP_RIGHT ); }
#line 8898 "configparser.cc"
    break;

  case 893: /* $@392: %empty  */
#line 1417 "configparser.yy"
                                                  { ps.lconfig_fp1 = std::make_shared<CreateSymlinkOp>(); }
#line 8904 "configparser.cc"
    break;

  case 894: /* createsymlinkop: CREATESYMLINKOP_WCP LEFTBRACE_WCP $@392 createsymlinkopbody RIGHTBRACE_WCP  */
#line 1418 "configparser.yy"
                                                   { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8910 "configparser.cc"
    break;

  case 895: /* $@393: %empty  */
#line 1420 "configparser.yy"
                                          { std::dynamic_pointer_cast<CreateSymlinkOp>(ps.lconfig_fp1)->setSameDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8916 "configparser.cc"
    break;

  case 897: /* $@394: %empty  */
#line 1421 "configparser.yy"
                                           { std::dynamic_pointer_cast<CreateSymlinkOp>(ps.lconfig_fp1)->setLocal( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8922 "configparser.cc"
    break;

  case 899: /* $@395: %empty  */
#line 1422 "configparser.yy"
                                               { std::dynamic_pointer_cast<CreateSymlinkOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8928 "configparser.cc"
    break;

  case 902: /* $@396: %empty  */
#line 1425 "configparser.yy"
                                                  { ps.lconfig_fp1 = std::make_shared<ChangeSymlinkOp>(); }
#line 8934 "configparser.cc"
    break;

  case 903: /* changesymlinkop: CHANGESYMLINKOP_WCP LEFTBRACE_WCP $@396 changesymlinkopbody RIGHTBRACE_WCP  */
#line 1426 "configparser.yy"
                                                   { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8940 "configparser.cc"
    break;

  case 904: /* $@397: %empty  */
#line 1428 "configparser.yy"
                                           { std::dynamic_pointer_cast<ChangeSymlinkOp>(ps.lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_EDIT ); }
#line 8946 "configparser.cc"
    break;

  case 906: /* $@398: %empty  */
#line 1429 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeSymlinkOp>(ps.lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_MAKE_ABSOLUTE ); }
#line 8952 "configparser.cc"
    break;

  case 908: /* $@399: %empty  */
#line 1430 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeSymlinkOp>(ps.lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_MAKE_RELATIVE ); }
#line 8958 "configparser.cc"
    break;

  case 911: /* $@400: %empty  */
#line 1433 "configparser.yy"
                                  { ps.lconfig_fp1 = std::make_shared<ChModOp>(); }
#line 8964 "configparser.cc"
    break;

  case 912: /* chmodop: CHMODOP_WCP LEFTBRACE_WCP $@400 chmodopbody RIGHTBRACE_WCP  */
#line 1434 "configparser.yy"
                                   { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 8970 "configparser.cc"
    break;

  case 913: /* $@401: %empty  */
#line 1436 "configparser.yy"
                                  { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setOnFiles( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8976 "configparser.cc"
    break;

  case 915: /* $@402: %empty  */
#line 1437 "configparser.yy"
                                 { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setOnDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8982 "configparser.cc"
    break;

  case 917: /* $@403: %empty  */
#line 1438 "configparser.yy"
                                    { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8988 "configparser.cc"
    break;

  case 919: /* $@404: %empty  */
#line 1439 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8994 "configparser.cc"
    break;

  case 921: /* $@405: %empty  */
#line 1440 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_ASK_PERMISSIONS ); }
#line 9000 "configparser.cc"
    break;

  case 923: /* $@406: %empty  */
#line 1441 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_SET_PERMISSIONS ); }
#line 9006 "configparser.cc"
    break;

  case 925: /* $@407: %empty  */
#line 1442 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_ADD_PERMISSIONS ); }
#line 9012 "configparser.cc"
    break;

  case 927: /* $@408: %empty  */
#line 1443 "configparser.yy"
                                          { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_REMOVE_PERMISSIONS ); }
#line 9018 "configparser.cc"
    break;

  case 929: /* $@409: %empty  */
#line 1444 "configparser.yy"
                                           { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setPermissions( (yyvsp[-1].strptr) ); }
#line 9024 "configparser.cc"
    break;

  case 932: /* $@410: %empty  */
#line 1447 "configparser.yy"
                                    { ps.lconfig_fp1 = std::make_shared<ChTimeOp>(); }
#line 9030 "configparser.cc"
    break;

  case 933: /* chtimeop: CHTIMEOP_WCP LEFTBRACE_WCP $@410 chtimeopbody RIGHTBRACE_WCP  */
#line 1448 "configparser.yy"
                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9036 "configparser.cc"
    break;

  case 934: /* $@411: %empty  */
#line 1450 "configparser.yy"
                                   { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setOnFiles( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9042 "configparser.cc"
    break;

  case 936: /* $@412: %empty  */
#line 1451 "configparser.yy"
                                  { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setOnDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9048 "configparser.cc"
    break;

  case 938: /* $@413: %empty  */
#line 1452 "configparser.yy"
                                     { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9054 "configparser.cc"
    break;

  case 940: /* $@414: %empty  */
#line 1453 "configparser.yy"
                                        { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9060 "configparser.cc"
    break;

  case 943: /* $@415: %empty  */
#line 1456 "configparser.yy"
                                                        { ps.lconfig_fp1 = std::make_shared<ToggleListermodeOp>(); }
#line 9066 "configparser.cc"
    break;

  case 944: /* togglelistermodeop: TOGGLELISTERMODEOP_WCP LEFTBRACE_WCP $@415 togglelistermodeopbody RIGHTBRACE_WCP  */
#line 1457 "configparser.yy"
                                                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9072 "configparser.cc"
    break;

  case 945: /* $@416: %empty  */
#line 1459 "configparser.yy"
                                                { std::dynamic_pointer_cast<ToggleListermodeOp>(ps.lconfig_fp1)->setMode( Worker::getID4Name( (yyvsp[-1].strptr) ) ); }
#line 9078 "configparser.cc"
    break;

  case 948: /* $@417: %empty  */
#line 1462 "configparser.yy"
                                              { ps.lconfig_fp1 = std::make_shared<SetSortmodeOp>();
                                        std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setMode( 0 );
                                      }
#line 9086 "configparser.cc"
    break;

  case 949: /* setsortmodeop: SETSORTMODEOP_WCP LEFTBRACE_WCP $@417 setsortmodeopbody RIGHTBRACE_WCP  */
#line 1465 "configparser.yy"
                                               { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9092 "configparser.cc"
    break;

  case 950: /* $@418: %empty  */
#line 1467 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_NAME ); }
#line 9098 "configparser.cc"
    break;

  case 952: /* $@419: %empty  */
#line 1468 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_SIZE ); }
#line 9104 "configparser.cc"
    break;

  case 954: /* $@420: %empty  */
#line 1469 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_ACCTIME ); }
#line 9110 "configparser.cc"
    break;

  case 956: /* $@421: %empty  */
#line 1470 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_MODTIME ); }
#line 9116 "configparser.cc"
    break;

  case 958: /* $@422: %empty  */
#line 1471 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_CHGTIME ); }
#line 9122 "configparser.cc"
    break;

  case 960: /* $@423: %empty  */
#line 1472 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_TYPE ); }
#line 9128 "configparser.cc"
    break;

  case 962: /* $@424: %empty  */
#line 1473 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_OWNER ); }
#line 9134 "configparser.cc"
    break;

  case 964: /* $@425: %empty  */
#line 1474 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_INODE ); }
#line 9140 "configparser.cc"
    break;

  case 966: /* $@426: %empty  */
#line 1475 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_NLINK ); }
#line 9146 "configparser.cc"
    break;

  case 968: /* $@427: %empty  */
#line 1476 "configparser.yy"
                                                 { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_PERMISSION ); }
#line 9152 "configparser.cc"
    break;

  case 970: /* $@428: %empty  */
#line 1477 "configparser.yy"
                                                { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortFlag( SORT_REVERSE ); }
#line 9158 "configparser.cc"
    break;

  case 972: /* $@429: %empty  */
#line 1478 "configparser.yy"
                                                { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortFlag( SORT_DIRLAST ); }
#line 9164 "configparser.cc"
    break;

  case 974: /* $@430: %empty  */
#line 1479 "configparser.yy"
                                                 { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortFlag( SORT_DIRMIXED ); }
#line 9170 "configparser.cc"
    break;

  case 976: /* $@431: %empty  */
#line 1480 "configparser.yy"
                                                { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_EXTENSION ); }
#line 9176 "configparser.cc"
    break;

  case 978: /* $@432: %empty  */
#line 1481 "configparser.yy"
                                                      { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_CUSTOM_ATTRIBUTE ); }
#line 9182 "configparser.cc"
    break;

  case 981: /* $@433: %empty  */
#line 1484 "configparser.yy"
                                          { ps.lconfig_fp1 = std::make_shared<SetFilterOp>(); }
#line 9188 "configparser.cc"
    break;

  case 982: /* setfilterop: SETFILTEROP_WCP LEFTBRACE_WCP $@433 setfilteropbody RIGHTBRACE_WCP  */
#line 1485 "configparser.yy"
                                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9194 "configparser.cc"
    break;

  case 983: /* $@434: %empty  */
#line 1487 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9200 "configparser.cc"
    break;

  case 985: /* $@435: %empty  */
#line 1488 "configparser.yy"
                                          { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::EXCLUDE_FILTER ); }
#line 9206 "configparser.cc"
    break;

  case 987: /* $@436: %empty  */
#line 1489 "configparser.yy"
                                          { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::INCLUDE_FILTER ); }
#line 9212 "configparser.cc"
    break;

  case 989: /* $@437: %empty  */
#line 1490 "configparser.yy"
                                        { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::UNSET_FILTER ); }
#line 9218 "configparser.cc"
    break;

  case 991: /* $@438: %empty  */
#line 1491 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::UNSET_ALL ); }
#line 9224 "configparser.cc"
    break;

  case 993: /* $@439: %empty  */
#line 1492 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFilter( (yyvsp[-1].strptr) ); }
#line 9230 "configparser.cc"
    break;

  case 995: /* $@440: %empty  */
#line 1493 "configparser.yy"
                                                  { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkLabel( (yyvsp[-1].strptr) ); }
#line 9236 "configparser.cc"
    break;

  case 997: /* $@441: %empty  */
#line 1494 "configparser.yy"
                                                              { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ONLY_BOOKMARKS ); }
#line 9242 "configparser.cc"
    break;

  case 999: /* $@442: %empty  */
#line 1495 "configparser.yy"
                                                          { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ONLY_LABEL ); }
#line 9248 "configparser.cc"
    break;

  case 1001: /* $@443: %empty  */
#line 1496 "configparser.yy"
                                                    { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ALL ); }
#line 9254 "configparser.cc"
    break;

  case 1003: /* $@444: %empty  */
#line 1497 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setOptionMode( SetFilterOp::SET_OPTION ); }
#line 9260 "configparser.cc"
    break;

  case 1005: /* $@445: %empty  */
#line 1498 "configparser.yy"
                                               { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setOptionMode( SetFilterOp::INVERT_OPTION ); }
#line 9266 "configparser.cc"
    break;

  case 1007: /* $@446: %empty  */
#line 1499 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setChangeFilters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9272 "configparser.cc"
    break;

  case 1009: /* $@447: %empty  */
#line 1500 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setChangeBookmarks( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9278 "configparser.cc"
    break;

  case 1011: /* $@448: %empty  */
#line 1501 "configparser.yy"
                                         { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setQueryLabel( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9284 "configparser.cc"
    break;

  case 1014: /* $@449: %empty  */
#line 1504 "configparser.yy"
                                                        { ps.lconfig_fp1 = std::make_shared<ShortkeyFromListOp>(); }
#line 9290 "configparser.cc"
    break;

  case 1015: /* shortkeyfromlistop: SHORTKEYFROMLISTOP_WCP LEFTBRACE_WCP $@449 RIGHTBRACE_WCP  */
#line 1505 "configparser.yy"
                                  { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9296 "configparser.cc"
    break;

  case 1016: /* $@450: %empty  */
#line 1507 "configparser.yy"
                                  { ps.lconfig_fp1 = std::make_shared<ChOwnOp>(); }
#line 9302 "configparser.cc"
    break;

  case 1017: /* chownop: CHOWNOP_WCP LEFTBRACE_WCP $@450 chownopbody RIGHTBRACE_WCP  */
#line 1508 "configparser.yy"
                                   { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9308 "configparser.cc"
    break;

  case 1018: /* $@451: %empty  */
#line 1510 "configparser.yy"
                                  { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setOnFiles( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9314 "configparser.cc"
    break;

  case 1020: /* $@452: %empty  */
#line 1511 "configparser.yy"
                                 { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setOnDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9320 "configparser.cc"
    break;

  case 1022: /* $@453: %empty  */
#line 1512 "configparser.yy"
                                    { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9326 "configparser.cc"
    break;

  case 1024: /* $@454: %empty  */
#line 1513 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9332 "configparser.cc"
    break;

  case 1027: /* $@455: %empty  */
#line 1516 "configparser.yy"
                                    { ps.lconfig_fp1 = std::make_shared<ScriptOp>(); }
#line 9338 "configparser.cc"
    break;

  case 1028: /* scriptop: SCRIPTOP_WCP LEFTBRACE_WCP $@455 scriptopbody RIGHTBRACE_WCP  */
#line 1517 "configparser.yy"
                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9344 "configparser.cc"
    break;

  case 1029: /* $@456: %empty  */
#line 1519 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_NOP ); }
#line 9350 "configparser.cc"
    break;

  case 1031: /* $@457: %empty  */
#line 1520 "configparser.yy"
                                    { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_PUSH ); }
#line 9356 "configparser.cc"
    break;

  case 1033: /* $@458: %empty  */
#line 1521 "configparser.yy"
                                     { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_LABEL ); }
#line 9362 "configparser.cc"
    break;

  case 1035: /* $@459: %empty  */
#line 1522 "configparser.yy"
                                  { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_IF ); }
#line 9368 "configparser.cc"
    break;

  case 1037: /* $@460: %empty  */
#line 1523 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_END ); }
#line 9374 "configparser.cc"
    break;

  case 1039: /* $@461: %empty  */
#line 1524 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_POP ); }
#line 9380 "configparser.cc"
    break;

  case 1041: /* $@462: %empty  */
#line 1525 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_SETTINGS ); }
#line 9386 "configparser.cc"
    break;

  case 1043: /* $@463: %empty  */
#line 1526 "configparser.yy"
                                      { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_WINDOW ); }
#line 9392 "configparser.cc"
    break;

  case 1045: /* $@464: %empty  */
#line 1527 "configparser.yy"
                                    { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_GOTO ); }
#line 9398 "configparser.cc"
    break;

  case 1047: /* $@465: %empty  */
#line 1528 "configparser.yy"
                                           { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_EVALCOMMAND ); }
#line 9404 "configparser.cc"
    break;

  case 1049: /* $@466: %empty  */
#line 1529 "configparser.yy"
                                         { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setPushUseOutput( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9410 "configparser.cc"
    break;

  case 1051: /* $@467: %empty  */
#line 1530 "configparser.yy"
                                                { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setPushOutputReturnCode( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9416 "configparser.cc"
    break;

  case 1053: /* $@468: %empty  */
#line 1531 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setDoDebug( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9422 "configparser.cc"
    break;

  case 1055: /* $@469: %empty  */
#line 1532 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWPURecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9428 "configparser.cc"
    break;

  case 1057: /* $@470: %empty  */
#line 1533 "configparser.yy"
                                       { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWPUTakeDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9434 "configparser.cc"
    break;

  case 1059: /* $@471: %empty  */
#line 1534 "configparser.yy"
                                      { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setStackNr( (yyvsp[-1].num) ); }
#line 9440 "configparser.cc"
    break;

  case 1061: /* $@472: %empty  */
#line 1535 "configparser.yy"
                                       { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setLabel( (yyvsp[-1].strptr) ); }
#line 9446 "configparser.cc"
    break;

  case 1063: /* $@473: %empty  */
#line 1536 "configparser.yy"
                                            { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setPushString( (yyvsp[-1].strptr) ); }
#line 9452 "configparser.cc"
    break;

  case 1065: /* $@474: %empty  */
#line 1537 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setIfTest( (yyvsp[-1].strptr) ); }
#line 9458 "configparser.cc"
    break;

  case 1067: /* $@475: %empty  */
#line 1538 "configparser.yy"
                                         { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setIfLabel( (yyvsp[-1].strptr) ); }
#line 9464 "configparser.cc"
    break;

  case 1069: /* $@476: %empty  */
#line 1539 "configparser.yy"
                                       { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_OPEN ); }
#line 9470 "configparser.cc"
    break;

  case 1071: /* $@477: %empty  */
#line 1540 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_CLOSE ); }
#line 9476 "configparser.cc"
    break;

  case 1073: /* $@478: %empty  */
#line 1541 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_LEAVE ); }
#line 9482 "configparser.cc"
    break;

  case 1075: /* $@479: %empty  */
#line 1542 "configparser.yy"
                                          { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setChangeProgress( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9488 "configparser.cc"
    break;

  case 1077: /* $@480: %empty  */
#line 1543 "configparser.yy"
                                      { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setChangeText( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9494 "configparser.cc"
    break;

  case 1079: /* $@481: %empty  */
#line 1544 "configparser.yy"
                                             { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setProgressUseOutput( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9500 "configparser.cc"
    break;

  case 1081: /* $@482: %empty  */
#line 1545 "configparser.yy"
                                            { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinTextUseOutput( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9506 "configparser.cc"
    break;

  case 1083: /* $@483: %empty  */
#line 1546 "configparser.yy"
                                          { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setProgress( (yyvsp[-1].strptr) ); }
#line 9512 "configparser.cc"
    break;

  case 1085: /* $@484: %empty  */
#line 1547 "configparser.yy"
                                         { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinText( (yyvsp[-1].strptr) ); }
#line 9518 "configparser.cc"
    break;

  case 1087: /* $@485: %empty  */
#line 1548 "configparser.yy"
                                               { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setCommandStr( (yyvsp[-1].strptr) ); }
#line 9524 "configparser.cc"
    break;

  case 1090: /* $@486: %empty  */
#line 1551 "configparser.yy"
                                                { ps.lconfig_fp1 = std::make_shared<ShowDirCacheOp>(); }
#line 9530 "configparser.cc"
    break;

  case 1091: /* showdircacheop: SHOWDIRCACHEOP_WCP LEFTBRACE_WCP $@486 RIGHTBRACE_WCP  */
#line 1552 "configparser.yy"
                              { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9536 "configparser.cc"
    break;

  case 1092: /* $@487: %empty  */
#line 1554 "configparser.yy"
                                                { ps.lconfig_fp1 = std::make_shared<ParentActionOp>(); }
#line 9542 "configparser.cc"
    break;

  case 1093: /* parentactionop: PARENTACTIONOP_WCP LEFTBRACE_WCP $@487 RIGHTBRACE_WCP  */
#line 1555 "configparser.yy"
                              { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9548 "configparser.cc"
    break;

  case 1094: /* $@488: %empty  */
#line 1557 "configparser.yy"
                                              { ps.lconfig_fp1 = std::make_shared<NoOperationOp>(); }
#line 9554 "configparser.cc"
    break;

  case 1095: /* nooperationop: NOOPERATIONOP_WCP LEFTBRACE_WCP $@488 RIGHTBRACE_WCP  */
#line 1558 "configparser.yy"
                             { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9560 "configparser.cc"
    break;

  case 1096: /* $@489: %empty  */
#line 1560 "configparser.yy"
                                  { ps.lconfig_fp1 = std::make_shared<GoFTPOp>(); }
#line 9566 "configparser.cc"
    break;

  case 1097: /* goftpop: GOFTPOP_WCP LEFTBRACE_WCP $@489 goftpopbody RIGHTBRACE_WCP  */
#line 1561 "configparser.yy"
                                   { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9572 "configparser.cc"
    break;

  case 1098: /* $@490: %empty  */
#line 1563 "configparser.yy"
                                       { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9578 "configparser.cc"
    break;

  case 1100: /* $@491: %empty  */
#line 1564 "configparser.yy"
                                         { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setHost( (yyvsp[-1].strptr) ); }
#line 9584 "configparser.cc"
    break;

  case 1102: /* $@492: %empty  */
#line 1565 "configparser.yy"
                                         { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setUser( (yyvsp[-1].strptr) ); }
#line 9590 "configparser.cc"
    break;

  case 1104: /* $@493: %empty  */
#line 1566 "configparser.yy"
                                         { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setPass( (yyvsp[-1].strptr) ); }
#line 9596 "configparser.cc"
    break;

  case 1106: /* $@494: %empty  */
#line 1567 "configparser.yy"
                                       { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setDontEnterFTP( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9602 "configparser.cc"
    break;

  case 1108: /* $@495: %empty  */
#line 1568 "configparser.yy"
                                        { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setAlwaysStorePW( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9608 "configparser.cc"
    break;

  case 1110: /* $@496: %empty  */
#line 1569 "configparser.yy"
                                           { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setAVFSModule( (yyvsp[-1].strptr) ); }
#line 9614 "configparser.cc"
    break;

  case 1113: /* $@497: %empty  */
#line 1572 "configparser.yy"
                                                { ps.lconfig_fp1 = std::make_shared<InternalViewOp>(); }
#line 9620 "configparser.cc"
    break;

  case 1114: /* internalviewop: INTERNALVIEWOP_WCP LEFTBRACE_WCP $@497 internalviewopbody RIGHTBRACE_WCP  */
#line 1573 "configparser.yy"
                                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9626 "configparser.cc"
    break;

  case 1115: /* $@498: %empty  */
#line 1575 "configparser.yy"
                                              { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9632 "configparser.cc"
    break;

  case 1117: /* $@499: %empty  */
#line 1576 "configparser.yy"
                                            { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setCustomFiles( (yyvsp[-1].strptr) ); }
#line 9638 "configparser.cc"
    break;

  case 1119: /* $@500: %empty  */
#line 1577 "configparser.yy"
                                         { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setShowFileMode( InternalViewOp::SHOW_ACTIVE_FILE ); }
#line 9644 "configparser.cc"
    break;

  case 1121: /* $@501: %empty  */
#line 1578 "configparser.yy"
                                         { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setShowFileMode( InternalViewOp::SHOW_CUSTOM_FILES ); }
#line 9650 "configparser.cc"
    break;

  case 1124: /* $@502: %empty  */
#line 1582 "configparser.yy"
                                          { ps.lconfig_fp1 = std::make_shared<ClipboardOp>(); }
#line 9656 "configparser.cc"
    break;

  case 1125: /* clipboardop: CLIPBOARDOP_WCP LEFTBRACE_WCP $@502 clipboardopbody RIGHTBRACE_WCP  */
#line 1583 "configparser.yy"
                                           { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9662 "configparser.cc"
    break;

  case 1126: /* $@503: %empty  */
#line 1585 "configparser.yy"
                                                    { std::dynamic_pointer_cast<ClipboardOp>(ps.lconfig_fp1)->setClipboardString( (yyvsp[-1].strptr) ); }
#line 9668 "configparser.cc"
    break;

  case 1129: /* $@504: %empty  */
#line 1588 "configparser.yy"
                                    { ps.lconfig_fp1 = std::make_shared<SearchOp>(); }
#line 9674 "configparser.cc"
    break;

  case 1130: /* searchop: SEARCHOP_WCP LEFTBRACE_WCP $@504 searchopbody RIGHTBRACE_WCP  */
#line 1589 "configparser.yy"
                                     { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9680 "configparser.cc"
    break;

  case 1131: /* $@505: %empty  */
#line 1591 "configparser.yy"
                                             { std::dynamic_pointer_cast<SearchOp>(ps.lconfig_fp1)->setEditCommand( (yyvsp[-1].strptr) ); }
#line 9686 "configparser.cc"
    break;

  case 1133: /* $@506: %empty  */
#line 1592 "configparser.yy"
                                           { std::dynamic_pointer_cast<SearchOp>(ps.lconfig_fp1)->setShowPrevResults( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9692 "configparser.cc"
    break;

  case 1135: /* $@507: %empty  */
#line 1593 "configparser.yy"
                                              { std::dynamic_pointer_cast<SearchOp>(ps.lconfig_fp1)->setUseSelectedEntries( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9698 "configparser.cc"
    break;

  case 1138: /* $@508: %empty  */
#line 1596 "configparser.yy"
                                              { ps.lconfig_fp1 = std::make_shared<DirBookmarkOp>(); }
#line 9704 "configparser.cc"
    break;

  case 1139: /* dirbookmarkop: DIRBOOKMARKOP_WCP LEFTBRACE_WCP $@508 RIGHTBRACE_WCP  */
#line 1597 "configparser.yy"
                             { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9710 "configparser.cc"
    break;

  case 1140: /* $@509: %empty  */
#line 1599 "configparser.yy"
                                                      { ps.lconfig_fp1 = std::make_shared<OpenContextMenuOp>(); }
#line 9716 "configparser.cc"
    break;

  case 1141: /* opencontextmenuop: OPENCONTEXTMENUOP_WCP LEFTBRACE_WCP $@509 opencontextmenuopbody RIGHTBRACE_WCP  */
#line 1600 "configparser.yy"
                                                       { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9722 "configparser.cc"
    break;

  case 1142: /* $@510: %empty  */
#line 1602 "configparser.yy"
                                                          { std::dynamic_pointer_cast<OpenContextMenuOp>(ps.lconfig_fp1)->setHighlightUserAction( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9728 "configparser.cc"
    break;

  case 1145: /* $@511: %empty  */
#line 1605 "configparser.yy"
                                                  { ps.lconfig_fp1 = std::make_shared<RunCustomAction>(); }
#line 9734 "configparser.cc"
    break;

  case 1146: /* runcustomaction: RUNCUSTOMACTION_WCP LEFTBRACE_WCP $@511 runcustomactionbody RIGHTBRACE_WCP  */
#line 1606 "configparser.yy"
                                                   { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9740 "configparser.cc"
    break;

  case 1147: /* $@512: %empty  */
#line 1608 "configparser.yy"
                                                   { std::dynamic_pointer_cast<RunCustomAction>(ps.lconfig_fp1)->setCustomName( (yyvsp[-1].strptr) ); }
#line 9746 "configparser.cc"
    break;

  case 1150: /* $@513: %empty  */
#line 1611 "configparser.yy"
                                                    { ps.lconfig_fp1 = std::make_shared<OpenWorkerMenuOp>(); }
#line 9752 "configparser.cc"
    break;

  case 1151: /* openworkermenuop: OPENWORKERMENUOP_WCP LEFTBRACE_WCP $@513 RIGHTBRACE_WCP  */
#line 1612 "configparser.yy"
                                { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9758 "configparser.cc"
    break;

  case 1152: /* $@514: %empty  */
#line 1614 "configparser.yy"
                                              { ps.lconfig_fp1 = std::make_shared<ChangeLabelOp>(); }
#line 9764 "configparser.cc"
    break;

  case 1153: /* changelabelop: CHANGELABELOP_WCP LEFTBRACE_WCP $@514 changelabelopbody RIGHTBRACE_WCP  */
#line 1615 "configparser.yy"
                                               { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9770 "configparser.cc"
    break;

  case 1154: /* $@515: %empty  */
#line 1617 "configparser.yy"
                                            { std::dynamic_pointer_cast<ChangeLabelOp>(ps.lconfig_fp1)->setAskForLabel( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9776 "configparser.cc"
    break;

  case 1156: /* $@516: %empty  */
#line 1618 "configparser.yy"
                                            { std::dynamic_pointer_cast<ChangeLabelOp>(ps.lconfig_fp1)->setLabel( (yyvsp[-1].strptr) ); }
#line 9782 "configparser.cc"
    break;

  case 1159: /* $@517: %empty  */
#line 1621 "configparser.yy"
                                            { ps.lconfig_fp1 = std::make_shared<ModifyTabsOp>(); }
#line 9788 "configparser.cc"
    break;

  case 1160: /* modifytabsop: MODIFYTABSOP_WCP LEFTBRACE_WCP $@517 modifytabsopbody RIGHTBRACE_WCP  */
#line 1622 "configparser.yy"
                                             { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 9794 "configparser.cc"
    break;

  case 1161: /* $@518: %empty  */
#line 1624 "configparser.yy"
                                               { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::NEW_TAB ); }
#line 9800 "configparser.cc"
    break;

  case 1163: /* $@519: %empty  */
#line 1625 "configparser.yy"
                                                        { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::CLOSE_CURRENT_TAB ); }
#line 9806 "configparser.cc"
    break;

  case 1165: /* $@520: %empty  */
#line 1626 "configparser.yy"
                                                { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::NEXT_TAB ); }
#line 9812 "configparser.cc"
    break;

  case 1167: /* $@521: %empty  */
#line 1627 "configparser.yy"
                                                { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::PREV_TAB ); }
#line 9818 "configparser.cc"
    break;

  case 1169: /* $@522: %empty  */
#line 1628 "configparser.yy"
                                                        { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::TOGGLE_LOCK_STATE ); }
#line 9824 "configparser.cc"
    break;

  case 1171: /* $@523: %empty  */
#line 1629 "configparser.yy"
                                                    { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::MOVE_TAB_LEFT ); }
#line 9830 "configparser.cc"
    break;

  case 1173: /* $@524: %empty  */
#line 1630 "configparser.yy"
                                                     { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::MOVE_TAB_RIGHT ); }
#line 9836 "configparser.cc"
    break;

  case 1176: /* $@525: %empty  */
#line 1633 "configparser.yy"
                                                {
                 ps.lconfig_fp1 = std::make_shared<ChangeLayoutOp>();
                 layout_sets = LayoutSettings();
             }
#line 9845 "configparser.cc"
    break;

  case 1177: /* changelayoutop: CHANGELAYOUTOP_WCP LEFTBRACE_WCP $@525 changelayoutopbody RIGHTBRACE_WCP  */
#line 1636 "configparser.yy"
                                                 {
                 std::dynamic_pointer_cast<ChangeLayoutOp>(ps.lconfig_fp1)->setLayout( layout_sets );
                 lconfig_listcom.push_back( ps.lconfig_fp1 );
             }
#line 9854 "configparser.cc"
    break;

  case 1178: /* $@526: %empty  */
#line 1641 "configparser.yy"
                                   { layout_sets.pushBackOrder( LayoutSettings::LO_STATEBAR ); }
#line 9860 "configparser.cc"
    break;

  case 1180: /* $@527: %empty  */
#line 1642 "configparser.yy"
                                   { layout_sets.pushBackOrder( LayoutSettings::LO_CLOCKBAR ); }
#line 9866 "configparser.cc"
    break;

  case 1182: /* $@528: %empty  */
#line 1643 "configparser.yy"
                                  { layout_sets.pushBackOrder( LayoutSettings::LO_BUTTONS ); }
#line 9872 "configparser.cc"
    break;

  case 1184: /* $@529: %empty  */
#line 1644 "configparser.yy"
                                    { layout_sets.pushBackOrder( LayoutSettings::LO_LISTVIEWS ); }
#line 9878 "configparser.cc"
    break;

  case 1186: /* $@530: %empty  */
#line 1645 "configparser.yy"
                              { layout_sets.pushBackOrder( LayoutSettings::LO_BLL ); }
#line 9884 "configparser.cc"
    break;

  case 1188: /* $@531: %empty  */
#line 1646 "configparser.yy"
                              { layout_sets.pushBackOrder( LayoutSettings::LO_LBL ); }
#line 9890 "configparser.cc"
    break;

  case 1190: /* $@532: %empty  */
#line 1647 "configparser.yy"
                              { layout_sets.pushBackOrder( LayoutSettings::LO_LLB ); }
#line 9896 "configparser.cc"
    break;

  case 1192: /* $@533: %empty  */
#line 1648 "configparser.yy"
                             { layout_sets.pushBackOrder( LayoutSettings::LO_BL ); }
#line 9902 "configparser.cc"
    break;

  case 1194: /* $@534: %empty  */
#line 1649 "configparser.yy"
                             { layout_sets.pushBackOrder( LayoutSettings::LO_LB ); }
#line 9908 "configparser.cc"
    break;

  case 1196: /* $@535: %empty  */
#line 1650 "configparser.yy"
                                            { layout_sets.setButtonVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9914 "configparser.cc"
    break;

  case 1198: /* $@536: %empty  */
#line 1651 "configparser.yy"
                                              { layout_sets.setListViewVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9920 "configparser.cc"
    break;

  case 1200: /* $@537: %empty  */
#line 1652 "configparser.yy"
                                                  { layout_sets.setListViewWeight( (yyvsp[-1].num) ); }
#line 9926 "configparser.cc"
    break;

  case 1202: /* $@538: %empty  */
#line 1653 "configparser.yy"
                                               { layout_sets.setWeightRelToActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9932 "configparser.cc"
    break;

  case 1205: /* $@539: %empty  */
#line 1656 "configparser.yy"
                                                  {
                  ps.lconfig_fp1 = std::make_shared<ChangeColumnsOp>();
                  columns.clear();
                }
#line 9941 "configparser.cc"
    break;

  case 1206: /* changecolumnsop: CHANGECOLUMNSOP_WCP LEFTBRACE_WCP $@539 changecolumnsopbody RIGHTBRACE_WCP  */
#line 1659 "configparser.yy"
                                                     {
                  std::dynamic_pointer_cast<ChangeColumnsOp>(ps.lconfig_fp1)->setColumns( columns );
                  lconfig_listcom.push_back( ps.lconfig_fp1 );
                }
#line 9950 "configparser.cc"
    break;

  case 1207: /* $@540: %empty  */
#line 1664 "configparser.yy"
                               { columns.push_back( WorkerTypes::LISTCOL_NAME ); }
#line 9956 "configparser.cc"
    break;

  case 1209: /* $@541: %empty  */
#line 1665 "configparser.yy"
                               { columns.push_back( WorkerTypes::LISTCOL_SIZE ); }
#line 9962 "configparser.cc"
    break;

  case 1211: /* $@542: %empty  */
#line 1666 "configparser.yy"
                               { columns.push_back( WorkerTypes::LISTCOL_TYPE ); }
#line 9968 "configparser.cc"
    break;

  case 1213: /* $@543: %empty  */
#line 1667 "configparser.yy"
                                     { columns.push_back( WorkerTypes::LISTCOL_PERM ); }
#line 9974 "configparser.cc"
    break;

  case 1215: /* $@544: %empty  */
#line 1668 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_OWNER ); }
#line 9980 "configparser.cc"
    break;

  case 1217: /* $@545: %empty  */
#line 1669 "configparser.yy"
                                      { columns.push_back( WorkerTypes::LISTCOL_DEST ); }
#line 9986 "configparser.cc"
    break;

  case 1219: /* $@546: %empty  */
#line 1670 "configparser.yy"
                                  { columns.push_back( WorkerTypes::LISTCOL_MOD ); }
#line 9992 "configparser.cc"
    break;

  case 1221: /* $@547: %empty  */
#line 1671 "configparser.yy"
                                  { columns.push_back( WorkerTypes::LISTCOL_ACC ); }
#line 9998 "configparser.cc"
    break;

  case 1223: /* $@548: %empty  */
#line 1672 "configparser.yy"
                                  { columns.push_back( WorkerTypes::LISTCOL_CHANGE ); }
#line 10004 "configparser.cc"
    break;

  case 1225: /* $@549: %empty  */
#line 1673 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_INODE ); }
#line 10010 "configparser.cc"
    break;

  case 1227: /* $@550: %empty  */
#line 1674 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_NLINK ); }
#line 10016 "configparser.cc"
    break;

  case 1229: /* $@551: %empty  */
#line 1675 "configparser.yy"
                                 { columns.push_back( WorkerTypes::LISTCOL_BLOCKS ); }
#line 10022 "configparser.cc"
    break;

  case 1231: /* $@552: %empty  */
#line 1676 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_SIZEH ); }
#line 10028 "configparser.cc"
    break;

  case 1233: /* $@553: %empty  */
#line 1677 "configparser.yy"
                                    { columns.push_back( WorkerTypes::LISTCOL_EXTENSION ); }
#line 10034 "configparser.cc"
    break;

  case 1235: /* $@554: %empty  */
#line 1678 "configparser.yy"
                                          { columns.push_back( WorkerTypes::LISTCOL_CUSTOM_ATTR ); }
#line 10040 "configparser.cc"
    break;

  case 1238: /* $@555: %empty  */
#line 1681 "configparser.yy"
                                                  { ps.lconfig_fp1 = std::make_shared<VolumeManagerOp>(); }
#line 10046 "configparser.cc"
    break;

  case 1239: /* volumemanagerop: VOLUMEMANAGEROP_WCP LEFTBRACE_WCP $@555 RIGHTBRACE_WCP  */
#line 1682 "configparser.yy"
                               { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10052 "configparser.cc"
    break;

  case 1240: /* $@556: %empty  */
#line 1684 "configparser.yy"
                                                        { ps.lconfig_fp1 = std::make_shared<SwitchButtonBankOp>(); }
#line 10058 "configparser.cc"
    break;

  case 1241: /* switchbuttonbankop: SWITCHBUTTONBANKOP_WCP LEFTBRACE_WCP $@556 switchbuttonbankopbody RIGHTBRACE_WCP  */
#line 1685 "configparser.yy"
                                                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10064 "configparser.cc"
    break;

  case 1242: /* $@557: %empty  */
#line 1687 "configparser.yy"
                                                          { std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_NEXT_BANK ); }
#line 10070 "configparser.cc"
    break;

  case 1244: /* $@558: %empty  */
#line 1688 "configparser.yy"
                                                          { std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_PREV_BANK ); }
#line 10076 "configparser.cc"
    break;

  case 1246: /* $@559: %empty  */
#line 1689 "configparser.yy"
                                                        { std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_BANK_NR ); }
#line 10082 "configparser.cc"
    break;

  case 1248: /* $@560: %empty  */
#line 1690 "configparser.yy"
                                              { std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setBankNr( (yyvsp[-1].num) ); }
#line 10088 "configparser.cc"
    break;

  case 1251: /* $@561: %empty  */
#line 1693 "configparser.yy"
                                        { ps.lconfig_fp1 = std::make_shared<PathJumpOp>(); }
#line 10094 "configparser.cc"
    break;

  case 1252: /* pathjumpop: PATHJUMPOP_WCP LEFTBRACE_WCP $@561 pathjumpopbody RIGHTBRACE_WCP  */
#line 1694 "configparser.yy"
                                         { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10100 "configparser.cc"
    break;

  case 1253: /* $@562: %empty  */
#line 1696 "configparser.yy"
                                                  { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_TIME ); }
#line 10106 "configparser.cc"
    break;

  case 1255: /* $@563: %empty  */
#line 1697 "configparser.yy"
                                                    { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_FILTER ); }
#line 10112 "configparser.cc"
    break;

  case 1257: /* $@564: %empty  */
#line 1698 "configparser.yy"
                                                     { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_PROGRAM ); }
#line 10118 "configparser.cc"
    break;

  case 1259: /* $@565: %empty  */
#line 1699 "configparser.yy"
                                                { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_ALL ); }
#line 10124 "configparser.cc"
    break;

  case 1261: /* $@566: %empty  */
#line 1700 "configparser.yy"
                                                    { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_SUBDIRS ); }
#line 10130 "configparser.cc"
    break;

  case 1263: /* $@567: %empty  */
#line 1701 "configparser.yy"
                                                          { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_DIRECT_SUBDIRS ); }
#line 10136 "configparser.cc"
    break;

  case 1265: /* $@568: %empty  */
#line 1702 "configparser.yy"
                                        { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setIncludeAllData( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10142 "configparser.cc"
    break;

  case 1267: /* $@569: %empty  */
#line 1703 "configparser.yy"
                                             { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setHideNonExisting( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10148 "configparser.cc"
    break;

  case 1269: /* $@570: %empty  */
#line 1704 "configparser.yy"
                                              { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setLockOnCurrentDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10154 "configparser.cc"
    break;

  case 1272: /* $@571: %empty  */
#line 1707 "configparser.yy"
                                              { ps.lconfig_fp1 = std::make_shared<CommandMenuOp>(); }
#line 10160 "configparser.cc"
    break;

  case 1273: /* commandmenuop: COMMANDMENUOP_WCP LEFTBRACE_WCP $@571 commandmenuopbody RIGHTBRACE_WCP  */
#line 1708 "configparser.yy"
                                               { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10166 "configparser.cc"
    break;

  case 1274: /* $@572: %empty  */
#line 1710 "configparser.yy"
                                           { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setShowRecentlyUsed( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10172 "configparser.cc"
    break;

  case 1276: /* $@573: %empty  */
#line 1711 "configparser.yy"
                                               { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::MENUS ); }
#line 10178 "configparser.cc"
    break;

  case 1278: /* $@574: %empty  */
#line 1712 "configparser.yy"
                                                 { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::BUTTONS ); }
#line 10184 "configparser.cc"
    break;

  case 1280: /* $@575: %empty  */
#line 1713 "configparser.yy"
                                                 { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::HOTKEYS ); }
#line 10190 "configparser.cc"
    break;

  case 1282: /* $@576: %empty  */
#line 1714 "configparser.yy"
                                               { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::PATHS ); }
#line 10196 "configparser.cc"
    break;

  case 1284: /* $@577: %empty  */
#line 1715 "configparser.yy"
                                                 { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::LV_MODES ); }
#line 10202 "configparser.cc"
    break;

  case 1286: /* $@578: %empty  */
#line 1716 "configparser.yy"
                                                    { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::LV_COMMANDS ); }
#line 10208 "configparser.cc"
    break;

  case 1288: /* $@579: %empty  */
#line 1717 "configparser.yy"
                                                  { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::COMMANDS ); }
#line 10214 "configparser.cc"
    break;

  case 1290: /* $@580: %empty  */
#line 1718 "configparser.yy"
                                                  { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::TOP_LEVEL ); }
#line 10220 "configparser.cc"
    break;

  case 1293: /* $@581: %empty  */
#line 1721 "configparser.yy"
                                                      { ps.lconfig_fp1 = std::make_shared<ViewNewestFilesOp>(); }
#line 10226 "configparser.cc"
    break;

  case 1294: /* viewnewestfilesop: VIEWNEWESTFILESOP_WCP LEFTBRACE_WCP $@581 RIGHTBRACE_WCP  */
#line 1722 "configparser.yy"
                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10232 "configparser.cc"
    break;

  case 1295: /* $@582: %empty  */
#line 1724 "configparser.yy"
                                            { ps.lconfig_fp1 = std::make_shared<DirCompareOp>(); }
#line 10238 "configparser.cc"
    break;

  case 1296: /* dircompareop: DIRCOMPAREOP_WCP LEFTBRACE_WCP $@582 RIGHTBRACE_WCP  */
#line 1725 "configparser.yy"
                            { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10244 "configparser.cc"
    break;

  case 1297: /* $@583: %empty  */
#line 1727 "configparser.yy"
                                              { ps.lconfig_fp1 = std::make_shared<TabProfilesOp>(); }
#line 10250 "configparser.cc"
    break;

  case 1298: /* tabprofilesop: TABPROFILESOP_WCP LEFTBRACE_WCP $@583 RIGHTBRACE_WCP  */
#line 1728 "configparser.yy"
                             { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10256 "configparser.cc"
    break;

  case 1299: /* $@584: %empty  */
#line 1730 "configparser.yy"
                                                { ps.lconfig_fp1 = std::make_shared<ExternalVDirOp>(); }
#line 10262 "configparser.cc"
    break;

  case 1300: /* externalvdirop: EXTERNALVDIROP_WCP LEFTBRACE_WCP $@584 externalvdiropbody RIGHTBRACE_WCP  */
#line 1731 "configparser.yy"
                                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10268 "configparser.cc"
    break;

  case 1301: /* $@585: %empty  */
#line 1733 "configparser.yy"
                                                     { std::dynamic_pointer_cast<ExternalVDirOp>(ps.lconfig_fp1)->setCommandString( (yyvsp[-1].strptr) ); }
#line 10274 "configparser.cc"
    break;

  case 1303: /* $@586: %empty  */
#line 1734 "configparser.yy"
                                              { std::dynamic_pointer_cast<ExternalVDirOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10280 "configparser.cc"
    break;

  case 1306: /* $@587: %empty  */
#line 1737 "configparser.yy"
                                              { auto tfp = std::make_shared< ScriptOp >();
                                                tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                                tfp->setCommandStr( "open_current_tab_menu" );
                                                ps.lconfig_fp1 = tfp;
                                              }
#line 10290 "configparser.cc"
    break;

  case 1307: /* opentabmenuop: OPENTABMENUOP_WCP LEFTBRACE_WCP $@587 RIGHTBRACE_WCP  */
#line 1742 "configparser.yy"
                             { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10296 "configparser.cc"
    break;

  case 1308: /* $@588: %empty  */
#line 1744 "configparser.yy"
                                                    { dir_preset = directory_presets(); }
#line 10302 "configparser.cc"
    break;

  case 1309: /* $@589: %empty  */
#line 1745 "configparser.yy"
                                                 { dir_presets[path] = dir_preset;
                                                 }
#line 10309 "configparser.cc"
    break;

  case 1312: /* $@590: %empty  */
#line 1749 "configparser.yy"
                                          { path  = (yyvsp[-1].strptr); }
#line 10315 "configparser.cc"
    break;

  case 1314: /* $@591: %empty  */
#line 1750 "configparser.yy"
                                          { dir_preset.sortmode = SORT_NAME; }
#line 10321 "configparser.cc"
    break;

  case 1316: /* $@592: %empty  */
#line 1751 "configparser.yy"
                                          { dir_preset.sortmode = SORT_SIZE; }
#line 10327 "configparser.cc"
    break;

  case 1318: /* $@593: %empty  */
#line 1752 "configparser.yy"
                                             { dir_preset.sortmode = SORT_ACCTIME; }
#line 10333 "configparser.cc"
    break;

  case 1320: /* $@594: %empty  */
#line 1753 "configparser.yy"
                                             { dir_preset.sortmode = SORT_MODTIME; }
#line 10339 "configparser.cc"
    break;

  case 1322: /* $@595: %empty  */
#line 1754 "configparser.yy"
                                             { dir_preset.sortmode = SORT_CHGTIME; }
#line 10345 "configparser.cc"
    break;

  case 1324: /* $@596: %empty  */
#line 1755 "configparser.yy"
                                          { dir_preset.sortmode = SORT_TYPE; }
#line 10351 "configparser.cc"
    break;

  case 1326: /* $@597: %empty  */
#line 1756 "configparser.yy"
                                           { dir_preset.sortmode = SORT_OWNER; }
#line 10357 "configparser.cc"
    break;

  case 1328: /* $@598: %empty  */
#line 1757 "configparser.yy"
                                           { dir_preset.sortmode = SORT_INODE; }
#line 10363 "configparser.cc"
    break;

  case 1330: /* $@599: %empty  */
#line 1758 "configparser.yy"
                                           { dir_preset.sortmode = SORT_NLINK; }
#line 10369 "configparser.cc"
    break;

  case 1332: /* $@600: %empty  */
#line 1759 "configparser.yy"
                                                { dir_preset.sortmode = SORT_PERMISSION; }
#line 10375 "configparser.cc"
    break;

  case 1334: /* $@601: %empty  */
#line 1760 "configparser.yy"
                                               { dir_preset.sortmode |= SORT_REVERSE; }
#line 10381 "configparser.cc"
    break;

  case 1336: /* $@602: %empty  */
#line 1761 "configparser.yy"
                                               { dir_preset.sortmode |= SORT_DIRLAST; }
#line 10387 "configparser.cc"
    break;

  case 1338: /* $@603: %empty  */
#line 1762 "configparser.yy"
                                                { dir_preset.sortmode |= SORT_DIRMIXED; }
#line 10393 "configparser.cc"
    break;

  case 1340: /* $@604: %empty  */
#line 1763 "configparser.yy"
                                               { dir_preset.sortmode = SORT_EXTENSION; }
#line 10399 "configparser.cc"
    break;

  case 1342: /* $@605: %empty  */
#line 1764 "configparser.yy"
                                                     { dir_preset.sortmode = SORT_CUSTOM_ATTRIBUTE; }
#line 10405 "configparser.cc"
    break;

  case 1344: /* $@606: %empty  */
#line 1765 "configparser.yy"
                                               { dir_preset.show_hidden = true; }
#line 10411 "configparser.cc"
    break;

  case 1346: /* $@607: %empty  */
#line 1766 "configparser.yy"
                                               { dir_preset.show_hidden = false; }
#line 10417 "configparser.cc"
    break;

  case 1348: /* $@608: %empty  */
#line 1767 "configparser.yy"
                                          { dir_preset_filter = NM_Filter(); }
#line 10423 "configparser.cc"
    break;

  case 1349: /* $@609: %empty  */
#line 1768 "configparser.yy"
                                                       { dir_preset.filters.push_back( dir_preset_filter ); }
#line 10429 "configparser.cc"
    break;

  case 1352: /* $@610: %empty  */
#line 1771 "configparser.yy"
                                                    { dir_preset_filter.setPattern( (yyvsp[-1].strptr) ); }
#line 10435 "configparser.cc"
    break;

  case 1354: /* $@611: %empty  */
#line 1772 "configparser.yy"
                                                  { dir_preset_filter.setCheck( NM_Filter::EXCLUDE ); }
#line 10441 "configparser.cc"
    break;

  case 1356: /* $@612: %empty  */
#line 1773 "configparser.yy"
                                                  { dir_preset_filter.setCheck( NM_Filter::INCLUDE ); }
#line 10447 "configparser.cc"
    break;

  case 1359: /* $@613: %empty  */
#line 1776 "configparser.yy"
                                { auto tfp = std::make_shared< HelpOp >();
                                  ps.lconfig_fp1 = tfp;
                                }
#line 10455 "configparser.cc"
    break;

  case 1360: /* helpop: HELPOP_WCP LEFTBRACE_WCP $@613 RIGHTBRACE_WCP  */
#line 1779 "configparser.yy"
                      { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10461 "configparser.cc"
    break;

  case 1361: /* $@614: %empty  */
#line 1781 "configparser.yy"
                                                    { auto tfp = std::make_shared< ViewCommandLogOp >();
                                                      ps.lconfig_fp1 = tfp;
                                                    }
#line 10469 "configparser.cc"
    break;

  case 1362: /* viewcommandlogop: VIEWCOMMANDLOGOP_WCP LEFTBRACE_WCP $@614 RIGHTBRACE_WCP  */
#line 1784 "configparser.yy"
                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10475 "configparser.cc"
    break;

  case 1363: /* $@615: %empty  */
#line 1786 "configparser.yy"
                                                                { ps.lconfig_fp1 = std::make_shared<ActivateTextViewModeOp>(); }
#line 10481 "configparser.cc"
    break;

  case 1364: /* activatetextviewmodeop: ACTIVATETEXTVIEWMODEOP_WCP LEFTBRACE_WCP $@615 activatetextviewmodeopbody RIGHTBRACE_WCP  */
#line 1787 "configparser.yy"
                                                                 { lconfig_listcom.push_back( ps.lconfig_fp1 ); }
#line 10487 "configparser.cc"
    break;

  case 1365: /* $@616: %empty  */
#line 1789 "configparser.yy"
                                                    { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setActiveSide( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10493 "configparser.cc"
    break;

  case 1367: /* $@617: %empty  */
#line 1790 "configparser.yy"
                                                              { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setSourceMode( TextViewMode::ACTIVE_FILE_OTHER_SIDE ); }
#line 10499 "configparser.cc"
    break;

  case 1369: /* $@618: %empty  */
#line 1791 "configparser.yy"
                                                                 { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setSourceMode( TextViewMode::COMMAND_STR ); }
#line 10505 "configparser.cc"
    break;

  case 1371: /* $@619: %empty  */
#line 1792 "configparser.yy"
                                                                  { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setSourceMode( TextViewMode::FILE_TYPE_ACTION ); }
#line 10511 "configparser.cc"
    break;

  case 1373: /* $@620: %empty  */
#line 1793 "configparser.yy"
                                                             { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setCommandStr( (yyvsp[-1].strptr) ); }
#line 10517 "configparser.cc"
    break;

  case 1375: /* $@621: %empty  */
#line 1794 "configparser.yy"
                                                              { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setFileTypeAction( (yyvsp[-1].strptr) ); }
#line 10523 "configparser.cc"
    break;

  case 1377: /* $@622: %empty  */
#line 1795 "configparser.yy"
                                                      { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->addOption( ( ( (yyvsp[-1].num) ) == 1 ) ? TextViewMode::FOLLOW_ACTIVE : 0 ); }
#line 10529 "configparser.cc"
    break;

  case 1379: /* $@623: %empty  */
#line 1796 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->addOption( ( ( (yyvsp[-1].num) ) == 1 ) ? TextViewMode::WATCH_FILE : 0 ); }
#line 10535 "configparser.cc"
    break;

  case 1381: /* $@624: %empty  */
#line 1797 "configparser.yy"
                                                      { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10541 "configparser.cc"
    break;


#line 10545 "configparser.cc"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 1800 "configparser.yy"

/**/
int yymyparse(void)
{
  int erg;

  lconfig_error[0] = '\0';
  lconfig_linenr = 0;
  erg = yyparse();
  /* free all strings found by the lexer
     we don't need them anymore */
  lexer_cleanup();
  lconfig_cleanup();
  return erg;
}

int yyerror( const char *s )
{
  lconfig_cleanup();
  /* the following isn't really needed because yyerror is called inside the parser
     which itselve was started with yymyparse which will call it again
     but it doesn't hurt...*/
  lexer_cleanup();
  snprintf( lconfig_error, sizeof( lconfig_error ), "%s", s );
  return 0;
}
