/* lister.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef LISTER_H
#define LISTER_H

#include "wdefines.h"
#include "aguix/message.h"
#include <set>
#include <string>

class Worker;
class AGUIX;
class ListerMode;
class AContainer;
class AWindow;
class Button;
class Datei;
class DNDMsg;

typedef enum {
    CYCLICFUNC_MODE_NORMAL,
    CYCLICFUNC_MODE_RESTART
} cyclicfunc_mode_t;

class Lister
{
public:
  Lister( Worker*, int side );
  ~Lister();
  Lister( const Lister &other );
  Lister &operator=( const Lister &other );

  void messageHandler(AGMessage *msg);
  AGUIX *getAGUIX();
  AWindow *getAWindow();
  void setActiveMode(ListerMode*);
  void getGeometry(int*,int*,int*,int*);
  ListerMode *getActiveMode();
  void setFocus(bool);
  bool getFocus();
  void switch2Mode(int);
  void setName(const char*);
  void setStatebarText(const char*);
  void makeActive();
  int getSide();
  bool isActive();
  void configure();
  Worker *getWorker();
  void cyclicfunc(cyclicfunc_mode_t mode);
  int saveState(Datei *fh);
  int loadState();
  const char *getNameOfMode(int);
  
  void relayout();
  
  bool startdnd(DNDMsg *dm);

  void setContainer( AContainer *mode_cont );
    bool pathsChanged( const std::set< std::string > changed_paths );

    void finalizeBGOps();

    void setLastPath( const std::string &path );
    std::string getLastPath() const;
private:
  Worker *parent;
  AGUIX *aguix;
  AWindow *win;
  Button *lvb;
  Button *m_configure_b;
  ListerMode *activemode;
  int activemodeid;
  ListerMode **modes;
  bool hasFocus;
  
  struct timeval lastlvbclick;

  AContainer *m_cont;
  AContainer *m_mode_cont;
  AContainer *m_button_cont;

    std::string m_last_path;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
