/* changelayoutop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "changelayoutop.hh"
#include "listermode.h"
#include "worker.h"
#include "aguix/awindow.h"
#include "aguix/text.h"
#include "aguix/solidbutton.h"
#include "aguix/button.h"
#include "aguix/cyclebutton.h"
#include "aguix/slider.h"
#include "aguix/choosebutton.h"
#include "aguix/fieldlistview.h"
#include <sstream>
#include <algorithm>
#include "datei.h"
#include "worker_locale.h"

const char *ChangeLayoutOp::name = "ChangeLayoutOp";

ChangeLayoutOp::ChangeLayoutOp() : FunctionProto()
{
    hasConfigure = true;
    m_category = FunctionProto::CAT_SETTINGS;
}

ChangeLayoutOp::~ChangeLayoutOp()
{
}

ChangeLayoutOp*
ChangeLayoutOp::duplicate() const
{
    ChangeLayoutOp *ta = new ChangeLayoutOp();
    ta->m_layout = m_layout;
    return ta;
}

bool
ChangeLayoutOp::isName( const char *str )
{
    if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *
ChangeLayoutOp::getName()
{
    return name;
}

int
ChangeLayoutOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    if ( msg->getWorker()->getUseCustomLayout() == false ) {
        msg->getWorker()->setCustomLayout( m_layout );
    } else {
        LayoutSettings cur_sets = msg->getWorker()->getCustomLayout();
        if ( cur_sets != m_layout ) {
            msg->getWorker()->setCustomLayout( m_layout );
        } else {
            msg->getWorker()->unsetCustomLayout();
        }
    }
    return 0;
}

bool
ChangeLayoutOp::save( Datei *fh )
{
    if ( fh == NULL ) return false;

    std::list<LayoutSettings::layoutID_t>::const_iterator it1;

    fh->configPutPairBool( "buttonsvertical", m_layout.getButtonVert() );
    fh->configPutPairBool( "listviewsvertical", m_layout.getListViewVert() );
    fh->configPutPairNum( "listviewweight", m_layout.getListViewWeight() );
    fh->configPutPairBool( "weighttoactive", m_layout.getWeightRelToActive() );
    for ( it1 = m_layout.getOrders().begin();
          it1 != m_layout.getOrders().end();
          it1++ ) {
        switch ( *it1 ) {
            case LayoutSettings::LO_STATEBAR:
                fh->configPutInfo( "statebar", true );
                break;
            case LayoutSettings::LO_CLOCKBAR:
                fh->configPutInfo( "clockbar", true );
                break;
            case LayoutSettings::LO_BUTTONS:
                fh->configPutInfo( "buttons", true );
                break;
            case LayoutSettings::LO_LISTVIEWS:
                fh->configPutInfo( "listviews", true );
                break;
            case LayoutSettings::LO_BLL:
                fh->configPutInfo( "bll", true );
                break;
            case LayoutSettings::LO_LBL:
                fh->configPutInfo( "lbl", true );
                break;
            case LayoutSettings::LO_LLB:
                fh->configPutInfo( "llb", true );
                break;
            case LayoutSettings::LO_BL:
                fh->configPutInfo( "bl", true );
                break;
            case LayoutSettings::LO_LB:
                fh->configPutInfo( "lb", true );
                break;
            default:
                break;
        }
    }
    
    return true;
}

const char *
ChangeLayoutOp::getDescription()
{
    return catalog.getLocale( 1298 );
}

static struct {
    LayoutSettings::layoutID_t id;
    int descr;
} layoutDescr[] = { { LayoutSettings::LO_STATEBAR, 556 },
                    { LayoutSettings::LO_CLOCKBAR, 557 },
                    { LayoutSettings::LO_BUTTONS, 558 },
                    { LayoutSettings::LO_LISTVIEWS, 559 },
                    { LayoutSettings::LO_BLL, 560 },
                    { LayoutSettings::LO_LBL, 561 },
                    { LayoutSettings::LO_LLB, 562 },
                    { LayoutSettings::LO_BL, 563 },
                    { LayoutSettings::LO_LB, 564 } };

static std::string getDescrOfLayoutID( LayoutSettings::layoutID_t id )
{
    int s, i;
    std::string s1;
    
    s = sizeof( layoutDescr ) / sizeof( layoutDescr[0] );
    s1 = "";
    for ( i = 0; i < s; i++ ) {
        if ( layoutDescr[i].id == id ) {
            s1 = catalog.getLocale( layoutDescr[i].descr );
            break;
        }
    }
    return s1;
}

static LayoutSettings::layoutID_t getLayoutIDOfDescr( const std::string &descr )
{
    int s, i;
    
    s = sizeof( layoutDescr ) / sizeof( layoutDescr[0] );
    for ( i = 0; i < s; i++ ) {
        std::string s1;
        s1 = catalog.getLocale( layoutDescr[i].descr );
        if ( s1 == descr ) return layoutDescr[i].id;
    }
    return LayoutSettings::LO_STATEBAR;
}

int
ChangeLayoutOp::configure()
{
    m_aguix = Worker::getAGUIX();
    AGMessage *msg;
    int endmode = -1;
    char *tstr;
  
    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) +
                              strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
    m_win = new AWindow( m_aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    m_win->create();
    _freesafe(tstr);
    
    m_ac1 = NULL;
    m_bv = m_lvv = false;
    m_cb1 = m_cb2 = NULL;
    m_ac2 = NULL;
    m_button_ac = NULL;
    m_lv_ac = NULL;
    m_lv2_ac = NULL;
    m_available_elements_ac = NULL;
    m_used_elements_ac = NULL;
    
    m_available_elements[ std::pair< bool, bool >( false, false ) ] = LayoutSettings::getAvailableElements( false, false );
    m_available_elements[ std::pair< bool, bool >( true, false ) ] = LayoutSettings::getAvailableElements( true, false );
    m_available_elements[ std::pair< bool, bool >( false, true ) ] = LayoutSettings::getAvailableElements( false, true );
    m_available_elements[ std::pair< bool, bool >( true, true ) ] = LayoutSettings::getAvailableElements( true, true );
    
    m_button_example_ac = NULL;
    m_listview_example_ac = NULL;
    m_listview_example_b1 = NULL;
    m_listview_example_b2 = NULL;
    
    m_available_elements_lv = NULL;
    m_used_elements_lv = NULL;
    m_add_element_b = NULL;;
    m_remove_element_b = NULL;
    
    m_elements_example_ac1 = NULL;
    m_elements_example_ac2 = NULL;
    
    m_example_statebar_b = NULL;
    m_example_clockbar_b = NULL;
    m_listview_example2_b1 = NULL;
    m_listview_example2_b2 = NULL;

    m_weight_sl = NULL;
    m_left_weight_b = NULL;
    m_right_weight_b = NULL;
    m_weight_active_cb = NULL;

    int n, i;
    Text **texts;
    
    m_bv = m_layout.getButtonVert();
    m_lvv = m_layout.getListViewVert();
    
    // global container 1x3
    m_ac1 = m_win->setContainer( new AContainer( m_win, 1, 8 ), true );
    m_ac1->setMaxSpace( 5 );
    m_ac1->setBorderWidth( 5 );
    
    m_win->addMultiLineText( catalog.getLocale( 690 ),
                             *m_ac1,
                             0, 0,
                             NULL, NULL );
    
    m_ac1->add( new Text( m_aguix, 0, 0, catalog.getLocale( 565 ) ), 0, 1, AContainer::CO_INCWNR );
    // sub container 2x2 for lister config
    m_ac2 = m_ac1->add( new AContainer( m_win, 2, 2 ), 0, 2 );
    m_ac2->setMaxSpace( 5 );
    m_ac2->setBorderWidth( 0 );
    
    m_cb1 = (ChooseButton*)m_ac2->add( new ChooseButton( m_aguix,
                                                         0,
                                                         0,
                                                         20,
                                                         20,
                                                         ( m_bv == true ) ? 1 : 0,
                                                         catalog.getLocale( 567 ),
                                                         LABEL_RIGHT,
                                                         0 ), 0, 0, AContainer::CO_INCWNR );
    m_cb2 = (ChooseButton*)m_ac2->add( new ChooseButton( m_aguix,
                                                         0,
                                                         0,
                                                         20,
                                                         20,
                                                         ( m_lvv == true ) ? 1 : 0,
                                                         catalog.getLocale( 568 ),
                                                         LABEL_RIGHT,
                                                         0 ), 1, 0, AContainer::CO_INCWNR );
    
    setupExampleContainers();
    
    m_win->addTextFromString( catalog.getLocale( 856 ), 0, 0, 0, &texts, &n, NULL );
    AContainer *actext = m_ac1->add( new AContainer( m_win, 1, n ), 0, 3 );
    actext->setMaxSpace( 5 );
    actext->setBorderWidth( 0 );
    for ( i = 0; i < n; i++ ) {
        actext->add( texts[i], 0, i, AContainer::CO_INCWNR );
    }
    _freesafe( texts );
    
    m_used_elements = m_layout.getOrders();
    
    m_elements_example_ac1 = m_ac1->add( new AContainer( m_win, 4, 1 ), 0, 4 );
    m_elements_example_ac1->setMaxSpace( 5 );
    m_elements_example_ac1->setBorderWidth( 0 );
    
    m_available_elements_ac = m_elements_example_ac1->add( new AContainer( m_win, 1, 2 ), 0, 0 );
    m_available_elements_ac->setMaxSpace( 0 );
    m_available_elements_ac->setMinSpace( 0 );
    m_available_elements_ac->setBorderWidth( 0 );
    m_available_elements_ac->add( new SolidButton( m_aguix, 0, 0, catalog.getLocale( 857 ), false ),
                                  0, 0, AContainer::CO_FIX );
    m_available_elements_lv = (FieldListView*)m_available_elements_ac->add( new FieldListView( m_aguix, 0, 0, 100, 10, 0 ),
                                                                            0, 1, AContainer::CO_FIX );
    m_available_elements_lv->setHBarState( 0 );
    m_available_elements_lv->setVBarState( 0 );
    
    m_used_elements_ac = m_elements_example_ac1->add( new AContainer( m_win, 1, 2 ), 2, 0 );
    m_used_elements_ac->setMaxSpace( 0 );
    m_used_elements_ac->setMinSpace( 0 );
    m_used_elements_ac->setBorderWidth( 0 );
    m_used_elements_ac->add( new SolidButton( m_aguix, 0, 0, catalog.getLocale( 858 ), false ),
                             0, 0, AContainer::CO_FIX );
    m_used_elements_lv = (FieldListView*)m_used_elements_ac->add( new FieldListView( m_aguix, 0, 0, 100, 10, 0 ),
                                                                  0, 1, AContainer::CO_FIX );
    m_used_elements_lv->setHBarState( 0 );
    m_used_elements_lv->setVBarState( 0 );
    
    AContainer *m_elements_example_ac1_2 = m_elements_example_ac1->add( new AContainer( m_win, 1, 5 ), 1, 0 );
    m_elements_example_ac1_2->setMaxSpace( 0 );
    m_elements_example_ac1_2->setMinSpace( 0 );
    m_elements_example_ac1_2->setBorderWidth( 0 );
    m_add_element_b = (Button*)m_elements_example_ac1_2->add( new Button( m_aguix, 0, 0, catalog.getLocale( 859 ), 0 ),
                                                              0, 1, AContainer::CO_FIX );
    m_remove_element_b = (Button*)m_elements_example_ac1_2->add( new Button( m_aguix, 0, 0, catalog.getLocale( 860 ), 0 ),
                                                                 0, 3, AContainer::CO_FIX );
    m_elements_example_ac1_2->setMinHeight( 10, 0, 2 );
    m_elements_example_ac1_2->setMaxHeight( 10, 0, 2 );
    
    setupElementsExample();
    setupElementsLV();
    
    AContainer *weight_ac1 = m_ac1->add( new AContainer( m_win, 5, 1 ), 0, 5 );
    weight_ac1->setMaxSpace( 5 );
    weight_ac1->setBorderWidth( 0 );
    
    weight_ac1->add( new Text( m_aguix, 0, 0, catalog.getLocale( 861 ) ), 0, 0, AContainer::CO_FIX );
    m_left_weight_b = (SolidButton*)weight_ac1->add( new SolidButton( m_aguix, 0, 0, "000%", true ),
                                                     1, 0, AContainer::CO_FIX );
    m_weight_sl = (Slider*)weight_ac1->add( new Slider( m_aguix, 0, 0, 10, 10, false, 0 ),
                                            2, 0, AContainer::CO_MIN );
    m_weight_sl->setMaxLen( 8 );
    m_right_weight_b = (SolidButton*)weight_ac1->add( new SolidButton( m_aguix, 0, 0, "100%", true ),
                                                      3, 0, AContainer::CO_FIX );
    
    int weight = m_layout.getListViewWeight();
    if ( weight < 1 || weight > 9 ) weight = 5;
    m_weight_sl->setOffset( weight - 1 );
    updateWeightSlider();

    m_weight_active_cb = (ChooseButton*)m_ac1->add( new ChooseButton( m_aguix,
                                                                      0,
                                                                      0,
                                                                      20,
                                                                      20,
                                                                      ( m_layout.getWeightRelToActive() == true ) ? 1 : 0,
                                                                      catalog.getLocale( 908 ),
                                                                      LABEL_RIGHT,
                                                                      0 ), 0, 6, AContainer::CO_INCWNR );

    AContainer *ac1_3 = m_ac1->add( new AContainer( m_win, 2, 1 ), 0, 7 );
    ac1_3->setMinSpace( 5 );
    ac1_3->setMaxSpace( -1 );
    ac1_3->setBorderWidth( 0 );
    Button *okb =(Button*)ac1_3->add( new Button( m_aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 11 ),
                                                  0 ), 0, 0, AContainer::CO_FIX );
    Button *cb = (Button*)ac1_3->add( new Button( m_aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 8 ),
                                                  0 ), 1, 0, AContainer::CO_FIX );

    m_win->setDoTabCycling( true );
    m_win->contMaximize( true );
    m_win->show();

    for ( ; endmode == -1; ) {
        msg = m_aguix->WaitMessage( m_win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == m_win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cb ) endmode = 1;
                    else if ( msg->button.button == m_add_element_b ) {
                        int row = m_available_elements_lv->getActiveRow();
                        if ( m_available_elements_lv->isValidRow( row ) == true ) {
                            LayoutSettings::layoutID_t id = getLayoutIDOfDescr( m_available_elements_lv->getText( row, 0 ) );
                            m_used_elements.push_back( id );
                            
                            setupExampleContainers();
                            setupElementsExample();
                            setupElementsLV();
                            m_win->contMaximize( true );
                        }
                    } else if ( msg->button.button == m_remove_element_b ) {
                        int row = m_used_elements_lv->getActiveRow();
                        if ( m_used_elements_lv->isValidRow( row ) == true ) {
                            std::list< LayoutSettings::layoutID_t >::iterator it3 = m_used_elements.begin();
                            for ( ; row > 0 && it3 != m_used_elements.end(); it3++, row-- );
                            
                            if ( it3 != m_used_elements.end() ) {
                                m_used_elements.erase( it3 );
                                
                                setupExampleContainers();
                                setupElementsExample();
                                setupElementsLV();
                                m_win->contMaximize( true );
                            }
                        }
                    }
                    break;
                case AG_CHOOSECLICKED:
                    m_bv = m_cb1->getState();
                    m_lvv = m_cb2->getState();
                    
                    m_used_elements = LayoutSettings::fixUsedList( m_available_elements[ std::pair< bool, bool >( m_bv, m_lvv ) ], m_used_elements );
                    
                    setupExampleContainers();
                    setupElementsExample();
                    setupElementsLV();
                    m_win->contMaximize( true );
                    break;
                case AG_SLIDER_CHANGED:
                    updateWeightSlider();
                    break;
                case AG_KEYPRESSED:
                    if ( m_win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
            m_aguix->ReplyMessage( msg );
        }
    }

    if ( endmode == 0 ) {
        // ok
        m_layout.clearOrders();
        
        m_used_elements = LayoutSettings::fixUsedList( m_available_elements[ std::pair< bool, bool >( m_bv, m_lvv ) ], m_used_elements );
        
        m_layout.setOrders( m_used_elements );
    
        m_layout.setButtonVert( m_bv );
        m_layout.setListViewVert( m_lvv );

        int o = m_weight_sl->getOffset();
        o = o + 1;
        m_layout.setListViewWeight( o );

        m_layout.setWeightRelToActive( m_weight_active_cb->getState() );
    }
    
    if ( m_button_ac != NULL ) {
        delete m_button_ac;
        m_button_ac = NULL;
    }
    if ( m_lv2_ac != NULL ) {
        delete m_lv2_ac;
        m_lv2_ac = NULL;
    }
    if ( m_lv_ac != NULL ) {
        delete m_lv_ac;
        m_lv_ac = NULL;
    }
    if ( m_elements_example_ac2 != NULL ) {
        delete m_elements_example_ac2;
        m_elements_example_ac2 = NULL;
    }
    m_example_buttons.clear();
    m_example_buttons2.clear();

    delete m_win;
    m_win = NULL;
    m_aguix = NULL;
    
    return endmode;
}

void ChangeLayoutOp::setupExampleContainers()
{
    if ( m_button_example_ac != NULL ) delete m_button_example_ac;
    if ( m_listview_example_ac != NULL ) delete m_listview_example_ac;

    int be_w, be_h;
    if ( m_bv == true ) {
        be_w = 1;
        be_h = 6;
    } else {
        be_w = 3;
        be_h = 2;
    }
    m_button_example_ac = m_ac2->add( new AContainer( m_win, be_w, be_h ), 0, 1 );
    m_button_example_ac->setBorderWidth( 0 );
    m_button_example_ac->setMaxSpace( 0 );
    m_button_example_ac->setMinSpace( 0 );

    while ( m_example_buttons.size() < 6 ) {
        SolidButton *b = new SolidButton( m_aguix, 0, 0, 20, 5, "", 0 );
        b->setAcceptFocus( false );
        m_example_buttons.push_back( b );
    }

    std::list<SolidButton*>::iterator b_it1 = m_example_buttons.begin();
    for ( int y = 0; y < be_h; y++ ) {
        for ( int x = 0; x < be_w; x++ ) {
            if ( b_it1 == m_example_buttons.end() ) break;

            m_button_example_ac->add( *b_it1, x, y, AContainer::CO_FIX );

            b_it1++;
        }
    }

    if ( m_lvv == true ) {
        m_listview_example_ac = m_ac2->add( new AContainer( m_win, 1, 2 ), 1, 1 );
    } else {
        m_listview_example_ac = m_ac2->add( new AContainer( m_win, 2, 1 ), 1, 1 );
    }
    m_listview_example_ac->setBorderWidth( 0 );
    m_listview_example_ac->setMaxSpace( 0 );
    m_listview_example_ac->setMinSpace( 0 );

    if ( m_listview_example_b1 == NULL ) {
        m_listview_example_b1 = new SolidButton( m_aguix, 0, 0, 10, 10, "", 0 );
        m_listview_example_b1->setAcceptFocus( false );
    }
    if ( m_listview_example_b2 == NULL ) {
        m_listview_example_b2 = new SolidButton( m_aguix, 0, 0, 10, 10, "", 0 );
        m_listview_example_b2->setAcceptFocus( false );
    }

    if ( m_lvv == true ) {
        m_listview_example_b1->resize( 100, 20 );
        m_listview_example_b2->resize( 100, 20 );
        m_listview_example_ac->add( m_listview_example_b1, 0, 0, AContainer::CO_FIX );
        m_listview_example_ac->add( m_listview_example_b2, 0, 1, AContainer::CO_FIX );
    } else {
        m_listview_example_b1->resize( 50, 40 );
        m_listview_example_b2->resize( 50, 40 );
        m_listview_example_ac->add( m_listview_example_b1, 0, 0, AContainer::CO_FIX );
        m_listview_example_ac->add( m_listview_example_b2, 1, 0, AContainer::CO_FIX );
    }
}

void ChangeLayoutOp::setupElementsExample()
{
    if ( m_button_ac != NULL ) {
        delete m_button_ac;
        m_button_ac = NULL;
    }
    if ( m_lv2_ac != NULL ) {
        delete m_lv2_ac;
        m_lv2_ac = NULL;
    }
    if ( m_lv_ac != NULL ) {
        delete m_lv_ac;
        m_lv_ac = NULL;
    }
    if ( m_elements_example_ac2 != NULL ) {
        delete m_elements_example_ac2;
        m_elements_example_ac2 = NULL;
    }

    m_elements_example_ac2 = m_elements_example_ac1->add( new AContainer( m_win, 1, 5 ), 3, 0 );
    m_elements_example_ac2->setBorderWidth( 0 );
    m_elements_example_ac2->setMaxSpace( 0 );
    m_elements_example_ac2->setMinSpace( 0 );

    if ( m_example_statebar_b == NULL ) {
        m_example_statebar_b = new SolidButton( m_aguix, 0, 0, 100, 10, "", 0 );
        m_example_statebar_b->setAcceptFocus( false );
        m_win->add( m_example_statebar_b );
    }
    if ( m_example_clockbar_b == NULL ) {
        m_example_clockbar_b = new SolidButton( m_aguix, 0, 0, 100, 10, "", 0 );
        m_example_clockbar_b->setAcceptFocus( false );
        m_win->add( m_example_clockbar_b );
    }
    if ( m_listview_example2_b1 == NULL ) {
        m_listview_example2_b1 = new SolidButton( m_aguix, 0, 0, 10, 10, "", 0 );
        m_listview_example2_b1->setAcceptFocus( false );
        m_win->add( m_listview_example2_b1 );
    }
    if ( m_listview_example2_b2 == NULL ) {
        m_listview_example2_b2 = new SolidButton( m_aguix, 0, 0, 10, 10, "", 0 );
        m_listview_example2_b2->setAcceptFocus( false );
        m_win->add( m_listview_example2_b2 );
    }

    while ( m_example_buttons2.size() < 6 ) {
        SolidButton *b = new SolidButton( m_aguix, 0, 0, 20, 5, "", 0 );
        b->setAcceptFocus( false );
        m_example_buttons2.push_back( b );
        m_win->add( b );
    }

    m_example_statebar_b->hide();
    m_example_statebar_b->resize( 100, 10 );
    m_example_clockbar_b->hide();
    m_example_clockbar_b->resize( 100, 10 );
    m_listview_example2_b1->hide();
    m_listview_example2_b2->hide();

    for ( std::list<SolidButton*>::iterator it1 = m_example_buttons2.begin();
          it1 != m_example_buttons2.end();
          it1++ ) {
        (*it1)->hide();
        (*it1)->resize( 20, 5 );
    }

    int cur_pos = 0;
    int lv_placed = 0;

    for ( std::list< LayoutSettings::layoutID_t >::iterator it1 = m_used_elements.begin();
          it1 != m_used_elements.end();
          it1++ ) {
        switch ( *it1 ) {
            case LayoutSettings::LO_CLOCKBAR:
                m_elements_example_ac2->add( m_example_clockbar_b, 0, cur_pos++, AContainer::CO_INCW );
                m_example_clockbar_b->show();
                break;
            case LayoutSettings::LO_STATEBAR:
                m_elements_example_ac2->add( m_example_statebar_b, 0, cur_pos++, AContainer::CO_INCW );
                m_example_statebar_b->show();
                break;
            case LayoutSettings::LO_BUTTONS:
                {
                    m_button_ac = m_elements_example_ac2->add( new AContainer( m_win, 3, 2 ), 0, cur_pos++ );
                    m_button_ac->setBorderWidth( 0 );
                    m_button_ac->setMaxSpace( 0 );
                    m_button_ac->setMinSpace( 0 );
                    
                    std::list<SolidButton*>::iterator b_it1 = m_example_buttons2.begin();
                    for ( int y = 0; y < 2; y++ ) {
                        for ( int x = 0; x < 3; x++ ) {
                            if ( b_it1 == m_example_buttons2.end() ) break;
                            
                            m_button_ac->add( *b_it1, x, y, AContainer::CO_INCW );
                            (*b_it1)->show();
                            
                            b_it1++;
                        }
                    }
                }
                break;
            case LayoutSettings::LO_LISTVIEWS:
                if ( m_lvv == true ) {
                    if ( lv_placed == 0 ) {
                        m_listview_example2_b1->resize( 100, 20 );
                        m_elements_example_ac2->add( m_listview_example2_b1, 0, cur_pos++, AContainer::CO_MIN );
                        m_listview_example2_b1->show();
                        lv_placed++;
                    } else {
                        m_listview_example2_b2->resize( 100, 20 );
                        m_elements_example_ac2->add( m_listview_example2_b2, 0, cur_pos++, AContainer::CO_MIN );
                        m_listview_example2_b2->show();
                        lv_placed++;
                    }
                } else {
                    m_lv_ac = m_elements_example_ac2->add( new AContainer( m_win, 2, 1 ), 0, cur_pos++ );
                    m_lv_ac->setBorderWidth( 0 );
                    m_lv_ac->setMaxSpace( 0 );
                    m_lv_ac->setMinSpace( 0 );

                    m_listview_example2_b1->resize( 50, 40 );
                    m_lv_ac->add( m_listview_example2_b1, 0, 0, AContainer::CO_MIN );
                    m_listview_example2_b1->show();

                    m_listview_example2_b2->resize( 50, 40 );
                    m_lv_ac->add( m_listview_example2_b2, 1, 0, AContainer::CO_MIN );
                    m_listview_example2_b2->show();
                }
                break;
            case LayoutSettings::LO_BLL:
            case LayoutSettings::LO_LBL:
            case LayoutSettings::LO_LLB:
                {
                    m_lv_ac = m_elements_example_ac2->add( new AContainer( m_win, 3, 1 ), 0, cur_pos++ );
                    m_lv_ac->setBorderWidth( 0 );
                    m_lv_ac->setMaxSpace( 0 );
                    m_lv_ac->setMinSpace( 0 );

                    if ( *it1 == LayoutSettings::LO_BLL ) {
                        m_button_ac = m_lv_ac->add( new AContainer( m_win, 1, 6 ), 0, 0 );
                    } else if ( *it1 == LayoutSettings::LO_LBL ) {
                        m_button_ac = m_lv_ac->add( new AContainer( m_win, 1, 6 ), 1, 0 );
                    } else {
                        m_button_ac = m_lv_ac->add( new AContainer( m_win, 1, 6 ), 2, 0 );
                    }
                    m_button_ac->setBorderWidth( 0 );
                    m_button_ac->setMaxSpace( 0 );
                    m_button_ac->setMinSpace( 0 );
                    
                    std::list<SolidButton*>::iterator b_it1 = m_example_buttons2.begin();
                    for ( int y = 0; y < 6; y++ ) {
                        if ( b_it1 == m_example_buttons2.end() ) break;
                        
                        m_button_ac->add( *b_it1, 0, y, AContainer::CO_INCH );
                        (*b_it1)->show();
                        
                        b_it1++;
                    }

                    m_listview_example2_b1->resize( 50, 40 );
                    m_listview_example2_b2->resize( 50, 40 );

                    if ( *it1 == LayoutSettings::LO_BLL ) {
                        m_lv_ac->add( m_listview_example2_b1, 1, 0, AContainer::CO_MIN );
                        m_lv_ac->add( m_listview_example2_b2, 2, 0, AContainer::CO_MIN );
                    } else if ( *it1 == LayoutSettings::LO_LBL ) {
                        m_lv_ac->add( m_listview_example2_b1, 0, 0, AContainer::CO_MIN );
                        m_lv_ac->add( m_listview_example2_b2, 2, 0, AContainer::CO_MIN );
                    } else {
                        m_lv_ac->add( m_listview_example2_b1, 0, 0, AContainer::CO_MIN );
                        m_lv_ac->add( m_listview_example2_b2, 1, 0, AContainer::CO_MIN );
                    }
                    m_listview_example2_b1->show();
                    m_listview_example2_b2->show();
                }
                break;
            case LayoutSettings::LO_BL:
            case LayoutSettings::LO_LB:
                {
                    m_lv_ac = m_elements_example_ac2->add( new AContainer( m_win, 2, 1 ), 0, cur_pos++ );
                    m_lv_ac->setBorderWidth( 0 );
                    m_lv_ac->setMaxSpace( 0 );
                    m_lv_ac->setMinSpace( 0 );

                    if ( *it1 == LayoutSettings::LO_BL ) {
                        m_button_ac = m_lv_ac->add( new AContainer( m_win, 1, 6 ), 0, 0 );
                    } else {
                        m_button_ac = m_lv_ac->add( new AContainer( m_win, 1, 6 ), 1, 0 );
                    }
                    m_button_ac->setBorderWidth( 0 );
                    m_button_ac->setMaxSpace( 0 );
                    m_button_ac->setMinSpace( 0 );
                    
                    std::list<SolidButton*>::iterator b_it1 = m_example_buttons2.begin();
                    for ( int y = 0; y < 6; y++ ) {
                        if ( b_it1 == m_example_buttons2.end() ) break;
                        
                        m_button_ac->add( *b_it1, 0, y, AContainer::CO_INCH );
                        (*b_it1)->show();
                        
                        b_it1++;
                    }

                    m_listview_example2_b1->resize( 100, 20 );
                    m_listview_example2_b2->resize( 100, 20 );

                    if ( *it1 == LayoutSettings::LO_BL ) {
                        m_lv2_ac = m_lv_ac->add( new AContainer( m_win, 1, 2 ), 1, 0 );
                    } else {
                        m_lv2_ac = m_lv_ac->add( new AContainer( m_win, 1, 2 ), 0, 0 );
                    }
                    m_lv2_ac->setBorderWidth( 0 );
                    m_lv2_ac->setMaxSpace( 0 );
                    m_lv2_ac->setMinSpace( 0 );

                    m_lv2_ac->add( m_listview_example2_b1, 0, 0, AContainer::CO_MIN );
                    m_lv2_ac->add( m_listview_example2_b2, 0, 1, AContainer::CO_MIN );
                    m_listview_example2_b1->show();
                    m_listview_example2_b2->show();
                }
                break;
            default:
                break;
        }
    }
}

void ChangeLayoutOp::setupElementsLV()
{
    std::map< LayoutSettings::layoutID_t, int > count_map;

    m_available_elements_lv->setSize( 0 );
    std::list< std::list< LayoutSettings::layoutID_t > > &l = m_available_elements[ std::pair< bool, bool >( m_bv, m_lvv ) ];
    for ( std::list< std::list< LayoutSettings::layoutID_t > >::iterator it1 =  l.begin();
          it1 != l.end();
          it1++ ) {

        // if any element in *it1 is found in m_used_elements don't add it
        bool found = false;
        for ( std::list< LayoutSettings::layoutID_t >::iterator it2 = it1->begin();
              it2 != it1->end();
              it2++ ) {
            if ( std::count( m_used_elements.begin(),
                             m_used_elements.end(),
                             *it2 ) >= LayoutSettings::countID( l, *it2 ) ) {
                found = true;
            }
        }
        
        if ( found == false ) {
            for ( std::list< LayoutSettings::layoutID_t >::iterator it2 = it1->begin();
                  it2 != it1->end();
                  it2++ ) {

                if ( count_map[*it2] + std::count( m_used_elements.begin(),
                                                   m_used_elements.end(),
                                                   *it2 ) < LayoutSettings::countID( l, *it2 ) ) {
                    int row = m_available_elements_lv->addRow();
                    m_available_elements_lv->setText( row, 0, getDescrOfLayoutID( *it2 ) );
                    m_available_elements_lv->setPreColors( row, FieldListView::PRECOLOR_ONLYACTIVE );
                    count_map[*it2]++;
                }
            }
        }
    }
    if ( m_available_elements_lv->getElements() < 1 ) {
        m_available_elements_lv->resize( 100, 10 );
    } else {
        m_available_elements_lv->maximizeX();
        m_available_elements_lv->maximizeY();
    }
    m_available_elements_lv->redraw();

    m_used_elements_lv->setSize( 0 );
    for ( std::list< LayoutSettings::layoutID_t >::iterator it1 =  m_used_elements.begin();
          it1 != m_used_elements.end();
          it1++ ) {
        int row = m_used_elements_lv->addRow();
        m_used_elements_lv->setText( row, 0, getDescrOfLayoutID( *it1 ) );
        m_used_elements_lv->setPreColors( row, FieldListView::PRECOLOR_ONLYACTIVE );
    }
    if ( m_used_elements_lv->getElements() < 1 ) {
        m_used_elements_lv->resize( 100, 10 );
    } else {
        m_used_elements_lv->maximizeX();
        m_used_elements_lv->maximizeY();
    }
    m_used_elements_lv->redraw();

    m_available_elements_ac->readLimits();
    m_used_elements_ac->readLimits();
}

void ChangeLayoutOp::setLayout( const LayoutSettings &l )
{
    m_layout = l;
}

void ChangeLayoutOp::updateWeightSlider()
{
    int o = m_weight_sl->getOffset();
    o++;

    o *= 10;
    
    {
        std::stringstream s1;
        s1 << o;
        s1 << "%";
        m_left_weight_b->setText( s1.str().c_str() );
    }

    o = 100 - o;
    {
        std::stringstream s1;
        s1 << o;
        s1 << "%";
        m_right_weight_b->setText( s1.str().c_str() );
    }
}
