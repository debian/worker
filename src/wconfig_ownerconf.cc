/* wconfig_ownerconf.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_ownerconf.hh"
#include "wconfig.h"
#include "worker_locale.h"
#include "aguix/cyclebutton.h"

OwnerConfPanel::OwnerConfPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

OwnerConfPanel::~OwnerConfPanel()
{
}

int OwnerConfPanel::create()
{
  Panel::create();

  AContainer *ac1 = setContainer( new AContainer( this, 1, 2 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 678 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 130 ) ),
              0, 0, AContainer::CO_FIX );
  
  cyb = (CycleButton*)ac1_1->add( new CycleButton( _aguix,
                                                   0,
                                                   0,
                                                   100,
                                                   0 ), 1, 0, AContainer::CO_INCW );
  cyb->addOption( catalog.getLocale( 131 ) );
  cyb->addOption( catalog.getLocale( 132 ) );
  cyb->resize( cyb->getMaxSize(), cyb->getHeight() );
  if ( _baseconfig.getOwnerstringtype() == 1 ) cyb->setOption( 1 );
  else cyb->setOption( 0 );
  ac1_1->readLimits();

  contMaximize( true );
  return 0;
}

int OwnerConfPanel::saveValues()
{
  if ( cyb->getSelectedOption() == 1 ) {
    _baseconfig.setOwnerstringtype( 1 );
  } else {
    _baseconfig.setOwnerstringtype( 0 );
  }
  return 0;
}
