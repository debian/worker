%{
/* parser for config file (bison generated)
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2003-2025 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wdefines.h"
#include "wconfig.h"
#include "wcdoubleshortkey.hh"
#include "wcpath.hh"
#include "wcfiletype.hh"
#include "wchotkey.hh"
#include "wcbutton.hh"
#include <stdlib.h>
#include <string.h>
#include "worker_commands.h"
#include "configheader.h"
#include <list>
#include "verzeichnis.hh"
#include <map>
#include "dirfiltersettings.hh"
#include "layoutsettings.hh"
#include "simplelist.hh"
#include "worker_types.h"
#include "wconfig_types.hh"

using namespace CopyParams;

int lconfig_side;
int lconfig_pos;
List *lconfig_listp,
     *lconfig_listb,
     *lconfig_listft,
     *lconfig_listh,
     *lconfig_listsk;
std::list< WConfigTypes::dont_check_dir > lconfig_ignorelist;
WConfigTypes::dont_check_dir lconfig_ignore_dir;
command_list_t lconfig_listcom;
std::list< std::string > lconfig_pathjump_allow;
std::map< std::string, struct directory_presets > dir_presets;
std::string path;
struct directory_presets dir_preset;
NM_Filter dir_preset_filter;
WCButton *lconfig_bt1;
WCFiletype *lconfig_ft1, *old_lconfig_ft1;
std::list<WCFiletype*> filetypehier;
WCHotkey *lconfig_hk1;
WCDoubleShortkey *lconfig_dk;
WCPath *lconfig_p1;
int lconfig_id;
KeySym lconfig_nkeysym;
unsigned int lconfig_keymod;
char *lconfig_tstr;
char lconfig_error[256];
std::map<std::string, WConfig::ColorDef::label_colors_t> lconfig_labelcolors;
WConfig::ColorDef::label_colors_t lconfig_labelcolor;
std::string lconfig_labelcolor_name;
LayoutSettings layout_sets;
std::string lconfig_face_name;
int lconfig_face_color;
std::list< std::pair< std::string, int > > lconfig_faces_list;
std::vector< WorkerTypes::listcol_t > columns;

class ParseState {
public:
    void clear()
    {
        lconfig_fp1.reset();
    }

    std::shared_ptr< FunctionProto > lconfig_fp1;
};

static ParseState ps;

extern WConfig *lconfig;

extern int yyerror( const char *s );

void lconfig_parseinit(void)
{
  lconfig_listp = NULL;
  lconfig_listb = NULL;
  lconfig_listft = NULL;
  lconfig_listh = NULL;
  lconfig_listsk = NULL;
  lconfig_listcom = {};
  lconfig_ignorelist.clear();
  lconfig_bt1 = NULL;
  lconfig_ft1 = NULL;
  lconfig_hk1 = NULL;
  lconfig_dk = NULL;
  lconfig_p1 = NULL;
  lconfig_tstr = NULL;
  ps.clear();
}

void lconfig_cleanup(void)
{
  ps.clear();

  if ( lconfig_tstr != NULL ) {
    _freesafe( lconfig_tstr );
    lconfig_tstr = NULL;
  }
  if ( lconfig_p1 != NULL ) {
    delete lconfig_p1;
    lconfig_p1 = NULL;
  }
  if ( lconfig_dk != NULL ) {
    delete lconfig_dk;
    lconfig_dk = NULL;
  }
  if ( lconfig_hk1 != NULL ) {
    delete lconfig_hk1;
    lconfig_hk1 = NULL;
  }
  if ( lconfig_ft1 != NULL ) {
    delete lconfig_ft1;
    lconfig_ft1 = NULL;
  }
  if ( lconfig_bt1 != NULL ) {
    delete lconfig_bt1;
    lconfig_bt1 = NULL;
  }
  if ( lconfig_listp != NULL ) {
    lconfig_id = lconfig_listp->initEnum();
    lconfig_p1 = (WCPath*)lconfig_listp->getFirstElement( lconfig_id );
    while ( lconfig_p1 != NULL ) {
      delete lconfig_p1;
      lconfig_p1 = (WCPath*)lconfig_listp->getNextElement( lconfig_id );
    }
    lconfig_listp->closeEnum( lconfig_id );
    delete lconfig_listp;
    lconfig_listp = NULL;
  }
  if ( lconfig_listb != NULL ) {
    lconfig_id = lconfig_listb->initEnum();
    lconfig_bt1 = (WCButton*)lconfig_listb->getFirstElement( lconfig_id );
    while ( lconfig_bt1 != NULL ) {
      delete lconfig_bt1;
      lconfig_bt1 = (WCButton*)lconfig_listb->getNextElement( lconfig_id );
    }
    lconfig_listb->closeEnum( lconfig_id );
    delete lconfig_listb;
    lconfig_listb = NULL;
  }
  if ( lconfig_listft != NULL ) {
    lconfig_id = lconfig_listft->initEnum();
    lconfig_ft1 = (WCFiletype*)lconfig_listft->getFirstElement( lconfig_id );
    while ( lconfig_ft1 != NULL ) {
      delete lconfig_ft1;
      lconfig_ft1 = (WCFiletype*)lconfig_listft->getNextElement( lconfig_id );
    }
    lconfig_listft->closeEnum( lconfig_id );
    delete lconfig_listft;
    lconfig_listft = NULL;
  }

  while ( filetypehier.size() > 0 ) {
      lconfig_ft1 = filetypehier.back();
      filetypehier.pop_back();
      delete lconfig_ft1;
      lconfig_ft1 = NULL;
  }

  if ( lconfig_listh != NULL ) {
    lconfig_id = lconfig_listh->initEnum();
    lconfig_hk1 = (WCHotkey*)lconfig_listh->getFirstElement( lconfig_id );
    while ( lconfig_hk1 != NULL ) {
      delete lconfig_hk1;
      lconfig_hk1 = (WCHotkey*)lconfig_listh->getNextElement( lconfig_id );
    }
    lconfig_listh->closeEnum( lconfig_id );
    delete lconfig_listh;
    lconfig_listh = NULL;
  }
  if ( lconfig_listsk != NULL ) {
    lconfig_id = lconfig_listsk->initEnum();
    lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
    while ( lconfig_dk != NULL ) {
      delete lconfig_dk;
      lconfig_listsk->removeFirstElement();
      lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
    }
    lconfig_listsk->closeEnum( lconfig_id );
    delete lconfig_listsk;
    lconfig_listsk = NULL;
  }
  lconfig_listcom.clear();
  lconfig_ignorelist.clear();
}

/*#define YYERROR_VERBOSE*/

%}

%union {
  int num;
  char *strptr;
}

%token LEFTBRACE_WCP RIGHTBRACE_WCP GLOBAL_WCP COLORS_WCP LANG_WCP PALETTE_WCP OWNOP_WCP LISTERSETS_WCP YES_WCP NO_WCP
%token LEFT_WCP RIGHT_WCP HBARTOP_WCP HBARHEIGHT_WCP VBARLEFT_WCP VBARWIDTH_WCP DISPLAYSETS_WCP
%token NAME_WCP SIZE_WCP TYPE_WCP PERMISSION_WCP OWNER_WCP DESTINATION_WCP MODTIME_WCP ACCTIME_WCP CHGTIME_WCP ROWS_WCP
%token COLUMNS_WCP CACHESIZE_WCP OWNERSTYLE_WCP TERMINAL_WCP USESTRINGFORDIRSIZE_WCP
%token TIMESETS_WCP STYLE1_WCP STYLE2_WCP STYLE3_WCP DATE_WCP TIME_WCP DATESUBSTITUTION_WCP DATEBEFORETIME_WCP
%token STATEBAR_WCP SELLVBAR_WCP UNSELLVBAR_WCP CLOCKBAR_WCP REQUESTER_WCP UNSELDIR_WCP SELDIR_WCP UNSELFILE_WCP
%token SELFILE_WCP UNSELACTDIR_WCP SELACTDIR_WCP UNSELACTFILE_WCP SELACTFILE_WCP LVBG_WCP
%token STARTUP_WCP BUTTON_WCP BUTTONS_WCP POSITION_WCP TITLE_WCP COMMANDS_WCP SHORTKEYS_WCP
%token PATHS_WCP PATH_WCP FILETYPES_WCP FILETYPE_WCP NORMAL_WCP NOTYETCHECKED_WCP UNKNOWN_WCP NOSELECT_WCP DIR_WCP
%token USEPATTERN_WCP USECONTENT_WCP PATTERN_WCP CONTENT_WCP FTCOMMANDS_WCP DND_WCP DC_WCP SHOW_WCP RAWSHOW_WCP USER_WCP
%token FLAGS_WCP IGNOREDIRS_WCP HOTKEYS_WCP HOTKEY_WCP FONTS_WCP GLOBALFONT_WCP BUTTONFONT_WCP LEFTFONT_WCP RIGHTFONT_WCP TEXTVIEWFONT_WCP STATEBARFONT_WCP TEXTVIEWALTFONT_WCP
%token CLOCKBARSETS_WCP MODUS_WCP TIMESPACE_WCP VERSION_WCP EXTERN_WCP UPDATETIME_WCP PROGRAM_WCP SHOWHINTS_WCP
%token KEY_WCP DOUBLE_WCP MOD_WCP CONTROL_WCP SHIFT_WCP LOCK_WCP MOD1_WCP MOD2_WCP MOD3_WCP MOD4_WCP MOD5_WCP
%token DNDACTION_WCP DCACTION_WCP SHOWACTION_WCP RSHOWACTION_WCP USERACTION_WCP ROWUP_WCP ROWDOWN_WCP
%token CHANGEHIDDENFLAG_WCP COPYOP_WCP FIRSTROW_WCP LASTROW_WCP PAGEUP_WCP PAGEDOWN_WCP SELECTOP_WCP
%token SELECTALLOP_WCP SELECTNONEOP_WCP INVERTALLOP_WCP PARENTDIROP_WCP ENTERDIROP_WCP
%token CHANGELISTERSETOP_WCP SWITCHLISTEROP_WCP FILTERSELECTOP_WCP FILTERUNSELECTOP_WCP
%token PATHTOOTHERSIDEOP_WCP QUITOP_WCP DELETEOP_WCP RELOADOP_WCP MAKEDIROP_WCP RENAMEOP_WCP DIRSIZEOP_WCP
%token SIMDDOP_WCP STARTPROGOP_WCP SEARCHENTRYOP_WCP ENTERPATHOP_WCP SCROLLLISTEROP_WCP
%token CREATESYMLINKOP_WCP CHANGESYMLINKOP_WCP CHMODOP_WCP TOGGLELISTERMODEOP_WCP SETSORTMODEOP_WCP
%token SETFILTEROP_WCP SHORTKEYFROMLISTOP_WCP CHOWNOP_WCP WORKERCONFIG_WCP
%token USERSTYLE_WCP DATESTRING_WCP TIMESTRING_WCP COLOR_WCP PATTERNIGNORECASE_WCP PATTERNUSEREGEXP_WCP PATTERNUSEFULLNAME_WCP
%token COM_WCP SEPARATEEACHENTRY_WCP RECURSIVE_WCP START_WCP TERMINALWAIT_WCP SHOWOUTPUT_WCP VIEWSTR_WCP SHOWOUTPUTINT_WCP SHOWOUTPUTOTHERSIDE_WCP SHOWOUTPUTCUSTOMATTRIBUTE_WCP
%token INBACKGROUND_WCP TAKEDIRS_WCP ACTIONNUMBER_WCP HIDDENFILES_WCP HIDE_WCP TOGGLE_WCP
%token FOLLOWSYMLINKS_WCP MOVE_WCP RENAME_WCP SAMEDIR_WCP REQUESTDEST_WCP REQUESTFLAGS_WCP OVERWRITE_WCP
%token ALWAYS_WCP NEVER_WCP COPYMODE_WCP FAST_WCP PRESERVEATTR_WCP MODE_WCP ACTIVE_WCP ACTIVE2OTHER_WCP SPECIAL_WCP
%token REQUEST_WCP CURRENT_WCP OTHER_WCP FILTER_WCP QUICK_WCP ALSOACTIVE_WCP RESETDIRSIZES_WCP KEEPFILETYPES_WCP
%token RELATIVE_WCP ONFILES_WCP ONDIRS_WCP SORTBY_WCP SORTFLAG_WCP REVERSE_WCP DIRLAST_WCP DIRMIXED_WCP EXCLUDE_WCP
%token INCLUDE_WCP UNSET_WCP UNSETALL_WCP SCRIPTOP_WCP NOP_WCP PUSH_WCP LABEL_WCP IF_WCP END_WCP POP_WCP SETTINGS_WCP WINDOW_WCP
%token GOTO_WCP PUSHUSEOUTPUT_WCP DODEBUG_WCP WPURECURSIVE_WCP WPUTAKEDIRS_WCP STACKNR_WCP PUSHSTRING_WCP PUSHOUTPUTRETURNCODE_WCP
%token IFTEST_WCP IFLABEL_WCP WINTYPE_WCP OPEN_WCP CLOSE_WCP LEAVE_WCP CHANGEPROGRESS_WCP CHANGETEXT_WCP
%token PROGRESSUSEOUTPUT_WCP WINTEXTUSEOUTPUT_WCP PROGRESS_WCP WINTEXT_WCP SHOWDIRCACHEOP_WCP
%token INODE_WCP NLINK_WCP BLOCKS_WCP SHOWHEADER_WCP LVHEADER_WCP IGNORECASE_WCP
%token LISTVIEWS_WCP BLL_WCP LBL_WCP LLB_WCP BL_WCP LB_WCP LAYOUT_WCP BUTTONSVERT_WCP LISTVIEWSVERT_WCP LISTVIEWWEIGHT_WCP WEIGHTTOACTIVE_WCP
%token EXTCOND_WCP USEEXTCOND_WCP
%token SUBTYPE_WCP PARENTACTIONOP_WCP NOOPERATIONOP_WCP
%token COLORMODE_WCP DEFAULT_WCP CUSTOM_WCP UNSELECTCOLOR_WCP SELECTCOLOR_WCP UNSELECTACTIVECOLOR_WCP
%token SELECTACTIVECOLOR_WCP COLOREXTERNPROG_WCP PARENT_WCP DONTCD_WCP DONTCHECKVIRTUAL_WCP GOFTPOP_WCP
%token HOSTNAME_WCP USERNAME_WCP PASSWORD_WCP DONTENTERFTP_WCP ALWAYSSTOREPW_WCP
%token INTERNALVIEWOP_WCP CUSTOMFILES_WCP SHOWMODE_WCP SELECTED_WCP REVERSESEARCH_WCP
%token MOUSECONF_WCP SELECTBUTTON_WCP ACTIVATEBUTTON_WCP SCROLLBUTTON_WCP SELECTMETHOD_WCP ALTERNATIVE_WCP DELAYEDCONTEXTMENU_WCP
%token SEARCHOP_WCP EDITCOMMAND_WCP SHOWPREVRESULTS_WCP
%token TEXTVIEW_WCP TEXTVIEWHIGHLIGHTED_WCP TEXTVIEWSELECTION_WCP
%token DIRBOOKMARKOP_WCP OPENCONTEXTMENUOP_WCP RUNCUSTOMACTION_WCP CUSTOMNAME_WCP
%token CONTEXTBUTTON_WCP ACTIVATEMOD_WCP SCROLLMOD_WCP CONTEXTMOD_WCP NONE_WCP
%token CUSTOMACTION_WCP SAVE_WORKER_STATE_ON_EXIT_WCP OPENWORKERMENUOP_WCP
%token CHANGELABELOP_WCP ASKFORLABEL_WCP LABELCOLORS_WCP
%token BOOKMARKLABEL_WCP BOOKMARKFILTER_WCP SHOWONLYBOOKMARKS_WCP SHOWONLYLABEL_WCP SHOWALL_WCP
%token OPTIONMODE_WCP INVERT_WCP SET_WCP CHANGEFILTERS_WCP CHANGEBOOKMARKS_WCP QUERYLABEL_WCP
%token MODIFYTABSOP_WCP TABACTION_WCP NEWTAB_WCP CLOSECURRENTTAB_WCP NEXTTAB_WCP PREVTAB_WCP TOGGLELOCKSTATE_WCP MOVETABLEFT_WCP MOVETABRIGHT_WCP
%token CHANGELAYOUTOP_WCP INFIXSEARCH_WCP VOLUMEMANAGEROP_WCP
%token ERROR ACTIVESIDE_WCP SHOWFREESPACE_WCP ACTIVEMODE_WCP ASK_WCP SSHALLOW_WCP FIELD_WIDTH_WCP QUICKSEARCHENABLED_WCP
%token FILTEREDSEARCHENABLED_WCP XFTFONTS_WCP
%token USEMAGIC_WCP MAGICPATTERN_WCP MAGICCOMPRESSED_WCP MAGICMIME_WCP
%token VOLUMEMANAGER_WCP MOUNTCOMMAND_WCP UNMOUNTCOMMAND_WCP FSTABFILE_WCP MTABFILE_WCP PARTFILE_WCP REQUESTACTION_WCP
%token SIZEH_WCP NEXTAGING_WCP ENTRY_WCP PREFIX_WCP VALUE_WCP
%token EXTENSION_WCP
%token EJECTCOMMAND_WCP CLOSETRAYCOMMAND_WCP
%token AVFSMODULE_WCP FLEXIBLEMATCH_WCP USE_VERSION_STRING_COMPARE_WCP
%token SWITCHBUTTONBANKOP_WCP SWITCHTONEXTBANK_WCP SWITCHTOPREVBANK_WCP SWITCHTOBANKNR_WCP BANKNR_WCP
%token USE_VIRTUAL_TEMP_COPIES_WCP PATHJUMPOP_WCP PATHJUMPALLOWDIRS_WCP
%token CLIPBOARDOP_WCP CLIPBOARDSTRING_WCP
%token COMMANDSTRING_WCP EVALCOMMAND_WCP WATCHMODE_WCP
%token APPLY_WINDOW_DIALOG_TYPE_WCP FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP
%token COMMANDMENUOP_WCP SEARCHMODEONKEYPRESS_WCP
%token PATHENTRYONTOP_WCP
%token STORE_FILES_ALWAYS_WCP PATHJUMPSETS_WCP
%token SHOWDOTDOT_WCP SHOWBREADCRUMB_WCP
%token FACES_WCP FACE_WCP
%token ENABLE_INFO_LINE_WCP
%token INFO_LINE_LUA_MODE_WCP
%token INFO_LINE_CONTENT_WCP
%token RESTORE_TABS_MODE_WCP STORE_TABS_MODE_WCP AS_EXIT_STATE_WCP
%token USE_EXTENDED_REGEX_WCP HIGHLIGHT_USER_ACTION_WCP
%token CHTIMEOP_WCP ADJUSTRELATIVESYMLINKS_WCP OUTSIDE_WCP
%token EDIT_WCP MAKEABSOLUTE_WCP MAKERELATIVE_WCP
%token MAGICIGNORECASE_WCP
%token ENSURE_FILE_PERMISSIONS_WCP USER_RW_WCP USER_RW_GROUP_R_WCP USER_RW_ALL_R_WCP LEAVE_UNMODIFIED_WCP
%token PREFERUDISKSVERSION_WCP
%token CHANGECOLUMNSOP_WCP
%token PATTERNISCOMMASEPARATED_WCP
%token STRCASECMP_WCP
%token USE_STRING_COMPARE_MODE_WCP
%token VIEWNEWESTFILESOP_WCP
%token SHOWRECENT_WCP
%token DIRCOMPAREOP_WCP
%token TABPROFILESOP_WCP
%token EXTERNALVDIROP_WCP
%token VDIR_PRESERVE_DIR_STRUCTURE_WCP
%token OPENTABMENUOP_WCP
%token INITIALTAB_WCP SHOWBYTIME_WCP SHOWBYFILTER_WCP SHOWBYPROGRAM_WCP
%token TERMINAL_RETURNS_EARLY_WCP
%token DIRECTORYPRESETS_WCP DIRECTORYPRESET_WCP
%token HELPOP_WCP
%token PRIORITY_WCP
%token AUTOFILTER_WCP
%token ENABLE_CUSTOM_LVB_LINE_WCP
%token CUSTOM_LVB_LINE_WCP
%token CUSTOM_DIRECTORY_INFO_COMMAND_WCP
%token IMMEDIATE_FILTER_APPLY_WCP
%token DISABLEBGCHECKPREFIX_WCP
%token DISPLAYMODE_WCP SHOWSUBDIRS_WCP SHOWDIRECTSUBDIRS_WCP INCLUDEALL_WCP
%token APPLYMODE_WCP ADD_WCP REMOVE_WCP
%token HIDENONEXISTING_WCP
%token VIEWCOMMANDLOGOP_WCP
%token LOCKONCURRENTDIR_WCP
%token STARTNODE_WCP
%token MENUS_WCP LVMODES_WCP LVCOMMANDS_WCP TOPLEVEL_WCP
%token FOLLOWACTIVE_WCP WATCHFILE_WCP
%token ACTIVATETEXTVIEWMODEOP_WCP SOURCEMODE_WCP ACTIVEFILE_WCP FILETYPEACTION_WCP
%token IGNOREDIRS2_WCP IGNOREDIR_WCP
%token CUSTOMATTRIBUTE_WCP
%token STARTSEARCHSTRING_WCP
%token APPLYONLY_WCP
%token USESELECTEDENTRIES_WCP
%token ONLYEXACTPATH_WCP
%token ALSOSELECTEXTENSION_WCP

%type <num> bool
%type <num> ft_type
%token <strptr> STRING_WCP 
%token <num> NUM_WCP

%%
S:{ lconfig_parseinit();}WORKERCONFIG_WCP NUM_WCP'.'NUM_WCP'.'NUM_WCP';'{ lconfig->setLoadedVersion( $3, $5, $7 ); } start
 ;

start:GLOBAL_WCP LEFTBRACE_WCP glb RIGHTBRACE_WCP start
     |COLORS_WCP LEFTBRACE_WCP { lconfig_faces_list.clear(); } cols
      {
        lconfig->applyNewFacesFromList( lconfig_faces_list );
      } RIGHTBRACE_WCP start
     |STARTUP_WCP LEFTBRACE_WCP startup RIGHTBRACE_WCP start
     |PATHS_WCP LEFTBRACE_WCP { lconfig_listp = new List(); }
      paths RIGHTBRACE_WCP { lconfig->setPaths( lconfig_listp );
                             lconfig_id = lconfig_listp->initEnum();
                             lconfig_p1 = (WCPath*)lconfig_listp->getFirstElement( lconfig_id );
                             while ( lconfig_p1 != NULL ) {
                               delete lconfig_p1;
                               lconfig_p1 = (WCPath*)lconfig_listp->getNextElement( lconfig_id );
                             }
                             lconfig_listp->closeEnum( lconfig_id );
                             delete lconfig_listp;
                             lconfig_listp = NULL;
                           } start
     |FILETYPES_WCP LEFTBRACE_WCP { lconfig_listft = new List(); }
      filetypes RIGHTBRACE_WCP { lconfig->setFiletypes( lconfig_listft );
                                 lconfig_id = lconfig_listft->initEnum();
                                 lconfig_ft1 = (WCFiletype*)lconfig_listft->getFirstElement( lconfig_id );
                                 while ( lconfig_ft1 != NULL ) {
                                   delete lconfig_ft1;
                                   lconfig_ft1 = (WCFiletype*)lconfig_listft->getNextElement( lconfig_id );
                                 }
                                 lconfig_listft->closeEnum( lconfig_id );
                                 delete lconfig_listft;
                                 lconfig_listft = NULL;
                               } start
     |HOTKEYS_WCP LEFTBRACE_WCP { lconfig_listh = new List(); }
      hotkeys RIGHTBRACE_WCP { lconfig->setHotkeys( lconfig_listh );
                               lconfig_id = lconfig_listh->initEnum();
                               lconfig_hk1 = (WCHotkey*)lconfig_listh->getFirstElement( lconfig_id );
                               while ( lconfig_hk1 != NULL ) {
                                 delete lconfig_hk1;
                                 lconfig_hk1 = (WCHotkey*)lconfig_listh->getNextElement( lconfig_id );
                               }
                               lconfig_listh->closeEnum( lconfig_id );
                               delete lconfig_listh;
                               lconfig_listh = NULL;
                             } start
     |BUTTONS_WCP LEFTBRACE_WCP { lconfig_listb = new List(); }
      buttons RIGHTBRACE_WCP { lconfig->setButtons( lconfig_listb );
                               lconfig_id = lconfig_listb->initEnum();
                               lconfig_bt1 = (WCButton*)lconfig_listb->getFirstElement( lconfig_id );
                               while ( lconfig_bt1 != NULL ) {
                                 delete lconfig_bt1;
                                 lconfig_bt1 = (WCButton*)lconfig_listb->getNextElement( lconfig_id );
                               }
                               lconfig_listb->closeEnum( lconfig_id );
                               delete lconfig_listb;
                               lconfig_listb = NULL;
                             } start
     |FONTS_WCP LEFTBRACE_WCP fonts RIGHTBRACE_WCP start
     |XFTFONTS_WCP LEFTBRACE_WCP xftfonts RIGHTBRACE_WCP start
     |CLOCKBARSETS_WCP LEFTBRACE_WCP clockbarsets RIGHTBRACE_WCP start
     |PATHJUMPSETS_WCP LEFTBRACE_WCP pathjumpsets RIGHTBRACE_WCP start
     |PATHJUMPALLOWDIRS_WCP LEFTBRACE_WCP { lconfig_pathjump_allow.clear(); }
      pjallowdirs RIGHTBRACE_WCP { lconfig->setPathJumpAllowDirs( lconfig_pathjump_allow );
                                   lconfig_pathjump_allow.clear();
                                 } start
     |DIRECTORYPRESETS_WCP LEFTBRACE_WCP { dir_presets.clear(); }
      directorypresets RIGHTBRACE_WCP { lconfig->setDirectoryPresets( dir_presets );
                                 } start
     |;

pjallowdirs:STRING_WCP';' { lconfig_pathjump_allow.push_back( $1 ); } pjallowdirs
            |;

pathjumpsets:STORE_FILES_ALWAYS_WCP'='bool';'{ lconfig->setPathJumpStoreFilesAlways( ( ( $3 ) == 1 ) ? true : false ); } pathjumpsets
            |;

glb:LANG_WCP'='STRING_WCP';' { lconfig->setLang( $3 ); } glb
   |ROWS_WCP'='NUM_WCP';'{ lconfig->setRows( $3 ); } glb
   |COLUMNS_WCP'='NUM_WCP';'{ lconfig->setColumns( $3 ); } glb
   |CACHESIZE_WCP'='NUM_WCP';'{ lconfig->setCacheSize( $3 ); } glb
   |LISTERSETS_WCP LEFTBRACE_WCP listersets RIGHTBRACE_WCP glb
   |OWNERSTYLE_WCP'='STYLE1_WCP';' { lconfig->setOwnerstringtype( 0 ); } glb
   |OWNERSTYLE_WCP'='STYLE2_WCP';'{ lconfig->setOwnerstringtype( 1 ); } glb
   |TERMINAL_WCP'='STRING_WCP';'{ lconfig->setTerminalBin( $3 ); } glb
   |TERMINAL_RETURNS_EARLY_WCP'='bool';' { lconfig->setTerminalReturnsEarly( ( ( $3 ) == 1 ) ? true : false ); } glb
   |USESTRINGFORDIRSIZE_WCP'='bool';'{ lconfig->setShowStringForDirSize( ( ( $3 ) == 1 ) ? true : false ); } glb
/*   |STRINGFORDIRSIZE'='STRING_WCP';' { lconfig->setStringForDirSize( $3 ); } glb*/
    |TIMESETS_WCP LEFTBRACE_WCP timesets RIGHTBRACE_WCP glb
    |PALETTE_WCP LEFTBRACE_WCP { lconfig->resetColors(); } pal RIGHTBRACE_WCP glb
    |LAYOUT_WCP LEFTBRACE_WCP { lconfig->clearLayoutOrders(); } layout RIGHTBRACE_WCP glb
    |MOUSECONF_WCP LEFTBRACE_WCP mouseconf RIGHTBRACE_WCP glb
    |SAVE_WORKER_STATE_ON_EXIT_WCP'='bool';'{ lconfig->setSaveWorkerStateOnExit( ( ( $3 ) == 1 ) ? true : false ); } glb
    |USE_VERSION_STRING_COMPARE_WCP'='bool';'{ lconfig->setUseStringCompareMode( ( ( $3 ) == 1 ) ? StringComparator::STRING_COMPARE_VERSION : StringComparator::STRING_COMPARE_REGULAR ); } glb
    |USE_STRING_COMPARE_MODE_WCP'='STRCASECMP_WCP';'{ lconfig->setUseStringCompareMode( StringComparator::STRING_COMPARE_NOCASE ); } glb
    |APPLY_WINDOW_DIALOG_TYPE_WCP'='bool';'{ lconfig->setApplyWindowDialogType( ( ( $3 ) == 1 ) ? true : false ); } glb
    |USE_EXTENDED_REGEX_WCP'='bool';'{ lconfig->setUseExtendedRegEx( ( ( $3 ) == 1 ) ? true : false ); } glb
    |VOLUMEMANAGER_WCP LEFTBRACE_WCP volumemanager RIGHTBRACE_WCP glb
    |DISABLEBGCHECKPREFIX_WCP'='STRING_WCP';'{ lconfig->setDisableBGCheckPrefix( $3 ); } glb
    |;

layout:STATEBAR_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_STATEBAR ); } layout
      |CLOCKBAR_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_CLOCKBAR ); } layout
      |BUTTONS_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_BUTTONS ); } layout
      |LISTVIEWS_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_LISTVIEWS ); } layout
      |BLL_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_BLL ); } layout
      |LBL_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_LBL ); } layout
      |LLB_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_LLB ); } layout
      |BL_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_BL ); } layout
      |LB_WCP';' { lconfig->layoutAddEntry( LayoutSettings::LO_LB ); } layout
      |BUTTONSVERT_WCP'='bool';'{ lconfig->setLayoutButtonVert( ( ( $3 ) == 1 ) ? true : false ); } layout
      |LISTVIEWSVERT_WCP'='bool';'{ lconfig->setLayoutListviewVert( ( ( $3 ) == 1 ) ? true : false ); } layout
      |LISTVIEWWEIGHT_WCP'='NUM_WCP';'{ lconfig->setLayoutListViewWeight( $3 ); } layout
      |WEIGHTTOACTIVE_WCP'='bool';'{ lconfig->setLayoutWeightRelToActive( ( ( $3 ) == 1 ) ? true : false ); } layout
      |;

pal:NUM_WCP'='NUM_WCP','NUM_WCP','NUM_WCP';'{ lconfig->setColor( $1, $3, $5, $7 ); } pal
   |;

listersets:LEFT_WCP{ lconfig_side = 0; } LEFTBRACE_WCP listersets2 RIGHTBRACE_WCP listersets
          |RIGHT_WCP{ lconfig_side = 1; } LEFTBRACE_WCP listersets2 RIGHTBRACE_WCP listersets
          |;

listersets2:HBARTOP_WCP'='bool';'{ lconfig->setHBarTop( lconfig_side, ( ( $3 ) == 1 ) ? true : false ); } listersets2
           |HBARHEIGHT_WCP'='NUM_WCP';'{ lconfig->setHBarHeight( lconfig_side, $3 ); } listersets2
           |VBARLEFT_WCP'='bool';'{ lconfig->setVBarLeft( lconfig_side, ( ( $3 ) == 1 ) ? true : false ); } listersets2
           |VBARWIDTH_WCP'='NUM_WCP';'{ lconfig->setVBarWidth( lconfig_side, $3 ); } listersets2
           |DISPLAYSETS_WCP LEFTBRACE_WCP { lconfig->clearVisCols( lconfig_side ); } displaysets RIGHTBRACE_WCP listersets2
           |SHOWHEADER_WCP'='bool';'{ lconfig->setShowHeader( lconfig_side, ( ( $3 ) == 1 ) ? true : false ); } listersets2
           |PATHENTRYONTOP_WCP'='bool';'{ lconfig->setPathEntryOnTop( lconfig_side, ( ( $3 ) == 1 ) ? true : false ); } listersets2
           |;

displaysets:NAME_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_NAME ); } displaysets
           |SIZE_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_SIZE ); } displaysets
           |TYPE_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_TYPE ); } displaysets
           |PERMISSION_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_PERM ); } displaysets
           |OWNER_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_OWNER ); } displaysets
           |DESTINATION_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_DEST ); } displaysets
           |MODTIME_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_MOD ); } displaysets
           |ACCTIME_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_ACC ); } displaysets
           |CHGTIME_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_CHANGE ); } displaysets
           |INODE_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_INODE ); } displaysets
           |NLINK_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_NLINK ); } displaysets
           |BLOCKS_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_BLOCKS ); } displaysets
           |SIZEH_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_SIZEH ); } displaysets
           |EXTENSION_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_EXTENSION ); } displaysets
           |CUSTOMATTRIBUTE_WCP';'{ lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_CUSTOM_ATTR ); } displaysets
           |;

timesets:DATE_WCP'='STYLE1_WCP';' { lconfig->setDateFormat( 0 ); } timesets
	|DATE_WCP'='STYLE2_WCP';' { lconfig->setDateFormat( 1 ); } timesets
	|DATE_WCP'='STYLE3_WCP';' { lconfig->setDateFormat( 2 ); } timesets
	|DATE_WCP'='USERSTYLE_WCP';' { lconfig->setDateFormat( -1 ); } timesets
        |DATESTRING_WCP'='STRING_WCP';' { lconfig->setDateFormatString( $3 ); } timesets
        |DATESUBSTITUTION_WCP'='bool';' { lconfig->setDateSubst( ( ( $3 ) == 1 ) ? true : false ); } timesets
        |TIME_WCP'='STYLE1_WCP';' { lconfig->setTimeFormat( 0 ); } timesets
        |TIME_WCP'='STYLE2_WCP';' { lconfig->setTimeFormat( 1 ); } timesets
        |TIME_WCP'='USERSTYLE_WCP';' { lconfig->setTimeFormat( -1 ); } timesets
        |TIMESTRING_WCP'='STRING_WCP';' { lconfig->setTimeFormatString( $3 ); } timesets
        |DATEBEFORETIME_WCP'='bool';' { lconfig->setDateBeforeTime( ( ( $3 ) == 1 ) ? true : false ); } timesets
	|;

mouseconf:SELECTBUTTON_WCP'='NUM_WCP';' { lconfig->setMouseSelectButton( $3 ); } mouseconf
         |ACTIVATEBUTTON_WCP'='NUM_WCP';' { lconfig->setMouseActivateButton( $3 ); } mouseconf
         |SCROLLBUTTON_WCP'='NUM_WCP';' { lconfig->setMouseScrollButton( $3 ); } mouseconf
         |SELECTMETHOD_WCP'='NORMAL_WCP';' { lconfig->setMouseSelectMethod( WConfig::MOUSECONF_NORMAL_MODE ); } mouseconf
         |SELECTMETHOD_WCP'='ALTERNATIVE_WCP';' { lconfig->setMouseSelectMethod( WConfig::MOUSECONF_ALT_MODE ); } mouseconf
         |CONTEXTBUTTON_WCP'='NUM_WCP';' { lconfig->setMouseContextButton( $3 ); } mouseconf
         |ACTIVATEMOD_WCP LEFTBRACE_WCP { lconfig_keymod = 0; }
          mods RIGHTBRACE_WCP { lconfig->setMouseActivateMod( lconfig_keymod ); } mouseconf
         |SCROLLMOD_WCP LEFTBRACE_WCP { lconfig_keymod = 0; }
          mods RIGHTBRACE_WCP { lconfig->setMouseScrollMod( lconfig_keymod ); } mouseconf
         |CONTEXTMOD_WCP LEFTBRACE_WCP { lconfig_keymod = 0; }
          mods RIGHTBRACE_WCP { lconfig->setMouseContextMod( lconfig_keymod ); } mouseconf
         |DELAYEDCONTEXTMENU_WCP'='bool';' { lconfig->setMouseContextMenuWithDelayedActivate( ( ( $3 ) == 1 ) ? true : false ); } mouseconf
	 |;

cols:STATEBAR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "statusbar-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "statusbar-bg", $5 ) );
        } cols
    |SELLVBAR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "lvb-active-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "lvb-active-bg", $5 ) );
        } cols
    |UNSELLVBAR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "lvb-inactive-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "lvb-inactive-bg", $5 ) );
        } cols
    |SELDIR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-select-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-select-bg", $5 ) );
        } cols
    |UNSELDIR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-normal-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-normal-bg", $5 ) );
        } cols
    |SELFILE_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-select-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-select-bg", $5 ) );
        } cols
    |UNSELFILE_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-normal-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-normal-bg", $5 ) );
        } cols
    |CLOCKBAR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "clockbar-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "clockbar-bg", $5 ) );
        } cols
    |REQUESTER_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "request-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "request-bg", $5 ) );
        } cols
    |SELACTDIR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-selact-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-selact-bg", $5 ) );
        } cols
    |UNSELACTDIR_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-active-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-active-bg", $5 ) );
        } cols
    |SELACTFILE_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-selact-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-selact-bg", $5 ) );
        } cols
    |UNSELACTFILE_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-active-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-active-bg", $5 ) );
        } cols
    |LVHEADER_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-header-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-header-bg", $5 ) );
        } cols
    |LVBG_WCP'='NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "dirview-bg", $3 ) );
        } cols
    |TEXTVIEW_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "textview-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-bg", $5 ) );
        } cols
    |TEXTVIEWHIGHLIGHTED_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "textview-highlight-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-highlight-bg", $5 ) );
        } cols
    |TEXTVIEWSELECTION_WCP'='NUM_WCP','NUM_WCP';' {
        lconfig_faces_list.push_back( std::make_pair( "textview-selection-fg", $3 ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-selection-bg", $5 ) );
        } cols
    |LABELCOLORS_WCP LEFTBRACE_WCP { lconfig_labelcolors.clear(); }
        labelcolors RIGHTBRACE_WCP {
          WConfig::ColorDef c = lconfig->getColorDefs();
          c.setLabelColors( lconfig_labelcolors );
          lconfig->setColorDefs( c );
        } cols
    |FACES_WCP LEFTBRACE_WCP
        faces RIGHTBRACE_WCP cols
    |;

labelcolors:COLOR_WCP LEFTBRACE_WCP {
              lconfig_labelcolor.normal_fg = -1;
              lconfig_labelcolor.normal_bg = -1;
              lconfig_labelcolor.active_fg = -1;
              lconfig_labelcolor.active_bg = -1;
              lconfig_labelcolor_name = "";
              lconfig_labelcolor.only_exact_path = false;
            }
            labelcolor RIGHTBRACE_WCP {
              lconfig_labelcolors[lconfig_labelcolor_name] = lconfig_labelcolor;
            } labelcolors
           |;

labelcolor:NORMAL_WCP'='NUM_WCP','NUM_WCP';' {
             lconfig_labelcolor.normal_fg = $3;
             lconfig_labelcolor.normal_bg = $5;
            } labelcolor
          |ACTIVE_WCP'='NUM_WCP','NUM_WCP';' {
             lconfig_labelcolor.active_fg = $3;
             lconfig_labelcolor.active_bg = $5;
            } labelcolor
          |NAME_WCP '=' STRING_WCP';' { lconfig_labelcolor_name = $3; } labelcolor
          |ONLYEXACTPATH_WCP'='bool';' { lconfig_labelcolor.only_exact_path = ( ( $3 ) == 1 ) ? true : false; } labelcolor
          |;

faces:FACE_WCP LEFTBRACE_WCP {
              lconfig_face_name.clear();
              lconfig_face_color = -1;
            }
            face RIGHTBRACE_WCP {
              lconfig_faces_list.push_back( std::make_pair( lconfig_face_name, lconfig_face_color ) );
            } faces
           |;

face:COLOR_WCP'='NUM_WCP';' {
             lconfig_face_color = $3;
            } face
          |NAME_WCP '=' STRING_WCP';' { lconfig_face_name = $3; } face
          |;

startup:LEFT_WCP'='STRING_WCP';' { lconfig->setStartDir( 0, $3 ); } startup
       |RIGHT_WCP'='STRING_WCP';' { lconfig->setStartDir( 1, $3 ); } startup
       |RESTORE_TABS_MODE_WCP'='NEVER_WCP';' { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_NEVER ); } startup
       |RESTORE_TABS_MODE_WCP'='ALWAYS_WCP';' { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_ALWAYS ); } startup
       |RESTORE_TABS_MODE_WCP'='ASK_WCP';' { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_ASK ); } startup
       |STORE_TABS_MODE_WCP'='NEVER_WCP';' { lconfig->setStoreTabsMode( WConfig::STORE_TABS_NEVER ); } startup
       |STORE_TABS_MODE_WCP'='ALWAYS_WCP';' { lconfig->setStoreTabsMode( WConfig::STORE_TABS_ALWAYS ); } startup
       |STORE_TABS_MODE_WCP'='AS_EXIT_STATE_WCP';' { lconfig->setStoreTabsMode( WConfig::STORE_TABS_AS_EXIT_STATE ); } startup
       |STORE_TABS_MODE_WCP'='ASK_WCP';' { lconfig->setStoreTabsMode( WConfig::STORE_TABS_ASK ); } startup
       |;

paths:PATH_WCP LEFTBRACE_WCP { lconfig_p1 = new WCPath(); }
      path RIGHTBRACE_WCP { lconfig_p1->setCheck( true );
                        while ( lconfig_listp->size() < ( lconfig_pos + 1 ) ) lconfig_listp->addElement( new WCPath() );
                        lconfig_p1 = (WCPath*)lconfig_listp->exchangeElement( lconfig_pos, lconfig_p1 );
                        delete lconfig_p1;
                        lconfig_p1 = NULL;
                      } paths
     |;

path:POSITION_WCP'='NUM_WCP';' { lconfig_pos = $3;
                         if ( lconfig_pos < 0 ) lconfig_pos = 0;
                         if ( lconfig_pos > 10240 ) lconfig_pos = 10240;
                       } path
    |TITLE_WCP'='STRING_WCP';' { lconfig_p1->setName( $3 ); } path
    |COLOR_WCP'='NUM_WCP','NUM_WCP';' { lconfig_p1->setFG( $3 );
                            lconfig_p1->setBG( $5 );
                          } path
    |PATH_WCP'='STRING_WCP';' { lconfig_p1->setPath( $3 ); } path
    |SHORTKEYS_WCP LEFTBRACE_WCP { lconfig_listsk = new List(); }
     keylist RIGHTBRACE_WCP { lconfig_p1->setDoubleKeys( lconfig_listsk );
                              lconfig_id = lconfig_listsk->initEnum();
                              lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                              while ( lconfig_dk != NULL ) {
                                delete lconfig_dk;
                                lconfig_listsk->removeFirstElement();
                                lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                              }
                              lconfig_listsk->closeEnum( lconfig_id );
                              delete lconfig_listsk;
                              lconfig_listsk = NULL;
                            } path
    |;

keylist:NORMAL_WCP LEFTBRACE_WCP { lconfig_dk = new WCDoubleShortkey();
                                   lconfig_dk->setType( WCDoubleShortkey::WCDS_NORMAL );
                                 }
        KEY_WCP'='STRING_WCP';' { lconfig_nkeysym = AGUIX::getKeySymForString( $6 );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 0 );
                          else lconfig_dk->setKeySym( 0, 0 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
        mods { lconfig_dk->setMod( lconfig_keymod, 0 ); }
        RIGHTBRACE_WCP { lconfig_listsk->addElement( lconfig_dk ); }
        keylist
       |DOUBLE_WCP LEFTBRACE_WCP { lconfig_dk = new WCDoubleShortkey();
                                   lconfig_dk->setType( WCDoubleShortkey::WCDS_DOUBLE );
                                  }
        KEY_WCP'='STRING_WCP';' { lconfig_nkeysym = AGUIX::getKeySymForString( $6 );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 0 );
                          else lconfig_dk->setKeySym( 0, 0 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
        mods { lconfig_dk->setMod( lconfig_keymod, 0 ); /*TODO:finzt das so mit dem lconfig_keymod?*/}
        KEY_WCP'='STRING_WCP';' { lconfig_nkeysym = AGUIX::getKeySymForString( $13 );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 1 );
                          else lconfig_dk->setKeySym( 0, 1 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
        mods { lconfig_dk->setMod( lconfig_keymod, 1 ); }
        RIGHTBRACE_WCP { lconfig_listsk->addElement( lconfig_dk ); }
        keylist
       |;

mods:MOD_WCP'='CONTROL_WCP';' { lconfig_keymod |= ControlMask; } mods
    |MOD_WCP'='SHIFT_WCP';' { lconfig_keymod |= ShiftMask; } mods
    |MOD_WCP'='LOCK_WCP';' { lconfig_keymod |= LockMask; } mods
    |MOD_WCP'='MOD1_WCP';' { lconfig_keymod |= Mod1Mask; } mods
    |MOD_WCP'='MOD2_WCP';' { lconfig_keymod |= Mod2Mask; } mods
    |MOD_WCP'='MOD3_WCP';' { lconfig_keymod |= Mod3Mask; } mods
    |MOD_WCP'='MOD4_WCP';' { lconfig_keymod |= Mod4Mask; } mods
    |MOD_WCP'='MOD5_WCP';' { lconfig_keymod |= Mod5Mask; } mods
    |;

filetypes:FILETYPE_WCP LEFTBRACE_WCP { lconfig_ft1 = new WCFiletype();
                                       lconfig_ignorelist.clear();
                                     }
          filetype RIGHTBRACE_WCP { lconfig_listft->addElement( lconfig_ft1 );
                                lconfig_ft1 = NULL;
                              } filetypes
         |IGNOREDIRS_WCP LEFTBRACE_WCP { lconfig_ignorelist.remove_if( []( const auto &d ){ return ! d.is_pattern; } ); }
          ignoredirs RIGHTBRACE_WCP { lconfig->setDontCheckDirs( lconfig_ignorelist );
                                    } filetypes
         |IGNOREDIRS2_WCP LEFTBRACE_WCP { lconfig_ignorelist.remove_if( []( const auto &d ){ return d.is_pattern; } ); }
          ignoredirs2 RIGHTBRACE_WCP { lconfig->setDontCheckDirs( lconfig_ignorelist );
                                     } filetypes
	     |DONTCHECKVIRTUAL_WCP'='bool';' { lconfig->setDontCheckVirtual( ( ( $3 ) == 1 ) ? true : false ); } filetypes
         |;

ignoredirs:STRING_WCP';' { lconfig_ignorelist.push_back( { .is_pattern = false, .dir = $1 } ); } ignoredirs
          |;

ignoredirs2:IGNOREDIR_WCP LEFTBRACE_WCP { lconfig_ignore_dir = {}; }
            ignoredir RIGHTBRACE_WCP { lconfig_ignorelist.push_back( lconfig_ignore_dir ); } ignoredirs2
          |;

ignoredir:PATTERN_WCP'='STRING_WCP';' { lconfig_ignore_dir = { .is_pattern = true, .dir = $3 }; } ignoredir
         |;

subfiletypes:FILETYPE_WCP LEFTBRACE_WCP { filetypehier.push_back( lconfig_ft1 );
                                          lconfig_ft1 = new WCFiletype();
                                        }
             filetype RIGHTBRACE_WCP { old_lconfig_ft1 = filetypehier.back();
	                               filetypehier.pop_back();
				       if ( old_lconfig_ft1 != NULL ) {
					 old_lconfig_ft1->addSubType( lconfig_ft1 );
				       } else {
					 delete lconfig_ft1;
				       }
				       lconfig_ft1 = old_lconfig_ft1;
	                             } subfiletypes
            |;

filetype:TYPE_WCP'='ft_type';' { lconfig_ft1->setinternID( $3 ); } filetype
        |TITLE_WCP'='STRING_WCP';' { lconfig_ft1->setName( $3 ); } filetype
        |PATTERN_WCP'='STRING_WCP';' { lconfig_ft1->setPattern( $3 ); } filetype
        |USEPATTERN_WCP'='bool';' { lconfig_ft1->setUsePattern( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |CONTENT_WCP LEFTBRACE_WCP { lconfig_ft1->clearFiledesc(); }
         filecontent RIGHTBRACE_WCP filetype
        |USECONTENT_WCP'='bool';' { lconfig_ft1->setUseFiledesc( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |PATTERNIGNORECASE_WCP'='bool';' { lconfig_ft1->setPatternIgnoreCase( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |PATTERNUSEREGEXP_WCP'='bool';' { lconfig_ft1->setPatternUseRegExp( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |PATTERNUSEFULLNAME_WCP'='bool';' { lconfig_ft1->setPatternUseFullname( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |FTCOMMANDS_WCP LEFTBRACE_WCP ftcommands RIGHTBRACE_WCP filetype
        |EXTCOND_WCP'='STRING_WCP';' { lconfig_ft1->setExtCond( $3 ); } filetype
        |USEEXTCOND_WCP'='bool';' { lconfig_ft1->setUseExtCond( ( ( $3 ) == 1 ) ? true : false ); } filetype
	|SUBTYPE_WCP LEFTBRACE_WCP subfiletypes RIGHTBRACE_WCP filetype
	|UNSELECTCOLOR_WCP'='NUM_WCP','NUM_WCP';' { lconfig_ft1->setCustomColor( 0, 0, $3 );
	                                            lconfig_ft1->setCustomColor( 1, 0, $5 );
                                                  } filetype
	|SELECTCOLOR_WCP'='NUM_WCP','NUM_WCP';' { lconfig_ft1->setCustomColor( 0, 1, $3 );
	                                          lconfig_ft1->setCustomColor( 1, 1, $5 );
                                                } filetype
	|UNSELECTACTIVECOLOR_WCP'='NUM_WCP','NUM_WCP';' { lconfig_ft1->setCustomColor( 0, 2, $3 );
	                                                  lconfig_ft1->setCustomColor( 1, 2, $5 );
                                                        } filetype
	|SELECTACTIVECOLOR_WCP'='NUM_WCP','NUM_WCP';' { lconfig_ft1->setCustomColor( 0, 3, $3 );
	                                                lconfig_ft1->setCustomColor( 1, 3, $5 );
                                                      } filetype
        |COLOREXTERNPROG_WCP'='STRING_WCP';' { lconfig_ft1->setColorExt( $3 ); } filetype
        |COLORMODE_WCP'='DEFAULT_WCP';' { lconfig_ft1->setColorMode( WCFiletype::DEFAULTCOL ); } filetype
        |COLORMODE_WCP'='CUSTOM_WCP';' { lconfig_ft1->setColorMode( WCFiletype::CUSTOMCOL ); } filetype
        |COLORMODE_WCP'='EXTERN_WCP';' { lconfig_ft1->setColorMode( WCFiletype::EXTERNALCOL ); } filetype
        |COLORMODE_WCP'='PARENT_WCP';' { lconfig_ft1->setColorMode( WCFiletype::PARENTCOL ); } filetype
        |MAGICPATTERN_WCP'='STRING_WCP';' { lconfig_ft1->setMagicPattern( $3 ); } filetype
        |USEMAGIC_WCP'='bool';' { lconfig_ft1->setUseMagic( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |MAGICCOMPRESSED_WCP'='bool';' { lconfig_ft1->setMagicCompressed( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |MAGICMIME_WCP'='bool';' { lconfig_ft1->setMagicMime( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |MAGICIGNORECASE_WCP'='bool';' { lconfig_ft1->setMagicIgnoreCase( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |PATTERNISCOMMASEPARATED_WCP'='bool';' { lconfig_ft1->setPatternIsCommaSeparated( ( ( $3 ) == 1 ) ? true : false ); } filetype
        |PRIORITY_WCP'='NUM_WCP';' { lconfig_ft1->setPriority( $3 ); } filetype
        |;

filecontent:NUM_WCP'='NUM_WCP';' { lconfig_ft1->setFiledesc( $1, $3 ); } filecontent
           |;

ft_type:NORMAL_WCP { $$ = NORMALTYPE; }
       |NOTYETCHECKED_WCP { $$ = NOTYETTYPE; }
       |UNKNOWN_WCP { $$ = UNKNOWNTYPE; }
       |NOSELECT_WCP { $$ = VOIDTYPE; }
       |DIR_WCP { $$ = DIRTYPE; }
       ;

ftcommands:DND_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
           commands RIGHTBRACE_WCP { lconfig_ft1->setDNDActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               } ftcommands
          |DC_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
           commands RIGHTBRACE_WCP { lconfig_ft1->setDoubleClickActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               } ftcommands
          |SHOW_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
           commands RIGHTBRACE_WCP { lconfig_ft1->setShowActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               } ftcommands
          |RAWSHOW_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
           commands RIGHTBRACE_WCP { lconfig_ft1->setRawShowActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               } ftcommands
          |USER_WCP NUM_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
           commands RIGHTBRACE_WCP { lconfig_ft1->setUserActions( $2, lconfig_listcom );
                                     lconfig_listcom.clear();
                               } ftcommands
          |CUSTOMACTION_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
           NAME_WCP'='STRING_WCP';'
           COMMANDS_WCP LEFTBRACE_WCP
           commands RIGHTBRACE_WCP RIGHTBRACE_WCP { lconfig_ft1->setCustomActions( $6, lconfig_listcom );
                                                    lconfig_listcom.clear();
                               } ftcommands
          |;

commands:command commands
        |FLAGS_WCP LEFTBRACE_WCP flags RIGHTBRACE_WCP commands
	|;

flags:;

hotkeys:HOTKEY_WCP LEFTBRACE_WCP { lconfig_hk1 = new WCHotkey(); }
        hotkey RIGHTBRACE_WCP { lconfig_listh->addElement( lconfig_hk1 );
                            lconfig_hk1 = NULL;
                          } hotkeys
       |;

hotkey:TITLE_WCP'='STRING_WCP';' { lconfig_hk1->setName( $3 ); } hotkey
      |COMMANDS_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
       commands RIGHTBRACE_WCP { lconfig_hk1->setComs( lconfig_listcom );
                                 lconfig_listcom.clear();
                           } hotkey
      |SHORTKEYS_WCP LEFTBRACE_WCP { lconfig_listsk = new List(); }
       keylist RIGHTBRACE_WCP { lconfig_hk1->setDoubleKeys( lconfig_listsk );
                            lconfig_id = lconfig_listsk->initEnum();
                            lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            while ( lconfig_dk != NULL ) {
                              delete lconfig_dk;
                              lconfig_listsk->removeFirstElement();
                              lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            }
                            lconfig_listsk->closeEnum( lconfig_id );
                            delete lconfig_listsk;
                            lconfig_listsk = NULL;
                          } hotkey
      |;

buttons:BUTTON_WCP LEFTBRACE_WCP { lconfig_bt1 = new WCButton(); }
        button RIGHTBRACE_WCP { lconfig_bt1->setCheck( true );
                            while ( lconfig_listb->size() < ( lconfig_pos + 1 ) ) lconfig_listb->addElement( new WCButton() );
                            lconfig_bt1 = (WCButton*)lconfig_listb->exchangeElement( lconfig_pos, lconfig_bt1 );
                            delete lconfig_bt1;
                            lconfig_bt1 = NULL;
                          } buttons
       |;

button:POSITION_WCP'='NUM_WCP';' { lconfig_pos = $3;
                           if ( lconfig_pos < 0 ) lconfig_pos = 0;
                           if ( lconfig_pos > 100000 ) lconfig_pos = 100000;
                         } button
      |TITLE_WCP'='STRING_WCP';'{ lconfig_bt1->setText( $3 ); } button
      |COLOR_WCP'='NUM_WCP','NUM_WCP';' { lconfig_bt1->setFG( $3 );
                              lconfig_bt1->setBG( $5 );
                            } button
      |COMMANDS_WCP LEFTBRACE_WCP { lconfig_listcom.clear(); }
       commands RIGHTBRACE_WCP { lconfig_bt1->setComs( lconfig_listcom );
                                 lconfig_listcom.clear();
                           } button
      |SHORTKEYS_WCP LEFTBRACE_WCP { lconfig_listsk = new List(); }
       keylist RIGHTBRACE_WCP { lconfig_bt1->setDoubleKeys( lconfig_listsk );
                            lconfig_id = lconfig_listsk->initEnum();
                            lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            while ( lconfig_dk != NULL ) {
                              delete lconfig_dk;
                              lconfig_listsk->removeFirstElement();
                             lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            }
                            lconfig_listsk->closeEnum( lconfig_id );
                            delete lconfig_listsk;
                            lconfig_listsk = NULL;
                          } button
      |;

fonts:GLOBALFONT_WCP'='STRING_WCP';'
      {
#ifndef HAVE_XFT
        lconfig->setFont( 0, $3 );
#endif
        lconfig->setFontName( WConfig::FONT_X11_GLOBAL, $3 );
      } fonts
     |BUTTONFONT_WCP'='STRING_WCP';'
      {
#ifndef HAVE_XFT
        lconfig->setFont( 1, $3 );
#endif
        lconfig->setFontName( WConfig::FONT_X11_BUTTONS, $3 );
      } fonts
     |LEFTFONT_WCP'='STRING_WCP';'
      {
#ifndef HAVE_XFT
        lconfig->setFont( 2, $3 );
#endif
        lconfig->setFontName( WConfig::FONT_X11_LEFT, $3 );
      } fonts
     |RIGHTFONT_WCP'='STRING_WCP';'
      {
#ifndef HAVE_XFT
        lconfig->setFont( 3, $3 );
#endif
        lconfig->setFontName( WConfig::FONT_X11_RIGHT, $3 );
      } fonts
     |TEXTVIEWFONT_WCP'='STRING_WCP';'
      {
#ifndef HAVE_XFT
        lconfig->setFont( 4, $3 );
#endif
        lconfig->setFontName( WConfig::FONT_X11_TEXTVIEW, $3 );
      } fonts
     |STATEBARFONT_WCP'='STRING_WCP';'
      {
#ifndef HAVE_XFT
        lconfig->setFont( 5, $3 );
#endif
        lconfig->setFontName( WConfig::FONT_X11_STATEBAR, $3 );
      } fonts
     |TEXTVIEWALTFONT_WCP'='STRING_WCP';'
      {
#ifndef HAVE_XFT
        lconfig->setFont( 6, $3 );
#endif
        lconfig->setFontName( WConfig::FONT_X11_TEXTVIEW_ALT, $3 );
      } fonts
     |;

xftfonts:GLOBALFONT_WCP'='STRING_WCP';'
         {
#ifdef HAVE_XFT
           lconfig->setFont( 0, $3 );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_GLOBAL, $3 );
         } xftfonts
        |BUTTONFONT_WCP'='STRING_WCP';'
         {
#ifdef HAVE_XFT
           lconfig->setFont( 1, $3 );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_BUTTONS, $3 );
         } xftfonts
        |LEFTFONT_WCP'='STRING_WCP';'
         {
#ifdef HAVE_XFT
           lconfig->setFont( 2, $3 );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_LEFT, $3 );
         } xftfonts
        |RIGHTFONT_WCP'='STRING_WCP';'
         {
#ifdef HAVE_XFT
           lconfig->setFont( 3, $3 );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_RIGHT, $3 );
         } xftfonts
        |TEXTVIEWFONT_WCP'='STRING_WCP';'
         {
#ifdef HAVE_XFT
           lconfig->setFont( 4, $3 );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_TEXTVIEW, $3 );
         } xftfonts
        |STATEBARFONT_WCP'='STRING_WCP';'
         {
#ifdef HAVE_XFT
           lconfig->setFont( 5, $3 );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_STATEBAR, $3 );
         } xftfonts
        |TEXTVIEWALTFONT_WCP'='STRING_WCP';'
         {
#ifdef HAVE_XFT
           lconfig->setFont( 6, $3 );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_TEXTVIEW_ALT, $3 );
         } xftfonts
        |;

volumemanager:MOUNTCOMMAND_WCP'='STRING_WCP';'
              {
                  lconfig->setVMMountCommand( $3 );
              } volumemanager
             |UNMOUNTCOMMAND_WCP'='STRING_WCP';'
              {
                  lconfig->setVMUnmountCommand( $3 );
              } volumemanager
             |EJECTCOMMAND_WCP'='STRING_WCP';'
              {
                  lconfig->setVMEjectCommand( $3 );
              } volumemanager
             |CLOSETRAYCOMMAND_WCP'='STRING_WCP';'
              {
                  lconfig->setVMCloseTrayCommand( $3 );
              } volumemanager
             |FSTABFILE_WCP'='STRING_WCP';'
              {
                  lconfig->setVMFStabFile( $3 );
              } volumemanager
             |MTABFILE_WCP'='STRING_WCP';'
              {
                  lconfig->setVMMtabFile( $3 );
              } volumemanager
             |PARTFILE_WCP'='STRING_WCP';'
              {
                  lconfig->setVMPartitionFile( $3 );
              } volumemanager
             |REQUESTACTION_WCP'='bool';'
              {
                  lconfig->setVMRequestAction( ( ( $3 ) == 1 ) ? true : false );
              } volumemanager
             |PREFERUDISKSVERSION_WCP'='NUM_WCP';'
              {
                  lconfig->setVMPreferredUdisksVersion( $3 );
              } volumemanager
             |;

clockbarsets:MODUS_WCP'='TIMESPACE_WCP';' { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_TIMERAM ); } clockbarsets
            |MODUS_WCP'='TIME_WCP';' { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_TIME ); } clockbarsets
            |MODUS_WCP'='VERSION_WCP';' { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_VERSION ); } clockbarsets
            |MODUS_WCP'='EXTERN_WCP';' { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_EXTERN ); } clockbarsets
            |UPDATETIME_WCP'='NUM_WCP';' { lconfig->setClockbarUpdatetime( $3 ); } clockbarsets
            |PROGRAM_WCP'='STRING_WCP';' { lconfig->setClockbarCommand( $3 ); } clockbarsets
            |SHOWHINTS_WCP'='bool';' { lconfig->setClockbarHints( ( ( $3 ) == 1 ) ? true : false ); } clockbarsets
            |;

bool:YES_WCP { $$ = 1; }
    |NO_WCP { $$ = 0; };

command:ownop
       |dndaction
       |dcaction
       |showaction
       |rshowaction
       |useraction
       |rowup
       |rowdown
       |changehiddenflag
       |copyop
       |firstrow
       |lastrow
       |pageup
       |pagedown
       |selectop
       |selectallop
       |selectnoneop
       |invertallop
       |parentdirop
       |enterdirop
       |changelistersetop
       |switchlisterop
       |filterselectop
       |filterunselectop
       |pathtoothersideop
       |quitop
       |deleteop
       |reloadop
       |makedirop
       |renameop
       |dirsizeop
       |simddop
       |startprogop
       |searchentryop
       |enterpathop
       |scrolllisterop
       |createsymlinkop
       |changesymlinkop
       |chmodop
       |togglelistermodeop
       |setsortmodeop
       |setfilterop
       |shortkeyfromlistop
       |chownop
       |scriptop
       |showdircacheop
       |parentactionop
       |nooperationop
       |goftpop
       |internalviewop
       |searchop
       |dirbookmarkop
       |opencontextmenuop
       |runcustomaction
       |openworkermenuop
       |changelabelop
       |modifytabsop
       |changelayoutop
       |volumemanagerop
       |switchbuttonbankop
       |pathjumpop
       |clipboardop
       |commandmenuop
       |chtimeop
       |changecolumnsop
       |viewnewestfilesop
       |dircompareop
       |tabprofilesop
       |externalvdirop
       |opentabmenuop
       |helpop
       |viewcommandlogop
       |activatetextviewmodeop;

ownop:OWNOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared< OwnOp >(); }
      ownopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

ownopbody:COM_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setComStr( $3 ); } ownopbody
         |SEPARATEEACHENTRY_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setSeparateEachEntry( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |RECURSIVE_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setRecursive( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |START_WCP'='TERMINAL_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_IN_TERMINAL ); } ownopbody
         |START_WCP'='TERMINALWAIT_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_IN_TERMINAL_AND_WAIT4KEY ); } ownopbody
         |START_WCP'='SHOWOUTPUT_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT ); } ownopbody
         |START_WCP'='SHOWOUTPUTINT_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_INT ); } ownopbody
         |START_WCP'='SHOWOUTPUTOTHERSIDE_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_OTHER_SIDE ); } ownopbody
         |START_WCP'='SHOWOUTPUTCUSTOMATTRIBUTE_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_CUSTOM_ATTRIBUTE ); } ownopbody
         |START_WCP'='NORMAL_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_NORMAL ); } ownopbody
         |VIEWSTR_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setViewStr( $3 ); } ownopbody
         |INBACKGROUND_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setInBackground( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |TAKEDIRS_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setTakeDirs( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |DONTCD_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setDontCD( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |USE_VIRTUAL_TEMP_COPIES_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setUseVirtualTempCopies( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setFlagsAreRelativeToBaseDir( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |FOLLOWACTIVE_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setFollowActive( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |WATCHFILE_WCP'='bool';' { std::dynamic_pointer_cast<OwnOp>(ps.lconfig_fp1)->setWatchFile( ( ( $3 ) == 1 ) ? true : false ); } ownopbody
         |;

dndaction:DNDACTION_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<DNDAction>(); }
          RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

dcaction:DCACTION_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<DoubleClickAction>(); }
         RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

showaction:SHOWACTION_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ShowAction>(); }
           RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

rshowaction:RSHOWACTION_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<RawShowAction>(); }
            RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

useraction:USERACTION_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<UserAction>(); }
           useractionbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

useractionbody:ACTIONNUMBER_WCP'='NUM_WCP';' { std::dynamic_pointer_cast<UserAction>(ps.lconfig_fp1)->setNr( $3 ); } useractionbody
              |;

rowup:ROWUP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                tfp->setCommandStr( "up" );
                                ps.lconfig_fp1 = tfp;
                              }
      RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

rowdown:ROWDOWN_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "down" );
                                    ps.lconfig_fp1 = tfp;
                                  }
        RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

changehiddenflag:CHANGEHIDDENFLAG_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ChangeHiddenFlag>(); }
                 changehiddenflagbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

changehiddenflagbody:HIDDENFILES_WCP'='SHOW_WCP';' { std::dynamic_pointer_cast<ChangeHiddenFlag>(ps.lconfig_fp1)->setMode( 2 ); } changehiddenflagbody
                    |HIDDENFILES_WCP'='HIDE_WCP';' { std::dynamic_pointer_cast<ChangeHiddenFlag>(ps.lconfig_fp1)->setMode( 1 ); } changehiddenflagbody
                    |HIDDENFILES_WCP'='TOGGLE_WCP';' { std::dynamic_pointer_cast<ChangeHiddenFlag>(ps.lconfig_fp1)->setMode( 0 ); } changehiddenflagbody
                    |;

copyop:COPYOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<CopyOp>(); }
       copyopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

copyopbody:FOLLOWSYMLINKS_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setFollowSymlinks( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |MOVE_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setMove( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |RENAME_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setRename( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |SAMEDIR_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setSameDir( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |REQUESTDEST_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setRequestDest( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |OVERWRITE_WCP'='ALWAYS_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_ALWAYS ); } copyopbody
          |OVERWRITE_WCP'='NEVER_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_NEVER ); } copyopbody
          |OVERWRITE_WCP'='NORMAL_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_NORMAL ); } copyopbody
          |COPYMODE_WCP'='NORMAL_WCP';' {} copyopbody
          |COPYMODE_WCP'='FAST_WCP';' {} copyopbody
          |PRESERVEATTR_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setPreserveAttr( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |ADJUSTRELATIVESYMLINKS_WCP'='NEVER_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_NEVER ); } copyopbody
          |ADJUSTRELATIVESYMLINKS_WCP'='OUTSIDE_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_OUTSIDE ); } copyopbody
          |ADJUSTRELATIVESYMLINKS_WCP'='ALWAYS_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_ALWAYS ); } copyopbody
          |ENSURE_FILE_PERMISSIONS_WCP'='LEAVE_UNMODIFIED_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( LEAVE_PERMISSIONS_UNMODIFIED ); } copyopbody
          |ENSURE_FILE_PERMISSIONS_WCP'='USER_RW_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_PERMISSION ); } copyopbody
          |ENSURE_FILE_PERMISSIONS_WCP'='USER_RW_GROUP_R_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_GROUP_R_PERMISSION ); } copyopbody
          |ENSURE_FILE_PERMISSIONS_WCP'='USER_RW_ALL_R_WCP';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_ALL_R_PERMISSION ); } copyopbody
          |VDIR_PRESERVE_DIR_STRUCTURE_WCP'='bool';' { std::dynamic_pointer_cast<CopyOp>(ps.lconfig_fp1)->setVdirPreserveDirStructure( ( ( $3 ) == 1 ) ? true : false ); } copyopbody
          |;

firstrow:FIRSTROW_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "first" );
                                      ps.lconfig_fp1 = tfp;
                                    }
         RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

lastrow:LASTROW_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "last" );
                                    ps.lconfig_fp1 = tfp;
                                  }
        RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

pageup:PAGEUP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                  tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                  tfp->setCommandStr( "pageup" );
                                  ps.lconfig_fp1 = tfp;
                                }
       RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

pagedown:PAGEDOWN_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "pagedown" );
                                      ps.lconfig_fp1 = tfp;
                                    }
         RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

selectop:SELECTOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "selectentry" );
                                      ps.lconfig_fp1 = tfp;
                                    }
         RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

selectallop:SELECTALLOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "selectall" );
                                            ps.lconfig_fp1 = tfp;
                                          }
            RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

selectnoneop:SELECTNONEOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                              tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                              tfp->setCommandStr( "selectnone" );
                                              ps.lconfig_fp1 = tfp;
                                            }
             RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

invertallop:INVERTALLOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "invertall" );
                                            ps.lconfig_fp1 = tfp;
                                          }
            RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

parentdirop:PARENTDIROP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "parentdir" );
                                            ps.lconfig_fp1 = tfp;
                                          }
            RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

enterdirop:ENTERDIROP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<EnterDirOp>(); }
           enterdiropbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

enterdiropbody:DIR_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setDir( $3 ); } enterdiropbody
              |MODE_WCP'='ACTIVE_WCP';' { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_ACTIVE ); } enterdiropbody
              |MODE_WCP'='ACTIVE2OTHER_WCP';' { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_ACTIVE2OTHER ); } enterdiropbody
              |MODE_WCP'='SPECIAL_WCP';' { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_SPECIAL ); } enterdiropbody
              |MODE_WCP'='REQUEST_WCP';' { std::dynamic_pointer_cast<EnterDirOp>(ps.lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_REQUEST ); } enterdiropbody
              |;

changelistersetop:CHANGELISTERSETOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ChangeListerSetOp>(); }
                  changelistersetopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

changelistersetopbody:MODE_WCP'='LEFT_WCP';' { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_LEFT_LISTER ); } changelistersetopbody
                     |MODE_WCP'='RIGHT_WCP';' { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_RIGHT_LISTER ); } changelistersetopbody
                     |MODE_WCP'='CURRENT_WCP';' { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_CURRENT_LISTER ); } changelistersetopbody
                     |MODE_WCP'='OTHER_WCP';' { std::dynamic_pointer_cast<ChangeListerSetOp>(ps.lconfig_fp1)->setMode( ChangeListerSetOp::CLS_OTHER_LISTER ); } changelistersetopbody
                     |;

switchlisterop:SWITCHLISTEROP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<SwitchListerOp>(); }
               RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

filterselectop:FILTERSELECTOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<FilterSelectOp>(); }
               filterselectopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

filterselectopbody:FILTER_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<FilterSelectOp>(ps.lconfig_fp1)->setFilter( $3 ); } filterselectopbody
                  |AUTOFILTER_WCP'='bool';' { std::dynamic_pointer_cast<FilterSelectOp>(ps.lconfig_fp1)->setAutoFilter( ( ( $3 ) == 1 ) ? true : false ); } filterselectopbody
                  |IMMEDIATE_FILTER_APPLY_WCP'='bool';' { std::dynamic_pointer_cast<FilterSelectOp>(ps.lconfig_fp1)->setImmediateFilterApply( ( ( $3 ) == 1 ) ? true : false ); } filterselectopbody
                  |;

filterunselectop:FILTERUNSELECTOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<FilterUnSelectOp>(); }
                 filterunselectopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

filterunselectopbody:FILTER_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<FilterUnSelectOp>(ps.lconfig_fp1)->setFilter( $3 ); } filterunselectopbody
                    |AUTOFILTER_WCP'='bool';' { std::dynamic_pointer_cast<FilterUnSelectOp>(ps.lconfig_fp1)->setAutoFilter( ( ( $3 ) == 1 ) ? true : false ); } filterunselectopbody
                    |IMMEDIATE_FILTER_APPLY_WCP'='bool';' { std::dynamic_pointer_cast<FilterUnSelectOp>(ps.lconfig_fp1)->setImmediateFilterApply( ( ( $3 ) == 1 ) ? true : false ); } filterunselectopbody
                    |;

pathtoothersideop:PATHTOOTHERSIDEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<Path2OSideOp>(); }
                  RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

quitop:QUITOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<QuitOp>(); }
       quitopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

quitopbody:MODE_WCP'='NORMAL_WCP';' { std::dynamic_pointer_cast<QuitOp>(ps.lconfig_fp1)->setMode( QuitOp::Q_NORMAL_QUIT ); } quitopbody
          |MODE_WCP'='QUICK_WCP';' { std::dynamic_pointer_cast<QuitOp>(ps.lconfig_fp1)->setMode( QuitOp::Q_QUICK_QUIT ); } quitopbody
          |;

deleteop:DELETEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<DeleteOp>(); }
         deleteopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

deleteopbody:ALSOACTIVE_WCP'='bool';' { std::dynamic_pointer_cast<DeleteOp>(ps.lconfig_fp1)->setAlsoActive( ( ( $3 ) == 1 ) ? true : false ); } deleteopbody
            |;

reloadop:RELOADOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ReloadOp>(); }
         reloadopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

reloadopbody:MODE_WCP'='LEFT_WCP';' { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_LEFTSIDE ); } reloadopbody
            |MODE_WCP'='RIGHT_WCP';' { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_RIGHTSIDE ); } reloadopbody
            |MODE_WCP'='CURRENT_WCP';' { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_THISSIDE ); } reloadopbody
            |MODE_WCP'='OTHER_WCP';' { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_OTHERSIDE ); } reloadopbody
            |RESETDIRSIZES_WCP'='bool';' { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setResetDirSizes( ( ( $3 ) == 1 ) ? true : false ); } reloadopbody
            |KEEPFILETYPES_WCP'='bool';' { std::dynamic_pointer_cast<ReloadOp>(ps.lconfig_fp1)->setKeepFiletypes( ( ( $3 ) == 1 ) ? true : false ); } reloadopbody
            |;

makedirop:MAKEDIROP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<MakeDirOp>(); }
          RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

renameop:RENAMEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<RenameOp>(); }
         renameopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

renameopbody:ALSOSELECTEXTENSION_WCP'='bool';' { std::dynamic_pointer_cast<RenameOp>(ps.lconfig_fp1)->setAlsoSelectFileExtension( ( ( $3 ) == 1 ) ? true : false ); } renameopbody
            |;

dirsizeop:DIRSIZEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<DirSizeOp>(); }
          RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

simddop:SIMDDOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "simulate_doubleclick" );
                                    ps.lconfig_fp1 = tfp;
                                  }
        RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

startprogop:STARTPROGOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<StartProgOp>(); }
            startprogopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

startprogopbody:START_WCP'='TERMINAL_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_IN_TERMINAL ); } startprogopbody
               |START_WCP'='TERMINALWAIT_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_IN_TERMINAL_AND_WAIT4KEY ); } startprogopbody
               |START_WCP'='SHOWOUTPUT_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT ); } startprogopbody
               |START_WCP'='SHOWOUTPUTINT_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_INT ); } startprogopbody
               |START_WCP'='SHOWOUTPUTOTHERSIDE_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_OTHER_SIDE ); } startprogopbody
               |START_WCP'='SHOWOUTPUTCUSTOMATTRIBUTE_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_CUSTOM_ATTRIBUTE ); } startprogopbody
               |START_WCP'='NORMAL_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_NORMAL ); } startprogopbody
               |VIEWSTR_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setViewStr( $3 ); } startprogopbody
               |GLOBAL_WCP'='bool';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setGlobal( ( ( $3 ) == 1 ) ? true : false ); } startprogopbody
               |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } startprogopbody
               |INBACKGROUND_WCP'='bool';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setInBackground( ( ( $3 ) == 1 ) ? true : false ); } startprogopbody
               |DONTCD_WCP'='bool';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setDontCD( ( ( $3 ) == 1 ) ? true : false ); } startprogopbody
               |FOLLOWACTIVE_WCP'='bool';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setFollowActive( ( ( $3 ) == 1 ) ? true : false ); } startprogopbody
               |WATCHFILE_WCP'='bool';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setWatchFile( ( ( $3 ) == 1 ) ? true : false ); } startprogopbody
               |SEPARATEEACHENTRY_WCP'='bool';' { std::dynamic_pointer_cast<StartProgOp>(ps.lconfig_fp1)->setSeparateEachEntry( ( ( $3 ) == 1 ) ? true : false ); } startprogopbody
               |;

searchentryop:SEARCHENTRYOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<SearchEntryOp>(); }
              searchentryopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

searchentryopbody:IGNORECASE_WCP'='bool';' { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setIgnoreCase( ( ( $3 ) == 1 ) ? true : false ); } searchentryopbody
                 |REVERSESEARCH_WCP'='bool';' { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setReverseSearch( ( ( $3 ) == 1 ) ? true : false ); } searchentryopbody
                 |INFIXSEARCH_WCP'='bool';' { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setInfixSearch( ( ( $3 ) == 1 ) ? true : false ); } searchentryopbody
                 |FLEXIBLEMATCH_WCP'='bool';' { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setFlexibleMatch( ( ( $3 ) == 1 ) ? true : false ); } searchentryopbody
                 |STARTSEARCHSTRING_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setStartSearchString( $3 ); } searchentryopbody
                 |APPLYONLY_WCP'='bool';' { std::dynamic_pointer_cast<SearchEntryOp>(ps.lconfig_fp1)->setApplyOnly( ( ( $3 ) == 1 ) ? true : false ); } searchentryopbody
                 |;

enterpathop:ENTERPATHOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<EnterPathOp>(); }
            enterpathopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

enterpathopbody:MODE_WCP'='LEFT_WCP';' { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_LEFTSIDE ); } enterpathopbody
               |MODE_WCP'='RIGHT_WCP';' { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_RIGHTSIDE ); } enterpathopbody
               |MODE_WCP'='CURRENT_WCP';' { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_THISSIDE ); } enterpathopbody
               |MODE_WCP'='OTHER_WCP';' { std::dynamic_pointer_cast<EnterPathOp>(ps.lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_OTHERSIDE ); } enterpathopbody
               |;

scrolllisterop:SCROLLLISTEROP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ScrollListerOp>(); }
               scrolllisteropbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

scrolllisteropbody:MODE_WCP'='LEFT_WCP';' { std::dynamic_pointer_cast<ScrollListerOp>(ps.lconfig_fp1)->setDir( ScrollListerOp::SCROLLLISTEROP_LEFT ); } scrolllisteropbody
                  |MODE_WCP'='RIGHT_WCP';' { std::dynamic_pointer_cast<ScrollListerOp>(ps.lconfig_fp1)->setDir( ScrollListerOp::SCROLLLISTEROP_RIGHT ); } scrolllisteropbody
                  |;

createsymlinkop:CREATESYMLINKOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<CreateSymlinkOp>(); }
                createsymlinkopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

createsymlinkopbody:SAMEDIR_WCP'='bool';' { std::dynamic_pointer_cast<CreateSymlinkOp>(ps.lconfig_fp1)->setSameDir( ( ( $3 ) == 1 ) ? true : false ); } createsymlinkopbody
                   |RELATIVE_WCP'='bool';' { std::dynamic_pointer_cast<CreateSymlinkOp>(ps.lconfig_fp1)->setLocal( ( ( $3 ) == 1 ) ? true : false ); } createsymlinkopbody
                   |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<CreateSymlinkOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } createsymlinkopbody
                   |;

changesymlinkop:CHANGESYMLINKOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ChangeSymlinkOp>(); }
                changesymlinkopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

changesymlinkopbody:MODE_WCP'='EDIT_WCP';' { std::dynamic_pointer_cast<ChangeSymlinkOp>(ps.lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_EDIT ); } changesymlinkopbody
                   |MODE_WCP'='MAKEABSOLUTE_WCP';' { std::dynamic_pointer_cast<ChangeSymlinkOp>(ps.lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_MAKE_ABSOLUTE ); } changesymlinkopbody
                   |MODE_WCP'='MAKERELATIVE_WCP';' { std::dynamic_pointer_cast<ChangeSymlinkOp>(ps.lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_MAKE_RELATIVE ); } changesymlinkopbody
                   |;

chmodop:CHMODOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ChModOp>(); }
        chmodopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

chmodopbody:ONFILES_WCP'='bool';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setOnFiles( ( ( $3 ) == 1 ) ? true : false ); } chmodopbody
           |ONDIRS_WCP'='bool';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setOnDirs( ( ( $3 ) == 1 ) ? true : false ); } chmodopbody
           |RECURSIVE_WCP'='bool';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setRecursive( ( ( $3 ) == 1 ) ? true : false ); } chmodopbody
           |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } chmodopbody
           |APPLYMODE_WCP'='ASK_WCP';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_ASK_PERMISSIONS ); } chmodopbody
           |APPLYMODE_WCP'='SET_WCP';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_SET_PERMISSIONS ); } chmodopbody
           |APPLYMODE_WCP'='ADD_WCP';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_ADD_PERMISSIONS ); } chmodopbody
           |APPLYMODE_WCP'='REMOVE_WCP';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setApplyMode( CHMOD_REMOVE_PERMISSIONS ); } chmodopbody
           |PERMISSION_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ChModOp>(ps.lconfig_fp1)->setPermissions( $3 ); } chmodopbody
           |;

chtimeop:CHTIMEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ChTimeOp>(); }
         chtimeopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

chtimeopbody:ONFILES_WCP'='bool';' { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setOnFiles( ( ( $3 ) == 1 ) ? true : false ); } chtimeopbody
            |ONDIRS_WCP'='bool';' { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setOnDirs( ( ( $3 ) == 1 ) ? true : false ); } chtimeopbody
            |RECURSIVE_WCP'='bool';' { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setRecursive( ( ( $3 ) == 1 ) ? true : false ); } chtimeopbody
            |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<ChTimeOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } chtimeopbody
            |;

togglelistermodeop:TOGGLELISTERMODEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ToggleListermodeOp>(); }
                   togglelistermodeopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

togglelistermodeopbody:MODE_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ToggleListermodeOp>(ps.lconfig_fp1)->setMode( Worker::getID4Name( $3 ) ); } togglelistermodeopbody
                      |;

setsortmodeop:SETSORTMODEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<SetSortmodeOp>();
                                        std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setMode( 0 );
                                      }
              setsortmodeopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

setsortmodeopbody:SORTBY_WCP'='NAME_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_NAME ); } setsortmodeopbody
                 |SORTBY_WCP'='SIZE_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_SIZE ); } setsortmodeopbody
                 |SORTBY_WCP'='ACCTIME_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_ACCTIME ); } setsortmodeopbody
                 |SORTBY_WCP'='MODTIME_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_MODTIME ); } setsortmodeopbody
                 |SORTBY_WCP'='CHGTIME_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_CHGTIME ); } setsortmodeopbody
                 |SORTBY_WCP'='TYPE_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_TYPE ); } setsortmodeopbody
                 |SORTBY_WCP'='OWNER_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_OWNER ); } setsortmodeopbody
                 |SORTBY_WCP'='INODE_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_INODE ); } setsortmodeopbody
                 |SORTBY_WCP'='NLINK_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_NLINK ); } setsortmodeopbody
                 |SORTBY_WCP'='PERMISSION_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_PERMISSION ); } setsortmodeopbody
                 |SORTFLAG_WCP'='REVERSE_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortFlag( SORT_REVERSE ); } setsortmodeopbody
                 |SORTFLAG_WCP'='DIRLAST_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortFlag( SORT_DIRLAST ); } setsortmodeopbody
                 |SORTFLAG_WCP'='DIRMIXED_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortFlag( SORT_DIRMIXED ); } setsortmodeopbody
                 |SORTBY_WCP'='EXTENSION_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_EXTENSION ); } setsortmodeopbody
                 |SORTBY_WCP'='CUSTOMATTRIBUTE_WCP';' { std::dynamic_pointer_cast<SetSortmodeOp>(ps.lconfig_fp1)->setSortby( SORT_CUSTOM_ATTRIBUTE ); } setsortmodeopbody
                 |;

setfilterop:SETFILTEROP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<SetFilterOp>(); }
            setfilteropbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

setfilteropbody:REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } setfilteropbody
               |MODE_WCP'='EXCLUDE_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::EXCLUDE_FILTER ); } setfilteropbody
               |MODE_WCP'='INCLUDE_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::INCLUDE_FILTER ); } setfilteropbody
               |MODE_WCP'='UNSET_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::UNSET_FILTER ); } setfilteropbody
               |MODE_WCP'='UNSETALL_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFiltermode( SetFilterOp::UNSET_ALL ); } setfilteropbody
               |FILTER_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setFilter( $3 ); } setfilteropbody
               |BOOKMARKLABEL_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkLabel( $3 ); } setfilteropbody
               |BOOKMARKFILTER_WCP'='SHOWONLYBOOKMARKS_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ONLY_BOOKMARKS ); } setfilteropbody
               |BOOKMARKFILTER_WCP'='SHOWONLYLABEL_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ONLY_LABEL ); } setfilteropbody
               |BOOKMARKFILTER_WCP'='SHOWALL_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ALL ); } setfilteropbody
               |OPTIONMODE_WCP'='SET_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setOptionMode( SetFilterOp::SET_OPTION ); } setfilteropbody
               |OPTIONMODE_WCP'='INVERT_WCP';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setOptionMode( SetFilterOp::INVERT_OPTION ); } setfilteropbody
               |CHANGEFILTERS_WCP'='bool';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setChangeFilters( ( ( $3 ) == 1 ) ? true : false ); } setfilteropbody
               |CHANGEBOOKMARKS_WCP'='bool';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setChangeBookmarks( ( ( $3 ) == 1 ) ? true : false ); } setfilteropbody
               |QUERYLABEL_WCP'='bool';' { std::dynamic_pointer_cast<SetFilterOp>(ps.lconfig_fp1)->setQueryLabel( ( ( $3 ) == 1 ) ? true : false ); } setfilteropbody
               |;

shortkeyfromlistop:SHORTKEYFROMLISTOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ShortkeyFromListOp>(); }
                   RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

chownop:CHOWNOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ChOwnOp>(); }
        chownopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

chownopbody:ONFILES_WCP'='bool';' { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setOnFiles( ( ( $3 ) == 1 ) ? true : false ); } chownopbody
           |ONDIRS_WCP'='bool';' { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setOnDirs( ( ( $3 ) == 1 ) ? true : false ); } chownopbody
           |RECURSIVE_WCP'='bool';' { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setRecursive( ( ( $3 ) == 1 ) ? true : false ); } chownopbody
           |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<ChOwnOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } chownopbody
           |;

scriptop:SCRIPTOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ScriptOp>(); }
         scriptopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

scriptopbody:TYPE_WCP'='NOP_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_NOP ); } scriptopbody
            |TYPE_WCP'='PUSH_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_PUSH ); } scriptopbody
            |TYPE_WCP'='LABEL_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_LABEL ); } scriptopbody
            |TYPE_WCP'='IF_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_IF ); } scriptopbody
            |TYPE_WCP'='END_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_END ); } scriptopbody
            |TYPE_WCP'='POP_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_POP ); } scriptopbody
            |TYPE_WCP'='SETTINGS_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_SETTINGS ); } scriptopbody
            |TYPE_WCP'='WINDOW_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_WINDOW ); } scriptopbody
            |TYPE_WCP'='GOTO_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_GOTO ); } scriptopbody
            |TYPE_WCP'='EVALCOMMAND_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setType( ScriptOp::SCRIPT_EVALCOMMAND ); } scriptopbody
            |PUSHUSEOUTPUT_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setPushUseOutput( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |PUSHOUTPUTRETURNCODE_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setPushOutputReturnCode( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |DODEBUG_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setDoDebug( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |WPURECURSIVE_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWPURecursive( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |WPUTAKEDIRS_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWPUTakeDirs( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |STACKNR_WCP'='NUM_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setStackNr( $3 ); } scriptopbody
            |LABEL_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setLabel( $3 ); } scriptopbody
            |PUSHSTRING_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setPushString( $3 ); } scriptopbody
            |IFTEST_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setIfTest( $3 ); } scriptopbody
            |IFLABEL_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setIfLabel( $3 ); } scriptopbody
            |WINTYPE_WCP'='OPEN_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_OPEN ); } scriptopbody
            |WINTYPE_WCP'='CLOSE_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_CLOSE ); } scriptopbody
            |WINTYPE_WCP'='LEAVE_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_LEAVE ); } scriptopbody
            |CHANGEPROGRESS_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setChangeProgress( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |CHANGETEXT_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setChangeText( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |PROGRESSUSEOUTPUT_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setProgressUseOutput( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |WINTEXTUSEOUTPUT_WCP'='bool';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinTextUseOutput( ( ( $3 ) == 1 ) ? true : false ); } scriptopbody
            |PROGRESS_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setProgress( $3 ); } scriptopbody
            |WINTEXT_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setWinText( $3 ); } scriptopbody
            |COMMANDSTRING_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ScriptOp>(ps.lconfig_fp1)->setCommandStr( $3 ); } scriptopbody
            |;

showdircacheop:SHOWDIRCACHEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ShowDirCacheOp>(); }
               RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

parentactionop:PARENTACTIONOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ParentActionOp>(); }
               RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

nooperationop:NOOPERATIONOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<NoOperationOp>(); }
              RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

goftpop:GOFTPOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<GoFTPOp>(); }
        goftpopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

goftpopbody:REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } goftpopbody
           |HOSTNAME_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setHost( $3 ); } goftpopbody
           |USERNAME_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setUser( $3 ); } goftpopbody
           |PASSWORD_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setPass( $3 ); } goftpopbody
	   |DONTENTERFTP_WCP'='bool';' { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setDontEnterFTP( ( ( $3 ) == 1 ) ? true : false ); } goftpopbody
	   |ALWAYSSTOREPW_WCP'='bool';' { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setAlwaysStorePW( ( ( $3 ) == 1 ) ? true : false ); } goftpopbody
           |AVFSMODULE_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<GoFTPOp>(ps.lconfig_fp1)->setAVFSModule( $3 ); } goftpopbody
           |;

internalviewop:INTERNALVIEWOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<InternalViewOp>(); }
               internalviewopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

internalviewopbody:REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } internalviewopbody
           |CUSTOMFILES_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setCustomFiles( $3 ); } internalviewopbody
           |SHOWMODE_WCP'='ACTIVE_WCP';' { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setShowFileMode( InternalViewOp::SHOW_ACTIVE_FILE ); } internalviewopbody
           |SHOWMODE_WCP'='CUSTOM_WCP';' { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setShowFileMode( InternalViewOp::SHOW_CUSTOM_FILES ); } internalviewopbody
	   /*           |SHOWMODE_WCP'='SELECTED_WCP';' { std::dynamic_pointer_cast<InternalViewOp>(ps.lconfig_fp1)->setShowFileMode( InternalViewOp::SHOW_SELECTED_FILES ); } internalviewopbody*/
           |;

clipboardop:CLIPBOARDOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ClipboardOp>(); }
            clipboardopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

clipboardopbody:CLIPBOARDSTRING_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ClipboardOp>(ps.lconfig_fp1)->setClipboardString( $3 ); } clipboardopbody
                |;

searchop:SEARCHOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<SearchOp>(); }
         searchopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

searchopbody:EDITCOMMAND_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<SearchOp>(ps.lconfig_fp1)->setEditCommand( $3 ); } searchopbody
            |SHOWPREVRESULTS_WCP'='bool';' { std::dynamic_pointer_cast<SearchOp>(ps.lconfig_fp1)->setShowPrevResults( ( ( $3 ) == 1 ) ? true : false ); } searchopbody
            |USESELECTEDENTRIES_WCP'='bool';' { std::dynamic_pointer_cast<SearchOp>(ps.lconfig_fp1)->setUseSelectedEntries( ( ( $3 ) == 1 ) ? true : false ); } searchopbody
            |;

dirbookmarkop:DIRBOOKMARKOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<DirBookmarkOp>(); }
              RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

opencontextmenuop:OPENCONTEXTMENUOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<OpenContextMenuOp>(); }
                  opencontextmenuopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

opencontextmenuopbody:HIGHLIGHT_USER_ACTION_WCP'='bool';' { std::dynamic_pointer_cast<OpenContextMenuOp>(ps.lconfig_fp1)->setHighlightUserAction( ( ( $3 ) == 1 ) ? true : false ); } opencontextmenuopbody
                     |;

runcustomaction:RUNCUSTOMACTION_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<RunCustomAction>(); }
                runcustomactionbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

runcustomactionbody:CUSTOMNAME_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<RunCustomAction>(ps.lconfig_fp1)->setCustomName( $3 ); } runcustomactionbody
              |;

openworkermenuop:OPENWORKERMENUOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<OpenWorkerMenuOp>(); }
                 RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

changelabelop:CHANGELABELOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ChangeLabelOp>(); }
              changelabelopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

changelabelopbody:ASKFORLABEL_WCP'='bool';' { std::dynamic_pointer_cast<ChangeLabelOp>(ps.lconfig_fp1)->setAskForLabel( ( ( $3 ) == 1 ) ? true : false ); } changelabelopbody
                 |LABEL_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ChangeLabelOp>(ps.lconfig_fp1)->setLabel( $3 ); } changelabelopbody
                 |;

modifytabsop:MODIFYTABSOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ModifyTabsOp>(); }
             modifytabsopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

modifytabsopbody:TABACTION_WCP'='NEWTAB_WCP';' { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::NEW_TAB ); } modifytabsopbody
                |TABACTION_WCP'='CLOSECURRENTTAB_WCP';' { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::CLOSE_CURRENT_TAB ); } modifytabsopbody
                |TABACTION_WCP'='NEXTTAB_WCP';' { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::NEXT_TAB ); } modifytabsopbody
                |TABACTION_WCP'='PREVTAB_WCP';' { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::PREV_TAB ); } modifytabsopbody
                |TABACTION_WCP'='TOGGLELOCKSTATE_WCP';' { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::TOGGLE_LOCK_STATE ); } modifytabsopbody
                |TABACTION_WCP'='MOVETABLEFT_WCP';' { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::MOVE_TAB_LEFT ); } modifytabsopbody
                |TABACTION_WCP'='MOVETABRIGHT_WCP';' { std::dynamic_pointer_cast<ModifyTabsOp>(ps.lconfig_fp1)->setTabAction( ModifyTabsOp::MOVE_TAB_RIGHT ); } modifytabsopbody
                |;

changelayoutop:CHANGELAYOUTOP_WCP LEFTBRACE_WCP {
                 ps.lconfig_fp1 = std::make_shared<ChangeLayoutOp>();
                 layout_sets = LayoutSettings();
             } changelayoutopbody RIGHTBRACE_WCP {
                 std::dynamic_pointer_cast<ChangeLayoutOp>(ps.lconfig_fp1)->setLayout( layout_sets );
                 lconfig_listcom.push_back( ps.lconfig_fp1 );
             };

changelayoutopbody:STATEBAR_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_STATEBAR ); } changelayoutopbody
                  |CLOCKBAR_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_CLOCKBAR ); } changelayoutopbody
                  |BUTTONS_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_BUTTONS ); } changelayoutopbody
                  |LISTVIEWS_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_LISTVIEWS ); } changelayoutopbody
                  |BLL_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_BLL ); } changelayoutopbody
                  |LBL_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_LBL ); } changelayoutopbody
                  |LLB_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_LLB ); } changelayoutopbody
                  |BL_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_BL ); } changelayoutopbody
                  |LB_WCP';' { layout_sets.pushBackOrder( LayoutSettings::LO_LB ); } changelayoutopbody
                  |BUTTONSVERT_WCP'='bool';'{ layout_sets.setButtonVert( ( ( $3 ) == 1 ) ? true : false ); } changelayoutopbody
                  |LISTVIEWSVERT_WCP'='bool';'{ layout_sets.setListViewVert( ( ( $3 ) == 1 ) ? true : false ); } changelayoutopbody
                  |LISTVIEWWEIGHT_WCP'='NUM_WCP';'{ layout_sets.setListViewWeight( $3 ); } changelayoutopbody
                  |WEIGHTTOACTIVE_WCP'='bool';'{ layout_sets.setWeightRelToActive( ( ( $3 ) == 1 ) ? true : false ); } changelayoutopbody
                  |;

changecolumnsop:CHANGECOLUMNSOP_WCP LEFTBRACE_WCP {
                  ps.lconfig_fp1 = std::make_shared<ChangeColumnsOp>();
                  columns.clear();
                } changecolumnsopbody RIGHTBRACE_WCP {
                  std::dynamic_pointer_cast<ChangeColumnsOp>(ps.lconfig_fp1)->setColumns( columns );
                  lconfig_listcom.push_back( ps.lconfig_fp1 );
                };

changecolumnsopbody:NAME_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_NAME ); } changecolumnsopbody
                   |SIZE_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_SIZE ); } changecolumnsopbody
                   |TYPE_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_TYPE ); } changecolumnsopbody
                   |PERMISSION_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_PERM ); } changecolumnsopbody
                   |OWNER_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_OWNER ); } changecolumnsopbody
                   |DESTINATION_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_DEST ); } changecolumnsopbody
                   |MODTIME_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_MOD ); } changecolumnsopbody
                   |ACCTIME_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_ACC ); } changecolumnsopbody
                   |CHGTIME_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_CHANGE ); } changecolumnsopbody
                   |INODE_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_INODE ); } changecolumnsopbody
                   |NLINK_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_NLINK ); } changecolumnsopbody
                   |BLOCKS_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_BLOCKS ); } changecolumnsopbody
                   |SIZEH_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_SIZEH ); } changecolumnsopbody
                   |EXTENSION_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_EXTENSION ); } changecolumnsopbody
                   |CUSTOMATTRIBUTE_WCP';'{ columns.push_back( WorkerTypes::LISTCOL_CUSTOM_ATTR ); } changecolumnsopbody
                   |;

volumemanagerop:VOLUMEMANAGEROP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<VolumeManagerOp>(); }
                RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

switchbuttonbankop:SWITCHBUTTONBANKOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<SwitchButtonBankOp>(); }
                   switchbuttonbankopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

switchbuttonbankopbody:MODE_WCP'='SWITCHTONEXTBANK_WCP';' { std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_NEXT_BANK ); } switchbuttonbankopbody
                      |MODE_WCP'='SWITCHTOPREVBANK_WCP';' { std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_PREV_BANK ); } switchbuttonbankopbody
                      |MODE_WCP'='SWITCHTOBANKNR_WCP';' { std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_BANK_NR ); } switchbuttonbankopbody
                      |BANKNR_WCP'='NUM_WCP';'{ std::dynamic_pointer_cast<SwitchButtonBankOp>(ps.lconfig_fp1)->setBankNr( $3 ); } switchbuttonbankopbody
                      |;

pathjumpop:PATHJUMPOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<PathJumpOp>(); }
           pathjumpopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

pathjumpopbody:INITIALTAB_WCP'='SHOWBYTIME_WCP';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_TIME ); } pathjumpopbody
              |INITIALTAB_WCP'='SHOWBYFILTER_WCP';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_FILTER ); } pathjumpopbody
              |INITIALTAB_WCP'='SHOWBYPROGRAM_WCP';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_PROGRAM ); } pathjumpopbody
              |DISPLAYMODE_WCP'='SHOWALL_WCP';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_ALL ); } pathjumpopbody
              |DISPLAYMODE_WCP'='SHOWSUBDIRS_WCP';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_SUBDIRS ); } pathjumpopbody
              |DISPLAYMODE_WCP'='SHOWDIRECTSUBDIRS_WCP';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_DIRECT_SUBDIRS ); } pathjumpopbody
              |INCLUDEALL_WCP'='bool';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setIncludeAllData( ( ( $3 ) == 1 ) ? true : false ); } pathjumpopbody
              |HIDENONEXISTING_WCP'='bool';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setHideNonExisting( ( ( $3 ) == 1 ) ? true : false ); } pathjumpopbody
              |LOCKONCURRENTDIR_WCP'='bool';' { std::dynamic_pointer_cast<PathJumpOp>(ps.lconfig_fp1)->setLockOnCurrentDir( ( ( $3 ) == 1 ) ? true : false ); } pathjumpopbody
              |;

commandmenuop:COMMANDMENUOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<CommandMenuOp>(); }
              commandmenuopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

commandmenuopbody:SHOWRECENT_WCP'='bool';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setShowRecentlyUsed( ( ( $3 ) == 1 ) ? true : false ); } commandmenuopbody
                 |STARTNODE_WCP'='MENUS_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::MENUS ); } commandmenuopbody
                 |STARTNODE_WCP'='BUTTONS_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::BUTTONS ); } commandmenuopbody
                 |STARTNODE_WCP'='HOTKEYS_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::HOTKEYS ); } commandmenuopbody
                 |STARTNODE_WCP'='PATHS_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::PATHS ); } commandmenuopbody
                 |STARTNODE_WCP'='LVMODES_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::LV_MODES ); } commandmenuopbody
                 |STARTNODE_WCP'='LVCOMMANDS_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::LV_COMMANDS ); } commandmenuopbody
                 |STARTNODE_WCP'='COMMANDS_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::COMMANDS ); } commandmenuopbody
                 |STARTNODE_WCP'='TOPLEVEL_WCP';' { std::dynamic_pointer_cast<CommandMenuOp>(ps.lconfig_fp1)->setStartNode( WorkerTypes::TOP_LEVEL ); } commandmenuopbody
                 |;

viewnewestfilesop:VIEWNEWESTFILESOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ViewNewestFilesOp>(); }
                  RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

dircompareop:DIRCOMPAREOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<DirCompareOp>(); }
             RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

tabprofilesop:TABPROFILESOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<TabProfilesOp>(); }
              RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

externalvdirop:EXTERNALVDIROP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ExternalVDirOp>(); }
               externalvdiropbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

externalvdiropbody:COMMANDSTRING_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ExternalVDirOp>(ps.lconfig_fp1)->setCommandString( $3 ); } externalvdiropbody
                  |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<ExternalVDirOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } externalvdiropbody
                  |;

opentabmenuop:OPENTABMENUOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ScriptOp >();
                                                tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                                tfp->setCommandStr( "open_current_tab_menu" );
                                                ps.lconfig_fp1 = tfp;
                                              }
              RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

directorypresets: DIRECTORYPRESET_WCP LEFTBRACE_WCP { dir_preset = directory_presets(); }
                  directorypreset RIGHTBRACE_WCP { dir_presets[path] = dir_preset;
                                                 } directorypresets
                |;

directorypreset: PATH_WCP'='STRING_WCP';' { path  = $3; } directorypreset
                |SORTBY_WCP'='NAME_WCP';' { dir_preset.sortmode = SORT_NAME; } directorypreset
                |SORTBY_WCP'='SIZE_WCP';' { dir_preset.sortmode = SORT_SIZE; } directorypreset
                |SORTBY_WCP'='ACCTIME_WCP';' { dir_preset.sortmode = SORT_ACCTIME; } directorypreset
                |SORTBY_WCP'='MODTIME_WCP';' { dir_preset.sortmode = SORT_MODTIME; } directorypreset
                |SORTBY_WCP'='CHGTIME_WCP';' { dir_preset.sortmode = SORT_CHGTIME; } directorypreset
                |SORTBY_WCP'='TYPE_WCP';' { dir_preset.sortmode = SORT_TYPE; } directorypreset
                |SORTBY_WCP'='OWNER_WCP';' { dir_preset.sortmode = SORT_OWNER; } directorypreset
                |SORTBY_WCP'='INODE_WCP';' { dir_preset.sortmode = SORT_INODE; } directorypreset
                |SORTBY_WCP'='NLINK_WCP';' { dir_preset.sortmode = SORT_NLINK; } directorypreset
                |SORTBY_WCP'='PERMISSION_WCP';' { dir_preset.sortmode = SORT_PERMISSION; } directorypreset
                |SORTFLAG_WCP'='REVERSE_WCP';' { dir_preset.sortmode |= SORT_REVERSE; } directorypreset
                |SORTFLAG_WCP'='DIRLAST_WCP';' { dir_preset.sortmode |= SORT_DIRLAST; } directorypreset
                |SORTFLAG_WCP'='DIRMIXED_WCP';' { dir_preset.sortmode |= SORT_DIRMIXED; } directorypreset
                |SORTBY_WCP'='EXTENSION_WCP';' { dir_preset.sortmode = SORT_EXTENSION; } directorypreset
                |SORTBY_WCP'='CUSTOMATTRIBUTE_WCP';' { dir_preset.sortmode = SORT_CUSTOM_ATTRIBUTE; } directorypreset
                |HIDDENFILES_WCP'='SHOW_WCP';' { dir_preset.show_hidden = true; } directorypreset
                |HIDDENFILES_WCP'='HIDE_WCP';' { dir_preset.show_hidden = false; } directorypreset
                |FILTER_WCP LEFTBRACE_WCP { dir_preset_filter = NM_Filter(); }
                 directorypreset_filter RIGHTBRACE_WCP { dir_preset.filters.push_back( dir_preset_filter ); } directorypreset
                |;

directorypreset_filter: PATTERN_WCP'='STRING_WCP';' { dir_preset_filter.setPattern( $3 ); } directorypreset_filter
                       |MODE_WCP'='EXCLUDE_WCP';' { dir_preset_filter.setCheck( NM_Filter::EXCLUDE ); } directorypreset_filter
                       |MODE_WCP'='INCLUDE_WCP';' { dir_preset_filter.setCheck( NM_Filter::INCLUDE ); } directorypreset_filter
                       |;

helpop:HELPOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< HelpOp >();
                                  ps.lconfig_fp1 = tfp;
                                }
       RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

viewcommandlogop:VIEWCOMMANDLOGOP_WCP LEFTBRACE_WCP { auto tfp = std::make_shared< ViewCommandLogOp >();
                                                      ps.lconfig_fp1 = tfp;
                                                    }
                  RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

activatetextviewmodeop:ACTIVATETEXTVIEWMODEOP_WCP LEFTBRACE_WCP { ps.lconfig_fp1 = std::make_shared<ActivateTextViewModeOp>(); }
                       activatetextviewmodeopbody RIGHTBRACE_WCP { lconfig_listcom.push_back( ps.lconfig_fp1 ); };

activatetextviewmodeopbody:ACTIVESIDE_WCP'='bool';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setActiveSide( ( ( $3 ) == 1 ) ? true : false ); } activatetextviewmodeopbody
                          |SOURCEMODE_WCP'='ACTIVEFILE_WCP';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setSourceMode( TextViewMode::ACTIVE_FILE_OTHER_SIDE ); } activatetextviewmodeopbody
                          |SOURCEMODE_WCP'='COMMANDSTRING_WCP';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setSourceMode( TextViewMode::COMMAND_STR ); } activatetextviewmodeopbody
                          |SOURCEMODE_WCP'='FILETYPEACTION_WCP';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setSourceMode( TextViewMode::FILE_TYPE_ACTION ); } activatetextviewmodeopbody
                          |COMMANDSTRING_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setCommandStr( $3 ); } activatetextviewmodeopbody
                          |FILETYPEACTION_WCP'='STRING_WCP';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setFileTypeAction( $3 ); } activatetextviewmodeopbody
                          |FOLLOWACTIVE_WCP'='bool';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->addOption( ( ( $3 ) == 1 ) ? TextViewMode::FOLLOW_ACTIVE : 0 ); } activatetextviewmodeopbody
                          |WATCHFILE_WCP'='bool';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->addOption( ( ( $3 ) == 1 ) ? TextViewMode::WATCH_FILE : 0 ); } activatetextviewmodeopbody
                          |REQUESTFLAGS_WCP'='bool';' { std::dynamic_pointer_cast<ActivateTextViewModeOp>(ps.lconfig_fp1)->setRequestParameters( ( ( $3 ) == 1 ) ? true : false ); } activatetextviewmodeopbody
                          |;

%%
/**/
int yymyparse(void)
{
  int erg;

  lconfig_error[0] = '\0';
  lconfig_linenr = 0;
  erg = yyparse();
  /* free all strings found by the lexer
     we don't need them anymore */
  lexer_cleanup();
  lconfig_cleanup();
  return erg;
}

int yyerror( const char *s )
{
  lconfig_cleanup();
  /* the following isn't really needed because yyerror is called inside the parser
     which itselve was started with yymyparse which will call it again
     but it doesn't hurt...*/
  lexer_cleanup();
  snprintf( lconfig_error, sizeof( lconfig_error ), "%s", s );
  return 0;
}
