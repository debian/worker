/* exprfilter.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2025 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EXPRFILTER_HH
#define EXPRFILTER_HH

#include "genericdirectoryfilter.hh"
#include "exprfilter_eval.hh"
#include <string>
#include <list>
#include "exprfilter_parser.hh"
#include "exprfilter_options.hh"

class NWCEntrySelectionState;

class ExprFilter : public GenericDirectoryFilter
{
public:
    ExprFilter( const std::string &filter,
                const ExprFilterOptions &options );
    ~ExprFilter();

    bool check( NWCEntrySelectionState &e );
    const ExprFilterEval::result_item &get_results() const;

    std::string getValidReconstructedFilter() const;
    std::list< std::string > getListOfExpectedStrings() const;
    std::list< ExprFilterParser::token_help > getListOfExpectedTokens() const;
    bool containsTypeExpression() const;
    bool valid() const;

    void setCheckInterrupt( std::function< bool () > interrupt_cb );
    void setFeedbackCallback( std::function< void ( const std::string & ) > feedback_cb );
private:
    std::string m_filter_string;
    std::string m_valid_reconstructed_filter;
    std::list< ExprFilterParser::token_help > m_expected_tokens;
    bool m_valid = false;

    ExprFilterEval m_efe;

    ExprFilterEval::result_item m_check_result;
};

#endif
