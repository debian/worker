/* activatetextviewmodeop.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef ACTIVATETEXTVIEWMODEOP_H
#define ACTIVATETEXTVIEWMODEOP_H

#include "wdefines.h"
#include "functionproto.h"
#include "textviewmode.hh"

class ActivateTextViewModeOp : public FunctionProto
{
public:
    ActivateTextViewModeOp();
    ~ActivateTextViewModeOp();
    ActivateTextViewModeOp( const ActivateTextViewModeOp &other );
    ActivateTextViewModeOp &operator=( const ActivateTextViewModeOp &other );

    ActivateTextViewModeOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;

    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;

    bool save(Datei*) override;
    const char *getDescription() override;
    int configure() override;
  
    void setActiveSide(bool);
    void setSourceMode( TextViewMode::source_mode source_mode );
    void setCommandStr( const std::string &command_str );
    void setFileTypeAction( const std::string &file_type_action );
    bool isInteractiveRun() const override;
    void setInteractiveRun() override;
    void setOptions( uint32_t options );
    void addOption( uint32_t option );

    static const char *name;
protected:
    // Infos to save

    bool m_active_side = false;
    TextViewMode::source_mode m_source_mode = TextViewMode::ACTIVE_FILE_OTHER_SIDE;
    std::string m_command_str;
    std::string m_file_type_action;
    uint32_t m_options = 0;
  
    // temp variables
    bool m_tactive_side = false;
    TextViewMode::source_mode m_tsource_mode = TextViewMode::ACTIVE_FILE_OTHER_SIDE;
    std::string m_tcommand_str;
    std::string m_tfile_type_action;
    uint32_t m_toptions = 0;
  
    int doconfigure( int mode );
};

#endif
