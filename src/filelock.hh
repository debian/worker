/* filelock.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILELOCK_HH
#define FILELOCK_HH

#include "wdefines.h"
#include <string>

class FileLock
{
public:
    FileLock( const std::string &filename );
    virtual ~FileLock();

    virtual bool lock();
    virtual bool trylock();
    virtual bool lock_retry( int count = 10, int wait_period = 10 );
    virtual void unlock();
private:
    std::string m_filename;
    int m_fd;
};

#endif /* FILELOCK_HH */
