/* shortkeyfromlist.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "shortkeyfromlistop.h"
#include "listermode.h"
#include "worker.h"
#include "worker_locale.h"

const char *ShortkeyFromListOp::name="ShortkeyFromListOp";

ShortkeyFromListOp::ShortkeyFromListOp() : FunctionProto()
{
}

ShortkeyFromListOp::~ShortkeyFromListOp()
{
}

ShortkeyFromListOp*
ShortkeyFromListOp::duplicate() const
{
  ShortkeyFromListOp *ta=new ShortkeyFromListOp();
  return ta;
}

bool
ShortkeyFromListOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
ShortkeyFromListOp::getName()
{
  return name;
}

int
ShortkeyFromListOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    msg->getWorker()->activateShortkeyFromList();
  }
  return 0;
}

const char *
ShortkeyFromListOp::getDescription()
{
  return catalog.getLocale(1283);
}

