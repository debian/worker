/* nwcentryselectionstate.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NWCENTRYSELECTIONSTATE_HH
#define NWCENTRYSELECTIONSTATE_HH

#include "wdefines.h"
#include "nwc_dir.hh"
#include "fileentry.hh"
#include "worker_types.h"
#include "aguix/refcount.hh"
#include "flagreplacer.hh"

class WCFiletype;

class NWCBookmarkInfo
{
public:
    NWCBookmarkInfo() : m_bookmark_match( FileEntry::NOT_IN_BOOKMARKS )
    {
    }

    FileEntry::bookmark_match_t m_bookmark_match;
    std::list< std::string > m_matching_labels;
};

class NWCEntrySelectionState
{
public:
    NWCEntrySelectionState( const NWCEntrySelectionState &other );
    NWCEntrySelectionState( const NWCEntrySelectionState &other, bool decoupleDir );
    NWCEntrySelectionState &operator=( const NWCEntrySelectionState &rhs );
    explicit NWCEntrySelectionState( const NWC::FSEntry &entry );

    void setUse( bool use );
    bool getUse() const;

    void setSelected( bool s );
    bool getSelected() const;

    const NWC::FSEntry *getNWCEntry() const;
    NWC::FSEntry *getNWCEntry();

    void setFiletype( WCFiletype *ft );
    WCFiletype *getFiletype() const;
    WCFiletype *getEffectiveFiletype() const;

    void setMatchingLabels( const std::list<std::string> &labels );
    void clearMatchingLabels();
    const std::list<std::string> &getMatchingLabels() const;
    bool hasMatchingLabels() const;

    FileEntry::bookmark_match_t getBookmarkMatch() const;
    void setBookmarkMatch( FileEntry::bookmark_match_t m );

    void setColor( const FileEntryColor &c );
    const FileEntryColor &getColor() const;

    void setCustomColor( const FileEntryCustomColor &c );
    void setCustomColors( const int fg[4], const int bg[4] );
    const FileEntryCustomColor &getCustomColor() const;
    int getCustomColorSet() const;
    void clearCustomColor();

    void setOverrideColor( int type, int fg, int bg );
    void clearOverrideColor();

    static int sortfunction( const NWCEntrySelectionState &e1,
                             const NWCEntrySelectionState &e2,
                             int mode );

    std::string getStringRepresentation( WorkerTypes::listcol_t type,
                                         int file_name_skip,
                                         FlagReplacer *rpl ) const;

    static std::string getStringRepresentation( WorkerTypes::listcol_t type,
                                                int file_name_skip,
                                                const NWC::FSEntry *fse,
                                                const WCFiletype *filetype,
                                                FlagReplacer *rpl,
                                                const std::string &custom_attribute );

    void setFiletypeFileOutput( const std::string &output );
    std::string getFiletypeFileOutput() const;

    void setMimeType( const std::string &mimetype );
    std::string getMimeType() const;

    void setCustomAttribute( const std::string &content );
    const std::string &getCustomAttribute() const;
protected:
    friend class DMCacheEntryNWC;
    NWCEntrySelectionState( int entry_id = -1 );
    void setDir( RefCount< NWC::Dir > dir );
    int getEntryID() const;
    void setEntryID( int entry_id );

    RefCount< NWCBookmarkInfo > getBookmarkInfo();
    void setBookmarkInfo( RefCount< NWCBookmarkInfo >  bookmark_info );
private:
    RefCount< NWC::Dir > m_dir;
    RefCount< NWC::FSEntry > m_cloned_entry;
    int m_entry_id;
    bool m_use;
    WCFiletype *m_filetype;
    RefCount< NWCBookmarkInfo > m_bookmark_info;
    FileEntryColor m_colors;
    bool m_selected;
    std::string m_filetype_file_output;
    std::string m_mime_type;
    std::string m_custom_attribute;
};

class NWCFlagProducer : public FlagReplacer::FlagProducer
{
public:
    NWCFlagProducer( const NWCEntrySelectionState *es,
                     const FileEntry *fe ) : m_es( es ),
                                             m_fe( fe )
    {}

    std::string getFlagReplacement( const std::string &flag );
    bool hasFlag( const std::string &str );
private:
    const NWCEntrySelectionState *m_es;
    const FileEntry *m_fe;
};

#endif
