/* worker_commands.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "worker.h"
#include "worker_commands.h"
#include "worker_locale.h"

int Worker::getNrOfCommands()
{
  return 60;
}

std::shared_ptr< FunctionProto > Worker::getCommand4ID(int id)
{
  FunctionProto *fpr=NULL;
  switch(id) {
    case 1:fpr=new DoubleClickAction();
      break;
    case 2:fpr=new ShowAction();
      break;
    case 3:fpr=new RawShowAction();
      break;
    case 4:fpr=new UserAction();
      break;
    case 5:
      fpr = new ChangeColumnsOp();
      break;
    case 6:
      fpr = new ViewNewestFilesOp();
      break;
    case 7:
      fpr=new ChangeHiddenFlag();
      break;
    case 8:
      fpr=new CopyOp();
      break;
    case 9:
      fpr = new DirCompareOp();
      break;
    case 10:
      fpr = new TabProfilesOp();
      break;
    case 11:
      fpr = new ExternalVDirOp();
      break;
    case 12:
      fpr = new ChangeLayoutOp();
      break;
    case 13:
      fpr = new ChTimeOp();
      break;
    case 14:
      fpr = new PathJumpOp();
      break;
    case 15:
      fpr = new ClipboardOp();
      break;
    case 16:
      fpr = new CommandMenuOp();
      break;
    case 17:
      fpr = new VolumeManagerOp();
      break;
    case 18:
      fpr=new EnterDirOp();
      break;
    case 19:
      fpr=new ChangeListerSetOp();
      break;
    case 20:
      fpr=new SwitchListerOp();
      break;
    case 21:
      fpr=new FilterSelectOp();
      break;
    case 22:
      fpr=new FilterUnSelectOp();
      break;
    case 23:
      fpr=new Path2OSideOp();
      break;
    case 24:
      fpr=new QuitOp();
      break;
    case 25:
      fpr=new DeleteOp();
      break;
    case 26:
      fpr=new ReloadOp();
      break;
    case 27:
      fpr=new MakeDirOp();
      break;
    case 28:
      fpr=new OwnOp();
      break;
    case 29:
      fpr=new RenameOp();
      break;
    case 30:
      fpr=new DirSizeOp();
      break;
    case 31:
      fpr = new SwitchButtonBankOp();
      break;
    case 32:
      fpr=new StartProgOp();
      break;
    case 33:
      fpr=new SearchEntryOp();
      break;
    case 34:
      fpr=new EnterPathOp();
      break;
    case 35:
      fpr=new ScrollListerOp();
      break;
    case 36:
      fpr=new CreateSymlinkOp();
      break;
    case 37:
      fpr=new ChangeSymlinkOp();
      break;
    case 38:
      fpr=new ChModOp();
      break;
    case 39:
      fpr=new ToggleListermodeOp();
      break;
    case 40:
      fpr=new SetSortmodeOp();
      break;
    case 41:
      fpr=new SetFilterOp();
      break;
    case 42:
      fpr=new ShortkeyFromListOp();
      break;
    case 43:
      fpr=new ChOwnOp();
      break;
    case 44:
      fpr=new ScriptOp();
      break;
    case 45:
      fpr = new ShowDirCacheOp();
      break;
    case 46:
      fpr = new ParentActionOp();
      break;
    case 47:
      fpr = new NoOperationOp();
      break;
    case 48:
      fpr = new GoFTPOp();
      break;
    case 49:
      fpr = new InternalViewOp();
      break;
    case 50:
      fpr = new SearchOp();
      break;
    case 51:
      fpr = new DirBookmarkOp();
      break;
    case 52:
      fpr = new OpenContextMenuOp();
      break;
    case 53:
      fpr = new RunCustomAction();
      break;
    case 54:
      fpr = new OpenWorkerMenuOp();
      break;
    case 55:
      fpr = new ChangeLabelOp();
      break;
    case 56:
      fpr = new ModifyTabsOp();
      break;
    case 57:
      fpr = new HelpOp();
      break;
    case 58:
      fpr = new ViewCommandLogOp();
      break;
    case 59:
      fpr = new ActivateTextViewModeOp();
      break;
    default:
      fpr=new DNDAction();
  }
  return std::shared_ptr< FunctionProto >( fpr );
}

int Worker::getID4Command(FunctionProto *fpr)
{
  int type=-1;
  if(fpr->isName("DNDAction")==true) type=0;
  else if(fpr->isName("DoubleClickAction")==true) type=1;
  else if(fpr->isName("ShowAction")==true) type=2;
  else if(fpr->isName("RawShowAction")==true) type=3;
  else if(fpr->isName("UserAction")==true) type=4;
  else if ( fpr->isName( ChangeColumnsOp::name ) == true ) type = 5;
  else if ( fpr->isName( ViewNewestFilesOp::name ) == true ) type = 6;
  else if(fpr->isName("ChangeHiddenFlag")==true) type=7;
  else if(fpr->isName("CopyOp")==true) type=8;
  else if ( fpr->isName( DirCompareOp::name ) == true ) type = 9;
  else if ( fpr->isName( TabProfilesOp::name ) == true ) type = 10;
  else if ( fpr->isName( ExternalVDirOp::name ) == true ) type = 11;
  else if ( fpr->isName( ChangeLayoutOp::name ) == true ) type = 12;
  else if ( fpr->isName( ChTimeOp::name ) == true ) type = 13;
  else if ( fpr->isName( PathJumpOp::name ) == true ) type = 14;
  else if ( fpr->isName( ClipboardOp::name ) == true ) type = 15;
  else if ( fpr->isName( CommandMenuOp::name ) == true ) type = 16;
  else if ( fpr->isName( VolumeManagerOp::name ) == true ) type = 17;
  else if(fpr->isName("EnterDirOp")==true) type=18;
  else if(fpr->isName("ChangeListerSetOp")==true) type=19;
  else if(fpr->isName("SwitchListerOp")==true) type=20;
  else if(fpr->isName("FilterSelectOp")==true) type=21;
  else if(fpr->isName("FilterUnSelectOp")==true) type=22;
  else if(fpr->isName("Path2OSideOp")==true) type=23;
  else if(fpr->isName("QuitOp")==true) type=24;
  else if(fpr->isName("DeleteOp")==true) type=25;
  else if(fpr->isName("ReloadOp")==true) type=26;
  else if(fpr->isName("MakeDirOp")==true) type=27;
  else if(fpr->isName("OwnOp")==true) type=28;
  else if(fpr->isName("RenameOp")==true) type=29;
  else if(fpr->isName("DirSizeOp")==true) type=30;
  else if ( fpr->isName( SwitchButtonBankOp::name ) == true ) type = 31;
  else if(fpr->isName("StartProgOp")==true) type=32;
  else if(fpr->isName("SearchEntryOp")==true) type=33;
  else if(fpr->isName("EnterPathOp")==true) type=34;
  else if(fpr->isName("ScrollListerOp")==true) type=35;
  else if(fpr->isName("CreateSymlinkOp")==true) type=36;
  else if(fpr->isName("ChangeSymlinkOp")==true) type=37;
  else if(fpr->isName("ChModOp")==true) type=38;
  else if(fpr->isName("ToggleListermodeOp")==true) type=39;
  else if(fpr->isName("SetSortmodeOp")==true) type=40;
  else if(fpr->isName("SetFilterOp")==true) type=41;
  else if(fpr->isName("ShortkeyFromListOp")==true) type=42;
  else if(fpr->isName("ChOwnOp")==true) type=43;
  else if(fpr->isName("ScriptOp")==true) type=44;
  else if ( fpr->isName( ShowDirCacheOp::name ) == true ) type = 45;
  else if ( fpr->isName( ParentActionOp::name ) == true ) type = 46;
  else if ( fpr->isName( NoOperationOp::name ) == true ) type = 47;
  else if ( fpr->isName( GoFTPOp::name ) == true ) type = 48;
  else if ( fpr->isName( InternalViewOp::name ) == true ) type = 49;
  else if ( fpr->isName( SearchOp::name ) == true ) type = 50;
  else if ( fpr->isName( DirBookmarkOp::name ) == true ) type = 51;
  else if ( fpr->isName( OpenContextMenuOp::name ) == true ) type = 52;
  else if ( fpr->isName( RunCustomAction::name ) == true ) type = 53;
  else if ( fpr->isName( OpenWorkerMenuOp::name ) == true ) type = 54;
  else if ( fpr->isName( ChangeLabelOp::name ) == true ) type = 55;
  else if ( fpr->isName( ModifyTabsOp::name ) == true ) type = 56;
  else if ( fpr->isName( HelpOp::name ) == true ) type = 57;
  else if ( fpr->isName( ViewCommandLogOp::name ) == true ) type = 58;
  else if ( fpr->isName( ActivateTextViewModeOp::name ) == true ) type = 59;
  return type;
}

int Worker::getMaxModeNr()
{
  // leider hardcoded, da aber die schlechte Methode in dieser Klasse bleibt, nicht so
  // schlimm
    return 4;
}

ListerMode *Worker::getMode4ID(int nr,Lister *parent)
{
  ListerMode *lm=NULL;
  switch(nr) {
    case 1:
      lm=new ShowImageMode(parent);
      break;
    case 2:
      lm=new InformationMode(parent);
      break;
    case 3:
      lm = new TextViewMode( parent );
      break;
    default:
      lm = new VirtualDirMode( parent );
      break;
  }
  return lm;
}

const char *Worker::getNameOfMode( int nr )
{
  switch ( nr ) {
    case 1:
      return ShowImageMode::getStaticType();
      break;
    case 2:
      return InformationMode::getStaticType();
      break;
    case 3:
      return TextViewMode::getStaticType();
      break;
  }
  return VirtualDirMode::getStaticType();
}

const char *Worker::getLocaleNameOfMode( int nr )
{
  switch ( nr ) {
    case 1:
      return ShowImageMode::getStaticLocaleName();
      break;
    case 2:
      return InformationMode::getStaticLocaleName();
      break;
    case 3:
      return TextViewMode::getStaticLocaleName();
      break;
  }
  return VirtualDirMode::getStaticLocaleName();
}

int Worker::getID4Name( const char *str )
{
  if ( str == NULL ) return -1;
  if ( strcasecmp( str, ShowImageMode::getStaticType() ) == 0 ) return 1;
  if ( strcasecmp( str, InformationMode::getStaticType() ) == 0 ) return 2;
  if ( strcasecmp( str, TextViewMode::getStaticType() ) == 0 ) return 3;
  if ( strcasecmp( str, VirtualDirMode::getStaticType() ) == 0 ) return 0;
  return -1;
}

int Worker::getID4Mode(ListerMode *lm)
{
  int type=-1;
  if(lm->isType("ShowImageMode")==true) type=1;
  else if(lm->isType("InformationMode")==true) type=2;
  else if ( lm->isType( "TextViewMode" ) == true ) type = 3;
  else if ( lm->isType( "VirtualDirMode" ) == true ) type = 0;
  return type;
}

