/* deeppathstore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2012-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DEEPPATHSTORE_HH
#define DEEPPATHSTORE_HH

#include "wdefines.h"
#include <list>
#include <string>
#include <functional>

class DeepPathNode
{
public:
    DeepPathNode( const std::string &name, time_t ts ) : m_name( name ),
                                                         m_ts( ts )
    {
    }

    DeepPathNode()
    {
    }

    const std::string &get_name() const
    {
        return m_name;
    }

    const time_t get_ts() const
    {
        return m_ts;
    }

    void set_ts( time_t ts )
    {
        m_ts = ts;
    }

    const std::list< DeepPathNode > &get_children() const
    {
        return m_children;
    }

    typedef enum {
        NODE_UNCHANGED,
        NODE_INSERTED,
        NODE_UPDATED,
        NODE_MODIFIED
    } node_change_t;

    DeepPathNode &lookup_or_insert( const std::string &name, time_t ts, node_change_t *changed )
    {
        for ( std::list< DeepPathNode >::iterator it1 = m_children.begin();
              it1 != m_children.end();
              it1++ ) {
            if ( it1->get_name() == name ) return *it1;
        }

        if ( changed != NULL ) {
            *changed = NODE_INSERTED;
        }

        return *m_children.insert( m_children.end(), DeepPathNode( name, ts ) );
    }

    DeepPathNode *lookup( const std::string &name )
    {
        for ( std::list< DeepPathNode >::iterator it1 = m_children.begin();
              it1 != m_children.end();
              it1++ ) {
            if ( it1->get_name() == name ) return &(*it1);
        }

        return NULL;
    }

    void insert( const DeepPathNode &node )
    {
        m_children.insert( m_children.end(), DeepPathNode( node ) );
    }

    void clear()
    {
        m_children.clear();
    }

    void remove( const std::string &name )
    {
        m_children.remove_if( [&name]( DeepPathNode &elem ) { return elem.get_name() == name; } );
    }
private:
    std::list< DeepPathNode > m_children;
    std::string m_name;
    time_t m_ts = 0;
};

class DeepPathStore
{
public:
    DeepPathStore();

    enum dps_ts_update {
                        DPS_TS_UPDATE_ALWAYS,
                        DPS_TS_UPDATE_NONZERO
    };
    
    void storePath( const std::string &path, time_t ts, DeepPathNode::node_change_t *changed, enum dps_ts_update ts_update  );
    std::list< std::pair< std::string, time_t > > getPaths() const;

    void walkPaths( std::function< void( const std::string &, size_t depth, time_t ) > cb ); 

    void relocateEntries( const std::string &dest,
                          const std::string &source,
                          time_t ts,
                          bool move,
                          DeepPathNode::node_change_t *changed );

    bool removeEntry( const std::string &path );

    void clear();

    bool empty() const;
private:
    void storePath( DeepPathNode &node,
                    const std::string &path,
                    time_t ts,
                    std::string::size_type start_pos,
                    DeepPathNode::node_change_t *changed,
                    enum dps_ts_update ts_update );
    int findNode( const std::string &path,
                  DeepPathNode **node_return,
                  DeepPathNode **parent_node_return );
    int insertNode( const std::string &path,
                    DeepPathNode **node_return,
                    time_t ts,
                    DeepPathNode::node_change_t *changed );
    int copyChildren( DeepPathNode *dest_node,
                      const DeepPathNode *source_node,
                      DeepPathNode::node_change_t *changed );
    void splitSegments( const std::string &path,
                        std::vector< std::string > &path_segments );

    DeepPathNode m_root;
};

#endif
