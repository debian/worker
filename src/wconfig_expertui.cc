/* wconfig_expertui.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_expertui.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"

ExpertUIPanel::ExpertUIPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

ExpertUIPanel::~ExpertUIPanel()
{
}

int ExpertUIPanel::create()
{
  Panel::create();

  AContainer *ac1 = setContainer( new AContainer( this, 1, 1 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 0 );
  ac1->setMaxSpace( 0 );

  this->addMultiLineText( catalog.getLocale( 686 ),
                          *ac1,
                          0, 0,
                          NULL, NULL );
  
  contMaximize( true );
  return 0;
}

int ExpertUIPanel::saveValues()
{
  return 0;
}
