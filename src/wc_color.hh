/* wc_color.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2006 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WC_COLOR_HH
#define WC_COLOR_HH

#include "wdefines.h"

class WC_Color
{
public:
  WC_Color();
  WC_Color( int r, int g, int b);
  WC_Color( const WC_Color &other );
  WC_Color &operator=( const WC_Color &other );

  WC_Color *duplicate() const;

  int getRed() const;
  int getGreen() const;
  int getBlue() const;
  void setRed( int );
  void setGreen( int );
  void setBlue( int );
private:
  int _red, _green, _blue;
};

#endif
