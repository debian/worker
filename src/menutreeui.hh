/* menutreeui.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013,2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MENUTREEUI_HH
#define MENUTREEUI_HH

#include <memory>
#include <list>
#include "pers_string_list.hh"

class MenuTree;
class PrefixDB;
class AGUIX;
class FieldListView;
class Text;
class MenuTreeNode;

class MenuTreeUI
{
public:
    MenuTreeUI( AGUIX *aguix );
    ~MenuTreeUI();

    void setMenuTree( MenuTree *menu );

    void show( bool show_recently_used,
               MenuTreeNode *initial_node );
private:
    void updateBreadcrumb();
    void showMenu();
    void highlightEntry( const MenuTreeNode *entry );
    void syncHelpLV( FieldListView *source_lv = NULL );
    void triggerEntryHighlightFunction( MenuTreeNode *entry );
    void triggerEntryHighlightFunction( int row );
    void callDeHighlightFunction( MenuTreeNode *new_entry, MenuTreeNode *old_entry );
    void callHighlightFunction( MenuTreeNode *new_entry, MenuTreeNode *old_entry );

    AGUIX *m_aguix;
    MenuTree *m_menu;    
    std::unique_ptr< PrefixDB > m_pdb;
    FieldListView *m_lv, *m_lv_help, *m_lv_key;
    Text *m_breadcrumb;

    std::list< std::pair< std::string, MenuTreeNode * > > m_current_entries;
    MenuTreeNode *m_current_dehighlight_entry;

    std::shared_ptr< PersistentStringList > m_recently_used;
    bool m_show_recently_used;
};

#endif /* MENUTREEUI_HH */
