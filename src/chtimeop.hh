/* chtimeop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2015-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHTIMEOP_H
#define CHTIMEOP_H

#include "wdefines.h"
#include "functionproto.h"

class ChTimeOp : public FunctionProto
{
public:
    ChTimeOp();
    ~ChTimeOp();
    ChTimeOp( const ChTimeOp &other );
    ChTimeOp &operator=( const ChTimeOp &other );

    ChTimeOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;

    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;

    bool save(Datei*) override;
    const char *getDescription() override;
    int configure() override;
  
    void setOnFiles(bool);
    void setOnDirs(bool);
    void setRecursive(bool);
    void setRequestFlags(bool);
    bool isInteractiveRun() const;
    void setInteractiveRun();

    static const char *name;
protected:
    // Infos to save

    bool m_onfiles;
    bool m_ondirs;
    bool m_recursive;
    bool m_requestflags;
  
    // temp variables
    Lister *startlister;

    bool m_tonfiles;
    bool m_tondirs;
    bool m_trecursive;
  
    int vdm_chtime( ActionMessage* );

    int doconfigure( int mode );
};

#endif
