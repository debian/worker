/* bookmarkdbproxy.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008-2009,2012-2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BOOKMARKDBPROXY_HH
#define BOOKMARKDBPROXY_HH

#include "wdefines.h"
#include <string>
#include <list>
#include <memory>
#include "generic_callback.hh"
#include "pathtree.hh"
#include "aguix/refcount.hh"

class BookmarkDBEntry;
class BookmarkDB;

class BookmarkDBProxy
{
public:
    BookmarkDBProxy( std::unique_ptr<BookmarkDB> bookmarks );
    ~BookmarkDBProxy();
    BookmarkDBProxy( const BookmarkDBProxy &other );
    BookmarkDBProxy &operator=( const BookmarkDBProxy &rhs );

    void addEntry( const BookmarkDBEntry &entry );
    void delEntry( const BookmarkDBEntry &entry );
    void updateEntry( const BookmarkDBEntry &entry,
                      const BookmarkDBEntry &newentry );

    std::unique_ptr<BookmarkDBEntry> getEntry( const std::string &name );
    std::list<std::string> getCats();
    std::list<BookmarkDBEntry> getEntries( const std::string &cat );
    std::list< std::string > getNamesOfEntries( const std::string &cat );

    int read();
    int write();

    int getSerialNr() const
    {
        return m_serial_nr;
    }

    void registerChangeCallback( GenericCallback<int> * );
    void unregisterChangeCallback( GenericCallback<int> * );

    typedef enum {
        NO_HIT,
        PREFIX_HIT,
        FULL_HIT
    } check_path_res_t;

    check_path_res_t checkPath( const std::string &path, std::list<BookmarkDBEntry> &return_entries );
private:
    int m_serial_nr;
    std::list< GenericCallback<int>* > m_change_callbacks;
    std::unique_ptr<BookmarkDB> m_bookmarks;
    PathTree< RefCount<BookmarkDBEntry> > m_pt;

    void bookmarksChanged();
};

#endif
