/* ownop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef OWNOP_H
#define OWNOP_H

#include "wdefines.h"
#include "functionproto.h"

class Worker;

typedef struct { const char *flag;
                 int catcode;
               } ownop_flags_t;
static const ownop_flags_t ownop_flags[] = { { "{Rs{Infotext}{Defaulttext}}", 0 },
                                             { "{pop 0}", 38 },
                                             { "{top 0}", 39 },
                                             { "{size 0}", 49 },
                                             { "{vEnvname}", 50 },
                                             { "{scripts}", 53 },
                                             { "{p}", 1 },
                                             { "{op}", 2 },
                                             { "{lp}", 3 },
                                             { "{rp}", 4 },
                                             { "{dp}", 37 },
                                             { "{f}", 5 },
                                             { "{F}", 6 },
                                             { "{a}", 7 },
                                             { "{A}", 8 },
                                             { "{fE}", 9 },
                                             { "{FE}", 10 },
                                             { "{aE}", 11 },
                                             { "{AE}", 12 },
                                             { "{uf}", 13 },
                                             { "{uF}", 14 },
                                             { "{ua}", 15 },
                                             { "{uA}", 16 },
                                             { "{ufE}", 17 },
                                             { "{uFE}", 18 },
                                             { "{uaE}", 19 },
                                             { "{uAE}", 20 },
                                             { "{of}", 21 },
                                             { "{oF}", 22 },
                                             { "{oa}", 23 },
                                             { "{oA}", 24 },
                                             { "{ofE}", 25 },
                                             { "{oFE}", 26 },
                                             { "{oaE}", 27 },
                                             { "{oAE}", 28 },
                                             { "{ouf}", 29 },
                                             { "{ouF}", 30 },
                                             { "{oua}", 31 },
                                             { "{ouA}", 32 },
                                             { "{oufE}", 33 },
                                             { "{ouFE}", 34 },
                                             { "{ouaE}", 35 },
                                             { "{ouAE}", 36 },
                                             { "{t}", 54 },
                                             { "{ut}", 55 },
                                             { "{ot}", 56 },
                                             { "{out}", 57 },
                                             { "{E}", 59 },
                                             { "{oE}", 60 },
                                             { "{s}", 61 },
                                             { "{os}", 62 },
                                             { "{Tm}", 63 },
                                             { "{oTm}", 64 },
                                             { "{Tc}", 65 },
                                             { "{oTc}", 66 },
                                             { "{currenttime}", 67 },
                                             { "{random}", 68 }
};

class OwnOp:public FunctionProto
{
public:
  OwnOp();
  virtual ~OwnOp();
  OwnOp( const OwnOp &other );
  OwnOp &operator=( const OwnOp &other );

  OwnOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;

  int run( std::shared_ptr< WPUContext > wpu, ActionMessage * ) override;

  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;

  typedef enum {OWNOP_START_NORMAL=0,
                OWNOP_START_IN_TERMINAL,
                OWNOP_START_IN_TERMINAL_AND_WAIT4KEY,
                OWNOP_SHOW_OUTPUT,
                OWNOP_SHOW_OUTPUT_INT,
                OWNOP_SHOW_OUTPUT_OTHER_SIDE,
                OWNOP_SHOW_OUTPUT_CUSTOM_ATTRIBUTE} ownstart_t;
  void setSeparateEachEntry(bool);
  void setRecursive(bool);
  void setOwnStart(ownstart_t);
  void setComStr(const char*);
  void setViewStr(const char*);
  void setInBackground( bool );
  void setTakeDirs( bool );
  void setDontCD( bool );
    void setUseVirtualTempCopies( bool nv );
    void setFlagsAreRelativeToBaseDir( bool nv );
    void setFollowActive( bool );
    void setWatchFile( bool );

    std::string getStringRepresentation() override;

  static char *getFlag();
  static const char *name;
protected:
  // Infos to save
  bool separate_each_entry;
  bool recursive;
  bool inbackground;
  bool take_dirs;
  bool dontcd;

  ownstart_t ownstart;

  char *com_str;
  char *view_str;
  
    bool m_use_virtual_temp_copies;
    bool m_flags_are_relative_to_base_dir;
    bool m_follow_active = false;
    bool m_watch_file = false;
  
    void externprog( std::shared_ptr< WPUContext > wpu, ActionMessage *msg );
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
