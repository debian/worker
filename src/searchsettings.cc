/* searchsettings.cc
 * This file belongs to Worker, a file manager for UNIX/X11.
 * Copyright (C) 2006-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "searchsettings.hh"
#include "aguix/util.h"

SearchSettings::SearchSettings() : _basedir( "" ),
                                   _matchname( "*" ),
                                   m_matchname_separator( "," ),
                                   _matchname_re( false ),
                                   _matchname_fullpath( false ),
                                   _matchname_case( false ),
                                   _matchcontent( "" ),
                                   _matchcontent_case( false ),
                                   _active_row( -1 ),
                                   _follow_symlinks( false ),
                                   _search_vfs( false ),
                                   _search_same_fs( false ),
                                   m_check_younger( false ),
                                   m_younger_than( "" ),
                                   m_check_older( false ),
                                   m_older_than( "" ),
                                   m_check_size_geq( false ),
                                   m_check_size_leq( false ),
                                   m_size_geq( "" ),
                                   m_size_leq( "" ),
                                   m_younger_date( 0 ),
                                   m_older_date( 0 ),
                                   m_size_geq_int( 0 ),
                                   m_size_leq_int( 0 ),
    m_max_vfs_depth( 0 ),
    m_match_also_directories( false )
{
    create_separated_names();
}

SearchSettings::SearchSettings( const std::string &basedir,
                                const std::string &matchname,
                                const std::string &matchname_separator,
                                bool matchname_re,
                                bool matchname_fullpath,
                                bool matchname_case,
                                const std::string &matchcontent,
                                bool matchcontent_case,
                                bool follow_symlinks,
                                bool search_vfs,
                                bool search_same_fs,
                                const std::string &exclude_label,
                                const std::string &match_expression,
                                bool check_younger,
                                const std::string &younger_than,
                                bool check_older,
                                const std::string &older_than ) : _basedir( basedir ),
                                                                  _matchname( matchname ),
                                                                  m_matchname_separator( matchname_separator ),
                                                                  _matchname_re( matchname_re ),
                                                                  _matchname_fullpath( matchname_fullpath ),
                                                                  _matchname_case( matchname_case ),
                                                                  _matchcontent( matchcontent ),
                                                                  _matchcontent_case( matchcontent_case ),
                                                                  _active_row( -1 ),
                                                                  _follow_symlinks( follow_symlinks ),
                                                                  _search_vfs( search_vfs ),
                                                                  _search_same_fs( search_same_fs ),
                                                                  m_exclude_label( exclude_label ),
                                                                  m_match_expression( match_expression ),
                                                                  m_check_younger( check_younger ),
                                                                  m_younger_than( younger_than ),
                                                                  m_check_older( check_older ),
                                                                  m_older_than( older_than ),
                                                                  m_check_size_geq( false ),
                                                                  m_check_size_leq( false ),
                                                                  m_size_geq( "" ),
                                                                  m_size_leq( "" ),
                                                                  m_younger_date( 0 ),
                                                                  m_older_date( 0 ),
                                                                  m_size_geq_int( 0 ),
                                                                  m_size_leq_int( 0 ),
    m_max_vfs_depth( 0 ),
    m_match_also_directories( false )
{
    convertDates();
    create_separated_names();
}

SearchSettings::~SearchSettings()
{
}

std::string SearchSettings::getBaseDir() const
{
  return _basedir;
}

std::string SearchSettings::getMatchName() const
{
  return _matchname;
}

std::string SearchSettings::getMatchNameSeparator() const
{
    return m_matchname_separator;
}

bool SearchSettings::getMatchNameRE() const
{
  return _matchname_re;
}

bool SearchSettings::getMatchNameFullPath() const
{
  return _matchname_fullpath;
}

bool SearchSettings::getMatchNameCase() const
{
  return _matchname_case;
}

std::string SearchSettings::getMatchContent() const
{
  return _matchcontent;
}

bool SearchSettings::getMatchContentCase() const
{
  return _matchcontent_case;
}

int SearchSettings::getActiveRow() const
{
  return _active_row;
}

void SearchSettings::setActiveRow( int nv )
{
  _active_row = nv;
}

bool SearchSettings::getFollowSymlinks() const
{
  return _follow_symlinks;
}

bool SearchSettings::getSearchVFS() const
{
  return _search_vfs;
}

bool SearchSettings::getSearchSameFS() const
{
  return _search_same_fs;
}

std::string SearchSettings::getExcludeLabel() const
{
    return m_exclude_label;
}

std::string SearchSettings::getMatchExpression() const
{
    return m_match_expression;
}

bool SearchSettings::getCheckYounger() const
{
    return m_check_younger;
}

std::string SearchSettings::getYoungerThan() const
{
    return m_younger_than;
}

bool SearchSettings::getCheckOlder() const
{
    return m_check_older;
}

std::string SearchSettings::getOlderThan() const
{
    return m_older_than;
}

time_t SearchSettings::getYoungerThanDate() const
{
    return m_younger_date;
}

time_t SearchSettings::getOlderThanDate() const
{
    return m_older_date;
}

time_t SearchSettings::convertDateString( const std::string &date ) const
{
    struct tm t1;
    time_t ct1;
    int year = -1, month = -1, day = -1;

    //TODO use strptime if available
    //fill struct tm with negativ dst and call mktime
    
    t1.tm_sec = 0;
    t1.tm_min = 0;
    t1.tm_hour = 0;
    t1.tm_isdst = -1;

    if ( sscanf( date.c_str(), "%d-%d-%d", &year, &month, &day ) == 3 ) {
        t1.tm_mday = day;
        t1.tm_mon = month - 1;
        t1.tm_year = year - 1900;
        
        ct1 = mktime( &t1 );
        return ct1;
    } else {
        return (time_t)-1;
    }
}

bool SearchSettings::convertDates()
{
    time_t ct1;

    ct1 = convertDateString( m_younger_than );
    if ( ct1 != (time_t)-1 ) {
        m_younger_date = ct1;
    } else {
        m_younger_date = 0;
    }

    ct1 = convertDateString( m_older_than );
    if ( ct1 != (time_t)-1 ) {
        m_older_date = ct1;
    } else {
        m_older_date = 0;
    }

    return false;
}

bool SearchSettings::getCheckSizeGEQ() const
{
    return m_check_size_geq;
}

bool SearchSettings::getCheckSizeLEQ() const
{
    return m_check_size_leq;
}

std::string SearchSettings::getSizeGEQ() const
{
    return m_size_geq;
}

std::string SearchSettings::getSizeLEQ() const
{
    return m_size_leq;
}

loff_t SearchSettings::getSizeGEQAsInt() const
{
    return m_size_geq_int;
}

loff_t SearchSettings::getSizeLEQAsInt() const
{
    return m_size_leq_int;
}

void SearchSettings::setCheckSizeGEQ( bool nv )
{
    m_check_size_geq = nv;
}

void SearchSettings::setCheckSizeLEQ( bool nv )
{
    m_check_size_leq = nv;
}

void SearchSettings::setSizeGEQ( const std::string &size )
{
    m_size_geq = size;
    m_size_geq_int = AGUIXUtils::convertHumanStringToNumber( size );
    if ( m_size_geq_int < 0 ) {
        m_size_geq_int = 0;
    }
}

void SearchSettings::setSizeLEQ( const std::string &size )
{
    m_size_leq = size;
    m_size_leq_int = AGUIXUtils::convertHumanStringToNumber( size );
    if ( m_size_leq_int < 0 ) {
        m_size_leq_int = 0;
    }
}

void SearchSettings::setMaxVFSDepth( int nv )
{
    m_max_vfs_depth = nv;

    if ( m_max_vfs_depth < 0 ) m_max_vfs_depth = 0;
}

int SearchSettings::getMaxVFSDepth() const
{
    return m_max_vfs_depth;
}

void SearchSettings::setMatchAlsoDirectories( bool nv )
{
    m_match_also_directories = nv;
}

bool SearchSettings::getMatchAlsoDirectories() const
{
    return m_match_also_directories;
}

const std::vector< std::string > &SearchSettings::getSeparatedMatchNames() const
{
    return m_separated_matchnames;
}

void SearchSettings::create_separated_names()
{
    m_separated_matchnames.clear();

    if ( ! m_matchname_separator.empty() ) {
        AGUIXUtils::split_string( m_separated_matchnames,
                                  _matchname,
                                  m_matchname_separator[0] );
    } else {
        m_separated_matchnames.push_back( _matchname );
    }
}
