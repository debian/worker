/* listermode.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef LISTERMODE_H
#define LISTERMODE_H

#include "wdefines.h"
#include "aguix/message.h"
#include "generic_callback.hh"
#include "aguix/refcount.hh"
#include "lister.h"
#include "nmspecialsourceext.hh"
#include <map>
#include <functional>
#include "functionproto.h"
#include "worker_types.h"

class ArgClass;
class Datei;
class DNDMsg;

class ListerMode
{
public:
  ListerMode(Lister *);
  virtual ~ListerMode();
  ListerMode( const ListerMode &other );
  ListerMode &operator=( const ListerMode &other );

  virtual void messageHandler(AGMessage *);
  virtual void messageHandlerInactive(AGMessage *);
  virtual void on();
  virtual void off();
  virtual void activate();
  virtual void deactivate();
  virtual bool isType(const char *);
  virtual const char *getType();
  virtual int configure();
  virtual void cyclicfunc(cyclicfunc_mode_t mode);
  virtual void cyclicfuncInactive( cyclicfunc_mode_t mode );
  virtual const char *getLocaleName();

  virtual int load();
  virtual bool save(Datei *);
  
  virtual void relayout();
/*  static ListerMode *preload(Worker*,Datei*,int side);
  static int presave(Datei*,ListerMode*);*/
  
  virtual bool startdnd(DNDMsg *dm);
  virtual bool isyours( Widget *elem );
  virtual void lvbDoubleClicked();

    virtual void runCommand( const std::string &command );
    virtual void runCommand( const std::string &command, const std::list< RefCount< ArgClass > > &args );
    virtual void registerCommand( const std::string &command,
                                  std::function< void (void) > callback,
                                  const std::string &description = "",
                                  FunctionProto::command_categories_t category = FunctionProto::CAT_SCRIPTING  );
    virtual void registerCommand( const std::string &command,
                                  const std::function< void ( const std::list< RefCount< ArgClass > > ) > callback,
                                  const std::string &description = "",
                                  FunctionProto::command_categories_t category = FunctionProto::CAT_SCRIPTING,
                                  const std::function< std::string ( const std::string &current_input ) > help_callback = nullptr );
    virtual void runCommandWithStrArgs( const std::string &command_str );

    virtual bool pathsChanged( const std::set< std::string > changed_paths );

    typedef enum { LM_GETFILES_ONLYACTIVE,
                   LM_GETFILES_ONLYSELECT,
                   LM_GETFILES_SELORACT} lm_getfiles_t;

    virtual int getSelFiles( std::list< NM_specialsourceExt > &return_list,
                             lm_getfiles_t get_mode,
                             bool unselect = false );

    virtual std::string getCurrentDirectory();

    virtual bool setEntrySelectionState( const std::string &filename,
                                         bool state );

    virtual void updateOnBookmarkChange();

    virtual void not_supported();

    virtual std::list< WorkerTypes::reg_command_info_t > getListOfCommandsWithoutArgs() const;
    virtual std::list< WorkerTypes::reg_command_info_t > getListOfCommandsWithArgs() const;
    virtual WorkerTypes::reg_command_info_t getCommandInfo( const std::string &command, bool args ) const;
    virtual std::string getCommandInputHelp( const std::string &command,
                                             const std::string &current_input ) const;
    virtual bool commandHasInputHelp( const std::string &command ) const;

    virtual void finalizeBGOps();

    virtual void interpretFinalize();
protected:
  Lister *parentlister;
  AGUIX *aguix;
  AWindow *parentawindow;
  static const char *type;

private:
    std::map< std::string, std::function< void( void ) > > m_commands;
    std::map< std::string, std::function< void ( const std::list< RefCount< ArgClass > > ) > > m_commands_with_args;
    std::map< std::pair< std::string, bool >,
              std::tuple< std::string,
                          FunctionProto::command_categories_t,
                          std::function< std::string ( const std::string &current_input ) > > > m_command_info;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
