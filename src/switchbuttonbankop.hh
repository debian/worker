/* switchbuttonbankop.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SWITCHBUTTONBANKOP_H
#define SWITCHBUTTONBANKOP_H

#include "wdefines.h"
#include "functionproto.h"

class SwitchButtonBankOp:public FunctionProto
{
public:
    SwitchButtonBankOp();
    virtual ~SwitchButtonBankOp();
    SwitchButtonBankOp( const SwitchButtonBankOp &other );
    SwitchButtonBankOp &operator=( const SwitchButtonBankOp &other );

    SwitchButtonBankOp *duplicate() const override;
    int configure() override;
    bool isName(const char *) override;
    const char *getName() override;
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save(Datei *) override;
  
    typedef enum {
        SWITCH_TO_NEXT_BANK,
        SWITCH_TO_PREV_BANK,
        SWITCH_TO_BANK_NR
    } switch_bank_mode_t;

    void setSwitchMode( switch_bank_mode_t mode );
    switch_bank_mode_t getSwitchMode() const;

    void setBankNr( int nr );
    int getBankNr() const;

    static const char *name;
private:
    switch_bank_mode_t m_mode;
    int m_bank_nr;
};

#endif
