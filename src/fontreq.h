/* fontreq.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2008,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FONTREQ_H
#define FONTREQ_H

#include "wdefines.h"

#define FONTREQ_MINSIZE 8
#define FONTREQ_STEPSIZE 1
#define FONTREQ_STEPS 40

class AGUIX;
class FieldListView;
class Text;
class Button;
class AWindow;
class List;
class AContainer;
class StringGadget;

class FontRequester
{
public:
  FontRequester(class AGUIX *parent);
  ~FontRequester();
  FontRequester( const FontRequester &other );
  FontRequester &operator=( const FontRequester &other );

  int request(const char *xname,char**return_xname);

  struct request_fonts {
    char *name;
    typedef enum { SCALABLE, FIXED } type_t;
    type_t type;
    char *xname;
    int size;
  };
private:

  AGUIX *aguix;
  FieldListView *lv,*lv2;
  Text *ftext, *missingtext;
  Button *okb,*cb,*xb;
  AWindow *win;
  List *fontlist;
  AContainer *ac1, *ac1_2;
  StringGadget *example_sg;
  
  void showsizes(int i);
  void updatefont();
  int getsize(int i,int j);
  int findfont(const char *xn,int *return_i, int *return_j);
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
