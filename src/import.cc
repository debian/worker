/* import.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "import.h"
#include "worker_commands.h"
#include "wc_color.hh"
#include "wcdoubleshortkey.hh"
#include "wcpath.hh"
#include "wcfiletype.hh"
#include "wchotkey.hh"
#include "wcbutton.hh"
#include "workerversion.h"
#include "wconfig.h"
#include "simplelist.hh"
#include "datei.h"
#include "worker_locale.h"

void WConfig::setDefaultConfig()
{
  List *np, *nb, *nh, *nf;
  WCPath *p1;
  WCDoubleShortkey *key;
  WCButton *b1;
  WCHotkey *h1;
  List *keylist;
  std::shared_ptr< FunctionProto > fp, rfp;
  int i;

  setLang("");
  setRows(6);
  setColumns(7);
  setCacheSize(10);
  List *ncolors=new List();
  ncolors->addElement(new WC_Color(160,160,160));
  ncolors->addElement(new WC_Color(0,0,0));
  ncolors->addElement(new WC_Color(255,255,255));
  ncolors->addElement(new WC_Color(0,85,187));
  ncolors->addElement(new WC_Color(204,34,0));
  ncolors->addElement(new WC_Color(50,180,20));
  ncolors->addElement(new WC_Color(119,0,119));
  ncolors->addElement(new WC_Color(238,170,68));
  setColors(ncolors);
  WC_Color *col=(WC_Color*)ncolors->getFirstElement();
  while(col!=NULL) {
    delete col;
    col=(WC_Color*)ncolors->getNextElement();
  }
  delete ncolors;
  setHBarTop(0,false);
  setHBarTop(1,false);
  setVBarLeft(0,false);
  setVBarLeft(1,true);
  setHBarHeight(0,10);
  setHBarHeight(1,10);
  setVBarWidth(0,10);
  setVBarWidth(1,10);
  clearVisCols( 0 );
  clearVisCols( 1 );
  addListCol( 0, WorkerTypes::LISTCOL_PERM );
  addListCol( 0, WorkerTypes::LISTCOL_SIZE );
  addListCol( 0, WorkerTypes::LISTCOL_NAME );
  addListCol( 0, WorkerTypes::LISTCOL_TYPE );
  addListCol( 1, WorkerTypes::LISTCOL_PERM );
  addListCol( 1, WorkerTypes::LISTCOL_SIZE );
  addListCol( 1, WorkerTypes::LISTCOL_NAME );
  addListCol( 1, WorkerTypes::LISTCOL_TYPE );

  FaceDB faces;

  faces.setColor( "statusbar-fg", 2 );
  faces.setColor( "statusbar-bg", 3 );

  faces.setColor( "lvb-active-fg", 2 );
  faces.setColor( "lvb-active-bg", 4 );
  faces.setColor( "lvb-inactive-fg", 1 );
  faces.setColor( "lvb-inactive-bg", 0 );
  faces.setColor( "dirview-dir-select-fg", 2 );
  faces.setColor( "dirview-dir-select-bg", 3 );
  faces.setColor( "dirview-dir-normal-fg", 3 );
  faces.setColor( "dirview-dir-normal-bg", 0 );
  faces.setColor( "dirview-file-select-fg", 2 );
  faces.setColor( "dirview-file-select-bg", 1 );
  faces.setColor( "dirview-file-normal-fg", 1 );
  faces.setColor( "dirview-file-normal-bg", 0 );
  faces.setColor( "clockbar-fg", 2 );
  faces.setColor( "clockbar-bg", 6 );
  faces.setColor( "request-fg", 1 );
  faces.setColor( "request-bg", 0 );
  faces.setColor( "dirview-header-fg", 1 );
  faces.setColor( "dirview-header-bg", 0 );

  setFaceDB( faces );

  setStartDir(0,"");
  setStartDir(1,"");
#ifdef HAVE_XFT
  setFont( 0, "Sans-10" );
  setFont( 1, "Sans-10" );
  setFont( 2, "Sans-10" );
  setFont( 3, "Sans-10" );
#else
  setFont(0,"fixed");
  setFont(1,"fixed");
  setFont(2,"fixed");
  setFont(3,"fixed");
#endif
  
  // now setup paths
  key=new WCDoubleShortkey();
  keylist = new List();
  keylist->addElement( key );
  np=new List();

  p1=new WCPath();
  p1->setName("/");
  p1->setFG(2);
  p1->setBG(3);
  p1->setPath("/");
  p1->setCheck(true);
  
  key->setMod( 0, 0 );
  key->setKeySym( XK_1, 0 );
  
  p1->setDoubleKeys( keylist );
  np->addElement(p1);

  p1=new WCPath();
  p1->setName("$HOME");
  p1->setPath("$HOME");
  p1->setCheck(true);
  
  key->setMod(0, 0);
  key->setKeySym(XK_2,0);
  
  p1->setDoubleKeys( keylist );
  np->addElement(p1);

  p1=new WCPath();
  p1->setName("/tmp");
  p1->setPath("/tmp");
  p1->setCheck(true);
  
  key->setMod(0,0);
  key->setKeySym(XK_3,0);
  
  p1->setDoubleKeys( keylist );
  np->addElement(p1);

  p1=new WCPath();
  np->addElement(p1);

  p1=new WCPath();
  np->addElement(p1);

  p1=new WCPath();
  np->addElement(p1);

  setPaths(np);
  
  p1=(WCPath*)np->getFirstElement();
  while(p1!=NULL) {
    delete p1;
    p1=(WCPath*)np->getNextElement();
  }
  delete np;
  
  // setup buttons
  command_list_t nc;
  nb=new List();
  
  b1=new WCButton();
  b1->setText("All");
  b1->setCheck(true);
  key->setKeySym(XK_KP_Add,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "selectall" );
      fp = tfp;
  }
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("None");
  b1->setCheck(true);
  key->setKeySym(XK_KP_Subtract,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "selectnone" );
      fp = tfp;
  }
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);
  
  b1=new WCButton();
  b1->setText("Invert selection");
  b1->setCheck(true);
  key->setKeySym(XK_KP_Multiply,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "invertall" );
      fp = tfp;
  }
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Patternselect");
  b1->setCheck(true);
  key->setKeySym(XK_KP_Divide,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< FilterSelectOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("PatternUnselect");
  b1->setCheck(true);
  fp = std::make_shared< FilterUnSelectOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Path2otherside");
  b1->setCheck(true);
  fp = std::make_shared< Path2OSideOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Active2Other");
  b1->setCheck(true);
  fp = std::make_shared< EnterDirOp >();
  std::dynamic_pointer_cast< EnterDirOp >(fp)->setMode(EnterDirOp::ENTERDIROP_ACTIVE2OTHER);
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Reload");
  b1->setCheck(true);
  key->setKeySym(XK_u,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< ReloadOp >();
  std::dynamic_pointer_cast< ReloadOp >(fp)->setReloadside(ReloadOp::RELOADOP_THISSIDE);
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Reload other");
  b1->setCheck(true);
  fp = std::make_shared< ReloadOp >();
  std::dynamic_pointer_cast< ReloadOp >(fp)->setReloadside(ReloadOp::RELOADOP_OTHERSIDE);
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  for(i=0;i<4;i++) nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("Run");
  b1->setCheck(true);
  key->setMod( ControlMask, 0);
  key->setKeySym(XK_x,0);
  key->setMod( ControlMask, 1);
  key->setKeySym( XK_x, 1 );
  key->setType( WCDoubleShortkey::WCDS_DOUBLE );
  b1->setDoubleKeys( keylist );
  key->setType( WCDoubleShortkey::WCDS_NORMAL );
  fp = std::make_shared< StartProgOp >();
  std::dynamic_pointer_cast< StartProgOp >(fp)->setStart(StartProgOp::STARTPROGOP_START_IN_TERMINAL_AND_WAIT4KEY);
  std::dynamic_pointer_cast< StartProgOp >(fp)->setRequestFlags(true);
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  for(i=0;i<13;i++) nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("Copy");
  b1->setCheck(true);
  b1->setFG(2);
  b1->setBG(6);
  key->setKeySym(XK_F5,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< CopyOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Copy++");
  b1->setCheck(true);
  b1->setFG(2);
  b1->setBG(6);
  key->setMod( ShiftMask, 0);
  key->setKeySym(XK_F5,0);
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< CopyOp >();
  std::dynamic_pointer_cast< CopyOp >(fp)->setRequestParameters( true );
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Delete");
  b1->setCheck(true);
  b1->setFG(2);
  b1->setBG(4);
  key->setKeySym(XK_F8,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< DeleteOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());
  
  b1=new WCButton();
  b1->setText("Rename");
  b1->setCheck(true);
  b1->setFG(2);
  b1->setBG(3);
  key->setKeySym(XK_n,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< RenameOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());
  
  b1=new WCButton();
  b1->setText("Move");
  b1->setCheck(true);
  b1->setFG(1);
  b1->setBG(7);
  key->setKeySym(XK_F6,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< CopyOp >();
  std::dynamic_pointer_cast< CopyOp >(fp)->setMove(true);
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Move++");
  b1->setCheck(true);
  b1->setFG(1);
  b1->setBG(7);
  key->setMod( ShiftMask, 0 );
  key->setKeySym(XK_F6,0);
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< CopyOp >();
  std::dynamic_pointer_cast< CopyOp >(fp)->setMove(true);
  std::dynamic_pointer_cast< CopyOp >(fp)->setRequestParameters( true );
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  b1=new WCButton();
  b1->setText("Create Symlink");
  b1->setCheck(true);
  key->setMod( ControlMask, 0);
  key->setKeySym(XK_x,0);
  key->setMod( 0, 1);
  key->setKeySym( XK_s, 1 );
  key->setType( WCDoubleShortkey::WCDS_DOUBLE );
  b1->setDoubleKeys( keylist );
  key->setType( WCDoubleShortkey::WCDS_NORMAL );
  fp = std::make_shared< CreateSymlinkOp >();
  std::dynamic_pointer_cast< CreateSymlinkOp >(fp)->setRequestParameters( true );
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());
  
  b1=new WCButton();
  b1->setText("new dir");
  b1->setCheck(true);
  fp = std::make_shared< MakeDirOp >();
  key->setKeySym(XK_F7,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());
  nb->addElement(new WCButton());
  nb->addElement(new WCButton());
  
  b1=new WCButton();
  b1->setText("Show");
  b1->setCheck(true);
  fp = std::make_shared< InternalViewOp >();
  key->setKeySym(XK_F3,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("Edit");
  b1->setCheck(true);
  key->setKeySym(XK_F4,0);
  key->setMod( 0, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< OwnOp >();
  std::dynamic_pointer_cast< OwnOp >(fp)->setComStr("{scripts}/xeditor {A}");
  nc.push_back(fp);
  rfp = std::make_shared< ReloadOp >();
  nc.push_back(rfp);
  b1->setComs(nc);
  fp.reset();
  rfp.reset();
  nc.clear();
  nb->addElement(b1);

  for(i=0;i<5;i++) nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("Change Symlink");
  b1->setCheck(true);
  key->setMod( ControlMask, 0);
  key->setKeySym(XK_x,0);
  key->setMod( ControlMask, 1);
  key->setKeySym( XK_s, 1 );
  key->setType( WCDoubleShortkey::WCDS_DOUBLE );
  b1->setDoubleKeys( keylist );
  key->setType( WCDoubleShortkey::WCDS_NORMAL );
  fp = std::make_shared< ChangeSymlinkOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  for(i=0;i<3;i++) nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("DirSize");
  b1->setCheck(true);
  fp = std::make_shared< DirSizeOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  for(i=0;i<15;i++) nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("ChMod");
  b1->setCheck(true);
  key->setMod( ControlMask, 0);
  key->setKeySym(XK_x,0);
  key->setMod( 0, 1);
  key->setKeySym( XK_c, 1 );
  key->setType( WCDoubleShortkey::WCDS_DOUBLE );
  b1->setDoubleKeys( keylist );
  key->setType( WCDoubleShortkey::WCDS_NORMAL );
  fp = std::make_shared< ChModOp >();
  std::dynamic_pointer_cast< ChModOp >(fp)->setRequestParameters( true );
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  for(i=0;i<3;i++) nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("Volume manager");
  b1->setCheck(true);
  key->setKeySym(XK_v,0);
  key->setMod( Mod1Mask, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< VolumeManagerOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("Bookmarks");
  b1->setCheck(true);
  key->setKeySym(XK_b,0);
  key->setMod( Mod1Mask, 0 );
  b1->setDoubleKeys( keylist );
  fp = std::make_shared< DirBookmarkOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());

  for(i=0;i<4;i++) nb->addElement(new WCButton());

  b1=new WCButton();
  b1->setText("Quit");
  b1->setCheck(true);
  b1->setFG(2);
  b1->setBG(1);
  fp = std::make_shared< QuitOp >();
  nc.push_back(fp);
  b1->setComs(nc);
  fp.reset();
  nc.clear();
  nb->addElement(b1);

  nb->addElement(new WCButton());

  // apply list
  setButtons(nb);
  
  b1=(WCButton*)nb->getFirstElement();
  while(b1!=NULL) {
    delete b1;
    b1=(WCButton*)nb->getNextElement();
  }
  delete nb;
  
  // setup hotkeys
  nh=new List();
  
  h1=new WCHotkey();
  h1->setName("Up");
  key->setKeySym(XK_Up,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );

  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "up" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Down");
  key->setKeySym(XK_Down,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "down" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Toggle HiddenFlag");
  key->setKeySym(XK_asciicircum,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< ChangeHiddenFlag >();
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Top");
  key->setKeySym(XK_Home,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "first" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Last");
  key->setKeySym(XK_End,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "last" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("PageUp");
  key->setKeySym(XK_Prior,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "pageup" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("PageDown");
  key->setKeySym(XK_Next,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "pagedown" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Invert current entry");
  key->setKeySym(XK_Insert,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "selectentry" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Parent");
  key->setKeySym(XK_Left,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "parentdir" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("EnterDir");
  key->setKeySym(XK_Right,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< EnterDirOp >();
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Change lister settings (left)");
  key->setMod( Mod1Mask, 0 );
  key->setKeySym(XK_F1,0);
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< ChangeListerSetOp >();
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Change lister settings (right)");
  key->setMod( Mod1Mask, 0 );
  key->setKeySym(XK_F2,0);
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< ChangeListerSetOp >();
  std::dynamic_pointer_cast< ChangeListerSetOp >(fp)->setMode(ChangeListerSetOp::CLS_RIGHT_LISTER);
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Switch side");
  key->setKeySym(XK_Tab,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< SwitchListerOp >();
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Simulate DoubleClick");
  key->setKeySym(XK_Return,0);
  key->setMod( 0, 0 );
  h1->setDoubleKeys( keylist );
  {
      auto tfp = std::make_shared< ScriptOp >();
      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
      tfp->setCommandStr( "simulate_doubleclick" );
      fp = tfp;
  }
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Search Entry");
  key->setKeySym(XK_s,0);
  key->setMod( ControlMask, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< SearchEntryOp >();
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Edit path");
  key->setKeySym(XK_Return,0);
  key->setMod( ControlMask, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< EnterPathOp >();
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Edit other path");
  key->setKeySym(XK_Return,0);
  key->setMod( ControlMask | ShiftMask, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< EnterPathOp >();
  std::dynamic_pointer_cast< EnterPathOp >(fp)->setSide(EnterPathOp::ENTERPATHOP_OTHERSIDE);
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Scroll left");
  key->setKeySym(XK_Left,0);
  key->setMod( ControlMask, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< ScrollListerOp >();
  std::dynamic_pointer_cast< ScrollListerOp >(fp)->setDir(ScrollListerOp::SCROLLLISTEROP_LEFT);
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  h1=new WCHotkey();
  h1->setName("Scroll right");
  key->setKeySym(XK_Right,0);
  key->setMod( ControlMask, 0 );
  h1->setDoubleKeys( keylist );
  fp = std::make_shared< ScrollListerOp >();
  std::dynamic_pointer_cast< ScrollListerOp >(fp)->setDir(ScrollListerOp::SCROLLLISTEROP_RIGHT);
  nc.push_back(fp);
  h1->setComs(nc);
  fp.reset();
  nc.clear();
  nh->addElement(h1);

  setHotkeys(nh);
  
  h1=(WCHotkey*)nh->getFirstElement();
  while(h1!=NULL) {
    delete h1;
    h1=(WCHotkey*)nh->getNextElement();
  }
  delete nh;

  nf = new List();  
  setFiletypes( nf );
  delete nf;
  initFixTypes();

  delete key;
  delete keylist;
}
