/* wconfig_imexport.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_imexport.hh"
#include "wconfig.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include <memory>
#include "worker_locale.h"
#include "filereq.h"

ImExportPanel::ImExportPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
  freq = new FileRequester( _aguix );
}

ImExportPanel::~ImExportPanel()
{
  delete freq;
}

int ImExportPanel::create()
{
  Panel::create();
  
  AContainer *ac1 = setContainer( new AContainer( this, 1, 3 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 691 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );
  
  AContainer *ac1_1 = ac1->add( new AContainer( this, 3, 1 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 0 );
  ac1_1->setMaxSpace( 0 );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 692 ) ),
              0, 0, AContainer::CO_FIX );
  
  sg = (StringGadget*)ac1_1->add( new StringGadget( _aguix, 0, 0, 30, "", 0 ),
                                  1, 0, AContainer::CO_INCW );
  fb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 693 ), 0 ),
                            2, 0, AContainer::CO_FIX );
  fb->connect( this );
  
  AContainer *ac1_2 = ac1->add( new AContainer( this, 2, 1 ), 0, 2 );
  ac1_2->setBorderWidth( 0 );
  ac1_2->setMinSpace( 0 );
  ac1_2->setMaxSpace( 0 );
  
  imb = (Button*)ac1_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 397 ), 0 ),
                             0, 0, AContainer::CO_INCW );
  imb->connect( this );
  
  exb = (Button*)ac1_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 398 ), 0 ),
                             1, 0, AContainer::CO_INCW );
  exb->connect( this );
  
  contMaximize( true );
  return 0;
}

int ImExportPanel::saveValues()
{
  return 0;
}

void ImExportPanel::run( Widget *elem, const AGMessage &msg )
{
  bool import_buttons = false, import_hotkeys = false, import_filetypes = false;
  int erg = 0;

  if ( _conf_cb == NULL ) return;
  
  if ( msg.type == AG_BUTTONCLICKED ) {
    if ( msg.button.button == imb ) {
      if ( strlen( sg->getText() ) < 1 ) {
        request( catalog.getLocale( 124 ), catalog.getLocale( 415 ),
                 catalog.getLocale( 11 ) );
      } else {
        std::unique_ptr<WConfig> importconfig( _baseconfig.duplicate() );
        WConfig *conf = NULL;
        
        WConfig::setAlwaysKeepOldKey( false );
        if ( importconfig->loadImportConfig( sg->getText(),
                                             import_buttons,
                                             import_hotkeys,
                                             import_filetypes,
                                             catalog.getLocale( 414 ),
                                             &conf ) == 0 ) {
          if ( import_buttons == true ) {
            erg = _conf_cb->addButtons( conf->getButtons(), WConfigPanelCallBack::CHECK_DOUBLEKEYS );
            if ( erg == 0 ) {
              erg = _conf_cb->addButtons( conf->getButtons(), WConfigPanelCallBack::DO_IMPORT );
            }
          }
          if ( import_hotkeys == true ) {
            if ( erg == 0 ) {
              erg = _conf_cb->addHotkeys( conf->getHotkeys(), WConfigPanelCallBack::CHECK_DOUBLEKEYS );
            }
            if ( erg == 0 ) {
              erg = _conf_cb->addHotkeys( conf->getHotkeys(), WConfigPanelCallBack::DO_IMPORT );
            }
          }
          if ( import_filetypes == true ) {
            if ( erg == 0 ) {
              erg = _conf_cb->addFiletypes( conf->getFiletypes(), WConfigPanelCallBack::DO_IMPORT );
            }
          }

          if ( import_buttons == false && import_hotkeys == false && import_filetypes == false ) {
            request( catalog.getLocale( 124 ), catalog.getLocale( 694 ), catalog.getLocale( 11 ) );
          } else if ( erg == 0 ) {
            request( catalog.getLocale( 124 ), catalog.getLocale( 430 ), catalog.getLocale( 11 ) );
          }
        }
        
        if ( conf != NULL )
          delete conf;
      }
    } else if ( msg.button.button == exb ) {
      if ( strlen( sg->getText() ) > 0 ) {
        _baseconfig.cfg_export( sg->getText() );
      } else {
        request( catalog.getLocale( 124 ), catalog.getLocale( 695 ),
                 catalog.getLocale( 11 ) );
      }
    } else if ( msg.button.button == fb ) {
      // choose file with requester
      if ( freq->request_entry( catalog.getLocale( 265 ),
                                NULL,
                                catalog.getLocale( 11 ),
                                catalog.getLocale( 8 ),
                                catalog.getLocale( 265 ),
                                true ) > 0 ) {
        std::string str1 = freq->getLastEntryStr();
        if ( str1.length() > 0 ) {
          sg->setText( str1.c_str() );
        }
      }
    }
  }
}
