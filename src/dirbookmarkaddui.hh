/* dirbookmarkaddui.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2008,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DIRBOOKMARKADDUI_HH
#define DIRBOOKMARKADDUI_HH

#include "wdefines.h"
#include <memory>
#include <string>

class BookmarkDBProxy;

class DirBookmarkAddUI
{
public:
    DirBookmarkAddUI( class AGUIX &aguix, BookmarkDBProxy &data,
                      const std::string &dirname, const std::string &basename );
    ~DirBookmarkAddUI();

    int mainLoop();
private:
    class AGUIX &m_aguix;
    BookmarkDBProxy &m_data;
    std::string m_dirname, m_basename;
    
    std::unique_ptr<class AWindow> m_win;
    class Button *m_okb_op1, *m_okb_op2, *m_okb_op3;
    class Button *m_cancelb;
};

#endif
