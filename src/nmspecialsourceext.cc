/* nmspecialsourceext.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2007,2011,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "nmspecialsourceext.hh"
#include "fileentry.hh"

NM_specialsourceExt::NM_specialsourceExt() : fe( NULL ), m_id( -1 )
{
}

NM_specialsourceExt::NM_specialsourceExt( const FileEntry *tfe,
                                          int id ) : m_id( id )
{
    if ( tfe == NULL ) {
        fe = NULL;
    } else {
        fe = new FileEntry( *tfe );
    }
}

NM_specialsourceExt::NM_specialsourceExt( const NM_specialsourceExt &other ) : fe( NULL ), m_id( other.m_id )
{
    if ( other.fe ) {
        fe = new FileEntry( *other.fe );
    }
}

NM_specialsourceExt::NM_specialsourceExt( NM_specialsourceExt &&other ) : m_id( other.m_id )
{
    fe = other.fe;
    other.fe = nullptr;
}

NM_specialsourceExt &NM_specialsourceExt::operator=( const NM_specialsourceExt &rhs )
{
    if ( this != &rhs ) {
        delete fe;

        if ( rhs.fe ) {
            fe = new FileEntry( *rhs.fe );
        } else {
            fe = NULL;
        }

        m_id = rhs.m_id;
    }
    return *this;
}

NM_specialsourceExt &NM_specialsourceExt::operator=( NM_specialsourceExt &&rhs )
{
    if ( this != &rhs ) {
        delete fe;

        fe = rhs.fe;
        rhs.fe = nullptr;

        m_id = rhs.m_id;
    }
    return *this;
}

NM_specialsourceExt::~NM_specialsourceExt()
{
    delete fe;
}
