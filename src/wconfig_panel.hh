/* wconfig_panel.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_PANEL_HH
#define WCONFIG_PANEL_HH

#include "wdefines.h"
#include "panel.hh"
#include "wconfig_initial_command.hh"

class WConfig;
class List;

class WConfigPanelCallBack
{
public:
  WConfigPanelCallBack();
  virtual ~WConfigPanelCallBack();
  virtual void setColors( List *colors );
  virtual void setRows( int rows );
  virtual void setColumns( int columns );
  virtual void setFaceDB( const FaceDB &faces ) = 0;

  typedef enum { CHECK_DOUBLEKEYS, DO_IMPORT } add_action_t;
  virtual int addButtons( List *buttons, add_action_t action );
  virtual int addHotkeys( List *hotkeys, add_action_t action );
  virtual int addFiletypes( List *filetypes, add_action_t action );
};

class WConfigPanel : public Panel
{
public:
  WConfigPanel( AWindow &basewin, WConfig &baseconfig );
  ~WConfigPanel();
  
  typedef enum { PANEL_NOACTION, PANEL_RECREATE } panel_action_t;

  virtual panel_action_t setColors( List *colors );
  virtual panel_action_t setRows( int rows );
  virtual panel_action_t setColumns( int columns );
  virtual panel_action_t setFaceDB( const FaceDB &faces );
  virtual int addButtons( List *buttons, WConfigPanelCallBack::add_action_t action );
  virtual int addHotkeys( List *hotkeys, WConfigPanelCallBack::add_action_t action );
  virtual int addFiletypes( List *filetypes, WConfigPanelCallBack::add_action_t action );
  virtual void executeInitialCommand( const WConfigInitialCommand &cmd );

  /**
   * indicates that recreation is required cause some values
   * changed
   */
  virtual bool need_recreate() const;

  virtual void setConfCB( WConfigPanelCallBack *conf_cb );

    virtual bool escapeInUse();
protected:
  WConfig &_baseconfig;
  WConfigPanelCallBack *_conf_cb;
  bool _need_recreate;
};
 
#endif
