/* argclass.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef ARGCLASS_HH
#define ARGCLASS_HH

#include "wdefines.h"
#include <string>
#include <memory>

class ArgClass
{
public:
    virtual ~ArgClass() = 0;
};

class IntArg;

class StringArg : public ArgClass
{
public:
    StringArg( const std::string &value ) : m_value( value )
    {}

    std::string getValue() const
    {
        return m_value;
    }

    std::pair< IntArg, bool > convertToInt() const;
private:
    std::string m_value;
};

class IntArg : public ArgClass
{
public:
    IntArg( int value ) : m_value( value )
    {}

    int getValue() const
    {
        return m_value;
    }
private:
    int m_value;
};

class BoolArg : public ArgClass
{
public:
    BoolArg( bool value ) : m_value( value )
    {}

    bool getValue() const
    {
        return m_value;
    }
private:
    bool m_value;
};

class KeyValueArg : public ArgClass
{
public:
    KeyValueArg( const std::string &key,
                 std::shared_ptr< ArgClass > value ) :
        m_key( key ),
        m_value( value )
    {}

    const std::string &key() const {
        return m_key;
    }

    std::shared_ptr< ArgClass > value() const {
        return m_value;
    }
private:
    std::string m_key;
    std::shared_ptr< ArgClass > m_value;
};

#endif
