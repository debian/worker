/* waitthread.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2010,2012 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WAITTHREAD_HH
#define WAITTHREAD_HH

#include "wdefines.h"
#include <string>
#include <list>
#include <memory>
#include "aguix/thread.hh"
#include "aguix/condvar.h"
#include "aguix/refcount.hh"

class EventQueue;
class TimeoutStore;

class WaitThread : public Thread
{
public:
    WaitThread( RefCount< EventQueue> eq,
                std::shared_ptr< TimeoutStore > timeouts );
    ~WaitThread();
    WaitThread( const WaitThread &other );
    WaitThread &operator=( const WaitThread &other );
    
    void watchFD( int fd );
    void watchFD2( int fd2 );
    void watchFD3( int fd3 );
    int run();
    
    typedef enum { WAITTHREAD_NONE,
                   WAITTHREAD_SELECT,
                   WAITTHREAD_STOP } waitthread_state_t;

    int state_set( waitthread_state_t v );
private:
    RefCount< EventQueue > m_eq;
    std::shared_ptr< TimeoutStore > m_timeouts;
    int m_fd, m_fd2, m_fd3;

    waitthread_state_t m_state;
    CondVar m_state_cv;
};

#endif
