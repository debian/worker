/* wconfig_start.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_start.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "aguix/stringgadget.h"
#include "aguix/cyclebutton.h"

StartPanel::StartPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

StartPanel::~StartPanel()
{
}

int StartPanel::create()
{
  Panel::create();

  AContainer *ac1 = setContainer( new AContainer( this, 1, 3 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 10 );
  ac1->setMaxSpace( 10 );

  addMultiLineText( catalog.getLocale( 681 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 2 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 46 ) ),
              0, 0, AContainer::CO_FIX );
  
  sg1 = (StringGadget*)ac1_1->add( new StringGadget( _aguix, 0, 0, 100, _baseconfig.getStartDir( 0 ), 0 ),
                                   1, 0, AContainer::CO_INCW );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 47 ) ),
              0, 1, AContainer::CO_FIX );
  
  sg2 = (StringGadget*)ac1_1->add( new StringGadget( _aguix, 0, 0, 100, _baseconfig.getStartDir( 1 ), 0 ),
                                   1, 1, AContainer::CO_INCW );
  
  AContainer *ac1_2 = ac1->add( new AContainer( this, 2, 2 ), 0, 2 );
  ac1_2->setBorderWidth( 0 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( 5 );

  ac1_2->add( new Text( _aguix, 0, 0, catalog.getLocale( 1064 ) ),
              0, 0, AContainer::CO_FIX );

  m_restore_paths_cycb = ac1_2->addWidget( new CycleButton( _aguix, 0, 0, 50, 0 ),
                                           1, 0, AContainer::CO_INCW );

  m_restore_paths_cycb->addOption( catalog.getLocale( 1065 ) );
  m_restore_paths_cycb->addOption( catalog.getLocale( 1066 ) );
  m_restore_paths_cycb->addOption( catalog.getLocale( 1067 ) );

  switch ( _baseconfig.getRestoreTabsMode() ) {
      case WConfig::RESTORE_TABS_ALWAYS:
          m_restore_paths_cycb->setOption( 1 );
          break;
      case WConfig::RESTORE_TABS_ASK:
          m_restore_paths_cycb->setOption( 2 );
          break;
      case WConfig::RESTORE_TABS_NEVER:
      default:
          m_restore_paths_cycb->setOption( 0 );
          break;
  }
  m_restore_paths_cycb->resize( m_restore_paths_cycb->getMaxSize(),
                                m_restore_paths_cycb->getHeight() );

  ac1_2->add( new Text( _aguix, 0, 0, catalog.getLocale( 1068 ) ),
              0, 1, AContainer::CO_FIX );

  m_store_paths_cycb = ac1_2->addWidget( new CycleButton( _aguix, 0, 0, 50, 0 ),
                                         1, 1, AContainer::CO_INCW );

  m_store_paths_cycb->addOption( catalog.getLocale( 1069 ) );
  m_store_paths_cycb->addOption( catalog.getLocale( 1070 ) );
  m_store_paths_cycb->addOption( catalog.getLocale( 1071 ) );
  m_store_paths_cycb->addOption( catalog.getLocale( 1072 ) );

  switch ( _baseconfig.getStoreTabsMode() ) {
      case WConfig::STORE_TABS_ALWAYS:
          m_store_paths_cycb->setOption( 1 );
          break;
      case WConfig::STORE_TABS_AS_EXIT_STATE:
          m_store_paths_cycb->setOption( 2 );
          break;
      case WConfig::STORE_TABS_ASK:
          m_store_paths_cycb->setOption( 3 );
          break;
      case WConfig::STORE_TABS_NEVER:
      default:
          m_store_paths_cycb->setOption( 0 );
          break;
  }
  m_store_paths_cycb->resize( m_store_paths_cycb->getMaxSize(),
                              m_store_paths_cycb->getHeight() );
  ac1_2->readLimits();

  contMaximize( true );
  return 0;
}

int StartPanel::saveValues()
{
  _baseconfig.setStartDir( 0, sg1->getText() );
  _baseconfig.setStartDir( 1, sg2->getText() );

  switch ( m_restore_paths_cycb->getSelectedOption() ) {
      case 1:
          _baseconfig.setRestoreTabsMode( WConfig::RESTORE_TABS_ALWAYS );
          break;
      case 2:
          _baseconfig.setRestoreTabsMode( WConfig::RESTORE_TABS_ASK );
          break;
      default:
          _baseconfig.setRestoreTabsMode( WConfig::RESTORE_TABS_NEVER );
          break;
  }

  switch ( m_store_paths_cycb->getSelectedOption() ) {
      case 1:
          _baseconfig.setStoreTabsMode( WConfig::STORE_TABS_ALWAYS );
          break;
      case 2:
          _baseconfig.setStoreTabsMode( WConfig::STORE_TABS_AS_EXIT_STATE );
          break;
      case 3:
          _baseconfig.setStoreTabsMode( WConfig::STORE_TABS_ASK );
          break;
      default:
          _baseconfig.setStoreTabsMode( WConfig::STORE_TABS_NEVER );
          break;
  }

  return 0;
}
