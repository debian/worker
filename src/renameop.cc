/* renameop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "renameop.h"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "worker_locale.h"
#include "renameorder.hh"
#include "virtualdirmode.hh"
#include "datei.h"
#include "aguix/awindow.h"
#include "aguix/choosebutton.h"
#include "aguix/acontainer.h"
#include "aguix/button.h"

const char *RenameOp::name="RenameOp";

RenameOp::RenameOp() : FunctionProto()
{
    m_category = FunctionProto::CAT_FILEOPS;

    hasConfigure = true;
}

RenameOp::~RenameOp()
{
}

RenameOp*
RenameOp::duplicate() const
{
  RenameOp *ta=new RenameOp();
  ta->setAlsoSelectFileExtension( m_also_select_file_extension );
  return ta;
}

bool
RenameOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
RenameOp::getName()
{
  return name;
}

int
RenameOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if(dynamic_cast< VirtualDirMode * >( lm1 ) ) {
              normalmoderename( msg );
          } else {
              lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

bool
RenameOp::save( Datei *fh )
{
    if ( fh == NULL ) return false;
    if ( m_also_select_file_extension ) {
        fh->configPutPairBool( "alsoselectextension", true );
    }
    return true;
}

const char *
RenameOp::getDescription()
{
  return catalog.getLocale(1270);
}

int
RenameOp::normalmoderename( ActionMessage *am )
{
  ListerMode *lm1=NULL;
  struct NM_renameorder renorder;
  NM_specialsourceExt *specialsource=NULL;
  
  if(startlister==NULL) return 1;
  lm1=startlister->getActiveMode();
  if(lm1==NULL) return 1;
  
  if(am->mode==am->AM_MODE_ONLYACTIVE)
    renorder.source=renorder.NM_ONLYACTIVE;
  else if(am->mode==am->AM_MODE_DNDACTION) {
    // insert DND-element into list
    renorder.source=renorder.NM_SPECIAL;
    renorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( NULL );
    //TODO: specialsource nach am besetzen (je nachdem wir ich das realisiere)
    renorder.sources->push_back(specialsource);
  } else if(am->mode==am->AM_MODE_SPECIAL) {
    renorder.source=renorder.NM_SPECIAL;
    renorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( am->getFE() );
    renorder.sources->push_back(specialsource);
  } else {
    renorder.source=renorder.NM_ALLENTRIES;
  }

  renorder.also_select_extension = m_also_select_file_extension;

  if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
      vdm->renamef( &renorder );
  }

  if(renorder.source==renorder.NM_SPECIAL) {
    if ( specialsource != NULL ) delete specialsource;
    delete renorder.sources;
  }

  return 0;
}

int
RenameOp::configure()
{
    return doconfigure(0);
}

int
RenameOp::doconfigure( int mode )
{
    AGUIX *aguix = Worker::getAGUIX();
    AGMessage *msg;

    int endmode = -1;

    auto title = AGUIXUtils::formatStringToString( catalog.getLocale( 293 ),
                                                   getDescription() );
    auto win = std::unique_ptr< AWindow >( new AWindow( aguix, 10, 10, 10, 10, title.c_str(), AWindow::AWINDOW_DIALOG ) );
    win->create();

    AContainer *ac1 = win->setContainer( new AContainer( win.get(), 1, 2 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );

    auto include_extension_cb = ac1->addWidget( new ChooseButton( aguix, 0, 0, m_also_select_file_extension, catalog.getLocale( 1564 ), LABEL_LEFT, 0 ),
                                                0, 0, AContainer::CO_FIXNR );

    AContainer *ac1_5 = ac1->add( new AContainer( win.get(), 2, 1 ), 0, 1 );
    ac1_5->setMinSpace( 5 );
    ac1_5->setMaxSpace( -1 );
    ac1_5->setBorderWidth( 0 );
    Button *okb = ac1_5->addWidget( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, AContainer::CO_FIX );
    Button *cancelb = ac1_5->addWidget( new Button( aguix,
                                                    0,
                                                    0,
                                                    catalog.getLocale( 8 ),
                                                    0 ), 1, 0, AContainer::CO_FIX );
    win->contMaximize( true );

    win->setDoTabCycling( true );
    win->show();
    for( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win.get() );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cancelb ) endmode = 1;
                    break;
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Return:
                                if ( cancelb->getHasFocus() == true ) {
                                    endmode = 1;
                                } else {
                                    endmode=0;
                                }
                                break;
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
  
    if ( endmode == 0 ) {
        // ok
        setAlsoSelectFileExtension( include_extension_cb->getState() );
    }

    return endmode;
}

void RenameOp::setAlsoSelectFileExtension( bool nv )
{
    m_also_select_file_extension = nv;
}
