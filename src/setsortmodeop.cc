/* setsortmodeop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "setsortmodeop.h"
#include "listermode.h"
#include "worker.h"
#include "verzeichnis.hh"
#include "datei.h"
#include "aguix/button.h"
#include "aguix/cyclebutton.h"
#include "aguix/choosebutton.h"
#include "worker_locale.h"
#include "virtualdirmode.hh"

/*****************
 SetSortmodeOp
******************/

const char *SetSortmodeOp::name="SetSortmodeOp";

SetSortmodeOp::SetSortmodeOp() : FunctionProto()
{
  mode=SORT_NAME;
  hasConfigure = true;
    m_category = FunctionProto::CAT_FILELIST;
}

SetSortmodeOp::~SetSortmodeOp()
{
}

SetSortmodeOp *SetSortmodeOp::duplicate() const
{
  SetSortmodeOp *ta=new SetSortmodeOp();
  ta->mode=mode;
  return ta;
}

bool SetSortmodeOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *SetSortmodeOp::getName()
{
  return name;
}

int SetSortmodeOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      lm1=l1->getActiveMode();
      if(lm1!=NULL) {
        if ( auto vdm = dynamic_cast< VirtualDirMode *>( lm1 ) ) {
            vdm->setSortmode( mode );
        }
      }
    }
  }
  return 0;
}

bool SetSortmodeOp::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  switch ( mode & 0xff ) {
    case SORT_SIZE:
      fh->configPutPair( "sortby", "size" );
      break;
    case SORT_ACCTIME:
      fh->configPutPair( "sortby", "acctime" );
      break;
    case SORT_MODTIME:
      fh->configPutPair( "sortby", "modtime" );
      break;
    case SORT_CHGTIME:
      fh->configPutPair( "sortby", "chgtime" );
      break;
    case SORT_TYPE:
      fh->configPutPair( "sortby", "type" );
      break;
    case SORT_OWNER:
      fh->configPutPair( "sortby", "owner" );
      break;
    case SORT_INODE:
      fh->configPutPair( "sortby", "inode" );
      break;
    case SORT_NLINK:
      fh->configPutPair( "sortby", "nlink" );
      break;
    case SORT_PERMISSION:
      fh->configPutPair( "sortby", "permission" );
      break;
    case SORT_EXTENSION:
      fh->configPutPair( "sortby", "extension" );
      break;
    case SORT_CUSTOM_ATTRIBUTE:
      fh->configPutPair( "sortby", "customattribute" );
      break;
    default:
      fh->configPutPair( "sortby", "name" );
      break;
  }
  if ( ( mode & SORT_REVERSE ) != 0 ) fh->configPutPair( "sortflag", "reverse" );
  if ( ( mode & SORT_DIRLAST ) != 0 ) fh->configPutPair( "sortflag", "dirlast" );
  else if ( ( mode & SORT_DIRMIXED ) != 0 ) fh->configPutPair( "sortflag", "dirmixed" );
  return true;
}

const char *SetSortmodeOp::getDescription()
{
  return catalog.getLocale(1281);
}

int SetSortmodeOp::configure()
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  CycleButton *cyb[2];
  ChooseButton *chb;
  AGMessage *msg;
  int endmode=-1;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr=(char*)_allocsafe(strlen(catalog.getLocale(293))+strlen(getName())+1);
  sprintf(tstr,catalog.getLocale(293),getName());
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 4 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  ac1_1->setBorderWidth( 0 );

  ac1_1->add( new Text( aguix, 0, 0, catalog.getLocale( 158 ) ), 0, 0, cfix );
  cyb[0] = (CycleButton*)ac1_1->add( new CycleButton( aguix, 0, 0, 100, 0 ), 1, 0, cincw );
  cyb[0]->addOption(catalog.getLocale(160));
  cyb[0]->addOption(catalog.getLocale(161));
  cyb[0]->addOption(catalog.getLocale(162));
  cyb[0]->addOption(catalog.getLocale(163));
  cyb[0]->addOption(catalog.getLocale(164));
  cyb[0]->addOption(catalog.getLocale( 432 ));
  cyb[0]->addOption(catalog.getLocale( 433 ));
  cyb[0]->addOption(catalog.getLocale( 552 ));
  cyb[0]->addOption(catalog.getLocale( 553 ));
  cyb[0]->addOption( catalog.getLocale( 907 ) );
  cyb[0]->addOption( catalog.getLocale( 1240 ) );
  cyb[0]->addOption( catalog.getLocale( 1534 ) );
  cyb[0]->resize(cyb[0]->getMaxSize(),cyb[0]->getHeight());
  if((mode&0xff)==SORT_SIZE) cyb[0]->setOption(1);
  else if((mode&0xff)==SORT_ACCTIME) cyb[0]->setOption(2);
  else if((mode&0xff)==SORT_MODTIME) cyb[0]->setOption(3);
  else if((mode&0xff)==SORT_CHGTIME) cyb[0]->setOption(4);
  else if((mode&0xff)==SORT_TYPE) cyb[0]->setOption(5);
  else if((mode&0xff)==SORT_OWNER) cyb[0]->setOption(6);
  else if ( ( mode & 0xff ) == SORT_INODE ) cyb[0]->setOption( 7 );
  else if ( ( mode & 0xff ) == SORT_NLINK ) cyb[0]->setOption( 8 );
  else if ( ( mode & 0xff ) == SORT_PERMISSION ) cyb[0]->setOption( 9 );
  else if ( ( mode & 0xff ) == SORT_EXTENSION ) cyb[0]->setOption( 10 );
  else if ( ( mode & 0xff ) == SORT_CUSTOM_ATTRIBUTE ) cyb[0]->setOption( 11 );
  else cyb[0]->setOption(0);
  ac1_1->readLimits();

  chb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20,
						   ( ( mode & SORT_REVERSE ) == SORT_REVERSE ) ? 1 : 0,
						   catalog.getLocale( 165 ), LABEL_RIGHT, 0 ), 0, 1, cincwnr );

  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 2 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( 5 );
  ac1_2->setBorderWidth( 0 );

  ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 353 ) ), 0, 0, cfix );
  cyb[1] = (CycleButton*)ac1_2->add( new CycleButton( aguix, 0, 0, 100, 0 ), 1, 0, cincw );
  cyb[1]->addOption(catalog.getLocale(354));
  cyb[1]->addOption(catalog.getLocale(355));
  cyb[1]->addOption(catalog.getLocale(356));
  cyb[1]->resize(cyb[1]->getMaxSize(),cyb[1]->getHeight());
  if((mode&SORT_DIRLAST)==SORT_DIRLAST) cyb[1]->setOption(1);
  else if((mode&SORT_DIRMIXED)==SORT_DIRMIXED) cyb[1]->setOption(2);
  else cyb[1]->setOption(0);
  ac1_2->readLimits();

  AContainer *ac1_3 = ac1->add( new AContainer( win, 2, 1 ), 0, 3 );
  ac1_3->setMinSpace( 5 );
  ac1_3->setMaxSpace( -1 );
  ac1_3->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_3->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_3->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );
  
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) endmode=0;
          else if(msg->button.button==cb) endmode=1;
          break;
        case AG_KEYPRESSED:
            if ( win->isParent( msg->key.window, false ) == true ) {
                switch ( msg->key.key ) {
                    case XK_Escape:
                        endmode = 1;
                        break;
                }
            }
            break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  
  if(endmode==0) {
    // ok
    int tsortmode=0;
    switch(cyb[0]->getSelectedOption()) {
      case 1:
        tsortmode=SORT_SIZE;
        break;
      case 2:
        tsortmode=SORT_ACCTIME;
        break;
      case 3:
        tsortmode=SORT_MODTIME;
        break;
      case 4:
        tsortmode=SORT_CHGTIME;
        break;
      case 5:
        tsortmode=SORT_TYPE;
        break;
      case 6:
        tsortmode=SORT_OWNER;
        break;
      case 7:
        tsortmode=SORT_INODE;
        break;
      case 8:
        tsortmode=SORT_NLINK;
        break;
      case 9:
        tsortmode=SORT_PERMISSION;
        break;
      case 10:
        tsortmode = SORT_EXTENSION;
        break;
      case 11:
        tsortmode = SORT_CUSTOM_ATTRIBUTE;
        break;
      default:
        tsortmode=SORT_NAME;
        break;
    }
    if ( chb->getState() == true ) tsortmode |= SORT_REVERSE;
    switch(cyb[1]->getSelectedOption()) {
      case 1:
        tsortmode|=SORT_DIRLAST;
        break;
      case 2:
        tsortmode|=SORT_DIRMIXED;
        break;
      default:
        break;
    }
    mode=tsortmode;
  }
  delete win;

  return endmode;
}

void SetSortmodeOp::setMode( int nv )
{
  if ( ISVALID_SORTMODE( nv ) ) mode = nv & ( 0xff | SORT_REVERSE | SORT_DIRLAST | SORT_DIRMIXED );
  else mode = SORT_NAME;
}

int SetSortmodeOp::getMode() const
{
  return mode;
}

void SetSortmodeOp::setSortby( int m )
{
  int usem;
  
  if ( ISVALID_SORTMODE( m ) ) usem = m & 0xff;
  else usem = SORT_NAME;
  
  mode = usem | ( mode & ( ~ 0xff ) );
}

void SetSortmodeOp::setSortFlag( int f )
{
  if ( ( f & SORT_REVERSE ) != 0 ) mode |= SORT_REVERSE;
  if ( ( f & SORT_DIRLAST ) != 0 ) {
    mode |= SORT_DIRLAST;
    mode &= ~SORT_DIRMIXED;
  } else if ( ( f & SORT_DIRMIXED ) != 0 ) {
    mode |= SORT_DIRMIXED;
    mode &= ~SORT_DIRLAST;
  }
}

std::string SetSortmodeOp::getStringRepresentation()
{
    std::string res = getDescription();

    res += " (";

    if ( ( mode & 0xff ) == SORT_SIZE ) res += catalog.getLocale( 161 );
    else if ( ( mode & 0xff ) == SORT_ACCTIME ) res += catalog.getLocale( 162 );
    else if ( ( mode & 0xff ) == SORT_MODTIME ) res += catalog.getLocale( 163 );
    else if ( ( mode & 0xff ) == SORT_CHGTIME ) res += catalog.getLocale( 164 );
    else if ( ( mode & 0xff ) == SORT_TYPE ) res += catalog.getLocale( 432 );
    else if ( ( mode & 0xff ) == SORT_OWNER ) res += catalog.getLocale( 433 );
    else if ( ( mode & 0xff ) == SORT_INODE ) res += catalog.getLocale( 552 );
    else if ( ( mode & 0xff ) == SORT_NLINK ) res += catalog.getLocale( 553 );
    else if ( ( mode & 0xff ) == SORT_PERMISSION ) res += catalog.getLocale( 907 );
    else if ( ( mode & 0xff ) == SORT_EXTENSION ) res += catalog.getLocale( 1240 );
    else if ( ( mode & 0xff ) == SORT_CUSTOM_ATTRIBUTE ) res += catalog.getLocale( 1534 );
    else res += catalog.getLocale( 160 );

    if ( ( mode & SORT_REVERSE ) == SORT_REVERSE ) {
        res += ", ";
        res += catalog.getLocale( 1374 );
    }

    res += ")";

    return res;
}
