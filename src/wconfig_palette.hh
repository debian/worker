/* wconfig_palette.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2006 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_PALETTE_HH
#define WCONFIG_PALETTE_HH

#include "wdefines.h"
#include "wconfig_panel.hh"

class WConfig;
class SolidButton;

class PalettePanel : public WConfigPanel
{
public:
  PalettePanel( AWindow &basewin, WConfig &baseconfig );
  ~PalettePanel();
  int create();
  int saveValues();

  /* gui elements callback */
  void run( Widget *, const AGMessage &msg ) override;

  /* set values to panel callback before hiding */
  void hide();

  /* we are interested in updated colors */
  panel_action_t setColors( List *colors );
protected:
  WConfig *tempconfig;
  Button *colbs[256];
  Text *palentryt;
  StringGadget *redsg, *greensg, *bluesg;
  Slider *redsl, *greensl, *bluesl;
  SolidButton *editcolsb;
  int labelindex;

  bool _values_changed;

  void showColor( int col );
};
 
#endif
