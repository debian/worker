%option nounput
%option warn nodefault
%option yylineno
/*%option outfile="configscanner.cc"*/
%top{
/* scanner for config file (flex generated)
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2003-2004 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: configscanner.l,v 1.7 2004/10/27 21:14:45 ralf Exp $ */

#include "wdefines.h"
#include "wconfig.h"
#include "configparser.hh"
#include <string.h>
#include <list>
}

%{
#include "configtokens.h"

void readcomment();
void removebackslash(char *, char *);
char *dupstring_lex( const char *);

#define YY_NO_UNPUT
int lconfig_linenr = 0;

int worker_token = 0;
std::list <char*> *lexer_string_list = NULL;

%}

delim	[ \t]
ws	{delim}+
letter	[A-Za-z]
eletter	[_A-Za-z]
digit	[0-9]
hexdigit	[0-9a-fA-F]
id	{eletter}({eletter}|{digit})*
fpnum	[\+\-]?{digit}+(\.{digit}+)?([Ee][\+\-]?{digit}+)?
num	[\+\-]?(([1-9]{digit}+)|({digit}))
onum	0[0-7]+
hexnum	0x{hexdigit}+

%%
\/\*	{ readcomment(); }
\"([^"\n\\]|\\.|\\\n)*\"	{ removebackslash(yytext, yytext); yylval.strptr = dupstring_lex( yytext );
				  return STRING_WCP; }
{ws}	{;}
\{	{return LEFTBRACE_WCP;}
\}	{return RIGHTBRACE_WCP;}
yes	{ return YES_WCP; }
no	{ return NO_WCP; }
true	{ return YES_WCP; }
false	{ return NO_WCP; }
{id}	{ return findToken( yytext ); }
{num}	{yylval.num=atoi(yytext);return NUM_WCP;}
\n	{lconfig_linenr++;}
.	{return yytext[0];}
%%
#ifndef yywrap
int yywrap()
{
  return 1;
}
#endif

void readcomment()
{
  /* liest Kommentar bis zum schliessenden Ende, egal in welcher Zeile */
  int ch;
  int mode=0;
  do {
    ch=yyinput();
    if ( ch == '\n' ) lconfig_linenr++;
    switch(mode) {
      case 1:
        if(ch=='/') mode=2;
	else if(ch!='*') mode=0;
        break;
      default:
        if(ch=='*') mode=1;
        break;
    }
  } while(mode!=2);
}

void removebackslash(char *src, char *dest)
{
  /* Entfernt die Backslash-Sequencen, alle geschuetzten Zeichen werden einge-
     fuegt, ausser dem Return */
  src++;
  while(*src!='"') {
    if((*src)=='\\') {
      src++;
      if(*src=='\n') {
        *dest++=*src++;
        lconfig_linenr++;
      } else *dest++=*src++;
    } else *dest++=*src++;
  }
  *dest='\0';
}

void readtoken()
{
  worker_token = yylex();
}

/*
 * dupstring_lex
 *
 * just the same like dupstring but store the pointer
 * in a list for freeing them at the end
 * 
 * there is an other way using much less memory by freeing
 * strings right after the parser used it but that's a mess
 * this way is easy and clean and even for a huge config the
 * waste of memory isn't such big that you would care
 */
char *dupstring_lex( const char *t)
{
  char *tstr;
  
  if ( t == NULL ) return NULL;
  tstr = dupstring( t );
  
  if ( lexer_string_list == NULL ) {
    lexer_string_list = new std::list <char*>;
  }
  lexer_string_list->push_back( tstr );
  
  return tstr;
}

void lexer_cleanup()
{
  if ( lexer_string_list != NULL ) {
    while ( lexer_string_list->empty() != true ) {
      _freesafe( lexer_string_list->front() );
      lexer_string_list->pop_front();
    }
    delete lexer_string_list;
    lexer_string_list = NULL;
  }
}

