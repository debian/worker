/* slider.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2005-2007,2012 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SLIDER_H
#define SLIDER_H

#include "aguixdefs.h"
#include "guielement.h"
#include "callback.h"

class AGUIX;

class Slider : public GUIElement
{
public:
  Slider( AGUIX *caguix, int cx, int cy, int cwidth, int cheight, bool cvertical, int cdata );
  ~Slider();
  Slider( const Slider &other );
  Slider &operator=( const Slider &other );

  void redraw();
  void resize( int w, int h );
  int getData() const;
  void setData( int );
  void flush();
  bool handleMessage( XEvent *E, Message *msg );
  void handleBar( Message *msg );
  bool handleKeys( Message *msg );

  const char *getType() const;
  bool isType( const char *type ) const;
  
  void setOffset( int nv );
  int getOffset() const;
  void setMaxLen( int nv );
  int getMaxLen() const;
  void setMaxDisplay( int nv );
  int getMaxDisplay() const;
  
  bool isParent( Window ) const;
  
  void setDisplayFocus( bool nv );
  bool getDisplayFocus() const;

  int getBG() const;
  void setBG( int v );

  void setShowArrowButtons( bool nv );
  bool getShowArrowButtons() const;
protected:
  static const char *type;
  
  int max_len, offset, maxdisplay;
  bool vertical;
  int old_offset;

  int data;
  int bar_dir;
  bool bar_pressed;
  void handleExpose( Window msgwin, int ex, int ey, int ew, int eh );

  bool displayFocus;

  typedef enum { BAR_IDLE, BAR_SCROLL, BAR_SCROLL_LEFT, BAR_SCROLL_RIGHT } barMode_t;
  barMode_t barMode;
  int grabs, scrollDelta;
  void checkValues();
  void msgAndCallback();

  void enableTimer();
  int _init_timer_wait;

  int m_bg;

  bool m_show_arrow_buttons;
};

#endif
