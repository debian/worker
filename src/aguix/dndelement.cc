/* dndelement.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dndelement.h"
#include "aguix.h"
#include "guielement.h"
#include "awindow.h"

DNDElement::DNDElement( AGUIX *parent )
{
  aguix=parent;
  Display *dsp=aguix->getDisplay();
  int scr=aguix->getScreen();
  Visual *vis=DefaultVisual(dsp,scr);
  unsigned long mask=CWEventMask|CWBackPixel|CWOverrideRedirect|CWSaveUnder;
  XSetWindowAttributes attr;
  attr.event_mask=ExposureMask|ButtonPressMask|ButtonReleaseMask|ButtonMotionMask|PointerMotionHintMask|KeyReleaseMask;
  attr.background_pixel=0;
  attr.override_redirect=true;
  attr.save_under=true;

  int xw,yw,xr,yr;
  Window root,child;
  unsigned int keys_button;
  XQueryPointer(dsp,RootWindow(dsp,scr),&root,&child,&xr,&yr,&xw,&yw,&keys_button);

  win=XCreateWindow(dsp,RootWindow(dsp,scr),xr,yr,1,1,0,aguix->getDepth(),InputOutput,vis,mask,&attr);
  XClearWindow(dsp,win);
  XFlush(dsp);

  delx=0;
  dely=0;

  created = false;
}

DNDElement::~DNDElement()
{
  close();
}

void DNDElement::handler( Message *msg )
{
  int tx, ty;
  if ( created == false ) {
    create();
  }
  if ( msg->type == MotionNotify ) {
    aguix->queryRootPointer( &tx, &ty );
    XMoveWindow( aguix->getDisplay(), win, delx + tx, dely + ty );
    aguix->Flush();
  } else if ( msg->type == Expose ) {
    if ( msg->window == win ) redraw();
  }
}

void DNDElement::redraw()
{
}

void DNDElement::close()
{
  if(win!=0) XDestroyWindow(aguix->getDisplay(),win);
  win=0;
}

void DNDElement::create()
{
  XMapRaised(aguix->getDisplay(),win);
  created = true;
}
