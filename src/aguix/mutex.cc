/* mutex.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2002-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "mutex.h"

MutEx::MutEx()
{
  pthread_mutex_init( &_mutex, NULL );
  _owner = 0;
  _owncount = 0;
}

MutEx::~MutEx()
{
  pthread_mutex_destroy( &_mutex );
}

void MutEx::lock()
{
    {
        std::unique_lock< std::mutex > lck( m_owner_data_mutex );

        if ( _owner == pthread_self() ) {
            _owncount++;
            return;
        }
    }

    pthread_mutex_lock( &_mutex );

    {
        std::unique_lock< std::mutex > lck( m_owner_data_mutex );

        _owner = pthread_self();
        _owncount = 1;
    }
}

void MutEx::unlock()
{
    std::unique_lock< std::mutex > lck( m_owner_data_mutex );

    if ( _owner == pthread_self() ) {
        _owncount--;

        if ( _owncount < 1 ) {
            _owner = 0;
            pthread_mutex_unlock( &_mutex );
        }
    }
}

bool MutEx::trylock()
{
    {
        std::unique_lock< std::mutex > lck( m_owner_data_mutex );

        if ( _owner == pthread_self() ) {
            _owncount++;
            return true;
        }
    }

    if ( pthread_mutex_trylock( &_mutex ) != 0 ) return false;

    {
        std::unique_lock< std::mutex > lck( m_owner_data_mutex );

        _owner = pthread_self();
        _owncount = 1;
    }

    return true;
}
