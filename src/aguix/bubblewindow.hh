/* bubblewindow.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BUBBLEWINDOW_HH
#define BUBBLEWINDOW_HH

#include "awindow.h"

class BubbleWindow : public AWindow {
public:
    BubbleWindow( AGUIX *parent,
                  int x,
                  int y,
                  int width,
                  int height,
                  std::string title,
                  const Widget *linked_widget );
    ~BubbleWindow();
    BubbleWindow( const BubbleWindow &other );
    BubbleWindow &operator=( const BubbleWindow &other );
    
    void show();
    int create();

    bool handleMessage( XEvent *e, Message *msg );

    bool ownedByLinkedWidget( Window w );
    void unsetLinkedWidget();

    void prepareHelpText( const std::string &m_bubble_help_text );
private:
    const Widget *m_linked_widget;
};

#endif
