/* lowlevelfunc.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef LOWLEVELFUNC_H
#define LOWLEVELFUNC_H

#include "aguixdefs.h"
#include "time.h"
#include <string>
#include "awidth.h"

void *_allocsafe(size_t size);
void waittime(unsigned long msec);
#define _freesafe(P) free(P)
char *dupstring(const char *str);
char *catstring(const char *str1,const char *str2);
char *shrinkstring( const char *str, int maxlen, AWidth &lencalc );
int MakeLong2NiceStr( loff_t size, std::string &buffer, bool do_nice = true );

double diffgtod( struct timeval *tv1, struct timeval *tv0 );
long ldiffgtod( struct timeval *tv1, struct timeval *tv0 );
long ldiffgtod_m( const struct timeval *tv1, const struct timeval *tv0 );

#ifdef DEBUG
# define debugmsg(P) printf( "[AGUIX]: %s\n", P )
#else
# define debugmsg(P) 
#endif

int AGUIX_getQuoteMode( const char *str );
char *AGUIX_catTrustedAndUnTrusted( const char *str1, const char *str2 );
char *AGUIX_prepareForSingleQuote( const char *str1 );
char *AGUIX_unquoteString( const char *str1 );
char *AGUIX_catQuotedAndUnQuoted( const char *str1, const char *str2 );
char *AGUIX_fixBackslashed( const char *str1 );

std::string AGUIX_catTrustedAndUnTrusted2( const std::string &str1,
                                           const std::string &str2 );
std::string AGUIX_catQuotedAndUnQuoted2( const std::string &str1,
                                         const std::string &str2 );
std::string AGUIX_prepareForSingleQuote2( const std::string str );

#endif /* LOWLEVELFUNC_H */

