/* compregex.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2004-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COMPREGEX_HH
#define COMPREGEX_HH

#include "aguixdefs.h"
#include <regex.h>
#include <string>
#include "avltree.hh"

class CompRegEx
{
public:
    CompRegEx();
    ~CompRegEx();
    CompRegEx( const CompRegEx &other );
    CompRegEx &operator=( const CompRegEx &other );
    
    bool match( const char *pattern,
                const char *str,
                bool ignore_case = false );
    bool match( const char *pattern,
                const char *str,
                bool ignore_case,
                int *start_offset,
                int *end_offset );

    static void setUseExtendedRegEx( bool nv );
private:
#ifdef HAVE_REGEX
    struct regExpComp_t {
        regex_t patternReg;
        char *pattern;
    };
    AVLList<std::string,regExpComp_t*> _compiled, _compiled_nocase;
    void freeCompiled();
    const struct regExpComp_t &getCompiled( const char *pattern, bool ignore_case );
    size_t maxsize;
    void freeLast( bool nocase );

    static bool s_use_extended_regex;
#endif
};

#endif
