/* faces.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FACES_HH
#define FACES_HH

#include "aguixdefs.h"
#include <string>
#include <map>
#include <list>
#include "aguixcolor.hh"

class Face
{
public:
    Face() : m_name( "" ),
             m_color( -1 ),
             m_parent( "" ) {}

    Face( const std::string &name,
          int color,
          const std::string &parent ) : m_name( name ),
                                        m_color( color ),
                                        m_parent( parent ) {}

    std::string getName() const
    {
        return m_name;
    }

    int getColor() const
    {
        return m_color;
    }

    void setColor( int col )
    {
        m_color = col;
    }

    std::string getParent() const
    {
        return m_parent;
    }
private:
    std::string m_name;
    int m_color;
    std::string m_parent;
};

class FaceDB
{
public:
    FaceDB()
    {
        initDefaultFaces();
    }

    void initDefaultFaces();

    int setFace( const Face &face );
    int getColor( const std::string &name ) const;
    AGUIXColor getAGUIXColor( const std::string &name ) const;

    void setColor( const std::string &name, int col );

    Face getFace( const std::string &name ) const;
    std::list< std::string > getListOfFaces() const;

    bool hasFace( const std::string &name ) const;
private:
    std::map< std::string, Face > m_faces;
};

#endif
