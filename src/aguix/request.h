/* request.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef AGUIX_REQUEST_H
#define AGUIX_REQUEST_H

#include "aguixdefs.h"
#include "textstorage.h"
#include <functional>
#include <string>

class Requester
{
public:
  Requester(class AGUIX *parent);
  Requester( class AGUIX *parent, class AWindow *twin );
  virtual ~Requester();
  Requester( const Requester &other );
  Requester &operator=( const Requester &other );

  typedef enum { REQUEST_NONE = 0,
		 REQUEST_CURSORLEFT = 1,
		 REQUEST_CURSORRIGHT = 2,
		 REQUEST_SELECTALL = 3,
		 REQUEST_CANCELWITHLEFT = 4 } request_flags_t;

    struct request_options {
        ssize_t select_characters = -1; ///< -1 means all, >= 0 selects number of character
    };

  int request(const char *title,const char *text,const char *buttons);
  int request(const char *title,const char *text,const char *buttons, request_flags_t flags);
  int request(const char *title,const char *text,const char *buttons, const AWindow *twin, request_flags_t flags);
  int string_request( const char *title, const char *lines, const char *default_str, const char *buttons,char **return_str);
  int string_request( const char *title, const char *lines, const char *default_str, const char *buttons,char **return_str, request_flags_t flags );
  int string_request( const char *title, const char *lines, const char *default_str, const char *buttons,char **return_str, const AWindow *twin, request_flags_t flags );
  int string_request( const char *title, const char *lines, const char *default_str, const char *buttons,char **return_str, const AWindow *twin, request_flags_t flags,
                      struct request_options &options );
  int string_request( const char *title, const char *lines, const char *default_str, const char *buttons,
                      char **return_str, const AWindow *twin, request_flags_t flags,
                      struct request_options &options,
                      const std::string &help_text,
                      const std::function< std::string ( const std::string &current_input_before_cursor,
                                                         const std::string &current_input_after_cursor ) > help_callback );
  int request_choose( const char *title, const char *text, const char *choose_text, bool &choose_var, const char *buttons );
  int request_choose( const char *title, const char *text, const char *choose_text, bool &choose_var, const char *buttons, request_flags_t flags );
  int request_choose( const char *title, const char *text, const char *choose_text, bool &choose_var, const char *buttons, const AWindow *twin, request_flags_t flags );

  int string_request_choose( const char *title,
                             const char *lines,
                             const char *default_str,
                             const char *choose_text,
                             bool &choose_var,
                             const char *buttons,
                             char **return_str );
  int string_request_choose( const char *title,
                             const char *lines,
                             const char *default_str,
                             const char *choose_text,
                             bool &choose_var,
                             const char *buttons,
                             char **return_str,
                             request_flags_t flags );
  int string_request_choose( const char *title,
                             const char *lines,
                             const char *default_str,
                             const char *choose_text,
                             bool &choose_var,
                             const char *buttons,
                             char **return_str,
                             const AWindow *twin,
                             request_flags_t flags );

  int request( const char *title, std::shared_ptr< TextStorage > ts, const char *buttons );
  int request( const char *title, std::shared_ptr< TextStorage > ts, const char *buttons, request_flags_t flags );
  int request( const char *title, std::shared_ptr< TextStorage > ts, const char *buttons, const AWindow *twin, request_flags_t flags );
private:
  class AGUIX *aguix;
  AWindow *transientawin;
};

#endif
