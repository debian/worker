/* dndtext.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dndtext.h"
#include "aguix.h"
#include "guielement.h"
#include "awindow.h"
#include "drawablecont.hh"

DNDText::DNDText( AGUIX *parent, const char *ttext, int tfg, int tbg, AGUIXFont *tfont ) : DNDElement( parent )
{
  this->text=dupstring(ttext);
  this->fg=tfg;
  this->bg=tbg;
  this->font=tfont;

  delx = 10;
  dely = 10;
}

DNDText::~DNDText()
{
  _freesafe(text);
}

void DNDText::redraw()
{
  aguix->ClearWin(win);

  DrawableCont dc( aguix, win );
  aguix->DrawText( dc, font, text, 0, 0, fg );
}

void DNDText::close()
{
  if(win!=0) XDestroyWindow(aguix->getDisplay(),win);
  win=0;
}

void DNDText::create()
{
  int w,h;
  Display *dsp=aguix->getDisplay();
  if(font!=NULL) h=font->getCharHeight();
  else h=aguix->getCharHeight();

  w = aguix->getTextWidth( text, font );
  w+=1;

  aguix->SetWindowBG(win,bg);
  XResizeWindow(dsp,win,w,h);

  int xw,yw,xr,yr;
  Window root,child;
  unsigned int keys_button;
  XQueryPointer(dsp,RootWindow(dsp,aguix->getScreen()),&root,&child,&xr,&yr,&xw,&yw,&keys_button);

  XMoveWindow(dsp,win,delx+xr,dely+yr);

  XMapRaised(dsp,win);
  redraw();
  created = true;
}
