/* aguixcolor.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef AGUIXCOLOR_HH
#define AGUIXCOLOR_HH

#include "aguixdefs.h"
#include <functional>

class AGUIXColor {
public:
    typedef enum { USER_COLOR, SYSTEM_COLOR, EXTRA_COLOR } color_type_t;
    
    AGUIXColor( int col, color_type_t type = USER_COLOR );
    bool operator<( const AGUIXColor &other ) const;
    
    int getColor() const;
    color_type_t getColorType() const;

private:
    int m_color;
    color_type_t m_color_type;
};

#endif
