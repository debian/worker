/* compregex.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2004-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "compregex.hh"
#include "lowlevelfunc.h"
#include <fnmatch.h>

#ifdef HAVE_REGEX
bool CompRegEx::s_use_extended_regex = false;
#endif

CompRegEx::CompRegEx()
{
#ifdef HAVE_REGEX
    maxsize = 1024;
#endif
}

CompRegEx::~CompRegEx()
{
#ifdef HAVE_REGEX
    freeCompiled();
#endif
}

#ifdef HAVE_REGEX

void CompRegEx::freeCompiled()
{
    while ( _compiled.size() > 0 ) {
        freeLast( false );
    }
    while ( _compiled_nocase.size() > 0 ) {
        freeLast( true );
    }
    _compiled.clear();
    _compiled_nocase.clear();
}

const struct CompRegEx::regExpComp_t &CompRegEx::getCompiled( const char *pattern, bool ignore_case )
{
    int erg;
    regExpComp_t *newre, **pe1;
    
    if ( pattern == NULL ) throw 1;
    
    if ( ignore_case == true ) {
        pe1 = _compiled_nocase.findToFront( pattern );
    } else {
        pe1 = _compiled.findToFront( pattern );
    }
    if ( pe1 != NULL ) {
        return **pe1;
    }
    
    newre = new regExpComp_t;
    erg = regcomp( &(newre->patternReg), pattern,
                   ( s_use_extended_regex ? REG_EXTENDED : 0 ) | ( ( ignore_case == true ) ? REG_ICASE : 0 ) );
    if ( erg != 0 ) {
        // error
        delete newre;
        throw 2;
    }
    newre->pattern = dupstring( pattern );
    
    if ( ignore_case == true ) {
        if ( _compiled_nocase.size() >= maxsize ) {
            freeLast( true );
        }
        _compiled_nocase.insert( pattern, newre );
    } else {
        if ( _compiled.size() >= maxsize ) {
            freeLast( false );
        }
        _compiled.insert( pattern, newre );
    }
    return *newre;
}

void CompRegEx::freeLast( bool nocase )
{
    regExpComp_t **pe1, *e1;
    
    if ( nocase == true ) {
        pe1 = _compiled_nocase.getLast();
    } else {
        pe1 = _compiled.getLast();
    }
    if ( pe1 != NULL ) {
        e1 = *pe1;
        regfree( &(e1->patternReg) );
        _freesafe( e1->pattern );
        delete e1;
        
        if ( nocase == true ) {
            _compiled_nocase.removeLast();
        } else {
            _compiled.removeLast();
        }
    }
}

#endif

bool CompRegEx::match( const char *pattern, const char *str, bool ignore_case )
{
    return match( pattern, str, ignore_case, NULL, NULL );
}

bool CompRegEx::match( const char *pattern, const char *str, bool ignore_case,
                       int *start_offset, int *end_offset )
{
    bool matched = false;
#ifdef HAVE_REGEX
    int erg;
#endif
    
    if ( pattern == NULL ) return false;
    if ( strlen( pattern ) < 1 ) return false;
    if ( str == NULL ) return false;
    if ( strlen( str ) < 1 ) return false;
    
#ifdef HAVE_REGEX
    try {
        const struct regExpComp_t &x = getCompiled( pattern, ignore_case );
        regmatch_t offset;

        erg = regexec( &(x.patternReg), str, 1, &offset, 0 );
        if ( erg == 0 ) {
            matched = true;
            if ( start_offset != NULL ) {
                *start_offset = offset.rm_so;
            }
            if ( end_offset != NULL ) {
                *end_offset = offset.rm_eo;
            }
        }
    } catch( int ) {
    }
#else
    if ( fnmatch( pattern, str, 0 ) == 0 ) {
        if ( start_offset != NULL ) {
            *start_offset = -1;
        }
        if ( end_offset != NULL ) {
            *end_offset = -1;
        }
        matched = true;
    }
#endif
    return matched;
}

void CompRegEx::setUseExtendedRegEx( bool nv )
{
#ifdef HAVE_REGEX
    s_use_extended_regex = nv;
#endif
}
 
