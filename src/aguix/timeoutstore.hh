/* timeoutstore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TIMEOUTSTORE_HH
#define TIMEOUTSTORE_HH

#include "aguixdefs.h"
#include <map>
#include "mutex.h"

class TimeoutStore
{
public:
    TimeoutStore();
    ~TimeoutStore();
    TimeoutStore( const TimeoutStore &other );
    TimeoutStore &operator=( const TimeoutStore &other );

    void addTimeout( loff_t timeout_ms );
    void clearTimeout( loff_t timeout_ms );
    loff_t getNextTimeout();
private:
    struct timeval m_time_reference;
    loff_t m_timeout_gcd;
    std::map< loff_t, int > m_timeout_store;
    MutEx m_lock;

    loff_t gcd( loff_t v1, loff_t v2 );
    void update();
};

#endif
