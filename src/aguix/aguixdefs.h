/* aguixdefs.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef AGUIXDEFS_H
#define AGUIXDEFS_H

#ifndef CONFIG_H_INCLUDED
#include "aguixconfig.h"
#define CONFIG_H_INCLUDED
#endif // CONFIG_H_INCLUDED

#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <errno.h>
#include <ctype.h>

//enum {PISNOTHING,PISBUTTON,PISPATH,PISHOTKEY,PISNUMBER,PISFILETYPE};

#define KEYSTATEMASK(x) (x&(ControlMask|Mod1Mask|ShiftMask))

/* Workaround for missing nanosleep prototype
 * from:
 * http://www.opensource.apple.com/bugs/X/Libraries/2753843.html
 */
/* This is no longer needed for newer Mac OS X versions (>=10.4?)
 * Uncomment the line if you want to compile it on older versions
 */
#ifdef __APPLE__ // missing - why?
//extern "C" void nanosleep(struct timespec *, struct timespec *);
#endif

#if defined( HAVE_SIGACTION ) && defined( HAVE_NANOSLEEP )
#define USE_AGUIXTIMER
#endif

#define a_min(a,b) (((a)<(b))?(a):(b))
#define a_max(a,b) (((a)>(b))?(a):(b))

/* define for the needed bytes to display a type x as decimal number
   one byte needs 3 bytes, so sizeof(x) bytes needs sizeof(x)*3 bytes
   +1 for a sign
   +1 for null byte */
#define A_BYTESFORNUMBER(x) ( sizeof( x ) * 3 + 1 + 1 )

#endif
