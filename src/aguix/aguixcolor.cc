/* aguixcolor.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "aguixcolor.hh"

AGUIXColor::AGUIXColor( int col, color_type_t type ) : m_color( col ),
                                                       m_color_type( type )
{
}

int AGUIXColor::getColor() const
{
    return m_color;
}

AGUIXColor::color_type_t AGUIXColor::getColorType() const
{
    return m_color_type;
}

bool AGUIXColor::operator<( const AGUIXColor &other ) const
{
    if ( getColorType() == AGUIXColor::SYSTEM_COLOR &&
         other.getColorType() != AGUIXColor::SYSTEM_COLOR ) return true;
    if ( getColorType() == AGUIXColor::USER_COLOR &&
         other.getColorType() == AGUIXColor::SYSTEM_COLOR ) return false;
    if ( getColorType() == AGUIXColor::USER_COLOR &&
         other.getColorType() == AGUIXColor::EXTRA_COLOR ) return true;
    if ( getColorType() == AGUIXColor::EXTRA_COLOR &&
         other.getColorType() != AGUIXColor::EXTRA_COLOR ) return false;
    return getColor() < other.getColor();
}
