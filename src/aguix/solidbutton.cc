/* solidbutton.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "solidbutton.h"
#include "lowlevelfunc.h"
#include "awindow.h"
#include "drawablecont.hh"

const char *SolidButton::type="SolidButton";

SolidButton::~SolidButton()
{
  if(text!=NULL) _freesafe(text);
}

SolidButton::SolidButton(AGUIX *taguix,int tx,int ty,
                         const char *ttext,int tfg,int tbg,bool tstate):GUIElement(taguix)
{
    init( taguix, tx, ty,
          ttext, tfg, tbg, tstate );
}

SolidButton::SolidButton(AGUIX *taguix,int tx,int ty,int width,
                         const char *ttext,int tfg,int tbg,bool tstate):GUIElement(taguix)
{
    init( taguix, tx, ty, width,
          ttext, tfg, tbg, tstate );
}

SolidButton::SolidButton(AGUIX *taguix,int tx,int ty,int width,int height,
                         const char *ttext,int tfg,int tbg,bool tstate):GUIElement(taguix)
{
    init( taguix, tx, ty, width, height,
          ttext, tfg, tbg, tstate );
}

void SolidButton::init( AGUIX *taguix, int tx, int ty,
                        int twidth, int theight,
                        const char *ttext,
                        int tfg, int tbg,
                        bool tstate )
{
    _x = tx;
    _y = ty;
    if ( twidth > 0 ) _w = twidth;
    if ( theight > 0 ) _h = theight;
    state = tstate;
    text = dupstring( ttext );
    fg = tfg;
    bg = tbg;
    font = NULL;

    applyFaces();
}

void SolidButton::init( AGUIX *taguix, int tx, int ty,
                        int twidth,
                        const char *ttext,
                        int tfg, int tbg,
                        bool tstate )
{
    int bw;

    bw = 2;

    int height = taguix->getCharHeight() + 2 * bw;

    init( taguix, tx, ty, twidth, height, ttext, tfg, tbg, tstate );
}

void SolidButton::init( AGUIX *taguix, int tx, int ty,
                        const char *ttext,
                        int tfg, int tbg,
                        bool tstate )
{
    int width = taguix->getTextWidth( ttext ) + taguix->getTextWidth( "  " );

    init( taguix, tx, ty, width, ttext, tfg, tbg, tstate );
}

SolidButton::SolidButton( AGUIX *taguix, int tx, int ty,
                          const char *ttext, bool tstate )
    : GUIElement( taguix )
{
    init( taguix, tx, ty, ttext, -1, -1, tstate );
}

SolidButton::SolidButton( AGUIX *taguix, int tx, int ty, int twidth,
                          const char *ttext, bool tstate )
    : GUIElement( taguix )
{
    init( taguix, tx, ty, twidth, ttext, -1, -1, tstate );
}

SolidButton::SolidButton( AGUIX *taguix, int tx, int ty, int twidth, int theight,
                          const char *ttext, bool tstate)
    : GUIElement( taguix )
{
    init( taguix, tx, ty, twidth, theight, ttext, -1, -1, tstate );
}

SolidButton::SolidButton( AGUIX *taguix, int tx, int ty, int width,
                          const char *ttext,
                          const std::string &fg_name,
                          const std::string &bg_name,
                          bool tstate ) : GUIElement( taguix )
{
    int tfg = _aguix->getFaces().getColor( fg_name );
    int tbg = _aguix->getFaces().getColor( bg_name );

    init( taguix, tx, ty, width, ttext, tfg, tbg, tstate );
}

SolidButton::SolidButton( AGUIX *taguix, int tx, int ty,
                          const char *ttext,
                          const std::string &fg_name,
                          const std::string &bg_name,
                          bool tstate ) : GUIElement( taguix )
{
    int tfg = _aguix->getFaces().getColor( fg_name );
    int tbg = _aguix->getFaces().getColor( bg_name );

    init( taguix, tx, ty, ttext, tfg, tbg, tstate );
}

const char *SolidButton::getText() const
{
  return text;
}

void SolidButton::setText(const char *new_text)
{
  if(text!=NULL) _freesafe(text);
  text=dupstring(new_text);
  redraw();
}

void SolidButton::setFG(int tfg)
{
    if ( tfg < _aguix->getNumberOfColorsForType( AGUIXColor::USER_COLOR ) && tfg >= 0 ) {
        this->fg = tfg;
        redraw();
    }
}

int SolidButton::getFG() const
{
  return fg;
}

void SolidButton::setBG(int tbg)
{
    if ( tbg < _aguix->getNumberOfColorsForType( AGUIXColor::USER_COLOR ) && tbg >= 0 ) {
        this->bg = tbg;
        redraw();
    }
}

int SolidButton::getBG() const
{
  return bg;
}

bool SolidButton::getState() const
{
  return state;
}

void SolidButton::setState(bool tstate)
{
  this->state=tstate;
  redraw();
}

void SolidButton::redraw()
{
  int len, strwidth;
  int bw;

  if ( isCreated() == false ) return;
  if(win==0) return;

  prepareBG();
  _aguix->ClearWin(win);

  _aguix->drawBorder( win, state, 0, 0, _w, _h, 0 );
  
  int ch;
  if(font==NULL) {
    ch=_aguix->getCharHeight();
  } else {
    ch=font->getCharHeight();
  }
  int dy=_h/2-ch/2;

  char *tstr;
  tstr=dupstring(text);

  bw = 2;

  len = _aguix->getStrlen4Width( tstr, _w - 2 * bw, &strwidth, font );
  if ( len >= 0 ) {
    tstr[len] = '\0';
    int dx = _w / 2 - strwidth / 2;

    DrawableCont dc( _aguix, win );
    _aguix->DrawText( dc, font, tstr, dx, dy, fg );
  }
  _freesafe(tstr);
  _aguix->Flush();
}

void SolidButton::flush()
{
}

bool SolidButton::handleMessage(XEvent *E,Message *msg)
{
  if ( isCreated() == false ) return false;
  if((msg->type==Expose)&&(msg->window==win)) {
    redraw();
  }
  return false;
}

int SolidButton::setFont( const char *fontname )
{
  font=_aguix->getFont(fontname);
  if(font==NULL) return -1;
  return 0;
}

const char *SolidButton::getType() const
{
  return type;
}

bool SolidButton::isType(const char *qtype) const
{
  if(strcmp(type,qtype)==0) return true;
  return false;
}

void SolidButton::prepareBG( bool force )
{
  if ( isCreated() == false ) return;
  if ( win == 0 ) return;

  _aguix->SetWindowBG( win, bg );
}

void SolidButton::applyFaces()
{
    if ( fg < 0 ) {
        fg = _aguix->getFaces().getColor( "button-fg" );
    }

    if ( bg < 0 ) {
        bg = _aguix->getFaces().getColor( "button-bg" );
    }
}
