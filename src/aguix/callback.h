/* callback.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2005 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: callback.h,v 1.1 2005/04/25 19:54:29 ralf Exp $ */

#ifndef CALLBACK_H
#define CALLBACK_H

#include "aguixdefs.h"    
#include "message.h"

class Widget;

class CallBack
{
 public:
  CallBack() {}
  virtual ~CallBack() = 0;

  /*
   * plain callback with no special infos
   */
  virtual void run_plain( Widget * );
  
  /*
   * Callback with int value
   * falls back to run( Widget *) if not overwritten
   */
  virtual void run_int( Widget *, int value );

  /*
   * Callback with message describing the event in detail
   * falls back to run( Widget *) if not overwritten
   */
  virtual void run( Widget *, const AGMessage &msg );
};

#endif

