/* pan_scrolling.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PAN_SCROLLING_HH
#define PAN_SCROLLING_HH

struct pan_info {
    enum pan_mode {
        PAN_INACTIVE,
        PAN_PREPARE,
        PAN_HORIZONTAL,
        PAN_VERTICAL
    } m_pan_mode = PAN_INACTIVE;

    int get_scroll_offset( enum pan_mode pan_mode,
                           int mouse_x,
                           int mouse_y );
    void init_mouse_positions( int pos, int full );
    void init_scroll_positions( int current_offset,
                                int last_offset,
                                int inner_scroll );

    int m_pan_base_mx = 0;
    int m_pan_base_my = 0;
    int m_pan_base[2][5] = { { 0 } };
};

#endif /* PAN_SCROLLING_HH */
