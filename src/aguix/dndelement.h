/* dndelement.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DNDELEMENT_H
#define DNDELEMENT_H

#include "aguixdefs.h"
#include "message.h"

class AGUIX;
class GUIElement;

class DNDElement
{
public:
  DNDElement( AGUIX * );
  virtual ~DNDElement();
  DNDElement( const DNDElement &other );
  DNDElement &operator=( const DNDElement &other );

  virtual void handler( Message *msg );
  virtual void redraw();
  virtual void create();
protected:
  virtual void close();
  AGUIX *aguix;
  Window win;
  int delx,dely;
  bool created;
};

#endif
