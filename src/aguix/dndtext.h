/* dndtext.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2001-2007 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DNDTEXT_H
#define DNDTEXT_H

#include "aguixdefs.h"
#include "dndelement.h"

class AGUIXFont;

class DNDText:public DNDElement
{
public:
  DNDText( AGUIX *, const char*, int, int, AGUIXFont* );
  virtual ~DNDText();
  DNDText( const DNDText &other );
  DNDText &operator=( const DNDText &other );

  virtual void redraw();
  virtual void create();
protected:
  virtual void close();
  AGUIXFont *font;
  char *text;
  int fg,bg;
};

#endif
