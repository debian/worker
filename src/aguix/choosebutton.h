/* choosebutton.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHOOSEBUTTON_H
#define CHOOSEBUTTON_H

#include "aguixdefs.h"
#include "guielement.h"

class AGUIXFont;

enum {LABEL_TOP,LABEL_LEFT,LABEL_BOTTOM,LABEL_RIGHT};

class ChooseButton:public GUIElement {
public:
  ChooseButton( AGUIX *aguix, int x, int y, int width, int height, bool state,
                const char *label, int labelpos, int data );
  ChooseButton( AGUIX *aguix, int x, int y, bool state,
                const char *label, int labelpos, int data );

  virtual ~ChooseButton();
  ChooseButton( const ChooseButton &other );
  ChooseButton &operator=( const ChooseButton &other );

  const char *getLabel() const;
  void setLabel(const char *);
  virtual void resize(int w,int h); // Notwendig, da Gr��e des ChooseButtons nicht die Gr��e
                                    // des Fensters ist
  virtual void move(int x,int y);
  void setLabelPos(int);
  int getLabelPos() const;
  void setLabelColor(int);
  int getLabelColor() const;
  int getData() const;
  void setData(int);
  void setState(bool);
  bool getState() const;
  virtual void redraw();
  virtual void flush();
  bool isInside(int x,int y) const;
  virtual bool handleMessage(XEvent *E,Message *msg);
  int setFont( const char * );
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
  virtual bool isParent(Window) const;
  virtual int getX() const;
  virtual int getY() const;
  virtual int getWidth() const;
  virtual int getHeight() const;
  virtual void toBack();
  virtual void toFront();
  virtual void hide();
  virtual void show();
protected:
  virtual void doCreateStuff();
  virtual void doDestroyStuff();
private:
  char *label;
  int labelpos;
  int labelcolor;
  int data;
  bool state;
  bool instate;
  bool laststate;
  bool cross;
  static const char *type;
  int lx,ly,lw,lh;
  void calcLabelGeometry();
  void updateWin();
  void calcRealPos(int,int);
  int getLabelWidth() const;
  int getLabelHeight() const;
  Window lwin;
  void lredraw();
  void cbredraw();
  AGUIXFont *font;

  void prepareBG( bool force = false );
};

#endif
