/* pan_scrolling.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "pan_scrolling.hh"

const static double s_pan_prepare_deadzone_percent = 0.05;

int pan_info::get_scroll_offset( enum pan_mode pan_mode,
                                 int mouse_x, int mouse_y )
{
    int p;
    if ( pan_mode == PAN_VERTICAL ) {
        p = mouse_y;
    } else if ( m_pan_mode == PAN_HORIZONTAL ) {
        p = mouse_x;
    } else {
        return -1;
    }

    for ( int i = 1; i < 5; i++ ) {
        if ( i == 4 || p <= m_pan_base[0][i] ) {
            double t;
            if ( m_pan_base[0][i] - m_pan_base[0][i-1] > 0 ) {
                t = p - m_pan_base[0][i-1];
                t /= m_pan_base[0][i] - m_pan_base[0][i-1];

                t *= m_pan_base[1][i] - m_pan_base[1][i-1];
                t += m_pan_base[1][i-1];
            } else {
                t = m_pan_base[1][i-1];
            }

            return t;
        }
    }

    return -1;
}

void pan_info::init_mouse_positions( int pos, int full )
{
    m_pan_base[0][0] = 0 + (double)full * s_pan_prepare_deadzone_percent;
    m_pan_base[0][2] = pos;
    m_pan_base[0][4] = (double)full * ( 1.0 - 1 * s_pan_prepare_deadzone_percent );

    if ( m_pan_base[0][0] > m_pan_base[0][2] ) m_pan_base[0][0] = m_pan_base[0][2];
    if ( m_pan_base[0][4] < m_pan_base[0][2] ) m_pan_base[0][4] = m_pan_base[0][2];

    m_pan_base[0][1] = m_pan_base[0][0] + ( m_pan_base[0][2] - m_pan_base[0][0] ) * 4 / 5;
    m_pan_base[0][3] = m_pan_base[0][2] + ( m_pan_base[0][4] - m_pan_base[0][2] ) * 1 / 5;
}

void pan_info::init_scroll_positions( int current_offset,
                                      int last_offset,
                                      int inner_scroll )
{
    m_pan_base[1][0] = 0;
    m_pan_base[1][2] = current_offset;
    m_pan_base[1][4] = last_offset;
    if ( m_pan_base[1][4] < m_pan_base[1][2] ) m_pan_base[1][4] = m_pan_base[1][2];

    m_pan_base[1][1] = m_pan_base[1][2] - inner_scroll;
    int mid = ( m_pan_base[1][0] + m_pan_base[1][2] ) / 2;
    if ( m_pan_base[1][1] < mid ) m_pan_base[1][1] = mid;
    if ( m_pan_base[1][1] < 0 ) m_pan_base[1][1] = 0;

    m_pan_base[1][3] = m_pan_base[1][2] + inner_scroll;

    mid = ( m_pan_base[1][2] + m_pan_base[1][4] ) / 2;
    if ( m_pan_base[1][3] > mid ) m_pan_base[1][3] = mid;
    if ( m_pan_base[1][3] > m_pan_base[1][4] ) m_pan_base[1][3] = m_pan_base[1][4];
}
