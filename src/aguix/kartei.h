/* kartei.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2004-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef KARTEI_H
#define KARTEI_H

#include "awindow.h"
#include <vector>

class Kartei : public AWindow {
public:
  Kartei( AGUIX *parent,
          int x,
          int y,
          int width,
          int height,
          std::string title );
  ~Kartei();
  Kartei( const Kartei &other );
  Kartei &operator=( const Kartei &other );
  void setOption( AWindow *win, unsigned int option, const char *optionname );
  void updateOption( unsigned int option, const char *optionname );
  void updateOption( unsigned int option, const std::string &optionname );
  void maximize();
  static void optionChangeCallback( Kartei *k1, unsigned int option );
  void optionChange( unsigned int option );
  void boxRedraw();
  bool handleMessage(XEvent *,Message *msg);
  int getCurOption() const;
protected:
  std::vector<AWindow*> options;
  class AContainer *cont;
  class KarteiButton *optionkb;
  unsigned int curOption;

  void showCurOption();
  void doCreateStuff();
  void doDestroyStuff();
};

#endif
