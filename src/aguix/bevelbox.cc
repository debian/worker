/* bevelbox.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "bevelbox.h"
#include "lowlevelfunc.h"
#include "awindow.h"

const char *BevelBox::type="BevelBox";

BevelBox::~BevelBox()
{
}

BevelBox::BevelBox(AGUIX *taguix):GUIElement(taguix)
{
  this->state=0;
}

BevelBox::BevelBox(AGUIX *taguix,int tx,int ty,int width,int height,
                   int tstate):GUIElement(taguix)
{
  _x=tx;
  _y=ty;
  if ( width > 0 ) _w = width;
  if ( height > 0 ) _h = height;
  this->state=tstate;
}

int BevelBox::getState() const
{
  return state;
}

void BevelBox::setState(int tstate)
{
  if(tstate==1) this->state=tstate;
  else this->state=0;

  redraw();
}

void BevelBox::redraw()
{
  if ( isCreated() == false ) return;

  prepareBG();

  _aguix->ClearWin(win);
  if(state==0) {
      _aguix->setFG( _aguix->getFaceCol_3d_bright() );
  } else {
    _aguix->setFG( _aguix->getFaceCol_3d_dark() );
  }
  _aguix->DrawLine(win,0,_h-1,0,0);
  _aguix->DrawLine(win,0,0,_w-1,0);
  if(state==0) {
    _aguix->setFG( _aguix->getFaceCol_3d_dark() );
  } else {
    _aguix->setFG( _aguix->getFaceCol_3d_bright() );
  }
  _aguix->DrawLine(win,0,_h-1,_w-1,_h-1);
  _aguix->DrawLine(win,_w-1,_h-1,_w-1,1);
}

void BevelBox::flush()
{
}

bool BevelBox::handleMessage(XEvent *E,Message *msg)
{
  if ( isCreated() == true ) {
    if((msg->type==Expose)&&(msg->window==win)) {
      redraw();
    }
  }
  return false;
}

const char *BevelBox::getType() const
{
  return type;
}

bool BevelBox::isType(const char *qtype) const
{
  if(strcmp(type,qtype)==0) return true;
  return false;
}

void BevelBox::prepareBG( bool force )
{
  if ( isCreated() == false ) return;
  if ( win == 0 ) return;

  _aguix->SetWindowBG( win, _parent->getBG() );
}
