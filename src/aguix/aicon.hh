/* aicon.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef AICON_HH
#define AICON_HH

#include "aguixdefs.h"

class AGUIX;

class AIcon {
public:
    AIcon( AGUIX *aguix,
           Drawable d,
           const int *pixels,
           size_t pixels_length );
    ~AIcon();

    void update( int fg, int bg, bool force );
    void update( Drawable d, int fg, int bg, bool force );
    void draw( Drawable d,
               int x, int y );
    void setScale( size_t factor );
    Drawable drawable() const;

    size_t w() const;
    size_t h() const;
private:
    void cleanPixmapCache();

    AGUIX *m_aguix;
    Drawable m_d;
    size_t m_w;
    size_t m_h;
    std::vector< int > m_pixels;
    std::map< std::pair< int, int >, Pixmap > m_pixmap_cache;
    size_t m_scale_factor;

    int m_aguix_color_instance;
    int m_current_fg;
    int m_current_bg;
};

#endif /* AICON_HH */
