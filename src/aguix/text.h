/* text.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TEXT_H
#define TEXT_H

#include "aguixdefs.h"
#include "guielement.h"
#include "textshrinker.hh"
#include <string>
#include "refcount.hh"

class Text:public GUIElement {
public:
  enum center_mode {
      CENTER_NONE = 0,
      CENTER_VERTICALLY = 1 << 0
  };

  Text( class AGUIX *aguix, int x, int y, const char *text, int fg, int bg, enum center_mode = CENTER_NONE );
  Text( class AGUIX *aguix, int x, int y, const char *text, enum center_mode = CENTER_NONE );

  virtual ~Text();
  Text( const Text &other );
  Text &operator=( const Text &other );
  const char *getText() const;
  void setText(const char *);
  int getColor() const;
  void setColor(int color);
  virtual void redraw();
  virtual void flush();
  virtual bool handleMessage(XEvent *E,Message *msg);
  int setFont( const char * );
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
  void setBG( int color );
  int getBG() const;
  void setFG( int color );
  int getFG() const;

  void setTextShrinker( RefCount<TextShrinker> shrinker );
private:
  char *text;
  int color, bg;
  bool bgset, ownbg;
  class AGUIXFont *font;
  static const char *type;
  RefCount<TextShrinker> m_shrinker;
  struct {
      std::string current_text;
      int current_width;
  } m_shrinker_buf;

    enum center_mode m_center_mode = CENTER_NONE;
};

#endif
