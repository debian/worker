/* bevelbox.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2001-2005 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: bevelbox.h,v 1.8 2005/10/05 19:37:23 ralf Exp $ */

#ifndef BEVELBOX_H
#define BEVELBOX_H

#include "aguixdefs.h"
#include "guielement.h"

class AGUIX;

class BevelBox:public GUIElement {
public:
  BevelBox(AGUIX *aguix);
  BevelBox(AGUIX *aguix,int x,int y,int width,int height,
           int state);
  virtual ~BevelBox();
  BevelBox( const BevelBox &other );
  BevelBox &operator=( const BevelBox &other );

  int getState() const;
  void setState(int);
  virtual void redraw();
  virtual void flush();
  virtual bool handleMessage(XEvent *E,Message *msg);
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
private:
  int state;
  static const char *type;

  void prepareBG( bool force = false );
};

#endif
