/* guielement.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GUIELEMENT_H
#define GUIELEMENT_H

#include "aguixdefs.h"
#include "message.h"
#include "widget.h"
#include "callback.h"
#include <memory>
#include <functional>

class AGUIX;
class AWindow;
class BubbleWindow;

class GUIElement : public Widget {
public:
  GUIElement(AGUIX *aguix);
  virtual ~GUIElement();
  GUIElement( const GUIElement &other );
  GUIElement &operator=( const GUIElement &other );

  void move( int nx, int ny );
  void resize( int nw, int nh );
  virtual void redraw();
  virtual void flush();
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
  virtual void getPos(int*,int*) const;
  virtual void getSize(int*,int*) const;
  virtual void toBack();
  virtual void toFront();
  bool hasWin(Window win) const;
  Window getWindow() const;
  bool isParent(Window) const;
  virtual void paste( const unsigned char *content );
  virtual void cancelpaste();
  virtual void cancelcut();
  virtual void takeFocus();
  virtual void hide();
  virtual void show();
  bool isVisible() const;
  bool handleMessage(XEvent *,Message *msg);

  void connect( CallBack *new_cb );
  void setSendMessage( bool nv );
  bool getSendMessage() const;

  virtual void setBubbleHelpText( const std::string &help_text );
  virtual void setBubbleHelpCallback( std::function< std::string( int data ) > cb, int data );
  virtual void openBubbleHelp();
protected:
  Window win;
  static const char *type;
  bool sendMsg;

  void doCreateStuff();
  void doDestroyStuff();
  bool visible;

  int msgAndCB( std::unique_ptr<AGMessage> msg );
  int callback( int val );
 private:
  CallBack *cb;
  std::string m_bubble_help_text;
  std::weak_ptr< BubbleWindow > m_bubble_help_window;

    std::function< std::string( int data ) > m_bubble_help_cb;
    int m_bubble_help_cb_int_data;
};

#endif

