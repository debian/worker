/* faces.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "faces.hh"

void FaceDB::initDefaultFaces()
{
    m_faces.clear();

    setFace( Face( "default-fg", 1, "" ) );
    setFace( Face( "default-bg", 0, "" ) );

    // these 3 are special colors which are derived from default-fg/bg
    // unless explicitely overwritten
    setFace( Face( "fg-bg-mix66", -1, "default-fg" ) );
    setFace( Face( "bg-90", -1, "default-bg" ) );
    setFace( Face( "bg-30", -1, "default-bg" ) );

    setFace( Face( "3d-bright", 2, "" ) );
    setFace( Face( "3d-dark", 1, "" ) );

    setFace( Face( "button-fg", -1, "default-fg" ) );
    setFace( Face( "button-bg", -1, "default-bg" ) );
    setFace( Face( "button-special1-fg", 2, "button-fg" ) );
    setFace( Face( "button-special1-bg", 3, "button-bg" ) );
    setFace( Face( "button-alt-fg", -1, "button-fg" ) );
    setFace( Face( "button-alt-bg", -1, "button-bg" ) );

    //not used for the moment
    //setFace( Face( "button-3d-bright", -1, "3d-bright" ) );
    //setFace( Face( "button-3d-dark", -1, "3d-dark" ) );

    setFace( Face( "popup-edit-field-bg", 2, "" ) );
    setFace( Face( "popup-highlight-fg", 2, "" ) );
    setFace( Face( "popup-bg", -1, "default-bg" ) );
    setFace( Face( "popup-fg", -1, "default-fg" ) );
    setFace( Face( "popup-submenu-arrow", -1, "default-fg" ) );

    setFace( Face( "stringgadget-selection-bg", 1, "" ) );
    setFace( Face( "stringgadget-selection-fg", 7, "" ) );
    setFace( Face( "stringgadget-active-bg", 7, "" ) );
    setFace( Face( "stringgadget-active-fg", 1, "" ) );
    setFace( Face( "stringgadget-normal-bg", -1, "button-bg" ) );
    setFace( Face( "stringgadget-normal-fg", -1, "button-fg" ) );

    setFace( Face( "textview-fg", -1, "default-fg" ) );
    setFace( Face( "textview-bg", -1, "default-bg" ) );
    setFace( Face( "textview-selection-fg", 2, "" ) );
    setFace( Face( "textview-selection-bg", 1, "" ) );
    setFace( Face( "textview-highlight-fg", 1, "" ) );
    setFace( Face( "textview-highlight-bg", 2, "" ) );

    setFace( Face( "listview-normal-fg", -1, "default-fg" ) );
    setFace( Face( "listview-normal-bg", -1, "default-bg" ) );
    setFace( Face( "listview-select-fg", 2, "" ) );
    setFace( Face( "listview-select-bg", 1, "" ) );
    setFace( Face( "listview-active-fg", 1, "" ) );
    setFace( Face( "listview-active-bg", 7, "" ) );
    setFace( Face( "listview-selact-fg", 2, "" ) );
    setFace( Face( "listview-selact-bg", 7, "" ) );
    setFace( Face( "listview-sort-indicator", -1, "fg-bg-mix66" ) );

    setFace( Face( "slider-arrow", -1, "button-fg" ) );
    setFace( Face( "slider-bg", -1, "button-bg" ) );
    setFace( Face( "slider-bg2", -1, "bg-90" ) );
    setFace( Face( "slider-selection", -1, "bg-30" ) );

    setFace( Face( "tab-active-fg", -1, "button-fg" ) );
    setFace( Face( "tab-active-bg", -1, "button-bg" ) );
    setFace( Face( "tab-inactive-fg", -1, "tab-active-fg" ) );
    setFace( Face( "tab-inactive-bg", -1, "bg-90" ) );

    setFace( Face( "statusbar-fg", 2, "" ) );
    setFace( Face( "statusbar-bg", 3, "" ) );

    setFace( Face( "lvb-active-fg", 2, "" ) );
    setFace( Face( "lvb-active-bg", 4, "" ) );

    setFace( Face( "lvb-inactive-fg", 1, "" ) );
    setFace( Face( "lvb-inactive-bg", 0, "" ) );

    setFace( Face( "dirview-dir-select-fg", 2, "" ) );
    setFace( Face( "dirview-dir-select-bg", 3, "" ) );
    setFace( Face( "dirview-dir-normal-fg", 3, "" ) );
    setFace( Face( "dirview-dir-normal-bg", -1, "listview-normal-bg" ) );
    setFace( Face( "dirview-file-select-fg", -1, "listview-select-fg" ) );
    setFace( Face( "dirview-file-select-bg", -1, "listview-select-bg" ) );
    setFace( Face( "dirview-file-normal-fg", -1, "listview-normal-fg" ) );
    setFace( Face( "dirview-file-normal-bg", -1, "listview-normal-bg" ) );

    setFace( Face( "dirview-dir-selact-fg", -1, "listview-selact-fg" ) );
    setFace( Face( "dirview-dir-selact-bg", -1, "listview-selact-bg" ) );
    setFace( Face( "dirview-dir-active-fg", -1, "listview-active-fg" ) );
    setFace( Face( "dirview-dir-active-bg", -1, "listview-active-bg" ) );
    setFace( Face( "dirview-file-selact-fg", -1, "listview-selact-fg" ) );
    setFace( Face( "dirview-file-selact-bg", -1, "listview-selact-bg" ) );
    setFace( Face( "dirview-file-active-fg", -1, "listview-active-fg" ) );
    setFace( Face( "dirview-file-active-bg", -1, "listview-active-bg" ) );

    setFace( Face( "dirview-bg", 0, "" ) );

    setFace( Face( "clockbar-fg", 2, "" ) );
    setFace( Face( "clockbar-bg", 6, "" ) );

    setFace( Face( "request-fg", 1, "" ) );
    setFace( Face( "request-bg", 0, "" ) );

    setFace( Face( "dirview-header-fg", 1, "" ) );
    setFace( Face( "dirview-header-bg", 0, "" ) );
}

int FaceDB::setFace( const Face &face )
{
    if ( face.getName().empty() ) return 1;

    if ( ! face.getParent().empty() &&
         m_faces.count( face.getParent() ) == 0 ) {
        // parent does not exists
        return 1;
    }

    if ( face.getParent().empty() &&
         face.getColor() < 0 ) {
        // no parent but no color set
        return 1;
    }

    m_faces[ face.getName() ] = face;

    return 0;
}

int FaceDB::getColor( const std::string &name ) const
{
    if ( m_faces.count( name ) == 0 ) return -1;

    if ( m_faces.at( name ).getColor() >= 0 ) {
        return m_faces.at( name ).getColor();
    }

    return getColor( m_faces.at( name ).getParent() );
}

AGUIXColor FaceDB::getAGUIXColor( const std::string &name ) const
{
    if ( m_faces.count( name ) == 0 ) return -1;

    if ( m_faces.at( name ).getColor() >= 0 ) {
        return m_faces.at( name ).getColor();
    }

    if ( name == "fg-bg-mix66" ) {
        return AGUIXColor( 0, AGUIXColor::SYSTEM_COLOR );
    } else if ( name == "bg-90" ) {
        return AGUIXColor( 1, AGUIXColor::SYSTEM_COLOR );
    } else if ( name == "bg-30" ) {
        return AGUIXColor( 2, AGUIXColor::SYSTEM_COLOR );
    }

    return getAGUIXColor( m_faces.at( name ).getParent() );
}

Face FaceDB::getFace( const std::string &name ) const
{
    if ( m_faces.count( name ) == 0 ) return Face();

    return m_faces.at( name );
}

std::list< std::string > FaceDB::getListOfFaces() const
{
    std::list< std::string > res;

    for ( auto &p : m_faces ) {
        res.push_back( p.first );
    }

    return res;
}

bool FaceDB::hasFace( const std::string &name ) const
{
    if ( m_faces.count( name ) == 0 ) return false;

    return true;
}

void FaceDB::setColor( const std::string &name, int col )
{
    if ( m_faces.count( name ) == 0 ) return;

    m_faces.at( name ).setColor( col );
}
