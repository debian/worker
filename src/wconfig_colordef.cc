/* wconfig_colordef.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig.h"

WConfig::ColorDef::ColorDef()
{
    label_colors_t c1;
    c1.normal_fg = 4;
    c1.normal_bg = 2;
    c1.active_fg = 4;
    c1.active_bg = 7;
    addLabelColor( "marked", c1 );
    addLabelColor( "1", c1 );

    c1.normal_fg = 5;
    c1.normal_bg = 2;
    c1.active_fg = 5;
    c1.active_bg = 7;
    addLabelColor( "2", c1 );

    c1.normal_fg = 6;
    c1.normal_bg = 2;
    c1.active_fg = 6;
    c1.active_bg = 7;
    addLabelColor( "3", c1 );

    c1.normal_fg = 4;
    c1.normal_bg = 0;
    c1.active_fg = 4;
    c1.active_bg = 7;
    c1.only_exact_path = true;
    addLabelColor( "exclude_search", c1 );
}
    
WConfig::ColorDef::~ColorDef()
{
}

WConfig::ColorDef &WConfig::ColorDef::operator=( const WConfig::ColorDef &other )
{
    if ( this != &other ) {
        setLabelColors( other.getLabelColors() );
    }
    return *this;
}

void WConfig::ColorDef::setLabelColors( const std::map<std::string, label_colors_t> &cols )
{
    m_label_colors = cols;
}

const std::map<std::string, WConfig::ColorDef::label_colors_t> &WConfig::ColorDef::getLabelColors() const
{
    return m_label_colors;
}

const WConfig::ColorDef::label_colors_t &WConfig::ColorDef::getLabelColor( const std::string &label ) const
{
    std::map<std::string, WConfig::ColorDef::label_colors_t>::const_iterator it1;

    it1 = m_label_colors.find( label );
    if ( it1 != m_label_colors.end() ) return it1->second;
    throw 1;
}

void WConfig::ColorDef::addLabelColor( const std::string &label,
                                       const label_colors_t &col )
{
    m_label_colors[label] = col;
}

void WConfig::ColorDef::removeLabelColor( const std::string &label )
{
    m_label_colors.erase( label );
}
