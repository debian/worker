/* wconfig_color.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_COLOR_HH
#define WCONFIG_COLOR_HH

#include "wdefines.h"
#include "wconfig_panel.hh"

class WConfig;
class SolidButton;
class FaceDB;

class ColorPanel : public WConfigPanel
{
public:
  ColorPanel( AWindow &basewin, WConfig &baseconfig );
  ~ColorPanel();
  int create();
  int saveValues();

  /* gui elements callback */
  void run( Widget *, const AGMessage &msg ) override;

  /* we are interested in updated colors */
  panel_action_t setColors( List *colors );
  panel_action_t setFaceDB( const FaceDB &faces );
protected:
  WConfig *tempconfig;
  SolidButton *sbsb;
  Button *sbvb;
  Button *sbhb;
  SolidButton *slvbsb;
  Button *slvbvb;
  Button *slvbhb;
  SolidButton *ulvbsb;
  Button *ulvbvb;
  Button *ulvbhb;
  FieldListView *lv;
  Button *b[10][2];
  SolidButton *cbsb;
  Button *cbvb;
  Button *cbhb;
  SolidButton *rsb;
  Button *rvb;
  Button *rhb;

    FieldListView *m_faces_lv;
    Button *m_face_modify;
    Button *m_face_unset;
    Button *m_face_default;

    FaceDB *m_facedb_copy;
    FaceDB m_facedb_default;

    void setLVColors( int row, int fg, int bg );

    void prepareFaceLV();

    void modifyFace();
    void unsetFace();
    void changeToDefaultFace();

    void adjustFaceLV( const std::string &name,
                       int col );
    void applyFaceChange( const std::string &name );

    void selectColor( const std::string &name );
};
 
#endif
