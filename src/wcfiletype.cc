/* wcfiletype.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wcfiletype.hh"
#include "aguix/aguix.h"
#include "aguix/util.h"
#include "datei.h"
#include "functionproto.h"
#include "simplelist.hh"
#include "stringmatcher_fnmatch.hh"

bool WCFiletype::s_use_extended_regex = false;
 
WCFiletype::WCFiletype()
{
  int i;
  name=dupstring("");
  pattern=dupstring("");
  usepattern=false;
  usefiledesc=false;
  for(i=1;i<64;i++) filedesc[i]=-1;
  filedesc[0]=-2;
  internID = NORMALTYPE;

  patternIgnoreCase = false;
  patternUseRegExp = false;
  patternUseFullname = false;
#ifdef HAVE_REGEX
  patternRegCompiled = false;
#endif

  extCond = dupstring( "" );
  useExtCond = false;

  subtypes = NULL;
  parentType = NULL;

  colmode = DEFAULTCOL;
  for ( i = 0; i < 4; i++ ) {
    colfg[i] = 1;
    colbg[i] = 0;
  }
  colext = NULL;

  m_use_magic = false;
  m_magic_pattern = "";
  m_magic_compressed = false;
  m_magic_mime = false;
  m_magic_ignore_case = false;

  m_pattern_is_comma_separated = false;
}

int WCFiletype::getinternID() const
{
  return internID;
}

void WCFiletype::setinternID(int newid)
{
  if((newid<NORMALTYPE)||(newid>DIRTYPE)) return;
  internID=newid;
}

WCFiletype::~WCFiletype()
{
  if(name!=NULL) _freesafe(name);

#ifdef HAVE_REGEX
  freeCompPatternReg();
#endif
  if(pattern!=NULL) _freesafe(pattern);

  if ( extCond != NULL ) _freesafe( extCond );

  if ( subtypes != NULL ) {
    while ( subtypes->size() > 0 ) {
      WCFiletype *tt = subtypes->back();
      subtypes->pop_back();
      delete tt;
    }
    delete subtypes;
  }

  if ( colext != NULL ) _freesafe( colext );
}

WCFiletype *WCFiletype::duplicate() const
{
  int i;
  WCFiletype *tft=new WCFiletype();
  tft->setName(name);
  tft->setPattern(pattern);
  tft->setUsePattern(usepattern);
  tft->setFiledesc(filedesc);
  tft->setUseFiledesc(usefiledesc);
  tft->setDNDActions(dndaction);
  tft->setDoubleClickActions(doubleclickaction);
  tft->setShowActions(showaction);
  tft->setRawShowActions(rawshowaction);
  for(i=0;i<10;i++) tft->setUserActions(i,useraction[i]);

  for ( auto &key : customaction ) {
      tft->setCustomActions( key.first, key.second );
  }

  tft->setinternID(internID);
  tft->setPatternIgnoreCase( patternIgnoreCase );
  tft->setPatternUseRegExp( patternUseRegExp );
  tft->setPatternUseFullname( patternUseFullname );
  tft->setExtCond( extCond );
  tft->setUseExtCond( useExtCond );
  
  if ( subtypes != NULL ) {
    std::list<WCFiletype*>::iterator it1;
    
    for ( it1 = subtypes->begin(); it1 != subtypes->end(); it1++ ) {
      tft->addSubType( (*it1)->duplicate() );
    }
  }

  tft->setColorMode( colmode );
  tft->setCustomColor( 0, colfg );
  tft->setCustomColor( 1, colbg );
  tft->setColorExt( colext );

  tft->setUseMagic( m_use_magic );
  tft->setMagicCompressed( m_magic_compressed );
  tft->setMagicPattern( m_magic_pattern );
  tft->setMagicMime( m_magic_mime );
  tft->setMagicIgnoreCase( m_magic_ignore_case );

  tft->setPatternIsCommaSeparated( m_pattern_is_comma_separated );

  tft->setPriority( m_priority );

  return tft;
}

void WCFiletype::setName(const char *nname)
{
  if(name!=NULL) _freesafe(name);
  if(nname==NULL) {
    name=dupstring("");
  } else {
    name=dupstring(nname);
  }

  m_name_contains_flags = false;
  if ( name ) {
      if ( strchr( name, '{' ) != NULL ) {
          m_name_contains_flags = true;
      }
  }
}

void WCFiletype::setPattern(const char *npattern)
{
  if(pattern!=NULL) _freesafe(pattern);
#ifdef HAVE_REGEX
  freeCompPatternReg();
#endif
  if(npattern==NULL) {
    pattern=dupstring("");
  } else {
    pattern=dupstring(npattern);
  }

  splitPattern();
}

std::string WCFiletype::getName() const
{
    return name;
}

char *WCFiletype::getPattern()
{
  return pattern;
}

void WCFiletype::setUsePattern(bool nup)
{
  usepattern=nup;
}

bool WCFiletype::getUsePattern() const
{
  return usepattern;
}

void WCFiletype::setUseFiledesc(bool nufd)
{
  usefiledesc=nufd;
}

bool WCFiletype::getUseFiledesc() const
{
  return usefiledesc;
}

void WCFiletype::setFiledesc( const short *nfd)
{
  for(int i=0;i<64;i++) filedesc[i]=nfd[i];
}

short *WCFiletype::getFiledesc()
{
  return filedesc;
}

command_list_t WCFiletype::getDNDActions()
{
  return dndaction;
}

command_list_t WCFiletype::getDoubleClickActions()
{
  return doubleclickaction;
}

command_list_t WCFiletype::getShowActions()
{
  return showaction;
}

command_list_t WCFiletype::getRawShowActions()
{
  return rawshowaction;
}

command_list_t WCFiletype::getUserActions(int id)
{
    if((id<0)||(id>9)) return {};
  return useraction[id];
}

void WCFiletype::setDNDActions( const command_list_t &ndnda )
{
    dndaction.clear();
    for ( auto &fp : ndnda ) {
        dndaction.push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

void WCFiletype::setDoubleClickActions( const command_list_t &ndca )
{
    doubleclickaction.clear();
    for ( auto &fp : ndca ) {
        doubleclickaction.push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

void WCFiletype::setShowActions( const command_list_t &nsa )
{
    showaction.clear();
    for ( auto &fp : nsa ) {
        showaction.push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

void WCFiletype::setRawShowActions( const command_list_t &nrsa )
{
    rawshowaction.clear();
    for ( auto &fp : nrsa ) {
        rawshowaction.push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

void WCFiletype::setUserActions(int uid, const command_list_t &nua )
{
  if((uid<0)||(uid>9)) return;

    useraction[uid].clear();
    for ( auto &fp : nua ) {
        useraction[uid].push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

void WCFiletype::setPatternIgnoreCase( bool nv )
{
#ifdef HAVE_REGEX
  freeCompPatternReg();
#endif
  patternIgnoreCase = nv;
}

bool WCFiletype::getPatternIgnoreCase() const
{
  return patternIgnoreCase;
}

void WCFiletype::setPatternUseRegExp( bool nv )
{
    if ( m_pattern_is_comma_separated == true ) return;

  patternUseRegExp = nv;
}

bool WCFiletype::getPatternUseRegExp() const
{
  return patternUseRegExp;
}

#ifdef HAVE_REGEX
void WCFiletype::freeCompPatternReg()
{
  if ( patternRegCompiled == true ) {
    regfree( &patternReg );
    patternRegCompiled = false;
  }
}

void WCFiletype::compilePatternReg()
{
  int erg;

  if ( patternRegCompiled == false ) {
    erg = regcomp( &patternReg, pattern,
                   ( s_use_extended_regex ? REG_EXTENDED : 0 ) | REG_NOSUB | ( ( patternIgnoreCase == true ) ? REG_ICASE : 0 ) );
    if ( erg == 0 ) {
      patternRegCompiled = true;
    } else {
      // error
    }
  }
}
#endif

bool WCFiletype::patternMatchString( const char *mstr )
{
  bool matched = false;
#ifdef HAVE_REGEX
  int erg;
#endif
  char *lstr;
  int i;

  if ( strlen( pattern ) < 1 ) return false;
  if ( mstr == NULL ) return false;
  if ( strlen( mstr ) < 1 ) return false;
  
#ifdef HAVE_REGEX
  if ( patternUseRegExp == false ) {
#else
  if ( 1 ) {
#endif
    lstr = dupstring( mstr );
    if ( patternIgnoreCase == true ) {
      for ( i = 0; lstr[i] != '\0'; i++ )
        lstr[i] = tolower( lstr[i] );
    }

    if ( m_pattern_is_comma_separated ) {
        for ( auto &s : m_pattern_split ) {
            if ( patternIgnoreCase == true ) {
                char *pstr = dupstring( s.c_str() );
                for ( i = 0; pstr[i] != '\0'; i++ )
                    pstr[i] = tolower( pstr[i] );
    
                if ( fnmatch( pstr, lstr, 0 ) == 0 ) matched = true;
                
                _freesafe( pstr );
            } else {
                if ( fnmatch( s.c_str(), lstr, 0 ) == 0 ) matched = true;
            }

            if ( matched ) break;
        }
    } else {
        char *pstr = dupstring( pattern );
        if ( patternIgnoreCase == true ) {
            for ( i = 0; pstr[i] != '\0'; i++ )
                pstr[i] = tolower( pstr[i] );
        }
    
        if ( fnmatch( pstr, lstr, 0 ) == 0 ) matched = true;
        _freesafe( pstr );
    }

    _freesafe( lstr );
  } else {
#ifdef HAVE_REGEX
    compilePatternReg();
    if ( patternRegCompiled == true ) {
      erg = regexec( &patternReg, mstr, 0, NULL, 0 );
      if ( erg == 0 ) matched = true;
    }
#endif
  }
  return matched;
}

int WCFiletype::sortfunction( void *p1, void *p2, int mode )
{
  WCFiletype *ft1, *ft2;
  int erg = 0;
  
  if ( ( p1 == NULL ) || ( p2 == NULL ) ) return 0;
  
  ft1 = (WCFiletype*)p1;
  ft2 = (WCFiletype*)p2;

  switch ( mode ) {
    default:
        erg = strcasecmp( ft1->getName().c_str(),
                          ft2->getName().c_str() );
      break;
  }

  return erg;
}

void WCFiletype::clearFiledesc()
{
  int i;
  
  for ( i = 1; i < 64; i++ ) filedesc[i] = -1;
  filedesc[0] = -2;
}

void WCFiletype::setFiledesc( int pos, short v )
{
  int i;

  if ( ( pos < 0 ) || ( pos > 63 ) ) return;
  if ( ( v < -2 ) || ( v > 255 ) ) return;
  
  for ( i = 0; i < pos; i++ ) {
    if ( filedesc[i] == -2 ) filedesc[i] = -1;
  }
  filedesc[pos] = v;
  for ( i = 63; i >= 0; i-- ) {
    if ( filedesc[i] >= 0 ) {
      if ( i < 63 ) filedesc[i + 1] = -2;
      break;
    }
  }
  if ( i < 0 ) if ( filedesc[0] < 0 ) filedesc[0] = -2;
}

void WCFiletype::setPatternUseFullname( bool nv )
{
  patternUseFullname = nv;
}

bool WCFiletype::getPatternUseFullname() const
{
  return patternUseFullname;
}

void WCFiletype::setExtCond( const char *tstr )
{
  if ( extCond != NULL ) _freesafe( extCond );
  extCond = dupstring ( tstr );
}

void WCFiletype::setUseExtCond( bool nv )
{
  useExtCond = nv;
}

const char *WCFiletype::getExtCond() const
{
  return extCond;
}

bool WCFiletype::getUseExtCond() const
{
  return useExtCond;
}

void WCFiletype::addSubType( WCFiletype *newtype )
{
  if ( newtype == NULL ) return;
  if ( subtypes == NULL ) subtypes = new std::list<WCFiletype*>;
  subtypes->push_back( newtype );
  newtype->setParentType( this );
}

void WCFiletype::removeSubType( WCFiletype *remtype )
{
  std::list<WCFiletype*>::iterator it1;

  if ( remtype == NULL ) return;
  if ( subtypes == NULL ) return;
  for ( it1 = subtypes->begin(); it1 != subtypes->end(); it1++ ) {
    if ( *it1 == remtype ) {
      subtypes->erase( it1 );
      remtype->setParentType( NULL );
      break;
    }
  }
}

const std::list<WCFiletype*> *WCFiletype::getSubTypeList()
{
  return subtypes;
}

void WCFiletype::setParentType( WCFiletype *pt )
{
  parentType = pt;
}

WCFiletype *WCFiletype::getParentType() const
{
  return parentType;
}

std::vector<unsigned int> *WCFiletype::getTypePos()
{
  std::vector<unsigned int> *v;

  v = new std::vector<unsigned int>;
  if ( parentType != NULL ) parentType->getTypePos( this, v );
  return v;
}

void WCFiletype::getTypePos( const WCFiletype *child,
			     std::vector<unsigned int> *v )
{
  std::list<WCFiletype*>::iterator it1;
  unsigned int pos;
  
  if ( ( v == NULL ) || ( child == NULL ) ) return;

  for ( it1 = subtypes->begin(), pos = 0; it1 != subtypes->end(); it1++, pos++ ) {
    if ( *it1 == child ) {
      v->push_back( pos );
      break;
    }
  }
  if ( parentType != NULL ) parentType->getTypePos( this, v );
}

bool WCFiletype::sortfunctionType( WCFiletype *ft1, WCFiletype *ft2 ) {
  int erg = WCFiletype::sortfunction( ft1, ft2, 0 );
  if ( erg < 0 ) return true;
  return false;
}

void WCFiletype::sortSubTypeList()
{
  if ( subtypes == NULL ) return;

  subtypes->sort( sortfunctionType );
}

WCFiletype::colordef_t WCFiletype::getColorMode() const
{
  return colmode;
}

void WCFiletype::setColorMode( colordef_t newmode )
{
  colmode = newmode;
}

const int *WCFiletype::getCustomColor( int m ) const
{
  if ( m == 0 ) return colfg;
  else if ( m == 1 ) return colbg;
  return NULL;
}

void WCFiletype::setCustomColor( int m, const int *c )
{
  int i;

  if ( c == NULL ) return;
  if ( m == 0 ) {
    for ( i = 0; i < 4; i++ )
      colfg[i] = c[i];
  } else if ( m == 1 ) {
    for ( i = 0; i < 4; i++ )
      colbg[i] = c[i];
  }
}

void WCFiletype::setCustomColor( int m, int pos, int c )
{
  if ( m == 0 ) {
    if ( ( pos >= 0 ) && ( pos < 4 ) )
      colfg[pos] = c;
  } else if ( m == 1 ) {
    if ( ( pos >= 0 ) && ( pos < 4 ) )
      colbg[pos] = c;
  }
}

const char *WCFiletype::getColorExt() const
{
  return colext;
}

void WCFiletype::setColorExt( const char *s )
{
  if ( colext != NULL ) _freesafe( colext );
  
  if ( s == NULL ) colext = NULL;
  else colext = dupstring( s );
}

bool WCFiletype::save(Datei *fh) const
{
  int i;
  std::string str1;
  char buf[A_BYTESFORNUMBER(int)];
  if(fh==NULL) return false;

  fh->configPutPairString( "title", name );
  fh->configPutPairString( "pattern", pattern );
  fh->configPutPairBool( "usepattern", usepattern );
  fh->configPutPairBool( "usecontent", usefiledesc );
  
  fh->configOpenSection( "content" );
  for ( i = 0; i < 64; i++ ) {
    if ( filedesc[i] >= 0 ) {
      sprintf( buf, "%d", i );
      str1 = buf;
      str1 += " = ";
      sprintf( buf, "%d", filedesc[i] );
      str1 += buf;
      str1 += ";";
      fh->configPutInfo( str1.c_str(), false );
    }
  }
  fh->configCloseSection(); //content

  switch ( internID ) {
    case NOTYETTYPE:
      fh->configPutPair( "type", "notyetchecked" );
      break;
    case UNKNOWNTYPE:
      fh->configPutPair( "type", "unknown" );
      break;
    case VOIDTYPE:
      fh->configPutPair( "type", "noselect" );
      break;
    case DIRTYPE:
      fh->configPutPair( "type", "dir" );
      break;
    default:
      fh->configPutPair( "type", "normal" );
      break;
  }

  fh->configPutPairBool( "patternignorecase", patternIgnoreCase );
  fh->configPutPairBool( "patternuseregexp", patternUseRegExp );
  fh->configPutPairBool( "patternusefullname", patternUseFullname );

  fh->configPutPairString( "extcond", extCond );
  fh->configPutPairBool( "useextcond", useExtCond );

  fh->configOpenSection( "ftcommands" );

  if ( ! dndaction.empty() ) {
      fh->configOpenSection( "dnd" );
      for ( auto &f1 : dndaction ) {
          FunctionProto::presave( fh, f1.get() );
      }
      fh->configCloseSection(); //dnd
  }

  if ( ! doubleclickaction.empty() ) {
      fh->configOpenSection( "doubleclick" );
      for ( auto &f1 : doubleclickaction ) {
          FunctionProto::presave( fh, f1.get() );
      }
      fh->configCloseSection(); //doubleclick
  }

  if ( ! showaction.empty() ) {
      fh->configOpenSection( "show" );
      for ( auto &f1 : showaction ) {
          FunctionProto::presave( fh, f1.get() );
      }
      fh->configCloseSection(); //show
  }
  
  if ( ! rawshowaction.empty() ) {
      fh->configOpenSection( "rawshow" );
      for ( auto &f1 : rawshowaction ) {
          FunctionProto::presave( fh, f1.get() );
      }
      fh->configCloseSection(); //rawshow
  }

  for ( i = 0; i < 10; i++ ) {
      if ( ! useraction[i].empty() ) {
          sprintf( buf, "%d", i );
          str1 = "user ";
          str1 += buf;
          fh->configOpenSection( str1.c_str() );
          for ( auto &f1 : useraction[i] ) {
              FunctionProto::presave( fh, f1.get() );
          }
          fh->configCloseSection(); //user x
      }
  }

  for ( auto &key : customaction ) {
      fh->configOpenSection( "customaction" );
      fh->configPutPairString( "name", key.first.c_str() );

      fh->configOpenSection( "commands" );
      for ( auto &f1 : key.second ) {
          FunctionProto::presave( fh, f1.get() );
      }
      fh->configCloseSection(); //commands
      fh->configCloseSection(); //customaction
  }

  fh->configCloseSection(); //ftcommands

  fh->configOpenSection( "subtype" );
  if ( subtypes != NULL ) {
    std::list<WCFiletype*>::iterator it1;
    
    for ( it1 = subtypes->begin(); it1 != subtypes->end(); it1++ ) {
      fh->configOpenSection( "filetype" );
      (*it1)->save( fh );
      fh->configCloseSection(); //filetype
    }
  }
  fh->configCloseSection(); //subtype

  switch ( colmode ) {
    case CUSTOMCOL:
      fh->configPutPair( "colormode", "custom" );
      break;
    case EXTERNALCOL:
      fh->configPutPair( "colormode", "extern" );
      break;
    case PARENTCOL:
      fh->configPutPair( "colormode", "parent" );
      break;
    default:
      fh->configPutPair( "colormode", "default" );
      break;
  }
  if ( colext != NULL ) fh->configPutPairString( "colorexternprog", colext );
  fh->configPutPairNum( "unselectcolor", colfg[0], colbg[0] );
  fh->configPutPairNum( "selectcolor", colfg[1], colbg[1] );
  fh->configPutPairNum( "unselectactivecolor", colfg[2], colbg[2] );
  fh->configPutPairNum( "selectactivecolor", colfg[3], colbg[3] );

  if ( m_magic_pattern.length() > 0 ) {
      fh->configPutPairString( "magicpattern", m_magic_pattern.c_str() );
  }
  if ( m_use_magic == true ) {
      fh->configPutPairBool( "usemagic", m_use_magic );
  }

  if ( m_magic_compressed == true ) {
      fh->configPutPairBool( "magiccompressed", m_magic_compressed );
  }

  if ( m_magic_mime == true ) {
      fh->configPutPairBool( "magicmime", m_magic_mime );
  }

  if ( m_magic_ignore_case == true ) {
      fh->configPutPairBool( "magicignorecase", true );
  }

  if ( m_pattern_is_comma_separated == true ) {
      fh->configPutPairBool( "pattern_is_comma_separated", true );
  }

  if ( m_priority != 0 ) {
      fh->configPutPairNum( "priority", m_priority );
  }

  return true;
}

void WCFiletype::fillNamesOfCustomActions( bool recursive, std::list<std::string> &names )
{
    //TODO consider only lists with content
    
    std::map<std::string,List*>::iterator it1;
    
    //TODO iterate through names and check existence
    for ( auto &key : customaction ) {
        names.push_back( key.first );
    }
    
    if ( recursive == true && parentType != NULL ) {
        parentType->fillNamesOfCustomActions( recursive, names );
    }
}

command_list_t WCFiletype::getActionList( bool recursive, const ActionDescr &descr )
{
    auto res_list = descr.get_action_list( this );
    if ( recursive == true && getParentType() != NULL &&
         ( res_list.empty() ) ) {
        res_list = getParentType()->getActionList( recursive, descr );
    }
    return res_list;
}

std::pair< command_list_t, bool > WCFiletype::getCustomActions( const std::string &customname )
{
    if ( customaction.count( customname ) > 0 ) {
        return std::make_pair( customaction[customname], true );
    }
    return std::make_pair( command_list_t{}, false );
}

void WCFiletype::setCustomActions( const std::string &customname, const command_list_t &nua )
{
    if ( customaction.count( customname ) > 0 ) {
        customaction[customname].clear();
    } else {
        customaction[customname] = {};
    }

    for ( auto &fp : nua ) {
        customaction[customname].push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

void WCFiletype::removeCustomAction( const std::string &customname )
{
    if ( customaction.count( customname ) > 0 ) {
        customaction[customname].clear();
        customaction.erase( customname );
    }
}

void WCFiletype::clearCustomActions()
{
    customaction.clear();
}

bool WCFiletype::getUseMagic() const
{
    return m_use_magic;
}

void WCFiletype::setUseMagic( bool nv )
{
    m_use_magic = nv;
}

bool WCFiletype::getMagicCompressed() const
{
    return m_magic_compressed;
}

void WCFiletype::setMagicCompressed( bool nv )
{
    m_magic_compressed = nv;
}

std::string WCFiletype::getMagicPattern() const
{
    return m_magic_pattern;
}

void WCFiletype::setMagicPattern( const std::string &pat )
{
    m_magic_pattern = pat;
    m_magic_matcher = NULL;
}

bool WCFiletype::magicMatch( const std::string &match_string )
{
    if ( m_magic_matcher.get() == NULL ) {
        //TODO also support regexs matches
        m_magic_matcher.reset( new StringMatcherFNMatch() );
        m_magic_matcher->setMatchString( m_magic_pattern );
        m_magic_matcher->setMatchCaseSensitive( m_magic_ignore_case ? false : true );
    }
    return m_magic_matcher->match( match_string );
}

bool WCFiletype::getMagicMime() const
{
    return m_magic_mime;
}

void WCFiletype::setMagicMime( bool nv )
{
    m_magic_mime = nv;
}

void WCFiletype::setUseExtendedRegEx( bool nv )
{
    s_use_extended_regex = nv;
}
 
bool WCFiletype::getMagicIgnoreCase() const
{
    return m_magic_ignore_case;
}

void WCFiletype::setMagicIgnoreCase( bool nv )
{
    m_magic_ignore_case = nv;
    m_magic_matcher = NULL;
}

void WCFiletype::setPatternIsCommaSeparated( bool nv )
{
    if ( patternUseRegExp ) return;

    m_pattern_is_comma_separated = nv;

    splitPattern();
}

bool WCFiletype::getPatternIsCommaSeparated() const
{
    return m_pattern_is_comma_separated;
}

void WCFiletype::splitPattern()
{
    m_pattern_split.clear();

    if ( m_pattern_is_comma_separated ) {
        AGUIXUtils::split_string( m_pattern_split,
                                  pattern,
                                  ',' );
    }
}

bool WCFiletype::nameContainsFlags() const
{
    return m_name_contains_flags;
}

int WCFiletype::priority() const
{
    return m_priority;
}

void WCFiletype::setPriority( int prio )
{
    m_priority = prio;
}
