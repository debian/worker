/* worker_types.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2003-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WORKER_TYPES_H
#define WORKER_TYPES_H

#include "wdefines.h"
#include "functionproto.h"

namespace WorkerTypes {

typedef enum { LISTCOL_NAME = 0,
               LISTCOL_SIZE,
               LISTCOL_TYPE,
               LISTCOL_PERM,
               LISTCOL_OWNER,
               LISTCOL_DEST,
               LISTCOL_MOD,
               LISTCOL_ACC,
               LISTCOL_CHANGE,
               LISTCOL_INODE,
               LISTCOL_NLINK,
               LISTCOL_BLOCKS,
               LISTCOL_SIZEH,
               LISTCOL_EXTENSION,
               LISTCOL_CUSTOM_ATTR } listcol_t;

struct availListCols_t {
  listcol_t type;
  unsigned int catalogid;
};

extern struct availListCols_t availListCols[];

unsigned int getAvailListColsSize();
int getAvailListColEntry( listcol_t type );

struct reg_command_info_t {
    std::string name;
    std::string description;
    FunctionProto::command_categories_t category;
    bool has_args;
};

    enum command_menu_node {
        TOP_LEVEL,
        MENUS,
        BUTTONS,
        HOTKEYS,
        PATHS,
        LV_MODES,
        LV_COMMANDS,
        COMMANDS
    };
}

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
