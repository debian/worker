/* stringmatcher_fnmatch.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007,2012-2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STRINGMATCHER_FNMATCH_HH
#define STRINGMATCHER_FNMATCH_HH

#include "wdefines.h"
#include "stringmatcher.hh"

class StringMatcherFNMatch : public StringMatcher
{
public:
    StringMatcherFNMatch();
    ~StringMatcherFNMatch();

    bool match( const std::string &str );

    void setMatchString( const std::string &str );
    std::string getMatchString() const;
    void setMatchCaseSensitive( bool nv );
    bool getMatchCaseSensitive() const;

    static std::string convertMatchStringToFlexibleMatch( const std::string &str );
    static std::string cleanFlexibleMatchString( const std::string &str );
private:
    std::string _match_string;
    bool _match_case_sensitive;

    std::string _match_string_lowercase;
    void createLoweredCase();
};

#endif
