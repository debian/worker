/* nwc_os.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "nwc_os.hh"
#include "nwc_fsentry.hh"
#include "aguix/lowlevelfunc.h"

namespace NWC {
    namespace OS {
        std::string getCWD()
        {
            char *buf = NULL;
            int size = 1024;
            std::string s;

            for(;;) {
                buf = (char*)_allocsafe( size );
                if ( getcwd( buf, size ) != NULL ) break;
                else if ( errno != ERANGE ) {
                    strcpy( buf, "/" );  // use the root in case of error
                    break;
                }
                _freesafe( buf );
                size *= 2;
            }
            if ( buf == NULL || buf[0] != '/' ) {
                // no result or no absolute path
                // according to man page it should return absolute paths
                // but it does happen that there will be only a relative path
                // (like for stale nfs home directories)
                s = "/";
            } else {
                s = buf;
            }
            if ( buf != NULL ) _freesafe( buf );
            return s;
        }

        char *resolveEnv( const char* str )
        {
            char *resstr, *tstr, *tstr2;
            char *buf;
            int i, j;

            resstr = dupstring( "" );
            int pos = 0, count;

            for ( i = pos; i < (int)strlen( str ); i++ ) {
                if ( str[i] == '$' ) {
                    // there is an env-variable to resolve
                    if ( i > pos ) {
                        // copy the chars before
                        buf = (char*)_allocsafe( i - pos + 1 );
                        strncpy( buf, str + pos, i - pos );
                        buf[i - pos] = 0;
                        tstr = catstring( resstr, buf );
                        _freesafe( resstr );
                        _freesafe( buf );
                        resstr = tstr;
                    }
                    // now find the end of the env-variable
                    if ( str[ i + 1 ] == '{' ) {
                        count = 1;
                        // search for closing bracket
                        for ( j = i + 2;; j++ ) {
                            if ( str[j] == '{' ) count++;
                            else if ( str[j] == 0 ) break;
                            else if ( str[j] == '}' ) count--;
                            if ( count == 0 ) {
                                j++; // to point at the first char after this
                                break;
                            }
                        }
                        // j-1 is the closing bracket
                        if ( ( j - 2 ) >= ( i + 2 ) ) {
                            tstr = dupstring( str + i + 2 );
                            tstr[ ( j - 2 ) - ( i + 2 ) + 1 ] = 0;
                            tstr2 = resolveEnv( tstr );
                            _freesafe( tstr );
                            tstr = catstring( resstr, tstr2 );
                            _freesafe( resstr );
                            _freesafe( tstr2 );
                            resstr = tstr;
                        }
                    } else {
                        // find the "/" or the end
                        for ( j = i + 1;; j++ ) {
                            if ( str[j] == 0 ) break;
                            else if ( str[j] == '/' ) break;
                        }
                        // j-1 is last char for the env
                        if ( ( j - 1 ) >= ( i + 1 ) ) {
                            tstr = dupstring( str + i + 1 );
                            tstr[ ( j - 1 ) - ( i + 1 ) + 1 ] = 0;
                            tstr2 = resolveEnv( tstr );
                            _freesafe( tstr );
                            tstr = catstring( resstr, tstr2 );
                            _freesafe( resstr );
                            _freesafe( tstr2 );
                            resstr = tstr;
                        }
                    }
                    pos = j;
                }
            }
            if ( i > pos ) {
                // copy the chars before
                buf = (char*)_allocsafe( i - pos + 1 );
                strncpy( buf, str + pos, i - pos );
                buf[ i - pos ] = 0;
                tstr = catstring( resstr, buf );
                _freesafe( resstr );
                _freesafe( buf );
                resstr = tstr;
            }
            tstr = getenv( resstr );
            _freesafe( resstr );
            if ( tstr == NULL ) resstr = dupstring( "" );
            else resstr = dupstring( tstr );
            return resstr;
        }

        char *getLinkTarget( const char *symlink )
        {
            if ( ! symlink ) return NULL;

            char *buf = NULL;
            ssize_t buf_size = 1024;

            while ( 1 ) {
                buf = (char*)_allocsafe( buf_size * sizeof( char ) );
                ssize_t bytes = worker_readlink( symlink, buf, buf_size );
                if ( bytes < 0 ) {
                    _freesafe( buf );
                    buf = NULL;
                    break;
                } else if ( bytes >= buf_size ) {
                    _freesafe( buf );
                    buf_size *= 2;
                } else {
                    buf[ bytes ] = '\0';
                    break;
                }
            }

            return buf;
        }

    }
}
