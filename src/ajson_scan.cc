
#line 1 "ajson_scan.rl"
/* ajson_scan.rl
* This file belongs to Worker, a file manager for UN*X/X11.
* Copyright (C) 2017-2019 Ralf Hoffmann.
* You can contact me at: ralf@boomerangsworld.de
*   or http://www.boomerangsworld.de/worker
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
* to generate output, run
* ragel ajson_scan.rl -o ajson_scan.cc
*/

#include "ajson_scan.hh"


#line 74 "ajson_scan.rl"



static const char _ajson_scanner_actions[] = {
	0, 1, 0, 1, 1, 1, 2, 1,
	3, 1, 4, 1, 5, 1, 6, 1,
	7, 1, 8, 1, 9, 1, 10, 1,
	11, 1, 12, 1, 13, 0
};

static const char _ajson_scanner_key_offsets[] = {
	0, 0, 7, 12, 13, 14, 15, 16,
	17, 18, 19, 20, 21, 22, 38, 48,
	0
};

static const char _ajson_scanner_trans_keys[] = {
	0, 13, 15, 34, 92, 2, 10, 34,
	92, 110, 114, 116, 97, 108, 115, 101,
	117, 108, 108, 114, 117, 101, 13, 32,
	34, 44, 58, 91, 93, 102, 110, 116,
	123, 125, 9, 10, 48, 57, 13, 32,
	44, 58, 91, 93, 123, 125, 9, 10,
	48, 57, 0
};

static const char _ajson_scanner_single_lengths[] = {
	0, 5, 5, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 12, 8, 0,
	0
};

static const char _ajson_scanner_range_lengths[] = {
	0, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 2, 1, 1,
	0
};

static const char _ajson_scanner_index_offsets[] = {
	0, 0, 7, 13, 15, 17, 19, 21,
	23, 25, 27, 29, 31, 33, 48, 58,
	0
};

static const signed char _ajson_scanner_trans_cond_spaces[] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, 0
};

static const char _ajson_scanner_trans_offsets[] = {
	0, 1, 2, 3, 4, 5, 6, 7,
	8, 9, 10, 11, 12, 13, 14, 15,
	16, 17, 18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, 28, 29, 30, 31,
	32, 33, 34, 35, 36, 37, 38, 39,
	40, 41, 42, 43, 44, 45, 46, 47,
	48, 49, 50, 51, 52, 53, 54, 55,
	56, 57, 58, 59, 60, 61, 0
};

static const char _ajson_scanner_trans_lengths[] = {
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 0
};

static const char _ajson_scanner_cond_keys[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0
};

static const char _ajson_scanner_cond_targs[] = {
	0, 0, 0, 13, 2, 0, 1, 1,
	1, 1, 1, 1, 0, 4, 0, 5,
	0, 6, 0, 13, 0, 8, 0, 9,
	0, 13, 0, 11, 0, 12, 0, 13,
	0, 14, 14, 1, 13, 13, 13, 13,
	3, 7, 10, 13, 13, 14, 15, 0,
	14, 14, 13, 13, 13, 13, 13, 13,
	14, 13, 15, 13, 13, 13, 0
};

static const char _ajson_scanner_cond_actions[] = {
	0, 0, 0, 23, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 19, 0, 0, 0, 0,
	0, 21, 0, 0, 0, 0, 0, 17,
	0, 0, 0, 0, 15, 13, 5, 7,
	0, 0, 0, 9, 11, 0, 0, 0,
	0, 0, 15, 13, 5, 7, 9, 11,
	0, 27, 0, 25, 27, 25, 0
};

static const char _ajson_scanner_to_state_actions[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 1, 0, 0,
	0
};

static const char _ajson_scanner_from_state_actions[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 3, 0, 0,
	0
};

static const char _ajson_scanner_eof_trans_indexed[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 24, 25,
	0
};

static const char _ajson_scanner_eof_trans_direct[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 61, 62,
	0
};

static const char _ajson_scanner_nfa_targs[] = {
	0, 0
};

static const char _ajson_scanner_nfa_offsets[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0
};

static const char _ajson_scanner_nfa_push_actions[] = {
	0, 0
};

static const char _ajson_scanner_nfa_pop_trans[] = {
	0, 0
};

static const int ajson_scanner_start = 13;
static const int ajson_scanner_error = 0;

static const int ajson_scanner_en_main = 13;


#line 77 "ajson_scan.rl"


int ajson_scanner( const std::string &input,
std::list< ajson_token_t > &tokens )
{
	const char *p = input.c_str();
	const char *pe = p + input.length();
	const char *eof = pe;
	const char *ts = NULL, *te = NULL;
	int cs = ajson_scanner_start;
	
	
	{
		int _klen;
		unsigned int _trans = 0;
		unsigned int _cond = 0;
		const char *_acts;
		unsigned int _nacts;
		const char *_keys;
		const char *_ckeys;
		int _cpc;
		{
			
			if ( p == pe )
			goto _test_eof;
			if ( cs == 0 )
			goto _out;
			_resume:  {
				_acts = ( _ajson_scanner_actions + (_ajson_scanner_from_state_actions[cs]));
				_nacts = (unsigned int)(*( _acts));
				_acts += 1;
				while ( _nacts > 0 ) {
					switch ( (*( _acts)) ) {
						case 1:  {
							{
								#line 1 "NONE"
								{ts = p;}}
							break; }
					}
					_nacts -= 1;
					_acts += 1;
				}
				
				_keys = ( _ajson_scanner_trans_keys + (_ajson_scanner_key_offsets[cs]));
				_trans = (unsigned int)_ajson_scanner_index_offsets[cs];
				
				_klen = (int)_ajson_scanner_single_lengths[cs];
				if ( _klen > 0 ) {
					const char *_lower;
					const char *_mid;
					const char *_upper;
					_lower = _keys;
					_upper = _keys + _klen - 1;
					while ( 1 ) {
						if ( _upper < _lower )
						break;
						
						_mid = _lower + ((_upper-_lower) >> 1);
						if ( ( (*( p))) < (*( _mid)) )
						_upper = _mid - 1;
						else if ( ( (*( p))) > (*( _mid)) )
						_lower = _mid + 1;
						else {
							_trans += (unsigned int)(_mid - _keys);
							goto _match;
						}
					}
					_keys += _klen;
					_trans += (unsigned int)_klen;
				}
				
				_klen = (int)_ajson_scanner_range_lengths[cs];
				if ( _klen > 0 ) {
					const char *_lower;
					const char *_mid;
					const char *_upper;
					_lower = _keys;
					_upper = _keys + (_klen<<1) - 2;
					while ( 1 ) {
						if ( _upper < _lower )
						break;
						
						_mid = _lower + (((_upper-_lower) >> 1) & ~1);
						if ( ( (*( p))) < (*( _mid)) )
						_upper = _mid - 2;
						else if ( ( (*( p))) > (*( _mid + 1)) )
						_lower = _mid + 2;
						else {
							_trans += (unsigned int)((_mid - _keys)>>1);
							goto _match;
						}
					}
					_trans += (unsigned int)_klen;
				}
				
			}
			_match:  {
				_ckeys = ( _ajson_scanner_cond_keys + (_ajson_scanner_trans_offsets[_trans]));
				_klen = (int)_ajson_scanner_trans_lengths[_trans];
				_cond = (unsigned int)_ajson_scanner_trans_offsets[_trans];
				
				_cpc = 0;
				{
					const char *_lower;
					const char *_mid;
					const char *_upper;
					_lower = _ckeys;
					_upper = _ckeys + _klen - 1;
					while ( 1 ) {
						if ( _upper < _lower )
						break;
						
						_mid = _lower + ((_upper-_lower) >> 1);
						if ( _cpc < (int)(*( _mid)) )
						_upper = _mid - 1;
						else if ( _cpc > (int)(*( _mid)) )
						_lower = _mid + 1;
						else {
							_cond += (unsigned int)(_mid - _ckeys);
							goto _match_cond;
						}
					}
					cs = 0;
					goto _again;
				}
			}
			_match_cond:  {
				cs = (int)_ajson_scanner_cond_targs[_cond];
				
				if ( _ajson_scanner_cond_actions[_cond] == 0 )
				goto _again;
				
				_acts = ( _ajson_scanner_actions + (_ajson_scanner_cond_actions[_cond]));
				_nacts = (unsigned int)(*( _acts));
				_acts += 1;
				while ( _nacts > 0 )
				{
					switch ( (*( _acts)) )
					{
						case 2:  {
							{
								#line 60 "ajson_scan.rl"
								{te = p+1;{
										#line 60 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_BEGIN_ARRAY, "" ) ); }}}
							break; }
						case 3:  {
							{
								#line 61 "ajson_scan.rl"
								{te = p+1;{
										#line 61 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_END_ARRAY, "" ) ); }}}
							break; }
						case 4:  {
							{
								#line 62 "ajson_scan.rl"
								{te = p+1;{
										#line 62 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_BEGIN_OBJECT, "" ) ); }}}
							break; }
						case 5:  {
							{
								#line 63 "ajson_scan.rl"
								{te = p+1;{
										#line 63 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_END_OBJECT, "" ) ); }}}
							break; }
						case 6:  {
							{
								#line 64 "ajson_scan.rl"
								{te = p+1;{
										#line 64 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_NAME_SEPARATOR, "" ) ); }}}
							break; }
						case 7:  {
							{
								#line 65 "ajson_scan.rl"
								{te = p+1;{
										#line 65 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_VALUE_SEPARATOR, "" ) ); }}}
							break; }
						case 8:  {
							{
								#line 66 "ajson_scan.rl"
								{te = p+1;{
										#line 66 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_TRUE, "" ) ); }}}
							break; }
						case 9:  {
							{
								#line 67 "ajson_scan.rl"
								{te = p+1;{
										#line 67 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_FALSE, "" ) ); }}}
							break; }
						case 10:  {
							{
								#line 68 "ajson_scan.rl"
								{te = p+1;{
										#line 68 "ajson_scan.rl"
										tokens.push_back( std::make_pair( JSON_NULL, "" ) ); }}}
							break; }
						case 11:  {
							{
								#line 69 "ajson_scan.rl"
								{te = p+1;{
										#line 69 "ajson_scan.rl"
										std::string s1( ts + 1, te - ts - 2 ); tokens.push_back( std::make_pair( JSON_STRING, s1 ) ); }}}
							break; }
						case 12:  {
							{
								#line 70 "ajson_scan.rl"
								{te = p;p = p - 1;{
										#line 70 "ajson_scan.rl"
										std::string s1( ts, te - ts ); tokens.push_back( std::make_pair( JSON_NUMBER, s1 ) ); }}}
							break; }
						case 13:  {
							{
								#line 1 "-"
								{te = p;p = p - 1;}}
							break; }
					}
					_nacts -= 1;
					_acts += 1;
				}
				
				
			}
			_again:  {
				_acts = ( _ajson_scanner_actions + (_ajson_scanner_to_state_actions[cs]));
				_nacts = (unsigned int)(*( _acts));
				_acts += 1;
				while ( _nacts > 0 ) {
					switch ( (*( _acts)) ) {
						case 0:  {
							{
								#line 1 "NONE"
								{ts = 0;}}
							break; }
					}
					_nacts -= 1;
					_acts += 1;
				}
				
				if ( cs == 0 )
				goto _out;
				p += 1;
				if ( p != pe )
				goto _resume;
			}
			_test_eof:  { {}
				if ( p == eof )
				{
					if ( _ajson_scanner_eof_trans_direct[cs] > 0 ) {
						_trans = (unsigned int)_ajson_scanner_eof_trans_direct[cs] - 1;
						_cond = (unsigned int)_ajson_scanner_trans_offsets[_trans];
						goto _match_cond;
					}
				}
				
			}
			_out:  { {}
			}
		}
	}
	
	#line 88 "ajson_scan.rl"
	
	
	if (cs == ajson_scanner_error) {
		return -1;
	}
	
	return 0;
}
