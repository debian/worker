/* bookmarkdbfilter.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "bookmarkdbfilter.hh"
#include "bookmarkdbproxy.hh"
#include "bookmarkdbentry.hh"
#include <cctype>
#include <algorithm>
#include <functional>
#include "aguix/util.h"

BookmarkDBFilter::BookmarkDBFilter( BookmarkDBProxy &data ) : m_infix( "" ),
                                                              m_data( data ),
                                                              m_sort_mode( SORT_BY_NAME ),
                                                              m_reverse_sort( false )
{
}

void BookmarkDBFilter::setInfixFilter( const std::string &infix )
{
    m_infix = infix;
}

static bool compare_db_entry( const BookmarkDBFilter::bmdbfilter_sort_t m_sort_mode,
                              const bool m_reverse,
                              const BookmarkDBEntry& c1,
                              const BookmarkDBEntry& c2 )
{
    bool res;
    switch ( m_sort_mode ) {
        case BookmarkDBFilter::SORT_BY_ALIAS:
            res = c1.getAlias() < c2.getAlias();
            break;
        case BookmarkDBFilter::SORT_BY_CATEGORY:
            res = c1.getCategory() < c2.getCategory();
            break;
        default:
            res = c1.getName() < c2.getName();
            break;
    }
    if ( m_reverse ) res = ( res == false ) ? true : false;
    return res;
}

std::list< BookmarkDBEntry > BookmarkDBFilter::getEntries( const std::string &cat )
{
    std::list< BookmarkDBEntry > res, e = m_data.getEntries( cat );

    std::string infix = AGUIXUtils::tolower( m_infix );

    for ( std::list< BookmarkDBEntry >::iterator it1 = e.begin();
          it1 != e.end();
          it1++ ) {
        std::string name = AGUIXUtils::tolower( it1->getName() );
        if ( name.find( infix ) != std::string::npos ) {
            res.push_back( *it1 );
        } else {
            std::string alias = AGUIXUtils::tolower( it1->getAlias() );
            if ( alias.find( infix ) != std::string::npos ) {
                res.push_back( *it1 );
            }
        }
    }

    res.sort( [this]( auto &lhs, auto &rhs ) {
        return compare_db_entry( m_sort_mode,
                                 m_reverse_sort,
                                 lhs, rhs );
    });

    return res;
}

std::string BookmarkDBFilter::getInfixFilter() const
{
    return m_infix;
}

void BookmarkDBFilter::setSortMode( bmdbfilter_sort_t mode )
{
    m_sort_mode = mode;
}

void BookmarkDBFilter::setReverseSort( bool nv )
{
    m_reverse_sort = nv;
}

void BookmarkDBFilter::toggleSortMode( bmdbfilter_sort_t mode )
{
    if ( m_sort_mode != mode ) {
        m_sort_mode = mode;
        m_reverse_sort = false;
    } else {
        m_reverse_sort = ( m_reverse_sort == false ) ? true : false;
    }
}
