/* dirsortsettings.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2018 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dirsortsettings.hh"
#include "verzeichnis.hh"
#include "nwcentryselectionstate.hh"
#include <algorithm>

DirSortSettings::DirSortSettings() : _serial_nr( 0 ),
                                     _sort_mode( SORT_NAME )
{
}

DirSortSettings::~DirSortSettings()
{
}
    
bool DirSortSettings::check( std::vector< NWCEntrySelectionState > &d )
{
    std::sort( d.begin(),
               d.end(),
               [&]( const NWCEntrySelectionState &l,
                   const NWCEntrySelectionState &r ) -> bool
               {
                   return ( NWCEntrySelectionState::sortfunction( l,
                                                                  r,
                                                                  _sort_mode ) < 0 ) ? true : false;
               } );

    return true;
}

void DirSortSettings::setSortMode( int sort_mode )
{
    _sort_mode = sort_mode;
    _serial_nr++;
}

int DirSortSettings::getSortMode() const
{
    return _sort_mode;
}

int DirSortSettings::getSerialNr() const
{
    return _serial_nr;
}
