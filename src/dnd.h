/* dnd.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DND_H
#define DND_H

#include "wdefines.h"
#include "aguix/message.h"
#include <string>

class Lister;
class ListerMode;
class Worker;
class FileEntry;

class DNDMsg
{
public:
    DNDMsg( Worker *worker );
    ~DNDMsg();
    DNDMsg( const DNDMsg &other );
    DNDMsg &operator=( const DNDMsg &other );

    const FileEntry *getFE() const;
    std::string getDestDir() const;
    Lister *getDestLister() const;
    ListerMode *getDestMode() const;
    Lister *getSourceLister() const;
    ListerMode *getSourceMode() const;

    void setEnd( AGDNDEND *e );

    int getValue() const;
    class FieldLVRowData *getRowDataP() const;
    std::string *getExternalData() const;
    Widget *getSourceWidget() const;
    Widget *getDestWidget() const;
protected:
    int value;
    class FieldLVRowData *rowDataP;
    std::string *external_data;
  
    Lister *sl, *el;
    ListerMode *slm, *elm;
    Widget *source_widget;
    Widget *dest_widget;
  
    Worker *worker;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
