/* calendar.hh
 * This file belongs to Worker, a file manager for UNIX/X11.
 * Copyright (C) 2008,2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CALENDAR_HH
#define CALENDAR_HH

#include "wdefines.h"
#include <iosfwd>

class Calendar
{
public:
    Calendar( bool extended = false );
    ~Calendar();
    Calendar( const Calendar &other );
    Calendar &operator=( const Calendar &other );

    void setInitialDate( time_t t );
    void setInitialDate( const std::string &t );

    bool showCalendar();

    std::string getDate() const;
private:
    struct tm m_initial_date;
    struct tm m_current_date;
    struct tm m_today;
    bool m_extended;

    void updateWindow();
    void updateCalendar( const struct tm &new_time );

    class StringGadget *m_year_sg, *m_month_sg;
    class Button *m_day_buttons[6][7];
};

#endif
