/* wcpath.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCPATH_HH
#define WCPATH_HH

#include "wdefines.h"
#include "wckey.hh"

class Datei;

class WCPath:public WCKey
{
public:
  WCPath();
  ~WCPath();
  WCPath( const WCPath &other );
  WCPath &operator=( const WCPath &other );

  WCPath *duplicate();
  void setName(const char*);
  void setFG(int);
  void setBG(int);
  void setPath(const char*);
  char *getName();
  int getFG() const;
  int getBG() const;
  char *getPath();
  void setCheck(bool);
  bool getCheck() const;
  bool save(Datei*);
protected:
  char *name;
  int fg,bg;
  char *path;
  bool check;
};

#endif
