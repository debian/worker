/* waitthread.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "waitthread.hh"
#include "aguix/lowlevelfunc.h"
#include "worker.h"
#include "eventqueue.hh"
#include "aguix/timeoutstore.hh"

#include <sys/select.h>

WaitThread::WaitThread( RefCount< EventQueue > eq,
                        std::shared_ptr< TimeoutStore > timeouts ) : m_eq( eq ),
                                                                     m_timeouts( timeouts ),
                                                                     m_fd( -1 ),
                                                                     m_fd2( -1 ),
                                                                     m_fd3( -1 ),
                                                                     m_state( WAITTHREAD_NONE )
{
}

WaitThread::~WaitThread()
{
}

int WaitThread::run()
{
    debugmsg( "Wait thread started" );

    fd_set rdfs;
    int ret;
    bool running = true;

    m_state_cv.lock();
    while ( running ) {
        while ( m_state == WAITTHREAD_NONE ) {
            m_state_cv.wait();
        }

        if ( m_state == WAITTHREAD_STOP ) {
            running = false;
            continue;
        }

        m_state = WAITTHREAD_NONE;
        m_state_cv.unlock();

        loff_t timeout_ms;
        timeout_ms = m_timeouts->getNextTimeout();

        FD_ZERO( &rdfs );
        int max_fd_plus_one = 0;

        if ( m_fd >= 0 ) {
            FD_SET( m_fd, &rdfs );
            max_fd_plus_one = m_fd + 1;
        }

        if ( m_fd2 >= 0 ) {
            FD_SET( m_fd2, &rdfs );
            if ( ( m_fd2 + 1 ) > max_fd_plus_one ) {
                max_fd_plus_one = m_fd2 + 1;
            }
        }

        if ( m_fd3 >= 0 ) {
            FD_SET( m_fd3, &rdfs );
            if ( ( m_fd3 + 1 ) > max_fd_plus_one ) {
                max_fd_plus_one = m_fd3 + 1;
            }
        }

        if ( timeout_ms < 1 ) {
            ret = select( max_fd_plus_one, &rdfs, NULL, NULL, NULL );
        } else {
            struct timeval timeout;

            timeout.tv_sec = timeout_ms / 1000;
            timeout.tv_usec = ( timeout_ms % 1000 ) * 1000;
            ret = select( max_fd_plus_one, &rdfs, NULL, NULL, &timeout );
        }
        if ( ret > 0 ) {
            if ( m_fd2 >= 0 && FD_ISSET( m_fd2, &rdfs ) ) {
                m_eq->push( EventQueue::EQ_FD2 );
            } else if ( m_fd3 >= 0 && FD_ISSET( m_fd3, &rdfs ) ) {
                m_eq->push( EventQueue::EQ_FD3 );
            } else {
                m_eq->push( EventQueue::EQ_XEVENT );
            }
        } else if ( ret == 0 ) {
            m_eq->push( EventQueue::EQ_TIMEOUT );
        } else {
            m_eq->push( EventQueue::EQ_SELECT_ERROR );
        }
        m_state_cv.lock();
    }
    m_state_cv.unlock();

    debugmsg( "Wait thread finished" );
    return 0;
}

int WaitThread::state_set( waitthread_state_t v )
{
    m_state_cv.lock();
    m_state = v;
    m_state_cv.signal();
    m_state_cv.unlock();
    return 0;
}

void WaitThread::watchFD( int fd )
{
    m_fd = fd;
    if ( m_fd < -1 ) m_fd = -1;
}

void WaitThread::watchFD2( int fd2 )
{
    m_fd2 = fd2;
    if ( m_fd2 < -1 ) m_fd2 = -1;
}

void WaitThread::watchFD3( int fd3 )
{
    m_fd3 = fd3;
    if ( m_fd3 < -1 ) m_fd3 = -1;
}
