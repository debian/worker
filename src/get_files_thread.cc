/* get_files_thread.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2016 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "get_files_thread.hh"
#include "aguix/lowlevelfunc.h"
#include "nwc_file.hh"
#include "pdatei.h"
#include "worker_locale.h"
#include "nwc_virtualdir.hh"
#include <typeinfo>
#include <set>

#include <iostream>

GetFilesThread::GetFilesThread( std::unique_ptr<NWC::Dir> searchdir )
{
    m_searchdir = std::move( searchdir );
}

GetFilesThread::~GetFilesThread()
{
}

int GetFilesThread::run()
{
    debugmsg( "Search thread started" );

    doSearch();
  
    debugmsg( "Search thread finished" );

    return 0;
}

int GetFilesThread::doSearch()
{
    if ( m_searchdir ) {
        m_searchdir->walk( *this, NWC::Dir::RECURSIVE_DEMAND );
    }
    return 0;
}

int GetFilesThread::visit( NWC::File &file, NWC::Dir::WalkControlObj &cobj )
{
    if ( m_visit_cb ) {
        m_visit_cb( file );
    }
    
    if ( isCanceled() == true ) {
        cobj.setMode( NWC::Dir::WalkControlObj::ABORT );
    }
    return 0;
}

int GetFilesThread::visitEnterDir( NWC::Dir &dir, NWC::Dir::WalkControlObj &cobj )
{
    if ( m_visit_enter_dir_cb ) {
        bool res = m_visit_enter_dir_cb( dir );

        if ( res == false ) {
            cobj.setMode( NWC::Dir::WalkControlObj::SKIP );
        }
    }
    
    if ( isCanceled() == true ) {
        cobj.setMode( NWC::Dir::WalkControlObj::ABORT );
    }
    return 0;
}

int GetFilesThread::visitLeaveDir( NWC::Dir &dir, NWC::Dir::WalkControlObj &cobj )
{
    return 0;
}

int GetFilesThread::prepareDirWalk( const NWC::Dir &dir, NWC::Dir::WalkControlObj &cobj,
                                  std::list< RefCount< NWC::FSEntry > > &return_add_entries,
                                  std::list< RefCount< NWC::FSEntry > > &return_skip_entries )
{
    return 0;
}

void GetFilesThread::setVisitCB( std::function< void( NWC::File & ) > cb )
{
    m_visit_cb = cb;
}

void GetFilesThread::setVisitEnterDirCB( std::function< bool( NWC::Dir & ) > cb )
{
    m_visit_enter_dir_cb = cb;
}
