/* nwc_path.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NWC_PATH_HH
#define NWC_PATH_HH

#include "wdefines.h"

#include <string>

namespace NWC
{
  
    namespace Path {
        std::string join( const std::string &p1, const std::string &p2 );
        std::string normalize( const std::string &path );
        std::string basename( const std::string &path );
        std::string dirname( const std::string &path );
        std::string get_extended_basename( const std::string &prefix,
                                           const std::string &fullname );
        std::string get_real_path( const std::string &path );
        std::string get_last_local_path( const std::string &path );

        bool isAbs( const std::string &path );

        /*
         * checks if prefix is a directory prefix of path, i.e. the
         * directory "prefix" contains the entry "path"
         */
        bool is_prefix_dir( const std::string &prefix,
                            const std::string &path );

        char *parentDir( const char *, char * );
        char *parentDir( const char *, char *, int );
        char *handlePath( const char * );
        char *handlePathExt( const char* );
    }
}

#endif
