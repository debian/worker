/* ajson_scan.rl
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * to generate output, run
 * ragel ajson_scan.rl -o ajson_scan.cc
 */

#include "ajson_scan.hh"

%%{ machine ajson_scanner;

ws = [ \r\n\t]*;
beginarray = ws '\[';
beginobject = ws '{';
endarray = ws '\]';
endobject = ws '}';
nameseparator = ws ':';
valueseparator = ws ',';
number = [0-9]+;

false = 'false';
jnull = 'null';
true = 'true';

quotationmark = '"';

unescaped = ( 0x01-0x09 | 0x0b..0x0c | 0x0e | 0x10..0x21 | 0x23..0x5b | 0x5d..0x7f | 0x80..0xff );
escape = '\\';

char = unescaped |
          escape (
              '"'  |
              '\\' |
              'n'  |
              'r'  |
              't' );

string = quotationmark char* quotationmark;

main := |*
     beginarray => { tokens.push_back( std::make_pair( JSON_BEGIN_ARRAY, "" ) ); };
     endarray => { tokens.push_back( std::make_pair( JSON_END_ARRAY, "" ) ); };
     beginobject => { tokens.push_back( std::make_pair( JSON_BEGIN_OBJECT, "" ) ); };
     endobject => { tokens.push_back( std::make_pair( JSON_END_OBJECT, "" ) ); };
     nameseparator => { tokens.push_back( std::make_pair( JSON_NAME_SEPARATOR, "" ) ); };
     valueseparator => { tokens.push_back( std::make_pair( JSON_VALUE_SEPARATOR, "" ) ); };
     true => { tokens.push_back( std::make_pair( JSON_TRUE, "" ) ); };
     false => { tokens.push_back( std::make_pair( JSON_FALSE, "" ) ); };
     jnull => { tokens.push_back( std::make_pair( JSON_NULL, "" ) ); };
     string => { std::string s1( ts + 1, te - ts - 2 ); tokens.push_back( std::make_pair( JSON_STRING, s1 ) ); };
     number => { std::string s1( ts, te - ts ); tokens.push_back( std::make_pair( JSON_NUMBER, s1 ) ); };
     ws;
*|;

}%%

%% write data nofinal;

int ajson_scanner( const std::string &input,
                   std::list< ajson_token_t > &tokens )
{
    const char *p = input.c_str();
    const char *pe = p + input.length();
    const char *eof = pe;
    const char *ts = NULL, *te = NULL;
    int cs = ajson_scanner_start;

    %% write exec;

   if (cs == ajson_scanner_error) {
       return -1;
   }

   return 0;
}
