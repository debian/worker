/* worker_filefunc.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2005 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 ***********************************************************
 * worker_tmpdir is based on mc_tmpdir from mc-4.6.1pre<something>:
 *   Various utilities - Unix variants
 *   Copyright (C) 1994, 1995, 1996 the Free Software Foundation.
 *   Written 1994, 1995, 1996 by:
 *   Miguel de Icaza, Janne Kukonlehto, Dugan Porter,
 *   Jakub Jelinek, Mauricio Plaza.
 ***********************************************************
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: worker_filefunc.cc,v 1.2 2005/03/21 18:54:46 ralf Exp $ */

#include "wdefines.h"
#include "worker_filefunc.h"

int worker_myremove( const char *pathname )
{
  worker_struct_stat buf;

  if ( pathname != NULL ) {
    if ( worker_lstat( pathname, &buf ) == 0 ) {
      if ( S_ISDIR( buf.st_mode ) ) {
	return worker_rmdir( pathname );
      } else {
	return worker_unlink( pathname );
      }
    }
  }
  
  errno = EFAULT;
  return -1;
}

int worker_myislocal( const char *pathname )
{
  if ( pathname != NULL ) {
    return 1;
  }
  
  errno = EFAULT;
  return -1;
}

/*
 * Return the directory where worker should keep its temporary files.
 * This directory is (in Bourne shell terms) "${TMPDIR=/tmp}-$USER"
 * When called the first time, the directory is created if needed.
 * The first call should be done early, since we are using fprintf()
 * and not message() to report possible problems.
 */
#define TMPDIR_DEFAULT "/tmp"
const char *
worker_tmpdir (void)
{
    static char buffer[64];
    static const char *tmpdir = NULL;
    const char *sys_tmp;
    struct passwd *pwd;
    struct stat st;
    const char *error = NULL;

    /* Check if already initialized */
    if (tmpdir)
	return tmpdir;

    sys_tmp = getenv ("TMPDIR");
    if (!sys_tmp) {
	sys_tmp = TMPDIR_DEFAULT;
    }

    pwd = getpwuid (getuid ());
    snprintf (buffer, sizeof (buffer), "%s/worker-%s", sys_tmp,
	      pwd->pw_name);

    if (lstat (buffer, &st) == 0) {
	/* Sanity check for existing directory */
	if (!S_ISDIR (st.st_mode))
	    error = "%s is not a directory\n";
	else if (st.st_uid != getuid ())
	    error = "Directory %s is not owned by you\n";
	else if (((st.st_mode & 0777) != 0700)
		 && (chmod (buffer, 0700) != 0))
	    error = "Cannot set correct permissions for directory %s\n";
    } else {
	/* Need to create directory */
	if (mkdir (buffer, S_IRWXU) != 0) {
	    fprintf (stderr,
		     "Cannot create temporary directory %s: %s\n",
		     buffer, strerror (errno));
	    error = "";
	}
    }

    if (error) {
        if ( *error )
            fprintf (stderr, error, buffer);
        return NULL;
    }
    tmpdir = buffer;
    return tmpdir;
}
#undef TMPDIR_DEFAULT
