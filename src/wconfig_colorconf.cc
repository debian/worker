/* wconfig_colorconf.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_colorconf.hh"
#include "wconfig.h"
#include "wc_color.hh"
#include "worker.h"
#include "worker_locale.h"
#include "simplelist.hh"
#include "aguix/stringgadget.h"
#include "aguix/button.h"

ColorConfPanel::ColorConfPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
  tempconfig = _baseconfig.duplicate();
  _last_color_size = tempconfig->getColors()->size();
}

ColorConfPanel::~ColorConfPanel()
{
  delete tempconfig;
}

int ColorConfPanel::create()
{
  Panel::create();

  char buf[ A_BYTESFORNUMBER( int ) ];

  sprintf( buf, "%d", _baseconfig.getColors()->size() );

  AContainer *ac1 = setContainer( new AContainer( this, 1, 4 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 670 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 14 ) ),
              0, 0, AContainer::CO_FIX );
  
  sg = (StringGadget*)ac1_1->add( new StringGadget( _aguix, 0, 0, 30, buf, 0 ),
                                  1, 0, AContainer::CO_INCW );
  sg->connect( this );

  addMultiLineText( catalog.getLocale( 1329 ),
                    *ac1,
                    0, 2,
                    NULL, NULL );

  AContainer *ac1_2 = ac1->add( new AContainer( this, 3, 1 ), 0, 3 );
  ac1_2->setBorderWidth( 0 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( 5 );

  m_lightb = ac1_2->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 1330 ), 0 ),
                               0, 0, AContainer::CO_FIX );
  m_dark1_b = ac1_2->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 1331 ), 0 ),
                                1, 0, AContainer::CO_FIX );
  m_dark2_b = ac1_2->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 1566 ), 0 ),
                                2, 0, AContainer::CO_FIX );
  m_lightb->connect( this );
  m_dark1_b->connect( this );
  m_dark2_b->connect( this );

  contMaximize( true );
  return 0;
}

int ColorConfPanel::saveValues()
{
  _baseconfig.setColors( tempconfig->getColors() );
  return 0;
}

void ColorConfPanel::run( Widget *elem, const AGMessage &msg )
{
  List *colors = tempconfig->getColors();

  if ( msg.type == AG_STRINGGADGET_DEACTIVATE ) {
    if ( msg.stringgadget.sg == sg ) {
      int val = 0;
      sscanf( sg->getText(), "%d", &val );
      if ( ( val >= 8 ) && ( val <= 256 ) ) {
        while ( colors->size() < val ) colors->addElement( new WC_Color() );
        while ( colors->size() > val ) colors->removeLastElement();
        tempconfig->applyColorList( colors );
      } else {
        char *tstr = (char*)_allocsafe( A_BYTESFORNUMBER( int ) );
        sprintf( tstr, "%d", colors->size() );
        sg->setText( tstr );
        _freesafe( tstr );
      }
    }
  } else if ( msg.type == AG_BUTTONCLICKED ) {
      if ( msg.button.button == m_lightb ) {
          applyLightTheme();
      } else if ( msg.button.button == m_dark1_b ) {
          applyDarkTheme( 0 );
      } else if ( msg.button.button == m_dark2_b ) {
          applyDarkTheme( 1 );
      }
  }
}

void ColorConfPanel::hide()
{
  int color_size = tempconfig->getColors()->size();
  if ( color_size != _last_color_size ) {
    if ( _conf_cb != NULL )
      _conf_cb->setColors( tempconfig->getColors() );
    _last_color_size = color_size;
  }
  WConfigPanel::hide();
}

WConfigPanel::panel_action_t ColorConfPanel::setColors( List *colors )
{
  if ( colors != tempconfig->getColors() ) {
    tempconfig->setColors( colors );
  }

  return PANEL_NOACTION;
}

void ColorConfPanel::applyLightTheme()
{
    List *cols = new List();
    
    cols->addElement( new WC_Color( 180, 180, 180 ) );
    cols->addElement( new WC_Color( 0, 0, 0 ) );
    cols->addElement( new WC_Color( 255, 255, 255 ) );
    cols->addElement( new WC_Color( 0, 85, 187 ) );
    cols->addElement( new WC_Color( 204, 34, 0 ) );
    cols->addElement( new WC_Color( 50, 180, 20 ) );
    cols->addElement( new WC_Color( 119, 0, 119 ) );
    cols->addElement( new WC_Color( 238, 170, 68 ) );

    FaceDB new_faces;

    // default is already configured for light theme

    applyTheme( cols, &new_faces );
}

void ColorConfPanel::applyDarkTheme( int variant )
{
    List *cols = new List();

    if ( variant == 1 ) {
        cols->addElement( new WC_Color( 46, 52, 54 ) );
        //cols->addElement( new WC_Color( 236, 234, 201 ) );
        //cols->addElement( new WC_Color( 246, 233, 218 ) );
        cols->addElement( new WC_Color( 236, 234, 201 ) );
        cols->addElement( new WC_Color( 150, 150, 150 ) );

        cols->addElement( new WC_Color( 72, 107, 127 ) );
        cols->addElement( new WC_Color( 187, 71, 79 ) );
        cols->addElement( new WC_Color( 50, 180, 20 ) );
        cols->addElement( new WC_Color( 113, 63, 82 ) );
        cols->addElement( new WC_Color( 207, 126, 0 ) );
        cols->addElement( new WC_Color( 115, 153, 220 ) );
        cols->addElement( new WC_Color( 51, 38, 47 ) );
        cols->addElement( new WC_Color( 68, 71, 96 ) );
    } else {
        cols->addElement( new WC_Color( 40, 40, 40 ) );
        cols->addElement( new WC_Color( 200, 200, 200 ) );
        cols->addElement( new WC_Color( 255, 255, 255 ) );
        cols->addElement( new WC_Color( 2, 105, 207 ) );
        cols->addElement( new WC_Color( 204, 34, 0 ) );
        cols->addElement( new WC_Color( 50, 180, 20 ) );
        cols->addElement( new WC_Color( 119, 0, 119 ) );
        cols->addElement( new WC_Color( 207, 126, 0 ) );
        cols->addElement( new WC_Color( 50, 135, 242 ) );
        cols->addElement( new WC_Color( 0, 0, 0 ) );
        cols->addElement( new WC_Color( 70, 70, 70 ) );
    }

    FaceDB new_faces;

    if ( variant == 1 ) {
        new_faces.setColor( "clockbar-fg", 1 );
        new_faces.setColor( "statusbar-fg", 1 );
        new_faces.setColor( "3d-dark", 9 );
        new_faces.setColor( "3d-bright", 10 );
        new_faces.setColor( "button-bg", 10 );
        new_faces.setColor( "listview-active-fg", 0 );
        new_faces.setColor( "listview-select-fg", 0 );
        new_faces.setColor( "listview-sort-indicator", 1 );
        new_faces.setColor( "stringgadget-active-fg", 0 );
        new_faces.setColor( "stringgadget-normal-bg", 0 );
        new_faces.setColor( "stringgadget-selection-bg", 0 );
        new_faces.setColor( "lvb-inactive-fg", 1 );
        new_faces.setColor( "lvb-inactive-bg", 10 );
        new_faces.setColor( "lvb-active-fg", 1 );
        new_faces.setColor( "dirview-dir-select-fg", 1 );
        new_faces.setColor( "dirview-dir-select-bg", 8 );
        new_faces.setColor( "dirview-dir-selact-fg", 1 );
        new_faces.setColor( "dirview-dir-normal-fg", 8 );
        new_faces.setColor( "dirview-dir-normal-bg", 0 );
        new_faces.setColor( "dirview-file-select-fg", 9 );
        new_faces.setColor( "dirview-file-select-bg", 1 );
        new_faces.setColor( "dirview-dir-active-fg", 9 );
        new_faces.setColor( "dirview-dir-active-bg", 7 );
        new_faces.setColor( "dirview-file-active-fg", 9 );
        new_faces.setColor( "dirview-file-active-bg", 7 );
        new_faces.setColor( "dirview-file-selact-fg", 1 );
        new_faces.setColor( "request-fg", 1 );
        new_faces.setColor( "request-bg", 10 );
        new_faces.setColor( "dirview-header-fg", 1 );
        new_faces.setColor( "dirview-header-bg", 10 );
        new_faces.setColor( "textview-highlight-bg", 8 );
        new_faces.setColor( "textview-highlight-fg", 0 );
        new_faces.setColor( "textview-selection-fg", 3 );
    } else {
        new_faces.setColor( "3d-dark", 9 );
        new_faces.setColor( "button-bg", 10 );
        new_faces.setColor( "listview-active-fg", 0 );
        new_faces.setColor( "listview-select-fg", 0 );
        new_faces.setColor( "listview-sort-indicator", 1 );
        new_faces.setColor( "stringgadget-active-fg", 0 );
        new_faces.setColor( "stringgadget-selection-bg", 0 );
        new_faces.setColor( "lvb-inactive-fg", 1 );
        new_faces.setColor( "lvb-inactive-bg", 10 );
        new_faces.setColor( "dirview-dir-select-fg", 2 );
        new_faces.setColor( "dirview-dir-select-bg", 8 );
        new_faces.setColor( "dirview-dir-normal-fg", 8 );
        new_faces.setColor( "dirview-dir-normal-bg", 0 );
        new_faces.setColor( "dirview-file-select-fg", 9 );
        new_faces.setColor( "dirview-file-select-bg", 1 );
        new_faces.setColor( "dirview-dir-active-fg", 9 );
        new_faces.setColor( "dirview-dir-active-bg", 7 );
        new_faces.setColor( "dirview-file-active-fg", 9 );
        new_faces.setColor( "dirview-file-active-bg", 7 );
        new_faces.setColor( "request-fg", 1 );
        new_faces.setColor( "request-bg", 10 );
        new_faces.setColor( "dirview-header-fg", 1 );
        new_faces.setColor( "dirview-header-bg", 10 );
    }

    applyTheme( cols, &new_faces );
}

void ColorConfPanel::applyTheme( List *cols,
                                 FaceDB *new_faces )
{
    tempconfig->setColors( cols );
    tempconfig->applyColorList( cols );
    _conf_cb->setColors( cols );

    std::string tstr = AGUIXUtils::formatStringToString( "%d", cols->size() );
    sg->setText( tstr.c_str() );

    int id = cols->initEnum();
    WC_Color *tcol = (WC_Color*)cols->getFirstElement( id );
    int pos = 0;
    while ( tcol != NULL ) {
        _aguix->changeColor( pos, tcol->getRed(), tcol->getGreen(), tcol->getBlue() );
        delete tcol;
        tcol = (WC_Color*)cols->getNextElement( id );
        pos++;
    }
    cols->closeEnum( id );
    delete cols;

    _conf_cb->setFaceDB( *new_faces );

    _aguix->applyFaces( *new_faces );

    redraw();
}
