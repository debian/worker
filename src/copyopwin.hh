/* copyopwin.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COPYOPWIN_HH
#define COPYOPWIN_HH

#include "wdefines.h"
#include "aguix/request.h"
#include "aguix/message.h"
#include <mutex>
#include <condition_variable>
#include <list>
#include <memory>
#include "bgcopy_requestmessage.hh"
#include <thread>
#include "filenameshrinker.hh"

class AGUIX;
class AWindow;
class Text;
class SolidButton;
class BevelBox;
class Button;
class AFontWidth;
class ChooseButton;

class CopyOpWin
{
public:
  CopyOpWin( AGUIX*, bool _move = false );
  ~CopyOpWin();
  CopyOpWin( const CopyOpWin &other );
  CopyOpWin &operator=( const CopyOpWin &other );

    /*********************************
     * allowed for master thread only
     *********************************/

    int open();
    void close();

    /* should be called in copy-loop
     * does the update of the window AND process X-events
     * returnvalues: 0 - normal exit
     *               1 - cancel/close window pressed */ 
    int redraw( bool skip_message_handling = false );
    int handleMessage( AGMessage *msg );

    void master_process_request();

    void hide_detach_button();

    bool getKeepWindow() const;
    void setFinished();

    /***************************
     * allowed for slave thread
     ***************************/

    void starttimer(); // called once to set timer
    void stoptimer();  // when user-action is needed
    void conttimer();  // when user-interaction finished
    // called once:
    void set_files_to_copy( long nfiles );
    void set_dirs_to_copy( long ndirs );
    void set_bytes_to_copy( loff_t nbytes );
    // calld after newfile()
    void set_bytes_to_copy_curfile( loff_t nbytes );
  
    void dir_finished();
    void file_finished();
    // called when a block from the current file is copied
    void add_curbytes_copied( loff_t nbytes );
    // called when start a new file
    void newfile( const std::string &name,
                  const std::string &ndest);
    // for setting a message in the two lines used for display copy source/dest
    void setmessage( const std::string &msg, int line );
    // for dec file/dir-counter (mainly in case of skiped files/dirs
    void dec_file_counter( unsigned long f );
    void dec_dir_counter( unsigned long d );
    void dec_byte_counter( loff_t b );
  
    void file_skipped( loff_t byteSum, bool subCopied );

    int request( const std::string &title,
                 const std::string &text,
                 const std::string &buttons,
                 Requester::request_flags_t flags = Requester::REQUEST_NONE );
    int string_request( const std::string &title,
                        const std::string &lines,
                        const std::string &default_str,
                        const std::string &buttons,
                        std::string &return_str,
                        Requester::request_flags_t flags = Requester::REQUEST_NONE );
    void update_file_status();
protected:
    std::string source;
    std::string dest;
  loff_t copied_bytes_curfile,bytes_curfile;
  loff_t copied_bytes,bytes;
  long copied_files,files;
  long copied_dirs,dirs;
  struct timeval starttime;
  struct timeval filestarttime;
  struct timeval stoptime;
  struct timeval lasttime;
  struct timeval lasttime_global;
  struct timeval last_update_time;
  int skipped_updates;
  int sbw;
  loff_t lastbytes;
  double lastrate;
  loff_t lastbytes_global;
  double lastrate_global;
  
  double mintime;
  
  AGUIX *aguix;
  AWindow *window;
  Text *sourcetext,*desttext,*filetext,*globtext,*files2gotext,*dirs2gotext,*timetext,*fileavgtext;
  SolidButton *fsb,*gsb;
  BevelBox *bb1, *bb2, *bb3, *bb4;
  Button *cb;
  Button *m_detach_b;
  bool update_sourcetext,update_desttext,update_filetext,update_globtext,update_files2gotext;
  bool update_dirs2gotext;
  int melw;
  int filenamespace[2];
  bool ismessage[2];
  
/* currently displayed infos:
   1.source (only filename)
   2.destination (whole path)
   3.progressbar for current file
   4.<copied Bytes>/<bytes> @ <rate>
   5.files 2 copy
   6.dirs 2 copy
   7.progressbar for the whole operation
   8.<copied Bytes>/<bytes> @ <rate>
*/
  int glob_percent;
  bool move;
  AFontWidth *lencalc;

  bool timer_stopped;

  static const int UPDATES_PER_SECOND = 15;

    std::recursive_mutex m_status_update_lock;

    std::unique_ptr< BGCopyRequestMessage > m_request;
    std::unique_ptr< BGCopyRequestMessage > m_response;
    std::mutex m_request_lock;
    std::mutex m_response_lock;
    std::condition_variable m_response_cond;

    std::thread::id m_master_thread_id;

    int request_int( const char *title,
                     const char *text,
                     const char *buttons,
                     Requester::request_flags_t flags = Requester::REQUEST_NONE );
    int string_request_int( const char *title,
                            const char *lines,
                            const char *default_str,
                            const char *buttons,
                            char **return_str,
                            Requester::request_flags_t flags = Requester::REQUEST_NONE );

    void process_request( BGCopyRequestMessage &msg );

    void push_request( const BGCopyRequestMessage &msg );
    BGCopyRequestMessage wait_for_response();

    bool m_finished;
    bool m_detached;
    bool m_keep_window;

    ChooseButton *m_keep_window_cb;
    FileNameShrinker m_fns;
};

#endif
