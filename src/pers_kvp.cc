/* pers_kvp.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "pers_kvp.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include "nwc_fsentry.hh"
#include <algorithm>
#include "aguix/util.h"

PersistentKVP::PersistentKVP( const std::string &filename ) : m_filename( filename ),
                                                              m_lastmod( 0 ),
                                                              m_lastsize( 0 )
{
}

void PersistentKVP::read()
{
    NWC::FSEntry fe( m_filename );

    if ( ! fe.entryExists() ) return;

    if ( fe.isLink() ) {
        if ( fe.stat_dest_lastmod() == m_lastmod &&
             fe.stat_dest_size() == m_lastsize ) return;
    } else {
        if ( fe.stat_lastmod() == m_lastmod &&
             fe.stat_size() == m_lastsize ) return;
    }

    std::ifstream ifile( m_filename.c_str() );
    std::string line;

    m_elements.clear();
    
    if ( fe.isLink() ) {
        m_lastmod = fe.stat_dest_lastmod();
        m_lastsize = fe.stat_dest_size();
    } else {
        m_lastmod = fe.stat_lastmod();
        m_lastsize = fe.stat_size();
    }

    if ( ifile.is_open() ) {
        while ( std::getline( ifile, line ) ) {
            std::vector< std::string > v;

            AGUIXUtils::split_string( v, line, '=' );

            if ( v.size() == 2 ) {
                std::string key = v[0];
                int value;

                AGUIXUtils::strip( key );

                if ( AGUIXUtils::convertFromString( v[1], value ) ) {
                    m_elements[key] = value;
                }
            }
        }
    }
}

void PersistentKVP::write()
{
    std::ofstream ofile( m_filename.c_str() );

    for ( auto &p : m_elements ) {
        ofile << p.first << " = " << p.second << std::endl;
    }
}

int PersistentKVP::getIntValue( const std::string &key )
{
    read();
    if ( m_elements.count( key ) > 0 ) return m_elements[key];
    return 0;
}

int PersistentKVP::getIntValue( const std::string &key,
                                int min,
                                int max )
{
    int v = getIntValue( key );

    if ( v < min ) return min;
    if ( v > max ) return max;
    return v;
}

void PersistentKVP::setIntValue( const std::string &key,
                                 int value )
{
    read();
    if ( m_elements.count( key ) == 0 ||
         m_elements[key] != value ) {
        m_elements[key] = value;
        write();
    }
}
