/* dirfiltersettings.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DIRFILTERSETTINGS_HH
#define DIRFILTERSETTINGS_HH

#include "wdefines.h"
#include <list>
#include <memory>
#include "stringmatcher.hh"
#include "nmfilter.hh"
#include "genericdirectoryfilter.hh"
#include "nwc_fsentry.hh"

class DirFilterSettings
{
public:
    DirFilterSettings();
    DirFilterSettings( const DirFilterSettings &other );
    ~DirFilterSettings();
    
    bool check( const std::string &dirname,
                class NWCEntrySelectionState &e );
    bool check( const std::string &dirname,
                const NWC::FSEntry *fse );

    int getSerialNr() const;

    void setShowHidden( bool nv );
    void setFilters( const std::list<NM_Filter> &filters );

    bool getShowHidden() const;
    std::list<NM_Filter> getFilters() const;

    bool filterActive() const;
    void unsetAllFilters();
    void setFilter( const char *filter, NM_Filter::check_t mode );
    NM_Filter::check_t checkFilter( const char *filter );
    
    void setStringFilter( std::unique_ptr<StringMatcher> filter );

    void bookmarksChanged();

    void setHighlightBookmarkPrefix( bool nv );
    bool getHighlightBookmarkPrefix() const;

    typedef enum {
        SHOW_ALL,
        SHOW_ONLY_BOOKMARKS,
        SHOW_ONLY_LABEL
    } bookmark_filter_t;

    void setBookmarkFilter( bookmark_filter_t v );
    bookmark_filter_t getBookmarkFilter() const;

    void setSpecificBookmarkLabel( const std::string &l );
    const std::string &getSpecificBookmarkLabel() const;

    void setGenericFilter( std::unique_ptr< GenericDirectoryFilter > filter );
private:
    void calcInAndOuts();

    int _serial_nr;
    bool _show_hidden;
    std::list<NM_Filter> _filters;
    int _ins, _outs;

    std::unique_ptr<StringMatcher> _string_filter;

    bool m_highlight_bookmark_prefix;
    bookmark_filter_t m_bookmark_filter;
    std::string m_bookmark_label;

    std::unique_ptr< GenericDirectoryFilter > m_generic_filter;
};

#endif
