/* wconfig_button.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_BUTTON_HH
#define WCONFIG_BUTTON_HH

#include "wdefines.h"
#include "wconfig_panel.hh"

class WConfig;
class Button;
class CycleButton;
class SolidButton;

class ButtonPanel : public WConfigPanel
{
public:
  ButtonPanel( AWindow &basewin, WConfig &baseconfig );
  ~ButtonPanel();
  int create();
  int saveValues();

  /* gui elements callback */
  void run( Widget *, const AGMessage &msg ) override;

  /* we are interested in updated colors */
  panel_action_t setColors( List *colors );
  panel_action_t setRows( int r );
  panel_action_t setColumns( int c );
  int addButtons( List *buttons, WConfigPanelCallBack::add_action_t action );
  int addHotkeys( List *hotkeys, WConfigPanelCallBack::add_action_t action );

    bool escapeInUse() override;
protected:
  void showButtonBank();
  void updateBankStr();
    void setMode( int m );

  int banknr, maxbank;
  Button ***bbs;
  unsigned int rows;
  unsigned int columns;
  Button *nextbb, *prevbb, *newbb, *delbb, *swapnextb, *swapprevb, *copyb, *swapb, *delb;
  CycleButton *cyb;
  SolidButton *sbsb;
  Text *bnrt, *maxbankt;
  bool extmode;
    int m_mode;
  int selindex;
  int banksize;

    bool m_escape_active = false;
};
 
#endif
