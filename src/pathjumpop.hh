/* pathjumpop.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2012-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PATHJUMPOP_HH
#define PATHJUMPOP_HH

#include "wdefines.h"
#include "functionproto.h"
#include <string>
#include <list>

class PathJumpOp : public FunctionProto
{
public:
    PathJumpOp();
    ~PathJumpOp();
    PathJumpOp( const PathJumpOp &other );
    PathJumpOp &operator=( const PathJumpOp &other );
    
    PathJumpOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;
    
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save(Datei*) override;
    int configure() override;

    typedef enum {
        SHOW_BY_TIME = 0,
        SHOW_BY_FILTER,
        SHOW_BY_PROGRAM
    } initial_tab_t;

    typedef enum {
        SHOW_ALL = 0,
        SHOW_SUBDIRS,
        SHOW_DIRECT_SUBDIRS
    } display_mode_t;

    void setInitialTab( initial_tab_t v );
    void setDisplayMode( display_mode_t v );
    void setIncludeAllData( bool v );
    void setHideNonExisting( bool v );
    void setLockOnCurrentDir( bool v );

    static const char *name;
private:
    // Infos to save
    initial_tab_t m_initial_tab = SHOW_BY_TIME;
    display_mode_t m_display_mode = SHOW_ALL;
    bool m_include_all_data = false;
    bool m_hide_non_existing = false;
    bool m_lock_on_current_dir = false;
};

#endif
