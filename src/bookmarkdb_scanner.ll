%option prefix="bmdb"
%option nounput
%option warn nodefault
%option yylineno
/*%option outfile="bookmarkdb_scanner.cc"*/
%option outfile="lex.yy.c"
%top{
/* scanner for bookmarkdb config file (flex generated)
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2007-2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#define BMDB_HEADER_NO_PROTOS
#include "bookmarkdb_scanner.hh"
}

%{
static void bmdb_readcomment();
static void bmdb_removebackslash(char *, char *);

#define YY_NO_UNPUT
int bmdb_linenr = 0;

int bmdb_token = 0;
std::string bmdb_token_str;

%}

delim	[ \t]
ws	{delim}+

%%
\/\*	{ bmdb_readcomment(); }
\"([^"\n\\]|\\.|\\\n)*\"	{ bmdb_removebackslash( bmdbtext, bmdbtext ); bmdb_token_str = bmdbtext;
				  return BMDB_STRING; }
{ws}	{;}
yes	{ return BMDB_TRUE; }
no	{ return BMDB_FALSE; }
true	{ return BMDB_TRUE; }
false	{ return BMDB_FALSE; }
entry   { return BMDB_ENTRY; }
name    { return BMDB_NAME; }
alias    { return BMDB_ALIAS; }
useparent { return BMDB_USEPARENT; }
bookmarks { return BMDB_BOOKMARKS; }
category    { return BMDB_CATEGORY; }
\n	{ bmdb_linenr++; }
.	{ return bmdbtext[0]; }
%%

#ifndef bmdbwrap
int bmdbwrap()
{
  return 1;
}
#endif

static void bmdb_readcomment()
{
    /* liest Kommentar bis zum schliessenden Ende, egal in welcher Zeile */
    int ch;
    int mode = 0;
    do {
        ch = yyinput();
        if ( ch == '\n' ) bmdb_linenr++;
        switch ( mode ) {
          case 1:
              if ( ch == '/' ) mode = 2;
              else if ( ch != '*' ) mode = 0;
              break;
          default:
              if ( ch == '*' ) mode = 1;
              break;
        }
    } while ( mode != 2 );
}

static void bmdb_removebackslash( char *src, char *dest )
{
    /* Entfernt die Backslash-Sequencen, alle geschuetzten Zeichen werden einge-
       fuegt, ausser dem Return */
    src++;
    while ( *src != '"' ) {
        if ( ( *src ) == '\\' ) {
            src++;
            if ( *src == '\n' ) {
                src++;
                bmdb_linenr++;
            } else *dest++ = *src++;
        } else *dest++ = *src++;
    }
    *dest = '\0';
}

void bmdb_readtoken()
{
    bmdb_token = bmdblex();
}
