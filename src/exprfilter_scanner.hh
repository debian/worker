/* exprfilter_scanner.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EXPRFILTER_SCANNER_HH
#define EXPRFILTER_SCANNER_HH

#include "wdefines.h"
#include <list>
#include <string>
#include "aguix/util.h"

class ExprToken
{
public:
    typedef enum {
        NONE,
        WORD,
        NUMBER,
        LT,
        LE,
        EQ,
        NE,
        GE,
        GT,
        APPROX,
        AND,
        OR,
        OPEN_PAR,
        CLOSE_PAR,
        DATE,
    } expr_token_t;

    ExprToken( const expr_token_t type,
               const std::string &value ) : m_type( type ),
                                            m_value( value )
    {}

    ExprToken( const expr_token_t type ) : m_type( type )
    {}

    bool operator==( expr_token_t v )
    {
        if ( m_type == v ) return true;

        return false;
    }

    bool operator!=( expr_token_t v )
    {
        return ! operator==( v );
    }

    const std::string &getValue() const
    {
        return m_value;
    }

    void convertOptions()
    {
        m_options.clear();
        AGUIXUtils::split_string( m_options, m_value, ',' );
        if ( m_options.size() > 1 ) {
            m_value = m_options.at( 0 );
            m_options.erase( m_options.begin() );
        } else {
            m_options.clear();
        }
    }

    const std::vector< std::string > &getOptions() const
    {
        return m_options;
    }
private:
    expr_token_t m_type;
    std::string m_value;
    std::vector< std::string > m_options;
};

int exprfilter_scan_expr( const std::string &input, std::list< ExprToken > &output_tokens );

#endif /* EXPR_PARSER_HH */
