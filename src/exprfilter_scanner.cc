#line 1 "exprfilter_scanner.rl"
/* exprfilter_scanner.rl
* This file belongs to Worker, a file manager for UN*X/X11.
* Copyright (C) 2014-2016 Ralf Hoffmann.
* You can contact me at: ralf@boomerangsworld.de
*   or http://www.boomerangsworld.de/worker
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
* to generate output, run
* ragel exprfilter_scanner.rl -o exprfilter_scanner.cc
*/

#include "exprfilter_scanner.hh"


#line 57 "exprfilter_scanner.rl"



#line 33 "exprfilter_scanner.cc"
static const signed char _expr_parser_actions[] = {
	0, 1, 0, 1, 1, 1, 2, 1,
	3, 1, 4, 1, 5, 1, 6, 1,
	7, 1, 8, 1, 9, 1, 10, 1,
	11, 1, 12, 1, 13, 1, 14, 1,
	15, 1, 16, 1, 17, 1, 18, 1,
	19, 1, 20, 1, 21, 1, 22, 0
};

static const signed char _expr_parser_key_offsets[] = {
	0, 0, 1, 2, 4, 6, 7, 9,
	11, 26, 37, 38, 40, 42, 44, 47,
	49, 50, 51, 52, 0
};

static const char _expr_parser_trans_keys[] = {
	61, 34, 48, 57, 48, 57, 45, 48,
	57, 48, 57, 32, 33, 34, 38, 40,
	41, 60, 61, 62, 124, 126, 9, 13,
	48, 57, 38, 124, 126, 9, 13, 32,
	33, 40, 41, 60, 62, 38, 48, 57,
	48, 57, 48, 57, 45, 48, 57, 48,
	57, 61, 61, 61, 124, 0
};

static const signed char _expr_parser_single_lengths[] = {
	0, 1, 1, 0, 0, 1, 0, 0,
	11, 3, 1, 0, 0, 0, 1, 0,
	1, 1, 1, 1, 0
};

static const signed char _expr_parser_range_lengths[] = {
	0, 0, 0, 1, 1, 0, 1, 1,
	2, 4, 0, 1, 1, 1, 1, 1,
	0, 0, 0, 0, 0
};

static const signed char _expr_parser_index_offsets[] = {
	0, 0, 2, 4, 6, 8, 10, 12,
	14, 28, 36, 38, 40, 42, 44, 47,
	49, 51, 53, 55, 0
};

static const signed char _expr_parser_cond_targs[] = {
	8, 0, 8, 2, 4, 8, 5, 8,
	6, 8, 7, 8, 8, 8, 8, 1,
	2, 10, 8, 8, 16, 17, 18, 19,
	8, 8, 11, 9, 8, 8, 8, 8,
	8, 8, 8, 9, 8, 8, 12, 8,
	13, 8, 14, 8, 3, 15, 8, 15,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 0, 1, 2, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 0
};

static const signed char _expr_parser_cond_actions[] = {
	11, 0, 27, 0, 0, 45, 0, 45,
	0, 45, 0, 45, 25, 45, 29, 0,
	0, 0, 21, 23, 0, 0, 0, 0,
	15, 29, 0, 0, 41, 41, 41, 41,
	41, 41, 41, 0, 17, 37, 0, 43,
	0, 43, 5, 43, 0, 0, 43, 0,
	43, 7, 31, 9, 33, 13, 35, 19,
	39, 0, 0, 0, 45, 45, 45, 45,
	45, 0, 41, 37, 43, 43, 43, 43,
	43, 31, 33, 35, 39, 0
};

static const signed char _expr_parser_to_state_actions[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0
};

static const signed char _expr_parser_from_state_actions[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	3, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0
};

static const signed char _expr_parser_eof_trans[] = {
	58, 59, 60, 61, 62, 63, 64, 65,
	66, 67, 68, 69, 70, 71, 72, 73,
	74, 75, 76, 77, 0
};

static const int expr_parser_start = 8;
static const int expr_parser_error = 0;

static const int expr_parser_en_main = 8;


#line 59 "exprfilter_scanner.rl"


int exprfilter_scan_expr( const std::string &input, std::list< ExprToken > &output_tokens )
{
	const char *p = input.c_str();
	const char *pe = p + input.length();
	const char *eof = pe;
	const char *ts = NULL, *te = NULL;
	int cs = expr_parser_start;
	

#line 136 "exprfilter_scanner.cc"
	{
		int _klen;
		unsigned int _trans = 0;
		const char * _keys;
		const signed char * _acts;
		unsigned int _nacts;
		_resume: {}
		if ( p == pe && p != eof )
			goto _out;
		_acts = ( _expr_parser_actions + (_expr_parser_from_state_actions[cs]));
		_nacts = (unsigned int)(*( _acts));
		_acts += 1;
		while ( _nacts > 0 ) {
			switch ( (*( _acts)) ) {
				case 1:  {
						{
#line 1 "NONE"
						{ts = p;}}
					
#line 155 "exprfilter_scanner.cc"

					break; 
				}
			}
			_nacts -= 1;
			_acts += 1;
		}
		
		if ( p == eof ) {
			if ( _expr_parser_eof_trans[cs] > 0 ) {
				_trans = (unsigned int)_expr_parser_eof_trans[cs] - 1;
			}
		}
		else {
			_keys = ( _expr_parser_trans_keys + (_expr_parser_key_offsets[cs]));
			_trans = (unsigned int)_expr_parser_index_offsets[cs];
			
			_klen = (int)_expr_parser_single_lengths[cs];
			if ( _klen > 0 ) {
				const char *_lower = _keys;
				const char *_upper = _keys + _klen - 1;
				const char *_mid;
				while ( 1 ) {
					if ( _upper < _lower ) {
						_keys += _klen;
						_trans += (unsigned int)_klen;
						break;
					}
					
					_mid = _lower + ((_upper-_lower) >> 1);
					if ( ( (*( p))) < (*( _mid)) )
						_upper = _mid - 1;
					else if ( ( (*( p))) > (*( _mid)) )
						_lower = _mid + 1;
					else {
						_trans += (unsigned int)(_mid - _keys);
						goto _match;
					}
				}
			}
			
			_klen = (int)_expr_parser_range_lengths[cs];
			if ( _klen > 0 ) {
				const char *_lower = _keys;
				const char *_upper = _keys + (_klen<<1) - 2;
				const char *_mid;
				while ( 1 ) {
					if ( _upper < _lower ) {
						_trans += (unsigned int)_klen;
						break;
					}
					
					_mid = _lower + (((_upper-_lower) >> 1) & ~1);
					if ( ( (*( p))) < (*( _mid)) )
						_upper = _mid - 2;
					else if ( ( (*( p))) > (*( _mid + 1)) )
						_lower = _mid + 2;
					else {
						_trans += (unsigned int)((_mid - _keys)>>1);
						break;
					}
				}
			}
			
			_match: {}
		}
		cs = (int)_expr_parser_cond_targs[_trans];
		
		if ( _expr_parser_cond_actions[_trans] != 0 ) {
			
			_acts = ( _expr_parser_actions + (_expr_parser_cond_actions[_trans]));
			_nacts = (unsigned int)(*( _acts));
			_acts += 1;
			while ( _nacts > 0 ) {
				switch ( (*( _acts)) )
				{
					case 2:  {
							{
#line 1 "NONE"
							{te = p+1;}}
						
#line 236 "exprfilter_scanner.cc"

						break; 
					}
					case 3:  {
							{
#line 38 "exprfilter_scanner.rl"
							{te = p+1;{
#line 38 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::LE, s1 ) ); }
							}}
						
#line 247 "exprfilter_scanner.cc"

						break; 
					}
					case 4:  {
							{
#line 40 "exprfilter_scanner.rl"
							{te = p+1;{
#line 40 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::EQ, s1 ) ); }
							}}
						
#line 258 "exprfilter_scanner.cc"

						break; 
					}
					case 5:  {
							{
#line 41 "exprfilter_scanner.rl"
							{te = p+1;{
#line 41 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::NE, s1 ) ); }
							}}
						
#line 269 "exprfilter_scanner.cc"

						break; 
					}
					case 6:  {
							{
#line 42 "exprfilter_scanner.rl"
							{te = p+1;{
#line 42 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::GE, s1 ) ); }
							}}
						
#line 280 "exprfilter_scanner.cc"

						break; 
					}
					case 7:  {
							{
#line 44 "exprfilter_scanner.rl"
							{te = p+1;{
#line 44 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::APPROX, s1 ) ); }
							}}
						
#line 291 "exprfilter_scanner.cc"

						break; 
					}
					case 8:  {
							{
#line 47 "exprfilter_scanner.rl"
							{te = p+1;{
#line 47 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::AND, s1 ) ); }
							}}
						
#line 302 "exprfilter_scanner.cc"

						break; 
					}
					case 9:  {
							{
#line 48 "exprfilter_scanner.rl"
							{te = p+1;{
#line 48 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::OR, s1 ) ); }
							}}
						
#line 313 "exprfilter_scanner.cc"

						break; 
					}
					case 10:  {
							{
#line 49 "exprfilter_scanner.rl"
							{te = p+1;{
#line 49 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::OPEN_PAR, s1 ) ); }
							}}
						
#line 324 "exprfilter_scanner.cc"

						break; 
					}
					case 11:  {
							{
#line 50 "exprfilter_scanner.rl"
							{te = p+1;{
#line 50 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::CLOSE_PAR, s1 ) ); }
							}}
						
#line 335 "exprfilter_scanner.cc"

						break; 
					}
					case 12:  {
							{
#line 51 "exprfilter_scanner.rl"
							{te = p+1;{
#line 51 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::DATE, s1 ) ); }
							}}
						
#line 346 "exprfilter_scanner.cc"

						break; 
					}
					case 13:  {
							{
#line 53 "exprfilter_scanner.rl"
							{te = p+1;{
#line 53 "exprfilter_scanner.rl"
									if ( te - ts >= 2 ) { std::string s1( ts + 1, te - ts - 2 ); output_tokens.push_back( ExprToken( ExprToken::WORD, s1 ) ); } }
							}}
						
#line 357 "exprfilter_scanner.cc"

						break; 
					}
					case 14:  {
							{
#line 1 "-"
							{te = p+1;}}
						
#line 365 "exprfilter_scanner.cc"

						break; 
					}
					case 15:  {
							{
#line 37 "exprfilter_scanner.rl"
							{te = p;p = p - 1;{
#line 37 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::LT, s1 ) ); }
							}}
						
#line 376 "exprfilter_scanner.cc"

						break; 
					}
					case 16:  {
							{
#line 39 "exprfilter_scanner.rl"
							{te = p;p = p - 1;{
#line 39 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::EQ, s1 ) ); }
							}}
						
#line 387 "exprfilter_scanner.cc"

						break; 
					}
					case 17:  {
							{
#line 43 "exprfilter_scanner.rl"
							{te = p;p = p - 1;{
#line 43 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::GT, s1 ) ); }
							}}
						
#line 398 "exprfilter_scanner.cc"

						break; 
					}
					case 18:  {
							{
#line 45 "exprfilter_scanner.rl"
							{te = p;p = p - 1;{
#line 45 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::AND, s1 ) ); }
							}}
						
#line 409 "exprfilter_scanner.cc"

						break; 
					}
					case 19:  {
							{
#line 46 "exprfilter_scanner.rl"
							{te = p;p = p - 1;{
#line 46 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::OR, s1 ) ); }
							}}
						
#line 420 "exprfilter_scanner.cc"

						break; 
					}
					case 20:  {
							{
#line 52 "exprfilter_scanner.rl"
							{te = p;p = p - 1;{
#line 52 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::WORD, s1 ) ); }
							}}
						
#line 431 "exprfilter_scanner.cc"

						break; 
					}
					case 21:  {
							{
#line 54 "exprfilter_scanner.rl"
							{te = p;p = p - 1;{
#line 54 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::NUMBER, s1 ) ); }
							}}
						
#line 442 "exprfilter_scanner.cc"

						break; 
					}
					case 22:  {
							{
#line 54 "exprfilter_scanner.rl"
							{p = ((te))-1;
								{
#line 54 "exprfilter_scanner.rl"
									std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::NUMBER, s1 ) ); }
							}}
						
#line 454 "exprfilter_scanner.cc"

						break; 
					}
				}
				_nacts -= 1;
				_acts += 1;
			}
			
		}
		
		if ( p == eof ) {
			if ( cs >= 8 )
				goto _out;
		}
		else {
			_acts = ( _expr_parser_actions + (_expr_parser_to_state_actions[cs]));
			_nacts = (unsigned int)(*( _acts));
			_acts += 1;
			while ( _nacts > 0 ) {
				switch ( (*( _acts)) ) {
					case 0:  {
							{
#line 1 "NONE"
							{ts = 0;}}
						
#line 479 "exprfilter_scanner.cc"

						break; 
					}
				}
				_nacts -= 1;
				_acts += 1;
			}
			
			if ( cs != 0 ) {
				p += 1;
				goto _resume;
			}
		}
		_out: {}
	}
	
#line 69 "exprfilter_scanner.rl"

	
	if (cs == expr_parser_error)
		return -1;
	
	return 0;
}
