/* chmodop.c
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "chmodop.h"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "datei.h"
#include "aguix/acontainer.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "aguix/cyclebutton.h"
#include "aguix/stringgadget.h"
#include "aguix/util.h"
#include "worker_locale.h"
#include "chmodorder.hh"
#include "virtualdirmode.hh"

const char *ChModOp::name="ChModOp";

ChModOp::ChModOp() : FunctionProto()
{
  onfiles = true;
  ondirs=false;
  recursive=false;
  hasConfigure = true;
    m_category = FunctionProto::CAT_FILEOPS;
    setRequestParameters( true );
}

ChModOp::~ChModOp()
{
}

ChModOp*
ChModOp::duplicate() const
{
  ChModOp *ta=new ChModOp();
  ta->onfiles=onfiles;
  ta->ondirs=ondirs;
  ta->recursive=recursive;
  ta->setRequestParameters( requestParameters() );
  ta->setApplyMode( m_apply_mode );
  ta->setPermissions( m_permissions );
  return ta;
}

bool
ChModOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
ChModOp::getName()
{
  return name;
}

int
ChModOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if(dynamic_cast< VirtualDirMode * > ( lm1 ) ) {
              normalmodechmod( msg );
          } else {
              lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

bool
ChModOp::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  fh->configPutPairBool( "onfiles", onfiles );
  fh->configPutPairBool( "ondirs", ondirs );
  fh->configPutPairBool( "recursive", recursive );
  fh->configPutPairBool( "requestflags", requestParameters() );

  switch ( m_apply_mode ) {
      case CHMOD_SET_PERMISSIONS:
          fh->configPutPair( "applyMode", "set" );
          break;
      case CHMOD_ADD_PERMISSIONS:
          fh->configPutPair( "applyMode", "add" );
          break;
      case CHMOD_REMOVE_PERMISSIONS:
          fh->configPutPair( "applyMode", "remove" );
          break;
      default:
          break;
  }

  if ( m_permissions != 0 ) {
      std::string perm_str = AGUIXUtils::formatStringToString( "0%o", m_permissions );
      fh->configPutPairString( "permission", perm_str.c_str() );
  }

  return true;
}

const char *
ChModOp::getDescription()
{
  return catalog.getLocale(1279);
}

int
ChModOp::normalmodechmod( ActionMessage *am )
{
  ListerMode *lm1=NULL;
  NM_specialsourceExt *specialsource = NULL;
  bool cont=true;
  struct NM_chmodorder cmorder;
  
  if(startlister==NULL) return 1;
  lm1=startlister->getActiveMode();
  if(lm1==NULL) return 1;

  if ( requestParameters() == true ) {
    if(doconfigure(1)!=0) cont=false;
  } else {
    // set values in t* variables
    tonfiles=onfiles;
    tondirs=ondirs;
    trecursive=recursive;
    m_tapply_mode = m_apply_mode;
    m_tpermissions = m_permissions;
  }
  
  if(cont==true) {
    memset( &cmorder, 0, sizeof( cmorder ) );
    // detect what to copy
    if(am->mode==am->AM_MODE_ONLYACTIVE)
      cmorder.source=cmorder.NM_ONLYACTIVE;
    else if(am->mode==am->AM_MODE_DNDACTION) {
      // insert DND-element into list
      cmorder.source=cmorder.NM_SPECIAL;
      cmorder.sources=new std::list<NM_specialsourceExt*>;
      specialsource = new NM_specialsourceExt( NULL );
      //TODO: specialsource nach am besetzen (je nachdem wir ich das realisiere)
      cmorder.sources->push_back( specialsource );
    } else if(am->mode==am->AM_MODE_SPECIAL) {
      cmorder.source=cmorder.NM_SPECIAL;
      cmorder.sources=new std::list<NM_specialsourceExt*>;
      specialsource = new NM_specialsourceExt( am->getFE() );
      cmorder.sources->push_back( specialsource );
    } else
      cmorder.source=cmorder.NM_ALLENTRIES;

    cmorder.onfiles=tonfiles;
    cmorder.ondirs=tondirs;
    cmorder.recursive=trecursive;

    cmorder.apply_mode = m_tapply_mode;
    cmorder.permissions = m_tpermissions;

    if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
        vdm->chmodf( &cmorder );
    }

    if(cmorder.source==cmorder.NM_SPECIAL) {
      if ( specialsource != NULL ) delete specialsource;
      delete cmorder.sources;
    }
  }
  return 0;
}

int
ChModOp::configure()
{
  return doconfigure(0);
}

static void setChooseButtonMatrixForMode( ChooseButton **mcb, mode_t mode )
{
    mcb[0]->setState( ( ( mode & S_IRUSR ) == 0 ) ? false : true );
    mcb[1]->setState( ( ( mode & S_IWUSR ) == 0 ) ? false : true );
    mcb[2]->setState( ( ( mode & S_IXUSR ) == 0 ) ? false : true );
    mcb[3]->setState( ( ( mode & S_IRGRP ) == 0 ) ? false : true );
    mcb[4]->setState( ( ( mode & S_IWGRP ) == 0 ) ? false : true );
    mcb[5]->setState( ( ( mode & S_IXGRP ) == 0 ) ? false : true );
    mcb[6]->setState( ( ( mode & S_IROTH ) == 0 ) ? false : true );
    mcb[7]->setState( ( ( mode & S_IWOTH ) == 0 ) ? false : true );
    mcb[8]->setState( ( ( mode & S_IXOTH ) == 0 ) ? false : true );
    mcb[9]->setState( ( ( mode & S_ISUID ) == 0 ) ? false : true );
    mcb[10]->setState( ( ( mode & S_ISGID ) == 0 ) ? false : true );
    mcb[11]->setState( ( ( mode & S_ISVTX ) == 0 ) ? false : true );
}

static mode_t getChooseButtonMatrixMode( ChooseButton **mcb )
{
    mode_t newmode = 0;
    newmode |= ( mcb[0]->getState() == true ) ? S_IRUSR : 0;
    newmode |= ( mcb[1]->getState() == true ) ? S_IWUSR : 0;
    newmode |= ( mcb[2]->getState() == true ) ? S_IXUSR : 0;
    newmode |= ( mcb[3]->getState() == true ) ? S_IRGRP : 0;
    newmode |= ( mcb[4]->getState() == true ) ? S_IWGRP : 0;
    newmode |= ( mcb[5]->getState() == true ) ? S_IXGRP : 0;
    newmode |= ( mcb[6]->getState() == true ) ? S_IROTH : 0;
    newmode |= ( mcb[7]->getState() == true ) ? S_IWOTH : 0;
    newmode |= ( mcb[8]->getState() == true ) ? S_IXOTH : 0;
    newmode |= ( mcb[9]->getState() == true ) ? S_ISUID : 0;
    newmode |= ( mcb[10]->getState() == true ) ? S_ISGID : 0;
    newmode |= ( mcb[11]->getState() == true ) ? S_ISVTX : 0;

    return newmode;
}

static void setOctalSGForMode( StringGadget *octal_sg, mode_t permissions )
{
    std::string s = AGUIXUtils::formatStringToString( "%o", permissions );

    octal_sg->setText( s.c_str() );
}

static mode_t getModeFromOctalSG( StringGadget *octal_sg )
{
    int mode;

    if ( sscanf( octal_sg->getText(), "%o", &mode ) == 1 ) {
        return (mode_t)mode;
    }
    return 0;
}

int
ChModOp::doconfigure(int mode)
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  ChooseButton *ofcb,*odcb,*rfcb=NULL,*rcb;
  AGMessage *msg;
  int endmode=-1;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr=(char*)_allocsafe(strlen(catalog.getLocale(293))+strlen(getDescription())+1);
  sprintf(tstr,catalog.getLocale(293),getDescription());
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);
  
  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 8 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  ac1->add( new Text( aguix, 0, 0, getDescription() ), 0, 0, cincwnr );

  ofcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( onfiles == true ) ? 1 : 0,
						    catalog.getLocale( 295 ), LABEL_RIGHT, 0 ), 0, 1, cincwnr );

  odcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( ondirs == true ) ? 1 : 0,
						    catalog.getLocale( 296 ), LABEL_RIGHT, 0 ), 0, 2, cincwnr );

  rcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( recursive == true ) ? 1 : 0,
						   catalog.getLocale( 297 ), LABEL_RIGHT, 0 ), 0, 3, cincwnr );

  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 4 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( 5 );
  ac1_2->setBorderWidth( 0 );

  ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 1400 ) ), 0, 0, AContainer::CO_FIX );
  auto mode_cycb = ac1_2->addWidget( new CycleButton( aguix, 0, 0, 50, 0 ),
                                     1, 0, AContainer::CO_INCW );

  mode_cycb->addOption( catalog.getLocale( 1401 ) );
  mode_cycb->addOption( catalog.getLocale( 1402 ) );
  mode_cycb->addOption( catalog.getLocale( 1403 ) );
  mode_cycb->addOption( catalog.getLocale( 1404 ) );

  switch ( m_apply_mode ) {
      case CHMOD_SET_PERMISSIONS:
          mode_cycb->setOption( 1 );
          break;
      case CHMOD_ADD_PERMISSIONS:
          mode_cycb->setOption( 2 );
          break;
      case CHMOD_REMOVE_PERMISSIONS:
          mode_cycb->setOption( 3 );
          break;
      default:
          mode_cycb->setOption( 0 );
          break;
  }
  mode_cycb->resize( mode_cycb->getMaxSize(),
                     mode_cycb->getHeight() );
  ac1_2->readLimits();

  AWindow *permwin = new AWindow( aguix, 10, 10, 100, 50, "" );
  ac1->add( permwin, 0, 5, AContainer::CO_MIN );

    AContainer *permwin_ac1 = permwin->setContainer( new AContainer( permwin, 1, 2 ), true );
    permwin_ac1->setMinSpace( 5 );
    permwin_ac1->setMaxSpace( 5 );
    
    AContainer *permwin_ac1_2 = permwin_ac1->add( new AContainer( permwin, 4, 6 ), 0, 0 );
    permwin_ac1_2->setMinSpace( 5 );
    permwin_ac1_2->setMaxSpace( 5 );
    
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 218 ) ), 1, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 219 ) ), 2, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 220 ) ), 3, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 214 ) ), 0, 1, AContainer::CO_FIX );
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 215 ) ), 0, 2, AContainer::CO_FIX );
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 216 ) ), 0, 3, AContainer::CO_FIX );
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 217 ) ), 0, 5, AContainer::CO_FIX );
    
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 221 ) ), 1, 4, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 222 ) ), 2, 4, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    permwin_ac1_2->add( new Text( aguix, 0, 0, catalog.getLocale( 223 ) ), 3, 4, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    
    ChooseButton *mcb[12];
    mcb[0] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 1, 1, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[1] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 2, 1, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[2] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 3, 1, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[3] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 1, 2, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[4] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 2, 2, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[5] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 3, 2, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[6] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 1, 3, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[7] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 2, 3, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[8] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 3, 3, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[9] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                         0,
                                                         "", LABEL_RIGHT, 0 ), 1, 5, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[10] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                          0,
                                                          "", LABEL_RIGHT, 0 ), 2, 5, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
    mcb[11] = permwin_ac1_2->addWidget( new ChooseButton( aguix, 0, 0,
                                                          0,
                                                          "", LABEL_RIGHT, 0 ), 3, 5, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );

    AContainer *permwin_ac1_3 = permwin_ac1->add( new AContainer( permwin, 2, 1 ), 0, 1 );
    permwin_ac1_3->setMinSpace( 5 );
    permwin_ac1_3->setMaxSpace( 5 );

    permwin_ac1_3->add( new Text( aguix, 0, 0, catalog.getLocale( 1405 ) ), 0, 0, AContainer::CO_FIX );
    auto *octal_sg = permwin_ac1_3->addWidget( new StringGadget( aguix, 0, 0, 50, "", 0 ),
                                               1, 0, AContainer::CO_INCW );

    setChooseButtonMatrixForMode( mcb, m_permissions );
    setOctalSGForMode( octal_sg, m_permissions );
    permwin->contMaximize( true );

    if ( m_apply_mode != CHMOD_ASK_PERMISSIONS ) {
        permwin->show();
    }

    ac1->readLimits();

  if(mode==0) {
    rfcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( requestParameters() == true ) ? 1 : 0,
						      catalog.getLocale( 294 ), LABEL_RIGHT, 0 ), 0, 6, cincwnr );
  }

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 7 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( -1 );
  ac1_1->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_1->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_1->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );
  
  okb->takeFocus();
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) endmode=0;
          else if(msg->button.button==cb) endmode=1;
          break;
        case AG_KEYPRESSED:
            if(win->isParent(msg->key.window,false)==true) {
                switch(msg->key.key) {
                    case XK_1:
                        ofcb->setState( ( ofcb->getState() == true ) ? false : true );
                        break;
                    case XK_2:
                        odcb->setState( ( odcb->getState() == true ) ? false : true );
                        break;
                    case XK_3:
                        rcb->setState( ( rcb->getState() == true ) ? false : true );
                        break;
                    case XK_4:
                        mode_cycb->setOption( ( mode_cycb->getSelectedOption() + 1 ) % mode_cycb->getNrOfOptions() );
                        if ( mode_cycb->getSelectedOption() == 0 ) {
                            permwin->hide();
                        } else {
                            permwin->show();
                        }
                        break;                    
                    case XK_KP_1:
                    case XK_KP_End:
                        mcb[6]->setState( ( mcb[6]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_2:
                    case XK_KP_Down:
                        mcb[7]->setState( ( mcb[7]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_3:
                    case XK_KP_Next:
                        mcb[8]->setState( ( mcb[8]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_4:
                    case XK_KP_Left:
                        mcb[3]->setState( ( mcb[3]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_5:
                    case XK_KP_Begin:
                        mcb[4]->setState( ( mcb[4]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_6:
                    case XK_KP_Right:
                        mcb[5]->setState( ( mcb[5]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_7:
                    case XK_KP_Home:
                        mcb[0]->setState( ( mcb[0]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_8:
                    case XK_KP_Up:
                        mcb[1]->setState( ( mcb[1]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_KP_9:
                    case XK_KP_Prior:
                        mcb[2]->setState( ( mcb[2]->getState() == true ) ? false : true );
                        setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
                        break;
                    case XK_Return:
                        if ( cb->getHasFocus() == false &&
                             octal_sg->isActive() == false ) {
                            endmode=0;
                        }
                        break;
                    case XK_Escape:
                        if ( octal_sg->isActive() == false ) {
                            endmode=1;
                        }
                        break;
                }
            }
            break;
          case AG_CYCLEBUTTONCLICKED:
              if ( msg->cyclebutton.cyclebutton == mode_cycb ) {
                  switch ( msg->cyclebutton.option ) {
                      case 1:
                      case 2:
                      case 3:
                          permwin->show();
                          break;
                      default:
                          permwin->hide();
                          break;
                  }
              }
              break;
          case AG_STRINGGADGET_OK:
              if ( msg->stringgadget.sg == octal_sg ) {
                  mode_t m = getModeFromOctalSG( octal_sg );
                  setChooseButtonMatrixForMode( mcb, m );
              }
              break;
          case AG_STRINGGADGET_CANCEL:
              if ( msg->stringgadget.sg == octal_sg ) {
                  mode_t m = getChooseButtonMatrixMode( mcb );
                  setOctalSGForMode( octal_sg, m );
              }
              break;
          case AG_CHOOSECLICKED:
              setOctalSGForMode( octal_sg, getChooseButtonMatrixMode( mcb ) );
              break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  
  if(endmode==0) {
      // ok
      enum chmod_apply_mode new_apply_mode;
      switch ( mode_cycb->getSelectedOption() ) {
          case 1:
              new_apply_mode = CHMOD_SET_PERMISSIONS;
              break;
          case 2:
              new_apply_mode = CHMOD_ADD_PERMISSIONS;
              break;
          case 3:
              new_apply_mode = CHMOD_REMOVE_PERMISSIONS;
              break;
          default:
              new_apply_mode = CHMOD_ASK_PERMISSIONS;
              break;
      }

      mode_t new_permissions = getChooseButtonMatrixMode( mcb );
      
      if(mode==1) {
          tonfiles = ofcb->getState();
          tondirs = odcb->getState();
          trecursive = rcb->getState();
          m_tapply_mode = new_apply_mode;
          m_tpermissions = new_permissions;
      } else {
          onfiles = ofcb->getState();
          ondirs = odcb->getState();
          recursive = rcb->getState();
          setRequestParameters( rfcb->getState() );
          setApplyMode( new_apply_mode );
          setPermissions( new_permissions );
      }
  }
  
  delete win;

  return endmode;
}

void ChModOp::setOnFiles(bool nv)
{
  onfiles=nv;
}

void ChModOp::setOnDirs(bool nv)
{
  ondirs=nv;
}

void ChModOp::setRecursive(bool nv)
{
  recursive=nv;
}

bool ChModOp::isInteractiveRun() const
{
    return true;
}

void ChModOp::setInteractiveRun()
{
    setRequestParameters( true );
}

void ChModOp::setApplyMode( enum chmod_apply_mode mode )
{
    m_apply_mode = mode;
}

void ChModOp::setPermissions( mode_t permissions )
{
    m_permissions = permissions;
}

void ChModOp::setPermissions( const std::string &permissions )
{
    int mode;

    if ( sscanf( permissions.c_str(), "%o", &mode ) == 1 ) {
        m_permissions = mode;
    } else {
        m_permissions = 0;
    }
}
