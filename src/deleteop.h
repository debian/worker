/* deleteop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DELETEOP_H
#define DELETEOP_H

#include "wdefines.h"
#include "functionproto.h"
#include "aguix/request.h"
#include "filenameshrinker.hh"

class AGUIX;
class AWindow;
class Text;
class SolidButton;
class Button;
class BevelBox;
class AFontWidth;

class DeleteOp:public FunctionProto
{
public:
  DeleteOp();
  virtual ~DeleteOp();
  DeleteOp( const DeleteOp &other );
  DeleteOp &operator=( const DeleteOp &other );

  DeleteOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;
  int configure() override;
  bool save(Datei *) override;

  void setAlsoActive( bool );
protected:
  static const char *name;
  // Infos to save
  bool also_active;

  // temp variables
  Lister *startlister;
  
  int normalmodedelete( ActionMessage *am );
};

class DeleteOpWin
{
public:
  DeleteOpWin(AGUIX*);
  ~DeleteOpWin();
  DeleteOpWin( const DeleteOpWin &other );
  DeleteOpWin &operator=( const DeleteOpWin &other );

  int open();
  void close();
  // called once:
  void set_files_to_delete(long nfiles);
  void set_dirs_to_delete(long ndirs);
  
  void dir_finished();
  void file_finished();
  void setfilename(char *name);
  // for setting a message in the two lines used for display copy source/dest
  void setmessage( const char *msg );
  // for dec file/dir-counter (mainly in case of skiped files/dirs
  void dec_file_counter(unsigned long f);
  void dec_dir_counter(unsigned long d);

  /* should be called in copy-loop
   * does the update of the window AND process X-events
   * returnvalues: 0 - normal exists
   *               1 - cancel/close window pressed */ 
  int redraw();
  int request( const char *title,
               const char *text,
               const char *buttons,
               Requester::request_flags_t flags = Requester::REQUEST_NONE );
  int string_request( const char *title,
                      const char *lines,
                      const char *default_str,
                      const char *buttons,
                      char **return_str,
                      Requester::request_flags_t flags = Requester::REQUEST_NONE );
protected:
  char *source;
  long deleted_files,files;
  long deleted_dirs,dirs;
  int sbw;
  
  AGUIX *aguix;
  AWindow *window;
  Text *sourcetext,*files2gotext,*dirs2gotext;
  SolidButton *gsb;
  Button *cb;
  bool update_sourcetext,update_files2gotext,update_dirs2gotext;
  BevelBox *bb1, *bb2, *bb3;
  int melw;
  int filenamespace;
  bool ismessage;
  AFontWidth *lencalc;
  struct timeval m_last_update_time = {};
    FileNameShrinker m_fns;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
