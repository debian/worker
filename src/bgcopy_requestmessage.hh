/* bgcopy_requestmessage.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BGCOPY_REQUESTMESSAGE_HH
#define BGCOPY_REQUESTMESSAGE_HH

#include "wdefines.h"
#include <string>
#include "aguix/request.h"

class BGCopyRequestMessage
{
public:

    typedef enum {
        REGULAR_REQUEST,
        STRING_REQUEST
    } request_type_t;

    BGCopyRequestMessage( const std::string &title,
                          const std::string &text,
                          const std::string &buttons );
    BGCopyRequestMessage( const std::string &title,
                          const std::string &text,
                          const std::string &default_string,
                          const std::string &buttons,
                          Requester::request_flags_t flags = Requester::REQUEST_NONE );

    request_type_t getType() const;
    int getRes() const;
    std::string getResultString() const;

    std::string getTitle() const;
    std::string getText() const;
    std::string getDefaultString() const;
    std::string getButtons() const;
    Requester::request_flags_t getFlags() const;

    void setResultString( const char *result_string );
    void setRes( int res );
private:
    request_type_t m_type;
    std::string m_title;
    std::string m_text;
    std::string m_default_string;
    std::string m_buttons;
    Requester::request_flags_t m_flags;

    std::string m_result_string;
    int m_res;
};

#endif
