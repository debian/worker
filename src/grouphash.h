/* grouphash.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2001-2004 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: grouphash.h,v 1.9 2004/10/27 21:07:37 ralf Exp $ */

#ifndef GROUPHASH_H
#define GROUPHASH_H

#include "wdefines.h"

struct scl {char*name;
            uid_t uid;
            gid_t gid;
            struct scl *next;};

class GroupHash
{
public:
  GroupHash();
  ~GroupHash();
  GroupHash( const GroupHash &other );
  GroupHash &operator=( const GroupHash &other );

  char *getUserNameS(uid_t);
  char *getUserName(uid_t);
  void getUserName(uid_t,char*,int);
  char *getGroupNameS(gid_t);
  char *getGroupName(gid_t);
  void getGroupName(gid_t,char*,int);
  bool isInGroup( gid_t );
protected:
  int hash(int);
  void initUsersGroups();
  void freeUsersGroups();
  struct scl **users;
  struct scl **groups;
  struct scl **usersgroups;
  int size;
};

extern GroupHash *ugdb;

#endif
