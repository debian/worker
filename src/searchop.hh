/* searchop.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SEARCHOP_HH
#define SEARCHOP_HH

#include "wdefines.h"
#include "functionproto.h"
#include <string>

class SearchOp : public FunctionProto
{
public:
    SearchOp();
    ~SearchOp();
    SearchOp( const SearchOp &other );
    SearchOp &operator=( const SearchOp &other );

    SearchOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;

    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save( Datei * ) override;
    int configure() override;

    void setEditCommand( const std::string &str );
    void setShowPrevResults( bool nv );
    void setUseSelectedEntries( bool nv );
  
    static const char *name;
protected:
    // Infos to save
    std::string _edit_command;
    bool _show_prev_results;
    bool m_use_selected_entries = false;

    int prepareWindow( ActionMessage *msg );

    static int _stored_results_ctr;
};

#endif
