/* wconfig_hotkey.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2006 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_HOTKEY_HH
#define WCONFIG_HOTKEY_HH

#include "wdefines.h"
#include "wconfig_panel.hh"

class WConfig;

class HotkeyPanel : public WConfigPanel
{
public:
  HotkeyPanel( AWindow &basewin, WConfig &baseconfig );
  ~HotkeyPanel();
  int create();
  int saveValues();

  void run( Widget *, const AGMessage &msg ) override;
  int addButtons( List *buttons, WConfigPanelCallBack::add_action_t action );
  int addHotkeys( List *hotkeys, WConfigPanelCallBack::add_action_t action );
protected:
  Button *newb, *dupb, *delb, *editb;
  FieldListView *lv;
};
 
#endif
