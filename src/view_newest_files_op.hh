/* view_newest_files_op.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2016-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef VIEW_NEWEST_FILES_OP_HH
#define VIEW_NEWEST_FILES_OP_HH

#include "wdefines.h"
#include "functionproto.h"
#include <string>
#include <list>

class Button;
class FieldListView;
class AGUIX;
class Text;
class TextStorageString;

class ViewNewestFilesOp : public FunctionProto
{
public:
    ViewNewestFilesOp();
    ~ViewNewestFilesOp();
    ViewNewestFilesOp( const ViewNewestFilesOp &other );
    ViewNewestFilesOp &operator=( const ViewNewestFilesOp &other );
    
    ViewNewestFilesOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;
    
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    
    static const char *name;
private:
    std::unique_ptr<class AWindow> m_win;
    AGUIX *m_aguix = NULL;
    FieldListView *m_lv;
    Button *m_more_days_b, *m_less_days_b;
    Button *m_okb, *m_closeb;
    Text *m_infotext;
    Text *m_time_window_text;
    std::shared_ptr< TextStorageString > m_help_ts;

    int m_past_days;
    std::string m_base_dir;

    static int s_vdir_number;

    void openWindow();

    void updateResults( const std::list< std::pair< std::string, time_t > > &files,
                        time_t mod_limit );

    time_t calculateModLimit( time_t newest_time );

    void panelizeResults( Worker *w,
                          const std::list< std::pair< std::string, time_t > > &files,
                          time_t newest_time );

    void updateTimeWindowText();

    void sortResults( std::list< std::pair< std::string, time_t > > &files );
};

#endif
