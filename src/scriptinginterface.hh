/* scriptinginterface.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SCRIPTINGINTERFACE_HH
#define SCRIPTINGINTERFACE_HH

#include "wdefines.h"
#include <string>

class ScriptingInterface
{
public:
    virtual ~ScriptingInterface() = 0;

    virtual int setIntVariable( const std::string &name, int val ) = 0;
    virtual int setBoolVariable( const std::string &name, bool val ) = 0;
    virtual int setStringVariable( const std::string &name, const std::string &val ) = 0;

    virtual int evalCommand( const std::string &cmd, int number_return_values ) = 0;

    virtual int getIntReturnValue( int &res ) = 0;
    virtual int getBoolReturnValue( bool &res ) = 0;
    virtual int getStringReturnValue( std::string &res ) = 0;
};

#endif
