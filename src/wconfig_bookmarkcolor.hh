/* wconfig_bookmarkcolor.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_BOOKMARKCOLOR_HH
#define WCONFIG_BOOKMARKCOLOR_HH

#include "wdefines.h"
#include "wconfig_panel.hh"
#include "wconfig.h"

class BookmarkColorPanel : public WConfigPanel
{
public:
    BookmarkColorPanel( AWindow &basewin, WConfig &baseconfig );
    ~BookmarkColorPanel();
    int create();
    int saveValues();
    
    /* gui elements callback */
    void run( Widget *, const AGMessage &msg ) override;
    
    /* we are interested in updated colors */
    panel_action_t setColors( List *colors );
protected:
    FieldListView *m_lv;
    FieldListView *m_label_lv;
    Button *m_norm_b[2];
    Button *m_act_b[2];
    Button *m_add_label_b;
    StringGadget *m_label_sg;
    Button *m_del_label_b;
    ChooseButton *m_only_exact_path = nullptr;

    std::map<std::string, WConfig::ColorDef::label_colors_t> m_label_colors;
    
    void setLVColors( int row, int fg, int bg );
    void updateLV();
};
 
#endif
