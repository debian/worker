/* scriptop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2002-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SCRIPTOP_H
#define SCRIPTOP_H

#include "wdefines.h"
#include "functionproto.h"
#include "ownop.h"

class Worker;
class WPUContext;

static const ownop_flags_t scriptop_flags[] = { { "isEmpty(0)", 40 },
                                                { "size(0)", 41 },
                                                { "lasterror", 42 },
                                                { "filelistEmpty(0)", 43 },
                                                { "true", 44 },
                                                { "false", 45 },
                                                { "\"\"", 46 },
                                                { "?{command}", 47 },
                                                { "${command}", 48 },
                                                { "toNum()", 51 },
                                                { "toStr()", 52 },
						{ "isLocal()", 58 } };

class ScriptOp:public FunctionProto
{
public:
  ScriptOp();
  virtual ~ScriptOp();
  ScriptOp( const ScriptOp &other );
  ScriptOp &operator=( const ScriptOp &other );

  ScriptOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;
  int configure() override;
  bool save(Datei*) override;

  bool isLabel( const char *str );
  typedef enum { SCRIPT_NOP = 0,
                 SCRIPT_PUSH,
                 SCRIPT_LABEL,
                 SCRIPT_IF,
                 SCRIPT_END,
                 SCRIPT_POP,
                 SCRIPT_SETTINGS,
                 SCRIPT_WINDOW,
                 SCRIPT_GOTO,
                 SCRIPT_EVALCOMMAND } scriptop_type_t;
  typedef enum { SCRIPT_WINDOW_LEAVE = 0,
                 SCRIPT_WINDOW_OPEN,
                 SCRIPT_WINDOW_CLOSE } scriptop_window_t;
  void setType( scriptop_type_t ntype );
  void setLabel( const char *nlabel );
  void setStackNr( int nv );
  void setPushString( const char *nstr );
  void setPushUseOutput( bool nv );
  void setPushOutputReturnCode( bool nv );
  void setIfTest( const char *nstr );
  void setIfLabel( const char *nstr );
  scriptop_type_t getType();
  const char *getLabel();
  int getStackNr();
  const char *getPushString();
  bool getPushUseOutput();
  bool getPushOutputReturnCode();
  const char *getIfTest();
  const char *getIfLabel();
  char *getPushFlag();
  char *getRegisteredCommand();
  char *getIfFlag();
  void setDoDebug( bool nv );
  bool getDoDebug();
  void setWPURecursive( bool nv );
  bool getWPURecursive();
  void setWPUTakeDirs( bool nv );
  bool getWPUTakeDirs();
  void setWinType( scriptop_window_t nv );
  scriptop_window_t getWinType();
  void setChangeProgress( bool nv );
  bool getChangeProgress();
  void setChangeText( bool nv );
  bool getChangeText();
  void setProgress( const char *str );
  const char *getProgress();
  void setProgressUseOutput( bool nv );
  bool getProgressUseOutput();
  void setWinText( const char *str );
  const char *getWinText();
  void setWinTextUseOutput( bool nv );
  bool getWinTextUseOutput();

  void setCommandStr( const std::string &str );
  std::string getCommandStr() const;
protected:
  static const char *name;
  char *description_buf;
  int description_buf_size;

  // Infos to save
  scriptop_type_t type;
  char *label;
  int stack_nr;
  char *push_string;
  bool push_useoutput;
  bool m_push_output_return_code = false;
  char *if_test;
  char *if_label;
  bool dodebug;
  bool wpu_recursive;
  bool wpu_take_dirs;
  scriptop_window_t wintype;
  bool change_progress;
  bool change_text;
  char *progress;
  bool progress_useoutput;
  char *wintext;
  bool wintext_useoutput;

  std::string m_command_str;

  // temp variables
};

#endif
