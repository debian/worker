/* string_completion.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STRING_COMPLETION_HH
#define STRING_COMPLETION_HH

#include "wdefines.h"
#include "aguix/refcount.hh"
#include <list>
#include <string>

class StringMatcherFNMatch;

class StringCompletion
{
public:
    StringCompletion( RefCount< StringMatcherFNMatch > matcher );
    ~StringCompletion();

    std::string findCompletion( const std::string matching_string,
                                const std::list< std::string > &string_list );
private:
    size_t findShortestMatch( const std::string &s );

    RefCount< StringMatcherFNMatch > m_matcher;
};

#endif
