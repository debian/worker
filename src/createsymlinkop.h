/* createsymlinkop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CREATESYMLINKOP_H
#define CREATESYMLINKOP_H

#include "wdefines.h"
#include "functionproto.h"

class CreateSymlinkOp:public FunctionProto
{
public:
  CreateSymlinkOp();
  virtual ~CreateSymlinkOp();
  CreateSymlinkOp( const CreateSymlinkOp &other );
  CreateSymlinkOp &operator=( const CreateSymlinkOp &other );

  CreateSymlinkOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;
  
  int doconfigure(int mode);
  
  void setSameDir(bool);
  void setLocal(bool);
  bool isInteractiveRun() const;
  void setInteractiveRun();
protected:
  static const char *name;
  // Infos to save

  bool same_dir,local;
  
  // temp variables
  Lister *startlister,*endlister;
  
  bool tsame_dir,tlocal,trequestflags;
  
  int normalmodecreatesl( ActionMessage* );
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
