/* view_command_log_bg.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2022-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef VIEWCOMMANDLOGBG_HH
#define VIEWCOMMANDLOGBG_HH

#include "wdefines.h"
#include "file_command_log.hh"
#include "aguix/backgroundmessagehandler.hh"
#include <memory>
#include <string>

class FieldListView;
class AContainer;
class AWindow;
class Worker;
class AGUIX;

class ViewCommandLogBG : public BackgroundMessageHandler
{
public:
    ~ViewCommandLogBG();
    
    void createWindow();
    
    void handleAGUIXMessage( AGMessage &msg ) override;
protected:
    friend class ViewCommandLogOp;

    ViewCommandLogBG( Worker *worker, std::unique_ptr< std::list< FileCommandLog::Entry > > log );
    ViewCommandLogBG( const ViewCommandLogBG &other );
    ViewCommandLogBG &operator=( const ViewCommandLogBG &other );

    AWindow *getAWindow();
private:
    void maximizeWin( AGUIX *aguix,
                      AWindow *win,
                      FieldListView *lv,
                      AContainer *cont );
    void jumpToEntry( const std::string &path );

    void copyEntryToClipboard( int row );
    void copyAllToClipboard();
    std::string convertEntryToClipboardString( const FileCommandLog::Entry &entry ) const;

    Worker *m_worker;
    std::unique_ptr< std::list< FileCommandLog::Entry > > m_log;

    Button *m_closeb;
    Button *m_gob;
    Button *m_clearb;
    Button *m_clipboard_b;
    AWindow *m_win;
    FieldListView *m_lv;
};

#endif
