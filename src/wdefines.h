/* wdefines.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2012 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WDEFINES_H
#define WDEFINES_H

#ifndef CONFIG_H_INCLUDED
#  include <aguix/aguixconfig.h>
#  define CONFIG_H_INCLUDED
#endif

#include "worker_lfs.h"

#ifdef HAVE_AVFS
extern "C" {
#  include <virtual.h>
}
#endif

#include "worker_filefunc.h"

/* this following include isn't really needed at all
 * but the streambuf in (some?) stl implementations
 * undefing open/close (for whatever reason)
 * and this leads to problem on system supporting
 * large files with open to open64 define
 * including vector from stl (not streambuf directly
 * because it's not available everywhere) here should
 * avoid such problems but open in streambuf is not
 * accessable anymore
 * TODO: a better solution is highly wanted
 */
#include <vector>

#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#ifdef HAVE_SYS_PARAM_H
  #include <sys/param.h>
#endif

#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <grp.h>
#include <pwd.h>
#include <fnmatch.h>
#include <ctype.h>
#include <utime.h>

#if defined(HAVE_SYS_STATVFS_H)
  #include <sys/statvfs.h>
#endif
#ifdef HAVE_SYS_VFS_H
  #include <sys/vfs.h>
#endif
#ifdef HAVE_SYS_MOUNT_H
  #include <sys/mount.h>
#endif
#if defined HAVE_SYS_SYSINFO_H && defined HAVE_SYSINFO
  #include <sys/sysinfo.h>
#endif

#ifdef HAVE_REGEX
#include <regex.h>
#endif

#include "aguix/aguixdefs.h"

#ifdef HAVE_LIBZ
  #include <zlib.h>
#endif

#ifdef HAVE_LIBBZ2
  #include <bzlib.h>
#endif

#define OUTPUT_BIN "xmessage -file %s"
#define TERMINAL_BIN "xterm -e /bin/sh %s"

#ifndef LINUX
  #ifndef ARG_MAX
    #ifdef _POSIX_ARG_MAX
      #define ARG_MAX _POSIX_ARG_MAX
    #else
      #define ARG_MAX MAXPATHLEN
    #endif
  #endif
#endif

//#define EXE_STRING_LEN ARG_MAX
#if ! defined( MAX_CMD_LEN ) || MAX_CMD_LEN < 0
#  define EXE_STRING_LEN ( 1024 * 1024 )
#elif ( MAX_CMD_LEN < 4096 ) && ( MAX_CMD_LEN < ARG_MAX )
#  define EXE_STRING_LEN ARG_MAX
#elif MAX_CMD_LEN < ( 10 * 1024 * 1024 )
#  define EXE_STRING_LEN MAX_CMD_LEN
#else
// use this upper limit right now since sometimes the autoconf test gives very large values (like 4GB)
#  define EXE_STRING_LEN ( 10 * 1024 * 1024 )
#endif

#ifdef DEVELOPER
  #define USEOWNCONFIGFILES
#endif

#define w_max(a,b) (((a)>(b))?(a):(b))
#define w_min(a,b) (((a)<(b))?(a):(b))

#if ( ! defined S_ISSOCK  ) && defined S_IFSOCK
#  define S_ISSOCK(mode) ( ( (mode) & S_IFMT ) == S_IFSOCK )
#endif

#if ( ! defined S_ISLNK  ) && defined S_IFLNK
#  define S_ISLNK(mode) ( ( (mode) & S_IFMT ) == S_IFLNK )
#endif

#ifndef HW_VM_FSTAB
#define HW_VM_FSTAB "/etc/fstab"
#endif
#ifndef HW_VM_MTAB
#define HW_VM_MTAB "/etc/mtab"
#endif
#ifndef HW_VM_MOUNT_CMD
#define HW_VM_MOUNT_CMD "/bin/mount {f}"
#endif
#ifndef HW_VM_UNMOUNT_CMD
#define HW_VM_UNMOUNT_CMD "/bin/umount {f}"
#endif
#ifndef HW_VM_EJECT_CMD
#define HW_VM_EJECT_CMD "/usr/bin/eject {d}"
#endif
#ifndef HW_VM_CLOSETRAY_CMD
#define HW_VM_CLOSETRAY_CMD "/usr/bin/eject -t {d}"
#endif
#ifndef HW_VM_PARTS
#define HW_VM_PARTS "/proc/partitions"
#endif

#endif
