/* wcdoubleshortkey.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2012 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wcdoubleshortkey.hh"
#include "datei.h"
 
WCDoubleShortkey::WCDoubleShortkey()
{
  _key[0] = 0;
  _key[1] = 0;
  _mod[0] = 0;
  _mod[1] = 0;
  _type = WCDS_NORMAL;
}

WCDoubleShortkey::~WCDoubleShortkey()
{
}

WCDoubleShortkey *WCDoubleShortkey::duplicate() const
{
  WCDoubleShortkey *tsh = new WCDoubleShortkey();
  tsh->setMod( _mod[0], 0 );
  tsh->setMod( _mod[1], 1 );
  tsh->setKeySym( _key[0], 0 );
  tsh->setKeySym( _key[1], 1 );
  tsh->setType( _type );
  return tsh;
}

void WCDoubleShortkey::setKeySym( KeySym k, int pos )
{
  if ( pos < 0 || pos > 1 ) return;
  _key[pos] = k;
}

void WCDoubleShortkey::setMod( unsigned int m, int pos )
{
  if ( pos < 0 || pos > 1 ) return;
  _mod[pos] = m;
}

KeySym WCDoubleShortkey::getKeySym( int pos ) const
{
  if ( pos != 1 ) return _key[0];
  return _key[1];
}

unsigned int WCDoubleShortkey::getMod( int pos ) const
{
  if ( pos != 1 ) return _mod[0];
  return _mod[1];
}

bool WCDoubleShortkey::isShortkey( KeySym k, unsigned int m ) const
{
  if ( _type == WCDS_DOUBLE ) return false;
  if ( k == _key[0] && m == _mod[0] ) return true;
  return false;
}

bool WCDoubleShortkey::isShortkey( KeySym k1, unsigned int m1, KeySym k2, unsigned int m2 ) const
{
  if ( _type == WCDS_NORMAL ) return false;
  if ( k1 == _key[0] && m1 == _mod[0] &&
       k2 == _key[1] && m2 == _mod[1] ) return true;
  return false;
}

bool WCDoubleShortkey::isReal() const
{
  if ( _type == WCDS_NORMAL && _key[0] != 0 ) return true;
  if ( _type == WCDS_DOUBLE && _key[0] != 0 && _key[1] != 0 ) return true;
  return false;
}

void WCDoubleShortkey::setType( enum shortkey_type nv )
{
  _type = nv;
}

enum WCDoubleShortkey::shortkey_type WCDoubleShortkey::getType() const
{
  return _type;
}

bool WCDoubleShortkey::save( Datei *fh ) const
{
  if ( fh == NULL ) return false;
  char *kstr1, *kstr2;

  kstr1 = AGUIX::getStringForKeySym( _key[0] );
  kstr2 = AGUIX::getStringForKeySym( _key[1] );

  if ( _type == WCDS_DOUBLE ) {
    fh->configOpenSection( "double" );
  } else {
    fh->configOpenSection( "normal" );
  }
  if ( kstr1 != NULL ) {
    fh->configPutPairString( "key", kstr1 );
  } else {
    fh->configPutPairString( "key", "NoSymbol" );
  }
  if ( ( _mod[0] & ControlMask ) != 0 ) fh->configPutPair( "mod", "control" );
  if ( ( _mod[0] & ShiftMask ) != 0 ) fh->configPutPair( "mod", "shift" );
  if ( ( _mod[0] & LockMask ) != 0 ) fh->configPutPair( "mod", "lock" );
  if ( ( _mod[0] & Mod1Mask ) != 0 ) fh->configPutPair( "mod", "mod1" );
  if ( ( _mod[0] & Mod2Mask ) != 0 ) fh->configPutPair( "mod", "mod2" );
  if ( ( _mod[0] & Mod3Mask ) != 0 ) fh->configPutPair( "mod", "mod3" );
  if ( ( _mod[0] & Mod4Mask ) != 0 ) fh->configPutPair( "mod", "mod4" );
  if ( ( _mod[0] & Mod5Mask ) != 0 ) fh->configPutPair( "mod", "mod5" );

  if ( _type == WCDS_DOUBLE ) {
    if ( kstr2 != NULL ) {
      fh->configPutPairString( "key", kstr2 );
    } else {
      fh->configPutPairString( "key", "NoSymbol" );
    }
    if ( ( _mod[1] & ControlMask ) != 0 ) fh->configPutPair( "mod", "control" );
    if ( ( _mod[1] & ShiftMask ) != 0 ) fh->configPutPair( "mod", "shift" );
    if ( ( _mod[1] & LockMask ) != 0 ) fh->configPutPair( "mod", "lock" );
    if ( ( _mod[1] & Mod1Mask ) != 0 ) fh->configPutPair( "mod", "mod1" );
    if ( ( _mod[1] & Mod2Mask ) != 0 ) fh->configPutPair( "mod", "mod2" );
    if ( ( _mod[1] & Mod3Mask ) != 0 ) fh->configPutPair( "mod", "mod3" );
    if ( ( _mod[1] & Mod4Mask ) != 0 ) fh->configPutPair( "mod", "mod4" );
    if ( ( _mod[1] & Mod5Mask ) != 0 ) fh->configPutPair( "mod", "mod5" );
  }

  fh->configCloseSection();
  if ( kstr1 != NULL ) _freesafe( kstr1 );
  if ( kstr2 != NULL ) _freesafe( kstr2 );
  return true;
}

std::string WCDoubleShortkey::getName( AGUIX *aguix )
{
    std::string res = "";

    char *tstr1 = aguix->getNameOfKey( getKeySym( 0 ), getMod( 0 ) );

    if ( tstr1 != NULL )
        res = tstr1;
    else
        res = "???";

    _freesafe( tstr1 );

    if ( getType() == WCDS_DOUBLE ) {
        res += " ";
        tstr1 = aguix->getNameOfKey( getKeySym( 1 ), getMod( 1 ) );

        if ( tstr1 != NULL )
            res += tstr1;
        else
            res += "???";

        _freesafe( tstr1 );
    }

    return res;
}
