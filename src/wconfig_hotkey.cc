/* wconfig_hotkey.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_hotkey.hh"
#include "wconfig.h"
#include "worker.h"
#include "wchotkey.hh"
#include "worker_locale.h"
#include "aguix/fieldlistview.h"
#include "aguix/button.h"
#include "simplelist.hh"

HotkeyPanel::HotkeyPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

HotkeyPanel::~HotkeyPanel()
{
}

int HotkeyPanel::create()
{
  Panel::create();

  int row;

  AContainer *ac1 = setContainer( new AContainer( this, 1, 3 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  addMultiLineText( catalog.getLocale( 685 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  lv = (FieldListView*)ac1->add( new FieldListView( _aguix, 0, 0, 200, 150, 0 ), 0, 1, AContainer::CO_MIN );
  lv->setHBarState(2);
  lv->setVBarState(2);
  lv->setAcceptFocus( true );
  lv->setDisplayFocus( true );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 4, 1 ), 0, 2 );
  ac1_1->setMinSpace( 0 );
  ac1_1->setMaxSpace( 0 );
  ac1_1->setBorderWidth( 0 );

  newb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 154 ), 0 ), 0, 0, AContainer::CO_INCW );
  newb->connect( this );
  dupb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 155 ), 0 ), 1, 0, AContainer::CO_INCW );
  dupb->connect( this );
  delb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 156 ), 0 ), 2, 0, AContainer::CO_INCW );
  delb->connect( this );
  editb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 157 ), 0 ), 3, 0, AContainer::CO_INCW );
  editb->connect( this );
  
  int id = _baseconfig.getHotkeys()->initEnum();
  WCHotkey *h1=(WCHotkey*)_baseconfig.getHotkeys()->getFirstElement(id);
  while(h1!=NULL) {
    row = lv->addRow();
    lv->setText( row, 0, h1->getName() );
    lv->setPreColors( row, FieldListView::PRECOLOR_ONLYACTIVE );
    h1=(WCHotkey*)_baseconfig.getHotkeys()->getNextElement(id);
  }
  lv->redraw();
  _baseconfig.getHotkeys()->closeEnum(id);
  
  contMaximize( true );
  return 0;
}

int HotkeyPanel::saveValues()
{
  return 0;
}

void HotkeyPanel::run( Widget *elem, const AGMessage &msg )
{
  List *hotkeys = _baseconfig.getHotkeys();
  int trow;

  if ( _need_recreate == true ) return;

  if ( msg.type == AG_BUTTONCLICKED ) {
    if ( msg.button.button == newb ) {
      WCHotkey *th1 = new WCHotkey();
      hotkeys->addElement( th1 );
      trow = lv->addRow();
      lv->setText( trow, 0, th1->getName() );
      lv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
      lv->setActiveRow( trow );
      lv->showActive();
      int pos = hotkeys->getIndex( th1 );
      if ( _baseconfig.configureHotkeyIndex( pos ) == true ) {
	lv->setText( trow, 0, th1->getName() );
	lv->redraw();
      }
    } else if ( msg.button.button == dupb ) {
      trow = lv->getActiveRow();
      if ( lv->isValidRow( trow ) == true ) {
	int pos = trow;
	WCHotkey *th1 = (WCHotkey*)hotkeys->getElementAt( pos );
	if ( th1 != NULL ) {
	  WCHotkey *th2 = th1->duplicate();
	  // reset shortkey to avoid duplicates
	  th2->setDoubleKeys( NULL );
	  
	  hotkeys->addElement(th2);
	  trow = lv->addRow();
	  lv->setText( trow, 0, th2->getName() );
	  lv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
	  lv->setActiveRow( trow );
	  lv->showActive();
	  lv->redraw();
	}
      }
    } else if ( msg.button.button == delb ) {
      trow = lv->getActiveRow();
      if ( lv->isValidRow( trow ) == true ) {
	int pos = trow;
	WCHotkey *th1 = (WCHotkey*)hotkeys->getElementAt( pos );
	if ( th1 != NULL ) {
	  delete th1;
	  hotkeys->removeElementAt( pos );
	  lv->deleteRow( pos );
	  lv->redraw();
	}
      }
    } else if ( msg.button.button == editb ) {
      trow = lv->getActiveRow();
      if ( lv->isValidRow( trow ) == true ) {
	int pos = trow;
	WCHotkey *th1 = (WCHotkey*)hotkeys->getElementAt( pos );
	if ( th1 != NULL ) {
	  if ( _baseconfig.configureHotkeyIndex( pos ) == true ) {
	    lv->setText( trow, 0, th1->getName() );
	    lv->redraw();
	  }
	}
      }
    }
  }
}

int HotkeyPanel::addButtons( List *buttons,
                             WConfigPanelCallBack::add_action_t action )
{
  int erg = 0;

  if ( buttons != NULL && action == WConfigPanelCallBack::CHECK_DOUBLEKEYS ) {
    erg = _baseconfig.fixDoubleKeys( NULL, NULL, _baseconfig.getHotkeys(), buttons, NULL );
  }
  _need_recreate = true;
  return erg;
}

int HotkeyPanel::addHotkeys( List *hotkeys,
                             WConfigPanelCallBack::add_action_t action )
{
  int erg = 0;

  if ( hotkeys != NULL && action == WConfigPanelCallBack::DO_IMPORT ) {
    erg = _baseconfig.addHotkeys( hotkeys );
  }
  _need_recreate = true;
  return erg;
}
