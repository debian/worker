/* tabprofilesop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "tabprofilesop.hh"
#include "listermode.h"
#include "worker.h"
#include "aguix/awindow.h"
#include "aguix/text.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include "aguix/fieldlistview.h"
#include "aguix/acontainerbb.h"
#include "aguix/kartei.h"
#include "aguix/textview.h"
#include "aguix/request.h"
#include "worker_locale.h"
#include "virtualdirmode.hh"
#include "workerinitialsettings.hh"
#include "ajson.hh"
#include "nwc_os.hh"

using namespace AJSON;

const char *TabProfilesOp::name = "TabProfilesOp";

TabProfilesOp::TabProfilesOp() : FunctionProto(), m_left_l( nullptr ), m_right_l( nullptr )
{
    m_category = FunctionProto::CAT_OTHER;
}

TabProfilesOp::~TabProfilesOp()
{
}

TabProfilesOp*
TabProfilesOp::duplicate() const
{
    TabProfilesOp *ta = new TabProfilesOp();
    return ta;
}

bool
TabProfilesOp::isName( const char *str )
{
    if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *
TabProfilesOp::getName()
{
    return name;
}

int
TabProfilesOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    m_left_l = msg->getWorker()->getLister( 0 );
    m_right_l = msg->getWorker()->getLister( 1 );

    return gui();
}

const char *
TabProfilesOp::getDescription()
{
    return catalog.getLocale( 1308 );
}

std::vector< std::string > TabProfilesOp::getAvailableProfiles()
{
    std::vector< std::string > res;

    std::string basedir = WorkerInitialSettings::getInstance().getConfigBaseDir();

    basedir += "/tabs/";

    NWC::Dir d( basedir );

    d.readDir( false );

    auto entries = d.getFullnamesOfSubentries();

    for ( auto &e : entries ) {
        if ( AGUIXUtils::ends_with( e, ".ajson" ) ) {
            NWC::FSEntry f( e );

            std::string basename = f.getBasename();

            res.push_back( std::string( basename, 0, basename.size() - 6 ) );
        }
    }

    return res;
}

std::pair< std::vector< std::string >, std::vector< std::string > > TabProfilesOp::getProfile( const std::string &profile_name )
{
    std::pair< std::vector< std::string >, std::vector< std::string > > res;

    std::string basedir = WorkerInitialSettings::getInstance().getConfigBaseDir();

    basedir += "/tabs/";

    std::string filename = AGUIXUtils::formatStringToString( "%s%s.ajson",
                                                             basedir.c_str(),
                                                             profile_name.c_str() );

    auto jroot = as_object( AJSON::load( filename ) );

    if ( ! jroot ) {
        return res;
    }

    auto jleft = as_object( jroot->get( "left" ) );
    auto jright = as_object( jroot->get( "right" ) );

    if ( ! jleft || ! jright ) {
        return res;
    }

    auto jleft_opened = as_array( jleft->get( "opened" ) );
    auto jright_opened = as_array( jright->get( "opened" ) );

    for ( size_t p = 0; p < jleft_opened->size(); p++ ) {
        auto f = as_string( jleft_opened->get( p ) );

        if ( f ) {
            res.first.push_back( f->get_value() );
        }
    }

    for ( size_t p = 0; p < jright_opened->size(); p++ ) {
        auto f = as_string( jright_opened->get( p ) );

        if ( f ) {
            res.second.push_back( f->get_value() );
        }
    }

    return res;
}

void TabProfilesOp::show_profile_content( const std::string &profile_name,
                                          std::shared_ptr< TextStorageString > storage,
                                          TextView *tv )
{
    auto content = getProfile( profile_name );

    std::string str1;

    str1 += catalog.getLocale( 1192 );
    str1 += "\n";

    for ( auto &e : content.first ) {
        str1 += "  ";
        str1 += e;
        str1 += "\n";
    }

    str1 += "\n";
    str1 += catalog.getLocale( 1193 );
    str1 += "\n";

    for ( auto &e : content.second ) {
        str1 += "  ";
        str1 += e;
        str1 += "\n";
    }

    storage->setContent( str1 );
    tv->textStorageChanged();
}

std::pair< std::vector< std::string >, std::vector< std::string > > TabProfilesOp::get_current_tabs()
{
    VirtualDirMode *left_vdm = nullptr;
    VirtualDirMode *right_vdm = nullptr;

    if ( m_left_l ) {
        ListerMode *lm1 = m_left_l->getActiveMode();
        if ( lm1 ) {
            if ( auto vdm = dynamic_cast< VirtualDirMode *>( lm1 ) ) {
                left_vdm = vdm;
            }
        }
    }

    if ( m_right_l ) {
        ListerMode *lm1 = m_right_l->getActiveMode();
        if ( lm1 ) {
            if ( auto vdm = dynamic_cast< VirtualDirMode *>( lm1 ) ) {
                right_vdm = vdm;
            }
        }
    }

    std::pair< std::vector< std::string >, std::vector< std::string > > res;

    if ( left_vdm ) {
        res.first = left_vdm->get_tabs();
    }

    if ( right_vdm ) {
        res.second = right_vdm->get_tabs();
    }

    return res;
}

int TabProfilesOp::dump_tabs( const std::string &profile_name )
{
    if ( AGUIXUtils::contains( profile_name, "/" ) ) {
        Requester req( Worker::getAGUIX() );

        std::string t1 = AGUIXUtils::formatStringToString( catalog.getLocale( 1204 ),
                                                           profile_name.c_str() );

        req.request( catalog.getLocale( 124 ),
                     t1.c_str(),
                     catalog.getLocale( 11 ) );
        return 1;
    }

    std::string basedir = WorkerInitialSettings::getInstance().getConfigBaseDir();

    basedir += "/tabs/";

    if ( NWC::OS::make_dirs( basedir ) != 0 ) {
        return 1;
    }
    
    std::string filename = AGUIXUtils::formatStringToString( "%s%s.ajson",
                                                             basedir.c_str(),
                                                             profile_name.c_str() );

    NWC::FSEntry fse( filename );

    if ( fse.entryExists() ) {
        Requester req( Worker::getAGUIX() );

        std::string t1 = AGUIXUtils::formatStringToString( catalog.getLocale( 409 ),
                                                           profile_name.c_str() );
        std::string s1 = catalog.getLocale( 280 );
        s1 += "|";
        s1 += catalog.getLocale( 8 );

        int res = req.request( catalog.getLocale( 123 ),
                               t1.c_str(),
                               s1.c_str() );
        
        if ( res == 1 ) {
            return 1;
        }
    }

    auto jroot = make_object();

    auto jleft = make_object();
    auto jright = make_object();

    auto jleft_opened = make_array();
    auto jright_opened = make_array();

    auto tabs = get_current_tabs();

    for ( auto &p : tabs.first ) {
        jleft_opened->append( make_string( p ) );
    }
    for ( auto &p : tabs.second ) {
        jright_opened->append( make_string( p ) );
    }

    jleft->set( "opened", jleft_opened );
    jright->set( "opened", jright_opened );

    jroot->set( "left", jleft );
    jroot->set( "right", jright );

    int res = dump( jroot, filename );

    if ( res != 0 ) {
        Requester req( Worker::getAGUIX() );

        std::string t1 = AGUIXUtils::formatStringToString( catalog.getLocale( 1194 ),
                                                           profile_name.c_str() );

        req.request( catalog.getLocale( 124 ),
                     t1.c_str(),
                     catalog.getLocale( 633 ) );
    }

    return res;
}

int TabProfilesOp::load_tabs( const std::string &profile_name )
{
    auto tabs = getProfile( profile_name );

    if ( tabs.first.empty() && tabs.second.empty() ) {
        return 0;
    }

    VirtualDirMode *left_vdm = nullptr;
    VirtualDirMode *right_vdm = nullptr;

    if ( m_left_l ) {
        m_left_l->switch2Mode( 0 );

        ListerMode *lm1 = m_left_l->getActiveMode();
        if ( lm1 ) {
            if ( auto vdm = dynamic_cast< VirtualDirMode *>( lm1 ) ) {
                left_vdm = vdm;
            }
        }
    }

    if ( m_right_l ) {
        m_right_l->switch2Mode( 0 );

        ListerMode *lm1 = m_right_l->getActiveMode();
        if ( lm1 ) {
            if ( auto vdm = dynamic_cast< VirtualDirMode *>( lm1 ) ) {
                right_vdm = vdm;
            }
        }
    }

    if ( left_vdm ) {
        left_vdm->open_tabs( tabs.first );
    }

    if ( right_vdm ) {
        right_vdm->open_tabs( tabs.second );
    }

    return 0;
}

int TabProfilesOp::filter_available_profiles( const std::vector< std::string > &profiles,
                                              const std::string &filter,
                                              FieldListView *lv )
{
    lv->setSize( 0 );

    for ( auto &e : profiles ) {
        if ( AGUIXUtils::contains( e, filter ) ) {
            int row = lv->addRow();

            lv->setText( row, 0, e );
        }
    }

    lv->redraw();

    return 0;
}

int
TabProfilesOp::gui()
{
    AGUIX *aguix = Worker::getAGUIX();
    AGMessage *msg;
    int endmode = -1;

    auto available_profiles = getAvailableProfiles();
    
    AWindow *win = new AWindow( aguix, 10, 10, 10, 10, getDescription(), AWindow::AWINDOW_DIALOG );
    win->create();
    
    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 2 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );
  
    RefCount<AFontWidth> lencalc( new AFontWidth( aguix, NULL ) );
    auto help_ts = std::make_shared< TextStorageString >( catalog.getLocale( 1195 ), lencalc );
    TextView *help_tv = ac1->addWidget( new TextView( aguix,
                                                      0, 0, 50, 80, "", help_ts ),
                                        0, 0, AContainer::CO_INCW );
    help_tv->setLineWrap( true );
    help_tv->maximizeX( 500 );
    help_tv->maximizeYLines( 10, 500 );
    help_tv->showFrame( false );
    ac1->readLimits();
    help_tv->show();
    help_tv->setAcceptFocus( false );

    TextView::ColorDef tv_cd = help_tv->getColors();
    tv_cd.setBackground( 0 );
    tv_cd.setTextColor( 1 );
    help_tv->setColors( tv_cd );

    Kartei *k1 = new Kartei( aguix, 10, 10, 10, 10, "" );
    ac1->add( k1, 0, 1, AContainer::CO_MIN );
    k1->create();

    AWindow *subwin11 = new AWindow( aguix, 0, 0, 10, 10, "" );
    k1->add( subwin11 );
    subwin11->create();
  
    AContainer *acsw11 = subwin11->setContainer( new AContainer( subwin11, 1, 5 ), true );
    acsw11->setMinSpace( 5 );
    acsw11->setMaxSpace( 5 );
    acsw11->setBorderWidth( 5 );

    acsw11->add( new Text( aguix, 0, 0, catalog.getLocale( 1196 ) ), 0, 0, AContainer::CO_INCWNR );

    StringGadget *new_profile_name_sg = acsw11->addWidget( new StringGadget( aguix, 0, 0, 50, "tabs1", 0 ),
                                                           0, 1, AContainer::CO_INCW );

    acsw11->add( new Text( aguix, 0, 0, catalog.getLocale( 1197 ) ), 0, 2, AContainer::CO_INCWNR );

    FieldListView *existing_profiles_lv = acsw11->addWidget( new FieldListView( aguix, 0, 0, 200, 75, 0 ),
                                                             0, 3,
                                                             AContainer::CO_MIN );
    existing_profiles_lv->setHBarState( 2 );
    existing_profiles_lv->setVBarState( 2 );
    existing_profiles_lv->setAcceptFocus( true );
    existing_profiles_lv->setDisplayFocus( true );

    AContainer *ac1_5 = acsw11->add( new AContainer( subwin11, 2, 1 ), 0, 4 );
    ac1_5->setMinSpace( 5 );
    ac1_5->setMaxSpace( -1 );
    ac1_5->setBorderWidth( 0 );
    Button *saveb =(Button*)ac1_5->add( new Button( aguix,
                                                    0,
                                                    0,
                                                    catalog.getLocale( 7 ),
                                                    0 ), 0, 0, AContainer::CO_FIX );
    Button *cancelb = (Button*)ac1_5->add( new Button( aguix,
                                                       0,
                                                       0,
                                                       catalog.getLocale( 8 ),
                                                       0 ), 1, 0, AContainer::CO_FIX );

    acsw11->readLimits();

    AWindow *subwin12 = new AWindow( aguix, 0, 0, 10, 10, "" );
    k1->add( subwin12 );
    subwin12->create();
  
    AContainer *acsw12 = subwin12->setContainer( new AContainer( subwin12, 1, 4 ), true );
    acsw12->setMinSpace( 5 );
    acsw12->setMaxSpace( 5 );
    acsw12->setBorderWidth( 5 );

    AContainer *acsw12_1 = acsw12->add( new AContainer( subwin12, 2, 1 ), 0, 0 );
    acsw12_1->setMinSpace( 5 );
    acsw12_1->setMaxSpace( 5 );
    acsw12_1->setBorderWidth( 0 );

    acsw12_1->addWidget( new Text( aguix, 0, 0, catalog.getLocale( 758 ), 1, 0 ),
                         0, 0, AContainer::CO_FIX );

    StringGadget *findsg = acsw12_1->addWidget( new StringGadget( aguix, 0, 0, 50, "", 0 ),
                                                1, 0, AContainer::CO_INCW );

    acsw12->addWidget( new Text( aguix, 0, 0, catalog.getLocale( 1198 ), 1, 0 ),
                       0, 1, AContainer::CO_INCWNR );

    AContainer *acsw12_2 = acsw12->add( new AContainer( subwin12, 2, 1 ), 0, 2 );
    acsw12_2->setMinSpace( 5 );
    acsw12_2->setMaxSpace( 5 );
    acsw12_2->setBorderWidth( 0 );

    FieldListView *load_profiles_lv = acsw12_2->addWidget( new FieldListView( aguix, 0, 0, 200, 150, 0 ),
                                                           0, 0,
                                                           AContainer::CO_MIN );
    load_profiles_lv->setHBarState( 2 );
    load_profiles_lv->setVBarState( 2 );
    load_profiles_lv->setAcceptFocus( true );
    load_profiles_lv->setDisplayFocus( true );

    AContainer *acsw12_2_1 = acsw12_2->add( new AContainer( subwin12, 1, 2 ), 1, 0 );
    acsw12_2_1->setMinSpace( 5 );
    acsw12_2_1->setMaxSpace( 5 );
    acsw12_2_1->setBorderWidth( 0 );

    acsw12_2_1->addWidget( new Text( aguix, 0, 0, catalog.getLocale( 1199 ), 1, 0 ),
                           0, 0, AContainer::CO_INCWNR );

    auto content_ts = std::make_shared< TextStorageString >( "", lencalc );
    TextView *content_tv = acsw12_2_1->addWidget( new TextView( aguix,
                                                                0, 0, 200, 150, "", content_ts ),
                                                  0, 1, AContainer::CO_MIN );
    content_tv->setLineWrap( true );
    content_tv->showFrame( false );
    content_tv->maximizeYLines( 10, content_tv->getWidth() );
    acsw12_2_1->readLimits();
    content_tv->show();
    content_tv->setAcceptFocus( false );

    TextView::ColorDef tv_cd2 = content_tv->getColors();
    tv_cd2.setBackground( 0 );
    tv_cd2.setTextColor( 1 );
    content_tv->setColors( tv_cd2 );

    AContainer *acsw12_4 = acsw12->add( new AContainer( subwin12, 4, 1 ), 0, 3 );
    acsw12_4->setMinSpace( 5 );
    acsw12_4->setMaxSpace( -1 );
    acsw12_4->setBorderWidth( 0 );
    Button *openb = acsw12_4->addWidget( new Button( aguix,
                                                     0,
                                                     0,
                                                     catalog.getLocale( 1200 ),
                                                     0 ), 0, 0, AContainer::CO_FIX );
    Button *open_deleteb = acsw12_4->addWidget( new Button( aguix,
                                                            0,
                                                            0,
                                                            catalog.getLocale( 1201 ),
                                                            0 ), 1, 0, AContainer::CO_FIX );
    Button *deleteb = acsw12_4->addWidget( new Button( aguix,
                                                       0,
                                                       0,
                                                       catalog.getLocale( 1203 ),
                                                       0 ), 2, 0, AContainer::CO_FIX );
    Button *cancel2b = acsw12_4->addWidget( new Button( aguix,
                                                        0,
                                                        0,
                                                        catalog.getLocale( 8 ),
                                                        0 ), 3, 0, AContainer::CO_FIX );

    acsw12->readLimits();

    std::string str1 = catalog.getLocale( 7 );
    str1 += " - F1";
    k1->setOption( subwin11, 0, str1.c_str() );
    str1 = catalog.getLocale( 1202 );
    str1 += " - F2";
    k1->setOption( subwin12, 1, str1.c_str() );
    k1->maximize();
    k1->contMaximize();
    ac1->readLimits();

    win->setDoTabCycling( true );
    win->contMaximize( true );

    new_profile_name_sg->takeFocus();
    
    win->show();
    k1->show();

    existing_profiles_lv->setDefaultColorMode( FieldListView::precolor_t::PRECOLOR_ONLYACTIVE );
    load_profiles_lv->setDefaultColorMode( FieldListView::precolor_t::PRECOLOR_ONLYACTIVE );

    for ( auto &e : available_profiles ) {
        int row = existing_profiles_lv->addRow();

        existing_profiles_lv->setText( row, 0, e );

        row = load_profiles_lv->addRow();

        load_profiles_lv->setText( row, 0, e );
    }

    existing_profiles_lv->redraw();
    load_profiles_lv->redraw();
    content_tv->forceRedraw();

    new_profile_name_sg->selectAll();

    for ( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == saveb ) {
                        if ( dump_tabs( new_profile_name_sg->getText() ) == 0 ) {
                            endmode = 0;
                        }
                    } else if ( msg->button.button == openb ||
                                msg->button.button == open_deleteb ) {
                        int row = load_profiles_lv->getActiveRow();
                        if ( load_profiles_lv->isValidRow( row ) == true ) {
                            if ( load_tabs( load_profiles_lv->getText( row, 0 ) ) == 0 ) {
                                endmode = 0;

                                if ( msg->button.button == open_deleteb ) {
                                    delete_profile( load_profiles_lv->getText( row, 0 ) );
                                }
                            }
                        }
                    } else if ( msg->button.button == deleteb ) {
                        int row = load_profiles_lv->getActiveRow();
                        if ( load_profiles_lv->isValidRow( row ) == true ) {
                            delete_profile( load_profiles_lv->getText( row, 0 ) );
                            endmode = 1;
                        }
                    } else if ( msg->button.button == cancelb ||
                                msg->button.button == cancel2b ) {
                        endmode = 1;
                    }
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch(msg->key.key) {
                            case XK_F1:
                                k1->optionChange( 0 );
                                new_profile_name_sg->takeFocus();
                                break;
                            case XK_F2:
                                k1->optionChange( 1 );
                                findsg->takeFocus();
                                break;
                            case XK_Down:
                                if ( k1->getCurOption() == 0 ) {
                                    if ( new_profile_name_sg->getHasFocus() ) {
                                        existing_profiles_lv->takeFocus();
                                        if ( ! existing_profiles_lv->isValidRow( existing_profiles_lv->getActiveRow() ) &&
                                             existing_profiles_lv->getElements() > 0 ) {
                                            existing_profiles_lv->setActiveRow( 0 );
                                            new_profile_name_sg->setText( existing_profiles_lv->getText( 0, 0 ).c_str() );
                                        }
                                    }
                                } else if ( k1->getCurOption() == 1 ) {
                                    if ( findsg->getHasFocus() ) {
                                        load_profiles_lv->takeFocus();
                                        if ( ! load_profiles_lv->isValidRow( load_profiles_lv->getActiveRow() ) &&
                                             load_profiles_lv->getElements() > 0 ) {
                                            load_profiles_lv->setActiveRow( 0 );
                                            show_profile_content( load_profiles_lv->getText( 0, 0 ), content_ts, content_tv );
                                        }
                                    }
                                }
                                break;
                            case XK_Return:
                                if ( new_profile_name_sg->getHasFocus() == false &&
                                     findsg->getHasFocus() == false ) {
                                    if ( k1->getCurOption() == 0 ) {
                                        if ( dump_tabs( new_profile_name_sg->getText() ) == 0 ) {
                                            endmode = 0;
                                        }
                                    } else if ( k1->getCurOption() == 1 ) {
                                        int row = load_profiles_lv->getActiveRow();
                                        if ( load_profiles_lv->isValidRow( row ) == true ) {
                                            if ( load_tabs( load_profiles_lv->getText( row, 0 ) ) == 0 ) {
                                                endmode = 0;
                                            }
                                        }
                                    }
                                }
                                break;
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                case AG_STRINGGADGET_OK:
                    if ( msg->stringgadget.sg == new_profile_name_sg ) {
                        if ( dump_tabs( new_profile_name_sg->getText() ) == 0 ) {
                            endmode = 0;
                        }
                    } else if ( msg->stringgadget.sg == findsg ) {
                        int row = load_profiles_lv->getActiveRow();
                        if ( load_profiles_lv->isValidRow( row ) == true ) {
                            if ( load_tabs( load_profiles_lv->getText( row, 0 ) ) == 0 ) {
                                endmode = 0;
                            }
                        }
                    }
                    break;
                case AG_STRINGGADGET_CONTENTCHANGE:
                    if ( msg->stringgadget.sg == findsg ) {
                        int row = load_profiles_lv->getActiveRow();
                        std::string current_profile;
                        if ( load_profiles_lv->isValidRow( row ) == true ) {
                            current_profile = load_profiles_lv->getText( row, 0 );
                        }

                        filter_available_profiles( available_profiles,
                                                   findsg->getText(),
                                                   load_profiles_lv );

                        int p;

                        for ( p = 0; p < load_profiles_lv->getElements(); p++ ) {
                            if ( load_profiles_lv->getText( p, 0 ) == current_profile ) {
                                load_profiles_lv->setActiveRow( row );
                                break;
                            }
                        }

                        if ( p == load_profiles_lv->getElements() ) {
                            load_profiles_lv->setActiveRow( 0 );
                        }

                        row = load_profiles_lv->getActiveRow();
                        if ( load_profiles_lv->isValidRow( row ) == true ) {
                            show_profile_content( load_profiles_lv->getText( row, 0 ), content_ts, content_tv );
                        }
                    }
                    break;
                case AG_FIELDLV_ONESELECT:
                case AG_FIELDLV_MULTISELECT:
                    if ( msg->fieldlv.lv == existing_profiles_lv ) {
                        int row = existing_profiles_lv->getActiveRow();
                        if ( existing_profiles_lv->isValidRow( row ) == true ) {
                            new_profile_name_sg->setText( existing_profiles_lv->getText( row, 0 ).c_str() );
                        }
                    } else if ( msg->fieldlv.lv == load_profiles_lv ) {
                        int row = load_profiles_lv->getActiveRow();
                        if ( load_profiles_lv->isValidRow( row ) == true ) {
                            show_profile_content( load_profiles_lv->getText( row, 0 ), content_ts, content_tv );
                        }
                    }
                default:
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }

    delete win;

    return endmode;
}

void TabProfilesOp::delete_profile( const std::string &profile_name )
{
    std::string basedir = WorkerInitialSettings::getInstance().getConfigBaseDir();

    basedir += "/tabs/";

    std::string filename = AGUIXUtils::formatStringToString( "%s%s.ajson",
                                                             basedir.c_str(),
                                                             profile_name.c_str() );

    worker_unlink( filename.c_str() );
}
