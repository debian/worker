/* bgcopy_requestmessage.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "bgcopy_requestmessage.hh"

BGCopyRequestMessage::BGCopyRequestMessage( const std::string &title,
                                            const std::string &text,
                                            const std::string &buttons )
    : m_type( REGULAR_REQUEST ),
      m_title( title ),
      m_text( text ),
      m_buttons( buttons ),
      m_res( 0 )
{}

BGCopyRequestMessage::BGCopyRequestMessage( const std::string &title,
                                            const std::string &text,
                                            const std::string &default_string,
                                            const std::string &buttons,
                                            Requester::request_flags_t flags )
    : m_type( STRING_REQUEST ),
      m_title( title ),
      m_text( text ),
      m_default_string( default_string ),
      m_buttons( buttons ),
      m_flags( flags ),
      m_res( 0 )
{}

int BGCopyRequestMessage::getRes() const
{
    return m_res;
}

std::string BGCopyRequestMessage::getResultString() const
{
    return m_result_string;
}

BGCopyRequestMessage::request_type_t BGCopyRequestMessage::getType() const
{
    return m_type;
}

std::string BGCopyRequestMessage::getTitle() const
{
    return m_title;
}

std::string BGCopyRequestMessage::getText() const
{
    return m_text;
}

std::string BGCopyRequestMessage::getDefaultString() const
{
    return m_default_string;
}

std::string BGCopyRequestMessage::getButtons() const
{
    return m_buttons;
}

Requester::request_flags_t BGCopyRequestMessage::getFlags() const
{
    return m_flags;
}

void BGCopyRequestMessage::setResultString( const char *result_string )
{
    m_result_string = result_string;
}

void BGCopyRequestMessage::setRes( int res )
{
    m_res = res;
}
