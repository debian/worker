/* dirbookmarkeditui.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2008-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dirbookmarkeditui.hh"
#include "aguix/aguix.h"
#include "aguix/awindow.h"
#include "aguix/text.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include "aguix/choosebutton.h"
#include "worker_locale.h"
#include "bookmarkdbentry.hh"
#include "aguix/fieldlistview.h"
#include "bookmarkdbproxy.hh"
#include "wconfig.h"
#include <algorithm>

DirBookmarkEditUI::DirBookmarkEditUI( AGUIX &aguix, BookmarkDBProxy &data,
                                      BookmarkDBEntry &entry ) : m_aguix( aguix ),
                                                                 m_data( data ),
                                                                 m_entry( entry )
{
    m_okb = NULL;
    m_name_sg = NULL;
    m_alias_sg = NULL;
    m_useparent_cb = NULL;
    m_cat_lv = NULL;
    m_cat_sg = NULL;

    m_win = std::unique_ptr<AWindow>( new AWindow( &m_aguix,
                                                   0, 0,
                                                   400, 400,
                                                   catalog.getLocale( 805 ),
                                                   AWindow::AWINDOW_DIALOG ) );
    m_win->create();

    AContainer *co1 = m_win->setContainer( new AContainer( m_win.get(), 1, 5 ), true );
    co1->setMaxSpace( 5 );

    AContainer *co1_1 = co1->add( new AContainer( m_win.get(), 2, 1 ), 0, 0 );
    co1_1->setMinSpace( 5 );
    co1_1->setMaxSpace( 5 );
    co1_1->setBorderWidth( 0 );
    co1_1->add( new Text( &m_aguix, 0, 0, catalog.getLocale( 806 ) ),
                0, 0, AContainer::CO_FIX );
    m_name_sg = (StringGadget*)co1_1->add( new StringGadget(  &m_aguix,
                                                              0, 0, 100,
                                                              m_entry.getName().c_str(), 0 ),
                                           1, 0, AContainer::CO_INCW );

    AContainer *co1_2 = co1->add( new AContainer( m_win.get(), 2, 1 ), 0, 1 );
    co1_2->setMinSpace( 5 );
    co1_2->setMaxSpace( 5 );
    co1_2->setBorderWidth( 0 );
    co1_2->add( new Text( &m_aguix, 0, 0, catalog.getLocale( 807 ) ),
                0, 0, AContainer::CO_FIX );
    m_alias_sg = (StringGadget*)co1_2->add( new StringGadget(  &m_aguix,
                                                               0, 0, 100,
                                                               m_entry.getAlias().c_str(), 0 ),
                                            1, 0, AContainer::CO_INCW );

    AContainer *co1_3 = co1->add( new AContainer( m_win.get(), 1, 1 ), 0, 2 );
    co1_3->setMinSpace( 5 );
    co1_3->setMaxSpace( 5 );
    co1_3->setBorderWidth( 0 );
    m_useparent_cb = (ChooseButton*)co1_3->add( new ChooseButton( &m_aguix,
                                                                  0, 0,
                                                                  m_entry.getUseParent(),
                                                                  catalog.getLocale( 808 ),
                                                                  LABEL_LEFT,
                                                                  0 ),
                                                0, 0, AContainer::CO_INCWNR );

    AContainer *co1_5 = co1->add( new AContainer( m_win.get(), 2, 1 ), 0, 3 );
    co1_5->setMinSpace( 5 );
    co1_5->setMaxSpace( 5 );
    co1_5->setBorderWidth( 0 );
    co1_5->add( new Text( &m_aguix, 0, 0, catalog.getLocale( 831 ) ),
                0, 0, AContainer::CO_INCH + AContainer::ACONT_NORESIZE );

    AContainer *co1_5_1 = co1_5->add( new AContainer( m_win.get(), 1, 2 ), 1, 0 );
    co1_5_1->setMinSpace( 0 );
    co1_5_1->setMaxSpace( 0 );
    co1_5_1->setBorderWidth( 0 );
    m_cat_lv = (FieldListView*)co1_5_1->add( new FieldListView( &m_aguix, 0, 0, 100, 100, 0 ),
                                             0, 0, AContainer::CO_MIN );
    m_cat_lv->setHBarState( 2 );
    m_cat_lv->setVBarState( 2 );

    std::list<std::string> cats = data.getCats();
    const std::map<std::string, WConfig::ColorDef::label_colors_t> labels = wconfig->getColorDefs().getLabelColors();
    std::map<std::string, WConfig::ColorDef::label_colors_t>::const_iterator label_it;

    for ( label_it = labels.begin(); label_it != labels.end(); ++label_it ) {
        if ( std::find( cats.begin(), cats.end(),
                        label_it->first ) == cats.end() ) {
            cats.push_back( label_it->first );
        }
    }
    if ( std::find( cats.begin(), cats.end(),
                    m_entry.getCategory() ) == cats.end() ) {
        cats.push_back( m_entry.getCategory() );
    }
    for ( std::list<std::string>::iterator it1 = cats.begin();
          it1 != cats.end();
          ++it1 ) {
        int row = m_cat_lv->addRow();
        m_cat_lv->setText( row, 0, *it1 );
        m_cat_lv->setPreColors( row, FieldListView::PRECOLOR_ONLYACTIVE );
    }

    m_cat_sg = (StringGadget*)co1_5_1->add( new StringGadget(  &m_aguix,
                                                               0, 0, 100,
                                                               m_entry.getCategory().c_str(), 0 ),
                                            0, 1, AContainer::CO_INCW );

    AContainer *co1_4 = co1->add( new AContainer( m_win.get(), 2, 1 ), 0, 4 );
    co1_4->setBorderWidth( 0 );
    co1_4->setMinSpace( 5 );
    co1_4->setMaxSpace( -1 );
    m_okb = (Button*)co1_4->add( new Button( &m_aguix,
                                             0,
                                             0,
                                             catalog.getLocale( 11 ),
                                             0 ), 0, 0, AContainer::CO_FIX );

    m_cancelb = (Button*)co1_4->add( new Button( &m_aguix,
                                                 0,
                                                 0,
                                                 catalog.getLocale( 8 ),
                                                 0 ), 1, 0, AContainer::CO_FIX );

    m_win->contMaximize( true );
    m_win->setDoTabCycling( true );
}

DirBookmarkEditUI::~DirBookmarkEditUI()
{
}

int DirBookmarkEditUI::mainLoop()
{
    m_win->show();
    
    AGMessage *msg;
    int endmode = 0;
    for ( ; endmode == 0; ) {
        msg = m_aguix.WaitMessage( NULL );
        if ( msg != NULL ) {
            switch ( msg->type ) {
              case AG_CLOSEWINDOW:
                  endmode = -1;
                  break;
              case AG_BUTTONCLICKED:
                  if ( msg->button.button == m_okb ) {
                      endmode = 1;
                  } else if ( msg->button.button == m_cancelb ) {
                      endmode = -1;
                  }
                  break;
              case AG_KEYPRESSED:
                  if ( msg->key.key == XK_Escape ) {
                      endmode = -1;
                  }
                  break;
              case AG_FIELDLV_ONESELECT:
              case AG_FIELDLV_MULTISELECT:
                  if ( msg->fieldlv.lv == m_cat_lv ) {
                      int row = m_cat_lv->getActiveRow();
                      if ( m_cat_lv->isValidRow( row ) == true ) {
                          m_cat_sg->setText( m_cat_lv->getText( row, 0 ).c_str() );
                      }
                  }
                  break;
            }
            m_aguix.ReplyMessage( msg );
        }
    }

    if ( endmode == 1 ) {
        BookmarkDBEntry newentry( m_entry );
        newentry.setName( m_name_sg->getText() );
        newentry.setAlias( m_alias_sg->getText() );
        newentry.setUseParent( m_useparent_cb->getState() );
        newentry.setCategory( m_cat_sg->getText() );
        m_data.updateEntry( m_entry, newentry );
    }

    m_win->hide();
    
    return endmode;
}
