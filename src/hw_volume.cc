/* hw_volume.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2010 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "hw_volume.hh"

HWVolume::HWVolume( const std::string &udi ) : m_udi( udi ),
                                               m_mounted( false ),
                                               m_is_available( false ),
                                               m_is_in_fstab( false ),
                                               m_size( 0 )
{
}

HWVolume::HWVolume() : m_mounted( false ),
                       m_is_available( false ),
                       m_is_in_fstab( false ),
                       m_size( 0 )
{
}

HWVolume::~HWVolume()
{
}


std::string HWVolume::getUDI() const
{
    return m_udi;
}

void HWVolume::setIsMounted( bool nv )
{
    m_mounted = nv;
}

void HWVolume::setDevice( const std::string &nv )
{
    m_dev = nv;
}

void HWVolume::setMountPoint( const std::string &nv )
{
    m_mp = nv;
}

bool HWVolume::isMounted() const
{
    return m_mounted;
}

std::string HWVolume::getMountPoint() const
{
    return m_mp;
}

std::string HWVolume::getDevice() const
{
    return m_dev;
}

void HWVolume::setIsAvailable( bool nv )
{
    m_is_available = nv;
}

bool HWVolume::isAvailable() const
{
    return m_is_available;
}

void HWVolume::setIsInFStab( bool nv )
{
    m_is_in_fstab = nv;
}

bool HWVolume::isInFStab() const
{
    return m_is_in_fstab;
}

bool HWVolume::operator== ( const HWVolume &rhs ) const
{
    if ( m_mounted != rhs.m_mounted ) return false;
    if ( m_is_available != rhs.m_is_available ) return false;
    if ( m_is_in_fstab != rhs.m_is_in_fstab ) return false;

    if ( m_udi != rhs.m_udi ) return false;
    if ( m_dev != rhs.m_dev ) return false;
    if ( m_mp != rhs.m_mp ) return false;
    if ( m_supposed_mp != rhs.m_supposed_mp ) return false;

    return true;
}

bool HWVolume::operator!= ( const HWVolume &rhs ) const
{
    return ! operator==( rhs );
}

bool HWVolume::operator<( const HWVolume &rhs ) const
{
    if ( m_dev.length() > 0 && rhs.m_dev.length() > 0 ) {
        return m_dev < rhs.m_dev;
    } else if ( m_udi.length() > 0 && rhs.m_udi.length() > 0 ) {
        return m_udi < rhs.m_udi;
    } else {
        if ( m_dev.length() > 0 ) {
            if ( rhs.m_udi.length() > 0 ) {
                return m_dev < rhs.m_udi;
            }
        } else if ( rhs.m_dev.length() > 0 ) {
            if ( m_udi.length() > 0 ) {
                return m_udi < rhs.m_dev;
            }
        }
    }
    return false;
}

void HWVolume::setSupposedMountPoint( const std::string &nv )
{
    m_supposed_mp = nv;
}

std::string HWVolume::getSupposedMountPoint() const
{
    return m_supposed_mp;
}

bool HWVolume::sameDevice( const HWVolume &rhs ) const
{
    if ( m_udi != rhs.m_udi ) return false;
    if ( m_dev != rhs.m_dev ) return false;
    if ( m_supposed_mp != rhs.m_supposed_mp ) return false;

    return true;
}

void HWVolume::setLabel( const std::string &nv )
{
    m_label = nv;
}

std::string HWVolume::getLabel() const
{
    return m_label;
}

void HWVolume::setSize( loff_t nv )
{
    m_size = nv;
}

loff_t HWVolume::getSize() const
{
    return m_size;
}
