/* dirbookmarkop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dirbookmarkop.hh"
#include "listermode.h"
#include "ownop.h"
#include "bookmarkdb.hh"
#include "bookmarkdbentry.hh"
#include "dirbookmarkui.hh"
#include "dirbookmarkaddui.hh"
#include "nwc_fsentry.hh"
#include "nwc_path.hh"
#include "bookmarkdbproxy.hh"
#include "nmspecialsourceext.hh"
#include "worker.h"
#include "worker_locale.h"
#include "fileentry.hh"
#include "argclass.hh"

const char *DirBookmarkOp::name = "DirBookmarkOp";

DirBookmarkOp::DirBookmarkOp() : FunctionProto()
{
}

DirBookmarkOp::~DirBookmarkOp()
{
}

DirBookmarkOp *DirBookmarkOp::duplicate() const
{
    DirBookmarkOp *ta = new DirBookmarkOp();
    return ta;
}

bool DirBookmarkOp::isName(const char *str)
{
    if ( strcmp( str, name ) == 0 )
        return true;
    else
        return false;
}

const char *DirBookmarkOp::getName()
{
    return name;
}

int DirBookmarkOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    Lister *l1;
    ListerMode *lm1;
    
    l1 = msg->getWorker()->getActiveLister();
    if ( l1 == NULL )
        return 1;

    msg->getWorker()->setWaitCursor();

    BookmarkDBProxy &bookmarks = Worker::getBookmarkDBInstance();
    bookmarks.read();
    DirBookmarkUI ui( *msg->getWorker(),
                      *Worker::getAGUIX(),
                      bookmarks );

    lm1 = l1->getActiveMode();
    if ( lm1 != NULL ) {
        std::list< NM_specialsourceExt > sellist;
        lm1->getSelFiles( sellist, ListerMode::LM_GETFILES_ONLYACTIVE );
            
        std::string dirname, basename;

        dirname = lm1->getCurrentDirectory();

        if ( sellist.empty() == false ) {
            if ( (*sellist.begin()).entry() != NULL ) {
                basename = (*sellist.begin()).entry()->name;
                dirname = NWC::Path::dirname( sellist.begin()->entry()->fullname );
            }
        }
            
        ui.setCurrentDirname( dirname );
        ui.setCurrentBasename( basename );
    }
        
    if ( ui.mainLoop() == 1 ) {
        try {
            BookmarkDBEntry sel_entry = ui.getSelectedEntry();

            // enable path jump in progress flag so the next entry used
            // will be stored in path jump DB too
            msg->getWorker()->setPathJumpInProgress( ui.getFilterString() );

            std::string dir = sel_entry.getName();
            std::string highlight_entry;
                
            if ( sel_entry.getUseParent() == true ) {
                NWC::FSEntry fse( dir );
                    
                highlight_entry = fse.getBasename();
                dir = fse.getDirname();
            }

            std::list< RefCount< ArgClass > > args;

            args.push_back( RefCount< ArgClass >( new StringArg( dir ) ) );
            lm1->runCommand( "enter_dir", args );

            args.clear();

            if ( ! highlight_entry.empty() ) {
                args.push_back( RefCount< ArgClass >( new StringArg( highlight_entry ) ) );
                lm1->runCommand( "activate_entry", args );
            }
        } catch ( int ) {
        }
    }

    msg->getWorker()->unsetWaitCursor();
    return 0;
}

const char *DirBookmarkOp::getDescription()
{
    return catalog.getLocale( 1292 );
}
