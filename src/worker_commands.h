/* worker_commands.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WORKER_COMMANDS_H
#define WORKER_COMMANDS_H

#include "worker.h"
#include "basic_actions.h"
#include "copyop.h"
#include "deleteop.h"
#include "normalops.h"
#include "reloadop.h"
#include "makedirop.h"
#include "ownop.h"
#include "renameop.h"
#include "dirsizeop.h"
#include "startprogop.h"
#include "searchentryop.h"
#include "enterpathop.h"
#include "scrolllisterop.h"
#include "createsymlinkop.h"
#include "changesymlinkop.h"
#include "chmodop.h"
#include "togglelistermodeop.h"
#include "setsortmodeop.h"
#include "setfilterop.h"
#include "shortkeyfromlistop.h"
#include "chownop.h"
#include "scriptop.h"
#include "showdircacheop.h"
#include "parentactionop.h"
#include "nooperationop.h"
#include "goftpop.h"
#include "intviewop.h"
#include "searchop.hh"
#include "dirbookmarkop.hh"
#include "opencontextmenuop.hh"
#include "run_custom_action.hh"
#include "openworkermenuop.hh"
#include "changelabelop.hh"
#include "modifytabsop.hh"
#include "changelayoutop.hh"
#include "enterdirop.hh"
#include "volumemanagerop.hh"
#include "switchbuttonbankop.hh"
#include "pathjumpop.hh"
#include "clipboardop.hh"
#include "commandmenuop.hh"
#include "view_newest_files_op.hh"

#include "listermode.h"
#include "showimagemode.h"
#include "informationmode.h"
#include "textviewmode.hh"
#include "virtualdirmode.hh"

#include "chtimeop.hh"

#include "changecolumnsop.hh"

#include "dircompareop.hh"

#include "tabprofilesop.hh"

#include "externalvdirop.hh"

#include "helpop.hh"

#include "view_command_log_op.hh"

#include "activatetextviewmodeop.hh"

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
