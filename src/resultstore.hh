/* resultstore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2024 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef RESULTSTORE_HH
#define RESULTSTORE_HH

#include "wdefines.h"
#include <string>
#include <list>
#include <utility>
#include "searchthread.hh"
#include "nwc_virtualdir.hh"

class SearchSettings;

class ResultStore
{
public:
    ResultStore( const std::string &searchname,
                 bool follow_symlinks = false,
                 bool search_vfs = false,
                 bool search_same_fs = false );
    ~ResultStore();
    NWC::VirtualDir *getDir();
    NWC::VirtualDir *getClonedDir() const;
    std::list<SearchThread::SearchResult> *getRes();

    void setSearchSettings( const SearchSettings &sets );
    SearchSettings getSearchSettings() const;
    void setActiveRow( int row );

    void removeEntries( const std::list< std::pair< std::string, int > > &entry_list );
    void removeEntriesIf( std::function< bool ( const std::string &fullname, int line_nr ) > predicate );

    bool set_exclusive( void *p );
    void unset_exclusive( void *p );
    bool is_exclusive() const;

    size_t get_excluded() const;
    size_t get_removed() const;
    void set_excluded( size_t v );
    void set_removed( size_t v );
private:
    NWC::VirtualDir *_dir;
    std::list<SearchThread::SearchResult> *_res;

    SearchSettings _settings;

    void *m_p;

    size_t m_excluded = 0;
    size_t m_removed = 0;
};

#endif
