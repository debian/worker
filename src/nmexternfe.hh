/* nmexternfe.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NMEXTERNFE_HH
#define NMEXTERNFE_HH

#include "wdefines.h"
#include <string>
#include <optional>

class FileEntry;

class NM_extern_fe {
public:
  NM_extern_fe( const char *nfn );
  NM_extern_fe( const char *nfn, const FileEntry *fe );
  ~NM_extern_fe();
  NM_extern_fe( const NM_extern_fe &other );
  NM_extern_fe &operator=( const NM_extern_fe &other );

  char *getFullname(bool noext);
  char *getName(bool noext);
  std::optional< std::string > getExtension() const;
  void setDirFinished(int);
  int getDirFinished();
  const FileEntry *getFE();
protected:
  char *fullname[2];
  char *name[2];
  FileEntry *fe;
  int dirfinished;
};

#endif
