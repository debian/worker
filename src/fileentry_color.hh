/* fileentry_color.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILEENTRY_COLOR_HH
#define FILEENTRY_COLOR_HH

#include "wdefines.h"
#include "fileentry_customcolor.hh"
#include <memory>

class FileEntryColor
{
public:
    FileEntryColor();
    FileEntryColor( const FileEntryColor &other );
    ~FileEntryColor();
    FileEntryColor &operator=( const FileEntryColor &rhs );

    int getCustomColorSet() const;
    void setCustomColorSet( int v );

    void setCustomColor( const FileEntryCustomColor &c );
    const FileEntryCustomColor &getCustomColor() const;
    void setCustomColor( int type, int fg, int bg );
    int getCustomColor( int type, int *fg_return, int *bg_return ) const;
    void setCustomColors( const int fg[4], const int bg[4] );
    const int *getCustomColors( bool bg ) const;

    void setOverrideColor( const FileEntryCustomColor &c );
    const FileEntryCustomColor &getOverrideColor() const;
    int getOverrideColorSet() const;
    void setOverrideColorSet( int v );

    void setOverrideColor( int type, int fg, int bg );
    int getOverrideColor( int type, int *fg_return, int *bg_return ) const;
    void setOverrideColors( const int fg[4], const int bg[4] );
    const int *getOverrideColors( bool bg ) const;
private:
    std::unique_ptr<FileEntryCustomColor> m_custom_color;
    std::unique_ptr<FileEntryCustomColor> m_override_color;
};

#endif
