Installation Worker
^^^^^^^^^^^^^^^^^^^

###################
# Info about AVFS #
###################

Worker supports the virtual file system AVFS to look inside archives, enter
ftp sites and so on. To activate the support for this you need to install
avfs too, at least version 0.9.5 or higher which can be downloaded from:

  http://avf.sourceforge.net/

There are multiple installation targets but the recommended way to install it
is the shared library which can be installed with
  ./configure --enable-library && make && make install
inside the avfs directory.
After installation you can continue with the installation of Worker.

#####################
# Optional packages #
#####################

For a full feature set you may want to install one of the following
optional libraries:

 - Libmagic for file type recognition based on the magic database
   from the 'file' command. Libmagic comes with recent 'file'
   versions (ftp://ftp.astron.com/pub/file/).

 - HAL support is enabled when the libraries hal, hal-storage and
   dbus-glib-1 are installed.

########################
# Typical installation #
########################

./configure
make
make install       (as root)

############
# Detailed #
############

1.Start configure with all wanted options

    ./configure
  
  or e.g.

    ./configure --prefix=/somewhere/to/install

2.Start compilation
  
    make

3.Become root if you don't have write access to the installation directory
  (defaults to /usr/local )
  
    su

4.Install worker
  
    make install

#########
# Start #
#########

Type "worker" to start worker

#############
# Uninstall #
#############

To uninstall just do

  make uninstall

The user-configurations will not be removed so for full uninstall remove the
directory "$HOME/.worker" or "$HOME/.config/worker".
